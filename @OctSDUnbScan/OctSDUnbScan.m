classdef OctSDUnbScan < GenericScan %& GridScan
    %SCAN ocutpole and SDAE - SDBD unbalance
    %
    % vary Octupoles and SD sextupoles in a grid of points to seek for optimum.
    %
    %see also: GenericScan
    
    properties
        
        
        OctInitial
        OctDev
        
    end
    
    
    methods
        
        % creator
        function obj = OctSDUnbScan(machine)
            %OctSDUnbScan Construct an instance of this class
            %
            % for help type:
            % >> doc TuneScan
            %
            %see also: GenericScan
            
            obj@GenericScan(machine);
            
            obj.x_label = 'SD unb';
            obj.y_label = 'Oct';
            
            obj.C_h_range =  [-1 1]; % 2x1 array of min and max Q_h
            obj.C_v_range =  [-1 1]; % 2x1 array of min and max Q_v
            
            obj.OctDev = tango.Device('srmag/m-o/all');
            obj.OctInitial = obj.OctDev.CorrectionStrengths.set;
            
            obj.scan_name = ['Oct_SDunb_' datestr(now,'YYYY_mm_dd_HH_MM')]; 

        end
        
        function Reset(obj)
            % reset grid and measurement arrays for a new scan, returns to the initial WP
            obj.OctDev.CorrectionStrengths = obj.OctInitial;
            obj.SextDev.CorrectionStrengths = obj.SextInitial;
            
            Reset@GenericScan(obj);
        end
        
        function ExecuteChange(obj,DesiredChroma)
            % change chroma
            
            % get present tunes.
            
            inittune =  obj.GetTune; % read tunes
            disp(['Present tune: ' num2str(inittune)]);

            
            disp('desired change:')

            disp(DesiredChroma);
            
            unbvect = zeros(size(obj.SextInitial));
            unbvect([1:6:end,6:6:end]) = 1;
            unbvect([3:6:end,4:6:end]) = -1;
            OctStep = 40;
            
            obj.SextDev.CorrectionStrengths = obj.SextInitial + DesiredChroma(1)*unbvect;
            obj.OctDev.CorrectionStrengths = obj.OctInitial + DesiredChroma(2)*OctStep;
            
            pause(obj.wait_for_tune);
            
             input('press if ready to correct tunes','s');
             
            % change tunes to initial
            obj.movetune(obj.initial_tunes,inittune);
            
        end
        
    end
    
end

