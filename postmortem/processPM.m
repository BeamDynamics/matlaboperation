function dd=processPM(dd)
%PROCESSPM Process postmortem data
% dd = tango.Device('srdiag/bpm/all-brilliances')
% empty input will load PostMortem Data from the above device.
% see also: analyPM dvautocor_mosteff

if isempty(nargin)
    dd = tango.Device('srdiag/bpm/all-brilliances');
end

if ischar(dd)
    dd=load(dd);
end
try
    [T,OV,OH,Xdif,Zdif] = analyPM(dd, dd.All_PM_Sum, dd.All_PM_HPosition, dd.All_PM_VPosition,...
        dd.All_HInterlock, dd.All_HInterlock);
catch
    err=lasterror
    disp(err.message)
    errordlg('Error in data processing','Error');
end
end

