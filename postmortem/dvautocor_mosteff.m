function [corr,orbitfit] = dvautocor_mosteff(orbit, plane)
%DVAUTOCOR_MOSTEFF Compute most-effective correction
%   [CORR,ORBITFIT]=DVAUTOCOR_MOSTEFF(ORBIT,PLANE)
%
%ORBIT:     orbit to be analysed [m]
%PLANE:     orbit plane
%           'h', 'H', 'x', 'X', 1 => horizontal
%           'v', 'V', 'z', 'Z', 2 => vertical
%
%CORR:      10 most effective corrections [index strength[A] chi2]
%ORBITFIT:  Orbit fitted with the most effective steerer
%
%See also: dvautocor_svd

NBPMS = 320;
NCor = 288;

nmeff=10;
[label,devname]=selectplane(plane,{'HORIZONTAL','VERTICAL'},{'sr/beam-orbitcor/meffh','sr/beam-orbitcor/meffv'});
autocor=tango.Device(devname);

% fill with NaN/Zero/Something the spark data 
orbit_all = NaN(NBPMS,1);
selbrilliances = [1:10:NBPMS,2:10:NBPMS,4:10:NBPMS,7:10:NBPMS,9:10:NBPMS,10:10:NBPMS];
orbit_all(selbrilliances) = orbit; 
orbit = orbit_all;

result=autocor.Compute(reshape(orbit,1,[]));
corr=reshape(result.dvalue,nmeff,[]);
bestcor=sr.steername(corr(1,1)+1);
fprintf('     %10s  %6s  %5s\n','steerer','strength','Chi2');
for snum=1:10
    ids=corr(snum,1)+1;
    fprintf('%4d %10s  %6.3f  %g\n',ids,...
        srsteername(ids), corr(snum,2),corr(snum,3));
end
kicks=zeros(NCor,1);
kicks(corr(1,1)+1)=corr(1,2);
testcor=[1,corr(1,1:2)];
result=autocor.Test(testcor);
orbitfit=-result';

sbpm=(0.5+(0:(NBPMS-1))')/NBPMS;
sticks=linspace(0,1,17);
slabels=[4:2:32 2 4];
subplot(2,1,1);
plot(sbpm, [orbit orbitfit]);
ax=axis;
axis([0 1 ax(3:4)]);
set(gca,'XTick',sticks,'XTickLabel',slabels);
xlabel('Cell');
ylabel('orbit');
title(label);
grid on
legend('measured',['reconstructed with ' bestcor]);

skick=(0.5+(0:(Ncor-1))')/Ncor;
subplot(2,1,2);
bar(skick,kicks);
ax=axis;
axis([0 1 ax(3:4)]);
set(gca,'XTick',sticks,'XTickLabel',slabels);
xlabel('Cell');
ylabel('Strength');
end

