function resetPM()
%RESETPM Reset all PM notifications
%   
%see also: savePMdata processPM


dev = tango.Device('srdiag/bpm/all-brilliances');
trydev(dev,'ResetInterlocks');
trydev(dev,'ResetPM');
dvclose(dev);

function trydev(dev,cmd)
for trial=1:3
    pause(2);
    try
        dev.(cmd);
        break;
    catch
        err=lasterror;
        if ~strncmp(err.identifier,'Machdev:',8)
            rethrow(err);
        end
    end
end
end

end
