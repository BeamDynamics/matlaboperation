function dd=savePMdata(filename)
%SAVEPMDATA Read and save Postmortem data
%
bpm=tango.Device('srdiag/bpm/all-brilliances');

bpm.Timeout=5;
dd.date=now;
dd.All_PM_Notified=bpm.All_PM_Notified.read;
dd.All_PM_Notified=double(bpm.All_PM_Notified.read);
dd.All_HInterlock=bpm.All_HInterlock.read;
dd.All_VInterlock=bpm.All_VInterlock.read;
fprintf(1, '>> Flags recorded\n');
dd.All_PM_Sum=bpm.All_PM_Sum.read';
fprintf(1, '>> Sum data recorded\n');
pause(1);
dd.All_PM_HPosition=bpm.All_PM_HPosition.read';
fprintf(1, '>> X_Position data recorded\n');
pause(1);
dd.All_PM_VPosition=bpm.All_PM_VPosition.read';
fprintf(1, '>> Z_Position recorded\n');
try
    bpm.ResetInterlocks();
    fprintf(1, '>> Interlocks reset\n');
catch err
    if isa(err,'cs.DevError')
        fprintf(2,err.message);
    else
        rethrow(err);
    end
end
try
    bpm.ResetPM();
    fprintf(1, '>> PM Notifications reset\n');
catch err
    if isa(err,'cs.DevError')
        fprintf(2,err.message);
    else
        rethrow(err);
    end
end

if nargin >= 1
    save(filename,'-struct','dd');
end
end
