classdef model < atmodel
    %Class providing access to SY optical values
    %
    %Available locations:
    %
    %bpm:   All BPMs
    %hbpm:  Horizontal BPMs
    %h_bpms:Logical mask (1x75) of horizontal BPMs
    %vbpm:  Vertical BPMs
    %v_bpms:Logical mask (1x75) of vertical BPMs
    %qp:    All quadrupoles
    %sx:    All sextpoles
    %steerh:    Horizontal steerers
    %steerv:    Vertical steerers
    %
    %Available parameters:
    %
    %0:     index in the AT structure
    %1:     Integrated strength
    %2:     theta/2pi
    %3:     betax
    %4:     alphax
    %5:     phix/2pi/nux
    %6:     etax
    %7:     eta'x
    %8:     betaz
    %9:     alphaz
    %10:    phiz/2pi/nux
    %11:    etaz
    %12:    eta'z
    %
    %Default parameters:
    %
    %[   2         3         5        6      8       10      ]
    %[theta/2Pi, betax, Phix/2PiNux, etax, betaz, Phiz/2PiNux]
    
    properties 
        directory 
    end
    
    methods (Static)
        function ring=checklattice(ring,varargin)
            %NEWRING = EBS.CHECKLATTICE(RING)   Check the validity of an EBS lattice
            %
            %CHECKLATTICE will
            %-  set the QUADRUPOLE, BEND and MULTIPOLE pass methods
            %-  Add if necessary markers at ID locations (centre of straight sections)
            %-  Add if necessary markers at pinhole camera locations
            %-  Rename the BPMs with their device name
            %
            %CHECKLATTICE(...,'energy',energy)
            %   Set the ring energy
            %
            %CHECKLATTICE(...,'reduce',true,'keep',pattern)
            %   Remove elements with PassMethod='IdentityPass' and merge adjacent
            %   similar elements, but keeps elements with FamName matching "pattern"
            %   Pattern may be a logical mask.
            %   (Default: 'BPM.*|ID.*' for SR, '^BPM_..|^ID.*|JL1[AE].*|CA[02][57]').
            %
            %CHECKLATTICE(...,'remove',famnames)
            %   remove elements identified by the family names (cell array)
            %
            %CHECKLATTICE(...,'reduce',true)
            %   group similar elements together by calling atreduce
            %
            %CHECKLATTICE(...,'MaxOrder',n)
            %   Limit the order of polynomial field expansion to n at maximum
            %
            %CHECKLATTICE(...,'NumIntSteps',m)
            %   Set the NumIntSteps integration parameter to at least m
            %
            %CHECKLATTICE(...,'QuadFields',quadfields)
            %   Set quadrupoles fields to the specified ones.
            %   Default: {}
            %
            %CHECKLATTICE(...,'BendFields',bendfields)
            %   Set bending magnet fields to the specified ones.
            %   Default: {}

            %CHECKLATTICE(...,'DeviceNames',true)
            %   add device names in AT structure.
            
            [check,options]=getoption(varargin,'CheckLattice',true);
            if check
                [energy,periods]=atenergy(ring);
                [energy,options]=getoption(options,'energy',energy);
                [periods,options]=getoption(options,'Periods',periods);
                [keep,options]=getoption(options,'keep',false(size(ring)));
                if ~islogical(keep), keep=atgetcells(ring,'FamName',keep); end
                
                precious=atgetcells(ring,'Class','Monitor');
                ring=atclean(ring,options{:},'keep',keep|precious);
                
                rp=atgetcells(ring,'Class','RingParam');
                if any(rp)
                    params=ring(rp);
                    params{1}.Energy=energy;
                    params{1}.Periodicity=periods;
                else
                    params={atringparam('TL2',energy,1)};
                end
                ring=[params(1);ring(~rp)];
            end
        end
        
        function pth=getdirectory(varargin)
            global APPHOME
            nm=getargs(varargin,{'theory'});
            directory=fullfile(APPHOME,'optics','tl2');
            pth=fullfile(directory,nm);
        end
    end
    
   
    methods
        function this=model(varargin)
            %Builds the linear model of the ring
            %
            %M=SY.MODEL()               takes the lattice from
            %           $APPHOME/sy/optics/settings/theory/betamodel.mat
            %M=SY.MODEL('opticsname')   takes the lattice from
            %           $APPHOME/sy/optics/settings/opticsname/betamodel.mat
            %M=SY.MODEL(path)           takes the lattice from
            %           path or path/betamodel.mat
            %M=SY.MODEL(AT)             Uses the given AT structure
            
            this@atmodel(tl2.getat(varargin{:}));
            this.directory = this.getdirectory;
        end
        
        
    end
    
    methods (Access=protected)
        function idx=elselect(this,code)
            %Return indices of selected elements
            switch lower(code)
                case 'bpm'
                    idx=atgetcells(this.ring,'Class','Monitor') ;
                case 'steerh'
                    idx=atgetcells(this.ring,'FamName','.*ch[0-9]+');
                case 'steerv'
                    idx=atgetcells(this.ring,'FamName','.*cv[0-9]+');
                case 'sx'
                    idx=atgetcells(this.ring,'Class','ThinMultipole') | ...
                        atgetcells(this.ring,'Class','Sextupole');
                case 'qp'
                    idx=atgetcells(this.ring,'Class','Quadrupole');
                otherwise
                    idx=atgetcells(this.ring,'FamName',upper(code));
            end
        end
    end
end
