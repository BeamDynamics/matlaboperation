
function func_resp_mat(LATTICE,OUTDIR)
%lattice cell array name should be TL2 for this to work properly
%compile with: mcc -v -T link:lib -W cpplib:libfunc_resp_mat -d ../lib func_resp_mat.m passmethodlist
%matrices defined as n_pickups x n_corrector
%added fake line for SY pickup filled with zeros
%lib -> /operation/machine/lib/debian8/.
%.h -> /operation/machine/include
%lattice -> operation/appdata/tl2/optics/theory

load(LATTICE);

QuadNameList={'tl2/ps-q2/qf1','tl2/ps-q2/qd2','tl2/ps-q1/qf3','tl2/ps-q1/qf4',...
              'tl2/ps-q1/qd5','tl2/ps-q2/qf6','tl2/ps-q2/qf7','tl2/ps-q2/qd8',...
              'tl2/ps-q2/qf9','tl2/ps-q2/qd10','tl2/ps-q2/qf11','tl2/ps-q1/qd12',...
              'tl2/ps-q1/qf13','tl2/ps-q2/qd14'};
          
Excluded_corrv = {'CV2','CV9'};

kvals = zeros(length(QuadNameList),1);
%Not very nice: transfer function?? Polarity based on name??
for i=1:length(QuadNameList)
    device = tango.Device(QuadNameList{i});
    current = device.Current().set;   
    polarity = 0.0;
    if strfind(QuadNameList{i},'qf')
      polarity = 1.0;
    elseif strfind(QuadNameList{i},'qd')
        if strfind(QuadNameList{i},'14')
          polarity = 1.0;
        else
          polarity = -1.0;
        end
    end
    k = current*polarity/92.1623;   
    kvals(i)=k;
end

quads = atgetcells(TL2,'Class','Quadrupole');
TL2(quads) = atsetfieldvalues(TL2(quads),'PolynomB',{2},kvals);

sli = findcells(TL2,'FamName','^SL_\w*');

dk = 1.0e-4;
di = dk/161.0e-6;

corrh = findcells(TL2,'FamName','^CH\w*');
nch=length(corrh);
matx = zeros(length(sli)+1,nch);
for i =1:nch
    part=[0.0 0.0 0.0 0.0 0.0 0.0]';
    l = atgetfieldvalues(TL2,corrh(i),'Length');
    TL2(corrh(i)) = atsetfieldvalues(TL2(corrh(i)),'PolynomB',{1},-dk/l);
    part = linepass(TL2,part,sli);
    TL2(corrh(i)) = atsetfieldvalues(TL2(corrh(i)),'PolynomB',{1},0.0);
    matx(2:length(sli)+1,i)=part(1,:)./di;
end

csvwrite(strcat(OUTDIR,'mat_h.dat'),matx)


corrv = findcells(TL2,'FamName','^CV\w*');
corrv(findcells(TL2(corrv),'FamName',Excluded_corrv{:}))=[];
ncv=length(corrv);
maty = zeros(length(sli)+1,ncv);
for i =1:ncv
    part=[0.0 0.0 0.0 0.0 0.0 0.0]';
    l = atgetfieldvalues(TL2(corrv(i)),'Length');
    TL2(corrv(i)) = atsetfieldvalues(TL2(corrv(i)),'PolynomA',{1},dk/l);
    part = linepass(TL2,part,sli);
    TL2(corrv(i)) = atsetfieldvalues(TL2(corrv(i)),'PolynomA',{1},0.0);
    maty(2:length(sli)+1,i)=part(3,:)./di;
end

csvwrite(strcat(OUTDIR,'mat_v.dat'),maty)

end
