function CreateOpticsFiles(r,twiin,opticsname,varargin)
% CREATEOPTICSFUILES(r,opticsname,'property',value,...), 
% given an AT lattice R creates the relevant files for operation
%
% operation folder 2018 is APPHOME/ebs/optics/settings/(OPTICSNAME)
% operation folder 2019 could become APPHOME/<commissioingtoolsrepository>/optics/ebssr/(OPTICSNAME) 
%
% optional input ('NAME',val,'Name2',val2,...)
% 'OperationFolder'  : commissioningtools folder where optics/ebssr/theory
%                      link is present.
% 'MeasuredResponse' : folder containing ouput of full RM measurement
% 'errtab' : table of errors
% 'cortab' : table of corrections
%
% WARNING lattice contains QF8D modeled as dipoles. 
% Those are merged to a single quadrupole without bending angle for all 
% computations. This makes any function using dispersion (matching for ex.) 
% incorrect. 
% 
% example:
% load /machfs/liuzzo/esrfupgradelatticefiles-code/S28D/MagneticLengths/AT/errors/WP_021_034__S28D_Z0p67_newErrFunct_1_seed1_.mat
% CreateOpticsFiles(ring_err,'Seed1MeasResp','MeasuredResponse','/machfs/liuzzo/EBS/Simulator/responsematrix/resp2')
% 
%see also: LoadOpticsConfiguration

global APPHOME
p=inputParser;
addRequired(p,'r',@iscell);
addRequired(p,'opticsname',@ischar);
addOptional(p,'OperationFolder',APPHOME);
addOptional(p,'MeasuredResponse',[]);
addOptional(p,'ErrTab',[],@istable);
addOptional(p,'CorTab',[],@istable);

parse(p,r,opticsname,varargin{:});
r           = p.Results.r;
opticsname  = p.Results.opticsname;
OpeFolder   = p.Results.OperationFolder;
rm          = p.Results.MeasuredResponse;
errtab      = p.Results.ErrTab;
cortab      = p.Results.CorTab;

if isempty(errtab)
    errtab = atcreateerrortable(r);
end

if isempty(cortab)
    cortab = atgetcorrectiontable(r,r); % zeros
end

fulfold=fullfile(OpeFolder,'optics','tl2',opticsname);

curdir=pwd;
mkdir(fulfold);
cd(fulfold);

% AT lattice
betamodel=r;
try
    E0 = atenergy(betamodel);
catch
    betamodel = atsetenergy(betamodel,6e9);
end
save('betamodel.mat','betamodel','twiin');

% save image of lattice optics
f=figure;
atplot(r,'inputtwiss',twiin);
saveas(gca,[opticsname '.jpg']);
close(f);

% rfull=tl2.model(r);

% compute fast model once for all

% rmod=tl2.model(r,'reduce',true,'keep','BPM.*|ID.*');


cd(curdir);

end
