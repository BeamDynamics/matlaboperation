function SetBump(machine)
% set the operation kicker bump :
% - relink kicker_bump file
%
% machine may be: 'sr', 'sy', 'tl2'
%
%see also: SetOperationOptics

global APPHOME
[~,u] = system('whoami');

if ~strcmp(strtrim(u),'beamdyn') & ~strcmp(strtrim(u),'liuzzo')
    error('only beamdyn is allowed to do this')
end

directory=fullfile(APPHOME,'optics',machine);
tango_host = ['tango://' getenv('TANGO_HOST')];

% move to operation directory
curdir = pwd;
cd(directory);

% get present target directory
[~, theodir]= system('readlink -f theory');
cd(theodir(1:end-1))

[~, theobump]= system(['readlink -f kicker_bump']);
[bumpfold,curtheo]=fileparts(theobump);

% prompt to optics folder selection
[newtheobump,newtheobumpFodler] = uigetfile(['Present kicker bump is: ' theobump]);

% cancel from popo up window.
if ~newtheobump
    disp(['Nothing done. reference kicker bump is : ' theobump]);
    % link to theory bump
    if ~exist('kicker_bump','file')
        system('ln -s ./kicker_bumps/injbumpmat.csv kicker_bump');
        disp('kicker_bump link created.')
    else
        disp('kicker_bump link already exists.')
    end
    %return to original folder
    cd(curdir);
    return
end

% remove old link
system(['rm kicker_bump']);

% create new link
%[~,newtheodirrel] = fileparts(newtheobump);
system(['ln -s ' fullfile(newtheobumpFodler,newtheobump) ' kicker_bump']);
system('chmod -R g+rwx ./kicker_bump '); % give folder modification permission group machine


% set table in the bump device server
BumpMat = load(fullfile(theodir(1:end-1),'kicker_bump'));
%bumpdevice = 'test/InterpolBump/1';
bumpdevice = 'tango://acs:10000/sr/ps-bump/inj';
dev = tango.Device(bumpdevice);
dev.InterpolationTable = BumpMat;
disp(['Interpolation table for ' bumpdevice ' has been set to.']);
disp(BumpMat)

% back to initial directory.
cd(curdir);


end
