% change theory folder path

% foldname = '/operation/beamdyn/matlab/optics/sr/theory/';

foldname = '/machfs/liuzzo/EBS/beamdyn/matlab/optics/sr/minibeta_ID31_x7m_y1m/';

AttributesToSet = {...
'srmag/m-q/All/DesignStrengthFile','DesignStrengths.csv';....    
'srmag/m-s/All/DesignStrengthFile','DesignStrengths.csv';....    
'srmag/m-o/All/DesignStrengthFile','DesignStrengths.csv';....    
};

for iatr = 1:length(AttributesToSet)

    attr = tango.Attribute(AttributesToSet{iatr,1});
    attr.set = fullfile(foldname,AttributesToSet{iatr,2});

end


PropertyToSet = {
'srdiag/beam-tune/adjust', 'MatrixFileName', fullfile(foldname,'tunemat.csv');...
'srdiag/beam-tune/main', 'MatrixFileName', fullfile(foldname,'tunemat.csv');...
'sr/beam-chroma/adjust','' ,fullfile(foldname,'chrommat.csv');...
'sr/beam-analysis/harm-h','SteererFileName' ,fullfile(foldname,'h_steerers.csv');...
'sr/beam-analysis/harm-v','SteererFileName' ,fullfile(foldname,'v_steerers.csv');...
'sr/beam-orbitcor/meff-h','ActuatorFileName' ,fullfile(foldname,'h_steerers.csv');...
'sr/beam-orbitcor/meff-v','ActuatorFileName' ,fullfile(foldname,'v_steerers.csv');...
'sr/beam-orbitcor/svd-h','SVDFileName' ,fullfile(foldname,'h_resp.csv');...
'sr/beam-orbitcor/svd-v','SVDFileName' ,fullfile(foldname,'v_resp.csv');...
'sys/ringsimulator/ebs','RingFile' ,fullfile(foldname,'betamodel.mat');...
'sr/source/all','RingFile' ,fullfile(foldname,'betamodel');...
};


for iatr = 1:length(PropertyToSet)

    try
        dev = tango.Device(PropertyToSet{iatr,1}); % create device instance
        dev.set_property(PropertyToSet{iatr,2},PropertyToSet{iatr,3}); %
    catch
        disp(['could not set device: ' PropertyToSet{iatr,1} ' property ' PropertyToSet{iatr,2} ' to ' PropertyToSet{iatr,3}]);
    end
    
end


disp('update by hand Class Property of ResCor ''MatriXFileName''')

disp('to update this info for the bumps device servers, Script from E.Taurel to be called.')

