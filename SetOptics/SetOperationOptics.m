function SetOperationOptics(machine)
% set the operation optics folder:
% - relink theory folder
% - send optics parameters to diagnostics
% - Init Devices where needed.
% - set injection bump (call stand alone SetOpeationInjectionBump)
% - set simulator model (set propery and call reset_ebs_simu)
% - set d-orbit (beam position at ID) properties
%
%
%
% machine may be: 'sr', 'sy', 'tl2'
%
%
%see also: sr.CreateLatticeOpitcs, sy.CreateLatticeOptics,
%tl2.CreateLatticeOptics,


global APPHOME
[~,u] = system('whoami');

if ~strcmp(strtrim(u),'beamdyn') && ~strcmp(strtrim(u),'liuzzo')
    error('only beamdyn is allowed to do this')
end

directory=fullfile(APPHOME,'optics',machine);
tango_host = ['tango://' getenv('TANGO_HOST')];

% move to operation directory
curdir = pwd;
cd(directory);

% get present target directory
[~, theodir]= system('readlink -f theory');
[~,curtheo]=fileparts(theodir);

% prompt to optics folder selection
newtheodir = uigetdir(directory,['Present operation optics is: ' curtheo]);

% cancel from popo up window.
if ~newtheodir
    disp(['Nothing done. reference optics is : ' theodir]);
    cd(curdir);
    return
end

% test if folder is a good optics folder
listing = dir(newtheodir);
switch machine
    case 'sr'
        needed_file_names ={....
            'h_resp.csv',...
            'v_resp.csv',...
            'tunemat.csv',...
            'chrommat.csv',...
            'h_bump.txt',...
            'v_bump.txt',...
            'v_steerers.csv',...
            'h_steerers.csv',...
            'skewcor.csv',...
            'quadcor.csv',...
            'sextcor.csv',...
            'DesignStrengths.csv',...
            'alpha',...
            'bpms',...
            ...'\w*_layout.txt',...
            'betamodel.mat'...
            };
    case 'sy'
        needed_file_names ={....
            'h_resp.csv',...
            'v_resp.csv',...
            'h_motresp.csv',...
            'v_motresp.csv',...
            'h_bumps',...
            'v_bumps',...
            'h_motorbumps',...
            'v_motorbumps',...
            'v_steerers',...
            'h_steerers',...
            'alpha',...
            'bpms',...
            ...'\w*_layout.txt',...
            'betamodel.mat'...
            };
    otherwise
        needed_file_names ={....
            'betamodel.mat'...
            };
        
end
for iiff = 1:length(needed_file_names)
    
    files_present(iiff) = ismember(needed_file_names{iiff},{listing.name});
    
    if files_present(iiff) == 0
        disp([needed_file_names{iiff} ' missing'])
    end
    
end

if any(~files_present)
    
    warning('following files missing: ');
    disp(needed_file_names(~files_present))
    
    disp(['Nothing done. reference optics is : ' theodir]);
    cd(curdir);
    return
end

% remove old link
system(['rm theory']);

% create new link
[~,newtheodirrel] = fileparts(newtheodir);
system(['ln -s ' newtheodirrel ' theory']);
system('chmod -R g+rwx ./theory '); % give folder modification permission group machine

% back to initial directory.
cd(curdir);

% load newly set model
at_pinhole_names = [];

switch machine
    case 'sr'
        latticemodel = sr.model;
        
        % key names in sr.model for poinhole cameras
        at_pinhole_names = {'pinholeD09', 'pinholeD17', 'pinholeD27', 'pinholeID07', 'pinholeID25'};
        % simulatordevname = [tango_host '/sys/ringsimulator/ebs'];
        simulatordevname = '';
        
    case 'sy'
        latticemodel = sy.model;
        simulatordevname ='';
    case 'tl2'
        latticemodel = tl2.model;
        simulatordevname ='';
    otherwise
        error('Input must be one of the ollowing: ''sr'', ''sy'', ''tl2'',')
end

% update design strengths
switch machine
    case 'sr'
        try
            % update design strengths
            DSfile = '/operation/beamdyn/matlab/optics/sr/theory/DesignStrengths.csv';
            dev = tango.Device('srmag/m-q/all'); dev.Timeout=50000; dev.DesignStrengthFile = DSfile;
            dev = tango.Device('srmag/m-s/all'); dev.Timeout=50000; dev.DesignStrengthFile = DSfile;
            dev = tango.Device('srmag/m-o/all'); dev.Timeout=50000; dev.DesignStrengthFile = DSfile;
        catch err
            disp(err)
            warning('Errors writing DesignStrengths.csv')
        end
end

% emittance device parameters
switch machine
    case 'sr'
        % update optics calculator
        try
            disp('Properties of pinhole cameras are updated using OpticsCalculcator device')
            dev=tango.Device('sr/beam-optics/calculator');
            dev.ComputePinholes();
        catch
            warning('Optics Calculator not found / not updated.')
            disp('Optics Calculator not updated.')
        end
    otherwise
        disp(['Optics Calculator not updated for ' machine '.'])
end

% SrOrbit Parameters
switch machine
    case 'sr'
        
        r = atsetcavity(atradoff(latticemodel.ring,'','auto','auto'),6.5e6,0,992);
        
        % use optics without radiation to compute optics at Straigth Sections
        latticemodel=sr.model(r);
        
        [~,iddata,bmdata] = latticemodel.get(0,'id',[3,4,6,7,8,9],'id',[3 8],'bm');
        
        
        sl = repmat(5.3028,32,1);%latticemode.get(13,'bpm');
        
        devorb = tango.Device('srdiag/beam-position/bm_id_all');
        
        devorb.id_Hbeta = iddata(:,1)';
        devorb.id_Halpha = iddata(:,2)';
        devorb.id_Heta = iddata(:,3)';
        devorb.id_HetaPrime = iddata(:,4)';
        devorb.id_Vbeta = iddata(:,5)';
        devorb.id_Valpha = iddata(:,6)';
        devorb.bm_Hbeta = bmdata(:,1)';
        devorb.bm_Vbeta = bmdata(:,2)';
        devorb.ref_H_emit = latticemodel.emith;
        devorb.ref_V_emit = 5e-12;
        devorb.straight_section_length =sl';
        
        devorb.Init();
        
end


% injection bump
switch machine
    case 'sr'
        % set injection bump
        % SetBump(machine);
        disp('bump never used in the last two years. removed')
end


% set model in simulator.
if ~isempty(simulatordevname)
    
    p = tango.Device(simulatordevname); % get pinhole device name from lattice model.
    
    % prompt to simulator file selection
    cursimufile = p.get_property('RingFile');
    simulatorfile = uigetfile(fullfile(directory,'theory'),...
        ['Present simulator file is: ' cursimufile{1}]);
    if simulatorfile~=0
        [~,na,~]=fileparts(simulatorfile); % remoe .mat
        
        p.set_property('RingFile',fullfile(directory,'theory',na));
        
        reset_ebs_simu(tango_host);
    else
        disp(['Simulator file: ' cursimufile ' not changed']);
    end
    
end

disp('PLEASE RELOAD JSRMAG FILE IMMEDIATELY.')

end




