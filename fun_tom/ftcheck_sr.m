function [tunes,cur,traj,mach,machi]=ftcheck_sr(flagtune,average,numbpm)
% [tunes,cur,posm]=ftcheck_sr(flagtune,average,numbpm)  
%This function is making the actual measurement of the first turn
   %trajectory. If asked for (FLAGTUNE==1) then it will also compute the
   %tune on the trajectory measured ion the NUMBPM first BPM's (max=1000). The tune is
   %measured on the horizontal injection oscillation and on the oscillation
   %created by CV9 for the vertical plane.
   %the outputs are the TUNES, CURRENT and POSITIONS measured on the first
   %turn.
   %%
   numbpm=numbpm+10; 
   ithresh=0
  incs3=0.5E-4;
  inccv9=0.5E-4;
  %opens conection to the gun, and KE 
    %%%%Ke=tango.Device('sy/ps-ke/1');   
    %%%%%%gun=tango.Device('elin/beam/run');  
  srall=tango.Device('srdiag/beam-position/all');
    disable=srall.All_Status();
	
  if srall.TBT_Enable.read==false
      fprintf('error bpms are not in TBT mode, switch to TBT or go home')
    
  end
    
    
    
    %%%ramp=tango.Device('sy/ps-rips/manager');
%reads faulty BPM.
    
    bpmok=~bitand(disable.read,2);
%%%bpmok(80)=0;
bpmok=[bpmok bpmok bpmok bpmok bpmok];
  tunes=[0 0];  
%%%ramp.StartRamping();
pause(2);
    %starts a loop to measure turn by turn data AVERAGE times  
    %%%%%%%%res=gun.On();
    for i=1:average
      
      %res=Ke.On();     
        
      pause(2)
          
      [xx,zz,intens]=loadposlib(20);
      xx=xx';
      zz=zz';
      intens=intens';
      
      posxtest(i,:)=xx(:);
      posztest(i,:)=zz(:);
      
      curtest(i,:)=intens(:);
     end
  
 
 pause(3);
 %adapts BPMOK mask to cope with beam presence.
 
 bpmok=bpmok(1:numbpm)&intens(1:numbpm)>ithresh&isfinite(xx(1:numbpm));    
     
%averages the tbt data sets to extract h,v postion and intensity 
      
posm(:,1)=mean2(posxtest(:,1:numbpm),1)';
posm(:,2)=mean2(posztest(:,1:numbpm),1)';
cur=mean2(curtest(:,1:numbpm),1)';
traj=posm;
	
	traj(~bpmok(1:numbpm),:)=nan;
	cur(~bpmok(1:numbpm))=nan;
figure(14)
plot(traj);
    
    %if tune flag is turned on takes data with increases TL2/CV9 and S3
      
%data recording for vetical analysis increasing CV9 averaged over AVERAGE samples   
     
      
      CV=tango.Attribute('srmag/vst/all/Strengths');
       cvini=CV.read;
       cvnew=cvini;
       cvnew(1)=cvnew(1)+inccv9;
       CV.set=cvnew;
 		
	    pause(10)
	  %%%%%%  CV=tango.Attribute('tl2/ps-c1/cv9/current');
          %%%%  cvini=CV.set;
       %%%%% CV.set=cvini(1)+inccv9;
          for i=1:average
         %%%%%       res=Ke.On();   
         %%%%%       pause(3)
               [xx3,zz3,intens3]=loadposlib(20);
                zz3=zz3';
                intens3=intens3';
               posz(i,:)=zz3(1:numbpm);
            	intensv(i,:)=intens3(1:numbpm);
            end
            poszp=mean2(posz,1)';
            
% sets back CV9 to initial value
          CV.set=cvini;
            
            %prepares for Horizontal data sets measurements (again averaged
            %AVERAGE times)
           %%%S3=tango.Attribute('sr/ps-si/3/Current')
            %%%S3ini=S3.set;
           %%% S3.set=S3ini+incs3;
            
         S3=tango.Attribute('srmag/hst/all/Strengths');
       s3ini=S3.read;
      s3new=s3ini;
       s3new(1)=s3new(1)+incs3;
       S3.set=s3new;   
           
        pause(7)   
           %Data taking
            for i=1:average
                %%%%%%res=Ke.On();
                pause(2)
                [xx2,zz2,intens2]=loadposlib(20);
                xx2=xx2';
                intens2=intens2';
                posx(i,:)=xx2(1:numbpm);
                intensh(i,:)=intens2(1:numbpm);
            end
            posxp(:,1)=mean2(posx,1)';
            
	        
	    
	    bpmnok=0==(sum([intensh>ithresh;intensv>ithresh;bpmok(1:numbpm)],1)==((2*average+1)*ones(1,size(intensh,2))));
            
            
            %Stops the gun and reloads S3 inital value
        	
		pause(2);
		  
        S3.set=s3ini;      
       
	%computes h and v trajectory by substracting initial data in order to
	%extract betatron oscillations.
        
		%%%%%%traj=([posxp-posm(:,1) poszp-posm(:,2)]);
traj=([posxp-traj(:,1) poszp-traj(:,2)]);

for i=1:size(traj,1)-10
    traj(i,:)=traj(i,:)./(max(abs(traj(i:i+5,:)))/max(abs(traj(1:1+5,:))));
end
    traj=traj(1:end-10,:);


	bpmok(bpmnok)=0;
	traj(~bpmok,:)=nan;
	% extracts the tunes via fft
        figure(15)
        plot(traj);
	tunes=findtunetom2(traj(:,:),400,1)
        tunes=tunes*320;
 	m=sr.model;          
  	mach=m.ring;
    machi=mach;   
    if flagtune==1
        
        %extract the tunes via fit of model
 	 
        %%%bpm=findcells(mach,'FamName','BPM');
        bpm=m.get(0,'BPM');
        x0=[0 0 0 0 0 0];
        co4=[0 incs3 0 inccv9];
        x0(1:4)=co4;
        %re=fittune_traj(x0,bpm(1:numbpm),mach,traj(1:numbpm,:),co4);
        %[xr,res]=fminsearch(@(x) fittune_traj(x,bpm(1:numbpm),mach,traj(1:numbpm,:),co4),x0);
        %re=fittune_traj(x0,bpm(1:numbpm),mach,traj(1:numbpm,:));

        QD2=findcells(mach,'FamName','QD2\w*');
        QF1=findcells(mach,'FamName','QF1\w*');

        S_QD2ini=getcellstruct(mach,'PolynomB',QD2,1,2).*1.005;
        S_QF1ini=getcellstruct(mach,'PolynomB',QF1,1,2).*0.995;

        mach=setcellstruct(mach,'PolynomB',QD2,S_QD2ini,1,2);
        mach=setcellstruct(mach,'PolynomB',QF1,S_QF1ini,1,2);
        x0(6)=-0.004;
        x0(5)=0.004;
     
        numbpm=numbpm-10;
        OPTIONS = optimset('MaxFunEvals',100);
xr=[0 0]
        %[xr,res]=fminsearch(@(x) fittune_traj(x,bpm,mach,traj(1:numbpm,:),co4),x0(5:6));
        if numel(xr)==2
        x0(5:6)=xr;
        else
        x0=xr;
        end


        mach=setcellstruct(mach,'PolynomB',QD2,S_QD2ini.*(1+x0(5)),1,2);
        mach=setcellstruct(mach,'PolynomB',QF1,S_QF1ini.*(1+x0(6)),1,2);
        
        
        orb=linepass(mach,[x0(1:4) 0 0]',[bpm;numel(mach)+1]);
            if numbpm>320
            orb2=linepass(mach,orb(:,end),bpm(1:min([numbpm 320])));
            orb=[orb(:,1:end-1) orb2];
            orb=orb(1:2:4,1:numbpm)';
            else
            orb=orb(1:2:3,1:numbpm)';
            end

        
        figure(3)
        plot([traj(1:numbpm,:) orb]);
        grid on;
        legend('meas or h','meas or v','fit or h','fit or v');
          figure(4);
        pause(5)
          atplot(mach);
      
      end


%%%%ramp.StopRamping();
%%%%%gun.Off();   
%Ploting result
figure(1)
subplot(3,1,1);
plot(cur);
ylabel('intensity');
grid on;
subplot(3,1,2);
plot(traj(:,1));
ylabel('H trajectory (microm)');
grid on;
subplot(3,1,3);
plot(traj(:,2));
ylabel('V trajectory (microm)');
xlabel('bpm');
grid on;
figure(2)

t2=~isfinite(traj);
t3=traj;
t3(t2)=0;
subplot(2,1,1)

plot(abs(fft(t3(:,1))));
legend('hfft');
grid on;
subplot(2,1,2)
plot(abs(fft(t3(:,2))));
legend('vfft');
grid on;

    
     
     
     
     
      
     
     function [xx,zz,intens]=loadposlib(nbturns)
    vx0h=load_dd('H');
	pause(2)
	vx0v=load_dd('V');
	pause(2)
	intens=load_sumdd();
	vx0=cat(3,vx0h,vx0v);
	
	[xx]=preparevvtom(vx0(1:nbturns,:,1));
    [zz]=preparevvtom(vx0(1:nbturns,:,2));
intens=intens(7:end,:);
     end
    function [oscill]=preparevvtom(vx0)
    oscill=vx0;
    aver=mean2(vx0(:,:));
    %oscill=NaN(size(vx0));
    %[oscill(:,:),aver]=mtclean(vx0(:,:),mean2(vx0(:,:)),[]);
    end

 %%%   function res=fittune(y,bpm,mach,traj,co4)

%%% QD5D=findcells(mach,'FamName','QD5D');
%%% QF6D=findcells(mach,'FamName','QF6D');
 
 %%%cini=[co4';0;0]
  %%%mach=setcellstruct(mach,'PolynomB',QD5D,[y(1)],1,2);
  %%%mach=setcellstruct(mach,'PolynomB',QF6D,[y(2)],1,2);
  %%%orb=linepass(mach,cini,bpm);
  %%%orb=orb(1:2:3,:)';

 
  %%%res=sum(std2(orb-traj*1E-3,1))
%%%end

%
  %%  function res=fittune(x,bpm,mach,plane,or)
  %%cini=[x(1:4)';0;0]
  %%mach=atfittune([x(5) x(6)],'QD5D','QF6D');
  %%orb=linepass(mach,orini,bpm);
  %%orb=orb(1:2,:)';
  %%res=std2(orb-or);
   %%     end

        end
            
