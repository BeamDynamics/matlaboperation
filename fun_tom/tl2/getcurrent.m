function cur=getcurrent(noquad,grad,index)
%GETCURRENT specifies the current to send in a quad in order to reach the
%specified gradient (not integrated).
%NOQUAD is a vector specifying the quad numbers in TL2 for 
%which the current should be calculated.
%GRAD is a vector containing the corresponding focussing force to be
%reached for the quadrupoles designeted by NOQUAD.
%CUR is a vector containing the calculated currents for the quads
%designated by NOQUAD
%INDEX should be 1 if you want to convert gradient into current, and 2 if
%you want to convert current into gradient

fact=16.07/16.038;
bro=20.0138;
for i=1:size(noquad,2)

if noquad(i)==1
mat=[0 6.558 11.710 16.004];
end
if noquad(i)==2
mat=[0 4.85 8.0284 15.710];
mat2=[0 37.5 62.5 125];
end
if noquad(i)==3
mat=[0 6.578 11.729 16.004];
end
if noquad(i)==4
mat=[0 6.575 11.708 16.001];
end
if noquad(i)==5
mat=[0 6.446 11.643 15.815];
end
if noquad(i)==6
mat=[0 6.572 11.727 16.015];
end
if noquad(i)==7
mat=[0 6.561 11.722 16.009];
end
if noquad(i)==8
mat=[0 6.558 11.715 16.023];
end
if noquad(i)==9
mat=[0 6.560 11.716 16.009];
end
if noquad(i)==10
mat=[0 6.573 11.736 16.002];
end
if noquad(i)==11
mat=[0 6.546 11.695 15.964];
end
if noquad(i)==12
mat=[0 6.587 11.746 16.038];
end
if noquad(i)==13
mat=[0 6.576 11.721 16.004];
end
if noquad(i)==14
mat=[0 6.558 11.712 15.987];
end
if noquad(i)~=2
    mat2=[0 50 90 125];
end
if index==1
gradient(i)=grad(i)*6.0/0.2998*0.6;
cur(i)=linearinterpol(gradient(i),mat2,mat);
end
if index==2

%plot(mat,mat2);
cur(i)=linearinterpol(grad(i),mat,mat2);    
cur(i)=cur(i)/6.0*0.2998/0.6;
end

end
end
    function res=linearinterpol(value,X,Y)
size(Y,2);
sign=1;
if value<0
    sign=-1;
    value=abs(value);
end
for i=1:numel(Y)
    if Y(i)<=value
        
        indice=i;
    end
end
ratio=(value-Y(indice))/(Y(indice+1)-Y(indice));
res=sign*(X(indice)+ratio*(X(indice+1)-X(indice)));
end
