function [ datain,tl2,beta,alpha,disp,ddisp,spos,phase,dispv,ddispv] =buildtl2tom(Klist,plotflag)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
if isempty(Klist)    
Klist=[0.5615   -0.3258    0.1513    0.8280   -0.6954    0.3611    0.1085   -0.1859    0.0306   -0.0890    0.6097   -0.6026    0.1314    0.5352];
sq=[0 0 0];
end


    dp1=atrbend('DIP_I', 2/3*2.094, 2/3*0.0951998, 0, 'BendLinearPass');
    dp2=atrbend('DIP_O', 1/3*2.094, 1/3*0.0951998, 0, 'BendLinearPass');
    dp11=atrbend('DIP_I1', 2/3*2.094, 2/3*0.0951998*1.0145, 0, 'BendLinearPass');
    dp12=atrbend('DIP_O2', 1/3*2.094, 1/3*0.0951998*1.0145, 0, 'BendLinearPass');
    %se2=atrbend('SE2', 1.6, -0.080281, -0.04014255, -0.04014255, 0, 'BendLinearPass');
dr01=atdrift('DR01', 0.6350, 'DriftPass');
fs1=atmarker('FS1', 'IdentityPass');
dr02=atdrift('DR02', 0.340, 'DriftPass');
cv0=atcorrector('CV0',0,[0 0], 'CorrectorPass');
dr031=atdrift('DR031', 1.750, 'DriftPass');
bpm1=atmonitor('BPM_CV1');
dr032=atdrift('DR032', 0.450, 'DriftPass');
cv1=atcorrector('CV1',0,[0 0], 'CorrectorPass');
drfs=atdrift('DR_FS', 0.277, 'DriftPass');
drfs2=atdrift('DR_FS2', 0.152, 'DriftPass');
fs2=atmarker('FS2', 'IdentityPass');
fs3=atmarker('FS3', 'IdentityPass');
fs4=atmarker('FS4', 'IdentityPass');
fs5=atmarker('FS5', 'IdentityPass');
fs6=atmarker('FS6', 'IdentityPass');
fs7=atmarker('FS7', 'IdentityPass');
fs9=atmarker('FS8', 'IdentityPass');
fs8=atmarker('FS9', 'IdentityPass');
fs10=atmarker('FS10', 'IdentityPass');
fs11=atmarker('FS11', 'IdentityPass');
vlm1=atmarker('VLM1', 'IdentityPass');
vlm2=atmarker('VLM2', 'IdentityPass');
vlm3=atmarker('VLM3', 'IdentityPass');
vlm4=atmarker('VLM4', 'IdentityPass');
vlm5=atmarker('VLM5', 'IdentityPass');

dr0=atdrift('DR0', 0.2, 'DriftPass');
%%bpm3=atmarker('BPM_3', 'IdentityPass');
%bpm4=atmarker('BPM_4', 'IdentityPass');
%bpm5=atmarker('BPM_5', 'IdentityPass');
%bpm6=atmarker('BPM_6', 'IdentityPass');
%bpm7=atmarker('BPM_7', 'IdentityPass');
%bpm8=atmarker('BPM_8', 'IdentityPass');
ch1=atcorrector('CH1',0,[0 0], 'CorrectorPass');
drk=atdrift('DRK', 0.373, 'DriftPass');
qf1=atquadrupole('QF1', 0.6, Klist(1), 'StrMPoleSymplectic4Pass'); %


dr04=atdrift('DR04', 1.810, 'DriftPass');
ch2=atcorrector('CH2',0,[0 0], 'CorrectorPass');
qd2=atquadrupole('QD2', 0.6, Klist(2), 'StrMPoleSymplectic4Pass'); %
dr05=atdrift('DR05', 1.079, 'DriftPass');

dr061=atdrift('DR061', 2.950, 'DriftPass');
bpm2=atmonitor('BPM_QF3');
dr062=atdrift('DR062', 0.445, 'DriftPass');
qf3=atquadrupole('QF3', 0.6,Klist(3) , 'StrMPoleSymplectic4Pass'); %
cv3=atcorrector('CV3',0,[0 0], 'CorrectorPass');
ch3=atcorrector('CH3',0,[0 0], 'CorrectorPass');

dr072=atdrift('DR072', 1.160, 'DriftPass');
bpm3=atmonitor('BPM_CH3');
dr071=atdrift('DR071', 0.313, 'DriftPass');
dr08=atdrift('DR08', 0.377, 'DriftPass');
qf4=atquadrupole('QF4', 0.6,Klist(4), 'StrMPoleSymplectic4Pass'); %
dr091=atdrift('DR091', 0.517, 'DriftPass');
bpm4=atmonitor('BPM_QD5');
dr092=atdrift('DR092', 0.330, 'DriftPass');
qd5=atquadrupole('QD5', 0.6, Klist(5) , 'StrMPoleSymplectic4Pass'); %
cv4=atcorrector('CV4',0,[0 0], 'CorrectorPass');
ch4=atcorrector('CH4',0,[0 0], 'CorrectorPass');


dr10=atdrift('DR10', 4.6042, 'DriftPass');
bpm5=atmonitor('BPM_QF6');
dr11=atdrift('DR11', 0.5568, 'DriftPass');
qf6=atquadrupole('QF6', 0.6, Klist(6), 'StrMPoleSymplectic4Pass'); %
cv5=atcorrector('CV5',0,[0 0], 'CorrectorPass');
ch5=atcorrector('CH5',0,[0 0], 'CorrectorPass');
dr12=atdrift('DR12', 0.6849, 'DriftPass');

dr131=atdrift('DR131', 1.494, 'DriftPass');
bpm6=atmonitor('BPM_SX');
dr132=atdrift('DR132', 0.320, 'DriftPass');

dr133=atdrift('DR133', 0.248, 'DriftPass');
dr134=atdrift('DR134', 0.37, 'DriftPass');
dr1342=atdrift('DR1342', 0.373, 'DriftPass');
cv6=atcorrector('CV6',0.0,[0 0], 'CorrectorPass');
ch6=atcorrector('CH6',0.0,[0 0], 'CorrectorPass');
qf7=atquadrupole('QF7', 0.6,Klist(7), 'StrMPoleSymplectic4Pass'); %


dr14=atdrift('DR14', 3.876, 'DriftPass');
dr15=atdrift('DR15', 1.308, 'DriftPass');
dr152=atdrift('DR152', 0.230, 'DriftPass');

qd8=atquadrupole('QD8', 0.6, Klist(8), 'StrMPoleSymplectic4Pass'); %
dr16=atdrift('DR16',0.543, 'DriftPass');
dr161=atdrift('DR161',0.554, 'DriftPass');
dr162=atdrift('DR162',0.373, 'DriftPass');
cv7=atcorrector('CV7',0.0,[0 0], 'CorrectorPass');
ch7=atcorrector('CH7',0.0,[0 0], 'CorrectorPass');
qf9=atquadrupole('QF9', 0.6, Klist(9) , 'StrMPoleSymplectic4Pass'); %
dr17=atdrift('DR_17', 1.47, 'DriftPass');
sx=atsextupole('SX', 0.4,0, 'StrMPoleSymplectic4Pass');%
qd10=atquadrupole('QD10', 0.6, Klist(10), 'StrMPoleSymplectic4Pass'); %
dr181=atdrift('DR18', 1.497, 'DriftPass');
bpm7=atmonitor('BPM_QD8');
dr182=atdrift('DR18', 0.273, 'DriftPass');


qf11=atquadrupole('QF11', 0.6, Klist(11) , 'StrMPoleSymplectic4Pass'); %
dr19=atdrift('DR19', 12.3282-11.5362+0.004, 'DriftPass');
dr20=atdrift('DR20', 0.575, 'DriftPass');

dr201=atdrift('DR201', 0.12, 'DriftPass');


bpm8=atmonitor('BPM_QD12');
dr202=atdrift('DR202', 0.196, 'DriftPass');
qd12=atquadrupole('QD12', 0.6,  Klist(12), 'StrMPoleSymplectic4Pass'); %
cv8=atcorrector('CV8',0.0,[0 0], 'CorrectorPass');
ch8=atcorrector('CH8',0.0,[0 0], 'CorrectorPass');
dr21=atdrift('DR21', 0.476, 'DriftPass');
dr212=atdrift('DR212', 0.360, 'DriftPass');
dr213=atdrift('DR213', 0.400, 'DriftPass');
cv9=atcorrector('CV9',0.0,[0 0], 'CorrectorPass');
qf13=atquadrupole('QF13', 0.6,Klist(13), 'StrMPoleSymplectic4Pass'); %
dr221=atdrift('DR221', 0.32, 'DriftPass');
bpm9=atmonitor('BPM_QF14');
dr222=atdrift('DR222', 0.362, 'DriftPass');

qf14=atquadrupole('QD14', 0.6,Klist(14) , 'StrMPoleSymplectic4Pass'); %
dr23=atdrift('DR23', 0.345, 'DriftPass');
%dr24=drift('DR24', 0.935-0.45, 'DriftPass');
dr242=atdrift('DR242', 0.243, 'DriftPass');
bpm10=atmonitor('BPM_CV9');
dr25=atdrift('DR25', 0.545, 'DriftPass');
ts12=0.0426733;
s12=atrbend('S12', 22.9183*ts12, ts12, 0, 'BendLinearPass');
dr26=atdrift('DR26', -(2.3781-2.4326), 'DriftPass');
dr27=atdrift('DR27', 0.9, 'DriftPass');
ts3=0.0218166;
s3=atrbend('S3', 22.9183*ts3, ts3, 0, 'BendLinearPass');
dr28=atdrift('DR28', -01.725, 'DriftPass');
s22=atrbend('S22', 22.9183*ts12, ts12, 0, 'BendLinearPass');
QF1L=atsbend('QF1L',0.431,-0.0071,0,'BndMPoleSymplectic4Pass','PolynomB',[0 -0.2 0 0 0],'NumIntSteps',20,'MaxOrder',4);
dr26_1=atdrift('DR26_1', 2.9181-2.8636, 'DriftPass');

tl2={dr01 fs1 dr02 cv0 dr031 bpm1 dr032 cv1 drfs fs2 drfs ch1 drk,...
	qf1  dr04 drfs fs3 drfs ch2 drk qd2 dr05 dp11  vlm1 dp12  dr061 bpm2 dr062  qf3 drk,...
	cv3 drfs fs4 drfs ch3 dr071 bpm3 dr072 dp1  vlm2 dp2 dr08 qf4 dr091 bpm4 dr092 qd5 drk cv4,...
	 drfs  fs5 drfs ch4  dr10 bpm5 dr11 qf6  drk cv5 drfs fs6 drfs,...
	ch5 dr12 dp1  vlm3 dp2 dr131 bpm6 dr132   sx dr1342 cv6 drfs fs7 drfs  ch6  dr134 qf7 dr14 dp1  vlm4 dp2,...
	dr15 bpm7 dr152 qd8 dr16  cv7 dr161 ch7 dr162 qf9 dr17 qd10,...
	dr181 dr182 qf11 dr19 dp1  vlm5 dp2 dr20 bpm8 dr201 ,...
	qd12 dr21  qf13 dr213 cv8 dr212 ch8 dr221 bpm9 dr222 qf14 dr23 cv9  dr242 bpm10 dr25 s12 dr26_1 QF1L dr26 s22,...
	dr27 s3 fs11}';
in=atindex(tl2);
tl2{in.QF1L}.PolynomB=[0 -0.2];



%buildlat(tl2);
%evalin('caller','global THERING');


datain.ClosedOrbit=zeros(4,1);
datain.beta=[4 12.5];
datain.alpha=[-0.6 2.2];
datain.mu=[0 0];
%fited from last measurement: datain.dispersion=[ -00.2322; -0.0598];
datain.dispersion=[ -00.2322; -0.0598];
if nargin>1
[ beta,alpha,disp,ddisp,spos,phase,dispv,ddispv] = plottwissline(tl2,datain.beta,datain.alpha,datain.dispersion,1);
end
end






