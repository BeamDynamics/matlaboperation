function [ Iquad,klist] = load_tl2quad(klist,flag)
%[ Iquad klist] = load_tl2quad(klist,flag)
% Translate K values of KLIST into currents and loads it into 
%the TL2 quads if flag==1.
%If flag==2 the function reads currents set points on the magmets, and
%converts it into a list of K values returned in the variable klist.
%
%

if flag==1    
cur=getcurrent([1:14],klist,1);
if 1==0
% if(sum(sign(cur.*[1 -1 1 1 -1 1 1 -1 1 -1 1 -1 1 1])~=[1 1 1 1 1 1 1 1 1 1 1 1 1 1]))~=0
% fprintf ('quads do not have the right polarity')
 else
cur=abs(cur);
tango.Attribute('TL2/ps/qf1/Current').write(cur(1));
tango.Attribute('TL2/ps/qd2/Current').write(cur(2));
tango.Attribute('TL2/ps/qf6/Current').write(cur(6));
tango.Attribute('TL2/ps/qf7/Current').write(cur(7));
tango.Attribute('TL2/ps/qd8/Current').write(cur(8));
tango.Attribute('TL2/ps/qf9/Current').write(cur(9));
tango.Attribute('TL2/ps/qd10/Current').write(cur(10));
tango.Attribute('TL2/ps/qf11/Current').write(cur(11));
tango.Attribute('TL2/ps/qf14/Current').write(cur(14));
tango.Attribute('TL2/ps/qd12/Current').write(cur(12));
tango.Attribute('TL2/ps/qd5/Current').write(cur(5));
tango.Attribute('TL2/ps/qf13/Current').write(cur(13));
tango.Attribute('TL2/ps/qf4/Current').write(cur(4));
tango.Attribute('TL2/ps/qf3/Current').write(cur(3));
Iquad=cur;



end
end

if flag==2
cur(1)=tango.Attribute('TL2/ps/qf1/Current').set;
cur(2)=tango.Attribute('TL2/ps/qd2/Current').set;
cur(3)=tango.Attribute('TL2/ps/qf3/Current').set;
cur(4)=tango.Attribute('TL2/ps/qf4/Current').set;
cur(5)=tango.Attribute('TL2/ps/qd5/Current').set;
cur(6)=tango.Attribute('TL2/ps/qf6/Current').set;
cur(7)=tango.Attribute('TL2/ps/qf7/Current').set;
cur(8)=tango.Attribute('TL2/ps/qd8/Current').set;
cur(9)=tango.Attribute('TL2/ps/qf9/Current').set;
cur(10)=tango.Attribute('TL2/ps/qd10/Current').set;
cur(11)=tango.Attribute('TL2/ps/qf11/Current').set;
cur(12)=tango.Attribute('TL2/ps/qd12/Current').set;
cur(13)=tango.Attribute('TL2/ps/qf13/Current').set;
cur(14)=tango.Attribute('TL2/ps/qf14/Current').set;

Iquad=cur;
klist=[1 -1 1 1 -1 1 1 -1 1 -1 1 -1 1 1].*getcurrent([1:14],cur,2);
end