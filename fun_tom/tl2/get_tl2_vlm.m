function [im,bs] = get_tl2_vlm(flag_fit,average)
%Reads average times the images of the TL2 VLM's and stores them in the
%variable IM. If FLAG_FIT is set to 1 also performs the fit of the five images in term of beam size.
%pixel coeficient is set to 26,5microm/m
vlm1=tango.Device('tl2/d-ccd/dipole1');
vlm2=tango.Device('tl2/d-ccd/dipole2');
vlm3=tango.Device('tl2/d-ccd/dipole3');
vlm4=tango.Device('tl2/d-ccd/dipole4');
vlm5=tango.Device('tl2/d-ccd/dipole5');
for i=1:average
im(i).d1=vlm1.Image.read;
im(i).d2=vlm2.Image.read;
im(i).d3=vlm3.Image.read;
im(i).d4=vlm4.Image.read;
im(i).d5=vlm5.Image.read;
pause(2)
end


figure(1)
subplot(2,3,1)
image(im(1).d1);
title('dip1');

subplot(2,3,2)
image(im(1).d2);
title('dip2');

subplot(2,3,3)
image(im(1).d3);
title('dip3');


subplot(2,3,4)
image(im(1).d4);
title('dip4');


subplot(2,3,5)
image(im(1).d5);
title('dip5');
whos
if flag_fit==1
    figure(2)
    [posh,bs(1,1),fwhmh,posv,bs(2,2),fwhmv]=beamposandprof(0,im(1).d1,3,26E-6,0);
[posh,bs(2,1),fwhmh,posv,bs(2,2),fwhmv]=beamposandprof(0,im(1).d2,3,26,5E-6,0);
[posh,bs(3,1),fwhmh,posv,bs(3,2),fwhmv]=beamposandprof(0,im(1).d3,3,26,5E-6,0);
[posh,bs(4,1),fwhmh,posv,bs(4,2),fwhmv]=beamposandprof(0,im(1).d4,3,26,5E-6,0);
[posh,bs(5,1),fwhmh,posv,bs(5,2),fwhmv]=beamposandprof(0,im(1).d5,3,26,5E-6,0);
end
end