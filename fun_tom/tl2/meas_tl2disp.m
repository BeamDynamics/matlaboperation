function [dh,dv,orbithm,orbitvm,summ,orbith,orbitv,sum] = meas_tl2disp(frfspan,average)
%[dh,dv,orbithm,orbitvm,summ,orbith,orbitv,sum] = meas_tl2disp(frfspan,average)
% Measures dispersion in tl2 by changing the rf frequency of the booster ON
% MASTER SOURCE 2.
%FSPAN is the frequency span to be used for the RF frequency.
%AVERAGE is the number of averaged orbits to determine 1 orbit.
%DH/DV is the resulting H/V dispersion measured on the BPM's (1rst one is
%irelevant)
%ORBITHM/VM are the orbits after averaging (nbbpms*nb dfrf)
%SUMM is the record of the summ signal of bpm's in tl2
%ORBITH/V is the raw data of orbit measurements (nbbpms*averaging*dfrf);
alpha=9.1E-3;
tl2o=tango.Device('tl2/d-bpm/all');
syo=tango.Device('sy/d-bpm/all');
ke=tango.Device('sy/ps-ke/1');
keshot=tango.Attribute('sy/ps-ke/1/CounterMode');
keshot.set=int16(8);
rf=tango.Device('sy/ms/2');
span=frfspan;
syclock=tango.Device('sy/d-bpm/aclock');
clockini=syclock.ClockDelay;
syclock.ClockDelay=135;


sym=sy_meas();
sym.mkshots(average*20)
numelfrequ=6;
fini=rf.Frequency.read
dfrf=round([-span/2:span/numelfrequ:span/2])
dpp=dfrf./fini./alpha;
f=dfrf+fini;
for i=1:5
   rf.Frequency=fini-i*span/10; 
    pause(2)
end
sym.switch_on(average*4*numel(dfrf)*5);

for j=1:numel(f);
rf.Frequency=f(j)
   pause(1)
    for i=1:average+1
            %sym.switch_on(average*4)
            ke.cmd('On');
            pause(1)
            orbith(:,i,j)=tl2o.X_Position.read;
            orbitv(:,i,j)=tl2o.Z_Position.read;
            syorbith(:,i,j)=syo.XPositionMinusRef.read;
            syorbitv(:,i,j)=syo.ZPositionMinusRef.read;
            sum(:,i,j)=tl2o.Charge.read;
            
            pause(2)
           % sym.switch_off;
           frequency=f(j) 
  % im(i,j)=get_tl2_vlm(0) 
    end
  
    
end
fprintf('end of measurement')
sym.switch_off;
orbithm2=mean2(orbith(:,2:end,:),2);
orbitvm2=mean2(orbitv(:,2:end,:),2);
syorbith=mean2(syorbith(:,2:end,:),2);
syorbitv=mean2(syorbitv(:,2:end,:),2);

sum2=mean2(sum,2);


orbithm(:,:)=orbithm2(:,1,:);
summ(:,:)=sum2(:,1,:);
orbitvm(:,:)=orbitvm2(:,1,:);
for i=1:size(orbithm,1)
fh=polyfit(dpp(2:end),orbithm(i,2:end),1);
fv=polyfit(dpp(2:end),orbitvm(i,2:end),1);
dh(i)=fh(1);dv(i)=fv(1);
end
plot([dh' dv'])
xlabel('bpm num')
ylabel('dispersion(m)')
title('tl2 dispersion')
legend('h','v')
grid on

for i=1:5
   rf.Frequency=fini+span/2-i*span/10; 
    pause(3)
end
save data_disptl2_Nov2021_4average f orbitvm orbithm orbith orbitv summ sum dh dv syorbith syorbitv
%syclock.ClockDelay=clockini;
%plot(dfrf/fini/alpha,orbitvm-')
%xlabel(Dpp);
%ylabel(orbit);