classdef sy_meas
    %export TANGO_HOST=ebs-simu:10000
    
    %   Detailed explanation goes here
    
    properties
        SI
        GUN
        RIPS
        KI
        LINAC
        CLOCK
        TL1_CT
    end
    methods
        function obj = sy_meas()
            
            obj.LINAC=tango.Device('elin/master/op')
            obj.GUN=tango.Device('elin/beam/run');
            obj.RIPS=tango.Device('sy/ps-rips/manager');
            obj.KI=tango.Device('sy/ps-ki/1');
            obj.CLOCK=tango.Device('sy/d-bpm/aclock');
            obj.SI=tango.Device('sy/ps-si/1');
            ready=check_state(obj);
            obj.TL1_CT=tango.Attribute('tl1/d-ct/ict-1/TotalCharge');
            %ready1=[(tango.DevState.On==obj.KI.State);(tango.DevState.Off==obj.GUN.State);(obj.RIPS.State== tango.DevState.On)];
            % ready2=[ready1;(tango.DevState.On==tango.Device('sy/ps-si/1').State)];
        end
        
        
        function rd=check_state(obj)
            %checks the injector state, if ready to inject. error code printed
            %as "check". The bites to 0 means equipement is not in the
            %state as mensioned in the message. order of the bites corresponds to the order of the listed items
            %in the message.
            %
            
            ready1=[(tango.DevState.Standby==obj.KI.State);(tango.DevState.Off==obj.GUN.State);(obj.RIPS.State== tango.DevState.On)];
            ready2=[ready1;(tango.DevState.On==tango.Device('sy/ps-si/1').State)];
            
            if sum(ready2)~=4
                check=ready2
                ['Injector not ready, make sure ki is in standby with a nuber of shots between 1 and 1000,linac on and gun off, rips is on and not ramping, , SI is on, ']
                rd=logical(0);
                
                
                
            else
                rd=logical(1);
                ['Injector ready']
            end
        end
        
        
        
        function res_on=switch_on(obj,nbshots)
            %res_on=switch_on(obj,nbshots)
            %Sends some beam in the booster for NBSHOT number of cycles
            %
            %
            
            
            rd=obj.check_state;
            nb=int16(nbshots+2);
            
            if rd
                
                
                obj.KI.CounterMode=nb;
                
                try
                    obj.RIPS.cmd('StartRamping');
                catch be
                    pause(1)
                    fprintf('rips did not start first time');
                    obj.RIPS.cmd('StartRamping');
                end
                pause(1)
                obj.GUN.cmd('On');
                pause(1)
                
                obj.KI.cmd('On');
                pause(1)
                number_of_shots_made=nbshots
                res_on=logical(1);
            else
                res_on=logical(0);
                
            end
            
            
        end
        
        
        function res_off=switch_off(obj)
            % res_off=switch_off(obj)
            %switches the beam off in the booster andsets the injector to
            %warm mode (gun off, KI standby RIPS not ramping)
            obj.GUN.cmd('Off');
            obj.RIPS.cmd('StopRamping');
            obj.KI.cmd('Standby');
            obj.KI.CounterMode=int16(-1);
            pause(5)
        end
        
        
        function me=measure(obj,at_name,nbpulse)
            %measures the device value AT_NAME durring beam send in the
            %booster for NBPULSE cycles.
            %predefined measurements:
            %
            %   - me=measure(obj,'position-tbt',clock_delay)
            %    measures tbt data for 5ms starting from To+CLOCK_DELAY
            %   H pos measured on bpm QF5, v position on QD22;
            %   Stores the result in variable me with three colums:
            %C1=H data
            %C2=V data
            %C3=summ data
            
            
            
            %   - me=measure(obj,'position',Tmeas)
            %   measures closed orbit at time Tmeas
            %   ME is a four column vector with:
            %   C1: ref H orbit
            %   C2: measured H orbit
            %   C3: difference meas-ref H plane
            %   C4: Measured V orbit
            
            %   - me=measure(obj,'current_cycle',average)
            %   measures and plots the current vs time for one acceleration cycle.
            %   The graph title is the equivalent current measured in TL1. The
            %   ratio Itl1/I1rst turns is printed and named injeff_1turn, and
            %   the ratio Itl1/Icaptured is printed out and named injeff>
            %
            %   -me=measure(obj,'tune')
            %   measures tunes in both planes at each ms durring the cycle.Plots the result
            %   as tune vs time in cycle.
            %   me(:,1) is for horizontal tune, me(:,2) is for vertical
            
            clf;
            if strcmp('position_tbt',at_name)
                if nbpulse==0
                    nbpulse=0.00001;
                end
                
                delayini=obj.CLOCK.ClockDelay.set;
                obj.CLOCK.ClockDelay=nbpulse;
                ['tbt_measurement'];
                syall=tango.Device('sy/d-bpm/all');
                syall.cmd('ResetFillCounter');
                sig_p_ini=syall.SignalProcessing.set;
                decim_ini=syall.Decimation_Enable.set;
                qsum_ini=syall.TDP_QSUM_Enable.set;
                aquis_dur_ini=syall.AcquisitionDuration.set;
                
                syall.SignalProcessing=int16(0);
                syall.Decimation_Enable=false;
                syall.TDP_QSUM_Enable=true;
                syall.AcquisitionDuration=0.8;
                
                obj.switch_on(150)
                
                pause(6)
                at_name=tango.Attribute('sy/d-bpm/qf5/TDP_ZPosition');
                
                
                %at_name=tango.Attribute('sy/d-bpm/all/XPosition');
                %reforbit=tango.Attribute('sy/beam-orbitcor/svd-steer-h/RefOrbit');
                
                at_name
                
                me(:,2)=at_name.read;
                at_name=tango.Attribute('sy/d-bpm/qd22/TDP_XPosition');
                at_name
                me(:,1)=at_name.read;
                me(:,3)=mean(syall.All_TDP_QSUM_v2.read,1);
                pause(2)
                subplot(2,1,1)
                plot(me(:,1:2))
                grid on
                legend('tbt po12sition h','tbt_position_v');
                subplot(2,1,2)
                plot(me(:,3))
                legend('tbt current signal');
                obj.CLOCK.ClockDelay=delayini;
                syall.SignalProcessing=sig_p_ini;
                syall.Decimation_Enable=decim_ini;
                syall.TDP_QSUM_Enable=qsum_ini;
                syall.AcquisitionDuration=aquis_dur_ini;
               obj.switch_off()
                
            end
            
            if strcmp('position',at_name)
                nbpulse=120;
                aver=10
                if nbpulse==0 nbpulse=2; end
                syall=tango.Device('sy/d-bpm/all');
                sig_p_ini=syall.SignalProcessing.set;
                decim_ini=syall.Decimation_Enable.set;
                qsum_ini=syall.TDP_QSUM_Enable.set;
                aquis_dur_ini=syall.AcquisitionDuration.set;
                delayini=obj.CLOCK.ClockDelay.set
               % obj.CLOCK.ClockDelay=nbpulse;
                
                
                syall.SignalProcessing=int16(1);
                syall.Decimation_Enable=true;
                syall.TDP_QSUM_Enable=false;
                
                
                
                
                obj.switch_on(10*aver);
                pause(2)
                
                %at_name=tango.Attribute('sy/d-bpm/all/XPosition');
                reforbit=tango.Attribute('sy/beam-orbitcor/svd-steer-h/RefOrbit');
                
                me.ref=reforbit.read;
                for i=1:aver
                at_name=tango.Attribute('sy/d-bpm/all/XPosition');
                pause(0.3)
                me.h(:,i)=at_name.read;
                me.hmref(:,i)=at_name.read-reforbit.read;
                
                at_name=tango.Attribute('sy/d-bpm/all/ZPosition');
               pause(0.3)
                me.v(:,i)=at_name.read;
                pause(1)
               
                end
                rf=tango.Device('sy/ms/2');
                me.rf=rf.Frequency;
                
                %std=std2(me,1)
                pause(4)
                plot(me.h)
                grid on
                xlabel('bpm number');
                ylabel('orbit(m)');
                legend('ref_h orbit', 'measured h orbit', 'difference href-hmeas','vorbit');
                
                obj.CLOCK.ClockDelay=delayini;
                syall.SignalProcessing=sig_p_ini;
                syall.Decimation_Enable=decim_ini;
                syall.TDP_QSUM_Enable=qsum_ini;
                syall.AcquisitionDuration=aquis_dur_ini;
           obj.switch_off
            end
            
            
            if strcmp('tune',at_name)
                
                tune=tango.Attribute('sy/d-tm/profile/Tunes');
                aquis=tango.Device('sy/d-tm/profile');
                aquis.cmd('StartAcquisition');
                aquis.MeasurementPlane=logical(0);
                
                
                obj.switch_on(65);
                aquis.Average=int32(1);
                pause(6)
                me(:,1)=tune.read;
                aquis.MeasurementPlane=logical(1);
                aquis.Average=int32(1);
                pause(6)
                me(:,2)=tune.read;
                aquis.cmd('Stop');
                me(me>0.95)=nan;
                plot(me)
                grid on
                xlabel('time (0=tinj) ms');
                ylabel ('tunes')
                legend('h tune','v tune,');
            end
            
            
            if strcmp('current',at_name)
                at_name=tango.Attribute('sy/d-ct/1/Current');
                obj.switch_on(20);
                pause(3)
                for i=1:4
                    
                    me(:)=at_name.read
                    pause(1.2)
                end
                
            end
            
            if strcmp('current_cycle',at_name)
                at_name=tango.Attribute('sy/d-ct/1/Buffer');
                obj.switch_on(6*nbpulse+4);
                for i=1:nbpulse
                    pause(1)
                    
                    me(:,i)=at_name.read;
                    itl(i)=obj.TL1_CT.read;
                    
                    pause(0.5)
                end
                me=mean(me,2);
                itl1=mean(itl)
                plot(me)
                injeff_1turn=me(90)/itl1*1E-9
                injeff=mean(me(250:350))/itl1*1E-9
                grid on;
                xlabel('Time');
                ylabel('current (mA)');
                title(['TL1 current=' num2str(itl1)]);
            end
         obj.switch_off()   
        end
        
        function extr_off(obj,flagse1)
            b1=tango.Device('sy/ps-b/1');
            b2=tango.Device('sy/ps-b/2');
            b3=tango.Device('sy/ps-b/3');
            se1=tango.Device('sy/ps-se/1');
            se2=tango.Device('sy/ps-se/2-1');
        
            if b1.State=='On';
            b1.cmd('Standby')
            end
            
           if b2.State=='On';
            b2.cmd('Standby')
            end 
            if b3.State=='On';
            b3.cmd('Standby')
            end
            
            if se2.State=='On';
                if flagse1==1
                    se2.cmd('Standby')
                end
            end
            
            if se1.State=='On';
            
                if flagse1==1
                    se1.cmd('Standby')
                end
                
                
            end
            
        end
        
            
            function res=tl2(obj,se1flag)
            tl2o=tango.Device('tl2/d-bpm/all');
            ext=tango.Device('sy/ext/1');
           ext.cmd('On');
           pause(7);
            ke=tango.Device('sy/ps-ke/1');
             cm=tango.Attribute('sy/ps-ke/1/CounterMode');
             cm.set=int16(7);
            obj.switch_on(20);
             pause(0.5);
             ke.cmd('On');
             pause(7)
         
            obj.switch_off;
            obj.extr_off(se1flag);
            orbith(:)=tl2o.X_Position.read;
            orbitv(:)=tl2o.Z_Position.read;
           sum(:)=tl2o.Charge.read;
            res.oh=orbith;
            res.ov=orbitv;
            res.q=sum;
            end
        
        
        function res=mkshots(obj,nbshots);
            %res=mkshots(obj,nbshots);
            %Sends some beam in the booster for NBSHOTS number of shots
            switch_on(obj,nbshots);
            pause(nbshots/4+0.5);
            switch_off(obj);
            rd=obj.check_state;
            res=rd;
        end
        
        
        function res=inj_sr(obj,injcur);
            
            %Injects in sr the injcur amount of current INJCUR is the increment in
            %current expressed in mA
            
            %prepare booster injection
            obj.SI.cmd('Standby');
            pause(3);
            obj.SI.cmd('On');
            obj.KI.cmd('Standby');
            obj.LINAC.cmd('On');
            pause(5)
            obj.GUN.cmd('Off');
            %%checks current in the booster
            cur_sy=obj.measure('current')
            cur_sy=mean(cur_sy);
            if cur_sy<0.1
                ['no current in the booster check which ellement is in fault (ki,si,rips,rf,linac,tl1)']
                obj.SI.cmd('Standby');
                return
            end
            
            nbshots=floor(injcur/(cur_sy*352/992))*1.2
            
            %switch on extractionseptums
            syext_se1=tango.Device('sy/ps-se/1');
            syext_se2=tango.Device('sy/ps-se/2-1');
            ext=tango.Device('sy/ext/1');
            ext.cmd('On');
            
            
            
            %prepares ke for a given number of shots
            ke=tango.Device('sy/ps-ke/1');
            keshot=tango.Attribute('sy/ps-ke/1/CounterMode');
            keshot.set=int16(nbshots+25);
            pause(1)
            %checks septums state
            srinj_s2=tango.Device('sr/ps-si/2');
            srinj_s3=tango.Device('sr/ps-si/3');
            flag_septums_sr=strcmp(srinj_s2.State,'On') & strcmp(srinj_s3.State,'On');
            flag_septums_sy=strcmp(syext_se1.State,'On') & strcmp(syext_se2.State,'On');
            if~flag_septums_sy
                %extraction septums should already be on.
                ['extraction septums did not start properly'];
                
            end
            
            %Switch on injection septums if needed,
            if ~flag_septums_sr | ~flag_septums_sy
                srinj_s3.cmd('On');
                srinj_s2.cmd('On');
                
                fprintf('tempo because septums in standby \r');
                pause(10)
                flag_septums_sr=(strcmp(srinj_s2.State,'On') & strcmp(srinj_s3.State,'On'))
            end
            
            flag_septums_sy= strcmp(syext_se1.State,'On') & strcmp(syext_se2.State,'On')
            %%if all septums are on inject the given number of pulses
            if flag_septums_sr & flag_septums_sy
                pause(2)
                ['injecting']
                tic
                ke.cmd('On');
                toc
                pause(2)
                obj.switch_on(nbshots+5);
                pause(2)
                %ripst=obj.RIPS.State
                %sisy=tango.Device('sy/ps-si/1').State
                % kit=obj.KI.State
                
                
                
                pause((nbshots+5)/4);
                
            end
            
            
            %%Switch off all equipements
            obj.switch_off;
            pause(2)
            ext=tango.Device('sy/ext/1');
            ext.cmd('Off');
            srinj_s3.cmd('Standby');
            srinj_s2.cmd('Standby');
            pause(2)
            obj.SI.cmd('Standby');
            
            obj.KI.cmd('Standby');
            obj.LINAC.cmd('Standby');
            pause(3)
            
        end
        
        function res=top_up(Imax,Imin)
            ct=tango.Attribute('srdiag/beam-current/total/Current');
            
            r=0;
            while r==0
                cur=1000;
                while cur>Imin
                    cur=ct.read
                    pause(30)
                end
                obj.inj_sy(Imax-cur);
            end
        end
    end
end


