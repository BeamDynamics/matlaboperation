classdef search_obs
    %export TANGO_HOST=ebs-simu:10000

    %   Detailed explanation goes here
    
    properties
        CUR
        BLM
        ORBITH
        ORBITV
        LT
        RING
        MODEL
        HRESP
        VRESP
        HSTRENGTH
        VSTRENGTH
        INITSTRENGTH
        s_ok
    
    HSTNAME
    VSTNAME
    end
    
    methods
        function obj = search_obs()
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            f=pwd;
            cd /machfs/perron/matlab/programs;
            addpathtom;
           cd(f); 
          
        curdir=pwd;
        cd /machfs/perron/matlab/programs;
        addpathtom;
        cd(curdir);
        obj.CUR=tango.Attribute('srdiag/beam-current/total/current');

       %obj.BLM=1+mod([3:0.25:34.75],32);

        obj.BLM=tango.Device('srdiag/blm/all');    
        obj.ORBITH=tango.Attribute('srdiag/bpm/all/All_SA_HPosition');    
        obj.ORBITV=tango.Attribute('srdiag/bpm/all/All_SA_VPosition');
        %obj.LT=tango.Attribute('srdiag/bpm/all/HOrbitPeak');    
        obj.RING='sr';
        dirname=uigetdir('/operation/beamdyn/matlab/optics/sr/','optic to be loaded');
        addpath_recurse(dirname)
        obj.MODEL=sr.model(dirname);
         obj.MODEL.directory=dirname;  
         obj.HRESP=load(fullfile(dirname,'h_resp.csv'));   
        obj.VRESP=load(fullfile(dirname,'v_resp.csv'));
            
        obj.s_ok=true(12*32,1);
        if size(obj.HRESP,1)>288
          st=readtable(fullfile(dirname,'h_steerers.csv')); 
           isdq=strfind(table2array(st(:,1)),'dq');
                for i=1:numel(isdq);
               obj.s_ok(i)=isempty(isdq{i});
                end
        end
        %in=obj.s_ok;
       
        obj.HRESP=obj.HRESP(:,1:end-1);
            
        obj.HSTRENGTH=tango.Attribute('srmag/hst/all/Strengths');
        obj.VSTRENGTH=tango.Attribute('srmag/vst/all/Strengths');
        obj.INITSTRENGTH.h=obj.HSTRENGTH.set;
        obj.INITSTRENGTH.v=obj.VSTRENGTH.set;
        obj.HSTNAME=tango.Attribute('srmag/hst/all/CorrectorNames');
        obj.VSTNAME=tango.Attribute('srmag/vst/all/CorrectorNames');
        
        
        end
        
        function reset_steer(obj)
        % reset_steer()
        %resets steerers to the value they had when creating the instance
        % of the object.
                  
        obj.HSTRENGTH.set=obj.INITSTRENGTH.h;
        obj.VSTRENGTH.set=obj.INITSTRENGTH.v;
        
        end
        
        
        function [or_fit,err_exec]=mkbump(obj,bump_name,amplitude,numvect)
            %[or_fit,err_exec]=mkbump(bump_name,amplitude,numvect)
            %Makes a closed bump, using 30 correctors, on bpm corresponding to BUMP_NAME (formated like:'c02-h09') with the
            %amplitude given by BUMP_AMPLITUDE (m)
            %NUMVECT eigenvectors used to compute the bump, should be lower than 30. (default 10)
            %
            %outputs:
            %OR_FIT: fited orbit as from matrix response
            %ERR_EXEC, steerer strength increment in radiants used to perform the bump.
           
            
            if nargin<4 %input variable checks
            numvect=10
            end
           
            if numvect>28
             fprintf('error: numvect should be lower than29\n');
              return
            end
            
           
           
           % loads response matrix, corrector indices and strengths depending on the needed plane.
           if strfind(bump_name,'h')
               resp=obj.HRESP;
               icors=obj.MODEL.get(0,'steerh');
           st_strength=obj.HSTRENGTH;
           stname=obj.HSTNAME;
           else
               resp=obj.VRESP;
               icors=obj.MODEL.get(0,'steerv');
           st_strength=obj.VSTRENGTH;
           stname=obj.VSTNAME;
           end
            
           %converts bump name to BPM name 
           bpm_name=bump_name([1 2 3 4 6 7]);
           
           
            
            
            % finds in the lattice structure lists of bpm and corrector 
            %to be considered for the given bump. 
            ibpms=obj.MODEL.get(0,'BPM');
            ibpm=findcells(obj.MODEL.ring,'Device',['srdiag/beam-position/' bpm_name]);
            numbpm=find(ibpm<=ibpms,1)
            numcor=find(ibpm<icors,1);
               % if numcor>numel(icors); numcor=1; end
            numcor=[numcor-20:numcor+20];
            orb=zeros(numel(ibpms),1);
            orb(numbpm)=amplitude;
            bpmok=true(1,320);
            bpmok([numbpm-1 numbpm+1])=false;
            %%%bpmok([numbpm-3 numbpm-2 numbpm-1 numbpm+3 numbpm+2 numbpm+1])=false;
            
            orb(~bpmok)=nan;
            numcor=mod(numcor-1,288)+1;
            bpmok=mod(bpmok-1,320)+1;
            
            %calculates the steerer strengths to be applied
            [or_fit,err_exec]=calc_correction(numvect,resp(:,numcor),orb,0);
             %checks if the steerer strength is resonable and if yes aplies it.    
            
            stit=zeros(288,1);
            stit(numcor)=err_exec;    
                if numel(st_strength.set)==288
                stfin=st_strength.set+stit';
               
                else
                
                stfin(obj.s_ok)=stit'+st_strength.set(obj.s_ok);
                stfin(~obj.s_ok)=st_strength.set(~obj.s_ok);
                stfin(obj.s_ok);
                end
                
                if sum(stfin>4.42E-4)>0
                %fprintf('too strong steerers');
                steernok=find(stfin>6E-4);
                    for i=1:numel(steernok)
                        error=['Strength send to corrector ' stname.read{steernok(i)} ' out of range (' num2str(1000*stfin(steernok(i))) 'mrad)']
                       
                    end
                else
                    st_strength.set=stfin;
                end
             plot(stfin-st_strength.set);
             
             
        end
                
                
         function plot_or_cells(obj,cini,cend,plane)
             %plot_or_cells(cini,cend,plane)
             %Plots the orbit and the lattice structure between cell CINI
            %and CEND. PLANE=1 refers to horizontal, PLANE=2 refers to
            %vertical.
             
             mach=obj.MODEL.ring;
             
             mark=[findcells(mach,'FamName','ID\w*') sort([findcells(mach,'FamName','SDH\w*') findcells(mach,'FamName','SDL\w*')])]
             if numel(mark)==64
             mark=mark(2:2:end);
             end
             res=mach{mark(5)}
             if plane==1
                 orb=obj.ORBITH.read;
             elseif plane==2
                 orb=obj.ORBITV.read;
             end
             
             mach=circshift(mach,-mark(mod(cini-4,32)+1));
            orb=circshift(reshape(orb,[],32)',-mod(cini-3,32)+1)';
            orb=orb(:,[1:mod(cend-cini,32)]);
            
            orb=orb(:); 
            mark_shift=[findcells(mach,'FamName','ID\w*') sort([findcells(mach,'FamName','SDH\w*') findcells(mach,'FamName','SDL\w*')])]
            %mark_shift=sort([findcells(mach,'FamName','ID\w*')+2 findcells(mach,'FamName','DL\w*A\w*_2') findcells(mach,'FamName','DL\w*E\w*_2')...
             %   findcells(mach,'FamName','JL\w*_2') findcells(mach,'FamName','DL\w*D\w*_2')]);
            if numel(mark_shift)==64
             mark_shift=mark_shift(2:2:end);
            end
            
            cells=mach(1:mark_shift(mod(cend-cini,32)));
            bpm=[findcells(cells,'Class','Monitor') findcells(cells,'FamName','BPM')];
           
            figure(1);
            clf(1);
            
            
            atbaseplot(cells);
            
            if(numel(orb)==numel(bpm))
                spos=findspos(cells,bpm);
                hold on
                yyaxis right;
                %ylim([-2 2]);
                plot(spos,orb(:)*1000);
                legend(['orbit(mm) in cells: ' num2str([cini cend])])
                hold off;
            
            else
            error='orbit length different from BPM numbers'
            end
       end
         
       function plot_blm_cells(obj,cini,cend,nbturn,flag_tbt)
             %plot_blm_cells(cini,cend)
             
             
             mach=obj.MODEL.ring;
             
             mark=[findcells(mach,'FamName','ID\w*') sort([findcells(mach,'FamName','SDH\w*') findcells(mach,'FamName','SDL\w*')])];
             if numel(mark)==64
             mark=mark(2:2:end);
             end
             %res=mach{mark(5)}
             
             %blm=[obj.BLM.SA_Incoherencies()];
             if nargin>5
             blm_dev=tango.Attribute('srdiag/blm/tbt/TBT_data');
             else
             blm_dev=tango.Attribute('srdiag/blm/all/Losses'); 
             nbturn=1;
             end
             blm=blm_dev.read';
             
             if nargin>5
                 blm=blm';
             end
             
             blm=(blm(:,nbturn));
             
             blm=circshift(blm,-13,1);
             % blm=obj.BLM.All_Status.read;
             %blm=1+mod([3:0.25:34.75],32);
             %%if plane==2
               %%  orb=obj.ORBITV.read;
             %%elseif plane==1
             %%    orb=obj.ORBITH.read;
             %%end
            whos
             mach=circshift(mach,-mark(mod(cini-4,32)+1));
            blm=circshift(reshape(blm,[],32)',-mod(cini-3,32)+1)';
            blm=blm(:,[1:mod(cend-cini,32)]);
            
            blm=blm(:); 
            %mark_shift=sort([findcells(mach,'FamName','ID\w*')+2 findcells(mach,'FamName','DL\w*A\w*_2') findcells(mach,'FamName','DL\w*E\w*_2')...
             %   findcells(mach,'FamName','JL\w*_2') findcells(mach,'FamName','DL\w*D\w*_2')]);
            mark_shift=[findcells(mach,'FamName','ID\w*') sort([findcells(mach,'FamName','SDH\w*') findcells(mach,'FamName','SDL\w*')])];
             if numel(mark_shift)==64
             mark_shift=mark_shift(2:2:end);
             end
                       
            cells=mach(1:mark_shift(mod(cend-cini,32)));
            iblm=sort([findcells(cells,'FamName','ID\w*') findcells(cells,'FamName','QF7') findcells(cells,'FamName','B1H') findcells(cells,'FamName','DL\w*A\w*_2') findcells(cells,'FamName','DL\w*E\w*_2')...
                findcells(cells,'FamName','JL\w*_2') findcells(cells,'FamName','DL\w*D\w*_2')]);
            figure(2);
            clf(2);
            
            atbaseplot(cells);
            size(blm,1)
            size(iblm)
            if(size(blm,1)==numel(iblm))
                spos=findspos(cells,iblm);
                hold on
                yyaxis right;
                %ylim([-2 2]);
                plot(spos,blm(:)*1000);
                legend(['blm reading in cells: ' num2str([cini cend])])
                hold off;
            
            else
            error='blm vector length different from bmls numbers'
            end
       end
       

       function [or_sh,or_sv,blmh,blmv]=resp_blm(obj,amplitude)
           %  [or_sh,or_sv,blmh,blmv]=resp_blm(obj,amplitude)
           %measures the response of all BLMs to a steerer excitation. All
           %third steerers of each cell are used in horizontal and vertical
           %plane.
           %AMPLITUDE is the strength to be aplied to the steerer
           %OR_SH is the orbit response to horizontal steerers. 2d matrix of size numsteer*(2*numbpm).
           %horizontal and vertical orbits resulting from one steerer are
           %recorded in one single column.
           %first 320 lines are the Horbit and following 320 lines are the
           %vertical one.
           %OR_SV same as OR_SH but reponse to a vertical steerer.
           %BLMH reponse: steererh on BLM
           %BLMV response: steererv on BLM
           Iini=obj.CUR.read;
           ref_blm=[obj.BLM.read']./Iini;
           
           
           for i=1:8
           
               I=obj.CUR.read;
                fprintf(['cell' num2str(i) ',  current=' num2str(I) 'mA.\n']);
               if I<0.75*Iini
                   obj.reset_steer;                    
                    fprintf(['error, too much current lost, please reinject up to' num2str(Iini*1.1) 'mA']);
                   res=input('have you reinjected (y/n)','s');
                   if res~='y'
                       fprintf('error: current missing, measurement faled');
                   end
                end
                       
               
               
           ncorv=(i-1)*36+3;
           ncorh=(i-1)*48+3;
           ic_ini=obj.VSTRENGTH.read(ncorv);
           obj.VSTRENGTH.set(ncorv)=ic_ini+amplitude;
           pause(5)
           or_sv(:,i)=[obj.ORBITH.read';obj.ORBITV.read'];
           
           blmv(:,i)=(([obj.BLM.read']./obj.CUR.read));
           %%%%%-ref_blm);
           obj.VSTRENGTH.set(ncorv)=ic_ini;
           pause(5)        
           
           ic_ini=obj.HSTRENGTH.read(ncorh);
           obj.HSTRENGTH.set(ncorh)=ic_ini+amplitude;
           pause(5)
           or_sh(:,i)=[obj.ORBITH.read';obj.ORBITV.read'];
           blmh(:,i)=obj.BLM.read'./obj.CUR.read;
           %%%%%ref_blm;
           obj.HSTRENGTH.set(ncorh)=ic_ini;
           pause(5)
           end
           
           bpmstatus=tango.Attribute('srdiag/beam-position/all/All_Status');
           bpmok=~bitand(bpmstatus.read,128) | bitand(bpmstatus.read,2);
           or_sv([~bpmok(:);~bpmok(:)],:)=nan;
           or_sh([~bpmok(:);~bpmok(:)],:)=nan;
       end
       
       
       
     %% function resp=resp_or_blm(obj,plane,strength)
        %  if plane ==1
         %%     for i=1:32
           %%       blmini=obj.BLM(:);
          %%        cell=i
          %%      obj.BLM=obj.BLM(:)+randn(numel(obj.BLM),1); 
          %%        sti=obj.HSTRENGTH.read;
           %%       stt=sti;
                  
          %%        stt((i-1)*12+1)=stt((i-1)*12+1)+strength;
                 
       %%           obj.HSTRENGTH.set(:)=stt(:);
        %%          pause(15)
        %%          resp.blm(:,i)=obj.BLM(:)-blmini(:);
        %%          resp.or(:,i)=obj.ORBITH.read(:);
        %%          resp.stt(:,i)=stt;
        %%          obj.HSTRENGTH.set=sti;
        %%        pause(4)
        %%       end
        %%       figure(3)
        %%       plot(sum(resp.blm,2));
        %%   xlabel('BLM number');
        %%   ylabel('BLM signal sumed over all orbits');
        %   grid on
         %%  end
    %%   
       
       

      %% end
       
       
       
       function [corelh,corelv]=blm_resp_corel(obj,or_sh,or_sv,blmh,blmv)
             cells=obj.MODEL.ring;
             iblm=sort([findcells(cells,'FamName','ID\w*') findcells(cells,'FamName','QF7') findcells(cells,'FamName','B1H') findcells(cells,'FamName','DL\w*A\w*_2') findcells(cells,'FamName','DL\w*E\w*_2')...
             findcells(cells,'FamName','JL\w*_2') findcells(cells,'FamName','DL\w*D\w*_2')]);
             ibpm=[findcells(cells,'Class','Monitor') findcells(cells,'FamName','BPM')];
             whos           
            for i=1:320
                iblm_loc=find(iblm>ibpm(i),4);
                    if numel(iblm_loc)<4
                        iblm_loc=sort([iblm_loc 1:4-numel(iblm_loc)]);
                    end
                
                
                blmh_loc=blmh(iblm_loc,:);
                blmv_loc=blmv(iblm_loc,:);
                corelh(i)=mean2((or_sh(i,:)).*mean2(blmh_loc(1:4),2));
                corelv(i)=mean2((or_sv(320+i,:)).*mean2(blmv_loc(1:4),2));
            end
            figure(5);
            plot([corelh(:) corelv(:)]);
         legend('hcorelation','vcorelation');
          grid('on');  
         end
    end
end

