function [trajx,xx,zz] = ftcheck_sy(clock_delay,nbturn,thresh_sum,dirname)
%[spech,specv,Tmeas,delays] = meas_spec_sy(delays,Tmeas,dirname)
%plots a 3d graph of spectral analysis of tbt measurement in the booster and %zooms on low frequency part to see synchrotron lines. Spectrums are measured at %times defined by the DELAYS vector. 


%DELAYS: (ms) vector of size 1 X n. Spectrums measurements will start at times %tinj+DELAY
%TMEAS:(ms) acquisition window size
%FILENAME: optional FILENAME to save spectrums,DELAYS and TMEAS 
%SPECH matrix of h position spectrums(spectrums are in column)
%SPECV matrix of V position spectrums(spectrums are in column)
if clock_delay==0
clock_delay=0.00001
end

bpmh=[1:2:21 24 25:2:49 50:2:74];
bpmv=[2:2:22 23  26:2:48 51:2:75];
syall=tango.Device('sy/d-bpm/all');
clock=tango.Device('sy/d-bpm/aclock');
tmini=syall.AcquisitionDuration;
delayini=clock.ClockDelay;
syall.AcquisitionDuration=1;
syall.Decimation_Enable=false;
syall.TDP_QSUM_Enable=true;
syall.TDP_AMPL_Enable=true;
disable=syall.GetBPMStatus();
syall.SignalProcessing=int16(0);
gun=tango.Device('elin/beam/run');

ramp=tango.Device('sy/ps-rips/manager');
ramp.On();

res=gun.On();
for i=1:numel(disable);

dis(i)=size(disable{i},2)>10;
end

        
    clock.ClockDelay=clock_delay;
    pause(2.5)
    [zz(:,:),xx(:,:)] = Spark_All_TDP_XZ1s;
	sumsig=syall.All_TDP_QSUM;
    sumsig=sumsig.read';
    sumsig1=sumsig(:);
    


sumsig2=sumsig(numel(sumsig1)/2+1:end);
sumsig3=reshape(sumsig2,[],75);

sumsig=sumsig3';
       
   sum_nok=sumsig<thresh_sum;
  
   sum_nok(dis,:)=logical(1);
   sumsig(sum_nok)=nan;
   
   %sum_nok(bpmv,:)=logical(1)
   
   zz(sum_nok)=nan;
   xx(sum_nok)=nan;
    zz(bpmh,:)=nan;
    xx(bpmv,:)=nan; 
   
   
   trajx=(xx(sort([bpmh bpmv(12)]),4:nbturn+3));
   trajz=(zz(sort([bpmv bpmh(13) bpmh(26)]),4:nbturn+3));
   
   figure(1);
   subplot(3,1,1);
   plot(trajx(:));
   ylabel('H trajectory (mum)');
   
   subplot(3,1,2);
   plot(trajz(:));
   ylabel('V trajectory (mum)');
   
   subplot(3,1,3);
   plot(sumsig(3*75-5:end));
   ylabel('sum signal');
  
   figure(3)
   trajx(~isfinite(trajx))=0;
   trajz(~isfinite(trajz))=0;
  
   subplot(2,1,1)
   plot([0:1/nbturn:size(trajx,1)-1/nbturn],abs(fft2([trajx(:)])));
   legend(' fft of H trajectory');
   subplot(2,1,2)
   plot([0:1/nbturn:size(trajx,1)-1/nbturn],abs(fft2([trajz(:)])));
   legend('fft of V trajectory');
   tune(1)=findtunetom2(trajx(:),300,1)*39; 
   tune(2)=findtunetom2(trajz(:),300,1)*39 
xlabel('tune with integer part');
legend('fft of V trajectory');

if nargin>3
save(dirname,'spech','specv','Tmeas','delays','xx','zz');
end
%plot_fft_tbt(spech,delays,1E6,100000);

syall.SignalProcessing=int16(1);
syall.Decimation_Enable=true;
syall.AcquisitionDuration=tmini.read;
syall.TDP_QSUM_Enable=false;
clock.ClockDelay=delayini.read;

res=gun.Off();
ramp.Off();
gun.dvclose;
syall.dvclose;
