 function res=fittune_traj(x,bpm,mach,or,co4)
 init=x*1000
 
 numbpm=size(or,1);  
 
 QD2=findcells(mach,'FamName','QD2\w*');
 QF1=findcells(mach,'FamName','QF1\w*');
 
 S_QD2ini=getcellstruct(mach,'PolynomB',QD2,1,2);
 S_QF1ini=getcellstruct(mach,'PolynomB',QF1,1,2);
 
 if numel(x)>2
     cini=[x(1:4)';0;0];
    mach=setcellstruct(mach,'PolynomB',QD2,S_QD2ini.*(1+x(5)),1,2);
    mach=setcellstruct(mach,'PolynomB',QF1,S_QF1ini.*(1+x(6)),1,2);
 else
     cini=[co4(1:4)';0;0];
     mach=setcellstruct(mach,'PolynomB',QD2,S_QD2ini.*(1+x(1)),1,2);
     mach=setcellstruct(mach,'PolynomB',QF1,S_QF1ini.*(1+x(2)),1,2);
       
      
 end
  bpmok=isfinite(sum(or,2));

  orb=linepass(mach,cini,[bpm(1:min([numbpm 320]));numel(mach)+1]);
  if numbpm>320
      orb2=linepass(mach,orb(:,end),bpm(1:320));
      orb=[orb(:,1:end-1) orb2];
      orb=orb(1:2:3,1:size(or,1))' ;
  
  else
      
      orb=orb(1:2:3,1:size(or,1))';
  end

  
  diff=orb-or;
  diff(isnan(diff))=1E-2;
 figure(15)
 plot(diff);
  res=sum(abs(diff(:,1)))+sum(abs(diff(:,2)))
 