function [xintersect,dpp]=GetSlopeChange(dafile,seppoint)
%
% dafile.mat contains position and HKval
% seppoint is the last point to fit the first line. se second line is a
% constant.
%
% xintersect is scraper aperture at wich the DA limit is reached (not
% corrected for orbit)
%
% dpp is (if any) the energy deviation computed as dpp=-DF/alpha_c/Frf
% 
%see also: 

%load(['DA_dpp0perc.mat']);

dd = load(dafile);

% fit data with straigth line and constant.
pi=dd.position(~isnan(dd.HKval)); % min(position):1e-3:max(position);
hi=dd.HKval(~isnan(dd.HKval));    % interp1(position,HKval,pi,'linear');

if length(hi) < seppoint
    error('not enough points measured')
end

sel1=[1:seppoint];
sel2=[seppoint+1:length(hi)];

P1=polyfit(pi(sel1),hi(sel1),1); % hor. kicker  calibration

%P2=polyfit(pi(sel2),hi(sel2),1);
C2=polyfit(pi(sel2),hi(sel2),0); 

figure;
%subplot(2,1,1);
plot(pi,hi,'b-','LineWidth',2,'MarkerSize',12); hold on; 
plot(dd.position,dd.HKval,'ro','LineWidth',2,'MarkerSize',12);
plot(pi,polyval(P1,pi),'m--','LineWidth',2,'MarkerSize',12);
plot(pi,polyval(C2,pi),'c--','LineWidth',2,'MarkerSize',12);
xlabel({'','Scraper position [mm]'});
ylabel({'Horizontal Kicker Amplitude [A]',''});
title({'Horizontal Kicker Amplitude';'to loose 5% on the scraper'});

%subplot(2,1,2);
%plot(pi(1:end-1),diff(hi)./diff(pi)) ;%+(pi(2)-pi(1))/2

m1=P1(1);b1=P1(2);
%m2=P2(1);b2=P2(2);
m2=0; b2=C2;

xintersect = (b2-b1)/(m1-m2);
yintersect = m1*xintersect + b1;

plot(xintersect,yintersect,'ks','LineWidth',2,'MarkerSize',12);
grid on;
set(gcf,'color','white');

try
    a=load('/operation/beamdyn/matlab/optics/sr/theory/alpha');
    alpha_c = a;
    f0=dd.frf0;
    df=dd.frf1-dd.frf0;
    dpp=-df/alpha_c/f0;
catch
    dpp=0;
    df=0;
end
title(['Horizontal Kicker Amplitude to lose 5% on the scraper. Df=' num2str(df) ' Hz']);

