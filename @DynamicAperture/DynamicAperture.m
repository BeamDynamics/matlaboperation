classdef DynamicAperture < RingControl %& GridScan
    %DYNAMICAPERTURE
    %
    % measure dynamic aperture following scripts of 2015 Oct 06 MDT
    %
    % H kicker amplitude (V) is calibrated to m using an H scraper
    % Than the maximum kick at wich beam is not lost. This is considered the DA
    % limit.
    %
    %see also:
    
    properties
        
        dirname         % directory to save temporary data
        
        dfreq           % frequency change step [Hz]
        dpp
        
        scraper_positions % positions to step scraper in [mm]
        scraper_positions_injection % position of scraper for injection
        exp_DA_limit_scraper_position % expected DA limit in terms of scraper position (at the scraper)
        
        beta_scale          % scaling from beta at scraper to beta at injection 
        off_energy_beam_position % off energy beam position
            
        khdev           % device name of hor. kicker to use for amplitue scan
        kedev           % device name of extraction. kicker to use to trigger injection bump
        
        khdevCurr0      % initial Khdev Current, used only if kickmode = fullbump
        kickmode        % "fullbump" = full bump + reduce khdev, "singlekicker" = zero bump + increase khdev
        number_kicks_average
        
        inj_kick_devs   % array of kicker devices
        inj_kick_init_A   % value of kickers at initialization = values for injection
        inj_kick_pres_A   % present value of kickers to be restored after injection took place
        inj_kick_init_State   % kickers state at initialization = values for injection
        inj_kick_pres_State   % present value of kickers state to be restored after injection took place
        
        
        tolhk           % A
        diithres        % loss rate %
        StoreCur        % mA of stored beam
        
        HKval           % initialize h kicker value for threshold diithres
        
        amplhk          % kicker amplitudes to scan
        amplhk0         % initial kicker amplitudes to scan
        
        kicker_amplitude_step % step in kicker amplitudes
        
        measurement_name   %  label for the rpesent measurement
        measurement_figure %  figure handle
        measurement_type   %  type: kick-stored-beam, trajectory, ... 
        
        % final data
        x               % maximum horizontal amplitude
        d               % delta energy
        o               % orbit deviation due to frequency shift
        q               % tunes
        
        % temporary solutions for test in simulator
        ft              % instance of the FirstTunrs class to allow for DA measurement simulation.
    
    end
    
    
    methods
        
        % creator
        function obj = DynamicAperture(machine,varargin)
            %DynamicAperture Construct an instance of this class
            %
            % for help type:
            % >> doc DynamicAperture
            % or confluence.esrf.fr
            %
            %see also:
            
            expectedmodes = {'singlekicker','fullbump'};
            
            p=inputParser;
            addRequired(p,'machine');
            addParameter(p,'mode',expectedmodes{1});%,@(x)validatestring(x,expectedmodes))
            
            parse(p,machine,varargin{:});
            
            
            obj@RingControl(machine);
            
            obj.kickmode = p.Results.mode;
            
            obj.dfreq = 0; % on energy DA only by default
            obj.dpp =0;
            
            
            obj.scraper_positions = -[2.0:0.5:7]; % in [mm]

            obj.scraper_positions_injection = -7; % in [mm]
            
            % measurement results
            obj.x = NaN(length(obj.dfreq));
            obj.d = NaN(length(obj.dfreq));
            obj.o = NaN(length(obj.dfreq));
            obj.q = NaN(length(obj.dfreq),2);
            
            
            switch obj.machine
                case 'sr'
                    %obj.kickmode= 'fullbump'; % or 'singlekicker'
                    %obj.kickmode= 'singlekicker'; % or 'singlekicker'
                    
                    
                    
                    % kicker device used to change amplitude of stored beam
                    % oscillations
                    obj.khdev = 'sr/ps-k4/4';
                    
                    % kicker device used to trigger kickers firing (Extraction
                    % kicker)
                    obj.kedev = 'sy/ps-ke/1';
                    
                    disp(' - - - - - - - - - - ');
                    disp(' -                 - ');
                    disp('At this stage Injection kickers ');
                    disp('should be "ready to inject"');
                    disp('= Standby and correct currents for closed injection bump.')
                    disp(' -                 - ');
                    disp(' - - - - - - - - - - ');
                    
                    obj.inj_kick_devs = {...
                        tango.Device('sr/ps-k1/1'),...
                        tango.Device('sr/ps-k2/2'),...
                        tango.Device('sr/ps-k3/3'),...
                        tango.Device('sr/ps-k4/4')};  % array of kicker devices
                    
                    [obj.inj_kick_init_A, obj.inj_kick_init_State ]=...
                        obj.getInjKickCurrentsStates;   % value of kickers at initialization = values for injection
                    
                    obj.measurement_type = 'kick-stored-beam'; % ''
                    
                    obj.number_kicks_average = 3;
                    
                    switch obj.kickmode
                        case 'fullbump'
                            obj.khdevCurr0 = tango.Attribute([obj.khdev '/Current']).set;
                        case 'singlekicker'
                            obj.khdevCurr0 = 0;
                        otherwise
                            error('not appropriate kickmode, either fullbump or singlekicker');
                    end
            
            
            
                case 'ebs-simu'
                    % obj.kickmode= 'singlekicker'; % or 'singlekicker'
                    obj.measurement_type = 'trajectory'; % ''
                    obj.number_kicks_average = 1;
                    obj.kickmode = 'singlekicker'; 
                    obj.khdevCurr0 = 0;
                    % instance of the first turns class to simulate trajectory
                    % measurement in the simulator (simulator current is a number
                    % uncorrelated to beam properties)
                    obj.ft = FirstTurns(obj.machine);
                    obj.ft.setTurns(10);
                    
                otherwise
                    error('DynamicAperture is not supported for this machine')
                    
            end
               
            
            obj.tolhk=1; % A
            obj.diithres=0.01; % loss rate %
            obj.StoreCur=1.0; % mA of stored beam
            
            % initialize h kicker value for threshold diithres
            obj.HKval=NaN(size(obj.scraper_positions));
            
            
            switch obj.measurement_type
            
                case 'trajectory'
                    obj.kicker_amplitude_step = 5;
                    obj.amplhk =[100:obj.kicker_amplitude_step:500];  % initial kicker amplitudes to scan
             
                otherwise
                    obj.kicker_amplitude_step =5;
                    obj.amplhk =[10:obj.kicker_amplitude_step:1500];  % initial kicker amplitudes to scan
            end
            
            obj.amplhk0 = obj.amplhk;  % initial kicker amplitudes to scan
           
            obj.measurement_name = ['DynAp_' datestr(now,'YYYY_mm_dd_HH_MM')];
            obj.measurement_figure = figure('name',obj.measurement_name);
            
            
            obj.dirname = [obj.measurement_name ];%'_' strrep(num2str(obj.dfreq),'-','m') 'Hz'];
            
            
            % dispaly settings
            obj.DASettingSummary;
            
            
        end
        
        function scaling_coefficents(obj)
            
            r = obj.rmodel;
            f0 = unique(atgetfieldvalues(r,atgetcells(r,'Frequency'),'Frequency'));
            ind_scrap=find(atgetcells(r,'FamName','\w*H_Scraper'),2,'last');
            ind_scrap = ind_scrap(end); %only last scraper (C11)
            [l,~,~] = atlinopt(r,0,[1,ind_scrap']);
                
                
            for df = 1:length(obj.dfreq)
                
                obj.dpp(df) = - obj.dfreq(df)./f0./mcf(r);
                
                [ldpp,~,~] = atlinopt(r,obj.dpp(df),[1,ind_scrap']);
                
                obj.beta_scale(df) = sqrt(l(1).beta(1)/ldpp(end).beta(1)); %(on energy beta at injection to off energy beta at scraper)
                obj.off_energy_beam_position(df) = ldpp(end).ClosedOrbit(1)*1e3;
            end
            
        end
        
        function DASettingSummary(obj)
        % display present settings of DA measurements
        theodpp = [-6 : 1: 6]; % [%]
        theoDA = [0 -1 -2 -3 -6 -7 -10 -8 -8 -6 -5 -2 0]; %[mm]
        
        % recompute scaling coefficents
        obj.scaling_coefficents;
        
        obj.exp_DA_limit_scraper_position = interp1(theodpp,theoDA,obj.dpp*100)./obj.beta_scale + obj.off_energy_beam_position;
        
        disp('---- DA measurement settings ----')
        disp(['folder: ' obj.dirname]);
        disp(['machine: ' obj.machine]);
        disp('----  ----')
        disp(['delta frequency steps [Hz]: '  num2str(obj.dfreq)]);
        disp(['delta Energy [%]: '  num2str(obj.dpp*100,'%2.1f ')]);
        disp(['beta scaling to injection: '  num2str(obj.beta_scale)]);
        disp(['off energy beam position at scraper [mm]: '  num2str(obj.off_energy_beam_position)]);
        disp(['expected scraper limit [mm]: ' num2str(obj.exp_DA_limit_scraper_position)]);
        disp('----  ----')
        disp(['Scraper positions [mm]: ' num2str(obj.scraper_positions)]);
        disp(['measurement type: ' obj.measurement_type]);
        disp(['Kicker used: ' obj.khdev ' (' num2str(obj.khdevCurr0) ' A)']);
        disp(['Ext. Kicker: ' obj.kedev]);
        disp(['Kick mode: ' obj.kickmode]);
        disp(['# of Kicks to average : ' num2str(obj.number_kicks_average)]);
        disp(['Kicker amplitudes: ' num2str(obj.amplhk)]);
        disp(['---- ---- ---- ---- ---- ----']);
        
        end
        
        function [kickersCurrent,kickersState] = getInjKickCurrentsStates(obj)
            switch obj.machine
                case 'sr'
                    kickersCurrent = {...
                        obj.inj_kick_devs{1}.Current.set,...
                        obj.inj_kick_devs{2}.Current.set,...
                        obj.inj_kick_devs{3}.Current.set,...
                        obj.inj_kick_devs{4}.Current.set,...
                        };
                    
                    kickersState = {...
                        obj.inj_kick_devs{1}.State,...
                        obj.inj_kick_devs{2}.State,...
                        obj.inj_kick_devs{3}.State,...
                        obj.inj_kick_devs{4}.State,...
                        };
                    
                    
                    % update present currents and states
                    obj.inj_kick_pres_A  = kickersCurrent;
                    obj.inj_kick_pres_State  = kickersState;
                otherwise
                    obj.inj_kick_pres_A  = [];
                    obj.inj_kick_pres_State  = [];
            end
        end
        
       
        
        
        function  setInjKickCurrentsStates(obj, kickersCurrent, kickersState)
            % set kicker currents and states
            switch obj.machine
                case 'sr'
                    for ii = 1:4  % loop kickers
                        
                        obj.inj_kick_devs{ii}.Off();pause(1);
                        
                        obj.inj_kick_devs{ii}.Current = kickersCurrent{ii};
                        
                        % chose command to send according to wished state
                        switch kickersState{ii}
                            case 'On'
                                disp('Set On')
                                obj.inj_kick_devs{ii}.On();
                            case 'Standby'
                                disp('Set Standby')
                                obj.inj_kick_devs{ii}.Standby();
                            case 'Off'
                                disp('Set Off')
                                obj.inj_kick_devs{ii}.Off();
                        end
                        
                    end
                otherwise
                    disp('not SR. nothing done.');
            end
        end
        
        
        function [dii] = trajectory_numBPM_vs_kicker_amplitude(obj,val)
            % use first turns SIMULATED trajectory to "measure" DA
            
            for ii = 1:length(val)
               
                % simulated trajectory with kickers fired on the first turn
                disp(val(ii));
                
                [t,~,~,~] = ...
                    obj.ft.findNturnstrajectory6Err(...
                    obj.rmodel,[val(ii)*1e-5 0],[0 0],[0 0],zeros(6,1));
                
                
                dii(ii) = find(~isnan(t(1,:)),1,'last')/length(t(1,:));
            end
            
            dii = 1 - dii; % loss percentage
        end
        
        function [dii,cur] = stored_current_vs_kicker_amplitude(obj,nave,val,tag,threshold)
            % stored_current_vs_kicker_amplitude varies the kiecker amplitude
            % and reads the stored current variation over a given number of
            % shots.
            
            if nargin < 5, threshold=0.01; end
            if nargin < 4, tag='dii'; end
            cur=NaN*ones(size(val));
            dii=NaN*ones(size(val));
            hh=plot(val,dii,'-o','Tag',tag);
            i0=obj.stored_current();
            
            k1dev = tango.Device(obj.khdev);
             
            % change state to Standby, to make sure the relevant kicker is 
            % piloted by KE
            % this action done here allows to give any kicker as obj.khdev
            if k1dev.State ~= 'Standby'
                k1dev.Standby(); pause(3);
                pause(0.5);
            end
            
            kEdev = tango.Device(obj.kedev);
            
            disp(k1dev.Current.set)
            disp(obj.khdevCurr0 -val(1))
            
%            setstandby = false;
%            if k1dev.Current.set >  obj.khdevCurr0 -val(1)  % reducing
%                setstandby = true;
                k1dev.Off();pause(5);
%            end
            
            switch obj.kickmode
                case 'fullbump'
                    k1dev.Current=   obj.khdevCurr0 -val(1);
                case 'singlekicker'
                    k1dev.Current = val(1);
                otherwise
                    error('not appropriate kickmode, either fullbump or singlekicker');
            end
            
 %           if setstandby
            k1dev.Standby(); pause(8);
 %           end
            
 %           k1dev.Standby();
            
            InitCounterMode = kEdev.CounterMode;
            kEdev.CounterMode=int16(nave);
            
            for i=1:numel(val)
                
                switch obj.kickmode
                    case 'fullbump'
                        fprintf('Kicking with %.3f A\n',obj.khdevCurr0 -val(i));
                        k1dev.Current=   obj.khdevCurr0 -val(i);
                    case 'singlekicker'
                        fprintf('Kicking with %.3f A\n',val(i));
                        k1dev.Current=val(i);
                    otherwise
                        error('not appropriate kickmode, either fullbump or singlekicker');
                end
                
                pause(3);
                r1=obj.stored_current(); % mA
                kEdev.On(); % trgger kickers
                pause(8+nave); 
                r2=obj.stored_current(); % mA
                
                deltai=(r2-r1)/nave/r1; % average relative lost current per shot
                r=-deltai*sum(1./(1+(0:nave-1)*deltai))/nave; % adjust to correct percentage at each shot (initial current reduces one shot after the other)
                
                cur(i)=r2;
                dii(i)=r;
                
                set(hh,'YData',dii);
                
                % stop if current reduced more than threshold (avoid kill)
                if (r > 1.2*threshold) break; end 
                
                % stop if more than 90% of the initial current is lost
                if (r2 < i0/10) break; end
                
            end
            % kEdev.CounterMode=InitCounterMode;
            kEdev.Standby();
            
            
        end
        
        function [dii] = measure_dii(obj)
            % measures decrease of current for a given kick.
            % 
            % this curve will be used to determine the maximum kick for a
            % given scraper setting. if scrapers and collimators are open,
            % then the maximum kick is proportional to DA. 
            % 
           
            measurementtypes= {'kick-stored-beam','trajectory'};
            
            switch obj.measurement_type
                case measurementtypes{1}
                    % scan horizontal kicker
                    switch obj.machine
                        case 'sr'
                            dii = obj.stored_current_vs_kicker_amplitude(obj.number_kicks_average,obj.amplhk,'dii',obj.diithres); % stops at diithres*1.2
                        otherwise
                            error([measurementtypes{1} ' not supported for ' obj.machine]);
                    end
                case measurementtypes{2}
                    % count BPMs with beam in first turns
                    dii = obj.trajectory_numBPM_vs_kicker_amplitude(obj.amplhk);
                
                otherwise
                    disp(['Possible measurement types are: ']);
                    disp(measurementtypes);
            end
            
        end
    
        
        function measureDA(obj)
            % Measure horizontal DA
            
            % create directory to store data
            mkdir(obj.dirname);
            
            disp(['Change kicker states according to mode: ' obj.kickmode])
            % set kickers states for measurement.
            switch obj.kickmode
                case 'fullbump'
                    disp('all kickers Standby')
                    obj.setInjKickCurrentsStates(obj.inj_kick_init_A,{'Standby','Standby','Standby','Standby'})
                case 'singlekicker'
                    disp('all kickers Off, the relavant one will be turned back to Standby later.')
                    obj.setInjKickCurrentsStates(obj.inj_kick_init_A,{'Off','Off','Off','Off'})
                otherwise 
                    error(['not appropriate kickmode (' obj.kickmode '). Chose either fullbump or singlekicker']);
            end
            
            
            % reset initial kicker amplitudes to scan to default
            obj.amplhk = obj.amplhk0;  
           
            try
                set(0,'CurrentFigure',obj.measurement_figure);
            catch
                obj.measurement_figure = figure('name',obj.measurement_name);
            end
            
            position0 = obj.scraper.get;
            frf0 = obj.rf.get; % Hz
            
            for ifreq = 1:length(obj.dfreq) % loop frequency , thus Dp/p
                
                for ip=1:length(obj.scraper_positions)
                    
                    if ~exist(fullfile(obj.dirname,['df' num2str(ifreq) '_' num2str(ip) '_temp.mat']),'file')
                    % set RF frequency
                    %frf1=frf0-dpp*frf0*Alpha_c;
                    frf1=frf0+obj.dfreq(ifreq);
                    disp([' frequency going to : ' num2str(frf1) ' Hz.']);
                    
                    if frf1>frf0
                        fset=[frf0:20:frf1 frf1];
                    else
                        fset=[frf0:-20:frf1 frf1];
                        %    fset=[(frf0-950):-100:frf1 frf1];
                    end
                    
                    % get orbit before frequency change
                    orb = obj.measureorbit;
                    
                    Ox0 = orb(1,:);
                    Oy0 = orb(2,:);
                    
                    % get tunes before frequency change
                    Qx0 = obj.h_tune();
                    Qy0 = obj.v_tune();
                    
                    for ifre=fset
                        obj.rf.set(ifre);
                        pause(1); % long pause or beam lost due to too fast change
                    end
                    
                    % get orbit after frequency change
                    orb = obj.measureorbit;
                    
                    Oxdf = orb(1,:);
                    Oydf = orb(2,:);
                    
                    % get tunes after frequency change
                    Qxdf = obj.h_tune();
                    Qydf = obj.v_tune();
                    
                    theo_beam_pos = obj.off_energy_beam_position(ifreq);
                    disp(['theoretical beam position: ' num2str(theo_beam_pos,'%3.3f') ' mm']);
                    
                    % save orbit
                    save(fullfile(obj.dirname,['orb_scrappos' num2str(ip) '_df' num2str(ifreq) '.mat']),...
                        'frf1','frf0','Oxdf','Oydf','Ox0','Oy0');
                     save(fullfile(obj.dirname,['tune_scrappos' num2str(ip) '_df' num2str(ifreq) '.mat']),...
                        'frf1','frf0','Qxdf','Qydf','Qx0','Qy0');
                   
                    if ip==1
                        ooo=Oxdf-Ox0;
                        obj.o(ifreq)=(ooo(1)+ooo(end))/2; % average orbit at injection
                        
                        qqq=Qxdf-Qx0;
                        obj.q(ifreq,1)=qqq; % H tune change due to energy change
                        
                        qqq=Qydf-Qy0;
                        obj.q(ifreq,2)=qqq; % V tune change due to energy change
                    end
                    
                    
                    %% set scraper and scan Hor. Kicker
                    
                    % set scraper position
                    scrappos=obj.scraper_positions(ip);
                    
                    disp([' Scraper going to : ' num2str(scrappos) ' mm']);
                    
                    obj.scraper.set(scrappos,'wait'); % movable class takes care of waiting for setting to be reached
                     
                    % NOT WORKING CAUSE GET READS SET!
%                     while abs(obj.scraper.get()-scrappos)>0.1
%                         pause(5)
%                         disp(['Scraper at : ' num2str(obj.scraper.get()) ' mm']);
%                     end
                    disp(['Scraper at : ' num2str(obj.scraper.get()) ' mm']);
                    
                    if ip==1
                        pause(60);
                    else
                        pause(10);
                    end
                    
                    disp(['Scan horizontal kicker']);
                    
                    try
                       
                           [dii] = obj.measure_dii;
                       
                    catch errmes
                        
                        disp(errmes.message)
                        
                        % ask if loop is finished
                        reply='';
                        while isempty(find(strcmp(reply,{'Y','N'}), 1))
                            
                            reply = input('amplitude scan did not work. Retry once? Y/N :','s');
                            
                        end
                        
                        if  strcmp(reply,'N')
                            break;
                        end
                        
                        [dii] = obj.measure_dii;
  
                    end
                    
                    curs=obj.amplhk(1:length(dii));
                    
                    disp(['interpolate hor. kicker at ' num2str(obj.diithres)]);
                    
                    indthres=find(dii>obj.diithres,1); % first above threshold
                    
                    x1=curs(indthres-1); % previous point
                    x2=curs(indthres);
                    y1=dii(indthres-1); % previous point
                    y2=dii(indthres); %
                    
                    a=(x1-x2)/(y1-y2);
                    b=(x2*y1 - x1*y2)/(y1-y2);
                    curthres= (obj.diithres*a + b);
                    
                    diithres = obj.diithres;
                    save(fullfile(obj.dirname,['scrappos' num2str(ip) '_df' num2str(ifreq) '.mat']),...
                        'dii','curs','scrappos','curthres','diithres','frf1','frf0');
                    
                    disp(['Saved data in : ' [obj.dirname '/' 'scrappos' num2str(ip) '_df' num2str(ifreq) '.mat']])
                    
                    f2=figure;
                    
                    plot(curs,dii*100,'.b--','LineWidth',2,'MarkerSize',12);hold on;
                    plot([x1,x2],[y1,y2]*100,'g--','LineWidth',2,'MarkerSize',12);
                    plot(curthres,obj.diithres*100,'ro','LineWidth',2,'MarkerSize',12);
                    xlabel('hor. kicker amplitude [A]')
                    ylabel('lost current on scraper [%]');
                    title(['Scraper at : ' num2str(scrappos) ' mm'])
                    grid on;
                    set(gcf,'color','white');

                    drawnow;
                    saveas(gca,fullfile(obj.dirname,['scrappos' num2str(ip) '.fig']));
                    
                    % Alternative interpolation
                    %curthres2=interp1([y1,y2],[x1,x2],diithres);
                    %plot(curthres2,diithres,'gx')
                    
                    obj.HKval(ip) = curthres;
                    
                    % redefine amplitude of horizontal kicker.
                    disp('Move back by 6 steps in kicker current')
                    obj.amplhk=[(obj.HKval(ip)-6*obj.kicker_amplitude_step) :... % step back tree points in amplitude before the next scan
                        obj.kicker_amplitude_step : max(obj.amplhk)];
                    
                    % plot scraper scan figure
                    try
                        figure(obj.measurement_figure);
                    catch
                        obj.measurement_figure = figure;
                    end
                    
                    plot(obj.scraper_positions,obj.HKval,'bo-');
                    grid on;
                    xlabel({'',...
                        ['scraper position [mm] '],...
                        ['beam position at scraper: ' num2str(theo_beam_pos,'%3.3f') ' mm'],...
                        ['beta scaling: ' num2str(obj.beta_scale(ifreq),'%3.3f') ''],...
                        });
                    ylabel('kicker amplitude [A]');
                    
                     
                    
                    % save data
                    position = obj.scraper_positions;
                    HKval = obj.HKval;
                    beta_scale = obj.beta_scale(ifreq);
                    off_energy_position = obj.off_energy_beam_position(ifreq);
                    save(fullfile(obj.dirname,['df' num2str(ifreq) '_' num2str(ip) '_temp.mat']),...
                        'position','HKval','frf1','frf0','off_energy_position','beta_scale');
                    
                    
                    %     %% check if limit is reached
                    %     if ip>4
                    %     disp(['decide automatically if scraper scan is finished'])
                    %
                    %         meanhka=mean(HKval((end-1):(end-4)));
                    %         meanhk=mean(HKval((end):(end-3)));
                    %         disp(['mean cur  last 3: ' num2str(meanhk) ' '])
                    %         disp(['mean prev last 3: ' num2str(meanhka) ' '])
                    %
                    %          %  if abs(meanhk-meanhka)<tolhk
                    %
                    %                  disp('last tree points are identical, DA limit has been reached');
                    %
                    %              break;
                    %
                    %          end
                    %
                    %     end
                    
                    % set RF frequency to noiminal for injection in steps!
                    
                    for ifre=fset(end:-1:1)
                        obj.rf.set(ifre);
                        pause(1);
                    end
                    
                    obj.rf.set(frf0);
                    disp('Frequency back to initial value for injection.')
                    obj.scraper.set(obj.scraper_positions_injection);
                    disp('Scraper back to injection value.')
                    
                    
%                     % ask if loop is finished
%                     reply='';
%                     while isempty(find(strcmp(reply,{'Y','N'}), 1))
%                         
%                         reply = input('Do you want to stop the scraper scan? Y/N :','s');
%                         
%                     end
%                     
%                     if  strcmp(reply,'Y')
%                         break;
%                     end
%                     
                    
                    %% top up
                    
                    disp('topping up before next step')
                    switch obj.measurement_type
                        case 'trajectory'
                        otherwise
                            
                            % store present kickers states and currents
                            % updates inj_kick_pres_A inj_kick_pres_State
                            obj.getInjKickCurrentsStates;
                            
                            % set kickers to initial currents and states
                            obj.setInjKickCurrentsStates(obj.inj_kick_init_A, obj.inj_kick_init_State);
                            
                            input('please reload injection operation file and refill (if nor refill ratios will be WRONG). Press any key to continue ')
                            % refill();% using top up macro by L.F.
                            
                            % set kickers to currents and states before refill 
                            obj.setInjKickCurrentsStates(obj.inj_kick_pres_A, obj.inj_kick_pres_State);
                            
                    end
                    
                    while obj.stored_current()<obj.StoreCur
                        disp(['Perform now REFILL to ' num2str(obj.StoreCur) 'mA! ']);
                        
                        %disp('waiting for current (refill())');
                        
                        
                        pause(5);
                    end
                    
                   
                    close(f2);
                    
                    
                    else % reload existing data
                        disp( [ ['df' num2str(ifreq) '_temp.mat'] ' is already measured '] );
                        a = load(fullfile(obj.dirname,['df' num2str(ifreq) '_temp.mat']));
                        obj.HKval(ip)= a.HKval(ip);
                        frf1 = a.frf1;
                        frf0 = a.frf0;
                        
                    end
                end % end of scraper positions loop
                
                % back to intial scraper position
                obj.scraper.set(position0);
                
                % back to initial injection state
                obj.setInjKickCurrentsStates(obj.inj_kick_init_A, obj.inj_kick_init_State);
                            
                
                % save data
                filename = fullfile(obj.dirname,['df' num2str(ifreq)]);
                position = obj.scraper_positions;
                HKval = obj.HKval;
                
                save([filename '.mat'],'position','HKval','frf1','frf0');
               
                [xintersect,dpp]=obj.GetSlopeChange([filename '.mat'],3);
                
                obj.x(ifreq) = xintersect;
                obj.d(ifreq) = dpp;
                
                % save plot
                try 
                    figure(obj.measurement_figure);
                catch
                    obj.measurement_figure = figure;
                end
                plot(obj.scraper_positions,obj.HKval,'bo-');
                grid on;
                xlabel('scraper position [mm]');
                ylabel('kicker amplitude [A]');
                
                saveas(gca,[filename '.fig']);
                close(obj.measurement_figure);
                
            end
            
            figure;
            plot(obj.d*100,obj.x,'b.-','DisplayName','Scraper Limit');
            hold on; plot(obj.d*100,obj.x+obj.o*1e3,'r.-','DisplayName','DA, with orbit at Scraper'); % scraper position is negative,
            title('Measured hor. DA vs energy')
            legend toggle;
            ylim([0,20]);
            xlim([-2,2]);
            xlabel('Energy deviation [%]')
            ylabel('hor. DA. [mm]')
            
            saveas(gca,fullfile(obj.dirname,['DAvsDPP' obj.measurement_name  '.fig']));
            %export_fig('DAvsDPP.jpg');
            d=obj.d; x=obj.x; o=obj.o;
            save(fullfile(obj.dirname,['DAmeasData' obj.measurement_name  '.mat']),'d','x','o');
            
            
            % detuing with energy.
            figure;
            plot(obj.dfreq,obj.q(:,1),'r.-',obj.dfreq,obj.q(:,2),'b.-','LineWidth',2);
            grid on;
            xlabel('\Delta Freq RF [Hz]');
            ylabel('tune');
            legend('Q_x','Q_y');
            
        end
        
        % signature of methods defined in seprated functions below
        
        
    end
    
    
    methods (Static) % methods that do not need to know about this class, no input "obj"
        
        [xintersect,dpp]=GetSlopeChange(dafile,seppoint);
        
    end
    
end

