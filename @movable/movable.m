classdef movable < handle
    %MOOVABLE initialize tango attribute and defines ancillary properties
    %   
    %  tango attribute control object
    % 
    %  get method applies calibration, returns rad if calibration is rad/A
    %  set function :
    %       - input in calibrated units,
    %       - check limits
    %       - waits for setpoint to be reached.
    %  
    %  object stores also:
    %  min    minimum allowed setting (tango minimum if not provided or too small)
    %  max    maximum allowed setting (tango maximum if not provided or too large)
    %  value_at_init    initial value of attribue when object created.
    %  set_tolerance    difference tolerance to check for setting point 
    %  calibration      scalar calibration value
    %
    
    properties
        attr_name       % name of attribute in Tango
        attribute       % attribute object
        min_val             % minimum value
        max_val             % maximum value
        calibration     % calibration rad/A, [1/m]/A, ...
        set_tolerance   % tolerance for reaching set point
        value_at_init   % value of moovable at initialization
        get_fun         % function to get values
        set_fun         % function to set values
    end
    
    methods
        function obj = movable(name,varargin)
            %MOVABLE Construct an instance of this class
            %   a MOVABLE is an object that has:
            %
            %   name: TAGO Attribute name
            %   limits: [2x1] limits of tunability of attribute value
            %           defaults: attribute.attrinfo.min_value, attrinfo.max_value
            %   calibration: array of scalars of calibration [units]/A
            %           defaults: 1
            %   set_tolerance: array of scalars of calibration [units]/A
            %   value_at_init: value of attribute at init of class
            %   get_fun: function to retrive the wished values
            %            @(devname)fun(...)
            %   set_fun: function to set the wished values
            %            @(devname, values)fun(...)
            %            devname is the input name, that is passed to this
            %            object.
            % ex1:
            % rf = moovable(['srrf/master-oscillator/1/Frequency'],'absolute',true,'limits',[0 352e6]);
            % ex2: 
            % rf = moovable(['srrf/master-oscillator/1/Frequency'],'absolute',false,'limits',[0.9 1.1]);
            % ex3:
            % empt = moovable('');
            % ex4:
            % sh = movable([TANGO_HOST 'srmag/hst/all/Strengths'],...
            %      'get_fun',@(devname)tango.Attribute(devname).value,...
            %      'set_fun',@(devname,val)setfield(tango.Attribute(devname),'set',val),...
            %      'absolute',true,'limits',[-4e-4 4e-4]);
            % 
            %see also: 
           
            p = inputParser;
            
            addRequired(p,'name',@ischar);
            addOptional(p,'absolute',true,@islogical);
            addOptional(p,'limits',[],@isnumeric);
            addOptional(p,'calibration',1,@isnumeric);
            addOptional(p,'set_tolerance',[],@isnumeric);
            addOptional(p,'get_fun',@(devname)NaN(1),@(a)isa(a,'function_handle'));
            addOptional(p,'set_fun',@(devname,val)setfield(obj.attribute,'set',NaN) ,@(a)isa(a,'function_handle'));
            
            parse(p,name,varargin{:});
            
            obj.attr_name = p.Results.name;
            obj.get_fun = p.Results.get_fun;
            obj.set_fun = p.Results.set_fun;
            obj.calibration = p.Results.calibration;
            
            obj.min_val = -Inf;
            obj.max_val = Inf;
            
            % deal with empty moovable
            if isempty(obj.attr_name)
                warning('empty movable')
                
                obj.attribute.value = NaN;    % always return NaN
                obj.attribute.set = NaN;      % define a set field, will store the value set, but no effect on .value.
                obj.attribute.attrinfo.min_value = -Inf; % no limits
                obj.attribute.attrinfo.max_value = Inf;
                
            else
                try
                    obj.attribute.name = obj.attr_name;
                    obj.attribute.value = obj.get;    % always return NaN
                    obj.attribute.set = obj.attribute.value; 
                    obj.attribute.attrinfo.min_value = obj.min_val; % no limits
                    obj.attribute.attrinfo.max_value = obj.max_val;
                
                catch err
                    disp(err)
                    error('please provide a correct attribute name domain/family/member/attribute ');
                end
            end
            
            
            % define default calibration to 1.
            
            
            if ~ (any(size(obj.calibration) == size(obj.get)) | any(size(obj.calibration) == [1 1]))
                error('wrong size of calibration array');
            end
           
            
            % get present value of variable
            v0 = obj.get;
            obj.value_at_init = v0;
                    
            % default absolute limits
            limits = p.Results.limits;
            
            if isempty(limits) % absolute empty limits
                limits(1) = obj.attribute.attrinfo.min_value;
                limits(2) = obj.attribute.attrinfo.max_value;
                % relatie limits
                if ~p.Results.absolute % relative empty limits
                    limits(1) = limits(1)./ obj.value_at_init;
                    limits(2) = limits(2)./ obj.value_at_init;
                end
            else % not empty limits
                if ~p.Results.absolute % relative 
                    limits(1) = limits(1).* obj.value_at_init;
                    limits(2) = limits(2).* obj.value_at_init;
                end
            end
            
            % Tango limits are the real maximum and minimum
            if limits(1)<obj.attribute.attrinfo.min_value
                limits(1)=obj.attribute.attrinfo.min_value;
            end
            if limits(2)>obj.attribute.attrinfo.max_value
                limits(2)=obj.attribute.attrinfo.min_value;
            end
            
            obj.min_val = limits(1); % absolute minimum
            obj.max_val = limits(2); % absolute maximum
            
            
            
            % set tolerance for set to end readvalue = setpoint
            if isempty(p.Results.set_tolerance)
                obj.set_tolerance = 1; %
            else
                obj.set_tolerance = p.Results.set_tolerance; %
            end
            
        end
        
        % read attribute value
        function v = get(obj)
            % read attribute value in calibrated units
            if ~isempty(obj.attr_name)
                try
                    v = obj.get_fun(obj.attr_name) .* obj.calibration; % this should be .SET?
                catch
                    disp([obj.attr_name ' not readable with ' func2str(obj.get_fun)]);
                    v = NaN;
                end
            else
                v = NaN;
            end
        end
        
        
        function set(obj,value,mode)
            % set attribute values in calibrated units, 
            % check for limits 
            %
            % mode may be:
            % 'wait'    (default) wait for set point to be reached.
            % 'slow'    sets the given attribute in 10 steps spaced by 
            %           pauses of 3s and waits for final set point to be 
            %           reached
            % 'delta'   sets the value provided as additional delta 
            %           compared to the existing value.
            % 'slowdelta' sets the given attribute in addition to the 
            %             existing value in 10 steps spaced by pauses of 3s
            %             and waits for final set point to be reached
            %
            
            if nargin<3
                mode='wait';
            end
            
            % limit attribute to input maximuma nd minimum. if any already
            % above or below, the limit is set to that value for that
            % specific attribute element.
            
            above = value>obj.max_val;
            if any(above)
                
                value(above) = max(obj.max_val,obj.value_at_init(above));
                warning(['Found ' num2str(length(find(above))) ' above maxium (' num2str(obj.max_val) '). Set to maximum or initial value if it was above maximum.'])
            end
            
            below = value<obj.min_val;
            if any(below)
                
                value(below) = min(obj.min_val,obj.value_at_init(below));
                warning(['Found ' num2str(length(find(below))) ' below minimum (' num2str(obj.min_val) '). Set to minimum or initial value if it was below minimum.'])
            end
            
            % apply calibration
            value_calib = value ./ obj.calibration;
           
            % store initial values for '*delta' and slow setting option
            v0 = obj.get;
            
            % set value to attribute
            wait_slow = 1; % pause in s to wait between 2 steps in 'slow' mode
            N_slow_steps = 10; % steps in slow mode
            
            switch mode
                case 'wait'
                    obj.set_fun(obj.attr_name, value_calib );
                case 'slow'
                    
                    % define steps
                    % steps = linspace(v0,value_calib,N_slow_steps);
                    step = (value_calib-v0)/N_slow_steps;
                    
                    % loop waiting 
                    for is = 1:N_slow_steps
                        obj.set_fun(obj.attr_name, v0 + step*is);
                        
                        pause(wait_slow);
                        disp(['setting ' obj.attr_name ' slowly ('...
                            num2str(N_slow_steps) ' steps, '...
                            num2str(wait_slow) 's each): '...
                            num2str(is/N_slow_steps*100) '%'])
                    end
                    
                case 'delta'
                
                    obj.set_fun(obj.attr_name, v0 + value_calib );

                case 'slowdelta'
                    
                    % define steps
                    step = (value_calib)/N_slow_steps;
                    
                    % loop waiting 
                    for is = 1:N_slow_steps
                        obj.set_fun(obj.attr_name, v0 + step*is);
                        
                        pause(wait_slow);
                        disp(['setting ' obj.attr_name ' slowly ('...
                            num2str(N_slow_steps) ' steps, '...
                            num2str(wait_slow) 's each): '...
                            num2str(is/N_slow_steps*100) '%'])
                    end
                    
                otherwise
                    obj.set_fun(obj.attr_name, value_calib );
            end
            
            % get delta or absolute values
            function v=switchget()
                switch mode
                    case {'wait','slow'}
                        
                        v=obj.get;
                        
                    case {'delta','slowdelta'}
                        v=obj.get-v0;
                        
                    otherwise % default absolute setting
                        v=obj.get;
                end
            end
            
            % wait for set point reached
            v = switchget;
            
            while any(abs(v-value) > obj.set_tolerance)
                if length(v)>1
                    distance = v-value;
                else
                    distance = std(v-value);
                end
                disp(['waiting for set point to be reached. distance: ' num2str(distance)])
                pause(0.1);
                v = switchget;
                
            end
            
        end
        
        function restore_initial_values(obj)
            %restore values stored at device init
            obj.set(obj.value_at_init);
        end
        
    end
end

