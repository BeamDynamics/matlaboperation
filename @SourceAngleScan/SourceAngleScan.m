classdef SourceAngleScan < handle %& GridScan
    %SourceAngleScan
    %
    % vary Hor. and Ver. position at sample in a grid of points to seek for optimum signal at beamlines.
    %
    %
    %see also:
    
    properties
        
        machine         % specify lattice if any peculiar settings are needed
        
        cell_number     % cell number
        
        SS_or_CC        % Straigth Section or Cell Center sources
        sslen           % [m] Straight Section length
        
        distance_source_sample  % distance of Xray detector from the e- source
        
        bpm_bumps       % list of 2 BPM indexes (1-320) to be used for bumps
        bpm_bumps_names % name of the bumps (computed in obj.getBPMs)
        
        h_cod_read      % function handle to read hor cod
        v_cod_read      % function handle to read ver cod
        
        h_cod_initial   % initial hor COD
        v_cod_initial   % initial ver COD
        
        % for reference orbit bumps
        reforbh_attr   % reference hor orbit tango attribute;
        reforbv_attr   % reference ver orbit tango attribute;
        reforbh0       % initial value of reference hor orbit (zero in general)
        reforbv0       % initial value of reference ver orbit (zero in general)
        autocor_dev    % tango device for autocor
        
        % for tango bumps
        hbumps         % device for all tango bumps (320, one for BPM)
        vbumps         % device for all tango bumps (320, one for BPM)
        hbumps0        % store initial bump setting h
        vbumps0        % store initial bump setting v
            
        % scan parameters
        H_range         % horizontal range to scan at Xray detector
        V_range         % vertical range to scan at Xray detector
        H_steps          % grid steps to do in the horizontal direction
        V_steps          % grid steps to do in the vertical direction
        
        grid_points % 2xN array of min and max Q_h
        grid_size   % 2x1 array , size of the tune scan grid
        measured_grid_points % grid points measured after moving the beam
        number_of_removed_points % points to be removed from grid
        
        measured_H  % current measured position or angle at selected ID
        measured_V  % current measured position or angle at selected ID
        
        position_or_angle % vary position or angle (default) at SS or CC to align beamline
        
        scan_name       % label for the present scan.
        scan_figure     %
        scan_automatic  % do not ask any questions. just go.
        
        angle_change_mode % string: 'tango_bumps' or 'RefOrbit' or ''
        wait_for_angle  % seconds to wait between tune settings.
        
        initial_pos_ang % initial measured position/angle (0,0)
        
        beamline_observable % tango adress of observable variable at beamline
        
    end
    
    
    methods
        
        % creator
        function obj = SourceAngleScan(machine,cell_number)
            %SourceAngleScan Construct an instance of this class
            %
            % for help type:
            % >> doc SourceAngleScan
            % 
            % Example to scan beam position at ID02:
            % >> sa = SourceAngleScan('ebs-simu',2)
            % >> sa.RunScan
            % 
            %see also: RingControl
            
            obj.machine=machine;
            
            try
                a = load('/operation/beamdyn/matlab/optics/sr/theory/betamodel.mat');
            catch
                warning('could not read /operation/beamdyn/matlab/optics/sr/theory/betamodel.mat ');
            end
            
            % default properties
            
            obj.position_or_angle = 'angle'; % default angle, but position also possible
            
            obj.angle_change_mode = 'tango_bumps'; % string: 'tango_bumps' or 'RefOrbit' or ''
            
            obj.scan_name  = ['Source_' obj.position_or_angle '_' obj.angle_change_mode '_Scan_' datestr(now,'YYYY_mm_dd_HH_MM')];      % label for the present scan.
            obj.scan_figure = figure('name',obj.scan_name);    %
            obj.scan_automatic = false;  % do not ask any questions. just go.
           
           
            % scan parameters
            obj.H_range  = [-3 3]; % [mm] horizontal range to scan at Xray detector
            obj.V_range  = [-3 3]; % [mm] vertical range to scan at Xray detector
            obj.H_steps   = 3 ;       % grid steps to do in the horizontal direction
            obj.V_steps   = 3 ;       % grid steps to do in the vertical direction
            
            obj.measured_H = NaN;
            obj.measured_V = NaN;
            
            obj.distance_source_sample = 27.7;  % [m] distance of Xray detector from the e- source
            
            obj.DefineGridPoints; % 2xN array of min and max Q_h
            
            obj.cell_number  = cell_number ;  % cell number, if NaN, ask to set
            
            obj.SS_or_CC     = 'SS';  % 'SS' or 'CC' Straigth Section or Cell Center sources
            obj.sslen =5; % [m] Straight Section length
            
            % close orbit
            obj.h_cod_read = @()tango.Attribute('srdiag/beam-position/all/SA_HPositions').value;
            obj.v_cod_read = @()tango.Attribute('srdiag/beam-position/all/SA_VPositions').value;
            
            obj.h_cod_initial = obj.h_cod_read();
            obj.v_cod_initial = obj.v_cod_read();
            
            % ref orbit autocor parameters
            obj.reforbh_attr = tango.Attribute('sr/beam-orbitcor/svd-h/RefOrbit');
            obj.reforbv_attr = tango.Attribute('sr/beam-orbitcor/svd-v/RefOrbit');
            obj.reforbh0 = obj.reforbh_attr.value;
            obj.reforbv0 = obj.reforbv_attr.value;
            
            obj.autocor_dev = tango.Device('sr/beam-orbitcor/svd-auto');
            
            % tango bump parameters
            obj.hbumps = tango.Attribute('sr/beam-bump/all-h/BumpAmplitudes');
            obj.vbumps = tango.Attribute('sr/beam-bump/all-v/BumpAmplitudes');
            obj.hbumps0 = obj.hbumps.value; % store initial setting h
            obj.vbumps0 = obj.vbumps.value; % store initial setting v
            
            obj.wait_for_angle = 20;  % seconds to wait between bump settings (if zero, ask to continue).
            
            
            obj.initial_pos_ang = obj.GetPostionOrAngle;
            
            % machine specific settings.
            switch obj.machine
                case 'sr'
                    
                case 'ebs-simu'
                    
                case 'sy'
                    
                    
                otherwise
                    error('SourceAngleScan is not supported for this machine')
                    
            end
            
        end
        
        function RestoreInitialAnglePosition(obj)
            % Restore bump values stored at instantiation of this class
            
            % % beam back to nominal bumps
            
            indbump = obj.bpm_bumps;
            
            % hor.
            bumpnames = [obj.hbumps.device.BumpNames.read(indbump),...
                obj.vbumps.device.BumpNames.read(indbump)];
           
            initbumps=[obj.hbumps0(indbump),obj.vbumps0(indbump)];
            
            for ib = 1:length(bumpnames)
                dev = tango.Device(bumpnames{ib});
                dev.Amplitude = initbumps(ib);
            end
            
            
        end
        
        
        function Reset(obj)
            % reset grid and measurement arrays for a new scan, returns to the initial WP
            
            % go back to initial WP.
            obj.measured_H = NaN;
            obj.measured_V = NaN;
            
            obj.cell_number  = NaN ;  % cell number, if NaN, ask to set
            
            obj.SS_or_CC     = 'SS';  % 'SS' or 'CC' Straigth Section or Cell Center sources
            
            obj.distance_source_sample = 30;  % [m] distance of Xray detector from the e- source
            
            obj.h_cod_initial = obj.h_cod_read();
            obj.v_cod_initial = obj.v_cod_read();
            
            switch obj.angle_change_mode
                case 'RefOrbit'
                    % restore reference orbit setting
                    obj.reforbh_attr.set = obj.reforbh0;
                    obj.reforbv_attr.set = obj.reforbv0;
                case 'tango_bumps'
                    % resotre initial bump amplitudes
                    obj.hbumps.set = obj.hbumps0; % store initial setting h
                    obj.vbumps.set = obj.vbumps0; % store initial setting v
                otherwise
            end
            
            obj.H_range  = [-3 3]; % [mm] horizontal range to scan at Xray detector
            obj.V_range  = [-3 3]; % [mm] vertical range to scan at Xray detector
            obj.H_steps   = 5 ;       % grid steps to do in the horizontal direction
            obj.V_steps   = 5 ;       % grid steps to do in the vertical direction
            
            obj.DefineGridPoints;
            
            obj.position_or_angle = 'angle'; % default angle, but position also possible
            
            obj.angle_change_mode = 'tango_bumps'; % string: 'tango_bumps' or 'RefOrbit' or ''
            obj.wait_for_angle = 20;  % seconds to wait between bump settings (if zero, ask to continue).
            
            % reset scan name
            obj.scan_name  = ['Source_' obj.position_or_angle '_' obj.angle_change_mode '_Scan_' datestr(now,'YYYY_mm_dd_HH_MM')];      % label for the present scan.
           
            % no automatic.
            obj.scan_automatic = false;
            
            % remove set bumps, if any left
            obj.RestoreInitialAnglePosition;
            
        end
        
        function obj = DefineGridPoints(obj)
            % define the grid of points to scan
           
            % define initial linear list
            switch obj.position_or_angle
                
                case 'position' %[milli m]
                    x=linspace(obj.H_range(1),obj.H_range(2),obj.H_steps);
                    y=linspace(obj.V_range(1),obj.V_range(2),obj.V_steps);
                    
                case 'angle' % [milli rad]
                    % compute angle at source :
                    % sin(theta) = H / source-sample distance
                    
                    x=linspace(obj.H_range(1)/obj.distance_source_sample,obj.H_range(2)/obj.distance_source_sample,obj.H_steps);
                    y=linspace(obj.V_range(1)/obj.distance_source_sample,obj.V_range(2)/obj.distance_source_sample,obj.V_steps);
                otherwise
                    error([obj.position_or_angle ' should be: ' '''position''' ' or ' '''angle''']);
                    
            end
            
            
            % 2D list.
            [qy,qx]=meshgrid(y,x);
            
            % sort zig-zag low vertical to high vertical tune, to minimize steps in tune
            qx(:,2:2:end)=qx(end:-1:1,2:2:end);
            
            % remove points to avoid (coupling resonance)
            sel = (1:numel(qx) );
            
            qx=qx(sel)';
            qy=qy(sel)';
            
            obj.number_of_removed_points = sum(sel==0);
            
            obj.grid_points = [qx qy];
            obj.grid_size = size(obj.grid_points);
            
            obj.measured_grid_points = NaN(obj.grid_size); % measured tune grid
            
            % intialize scan
            obj.beamline_observable      = NaN(obj.grid_size(1),1);    %
            
            obj.PlotGridPoints;
            
            disp([num2str(obj.grid_size(1)) ...
                ' points will be tested. if 10s are needed for each point, '...
                num2str(obj.grid_size(1)*10/60) ' minutes to wait']);
            
            % grid has been updated, uptade measurement name as well.
            obj.scan_name  = ['SourceScan_' datestr(now,'YYYY_mm_dd_HH_MM')];      % label for the present scan.
            obj.scan_figure = figure('name',obj.scan_name);    %
            
        end
        
        function PlotGridPoints(obj)
            % display the points that will be scanned. smallest the dot,
            % earlier it will appear in the scan.
            
            % display points to be scanned.
            try
                set(0,'CurrentFigure',obj.scan_figure);
            catch
                obj.scan_figure = figure('name',obj.scan_name);
            end
            %figure('name',['Points to scan: ' obj.scan_name ', dot size = order']);
            scatter(obj.grid_points(:,1),obj.grid_points(:,2),(1:obj.grid_size(1))*5,'LineWidth',2);
            hold on;
            plot(obj.grid_points(:,1),obj.grid_points(:,2),'--','LineWidth',2);
            plot(obj.measured_H, obj.measured_V,'rx','MarkerSize',10,'LineWidth',3);
            grid on;
            hold off;
            
            
            switch obj.position_or_angle
                case 'position'
                    xlim(obj.H_range + [-0.1 0.1]);
                    ylim(obj.V_range + [-0.1 0.1]);
                    xlabel('source \Delta x [mm]'); ylabel('source \Delta y [mm]');
                    legend('points','order','present position');
                case 'angle'
                    xlim(obj.H_range/obj.distance_source_sample + [-0.1 0.1]/obj.distance_source_sample);
                    ylim(obj.V_range/obj.distance_source_sample + [-0.1 0.1]/obj.distance_source_sample);
                    xlabel('source \Delta \theta_x [mrad]'); ylabel('source \Delta \theta_y [mrad]');
                    legend('points','order','present position');
                otherwise
                    error([obj.position_or_angle ' should be: ' '''position''' ' or ' '''angle''']);
            end
            
            % saveas(gca,['PointsToScanOrder' obj.scan_name '.fig']);
        end
        
        function PlotScan(obj,varargin)
            % display all the measurements taken up to now.
            %
            % Lifetime, H and V emittance and beam current. Blue is best.
            %
            % by default the spots are drawn at the measured values.
            % obj.PlotScan(false) to plot at the location specified by
            % obj.grid_points
            %
            % the empty black circles are the points to scan
            % the red square the present optimum overall
            % the green squares the optimum of each parameter
            %
            %
            
            if length(varargin)>=1
                measured = varargin{1};
            else
                measured = true;
            end
            
            % display points to be scanned.
            if ~isempty(obj.scan_figure)
                try
                    set(0,'CurrentFigure',obj.scan_figure);
                catch err
                    % figure has been deleted by user.
                    obj.scan_figure = figure('name',...
                        ['Source ' obj.position_or_angle...
                        ' scan :' obj.scan_name]);
                end
                
            else
                obj.scan_figure = figure('name',...
                    ['Source ' obj.position_or_angle...
                    ' scan :' obj.scan_name]);
            end
            
            % display position at sample
            
            qxs = obj.grid_points(:,1); %source position or angle
            qys = obj.grid_points(:,2);
                    
            % position at sample
            switch obj.position_or_angle
                case 'position'
                    qx = qxs;
                    qy = qys;
                case 'angle'
                    qx = qxs*obj.distance_source_sample;
                    qy = qys*obj.distance_source_sample;
                otherwise
                    error([obj.position_or_angle ' should be: ' '''position''' ' or ' '''angle''']);
            end
            
            
            qx0=qx; qy0=qy;
            qxs0=qxs; qys0=qys;
            
            extraborder = [-0.01 0.01];
            
            if measured & find(~isnan(obj.measured_grid_points))
                extraborderval = max([ ...
                    abs(min(obj.measured_grid_points(:,1)) - obj.H_range(1)),...
                    abs(min(obj.measured_grid_points(:,2)) - obj.V_range(1)),...
                    abs(max(obj.measured_grid_points(:,1)) - obj.H_range(2)),...
                    abs(max(obj.measured_grid_points(:,2)) - obj.V_range(2))...
                    ]);
                extraborder = [-extraborderval extraborderval];
            end
            
            if measured
                
                % find last measured:
                indlast = find(isnan(obj.measured_grid_points(:,1)),1,'first');
                
                if isempty(indlast)
                    indlast = obj.grid_size(1);
                end
                
                qxs = obj.measured_grid_points(:,1); %source position or angle
                qys = obj.measured_grid_points(:,2);
                
                % display position at sample
                switch obj.position_or_angle
                    case 'position'
                        qx(1:indlast) = qxs(1:indlast);
                        qy(1:indlast) = qys(1:indlast);
                    case 'angle'
                        qx(1:indlast) = qxs(1:indlast)*obj.distance_source_sample;
                        qy(1:indlast) = qys(1:indlast)*obj.distance_source_sample;
                        
                    otherwise
                        error([obj.position_or_angle ' should be: ' '''position''' ' or ' '''angle''']);
                end
            
                clf; % clear current figure;
            end
            
            markersize = 50;
            fontsize = 14 ;
            scatter(qx,qy,markersize,...
                obj.beamline_observable(:),'filled');     % color is lifetime
            
            %plot points to be yet scanned
            hold on;
            scatter(qx0,qy0,markersize,[0 0 0]);  %
            
            % plot present best lifetime point
            hold on;
            indbest=obj.BestPoint;
            if ~isnan(indbest)
                plot(qx(indbest),qy(indbest),'gs','MarkerSize',markersize/3);
                plot(qx(indbest),qy(indbest),'gs','MarkerSize',markersize/4);
            end
            
            axis equal;
            ax= gca;
            ax.FontSize = fontsize;
            ax.Units='normalized';
            
            xlabel('x [mm] @sample'); ylabel('y [mm] @sample');
            
            xlim(obj.H_range + extraborder);
            ylim(obj.V_range + extraborder);
            c=colorbar;
            switch obj.SS_or_CC
                case 'SS'
                    str = 'straigth section';
                case 'CC'
                    str = 'cell center';
            end
            title({[' photon beam position'],[str ' of Cell ' num2str(obj.cell_number)]});
            hold off;
            
            
            ax= gca;
            
            ax.XTick = unique(qx0)';
            ax.YTick = unique(qy0)';
            ax.TickDir = 'out';
            
            %set(ax,'Position',ax.Position + [0.1 0.1 -0.1 -0.1]);
            ax1_pos = ax.Position;
            
            ax2h = axes('units','normalized',...
                'Position', ax1_pos + [0+ax.TightInset(1) 0 -2*ax.TightInset(1) -ax1_pos(4)],...
                'FontSize',8,...
                'YColor','None',...
                'XAxisLocation','top',...
                'Color','None'...
                );
            %ch2 = colorbar;
            ax2h.TickDir = 'out';
            ax2h.XLim = ax.XLim;
            ax2h.XTick = ax.XTick;
            ax2h.XTickLabel=arrayfun(@num2str,unique(qxs0),'un',0);
            ax2h.XTickLabelRotation=0;
            ax2h.XLabel.Margin=1;
            
            switch obj.position_or_angle
                case 'position'
                    ax2h.XLabel.String = ['hor. position [mm] @source'];
                case 'angle'
                    ax2h.XLabel.String = ['\theta_h [mrad] @source'];
            end
            
            
            ax2v = axes('Position', ax1_pos + [+ax.TightInset(1) 0 -(ax1_pos(3)-0.3*ax.TightInset(1)) 0],...
                'units','normalized',...
                'XColor','None',...
                'FontSize',8,...
                'YAxisLocation','right',...
                'Color','None'...
                );
            %ax2v.PlotBoxAspectRatio(2) = ax.PlotBoxAspectRatio(2);
            ax2v.TickDir = 'out';
            
            ax2v.YLim = ax.YLim;
            ax2v.YTick = ax.YTick;
            ax2v.YTickLabel=arrayfun(@num2str,unique(qys0),'un',0);
            ax2v.YTickLabelRotation=90;
            ax2v.YLabel.Margin=1;
            
            
            switch obj.position_or_angle
                case 'position'
                    ax2v.YLabel.String = ['ver. position [mm] @source'];
                case 'angle'
                    ax2v.YLabel.String = ['\theta_v [mrad] @source'];
            end

            
            % saveas(gca,['Scan' obj.scan_name '.fig']);
        end
        
        
        function [Hbpms, Vbpms, bpm_ind]=getBPMs(obj)
            % read BPM up and down stream in [m]
            
            % read orbit compared to initial rederence
            h = obj.h_cod_read() - obj.h_cod_initial;
            v = obj.v_cod_read() - obj.v_cod_initial;
            
            sel = [1:320]; % BPM index as ordered in the lattice
            
            
            if isnan(obj.cell_number)% a cell number has been set
            
                error('No cell number defined. obj.cell_number = 1 (1:32)')
            
            elseif obj.cell_number>32 | obj.cell_number<1 
            
                error(' obj.cell_number must be integer in the range (1:32)')
            
            else
                
                disp(['getting BPMS for cell: ' num2str(obj.cell_number)]);
                cell_nums = [4:32, 1:3];
                indcel = find(cell_nums == obj.cell_number)-1;
             
                switch obj.SS_or_CC   % 'SS' or 'CC' Straigth Section or Cell Center sources
                    case 'SS'
                        
                        if indcel==0 % injection cell
                            bpm_ind = [320 1];  % cell number, if NaN, ask to set
                        else
                            bpm_ind = sel(indcel*10+[0 1]);  % cell number, if NaN, ask to set
                        end
                        
                    case 'CC'
                        bpm_ind = sel(indcel*10+[5 6]);  % cell number, if NaN, ask to set
                        
                    otherwise
                        error([obj.SS_or_CC ' should be: ' '''SS'' for stright section' ' or ' '''CC'' for Cell Center']);
                end
            end
            disp(['BPMs: ' num2str(bpm_ind)]);
            disp(['    : ' sr.bpmname(bpm_ind(1))]);
            disp(['    : ' sr.bpmname(bpm_ind(2))]);
                
            
            % return bpm reading up stream and downstream selected source
            Hbpms = h(bpm_ind);
            Vbpms = v(bpm_ind);
            
            obj.bpm_bumps =  bpm_ind;
            obj.bpm_bumps_names =  {sr.bpmname(bpm_ind(1)), sr.bpmname(bpm_ind(2))};
            
        end
        
        function val = GetPostionOrAngle(obj)
            % read present tunes, return absolute values adding [76 27]
            
            switch obj.SS_or_CC   % 'SS' or 'CC' Straigth Section or Cell Center sources
                case 'SS'
                    obj.sslen =5; % [m] Straight Section length
                case 'CC'
                    obj.sslen =1; % [m] distance between BPMS 5 and 6
                otherwise
                    error([obj.SS_or_CC ' should be: ' '''SS'' for stright section' ' or ' '''CC'' for Cell Center']);
            end
            
            % read BPMs up and down stream of source point
            [Hbpms, Vbpms]=obj.getBPMs;
            
            switch obj.position_or_angle
                case 'position'
                    obj.measured_H = sum(Hbpms)/2;
                    obj.measured_V = sum(Vbpms)/2;
                case 'angle'
                    obj.measured_H = diff(Hbpms)/obj.sslen;
                    obj.measured_V = diff(Vbpms)/obj.sslen;
                otherwise
                    error('!');
            end
            
            val = [obj.measured_H obj.measured_V]*1e3; % mm or mrad
            
        end
        
        function movebeam(obj,desired,initial)
            % function to change beam position
            
            present = obj.GetPostionOrAngle;
            count =1;
            
            % loop tune change untill it reaches the desired values.
            while any( abs(desired - present )>1e-5)  & count < 2
                
                switch obj.angle_change_mode
                    
                    case{'RefOrbit'}
                        
                        error('NEVER TESTED CODE BELOW')
                        
                        % disable -2 +2 BPMS around bump 
                        bpmall = tango.Device('srdiag/beam-position/all');
                        
                        for ii =1:2
                            bpmdevs = bpmall.GetBPMName(obj.bpm_bumps(ii));
                            tango.Device(bpmdevs).Disable();
                        end
                        
                        % set off autocor
                        disp('Switch OFF autocor');
                        obj.autocor_dev.Off();
                        
                        % define initial reference orbit
                        reforbh = obj.reforbh0;
                        reforbv = obj.reforbv0;
                        
                        disp('Change Reference Orbit');
                        switch obj.position_or_angle
                            case 'position'
                                
                                reforbh(obj.bpm_bumps(1) )= desired(1);
                                reforbh(obj.bpm_bumps(2) )= desired(1);
                                
                                reforbv(obj.bpm_bumps(1) )= desired(2);
                                reforbv(obj.bpm_bumps(2) )= desired(2);
                                
                            case 'angle'
                                
                                reforbh(obj.bpm_bumps(1) )= -desired(1)*obj.sslen/2;
                                reforbh(obj.bpm_bumps(2) )= desired(1)*obj.sslen/2;
                                
                                reforbv(obj.bpm_bumps(1) )= -desired(2)*obj.sslen/2;
                                reforbv(obj.bpm_bumps(2) )= desired(2)*obj.sslen/2;
                            
                            otherwise
                                
                        end
                        
                        % update reference orbit
                        obj.reforbh_attr.set = reforbh; % [mm]
                        obj.reforbv_attr.set = reforbv; % [mm]
                        
                        % run autocor
                        disp('Switch ON autocor with new reference orbit');
                        obj.autocor_dev.On();
                           
                        % enable -2 +2 BPMS around bump 
                        bpmall = tango.Device('srdiag/beam-position/all');
                        
                        for ii =1:2
                            bpmdevs = bpmall.GetBPMName(obj.bpm_bumps(ii));
                            tango.Device(bpmdevs).Enable();
                        end
                        
                        
                    case 'tango_bumps' % use tango bumps to move beam at BPMS
                        
                        % initialize vectorized bumps
                        h_bumps = obj.hbumps0;
                        v_bumps = obj.vbumps0;
                        
                        
                        disp('Set bumps');
                        switch obj.position_or_angle
                            case 'position'
                                
                                h_bumps(obj.bpm_bumps(1) )= desired(1);
                                h_bumps(obj.bpm_bumps(2) )= desired(1);
                                
                                v_bumps(obj.bpm_bumps(1) )= desired(2);
                                v_bumps(obj.bpm_bumps(2) )= desired(2);
                                
                            case 'angle'
                                
                                h_bumps(obj.bpm_bumps(1) )= -desired(1)*obj.sslen/2;
                                h_bumps(obj.bpm_bumps(2) )=  desired(1)*obj.sslen/2;
                                
                                v_bumps(obj.bpm_bumps(1) )= -desired(2)*obj.sslen/2;
                                v_bumps(obj.bpm_bumps(2) )=  desired(2)*obj.sslen/2;
                            
                            otherwise
                        end
                        
                        try
                            % update bump values
                            obj.hbumps.set = h_bumps*1e-3; % set bumps in [m]
                            obj.vbumps.set = v_bumps*1e-3; % set bumps in [m]
                            
                        catch
                            % update bump values using individual device servers
                            % (TEMPORARY WARK AROUND!)
                            warning('should be able to use vectoried bump setting');
                            bh=tango.Device('sr/beam-bump/all-h');
                            bhup = bh.BumpNames.read{obj.bpm_bumps(1)};
                            bhdo = bh.BumpNames.read{obj.bpm_bumps(2)};
                            bv=tango.Device('sr/beam-bump/all-v');
                            bvup = bv.BumpNames.read{obj.bpm_bumps(1)};
                            bvdo = bv.BumpNames.read{obj.bpm_bumps(2)};
                          
                            try
                                b = tango.Device(bhup);
                                b.Amplitude = h_bumps(obj.bpm_bumps(1) )*1e-3;
                                b = tango.Device(bhdo);
                                b.Amplitude = h_bumps(obj.bpm_bumps(2) )*1e-3;
                                b = tango.Device(bvup);
                                b.Amplitude = v_bumps(obj.bpm_bumps(1) )*1e-3;
                                b = tango.Device(bvdo);
                                b.Amplitude = v_bumps(obj.bpm_bumps(2) )*1e-3;
                            
                            catch err
                                
                                disp(err)
                                warning(' error in bump setting. probalby too large bump or timeout. keep going.')
                            
                            end
                        end
                        
                        
                    otherwise
                        error('angle_change_mode not correct')
                end
                
                % wait setting and measurement
                pause(obj.wait_for_angle);
                
                % update present tunes
                present = obj.GetPostionOrAngle;
                
                % check if beam is present, if not, exit.
                if ~ ( getTotCurrent() > 0.01 | ~any(isnan(present)) )
                    
                    error('no beam! or positions unavailable. stopping loop. restart when ready, the points already done will not be measured again')
                    
                end
                
                % disp(count)
                % disp(present_tune);
                % abs(desired_tune - present_tune )
                % any( abs(desired_tune - present_tune )>0.001)
                
                count = count +1;
            end
            
            % read tunes
            disp(['Initial: ' num2str(initial)]);
            disp(['Desired: ' num2str(desired)]);
            disp(['Obtained: ' num2str(obj.GetPostionOrAngle)]);
            
        end
        
        function check_grid(obj)
            % check if grid points are consistent ot something has been
            % modified.
            
            %check size
            if obj.H_steps * obj.V_steps - obj.number_of_removed_points ~= obj.grid_size(1)
                error('Inconsistent grid size. ts.DefineGridPoints or ts.Reset to redefine the grid and start a new measurement');
            end
            
            switch obj.position_or_angle
                case 'position'    
                    H_r = obj.H_range;
                    V_r = obj.V_range;
                case 'angle'
                    H_r = obj.H_range./obj.distance_source_sample ;
                    V_r = obj.V_range./obj.distance_source_sample ;
            end
            
            % check range
            if  H_r(1) ~= min(obj.grid_points(:,1))
                error('Inconsistent min h range. ts.DefineGridPoints or ts.Reset to redefine the grid and start a new measurement');
            end
            if H_r(2) ~= max(obj.grid_points(:,1))
                error('Inconsistent max h range. ts.DefineGridPoints or ts.Reset to redefine the grid and start a new measurement');
            end
            
            if V_r(1) ~= min(obj.grid_points(:,2))
                error('Inconsistent min v range. ts.DefineGridPoints or ts.Reset to redefine the grid and start a new measurement');
            end
            if V_r(2) ~= max(obj.grid_points(:,2))
                error('Inconsistent max v range. ts.DefineGridPoints or ts.Reset to redefine the grid and start a new measurement');
            end
            
        end
        
        
        function RunScan(obj)
            % runs the working point scan
            
            % here a call to obj.DefineGridPoints could make safer the use of
            % the code, however, omitting this call, allows to run the scan
            % in multipole times, stopping and restarting from the last unmeasured point.
           
            obj.check_grid;
            
            qx = obj.grid_points(:,1);
            qy = obj.grid_points(:,2);
            
            % start loop
            for ipos=1:length(qx)
                
                if any( isnan( obj.measured_grid_points(ipos,:) ) )
                    
                    % get present tunes.
                    
                    initposang =  obj.GetPostionOrAngle; % read tunes
                    disp(['Presently at: ' num2str(initposang) ': ' num2str((ipos-1)/obj.grid_size(1)*100,'%2.0f') '% completed' ]);
                    
                    % ask if point is to be measured or skipped
                    if obj.scan_automatic
                        measure_or_skip='y'; % contiunue always
                    else
                        measure_or_skip = input(['Go to: '...
                            num2str([qx(ipos),qy(ipos)]) ...
                            ' / skip and move to next point / stop now / continue without asking? [(y)es / (n)ext / (s)top / (a)uto ]'],'s');
                    end
                    
                    if measure_or_skip == 'a'
                        obj.scan_automatic = true;
                        disp('running in automatic mode.');
                        disp('To return to ask questions, Ctrl-C and restart scan (points measured are kept).')
                    end
                    
                    if measure_or_skip == 'n' % skipped point, all data will be NaN
                        
                        disp(['Skipped tune: ' num2str([qx(ipos),qy(ipos)]) ]);
                        obj.measured_grid_points(ipos,:) = [qx(ipos),qy(ipos)] ;
                        
                    else
                        
                        
                        
                        % change position
                        obj.movebeam([qx(ipos),qy(ipos)],initposang);
                        
                        if ~obj.scan_automatic
                            input('press to measure data at this point?','s');
                        end
                        
                        
                        
                        disp('measure relevant quantities');
                        
                        % get lifetime
                        obj.beamline_observable(ipos)=getTotCurrent(); % vac, averaging time,
                        
                        % % get White beam viewer 
                        % obj.beamline_observable(ipos)=getBeamlineObservable(); % vac, averaging time,
                        
                        
                        % update measured tune
                        [actual]= obj.GetPostionOrAngle;
                        
                        % get quadrupole currents
                        curhsteer(ipos,:)=tango.Attribute('srmag/hst/all/Strengths').value;
                        curvsteer(ipos,:)=tango.Attribute('srmag/vst/all/Strengths').value;
                                                
                        obj.measured_grid_points(ipos,1)= actual(1);
                        obj.measured_grid_points(ipos,2)= actual(2);
                        
                    end
                    
                    % save
                    save(['temp_' obj.scan_name '.mat'],'obj','curhsteer','curvsteer');
                    
                    % plot present scan status
                    obj.PlotScan;
                    
                else
                    
                    
                    disp([obj.position_or_angle ': '])
                    disp([qx(ipos),qy(ipos)])
                    disp('already measured');
                    
                end
                
                
            end
            
            
            % go back to initial WP.
            obj.movebeam(obj.initial_pos_ang,obj.GetPostionOrAngle)
            
            % save final data
            save(['Data_' obj.scan_name '.mat'],'obj');
            
            % plot figures with measured tunes.
            obj.PlotScan;
            
            saveas(gca,['SourceScan' obj.scan_name '.fig']);
            
        end
     
        function val = getBeamlineObservable(obj)
           % get beamline observable
           if obj.cell_number==20 && strcmp(obj.SS_or_CC , 'SS')
                val = tango.Attribute('tango://id20:20000/ID20/beamviewer/wbv1/MaxIntensity').value;
           end
           
           % id20:
           % TANGO_HOST       id20:20000
           % device                    id20/beamviewer/wbv1
           %
           % id26:
           % TANGO_HOST    id26:20000
           % device                id26/beamviewer/wbv1
           %
           % id28:          id28:20000
           % TANGO_HOST   archimedes
           % device               id28/beamviewer/wbv
           %
        end
        
        
        function CancelLastPoints(obj,n)
            % CANCELLASTPOINTS erases the data taken in the last n measurements.
            %
            %example: obj.CancelLastPoints(3);
            
            nmeas = find(isnan(obj.measured_grid_points(:,1)),1,'first');
            disp([num2str(nmeas) ' points where measured, removing last ' num2str(n)]);
            if isempty(nmeas)
                nmeas = obj.grid_size(1);
            end
            ind2cancel = nmeas:-1:(nmeas-n+1);
            
            obj.measured_grid_points(ind2cancel,:)=NaN;
            
            obj.beamline_observable(ind2cancel)=NaN; % vac, averaging time,
            
            nmeas = find(isnan(obj.measured_grid_points(:,1)),1,'first');
            disp([num2str(nmeas) ' valid points ']);
            
            obj.PlotScan(true); % plot measured W.P.
            
        end
        
        
        function ind_best=BestPoint(obj)
            % guess of best WP from max(TLT*Current/hemit/vemit)
            %
            
            val =  obj.beamline_observable ;
            
            [~,ind_best] = max(val(~isnan(val)));
            disp(['Best: ' num2str(obj.measured_grid_points(ind_best,:))]);
            
        end
        
        function OpenBumpsDevices(obj)
            % open atkpanel for bump devices corresponding to present scan
            indbump = obj.bpm_bumps;
            
            % hor.
            bumpnames = obj.hbumps.device.BumpNames.read;
            system(['atkpanel ' bumpnames{indbump(1)} ' &']);
            system(['atkpanel ' bumpnames{indbump(2)} ' &']);
            
            % ver.
            bumpnames = obj.vbumps.device.BumpNames.read;
            system(['atkpanel ' bumpnames{indbump(1)} ' &']);
            system(['atkpanel ' bumpnames{indbump(2)} ' &']);
            
        end
       
        % signature of methods defined in seprated functions below
        
        
    end
    
    methods (Static) % methods that do not need to know about this class, no input "obj"
        
    end
    
end

