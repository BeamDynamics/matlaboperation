function SendBumpQF3(amplitude)
%SENDBUMPQD2 Summary of this function goes here
%   
% amplitude is the vertical bump amplitude at the bpm in m
%
ch2=tango.Device('sy/ps-c5/ch2');
ch3=tango.Device('sy/ps-c5/ch3');
ch4=tango.Device('sy/ps-c5/ch4');

val0=[ch2.current.set; ch3.current.set; ch4.current.set];

val_bump=[25.079; 15.668; 25.005]*amplitude;

val_tot=val0+val_bump;

ch2.current=val_tot(1);
ch3.current=val_tot(2);
ch4.current=val_tot(3);

disp(['horizontal bump of ' num2str(amplitude) ' m at QF3 bpm is sent']);

end

