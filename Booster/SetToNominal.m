function SetToNominal()
%   set the tango devices for orbit correction in the booster to nominal
%   values for operation. To be used after many test with
%   SetSY_RespMat_RefOrb.
%
%   See also: SetSY_RespMat_RefOrb

name_h='/operation/beamdyn/matlab/optics/sy/theory/h_resp.csv';
name_v='/operation/beamdyn/matlab/optics/sy/theory/v_resp.csv';
name_hmot='/operation/beamdyn/matlab/optics/sy/theory/h_motresp.csv';
name_vmot='/operation/beamdyn/matlab/optics/sy/theory/v_motresp.csv';
name_do_h='/operation/beamdyn/matlab/optics/sy/theory/desorbit.csv';

dev_h=tango.Device('sy/beam-orbitcor/svd-steer-h');
dev_v=tango.Device('sy/beam-orbitcor/svd-steer-v');
dev_moth=tango.Device('sy/beam-orbitcor/svd-mot-h');
dev_motv=tango.Device('sy/beam-orbitcor/svd-mot-v');

dev_h.set_property('SVDFileName',name_h);
dev_v.set_property('SVDFileName',name_v);
dev_moth.set_property('SVDFileName',name_hmot);
dev_motv.set_property('SVDFileName',name_vmot);
dev_moth.set_property('DesignOrbitFileName',name_do_h);
dev_h.set_property('DesignOrbitFileName',name_do_h);

% dev_h.RefOrbit=zeros(1,75);
% dev_moth.RefOrbit=zeros(1,75);

dev_h.Init();
dev_v.Init();
dev_moth.Init();
dev_motv.Init();

end