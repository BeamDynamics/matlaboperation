function SendBumpQD2(amplitude)
%SENDBUMPQD2 Summary of this function goes here
%   
% amplitude is the vertical bump amplitude at the bpm in m
%

cv1=tango.Device('sy/ps-c6/cv1');
cv2=tango.Device('sy/ps-c6/cv2');
cv3=tango.Device('sy/ps-c6/cv3');

val0=[cv1.current.set; cv2.current.set; cv3.current.set];

val_bump=[25.844; 0.503; 21.839]*amplitude;

val_tot=val0+val_bump;

cv1.current=val_tot(1);
cv2.current=val_tot(2);
cv3.current=val_tot(3);

disp(['vertical bump of ' num2str(amplitude) ' m at QD2 bpm is sent']);

end

