function SendBumpQF26(amplitude)
%SENDBUMPQD2 Summary of this function goes here
%   
% amplitude is the vertical bump amplitude at the bpm in m
%

ch25=tango.Device('sy/ps-c1/ch25');
ch26=tango.Device('sy/ps-c2/ch26');
ch27=tango.Device('sy/ps-c2/ch27');

val0=[ch25.current.set; ch26.current.set; ch27.current.set];

val_bump=[25.034; 15.663; 25.048]*amplitude;

val_tot=val0+val_bump;

ch25.current=val_tot(1);
ch26.current=val_tot(2);
ch27.current=val_tot(3);

disp(['horizontal bump of ' num2str(amplitude) ' m at QF26 bpm is sent']);

end

