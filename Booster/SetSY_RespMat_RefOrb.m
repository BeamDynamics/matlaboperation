function SetSY_RespMat_RefOrb( ring, dfrf_Hz, DescriptionString )
% SetSY_RespMat_RefOrb 
%   The function computes the response matrix for the correctors and for
%   the movers of the ESRF booster and sets the tango devices to use those
%   new matrixes. The function also computes the reference orbit and sets the
%   attribute RefOrbit of the tango devices.
%   The function also sends an Init command to the tango devices.
%   Tango devices used are: sy/beam-orbitcor/svd-mot-h, sy/beam-orbitcor/svd-mot-v,
%                           sy/beam-orbitcor/svd-steer-h, sy/beam-orbitcor/svd-steer-v
%
%   ring is the esrf booster ring 
%         (it has to have an RF cavity with class 'RFCavity'!!!)
%   use getbooster(1) if h tune is 11.75
%   use getbooster(2) if h tune is 12.75
%   dfrf_Hz is the change in RF frequency in Hz (can be 0, +40000, ...)
%   
%   FileName is a string to be attached to the response matrixes names
%
%   Example:
%
%   SetSY_RespMat_RefOrb( getbooster(1), 20000, 'b_nux11_dfrf20kHz' )
%   or
%   SetSY_RespMat_RefOrb( getbooster(2), 40000, 'b_nux12_dfrf40kHz' )
%

%% remove radiation
ring=atradoff(ring,'','auto','auto');

%% Set cavity and find indexes
indquad=findcells(ring,'FamName','Q[DF]\w*');
% ring=setcellstruct(ring,'PassMethod',indquad,'StrMPoleSymplectic4Pass');
RF_Voltage=9e6; % change RF voltage if needed
ring=atsetcavity(ring,RF_Voltage,0,352);
indcav=findcells(ring,'Class','RFCavity');
f0=ring{indcav(1)}.Frequency;
for ii=1:length(indcav)
    ring{indcav}.Frequency=f0+dfrf_Hz;
end

indHcorr=findcells(ring,'FamName','CH\w*');
indVcorr=findcells(ring,'FamName','CV\w*');
ring=setcellstruct(ring,'PassMethod',[indHcorr,indVcorr],'CorrectorPass');
indbpm=findcells(ring,'FamName','BPM');
motQuadV = [4    30    48    60];
motQuadH = [5    19    23    31    43    57    67    75];
indhmot=indquad(motQuadH);
indvmot=indquad(motQuadV);
hmov={'QF04','QF10','QF17','QF22','QF26','QF30','QF37','QF39'};
vmov={'QD03','QD12','QD18','QD29'};

%% compute reference orbit
orb=findorbit6(ring,indbpm);
orbh=orb(1,:);
% orbv=orb(3,:);

%% correctors part
RMh=findrespmat(ring, indbpm, indHcorr, 0.00001, 'KickAngle', 1, 1, 'findorbit6');
RMv=findrespmat(ring, indbpm, indVcorr, 0.00001, 'KickAngle', 1, 2, 'findorbit6');

RMhh=RMh{1}/0.00001*0.003795870000000;  % load_corcalib('sy','h')
RMvv=RMv{3}/0.00001*0.003934690000000;  % load_corcalib('sy','v')
atmodel=sy.model(ring);
dct=4.e-4;
forb=findsyncorbit(ring,dct,indbpm);
h_fresp=-forb(1,:)'/dct.*atmodel.ll*atmodel.ll/352/2.997924e8;

RMhh=[RMhh,h_fresp];

%% movers part
rmmotv=zeros(75,4);
rmmoth=zeros(75,8);

% deltapos=0.0001;
for ii=1:length(indvmot)
    iqq=findcells(ring,'FamName',vmov{ii});
    bp=atsetshift(ring,iqq,0,200e-6);
    bm=atsetshift(ring,iqq,0,-200e-6);
    orbp=findorbit6(bp,indbpm);
    orbm=findorbit6(bm,indbpm);
    do=(orbp(3,:)-orbm(3,:))/.0004;
%     ring=atsetshift(ring,indvmot(ii),0,+1*deltapos);
%     rmmotv(:,ii)=(orbpos(3,:)-orbneg(3,:))/(2*deltapos);
    rmmotv(:,ii)=-do';
end

for ii=1:length(indhmot)
    iqq=findcells(ring,'FamName',hmov{ii});
    bp=atsetshift(ring,iqq,200e-6,0);
    bm=atsetshift(ring,iqq,-200e-6,0);
    orbp=findorbit6(bp,indbpm);
    orbm=findorbit6(bm,indbpm);
    do=(orbp(1,:)-orbm(1,:))/.0004;
    
%     ring2=atsetshift(ring,indhmot(ii),+1*deltapos,0);
%     orbpos=findorbit6(ring2,indbpm);
%     ring2=atsetshift(ring,indhmot(ii),-1*deltapos,0);
%     orbneg=findorbit6(ring2,indbpm);
%     ring=atsetshift(ring,indhmot(ii),+1*deltapos,0);
    rmmoth(:,ii)=-do';
end

rmmoth=[rmmoth, h_fresp];
path='/operation/beamdyn/matlab/optics/sy/theory';
%% save response matrixes
name_h=fullfile(path,['h_resp_' DescriptionString '.csv']);
name_v=fullfile(path,['v_resp_' DescriptionString '.csv']);
name_hmot=fullfile(path,['h_motresp_' DescriptionString '.csv']);
name_vmot=fullfile(path,['v_motresp_' DescriptionString '.csv']);

dlmwrite(name_h,RMhh);
dlmwrite(name_v,RMvv);
dlmwrite(name_hmot,rmmoth);
dlmwrite(name_vmot,rmmotv);

%% save design orbits
name_do_h=fullfile(path,['desorbit_' DescriptionString '.csv']);
dlmwrite(name_do_h,orbh');

dev_h=tango.Device('sy/beam-orbitcor/svd-steer-h');
dev_v=tango.Device('sy/beam-orbitcor/svd-steer-v');
dev_moth=tango.Device('sy/beam-orbitcor/svd-mot-h');
dev_motv=tango.Device('sy/beam-orbitcor/svd-mot-v');

dev_h.set_property('SVDFileName',name_h);
dev_v.set_property('SVDFileName',name_v);
dev_moth.set_property('SVDFileName',name_hmot);
dev_motv.set_property('SVDFileName',name_vmot);
dev_h.set_property('DesignOrbitFileName',name_do_h);
dev_moth.set_property('DesignOrbitFileName',name_do_h);

dev_h.Init();
dev_v.Init();
dev_moth.Init();
dev_motv.Init();

% dev_h.RefOrbit=orbh;
% dev_moth.RefOrbit=orbh;

end
