function beamlines_angle_bumps(varargin)
%function beamlines_angle_bumps(varargin)
% 
% This function SETS properties in the Control System
%  
% optional inputs
% ...,'cell_num',[1:32],...  cells where to modify bumps
% ...,'bpm',[1:10],...  bpms where to modify bumps
% ...,'bm',true/false(defualt),...   modify BM angle bumps
% ...,'id',true/false(defualt),...   modify ID angle and position bumps
% ...,'hor',true(defualt)/false,...  modify bumps in horizontal plane
% ...,'ver',true(defualt)/false,...  modify bumps in vertical plane
% ...,'horpos',true(defualt)/false,...  modify bumps in horizontal plane
% ...,'verpos',true(defualt)/false,...  modify bumps in vertical plane
% ...,'ask',true(defualt)/false,...  ask confirmation to modify control
%                                    system properties
% ...,'ring',[](defualt)/ATlattice,...  use a lattice different from the
%                                       one in the theory folder
% 
% 
%see also: 

p=inputParser;
validbpm = @(y)(any(not(arrayfun(@(x)(isnumeric(x) && (x>0) && (x<=10)),y,'un',1)))==false);
addOptional(p,'cell_num',[1:32],@isnumeric)
addOptional(p,'bpm',[],validbpm)
addOptional(p,'bm',false,@islogical)
addOptional(p,'id',false,@islogical)
addOptional(p,'hor',true,@islogical)
addOptional(p,'ver',true,@islogical)
addOptional(p,'horpos',true,@islogical)
addOptional(p,'verpos',true,@islogical)
addOptional(p,'ask',true,@islogical)
addOptional(p,'ring',[],@iscell)

parse(p,varargin{:})

cell_num = p.Results.cell_num;


if p.Results.ask
    
    answer = input(['SETTING bump properties to ' getenv('TANGO_HOST') ' . Type yes to continue '],'s');
     
    if ~strcmp(answer, 'yes')
        disp(' Exit, nothing done')
        return
    end
end
TF = {'No','Yes'};
disp(['setting bumps in cells: ' num2str(cell_num,'%.2d, ')])
disp(['setting id bumps: ' TF{1+p.Results.id}])
disp(['setting bm bumps: ' TF{1+p.Results.bm}])
disp(['setting bpm bumps: ' num2str(p.Results.bpm,'%.2d, ')])
disp(['setting hor bumps: ' TF{1+p.Results.hor}])
disp(['setting ver bumps: ' TF{1+p.Results.ver}])

if isempty(p.Results.ring)
    rc = atsetcavity(atradoff(sr.model('CheckLattice',false).ring,'','auto','auto'),6.5e6,0,992);
else
    rc = p.Results.ring;
end

filename_ID = 'bump_ID.txt';
filename_IDpos = 'bump_IDpos.txt';
filename_BM = 'bump_BM.txt';
filename_BPM = 'bump_BPM.txt';

filedata_ID = fopen(filename_ID,'w+');
filedata_IDp = fopen(filename_IDpos,'w+');
filedata_BM = fopen(filename_BM,'w+');
filedata_BPM = fopen(filename_BPM,'w+');

tic;
for idnum=cell_num
    
    if p.Results.id
        disp(['---- ID ' num2str(idnum,'%.2d') ' -----']);
        if p.Results.hor
            setpropbump('h','id',rc,idnum,filedata_ID);
        end
        if p.Results.ver
            setpropbump('v','id',rc,idnum,filedata_ID);
        end
        if p.Results.horpos
            setpropbump('h-pos','id',rc,idnum,filedata_IDp);
        end
        if p.Results.verpos
            setpropbump('v-pos','id',rc,idnum,filedata_IDp);
        end
    end
    
    if p.Results.bm
        disp(['---- BM ' num2str(idnum,'%.2d') ' -----']);
        if p.Results.hor
            setpropbump('h','bm',rc,idnum,filedata_BM);
        end
        if p.Results.ver
            setpropbump('v','bm',rc,idnum,filedata_BM);
        end
    end
    
    t1(idnum) = toc;
    
    if p.Results.bpm
        
        for bpmnum=p.Results.bpm
            
            disp(['---- BPM ' num2str(idnum,'%.2d') 'c' num2str(bpmnum,'%.2d') ' -----']);
            
            if p.Results.hor
                setpropbump_bpmpos('h',bpmnum,rc,idnum,filedata_BPM);
            end
            
            if p.Results.ver
                setpropbump_bpmpos('v',bpmnum,rc,idnum,filedata_BPM);
            end
        end
        
    end
    
    t2(idnum) = toc;
    
end

fclose(filedata_ID);
fclose(filedata_IDp);
fclose(filedata_BM);

end


function setpropbump(plane,beamline,rc,idnum,filedata)

devname = ['sr/beam-bump/' beamline num2str(idnum,'%.2d') '-' plane];


bump = [0 0];

switch plane
    
    case 'h'
        ang = [1e-4,0];
        switch beamline
            case 'id'
                ang = [1e-4,0];
                bump = [0,0];
                [~,sh,~,hnames,~,selb,h,~,bpmupstream,h_bump] = bump_ID(rc, idnum, bump,ang);
            case 'bm'
                ang = [0,0];
                bump = [1e-4,0];
                bpmnum = 6;
                [~,sh,~,hnames,~,selb,h,~,bpmupstream,h_bump] = bump_bpm(rc, idnum, bpmnum, bump,ang);
                %           [~,sh,~,hnames,~,selb,h,~,bpmupstream] = bump_BM(rc, idnum, bump,ang);
        end
    case 'v'
        switch beamline
            case 'id'
                ang = [0,1e-4];
                bump = [0,0];
                [~,~,sh,~,hnames,selb,~,h,bpmupstream,~,h_bump] = bump_ID(rc, idnum, bump,ang);
            case 'bm'
                ang = [0,0];
                bump = [0,1e-4];
                bpmnum = 6;
                [~,~,sh,~,hnames,selb,~,h,bpmupstream,~,h_bump] = bump_bpm(rc, idnum, bpmnum, bump,ang);
                %           [~,~,sh,~,hnames,selb,~,h,bpmupstream] = bump_BM(rc, idnum, bump,ang);
        end
    case 'h-pos'
        ang = [0,0];
        bump = [1e-4,0];
        switch beamline
            case 'id'
                [~,sh,~,hnames,~,selb,h,~,bpmupstream,h_bump,~] = bump_ID(rc, idnum, bump,ang);
            case 'bm'
                error('no bm h-pos bump');  %  [~,sh,~,hnames,~,selb,h,~,bpmupstream] = bump_BM(rc, idnum, bump,ang);
        end
    case 'v-pos'
        ang = [0,0];
        bump = [0,1e-4];
        switch beamline
            case 'id'
                [~,~,sh,~,hnames,selb,~,h,bpmupstream,~,h_bump] = bump_ID(rc, idnum, bump,ang);
            case 'bm'
                error('no bm v-pos bump'); % [~,~,sh,~,hnames,selb,~,h,bpmupstream] = bump_BM(rc, idnum, bump,ang);
        end
end


switch beamline
    case 'id'
        D = 5.3028;
        switch plane
            case {'h','v'}
                % normalized quantities to maximum bump amplitude
                shn = sh'./max(h);
                hn = h./max(h);
                W  = [-1/D 1/D];
            case {'h-pos','v-pos'}
                % normalized quantities to maximum bump amplitude
                shn = sh'./(h_bump);
                hn = h./h_bump;
                W  = [0.5 0.5];
        end
        
        if idnum==31 % potential minibeta cell
            
            indang = find(atgetcells(rc,'FamName',['ID\w*' num2str(idnum,'%.2d')]));
            indbpm(1)=find(atgetcells(rc,'Device',['srdiag/beam-position/c' num2str(idnum-1,'%.2d') '-' num2str(10,'%.2d') '\w*']),1,'first')';
            indbpm(2)=find(atgetcells(rc,'Device',['srdiag/beam-position/c' num2str(idnum,'%.2d') '-' num2str(1,'%.2d') '\w*']),1,'first')';
            
            % compute scaling factors from local optics
            [~,~,~,~,WH,WV,POS_COEFF_H,POS_COEFF_V]=ang_from_pos(rc,indang,indbpm,[0,0],[0,0]);
           
            switch plane
                case 'h'
                    W = WH;
                case 'v'
                    W = WV;
                case 'h-pos'
                    W=POS_COEFF_H;
                case 'v-pos'
                    W=POS_COEFF_V;
                    
            end
            
        end
        
        PosAngBpmInd = bpmupstream-1;
        
    case 'bm'
        % normalized quantities to maximum bump amplitude
        %shn = sh'./abs(h(bpmupstream-selb(1)+1))*2;
        %hn = h./abs(h(bpmupstream-selb(1)+1))*2;
        shn = sh'./max(h);
        hn = h./max(h);
        
        switch plane
            case 'h'
                D =  0.9120 * 1.224;
            case 'v'
                D =  0.9120 * 0.8022;
        end
        
        % Pos V =  0.8060   *V_BPM_6 + 0.0990   *V_BPM_7
        % Pos H = 1.3612   *H_BPM_6 + 0.1552    *H_BPM_7
        indang = find(atgetcells(rc,'FamName',['\w*-BM' num2str(idnum,'%.2d')]));
        
        if ~isempty(indang)
            indbpm(1)=find(atgetcells(rc,'Device',['srdiag/beam-position/c' num2str(idnum,'%.2d') '-' num2str(bpmnum,'%.2d') '\w*']),1,'first')';
            indbpm(2)=find(atgetcells(rc,'Device',['srdiag/beam-position/c' num2str(idnum,'%.2d') '-' num2str(bpmnum+1,'%.2d') '\w*']),1,'first')';
            
            % compute scaling factors from local optics
            [~,~,~,~,WH,WV]=ang_from_pos(rc,indang,indbpm,[0,0],[0,0]);
        else % if no BM, use defaults
            WH = [1.8060  0.7759];
            WV = [-0.9696 0.49478];
        end
        
        switch plane
            case {'h'}
                % Ang H = 1.8060   *H_BPM_6 + 0.7759    *H_BPM_7
                W = WH;%[1.8060  0.7759];
            case {'v'}
                % Ang V = -0.9696   *V_BPM_6 + 0.49478 *V_BPM_7
                W = WV;%[-0.9696 0.49478];
        end
        
        PosAngBpmInd = bpmupstream;
        
end

if PosAngBpmInd == -1
    PosAngBpmInd=319
end

if idnum == 4
    b1 = hn(PosAngBpmInd -(selb(1)-1) +1);
    b2 = hn(PosAngBpmInd -(selb(1)-1) +2);
    
    switch plane
        case 'h-pos'
            W = [1/b1/2 1/b2/2];
        case 'v-pos'
            W = [1/b1/2 1/b2/2];
    end
end
                
% normalized quantities to maximum bump amplitude
switch plane
    case {'v-pos'}
        plane = 'v';
    case {'h-pos'}
        plane = 'h';
end

fullnames= cellfun(@(a)(['srmag/' plane 'st-' a '']),hnames,'un',0);


try
    
    TangoHost = ['tango://' getenv('TANGO_HOST')];
    dev = tango.Device([TangoHost '/' devname]);
    
    dev.set_property('ActuatorCoeff',arrayfun(@(a)num2str(a,'%4.7f'),shn,'un',0));
    dev.set_property('ActuatorDevNames',fullnames');
    dev.set_property('BpmCoeff',arrayfun(@(a)num2str(a,'%4.7f'),hn,'un',0));
    dev.set_property('BpmInd',num2str(selb(1)-1,'%2.2d')); % index of first bpm in BPM coeff
    
    dev.set_property('PosAngBpmInd',num2str(PosAngBpmInd));
    %dev.set_property('Distance',num2str(D,'%2.6f'));
    dev.set_property('Weight',arrayfun(@(a)num2str(a,'%4.7f'),W,'un',0));
    
    dev.Init();
    
catch err
    
    disp(err);
    disp([devname ' does not exist']);
    
end

if length(fullnames)==3
    fullnames{4} = '';
    shn(4) = NaN;
    
end

str = [...
    devname ...
    ' ' fullnames{1} ... steerer name
    ' ' num2str(shn(1)) ... steerer force / bump amplitude
    ' ' fullnames{2} ...steerer name
    ' ' num2str(shn(2)) ... steerer force / bump amplitude
    ' ' fullnames{3} ...steerer name
    ' ' num2str(shn(3)) ... steerer force / bump amplitude
    ' ' fullnames{4} ...steerer name
    ' ' num2str(shn(4)) ... steerer force / bump amplitude
    ' ' num2str(selb(1)-1) ... first BPM within bump [0-319]
    ' ' num2str(length(selb)) ... # of BPM involved
    ' ' num2str(hn) ...  amplitude at bpm / bump amplitude
    ' ' num2str(bpmupstream) ...  downstream BPM
    ' ' num2str(D) ...  Distance
    ''];


fprintf(filedata, '%s\n', str);

end


function setpropbump_bpmpos(plane,bpmnum,rc,idnum,filedata)

devname = ['sr/beam-bump/c' num2str(idnum,'%.2d') '-' plane num2str(bpmnum,'%.2d')];

CC = find(idnum==[4:32,1:3])-1;
bpmidx = CC*10 + bpmnum;


ang = [0 0];

switch plane
    
    case 'h'
        bump = [1e-4,0];
        
        [~,sh,~,hnames,~,selb,h,~,bpmupstream] = bump_bpm(rc, idnum, bpmnum, bump,ang);
    case 'v'
        bump = [0,1e-4];
        
        [~,~,sh,~,hnames,selb,~,h,bpmupstream] = bump_bpm(rc, idnum, bpmnum, bump,ang);
        
end

W = [1 0];

% normalized quantities to maximum bump amplitude

[~,ind]=min(abs(h-1e-4));
shn = sh'./h(ind);
hn = h./h(ind);


fullnames= cellfun(@(a)(['srmag/' plane 'st-' a '']),hnames,'un',0);

try
    TangoHost = ['tango://' getenv('TANGO_HOST')];
    dev = tango.Device([TangoHost '/' devname]);
    
    dev.set_property('ActuatorCoeff',arrayfun(@(a)num2str(a,'%4.4f'),shn,'un',0));
    dev.set_property('ActuatorDevNames',fullnames');
    dev.set_property('BpmCoeff',arrayfun(@(a)num2str(a,'%4.4f'),hn,'un',0));
    dev.set_property('BpmInd',num2str(selb(1)-1,'%2.2d'));
    
    dev.set_property('PosAngBpmInd',num2str(selb(ind)-1));
    dev.set_property('Weight',arrayfun(@(a)num2str(a,'%4.4f'),W,'un',0));
    
    dev.Init();
    
    
catch err
    
    disp(err);
    disp([devname ' does not exist']);
    
end

str = [...
    devname ...
    ' ' fullnames{1} ... steerer name
    ' ' num2str(shn(1)) ... steerer force / bump amplitude
    ' ' fullnames{2} ...steerer name
    ' ' num2str(shn(2)) ... steerer force / bump amplitude
    ' ' fullnames{3} ...steerer name
    ' ' num2str(shn(3)) ... steerer force / bump amplitude
    ' ' num2str(selb(1)-1) ... first BPM within bump [0-319]
    ' ' num2str(length(selb)) ... # of BPM involved
    ' ' num2str(hn) ...  amplitude at bpm / bump amplitude
    ' ' num2str(bpmupstream) ...  downstream BPM
    ' ' num2str(W) ...  Distance
    ''];

disp(str)

fprintf(filedata, '%s\n', str);

end



function [rbump,sh,sv,hnames,vnames,selb,h,v,bpmupstream,h_bump,v_bump] = bump_ID(rc, idnum, bump,ang)

bumph = bump(1);
bumpv = bump(2);
angh = ang(1);
angv = ang(2);
inCOD = zeros(6,1);


%modpinhole = sr.model.addpinholes(rc.rmodel);
% indpinholes=find(atgetcells(modpinhole,'FamName','PINHOLE\w*'))';
ring0 = atsetcavity(atradoff(rc,'CavityPass','auto','auto'),6.5e6,0,992);
indBPMbump=find(atgetcells(ring0,'FamName',['ID' num2str(idnum,'%.2d') '\w*']),1,'first')';

indBPM = find(atgetcells(ring0,'Class','Monitor'))';
indHst = find(atgetcells(ring0,'FamName','S[FDHIJ]\w*'))';
indVst = find(atgetcells(ring0,'FamName','S[FDHIJ]\w*'))';

% selh = [8,9,10,11];
% selv = [8,9,10,11];

    function s = circsel(v,st,en)
        
        N=length(v);
        
        if en>N
            en = mod(en,N);
        end
        
        if en<st
            en =  N + en;
            v = [v,v];
        end
        
        
        s = v(st:en);
        
    end


% selh = [find(indHst<indBPMbump,2,'last'),find(indHst>indBPMbump,2,'first')];

bbef = find(indHst<indBPMbump,2,'last');
baft = find(indHst>indBPMbump,2,'first');

if isempty(baft)
    baft = 1:2;
end

if isempty(bbef)
    bbef = 288-[1,0];
end



selh = circsel([1:288],bbef(1),baft(end));

% selv = [find(indVst<indBPMbump,2,'last'),find(indVst>indBPMbump,2,'first')];

bbef = find(indHst<indBPMbump,2,'last');
baft = find(indHst>indBPMbump,2,'first');

if isempty(baft)
    baft = 1:2;
end

if isempty(bbef)
    bbef = 288-[1,0];
end


selv = circsel([1:288],bbef(1),baft(end));



selb = [find(indBPM<indBPMbump,4,'last'),find(indBPM>indBPMbump,4,'first')];

bbef = find(indBPM<indBPMbump,4,'last');
baft = find(indBPM>indBPMbump,4,'first');

if isempty(baft)
    baft = 1:4;
end

if isempty(bbef)
    bbef = 320-[3,2,1,0];
end


selb = circsel([1:320],bbef(1),baft(end));

bpmupstream = baft(1)-1;

indHCor = indHst(selh);
hnames = sr.steername(selh');
indVCor = indVst(selv);
vnames = sr.steername(selv');

if idnum==4
    
    hmark = circsel([1:length(ring0)],indHCor(end),indHCor(end)+30);
    vmark = circsel([1:length(ring0)],indVCor(end),indVCor(end)+30);
else
    
    hmark = circsel([1:length(ring0)],indHCor(end),indHCor(end)+20);
    vmark = circsel([1:length(ring0)],indVCor(end),indVCor(end)+20);
end


h1=atVariableBuilder(ring0,indHCor(1),{'KickAngle',{1,1}});
h2=atVariableBuilder(ring0,indHCor(2),{'KickAngle',{1,1}});
h3=atVariableBuilder(ring0,indHCor(3),{'KickAngle',{1,1}});
h4=atVariableBuilder(ring0,indHCor(4),{'KickAngle',{1,1}});
%h5=atVariableBuilder(ring0,indHCor(5),{'KickAngle',{1,1}});
VariabH=[h1 h2 h3 h4];
v1=atVariableBuilder(ring0,indVCor(1),{'KickAngle',{1,2}});
v2=atVariableBuilder(ring0,indVCor(2),{'KickAngle',{1,2}});
v3=atVariableBuilder(ring0,indVCor(3),{'KickAngle',{1,2}});
v4=atVariableBuilder(ring0,indVCor(4),{'KickAngle',{1,2}});
%h5=atVariableBuilder(ring0,indHCor(5),{'KickAngle',{1,1}});
VariabV=[v1 v2 v3 v4];

% 6D orbit


LinConstr1H=atlinconstraint(...
    indBPMbump,...
    {{'ClosedOrbit',{1}},{'ClosedOrbit',{2}}},...
    [bumph,angh],...
    [bumph,angh],...
    [1 1]);

LinConstr2H=atlinconstraint(...
    hmark(3),...
    {{'ClosedOrbit',{1}},{'ClosedOrbit',{2}}},...
    [0,0],...
    [0,0],...
    [1 1]);


LinConstr3H=atlinconstraint(...
    hmark(end),...
    {{'ClosedOrbit',{1}},{'ClosedOrbit',{2}}},...
    [0,0],...
    [0,0],...
    [1 1]);



LinConstr1V=atlinconstraint(...
    indBPMbump,...
    {{'ClosedOrbit',{3}},{'ClosedOrbit',{4}}},...
    [bumpv,angv],...
    [bumpv,angv],...
    [1 1]);

LinConstr2V=atlinconstraint(...
    vmark(3),...
    {{'ClosedOrbit',{3}},{'ClosedOrbit',{4}}},...
    [0,0],...
    [0,0],...
    [1 1]);


LinConstr3V=atlinconstraint(...
    vmark(end),...
    {{'ClosedOrbit',{3}},{'ClosedOrbit',{4}}},...
    [0,0],...
    [0,0],...
    [1 1]);


rbump=ring0;

if bump(1)==0 && ang(1) ==0  % only V
    rbump=atmatch(rbump,VariabV,[LinConstr1V LinConstr3V],10^-15,20,3,@lsqnonlin);%,'fminsearch');%
    
elseif bump(2)==0 && ang(2) ==0  % only H
    rbump=atmatch(rbump,VariabH,[LinConstr1H LinConstr3H],10^-10,20,3,@lsqnonlin);%,'fminsearch');%
    
else
    %rbump=atmatch(rbump,VariabH,[LinConstr1 LinConstr3],10^-14,10,3,@fminsearch);%,'fminsearch');%
    rbump=atmatch(rbump,VariabH,[LinConstr1H LinConstr3H],10^-10,20,3,@lsqnonlin);%,'fminsearch');%
    rbump=atmatch(rbump,VariabV,[LinConstr1V LinConstr3V],10^-10,20,3,@lsqnonlin);%,'fminsearch');%
    
end

sh = atgetfieldvalues(rbump, indHCor,'KickAngle',{1,1});
sv = atgetfieldvalues(rbump, indVCor,'KickAngle',{1,2});

% indHCoraft = rc.indHst(24+[10 11 14 15]);
% rbump = atsetfieldvalues(rbump, indHCoraft,'KickAngle',{1,1},-sh);
disp('SH')
disp(hnames)
disp(sh*1e6)
disp('SV')
disp(vnames)
disp(sv*1e6)

o = findorbit4(rbump,0,indBPM);
h = o(1,selb);
v = o(3,selb);

o = findorbit4(rbump,0,indBPMbump);
h_bump = o(1,1);
v_bump = o(3,1);

%figure; atplot(rbump,[18,38],@plClosedOrbit)


end





function [rbump,sh,sv,hnames,vnames,selb,h,v,bpmupstream,h_bump,v_bump] = bump_BM(rc, idnum, bump,ang)

bumph = bump(1);
bumpv = bump(2);
angh = ang(1);
angv = ang(2);
inCOD = zeros(6,1);


%modpinhole = sr.model.addpinholes(rc.rmodel);
% indpinholes=find(atgetcells(modpinhole,'FamName','PINHOLE\w*'))';
ring0 = atsetcavity(atradoff(rc,'CavityPass','auto','auto'),6.5e6,0,992);
indBPMbumpb=find(atgetcells(ring0,'Device',['srmag/m-qf8/c' num2str(idnum,'%.2d') '-b\w*']),1,'first')';
indBPMbump=find(atgetcells(ring0,'Device',['srdiag/beam-position/c' num2str(idnum,'%.2d') '-06\w*']),1,'first')';

indBPM = find(atgetcells(ring0,'Class','Monitor'))';
indHst = find(atgetcells(ring0,'FamName','S[FDHIJ]\w*'))';
indVst = find(atgetcells(ring0,'FamName','S[FDHIJ]\w*'))';

% selh = [8,9,10,11];
% selv = [8,9,10,11];

selh = unique([find(indHst<=indBPMbumpb,2,'last'),find(indHst>=indBPMbump,2,'first')]);
selv = unique([find(indVst<=indBPMbumpb,2,'last'),find(indVst>=indBPMbump,2,'first')]);

selb = unique([find(indBPM<=indBPMbump,5,'last'),find(indBPM>=indBPMbump,5,'first')]);
bpmupstream = selb(5)-1;

indHCor = indHst(selh);
hnames = sr.steername(selh');
indVCor = indVst(selv);
vnames = sr.steername(selv');

h1=atVariableBuilder(ring0,indHCor(1),{'KickAngle',{1,1}});
h2=atVariableBuilder(ring0,indHCor(2),{'KickAngle',{1,1}});
h3=atVariableBuilder(ring0,indHCor(3),{'KickAngle',{1,1}});
h4=atVariableBuilder(ring0,indHCor(4),{'KickAngle',{1,1}});
%h5=atVariableBuilder(ring0,indHCor(5),{'KickAngle',{1,1}});
VariabH=[h1 h2 h3 h4];
v1=atVariableBuilder(ring0,indVCor(1),{'KickAngle',{1,2}});
v2=atVariableBuilder(ring0,indVCor(2),{'KickAngle',{1,2}});
v3=atVariableBuilder(ring0,indVCor(3),{'KickAngle',{1,2}});
v4=atVariableBuilder(ring0,indVCor(4),{'KickAngle',{1,2}});
%h5=atVariableBuilder(ring0,indHCor(5),{'KickAngle',{1,1}});
VariabV=[v1 v2 v3 v4];

% 6D orbit


LinConstr1H=atlinconstraint(...
    indBPMbump,...
    {{'ClosedOrbit',{1}},{'ClosedOrbit',{2}}},...
    [bumph,angh],...
    [bumph,angh],...
    [1e6 1]);

LinConstr2H=atlinconstraint(...
    indHCor(end)+2,...
    {{'ClosedOrbit',{1}},{'ClosedOrbit',{2}}},...
    [0,0],...
    [0,0],...
    [1 1]);


LinConstr3H=atlinconstraint(...
    indHCor(end)+20,...
    {{'ClosedOrbit',{1}},{'ClosedOrbit',{2}}},...
    [0,0],...
    [0,0],...
    [1 1]);



LinConstr1V=atlinconstraint(...
    indBPMbump,...
    {{'ClosedOrbit',{3}},{'ClosedOrbit',{4}}},...
    [bumpv,angv],...
    [bumpv,angv],...
    [1e6 1]);

LinConstr2V=atlinconstraint(...
    indVCor(end)+2,...
    {{'ClosedOrbit',{3}},{'ClosedOrbit',{4}}},...
    [0,0],...
    [0,0],...
    [1 1]);


LinConstr3V=atlinconstraint(...
    indVCor(end)+20,...
    {{'ClosedOrbit',{3}},{'ClosedOrbit',{4}}},...
    [0,0],...
    [0,0],...
    [1 1]);


rbump=ring0;


if bump(1)==0 && ang(1) ==0  % only V
    rbump=atmatch(rbump,VariabV,[LinConstr1V LinConstr3V],10^-14,20,3,@lsqnonlin);%,'fminsearch');%
    
elseif bump(2)==0 && ang(2) ==0  % only H
    rbump=atmatch(rbump,VariabH,[LinConstr1H LinConstr3H],10^-14,20,3,@lsqnonlin);%,'fminsearch');%
    
else
    %rbump=atmatch(rbump,VariabH,[LinConstr1 LinConstr3],10^-14,10,3,@fminsearch);%,'fminsearch');%
    rbump=atmatch(rbump,VariabH,[LinConstr1H LinConstr3H],10^-14,20,3,@lsqnonlin);%,'fminsearch');%
    rbump=atmatch(rbump,VariabV,[LinConstr1V LinConstr3V],10^-14,20,3,@lsqnonlin);%,'fminsearch');%
    
end

sh = atgetfieldvalues(rbump, indHCor,'KickAngle',{1,1});
sv = atgetfieldvalues(rbump, indVCor,'KickAngle',{1,2});

% indHCoraft = rc.indHst(24+[10 11 14 15]);
% rbump = atsetfieldvalues(rbump, indHCoraft,'KickAngle',{1,1},-sh);
disp('SH')
disp(hnames)
disp(sh*1e6)
disp('SV')
disp(vnames)
disp(sv*1e6)

o = findorbit4(rbump,0,indBPM);
h = o(1,selb);
v = o(3,selb);


o = findorbit4(rbump,0,indBPMbump);
h_bump = o(1,1);
v_bump = o(3,1);


%figure; atplot(rbump,[18,38],@plClosedOrbit)


end



function [rbump,sh,sv,hnames,vnames,selb,h,v,bpmupstream,h_bump,v_bump] = bump_bpm(rc, idnum, bpmnum, bump,ang)

bumph = bump(1);
bumpv = bump(2);
angh = ang(1);
angv = ang(2);
inCOD = zeros(6,1);


%modpinhole = sr.model.addpinholes(rc.rmodel);
% indpinholes=find(atgetcells(modpinhole,'FamName','PINHOLE\w*'))';
ring0 = atsetcavity(atradoff(rc,'CavityPass','auto','auto'),6.5e6,0,992);
% indBPMbump=find(atgetcells(ring0,'FamName',['ID' num2str(idnum,'%.2d') '\w*']),1,'first')';
indBPMbump=find(atgetcells(ring0,'Device',['srdiag/beam-position/c' num2str(idnum,'%.2d') '-' num2str(bpmnum,'%.2d') '\w*']),1,'first')';

indBPM = find(atgetcells(ring0,'Class','Monitor'))';
indHst = find(atgetcells(ring0,'FamName','S[FDHIJ]\w*'))';
indVst = find(atgetcells(ring0,'FamName','S[FDHIJ]\w*'))';

% selh = [8,9,10,11];
% selv = [8,9,10,11];
CC = find(idnum==[4:32,1:3])-1;
bpmidx = CC*10 + bpmnum;

H_bpm_cor=[...
    0 1 3;... 1
    1 2 4;... 2
    1 3 5;... 3
    4 5 6;... 4
    3 5 7;... 5
    3 5 7;... 6
    4 5 6;... 7
    5 7 9;... 8
    6 8 9;... 9
    8 9 10;... 10
    ];

V_bpm_cor=[...
    0 1 4;... 1
    1 2 4;... 2
    1 3 5;... 3
    4 5 6;... 4
    4 5 6;... 5
    4 5 6;... 6
    4 5 6;... 7
    5 7 9;... 8
    6 8 9;... 9
    7 9 10;... 10
    ];


cellnum=repmat([1:32],10,1);
cellnum=cellnum(:);

cor_for_bump_ind = mod(1:length(indBPM),10);
cor_for_bump_ind(cor_for_bump_ind==0)=10;

AllHCor=H_bpm_cor(cor_for_bump_ind,:) + repmat((cellnum-1)*9,1,3);
AllVCor=V_bpm_cor(cor_for_bump_ind,:) + repmat((cellnum-1)*9,1,3);
AllHCor(AllHCor==0)=288;
AllHCor(AllHCor==289)=1;
AllVCor(AllVCor==0)=288;
AllVCor(AllVCor==289)=1;



hbef = find(indHst<indBPMbump,2,'last');
haft = find(indHst>indBPMbump,2,'first');

selh = AllHCor(bpmidx,:);

vbef = find(indVst<indBPMbump,2,'last');
vaft = find(indVst>indBPMbump,2,'first');

selv = AllVCor(bpmidx,:);


indHCor = indHst(selh);
hnames = sr.steername(selh');
indVCor = indVst(selv);
vnames = sr.steername(selv');

bbef = find(indBPM<indHCor(1),4,'last');
baft = find(indBPM>indHCor(end),4,'first');

if isempty(baft)
    baft = 1:4;
end

if isempty(bbef)
    bbef = 320-[3,2,1,0];
end

    function s = circsel(v,st,en)
        
        N=length(v);
        
        if en>N
            en = mod(en,N);
        end
        
        if en<st
            en =  N + en;
            v = [v,v];
        end
        
        
        s = v(st:en);
        
    end


selb = circsel([1:320],bbef(1),baft(end));

hmark = circsel([1:length(ring0)],indHCor(end),indHCor(end)+20);
vmark = circsel([1:length(ring0)],indVCor(end),indVCor(end)+20);

bpmupstream = bpmidx-1;


h1=atVariableBuilder(ring0,indHCor(1),{'KickAngle',{1,1}});
h2=atVariableBuilder(ring0,indHCor(2),{'KickAngle',{1,1}});
h3=atVariableBuilder(ring0,indHCor(3),{'KickAngle',{1,1}});
%h4=atVariableBuilder(ring0,indHCor(4),{'KickAngle',{1,1}});
%h5=atVariableBuilder(ring0,indHCor(5),{'KickAngle',{1,1}});
VariabH=[h1 h2 h3];
v1=atVariableBuilder(ring0,indVCor(1),{'KickAngle',{1,2}});
v2=atVariableBuilder(ring0,indVCor(2),{'KickAngle',{1,2}});
v3=atVariableBuilder(ring0,indVCor(3),{'KickAngle',{1,2}});
%v4=atVariableBuilder(ring0,indVCor(4),{'KickAngle',{1,2}});
%h5=atVariableBuilder(ring0,indHCor(5),{'KickAngle',{1,1}});
VariabV=[v1 v2 v3];

% 6D orbit

Wh =[1 1];

if angh==0
    Wh = [1 1e6];
end
if bumph==0
    Wh = [1e6 1];
end


Wv =[1 1];

if angv==0
    Wv = [1 1e6];
end
if bumpv==0
    Wv = [1e6 1];
end

LinConstr1H=atlinconstraint(...
    indBPMbump,...
    {{'ClosedOrbit',{1}},{'ClosedOrbit',{2}}},...
    [bumph,angh],...
    [bumph,angh],...
    Wh);

LinConstr2H=atlinconstraint(...
    hmark(3),...
    {{'ClosedOrbit',{1}},{'ClosedOrbit',{2}}},...
    [0,0],...
    [0,0],...
    [1e-2 1e-2]);


LinConstr3H=atlinconstraint(...
    hmark(end),...
    {{'ClosedOrbit',{1}},{'ClosedOrbit',{2}}},...
    [0,0],...
    [0,0],...
    [1 1]);



LinConstr1V=atlinconstraint(...
    indBPMbump,...
    {{'ClosedOrbit',{3}},{'ClosedOrbit',{4}}},...
    [bumpv,angv],...
    [bumpv,angv],...
    Wv);

LinConstr2V=atlinconstraint(...
    vmark(3),...
    {{'ClosedOrbit',{3}},{'ClosedOrbit',{4}}},...
    [0,0],...
    [0,0],...
    [1e-2 1e-2]);


LinConstr3V=atlinconstraint(...
    vmark(end),...
    {{'ClosedOrbit',{3}},{'ClosedOrbit',{4}}},...
    [0,0],...
    [0,0],...
    [1 1]);


rbump=ring0;

if bump(1)==0 && ang(1) ==0  % only V
    rbump=atmatch(rbump,VariabV,[LinConstr1V LinConstr3V],10^-14,20,3,@lsqnonlin);%,'fminsearch');%
    
elseif bump(2)==0 && ang(2) ==0  % only H
    rbump=atmatch(rbump,VariabH,[LinConstr1H LinConstr3H],10^-14,20,3,@lsqnonlin);%,'fminsearch');%
    
else
    %rbump=atmatch(rbump,VariabH,[LinConstr1 LinConstr3],10^-14,10,3,@fminsearch);%,'fminsearch');%
    rbump=atmatch(rbump,VariabH,[LinConstr1H LinConstr3H],10^-14,20,3,@lsqnonlin);%,'fminsearch');%
    rbump=atmatch(rbump,VariabV,[LinConstr1V LinConstr3V],10^-14,20,3,@lsqnonlin);%,'fminsearch');%
    
end

sh = atgetfieldvalues(rbump, indHCor,'KickAngle',{1,1});
sv = atgetfieldvalues(rbump, indVCor,'KickAngle',{1,2});

% indHCoraft = rc.indHst(24+[10 11 14 15]);
% rbump = atsetfieldvalues(rbump, indHCoraft,'KickAngle',{1,1},-sh);
disp('SH')
disp(hnames)
disp(sh*1e6)
disp('SV')
disp(vnames)
disp(sv*1e6)

o = findorbit4(rbump,0,indBPM);
h = o(1,selb);
v = o(3,selb);


o = findorbit4(rbump,0,indBPMbump);
h_bump = o(1,1);
v_bump = o(3,1);

%figure; atplot(rbump,@plClosedOrbit)


end

