        function mach = addpinholes(mach)
            
            pinid=~any(atgetcells(mach,'FamName','PINHOLE_ID(\d\d)?'));
            pind=~any(atgetcells(mach,'FamName','PINHOLE_D(\d\d)?'));
            if pinid && ~isempty(sr.model.pinhole_id)
                fprintf('Add ID pinholes...\n');
                dl1a5 = atgetcells(mach, 'FamName', 'DL1A_5\w*'); % include canted
                nbdl = sum(dl1a5);
                if nbdl == 1        % standard cell or injection cell
                    msk=true;
                    pinlist=atmarker('PINHOLE_ID');
                else                % 31 or 32: full machine
                    msk=pinmask(sr.model.pinhole_id);
                    if nbdl == 31
                        msk=msk(2:end); % No DL1A_% in cell 4
                    end
                    pinlist=arrayfun(@(c) atmarker(sprintf('PINHOLE_ID%02d',c)),sr.model.pinhole_id,'UniformOutput',false);
                end
                dl1a5(dl1a5)=msk(1:nbdl);
                loc=2.2e-3./cellfun(@(el) el.BendingAngle, mach(dl1a5));
                mach=atinsertelems(mach,dl1a5,loc,pinlist);
            end
            if pind && ~isempty(sr.model.pinhole_bm)
                fprintf('Add D pinholes...\n');
                dq1d=atgetcells(mach,'FamName','DQ1D');
                ffdq1d=find(atgetcells(mach,'FamName','mlDQ1D'),1);
                nbdl = sum(dq1d);
                if (nbdl == 1) || (nbdl == 2)	% injection cell or standard cell
                    msk=true(2,1);
                    pinlist=atmarker('PINHOLE_D');
                else                            % 32: full machine
                    msk=pinmask(sr.model.pinhole_bm);
                    pinlist=arrayfun(@(c) atmarker(sprintf('PINHOLE_D%02d',c)),sr.model.pinhole_bm,'UniformOutput',false);
                end
                dq1d(dq1d)=msk(1:nbdl);
                if isempty(ffdq1d)
                    ffangle=0;
                else
                    ffangle=mach{ffdq1d}.BendingAngle;
                end
                loc=(1.5e-3-ffangle)./cellfun(@(el) el.BendingAngle, mach(dq1d));
                mach=atinsertelems(mach,dq1d,loc,pinlist);
            end
            
            function msk=pinmask(pin)
                msk=false(1,32);
                pin=mod(pin-4,32)+1;
                msk(pin)=true;
            end
            
           
        end