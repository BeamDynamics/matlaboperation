function mach = addcavities(mach)
% add cavities devices along the ring
% in the exact location (13 cavities)
% if cavities exists, reads voltage, Harm number and Energy to set new cavities

cav_dev = {...
    'CAV_C05_01',... cell 4
    'CAV_C05_02',... cell 4
    'CAV_C05_03',... cell 4
    'CAV_C05_04',... cell 4
    'CAV_C05_05',... cell 4
    'CAV_C07_01',... cell 4
    'CAV_C07_02',... cell 4
    'CAV_C07_03',... cell 4
    'CAV_C07_04',... cell 4
    'CAV_C07_05',... cell 4
    'CAV_C25_01',... cell 4
    'CAV_C25_02',... cell 4
    'CAV_C25_03',... cell 4
    };

% check if cavities already there
cavind = find(atgetcells(mach,'Frequency'))';
if length(cavind)==length(cav_dev)
    disp('cavities already present')
    return
end

% get cavities settings (if any)
try
    E0 = atenergy(mach); % get energy
    
    H = mach{cavind(1)}.HarmNumber;% harmonic number
    V = sum(atgetfieldvalues(mach,cavind,'Voltage'));% total Voltage
    R = mach{cavind(1)}.TimeLag~=0;% radiation on if Time Lag not equal zero
catch
    disp('using default parameters (6.5MV, 992, radiation ON)')
    V = 6.5e6;
    H = 992;
    R = true;
end


% remove existing cavities
mach(cavind) = [];


% define fractional position of cavities along the drifts before and after the ID
cav_id_frac={...
    5,'DR_45',[1/5,3/5,5/5];...
    5,'DR_01',[2/5,4/5];...
    7,'DR_45',[1/5,3/5,5/5];...
    7,'DR_01',[2/5,4/5];...
    25,'DR_45',[1/5,3/5,5/5];...
    };

% add new cavities
for ii=1:size(cav_id_frac,1)
    
    CAV = atrfcavity(cav_dev{ii},0,0,0,0,E0);
    
    % insert cavity 
    id_ind = find(atgetcells(mach,'FamName',['ID' num2str(cav_id_frac{ii,1},'%.2d')])); % id index
    dr = find(atgetcells(mach,'FamName',cav_id_frac{ii,2}))';
    
    if strcmp(cav_id_frac{ii,2} , 'DR_45')
        dr_id = dr(find(dr<id_ind,1,'last'));
    elseif strcmp(cav_id_frac{ii,2} , 'DR_01')
        dr_id = dr(find(dr>id_ind,1,'first'));
    end
    
    insertargs = {};
    for ic = cav_id_frac{ii,3}
        insertargs = [insertargs,ic,{CAV}];
    end
    
    mach = atinsertelems(mach,dr_id,insertargs{:});
   
end

% set cavities to intial parameters
mach = atsetcavity(mach,V,R,H);
% rename
cavind = find(atgetcells(mach,'Frequency'))';
mach = atsetfieldvalues(mach,cavind,'FamName',cav_dev);

end