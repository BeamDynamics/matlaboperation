function ring = checklattice(ring,varargin)
%NEWRING = sr.CHECKLATTICE(RING)   Check the validity of an sr lattice
%
%CHECKLATTICE will
%-  set the QUADRUPOLE, BEND and MULTIPOLE pass methods
%-  Add if necessary markers at ID locations (centre of straight sections before the cell)
%-  Add if necessary markers at BM locations 
%-  Add if necessary markers at pinhole camera locations
%-  Rename the BPMs with their device name
%
%CHECKLATTICE(...,'energy',energy)
%   Set the ring energy
%
%CHECKLATTICE(...,'reduce',true,'keep',pattern)
%   Remove elements with PassMethod='IdentityPass' and merge adjacent
%   similar elements, but keeps elements with FamName matching "pattern"
%   Pattern may be a logical mask.
%   (Default: 'BPM.*|ID.*' for SR, '^BPM_..|^ID.*|JL1[AE].*|CA[02][57]').
%
%CHECKLATTICE(...,'remove',famnames)
%   remove elements identified by the family names (cell array)
%
%CHECKLATTICE(...,'reduce',true)
%   group similar elements together by calling atreduce
%
%CHECKLATTICE(...,'MaxOrder',n)
%   Limit the order of polynomial field expansion to n at maximum
%
%CHECKLATTICE(...,'NumIntSteps',m)
%   Set the NumIntSteps integration parameter to at least m
%
%CHECKLATTICE(...,'QuadFields',quadfields)
%   Set quadrupoles fields to the specified ones.
%   Default: {}
%
%CHECKLATTICE(...,'BendFields',bendfields)
%   Set bending magnet fields to the specified ones.
%   Default: {}
%
%CHECKLATTICE(...,'DeviceNames',true)
%   add device names in AT structure.

[check,options]=getoption(varargin,'CheckLattice',true);
if check
    [energy,periods]=atenergy(ring);
    [energy,options]=getoption(options,'Energy',energy);
    [periods,options]=getoption(options,'Periods',periods);
    reduce=getoption(options,'reduce',false);
    [keep,options]=getoption(options,'keep',false(size(ring)));
    if ~islogical(keep), keep=atgetcells(ring,'FamName',keep); end
    [devnam,options]=getoption(options,'DeviceNames',true);
    
    cellnb=circshift(1:32,[0 -3]);
    
    precious=atgetcells(ring,'FamName','ID.*$|^JL1[AE].*') |...
        atgetcells(ring,'FamName','BM\w*') |...
        atgetcells(ring,'Class','Monitor');
    ring=atclean(ring,options{:},'keep',keep|precious);
    ring=rename_bpms(ring);
    ring=rename_ids(ring);
    ring=rename_bms(ring);
    
    if devnam
        ring = sr.model.adddevicenames(ring);
    end
    
    % add CH12 (DR38) diagnostics name
    disp('Add CH12 (DR38) diagnostics name')
    ring = sr.model.adddiagnostics(ring);
    
    rp=atgetcells(ring,'Class','RingParam');
    if any(rp)
        params=ring(rp);
        params{1}.Energy=energy;
        params{1}.Periodicity=periods;
    else
        params={atringparam('sr',energy,periods)};
    end
    ring=[params(1);ring(~rp)];
end

    function mach=rename_bpms(mach)
        bpmid=atgetcells(mach,'FamName','BPM_..');
        if sum(bpmid) == 320    % Full machine
            fprintf('Rename BPMs...\n');
            [cell,nb]=meshgrid(cellnb,1:10);
            bpmname=arrayfun(@(c,i) sprintf('BPM_C%02d-%02d',c,i),cell(:),nb(:),'UniformOutput',false);
            mach(bpmid)=atsetfieldvalues(mach(bpmid),'FamName',bpmname);
        end
    end

    function mach=rename_ids(mach)
        idid=atgetcells(mach,'FamName','IDMarker');
        if sum(idid) == 32      % Full machine
            
                fprintf('Rename IDs...\n');
                idname=arrayfun(@(c) sprintf('ID%02d',c),cellnb(:),'UniformOutput',false);
                mach(idid)=atsetfieldvalues(mach(idid),'FamName',idname);
           
        end
    end

    function mach=rename_bms(mach)
        
        idid=atgetcells(mach,'FamName','BMHARD');
        BMtype=cell(sum(idid),1);
        BMtype([1,5,7,25,31])={'2PW_A'};
        BMtype([14,29])={'2PW_B'};
        BMtype(23)={'3PW'};
        BMtype(18)={'3PW18'}; 
        BMtype([2,8,16,20,26,28,30,32])={'SBM'};

        if sum(idid) == 32      % Full machine
            
            fprintf('Rename BMs...\n');
            idname=arrayfun(@(c) sprintf('BM%02d',c),cellnb(:),'UniformOutput',false);
            mach(idid)=atsetfieldvalues(mach(idid),'FamName',idname);
            mach(idid)=atsetfieldvalues(mach(idid),'BMType',BMtype);
            
        end
    end



end

