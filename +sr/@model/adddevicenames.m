    function mach=adddevicenames(mach)
        fprintf('Add device names...\n');
        ind = find(atgetcells(mach,'FamName','Q[FDIJ]\w*'))';
        mach(ind)=atsetfieldvalues(mach(ind),'Device',''); % initialize Device field to empty
        firstsliceQF8D=diff(ind)~=1;% some QF8D are splitted, name first slice only!
        indi=[ind(firstsliceQF8D) ind(end)]; % real number of Quads (removing overcount for splitted elements)       
        mach(indi)=atsetfieldvalues(mach(indi),'Device',sr.qpname(1:length(indi)));
        
        mach(ind(end:-1:1)) = nameunamedaslastnamed(mach(ind(end:-1:1)));
        
        function r=nameunamedaslastnamed(r)
            % name devices without a name with the name of the last device
            % with a name
            lastname = '';
            for ii=1:length(r)
                if ~isempty(r{ii}.Device)
                    lastname = r{ii}.Device;
                else
                    r{ii}.Device = lastname;
                end
                
            end
        end
        
        
        ind=find(atgetcells(mach,'FamName','DQ\w*'))';
        firstslicedq=diff(ind)~=2;% some DQ are splitted, name first slice
        indi=[ind(firstslicedq) ind(end)]; % real number of DQ (removing overcount for splitted elements)       
        mach(indi)=atsetfieldvalues(mach(indi),'Device',sr.dqname(1:length(indi)));
        secondslicedq=ind(diff(ind)==2);
        firstofsecondslicedq=secondslicedq+2; % index of first slice for DQ with second slice
        dqsecondslicename=atgetfieldvalues(mach(firstofsecondslicedq),'Device'); % read device name fro first slice
        mach(secondslicedq)=atsetfieldvalues(mach(secondslicedq),'Device',dqsecondslicename); % assign to second slice
        
        ind=atgetcells(mach,'Class','Sextupole');
        mach(ind)=atsetfieldvalues(mach(ind),'Device',sr.sxname(1:sum(ind)));
        
        ind=atgetcells(mach,'FamName','O[FIJ]\w*');
        mach(ind)=atsetfieldvalues(mach(ind),'Device',sr.ocname(1:sum(ind)));
        
        ind=atgetcells(mach,'Class','Monitor');
        mach(ind)=atsetfieldvalues(mach(ind),'Device',sr.bpmname(1:sum(ind)));
        mach(ind)=atsetfieldvalues(mach(ind),'FamName',sr.bpmname(1:sum(ind),'BPM_C%02i-%02i'));
        
        ind=atgetcells(mach,'FamName','PINHOLE_.*');
        mach(ind)=atsetfieldvalues(mach(ind),'Device',sr.pinholename(1:sum(ind)));
        
        ind=atgetcells(mach,'FamName','SH[123]\w*');
        mach(ind)=atsetfieldvalues(mach(ind),'Device',...
            sr.steername(sort([1:9:288,5:9:288,9:9:288]),'srmag/vst-'));        
        
    end

