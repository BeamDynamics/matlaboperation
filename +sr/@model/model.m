classdef model < atmodel
    %Class providing access to SR optical values
    %
    %Available locations:
    %
    %bpm:       All BPMs
    %hbpm:      Horizontal BPMs
    %vbpm:      Vertical BPMs
    %di:        All dipole
    %qp:        All quadrupoles
    %qf8d:      Tilted QF8D quadrupoles (splitted for 2PW modelling)
    %dq:        All dipole-quadrupoles
    %sx:        All sextpoles
    %oc:        All octupoles
    %ki:        All kickers
    %steerhdq:  All Horizontal steerers
    %steerh:    Horizontal steerers (no DQ)
    %steerv:    Vertical steerers
    %skew:      skew quadrupoles
    %id:        All IDs
    %idx:       ID n
    %bm:        All BMs
    %bmx:       BM n
    %3pw:       All 3PW
    %2pw:       All 2PW
    %2pwa:      All 2PW configuration A
    %2pwb:      All 2PW configuration B
    %sbm:       All SBM
    %diag:      Diagnostics section (DR_38)
    %kickers:   K3, K4, K1, K2
    %pinhole    All 5 pinhole source points
    %pinholeID07  Pinhole id 07
    %pinholeD17	Pinhole bm 17
    %pinholeID25 Pinhole id 25
    %pinholeD27	Pinhole bm 27
    %pinholeD09	Pinhole bm 09
    %
    %
    %Available parameters:
    %
    %0:     index in the AT structure
    %1:     Integrated strength
    %2:     theta/2pi
    %3:     betax
    %4:     alphax
    %5:     phix/2pi/nux
    %6:     etax
    %7:     eta'x
    %8:     betaz
    %9:     alphaz
    %10:    phiz/2pi/nux
    %11:    etaz
    %12:    eta'z
    %
    %Default parameters:
    %
    %[   2         3         5        6      8       10      ]
    %[theta/2Pi, betax, Phix/2PiNux, etax, betaz, Phiz/2PiNux]
    
    properties
        directory   % location of lattice files (see this.getdirectory)
    end
    
    properties (Constant, Hidden)
        pinhole_id=[7 25]
        pinhole_bm=[9 17 27]
    end
    
    methods (Static)
        newring=checklattice(ring,varargin)
        
        function pth=getdirectory(varargin)
            global APPHOME
            nm=getargs(varargin,{'theory'});
            directory=fullfile(APPHOME,'optics','sr');
            pth=fullfile(directory,nm);
        end
    end
    
    methods
        function this=model(varargin)
            %Builds the linear model of the ring
            %
            %M=sr.MODEL(AT)            Uses the given AT structure
            %
            %M=sr.MODEL()              takes the lattice from
            %           $APPHOME/sr/optics/settings/theory/betamodel.mat
            %
            %M=sr.MODEL('opticsname')  takes the lattice from
            %           $APPHOME/sr/optics/settings/opticsname/betamodel.mat
            %
            %M=sr.MODEL(path)          takes the lattice from
            %           path/betamodel.mat
            
            this@atmodel(sr.getat(varargin{:}));
            this.directory = this.getdirectory;
        end
    end
    
    
    methods (Access=protected)
        
        function recompute(this,varargin)
            modified=this.modified;
            recompute@atmodel(this,varargin{:});
            
            if modified
                % add dq integrated field (sr specific)
                idz=this.getid('dq');
                this.vlave(idz,12)=setintegfield(this.ring(idz),'PolynomB',{2});
                idz=this.getid('qf8d');
                this.vlave(idz,12)=setintegfield(this.ring(idz),'PolynomB',{2});
            end
            
            function v=setintegfield(at,varargin)
                k=atgetfieldvalues(at,varargin{:});
                l=atgetfieldvalues(at,'Length');
                v=k.*l;
            end
        end
        
        function idx=elselect(this,code)
            %Return indices of selected elements
            switch lower(code)
                case {'bpm','hbpm','vbpm'}
                    idx=atgetcells(this.ring,'Class','Monitor');
                case 'steerh'
                    idx=atgetcells(this.ring,'FamName','SH..') | ...
                        atgetcells(this.ring,'Class','Sextupole');%| ...
                    %atgetcells(this.ring,'FamName','DQ\w*'); % this is
                    % needed if DQ steerers are introduced. However
                    % slidced DQ for pinholes give problems
                case 'steerhdq'
                    idx=atgetcells(this.ring,'FamName','SH..') | ...
                        atgetcells(this.ring,'Class','Sextupole')| ...
                        atgetcells(this.ring,'FamName','DQ\w*'); % this is
                    % needed if DQ steerers are introduced. However
                    % slidced DQ for pinholes give problems
                case 'steerv'
                    idx=atgetcells(this.ring,'FamName','SH..') | ...
                        atgetcells(this.ring,'Class','Sextupole');
                case 'skew'
                    idx=atgetcells(this.ring,'FamName','SH..') | ...
                        atgetcells(this.ring,'Class','Sextupole');
                case 'sx'
                    idx=atgetcells(this.ring,'Class','Sextupole');
                case 'oc'
                    idx=atgetcells(this.ring,'FamName','O[FDIJ]\w*');
                case 'qp'
                    idx=atgetcells(this.ring,'Class','Quadrupole');
                case 'qf8d'
                    idx=atgetcells(this.ring,'FamName','QF8D\w*') & atgetcells(this.ring,'Class','Bend') ;
                case 'dq'
                    idx=atgetcells(this.ring,'FamName','DQ\w*');
                case 'di'
                    idx=atgetcells(this.ring,'Class','Bend');
                case 'ki'
                    idx=atgetcells(this.ring,'FamName','DR_K[1234]');
                case 'pinhole'
                    idx=atgetcells(this.ring,'FamName','PINHOLE_.*');
                case 'id'
                    idx=atgetcells(this.ring,'FamName','IDMarker$|^id\d+');
                case 'bm'
                    idx=atgetcells(this.ring,'FamName','BM\w*|^bm\d+')|atgetcells(this.ring,'FamName','SBM-\w*');
                case '3pw'
                    idx=atgetcells(this.ring,'FamName','3PW\w*-w*');
                case '2pw'
                    idx=atgetcells(this.ring,'FamName','2PW[AB]-\w*');
                case '2pwa'
                    idx=atgetcells(this.ring,'FamName','2PWA-\w*');
                case '2pwb'
                    idx=atgetcells(this.ring,'FamName','2PWB-\w*');
                case 'sbm'
                    idx=atgetcells(this.ring,'FamName','SBM-\w*');
                case 'diag'
                    idx=atgetcells(this.ring,'FamName','DR_38$|^DR_K1');    % K1 occupies the space in cell 3
                case 'kickers'
                    idx=atgetcells(this.ring,'FamName','DR_K[1234]');
                otherwise
                    if strncmp(code,'id',2) % relies on "case 'id'" above
                        idx=this.mcellid(this.getid('id'),str2double(code(3:end)),1);
                    elseif strncmp(code,'bm',2) % relies on "case 'bm'" above
                        idx=this.mcellid(this.getid('id'),str2double(code(3:end)),1);
                    elseif strncmp(code,'pinhole',7) % relies on "case 'pinhole'" above
                        idx=atgetcells(this.ring,'FamName',['PINHOLE_' code(8:end)]);
                    else
                        idx=atgetcells(this.ring,'FamName',code);
                    end
            end
        end
        
        
        
    end
    
    methods (Access=public)
        
        function settune(this,tunes,varargin)
            %SETTUNE    Change thetunes of the model
            %
            %SETTUNE(TUNES)
            % TUNES:    1x2 vector of desired tunes
            %
            %SETTUNE(...,TUNEFIXMODE,VARARGIN)
            % TUNEFIXMODE :	'QF1QD2', 'QF1QD2inj'(default), 'QF1QD2_noQF1inj' or 'Matching'
            % VARARGIN :	'name', value,... see sr.opticsmatching
            %               used only if tunefixmode is 'Matching'
            %
            %see also: atfittune sr.opticsmatching
            
            
            expectedmode={'QF1QD2','QF1QD2inj','QF1QD2_noQF1inj','Matching'};
            [tunefixmode,matchargs]=getargs(varargin,expectedmode{2});
            
            if ~strcmp(tunefixmode,expectedmode{4})   % atfittune
                if any(tunes > [1 1])
                    disp([num2str(tunes) ' larger than one. Including the integer part'])
                    fitargs={'UseIntegerPart'};
                else
                    fitargs={};
                end
                ws=warning;                     % Save the warning status
                warning('off','atmodel:getid'); % Disable getid warnings
                switch tunefixmode
                    
                    case expectedmode{1}
                        F = find(this.select('QF1[ABDE]\w*'));
                        D = find(this.select('QD2[ABDE]\w*'));
                        D([1,end]) =[];
                        
                    case expectedmode{2}
                        F = find(this.select('QF1\w*'));
                        D = find(this.select('QD2\w*'));
                        
                    case expectedmode{3}
                        F = find(this.select('QF1\w*'));
                        D = find(this.select('QD2\w*'));
                        F([1,end]) =[];
                        
                    otherwise
                        
                        error('tunefix mode not correct. see help sr.model.settune');
                end
                warning(ws);                    % Restore the warning state
                rt=atfittune(this.ring,this.dpp_,tunes,F,D,fitargs{:});
                rt=atfittune(rt,this.dpp_,tunes,F,D,fitargs{:});
                rt=atfittune(rt,this.dpp_,tunes,F,D,fitargs{:});
                
                qpk=atgetfieldvalues(rt,this.getid('qp'),'PolynomB',{1,2});
                this.setfieldvalue('qp','PolynomB',{1,2},qpk);
            else                                % match tune
                muxc = tunes(1);
                muyc = tunes(2);
                
                if tunes(1)<1
                    muxc=(76 + tunes(1));
                end
                if tunes(2)<1
                    muyc=(27 + tunes(2));
                end
                
                [~,qpk,dqk,qf8dk]=sr.opticsmatching(this.ring,...
                    'mux',muxc,'muy',muyc,...
                    'noDQ','option3',...
                    matchargs{:},'verbose',true);
                
                this.setfieldvalue('qp','PolynomB',{1,2},qpk)
                this.setfieldvalue('dq','PolynomB',{1,2},dqk)
                this.setfieldvalue('qf8d','PolynomB',{1,2},qf8dk)
                % match tune
                muxc = tunes(1);
                muyc = tunes(2);
                
                if tunes(1)<1
                    muxc=(76 + tunes(1));
                end
                if tunes(2)<1
                    muyc=(27 + tunes(2));
                end
                
                [~,qpk,dqk,qf8dk]=sr.opticsmatching(this.ring,...
                    'mux',muxc,'muy',muyc,...
                    'noDQ','option3',...
                    matchargs{:},'verbose',true);
                
                this.setfieldvalue('qp','PolynomB',{1,2},qpk)
                this.setfieldvalue('dq','PolynomB',{1,2},dqk)
                this.setfieldvalue('qf8d','PolynomB',{1,2},qf8dk)
            end
        end
        
        function setchrom(this,chrom,varargin)
            %Change chromaticity
            %
            %setchrom(chrom,chromfixmode)
            % CHROM:    1x2 vector of desired chromaticity
            % chromfixmode :    string, 'SF2SD1' (default) or 'InjSext'
            % varargin : 'name', value,... see sr.fixchrominjsext
            %           use only if tunefixmode is 'InjSext'
            %
            %
            %see also: atfitchrom sr.fixchrominjsext
            
            expectedmode={'SF2SD1','InjSext'};
            
            if ~isempty(varargin)
                chromfixmode=varargin{1};
            else
                chromfixmode=expectedmode{1};
            end
            
            switch chromfixmode
                
                case expectedmode{1}
                    
                    % move tune to Q1
                    rt=atfitchrom(atradoff(this.ring),chrom,'S[IJD]1\w*','S[IJF]2\w*');
                    sxk=atgetfieldvalues(rt,this.getid('sx'),'PolynomB',{1,3});
                    this.setfieldvalue('sx','PolynomB',{1,3},sxk);
                    
                case expectedmode{2}
                    error('not done yet, use SF2SD1')
            end
        end
        
        function setinjecetionbump(this,amplitude,varargin)
            %
            % computers gradients for injection bump kickers at given amplitude
            %
            K=sr.mathcinjectionbump(this.ring,amplitude,varargin{:});
            this.setfieldvalue('ki','PolynomB',{1,1},K');
        end
        
    end
    
    methods (Static)
       
        r = addpinholes(r); % adds pinhoeles to a given sr lattice
        
        r = adddiagnostics(r); % adds CH 12 diagnostics
        
        r = adddevicenames(r); % adds device names to a given sr lattice
        
        r = addcavities(r); % adds device names to a given sr lattice
        
    end
end

