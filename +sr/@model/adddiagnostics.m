function mach = adddiagnostics(mach,force_replacement)
% add diagnostics devices along the ring
% the drift name is modified as DRNAME_DIAGNOSTICNAME
%

diag_dev = {...
    'H_Scraper',... cell 4
    'CT',... cell 5
    'V_Scraper',... cell 6
    'BeamKiller',... cell 7
    '',... cell 8
    'HV_Kicker',... cell 9  (MDT kicker, cell to be confirmed)
    'CT',... cell 10
    'H_Scraper',... cell 11
    '20-Button',... cell 12
    'Collimator',... cell 13
    '',... cell 14
    'CT',... cell 15
    '',... cell 16
    '',... cell 17
    '',... cell 18
    '',... cell 19
    '',... cell 20
    '',... cell 21
    '',... cell 22
    '',... cell 23
    'Collimator',... cell 24
    '',... cell 25
    'Shaker',... cell 26
    'H_Stripline',... cell 27
    'H_Stripline',... cell 28
    'V_Stripline',... cell 29
    'H_Stripline',... cell 30
    'V_Stripline',... cell 31
    '',... cell 32
    'Shaker',... cell 1
    '',... cell 2
    '',... cell 3
    };

dr38_ind = find(atgetcells(mach,'FamName','DR_38','DR_11I'));

dr02I_ind = find(atgetcells(mach,'FamName','DR_02J'));
if ~isempty(dr02I_ind)
    dr38_ind(1) = dr02I_ind; % injection scraper is not in DR38
else
    dr38_ind=[];
end

if isempty(dr38_ind)
    disp('diagnostics already present')
else
    drfamname = atgetfieldvalues(mach,dr38_ind,'FamName');
    
    mach = atsetfieldvalues(mach,dr38_ind,'FamName',cellfun(@(a,b)[a '_' b],drfamname,diag_dev','un',0));
end

% mark Libera/Spark BPMs
    
bpmsL = find(atgetcells(mach,'FamName','\w*Libera'));
if isempty(bpmsL)
    
    bpms = find(atgetcells(mach,'Class','Monitor'));
    
    bpmsname = atgetfieldvalues(mach,bpms,'FamName');
    libspark = repmat({'Libera','Libera','Spark','Libera','Spark','Spark','Libera','Spark','Libera','Libera'},1,32);
    
    mach = atsetfieldvalues(mach,bpms,'FamName',cellfun(@(a,b)[a '_' b],bpmsname,libspark','un',0));
    disp('BPM Libera/Spark already marked')
else
    disp('BPM Libera/Spark marked')
end


end