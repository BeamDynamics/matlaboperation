function [resp,devs]=opticsknobs(latmodel,varargin)
%opticsknobs compute quadrupole response for individual optics knobs
%
% INPUT: 
% atmodel  : EBS lattice model (ex: atmodel = sr.model(ring))
% opticsknobfile : defualt 'opt_knob_'
% 
% OUTPUT:
% resp   : n quadrupole x 2 tune response
% devs   : device names
%
%see also: sr.model


if nargin<2
    opticsknobfile = 'opt_knob_';
end


% get quadurpole and DQ indexes
iq  = latmodel.get(0,'qp');    % quadrupoles 
iq8  = latmodel.get(0,'qf8d'); % tilted qf8d (varying dipole component, 50 slices)
idq = latmodel.get(0,'dq');    % dipole quadrupole (fixed dipole component, but may be splitted for markers)
allq_unsort = [iq;iq8;idq];
% get order of quadrupoles components in lattice
[allq,iqa] = sort(allq_unsort); 


%% assign a group number to each qf8d, to be able to reconstruct.
q_group = NaN(size(allq));
    
ii=1;
ig=1;
while ii<length(allq)
    
    q_group(ii)=ig;
    % if not subsequent or after subsequent (mid. marker), change group 
    if allq(ii+1)-allq(ii)~=1 && allq(ii+1)-allq(ii)~=2
        ig=ig+1;
    end
    
    ii=ii+1;
    
end
Nq= q_group(end-1)+1;
q_group(end) = Nq;

    function dgls=sumk(dgl,qg)
        % sum integrated gradient change for magnets of the same group
        % (slices)
        % dgl are the delta gradient, qg the quadrupole number, both 
        % in lattice order.
        nq=max(qg);
        dgls=zeros(nq,1);
        
        for inq=1:nq
            dgls(inq)=sum(dgl(qg==inq));% sum gradients
        end
        
    end


% get initial tunes
nuh=latmodel.nuh;
nuv=latmodel.nuv;
Q0 = [nuh, nuv];

% make sure initial tune is matched correctly (in case the optics input
% have been matched with  2 famliies only and the matching here is to be
% done for all families)
latmodel.settune(Q0, 'Matching');

defaultmux  = Q0(1);
defaultmuy  = Q0(2);
defaultrm12 = 0.20;     % detuning horizontal
defaultrm34 = 0.60;     % detuning crossed
defaultbxid = 6.9;
defaultaysf = 0.680;    % detuning vertical
defaultbysf = 5.40;
defaultdxsf = 0.0882;
defaultbymid= 4.70;

% from sr.matching.arc
%     case 'option1'
%         % eclude QF4AE and DQ2C
%     case 'option2'
%         % exclude DQ1 DQ2 and QF68
%     case 'option3'
%         % exclude DQ1 DQ2
%     case 'option4'
%         % skip Contrs and 8 (bxid)
%     otherwise
%         % use all quads
    
% name, default value, delta
knobs{1}={'mux', defaultmux, 0.01, 'option2'};
knobs{2}={'muy', defaultmuy, 0.01, 'option2'};
knobs{3}={'bysf', defaultbysf, 0.1, 'option2'};
knobs{4}={'aysf', defaultaysf, 0.01, 'option2'};
knobs{5}={'rm12', defaultrm12, 0.1, 'ALL'}; % 'option3'
knobs{6}={'rm34', defaultrm34, 0.1, 'ALL'}; % 'option3'
knobs{7}={'bxid', defaultbxid, 0.1, 'option2'};
knobs{8}={'dxsf', defaultdxsf, 0.0001, 'option2'};
knobs{9}={'bymid', defaultbymid, 0.1, 'option1'};

for ii = 1:length(knobs)
    
    kk = knobs{ii};
    lat_mod_knob=sr.model(latmodel);%,'reduce',true);

        
    [~,qpk,dqk,qf8dk]=sr.opticsmatching(...
        lat_mod_knob.ring,...
        kk{1},kk{2}+kk{3},...
        'noDQ',kk{4},...
        'verbose',true);
    
    lat_mod_knob.setfieldvalue('qp','PolynomB',{1,2},qpk);
    lat_mod_knob.setfieldvalue('dq','PolynomB',{1,2},dqk);
    lat_mod_knob.setfieldvalue('qf8d','PolynomB',{1,2},qf8dk);
    
    % get difference of integrated strngths
    DGL_=lat_mod_knob.get(1,'qp') - latmodel.get(1,'qp');
    D8GL=lat_mod_knob.get(1,'qf8d') - latmodel.get(1,'qf8d');
    DqGL=lat_mod_knob.get(1,'dq') - latmodel.get(1,'dq');
    DGL=[DGL_; D8GL; DqGL];
    
    % sum gradient changes for sliced magnets
    DGLsum = sumk(DGL(iqa),q_group);
    
    Mat(:,1)=DGLsum/kk{3};
    
    clear lat_mod_knob
    
    
    uMat = Mat;
    selnonzero = 1:length(Mat) ;
    
    [~,iu,~]=unique(q_group);
    devs = atgetfieldvalues(latmodel.ring(allq(iu)),'Device');
    
    resp=uMat(selnonzero,1);

    fff=fopen([opticsknobfile kk{1} '.csv'],'w+');
    
    for iq = 1:length(devs)
        fprintf(fff,'%s,%2.10f\n',devs{iq},resp(iq,1));
    end
    
    fclose(fff);
    % save matlab file
    DKL = resp; %#ok<NASGU>
    save([opticsknobfile kk{1} '.mat'],'DKL');


end


end

