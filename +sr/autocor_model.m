function [hr,vr]=autocor_model(varargin)
%AUTOCOR_MODEL Creates data files for the autocor device server
%
%[RESPH,RESPV]=sr.AUTOCOR_MODEL(PATH)
%PATH:  Data directory (Default: pwd)
%
%[RESPH,RESPV]=sr.AUTOCOR_MODEL(..,'exp',...)
%   Use a measured response matrix

args={pwd};
[exper,options]=getflag(varargin,'exp');
[OutFileName,options]=getoption(options,'OutputName', '');
args(1:length(options))=options;
pth=args{1};
if exper, rmpth=args{2}; end

atmodel=sr.model(pth,'reduce',true,'keep','BPM.*|ID.*');
[bpmidx,sth_idx,stvidx,dqidx]=atmodel.select('bpm','steerh','steerv','dq');
[bpm,steer_h,steerv,dq]=atmodel.get('bpm','steerh','steerv','dq');

% sort DQ and hor Steerers
[~, h_ord]= sort([find(sth_idx); find(dqidx)]);
sthidx = sth_idx | dqidx;
steerh = [steer_h; dq];
steerh = steerh(h_ord,:);

nsth=size(steerh,1);
nstv=size(steerv,1);

if ~exist('./alpha','file')
    savetext('alpha','%.4e',atmodel.alpha);
end


if exper
    hr=sr.load_normresp(fullfile(rmpth),'h','TooLarge',0,'Zeros',0,'Fixed',0);
    vr=sr.load_normresp(fullfile(rmpth),'v','TooLarge',0,'Zeros',0,'Fixed',0);
    [hfr,~]=sr.load_fresp(rmpth);
    b_ok=all(isfinite([hr(:,~isnan(hr(1,:))) vr(:,~isnan(vr(1,:))) hfr(:,~isnan(hfr(1,:)))]),2);
    sh_ok=true(1,nsth);
    sv_ok=true(1,nstv);
    h_resp(b_ok,sh_ok)=hr(b_ok,sh_ok);
    v_resp(b_ok,sv_ok)=vr(b_ok,sv_ok);
    h_fresp(b_ok)=hfr(b_ok);
    
    matmode='measured';
else
    fprintf('Computing horizontal response m/rad \n');
    h_resp=-sr.orm(atmodel.ring,'h',sthidx,bpmidx);
    fprintf('Computing vertical response\n');
    v_resp=-sr.orm(atmodel.ring,'v',stvidx,bpmidx);
    dct=4.e-4;
    fprintf('Computing frequency response m/Hz \n');
    forb0=findsyncorbit(atmodel.ring,0,bpmidx);
    forb=findsyncorbit(atmodel.ring,dct,bpmidx);
    h_fresp=-(forb(1,:)-forb0(1,:))'/dct.*atmodel.ll*atmodel.ll/992/PhysConstant.speed_of_light_in_vacuum.value;
    
    matmode='theoretical';
end

% for most effective corrector
hv_bpms=[bpm(:,1:2) 2*pi*bpm(:,3) bpm(:,4:5) 2*pi*bpm(:,6)];
h_steerers = [steerh(:,1:2) 2*pi*steerh(:,3) ones(nsth,1)];
v_steerers = [steerv(:,[1 5]) 2*pi*steerv(:,6) ones(nstv,1)];
bpm_name=sr.bpmname((1:size(hv_bpms,1))');
steerh_name=sr.hsteername((1:nsth)','srmag/hst-');
steerv_name=sr.steername((1:nstv)','srmag/vst-');
store_bpm('bpms',hv_bpms,bpm_name);
store_bpm_CSV('bpms.csv',hv_bpms,bpm_name);
store_steerer('h_steerers',h_steerers,steerh_name);
store_steerer('v_steerers',v_steerers,steerv_name);
store_steerer_CSV('h_steerers.csv',h_steerers,steerh_name);
store_steerer_CSV('v_steerers.csv',v_steerers,steerv_name);

% [b,f,pu,npu]=bump_coefs(h_resp,bpm(:,1),steerh(:,1),pm.periods);
% kh=[b' pu-1 npu f];
% store_bump('h_bumps',kh,steerh_name);

% [b,f,pu,npu]=bump_coefs(v_resp,bpm(:,1),steerv(:,1),pm.periods);
% kv=[b' pu-1 npu f];
% store_bump('v_bumps',kv,steerv_name);

hr=[h_resp,h_fresp(:)];
vr=v_resp;

theoORmeas=[];
if exper
theoORmeas=['_' matmode];
end

dlmwrite(['h_resp' OutFileName theoORmeas '.csv'],hr,'delimiter',',','precision','%.24f');
dlmwrite(['v_resp' OutFileName theoORmeas '.csv'],vr,'delimiter',',','precision','%.24f');
store_variable('matlog','resp',matmode);
save(['resp' OutFileName theoORmeas '.mat'],'hr','vr');

end

function store_steerer_CSV(fname,st,st_name)

fid=fopen(fname,'w');
for i=1:size(st,1)
fprintf(fid,'%s,%.6f,%.4f,%.6f,%.5e\n',st_name{i},st(i,1:4));
end
fclose(fid);

end


function store_bpm_CSV(fname,bpm,bpm_name)

fid=fopen(fname,'w');
for i=1:size(bpm,1)
fprintf(fid,'%s,%.6f,%.4f,%.6f,%.4f,%.4f,%.6f\n',bpm_name{i},bpm(i,1:6));
end
fclose(fid);

end
