function [injbumpmat]=injbump_model(varargin)
%INJBUMP_MODEL	produces tune correction matrix for
%Tango://ebs-simu:10000/srdiag/beam-tune/adjust
%	[quadmat]=tune_model()
%
%INJBUMP_MODEL()               takes the lattice from
%           $APPHOME/sr/optics/settings/theory/betamodel.mat
%INJBUMP_MODEL('opticsname')   takes the lattice from
%           $APPHOME/sr/optics/settings/opticsname/betamodel.mat
%INJBUMP_MODEL(path)           takes the lattice from
%           path or path/betamodel.mat or path/betamodel.str
%INJBUMP_MODEL(AT)             Uses the given AT structure
%

args={pwd};
args(1:length(varargin))=varargin;
pth=args{1};

atmodel=sr.model(pth);

% must contain 0.0 amplitude and be size 20x5
bampl=[0,linspace(-10e-3,-16e-3,19)]; 

% match bump
[~,injbumpmat]=sr.matchinjectionbump(atmodel.ring,bampl,'plot',true); % integrated gradients, KL [rad]

% measured calibrations
ibm(:,1) = injbumpmat(:,[3])./0.00227*2000; % K3: 2.27mrad@2kA
ibm(:,2) = injbumpmat(:,[4])./0.00231*2000; % K4: 2.31mrad@2kA
ibm(:,3) = injbumpmat(:,[1])./0.00230*2000; % K1: 2.30mrad@2kA
ibm(:,4) = injbumpmat(:,[2])./0.00228*2000; % K2: 2.28mrad@2kA

dlmwrite('injbumpmat.csv',[bampl' ibm],.... %reshaffle and calibrate 2.36mrad at 2000A
    'delimiter',',','precision','%.6f');

end
