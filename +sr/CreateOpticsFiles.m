function CreateOpticsFiles(r,opticsname,varargin)
% CREATEOPTICSFUILES(r,opticsname,'property',value,...), 
% given an AT lattice R creates the relevant files for operation
%
% operation folder 2018 is APPHOME/sr/optics/settings/(OPTICSNAME)
% operation folder 2019 could become APPHOME/<commissioingtoolsrepository>/optics/sr/(OPTICSNAME) 
%
% optional input ('NAME',val,'Name2',val2,...)
% 'OperationFolder'  : commissioningtools folder where optics/sr/theory
%                      link is present.
% 'MeasuredResponse' : folder containing ouput of full RM measurement
% 'errtab' : table of errors
% 'cortab' : table of corrections
% 'compute_bumps' : false = skip COD bumps computation (default = true)
% 'compute_rm_derivatives' :(true) true = compute
% 'compute_tune_matching' :(false) true = compute
% 'rm_tune' :([]) [76.19 27.39] = compute response matrix for this tune as well
%
% WARNING lattice contains QF8D modeled as dipoles. 
% Those are merged to a single quadrupole without bending angle for all 
% computations. This makes any function using dispersion (matching for ex.) 
% incorrect. 
% 
% example 1):
% optics with errors and measured response  matrix
% load /machfs/liuzzo/esrfupgradelatticefiles-code/S28D/MagneticLengths/AT/errors/WP_021_034__S28D_Z0p67_newErrFunct_1_seed1_.mat
% CreateOpticsFiles(ring_err,'Seed1MeasResp','MeasuredResponse','/machfs/liuzzo/EBS/Simulator/responsematrix/resp2')
% 
% example 2):
% optics without BM sources + errors from simulations for use in the simulator
% a = load('/machfs/liuzzo/EBS/S28F20190701/LATTICE/AT/S28F_PA_Canting.mat');
% load a file with errors and PINHOLES!
% b = load('/machfs/liuzzo/EBS/S28F20190701/LATTICE/AT/S28Facph/RefLatModelS28Facph_ns10/Errors_0001.mat');
% 
% sr.CreateOpticsFiles(a.r,'S28F_restart',...
%                       'errtab',b.errtab,...
%                       'cortab',b.cortab,...
%                       'OperationFolder',fullfile(pwd,'optics'))
% 
%see also: SetOperationOptics

global APPHOME
p=inputParser;
addRequired(p,'r',@iscell);
addRequired(p,'opticsname',@ischar);
addOptional(p,'OperationFolder',APPHOME);
addOptional(p,'MeasuredResponse',[]);
addOptional(p,'errtab',[],@istable);
addOptional(p,'cortab',[],@istable);
addOptional(p,'compute_bumps',false,@islogical);
addOptional(p,'compute_rm_derivatives',false,@islogical);
addOptional(p,'compute_tune_matching',false,@islogical);
addOptional(p,'rm_tune',[],@isnumeric);

parse(p,r,opticsname,varargin{:});
r           = p.Results.r;
opticsname  = p.Results.opticsname;
OpeFolder   = p.Results.OperationFolder;
rm          = p.Results.MeasuredResponse;
errtab      = p.Results.errtab;
cortab      = p.Results.cortab;
compute_bumps      = p.Results.compute_bumps;
compute_rm_derivatives      = p.Results.compute_rm_derivatives;
compute_tune_matching = p.Results.compute_tune_matching;
rm_tune = p.Results.rm_tune;

% to be able to use those lattice in the simulator, the reference model,
% has pinholes.

% % make sure radiation is on, and cavity is set
% r = atradon(atsetcavity(atradoff(r),       6.5e6,0,992),'','auto','auto');
%% on request of SW and LF on the 10/Feb/2020 the above line is replaced by the one below
r = atradon(atsetcavity(atradoff(r,'','auto','auto'),       6.5e6,0,992),'CavityPass','BndMPoleSymplectic4RadPass',''); % no change to quad pass
disp('Pass Methods in betamodel lattice: ')
unique(atgetfieldvalues(r,atgetcells(r,'PassMethod'),'PassMethod'))

r = sr.model.addpinholes(r);
r = sr.model.adddevicenames(r);

% remove K
indk=atgetcells(r,'K');r(indk) = cellfun(@(a)rmfield(a,'K'),r(indk),'un',0);

if isempty(errtab)
    errtab = atcreateerrortable(r);
end

if isempty(cortab)
    cortab = atgetcorrectiontable(r,r); % zeros
end

fulfold=fullfile(OpeFolder,'optics','sr',opticsname);

curdir=pwd;
mkdir(fulfold);
cd(fulfold);

% ensure sufficent maxorder 
indm = atgetcells(r,'MaxOrder');
np = cellfun(@(a)min(length(a.PolynomB),length(a.PolynomA)),r(indm),'un',1);
r = atsetfieldvalues(r,indm,'MaxOrder',np-1);

% add serial numbers and calibration curves
r = addcalibrationcurves(r); 

% AT lattice
betamodel=atradoff(r);
errtab_tmp=errtab; % store input tables
cortab_tmp=cortab;

errtab = atcreateerrortable(r);
cortab = atgetcorrectiontable(r,r); % zeros
save('betamodel.mat','betamodel','errtab','cortab'); % errtab and cortab all zeros.
errtab = errtab_tmp; clear errtab_tmp; % restore input tables
cortab = cortab_tmp; clear cortab_tmp;

% AT lattice for simulator with errors and corrections
disp('errors and correction lattice for simulator (provide errtab and cortab or zeros will be set)')
errmodel=atseterrortable(r,errtab,'verbose',true);
errcormodel=atsetcorrectiontable(errmodel,cortab);
save('errcormodel.mat','errcormodel','cortab','errtab'); % errors and corrections

cortab = atgetcorrectiontable(r,r); % zeros
save('errmodel.mat','errmodel','cortab','errtab'); % only errors


% save image of lattice optics
f=figure;
atplot(r);
saveas(gca,[opticsname '.jpg']);
close(f);

% layout file for closed orbit application
atwriteacu([r;r(1:ceil(length(r)/32)+10)],[opticsname '_layout.txt']);

atwriteacuonlinesimulator(r,['SimpleLayout.txt']);

rfull=sr.model(r);

% compute fast model once for all
% merge DQ
indDQmark=atgetcells(r,'FamName','PINHOLE_D\w*','CellCenter');
r(indDQmark)=[]; % checklattice should merge all splitted elements.

% merge 2PW and transform back to quadrupole
indQF8dip = find(atgetcells(r,'FamName','QF8\w*') & atgetcells(r,'Class','Bend'))';
r=atsetfieldvalues(r,indQF8dip,'BendingAngle',0);
r=atsetfieldvalues(r,indQF8dip,'Class','Quadrupole');
r=atsetfieldvalues(r,indQF8dip,'PassMethod','StrMPoleSymplectic4Pass');

% get fast model (QF8D merged, not ok for optics computations)
rmod=sr.model(r,'reduce',true,'keep','BPM.*|ID.*');

% % store family magnet strengths to file (or in the format for magnet application, to be loaded)
disp('family magnets')
sr.magnetsstrengths_model(fullfile(pwd,'DesignStrengths.csv'),rmod);
  
disp('PS currents file')
sr.createPSfile(rmod.ring);

% resonance response
disp('resonances')
sr.reson_model(rfull);

% % tune response
disp('tune')
sr.tune_model(rfull); % splitted quadrupoles, lattice is wrong!
sr.tuneresponse(rfull,'QF1QD2inj',[1.3 1.4 1.40],'tunemat_QF1p3_QD1p4_V1p4');%,'Matching');

if compute_tune_matching
    sr.tuneresponse(rmod,'Matching',[1.0 1.0 1.0],'tunemat_matching');%,'Matching');
    
   % % files of quad strengths for each optics tuning knob
   sr.opticsknobs(rmod);
end
% orbit response, theory always,

sr.autocor_model(rfull);

if ~isempty(rm_tune)
    rt = atfittune(r,rm_tune,'QF1\w*','QD2\w*','UseIntegerPart');
    sr.autocor_model(rt,'OutputName',...
        ['_H' strrep(num2str(rm_tune(1),'%2.2f'),'.','p'),...
        '_V' strrep(num2str(rm_tune(2),'%2.2f'),'.','p'),...
        ]); 
end

% measured if required
if ~isempty(rm)
sr.autocor_model(rfull,'exp',rm);
end

% chromaticity response
disp('chromaticity')
sr.chrom_model(rfull);

%orbit bumps (takes long time, commented)
if compute_bumps
    sr.orbitbumps_model(rfull);
else
    disp('SKIPED orbit bumps')
end

if compute_rm_derivatives
    mach = rfull.ring;
    
    dqmask=rfull.select('dq');
    quadmask=rfull.select('qp');
    sfmask=atgetcells(rfull.ring,'FamName','S[DFIJ][12][ABDE]');
    errmask=quadmask | dqmask | sfmask;	% All QPs, DQs, SF2s
    cormask=quadmask;
    errcormask = errmask | cormask;
     
    ct = 0;
    
    bpmidx = find(rfull.select('bpm'))';
    
    dDx_dq = qemdispderiv(mach,ct,@setk,1.e-3,find(errcormask)',bpmidx); %#ok<NASGU>
    
    save('quadhdisp.mat','dDx_dq');
    
else
    disp('SKIPED RM derivative computation')
end


% injection bump 
disp('injection bump')
sr.injbump_model(rfull);
mkdir('kicker_bumps');
bumpfile = fullfile(pwd,'kicker_bumps','injbumpmat.cvs');
movefile(fullfile(pwd,'injbumpmat.csv'),bumpfile);
system('rm kicker_bump'); %remove old bump
system(['ln -s ' bumpfile ' kicker_bump']) % create link 

% tuning knobs
mkdir('knobs');
cd('knobs');
sr.tuning_knobs;
cd('..');

cd(curdir);

end


function elem=setk(elem,dk)
        strength=elem.PolynomB(2)+dk;
        elem.K=strength;
        elem.PolynomB(2)=strength;
end
    