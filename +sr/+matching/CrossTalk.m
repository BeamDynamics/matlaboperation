function r = CrossTalk(r0,ind_loose,frac_lost,ind_get,frac_get,mult)
% r = CrossTalk(...
% r0,...        lattice
% ind_loose,... index of element that gives field
% frac_lost,... fraction of field of ind_loose
% ind_get,...   index of element that gets field
% frac_get,...  fraction of field of ind_loose that is set to ind_get must
%               be same size as ind_get
% mult)
%
% EXAMPLE:
%
% r = CrossTalk(r0,56,0.99,58,0.02,2);
%
%
%see also:
r =r0;

try
    if isempty(ind_loose)
        error('ind_loose is empty')
    end
    if isempty(ind_get)
        error('ind_get is empty')
    end
    
    
    K0_loose = atgetfieldvalues(r0,ind_loose,'PolynomB',{1,mult}); % main multipole
    L0_loose = atgetfieldvalues(r0,ind_loose,'Length'); % main multipole
    
    for ig = 1:length(ind_get)
        K0_get{ig} = atgetfieldvalues(r0,ind_get{ig},'PolynomB',{1,mult}); % main multipole
        L0_get{ig} = atgetfieldvalues(r0,ind_get{ig},'Length'); % main multipole
        %K0s_get = atgetfieldvalues(r0,ind_get,'PolynomA',{1,mult}); % main multipole
        if isnan(K0_get{ig}), K0_get{ig} =zeros(size(ind_get{ig})); end
    end
    
    r = r0;
    
    % reduce main magnet
    r  = atsetfieldvalues(r, ind_loose,'PolynomB',{1,mult},K0_loose*frac_lost);
    
    %increase side magnet(s)
    for ig = 1:length(ind_get)
        
        r  = atsetfieldvalues(r, find(ind_get{ig}),'PolynomB',{1,mult},...
            K0_get{ig}*0 + K0_loose.*frac_get(ig).*L0_loose./L0_get{ig});
        
        Mord = atgetfieldvalues(r0,ind_get{ig},'MaxOrder'); % getting multipole maxorder
        r  = atsetfieldvalues(r, ind_get{ig},'MaxOrder',{1,1},max(mult-1,Mord));
        r(ind_get{ig})=PadPolynomAB(r(ind_get{ig}));
    end
    
catch err
    disp(err)
    disp('nothing done')
end


end
