function [ rc, arc ] = TwoPoleWigglerOptics( rc, r, idnum , varargin)
%TWOPOLEWIGGLEROPTICS(rc,r,idnum) match optics of 2PW lattice rc to those of r at idnum 
%
%Inputs:
% rc: EBS upgrade lattice with 2PW at idnum
% r: EBS upgrade lattice without 2PW at idnum
% idnum: ID number where the 2PW is placed
%
%Optional Inputs:
% Ang2PW: 2PW angle (defaut +1.64526e-3)
% configuration : A (default) or B
% ploton: prints out plots, true/false (default)
% verbose: prints matching process true/false (default)
%
%Output:
% r: EBS upgrade lattice with 2PW at id idnum
% arc : cell with 2pw
%
%Description:
% 2PW inserted in DR_25
% QF8D rotated to correct orbit displacement (model with 50 dipole slices)
% QF4 rematched to fix residual dispersion
%
%Example:
% >> [r2pw,arc2pw]=sr.matching.TwoPoleWiggler(LOW_EMIT_RING_INJ,5,...
%                                        'configuration','B','ploton',true)
%
%Warning:
% this function is not supposed to be used inside sr.model, as it changes
% the number of elements in the lattice
%
%see also:


p=inputParser;
addRequired(p,'rc',@iscell);
addRequired(p,'r',@iscell);
addRequired(p,'idnum',@isnumeric);
addOptional(p,'ploton',false,@islogical);
addOptional(p,'verbose',false,@islogical);

parse(p,rc,r,idnum,varargin{:});

ploton=p.Results.ploton;
verbose=p.Results.verbose;

% get reference cell without 2PW
[cellstartindex,cellendindex]=sr.getcellstartend(r,idnum);

ARCA=r(cellstartindex:cellendindex);

% get cell with 2PW to be matched
[cellstartindexc,cellendindexc]=sr.getcellstartend(rc,idnum);

arc0=rc(cellstartindexc:cellendindexc);


% match survey
[l,~,~]=atlinopt(r,0,1:(cellendindex+1)); % get optics at cell end (could be canted cell or injection, so use whole latice to get optics)
dxid=l(end).Dispersion(1);
dxpid=l(end).Dispersion(2);

twiin=l(cellstartindex); twiin.mu=[0 0];
[arc,~]=matchSurvey2PW(arc0,ARCA,twiin,dxid,dxpid,ploton,verbose);

% get optics at reference locations
sfaeindr=find(atgetcells(r(1:(cellendindex+1)),'FamName','DR_10','DR_35'),2,'last')+1;
centindr=find(atgetcells(r(1:(cellendindex+1)),'FamName','DQ2C\w*'),1,'last');
twiin=l(cellstartindex);
twiend=l(cellendindex+1);
twimid=l(centindr);
twisf=l(sfaeindr);

sfaeind=find(atgetcells(arc,'FamName','DR_10','DR_35'),2,'last')+1;
centind=find(atgetcells(arc,'FamName','DQ2C\w*'),1,'last');
excludequad=false(19,1);
excludequad([8:12])=true; % exclude DQ and QF8
excludeconstr=false(19,1);
excludeconstr([8:14])=true; % rm12 rm34, mux, muy, optics@end
arc=sr.matching.arcopen(...
    arc,...
    twiin,twiend,...
    twisf,...
    twimid,...
    sfaeind,...
    centind,...
    excludequad,...
    excludeconstr,...
    true);

% plot if required
if ploton
    thetaDQ2Chalf=sum(atgetfieldvalues(ARCA,atgetcells(ARCA,'FamName','\w*DQ2C\w*'),'BendingAngle'))/2;
    [parot]=atgeometry(ARCA,1:length(ARCA)+1,'Hangle',2*pi/32/2 + thetaDQ2Chalf );
    [sbrot]=atgeometry(arc0,1:length(arc0)+1,'Hangle',2*pi/32/2+ thetaDQ2Chalf);
    [sbmrot]=atgeometry(arc,1:length(arc)+1,'Hangle',2*pi/32/2+ thetaDQ2Chalf);
    figure;%('units','normalized','position',[0.3 0.2 0.6 0.3]);
    plot([parot.x],[parot.y],'x-','DisplayName','standard','Linewidth',2);hold on;
    plot([sbrot.x],[sbrot.y],'s:','DisplayName','2PW','Linewidth',2);
    plot([sbmrot.x],[sbmrot.y],'o--','DisplayName','2PW + QF8D rot','Linewidth',2);
    legend toggle;
    xlim([13.6 14.5]);
    xlabel('x [m]'); ylabel('y [m]')
end

% reconstruct lattice with new cell

rc=[rc(1:cellstartindexc-1);...
    arc;... % arcwith 2PW
    rc(cellendindexc+1:end)];

end



function [rok,d]=matchSurvey2PW(r,r0,twiin,dxid,dxpid,ploton,verbose)
% function matchSurvey2PW(r,r0,dxid,dxpid,ploton,verbose)
% r= lattice to be matched
% r0 = reference lattice (target of geometry matching)
% dxid,dxpid = dispersion and dispersion prime at cell end 
%   (not taken from r0 as could be asymmetric for injection or canted cell)
% ploton : do plots
% verbose: print text
%
%Description:
% match the geometry of r to that of r0 using DQ angles and DW1 drift 
% QF4 unbalance to recover dispersion at cell end.
%
if verbose
disp('dispersion and dispersion prime:')
disp(dxid)
disp(dxpid)
end

allpos0=1:length(r0);
allpos=1:length(r);

%[twiin,~,~]=atlinopt(r0,0,1);

idpos=[find(atgetcells(r,'FamName','DQ1D\w*'),1,'first'); length(r)+1];
idpos0=[find(atgetcells(r0,'FamName','DQ1D\w*'),1,'first'); length(r0)+1];


p0=atgeometry(r0,allpos0);%,of0);,'centered'
p=atgeometry(r,allpos);%,of);
xx=arrayfun(@(p)p.x,p);
yy=arrayfun(@(p)p.y,p);
xx0=arrayfun(@(p)p.x,p0);
yy0=arrayfun(@(p)p.y,p0);

if ploton
    figure;
    plot(xx0,yy0,'k+-',xx,yy,'r+-');
end

dif= getposdif(r,idpos,r0,idpos0);

zz=zeros(size(dif));
oo= ones(size(dif))/10000;

indQF8=findcells(r,'FamName','QF8D');
L=sum(atgetfieldvalues(r,indQF8,'Length'));
K=atgetfieldvalues(r,indQF8(1),'PolynomB',{1,2});

v1=atVariableBuilder(r,...
    {@(ar,delta)setshiftandroll(ar,indQF8,L,K,delta)},{[0, 0]'});

% vary a drift as total length will change 
inddrift=findcells(r,'FamName','DW1');%
DriftLen0=sum(atgetfieldvalues(r,inddrift,'Length'));
v2=atVariableBuilder(r,...
    {@(ar,driftlen)atsetfieldvalues(ar,inddrift,'Length',driftlen)},{DriftLen0});

indqf4=find(atgetcells(r,'FamName','QF4\w*'));
pb0qf=atgetfieldvalues(r,indqf4,'PolynomB',{1,2});% r{indqf4(1)}.PolynomB(2);
v3=atVariableBuilder(r,{@(r,v)funUnbQF4(r,v,pb0qf,indqf4)},{[0.0]'});

% v3=atVariableBuilder(r,'QF8D',{'PolynomB',{1,2}});

% v3=atVariableBuilder(r,'QF4[AB]',{'PolynomB',{1,2}});
% v4=atVariableBuilder(r,'QF4[DE]',{'PolynomB',{1,2}});
% indQF4A=find(atgetcells(r,'FamName','QF4A'));
% indQF4E=find(atgetcells(r,'FamName','QF4E'));
% ld=atlinopt(r,0,1:indQF4E);
% rm12=RM44(ld([indQF4A,indQF4E]),1,2);
% 
% cRM12=struct(...
%     'Fun',@(~,ld,~)RM44(ld,1,2),...
%     'Weight',1,...
%     'RefPoints',[indQF4A,indQF4E],...
%     'Min',rm12,...
%     'Max',rm12...
%     );

cSURV=struct(...
    'Fun',@(r,~,~)getposdif(r,idpos,r0,idpos0),...
    'Weight',oo,...
    'RefPoints',[1],...
    'Min',zz,...
    'Max',zz...
    );

LinConstrETA=atlinconstraint(...
    length(r)+1,...
    {{'Dispersion',{1}}},...
    [dxid],...
    [dxid],...
    [1e-1]);

LinConstrETAp=atlinconstraint(...
    length(r)+1,...
    {{'Dispersion',{2}}},...
    [dxpid],...
    [dxpid],...
    [1e-1]);

cETA=[LinConstrETA LinConstrETAp]; % LinConstr1 LinConstr2  LinConstr3

verb=0;
if verbose, verb=3; end

[rok,~,d]=atmatch(r,[v1, v2, v3],[cSURV cETA],10^-10,50,verb,@lsqnonlin,twiin);
%[rok,~,d]=atmatch(r,[v1, v2],[cSURV],10^-10,20,verb,@lsqnonlin,twiin);

d(3)=d(3)-DriftLen0;
if verbose, disp(['QF8D Tilt  is: ' num2str(d(1)) ' rad']); end
if verbose, disp(['QF8D shift is: ' num2str(d(2)) ' m']); end
if verbose, disp(['Delta DW1  is: ' num2str(d(3)) ' m']); end
if verbose, disp(['Delta QF4  is: ' num2str(d(4)) ' 1/m2']); end
%if verbose, disp(['QF8D K     is: ' num2str(d(4)) ' 1/m2']); end

p=atgeometry(rok,allpos);%,'centered'
xxm=arrayfun(@(p)p.x,p);
yym=arrayfun(@(p)p.y,p);

if ploton
    hold on;
    
    plot(xxm,yym,'+-','Color',[0,0.5,0]);
    
    legend('ARCA',['2PW'],['2PW + QF8D rot: ' num2str(d')]);
    
    [~,dx,dt]=setshiftandroll(r,indQF8,L,K,d([1,2]));
    
    figure; plot(dx); hold on;plot(dt);plot(dx+dt); plot(-(dx+dt).*K.*L./length(indQF8));
    legend('dx','dt','dx + dt','dtheta');
    title({[num2str(L) ' ' num2str(K)],[num2str(d)]})
    %saveas(gca,'QF8Dx.fig')
end

end

function r=funUnbQF4(r,v,pb0,indqf4)

r=atsetfieldvalues(r,indqf4([1,2]),'PolynomB',{1,2},pb0([1 2])+v(1));
r=atsetfieldvalues(r,indqf4([3,4]),'PolynomB',{1,2},pb0([3,4])-v(1));

end

function r=funUnbQD5(r,v,pb0,indqd5)

r=atsetfieldvalues(r,indqd5([1]),'PolynomB',{1,2},pb0([1])+v(1));
r=atsetfieldvalues(r,indqd5([2]),'PolynomB',{1,2},pb0([2])-v(1));

end


function [r,dx,dt]=setshiftandroll(r,ind,L,K,v)
% dipole version
% ind= indexes of sliced QF8D
% L total length
% K QD8 gradient
% v =[rot,displacement]

K=atgetfieldvalues(r,ind(1),'PolynomB',{1,2});

rotval=v(1);
dispval=v(2);%1234*1e-6/2; % v(2)

%define displacements
dx = dispval .* ones(size(ind)) ;
dt = linspace(-L/2,L/2,length(ind))* sin(rotval);

% initialize to zero
theta=-(dx+dt).*K.*L./length(ind);
r=atsetfieldvalues(r,ind','BendingAngle',{1,1},theta');

end

function [r11]=RM44(lindata,ind1,ind2)
% get value of of indeces ind1 and ind2 (1 to 4) of 
% M44 between two points first and last

Mlast=lindata(2).M44;
Mfirst=lindata(1).M44;

Mfirstinv=[[Mfirst(2,2),-Mfirst(1,2);-Mfirst(2,1),Mfirst(1,1)],...
            zeros(2,2);...
            zeros(2,2),...
            [Mfirst(4,4),-Mfirst(3,4);-Mfirst(4,3),Mfirst(3,3)]];

R=Mlast*Mfirstinv;
%R=Mlast/Mfirst;

r11=R(ind1,ind2);

end

function posdif=getposdif(r,idpos,r0,idpos0)

% [p0,of0]=atgeometry(r0,idpos0);
% [p,of]=atgeometry(r,idpos);

of=0;
of0=0;

p0=atgeometry(r0,idpos0,of0);
p=atgeometry(r,idpos,of);

xx=arrayfun(@(p)p.x,p);
yy=arrayfun(@(p)p.y,p);

xx0=arrayfun(@(p)p.x,p0);
yy0=arrayfun(@(p)p.y,p0);

%[t,R]=cart2pol(xx,yy);
%[t0,R0]=cart2pol(xx0,yy0);

%posdif=[(xx-xx0)./xx0;(yy-yy0)./yy0]';

posdif=abs([(xx-xx0);(yy-yy0)]');

%radius=sqrt(xx.^2+yy.^2);
%radius0=sqrt(xx0.^2+yy0.^2);
%posdif=((radius-radius0)./radius0)';

% L0=xx0.^2+yy0.^2
%posdif=[R-R0;t-t0]';

% [p0,of0]=atgeometry(r0,idpos0);
% [p,of]=atgeometry(r,idpos);
% posdif=of-of0;
end



