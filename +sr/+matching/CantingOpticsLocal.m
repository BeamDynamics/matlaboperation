function [rcm, arcc] = CantingOpticsLocal(rc,idnum,inittwiss,muxid,muyid,rm44_12,rm44_34, ploton, verbose)
%CantingOptics match optics for canted lattice rc to those of r at idnum
%
%Inputs: 
% rc: EBS  lattice with canted beamlines
% r : EBS lattice without canted beamlines
% ploton: print figures, true/false(default)
% verbose: print text, true/false(default)
% 
%Output:
% rmc: EBS lattice with canted bemalines matched. 
%
%Description:
% vary gradients of QF1, QD2, QD3, QF4A, QF4B, QD5, QF6 
% in couples about canted ID, to match: 
% 1) alpha_x, alpha_y, dispersion prime = 0 at ID canted, 
% 2) dx and dxp at 2 ID before canted ID, 
% 3) RM 12 and RM34 unmodified.
%
%see also: sr.matching.Canting sr.opticsmatching

% cantd cell before ID
[indidc,indidendc]=sr.getcellstartend(rc,idnum-1);

arcc=rc(indidc:indidendc);

% non canted ID center
%[indid,indidend]=sr.getcellstartend(r,idnum-1);
%arc0=r(indid:indidend);

% get target parametes (from lattice without canted beamline)
% [twi,~,~]=atlinopt(r,0,1:indidend+1);
% muxid=(twi(end).mu(1)-twi(indid).mu(1))/2/pi;
% muyid=(twi(end).mu(2)-twi(indid).mu(2))/2/pi;
% 
% inittwiss=twi(indid);
% inittwiss.mu=[0,0];

% get indexes to compute RM before and after optics matching
indsfcrm=[find(atgetcells(arcc,'FamName','SF2A'),1,'first');...
          find(atgetcells(arcc,'FamName','SF2E'),1,'first');...
         ];

% indsfrm=[find(atgetcells(arc0,'FamName','DR_10'));...
%          find(atgetcells(arc0,'FamName','DR_35'));...
%          ];
% 
% [lsf,~,~]=atlinopt(arc0,0,indsfrm);
% 
% rm44_12a= RM44(lsf,1,2);
% rm44_34a= RM44(lsf,3,4);

DispBumInd=indsfcrm;

% [lsf,~,~]=atlinopt(r,0,indidend+1);
% axend=lsf(end).alpha(1);
% ayend=lsf(end).alpha(2);
axend=0;
ayend=0;

% get quadrupoles about canted ID for matching

v=[];
v=[v atVariableBuilder(arcc,'QF1E',{'PolynomB',{1,2}},-5,5)];
v=[v atVariableBuilder(arcc,'QD2E',{'PolynomB',{1,2}},-5,5)];
v=[v atVariableBuilder(arcc,'QD3E',{'PolynomB',{1,2}},-5,5)];
v=[v atVariableBuilder(arcc,'QF4E',{'PolynomB',{1,2}},-5,5)];
v=[v atVariableBuilder(arcc,'QF4D',{'PolynomB',{1,2}},-5,5)];
v=[v atVariableBuilder(arcc,'QD5D',{'PolynomB',{1,2}},-5,5)];
v=[v atVariableBuilder(arcc,'QF6D',{'PolynomB',{1,2}},-5,5)];

arcend=length(arcc)+1;

muxconstr=struct(...
    'Fun',@(~,ld,~)mux(ld),...
    'Weight',1e-0,...
    'RefPoints',(1:arcend),...
    'Min',muxid,...
    'Max',muxid);

muycontsr=struct(...
    'Fun',@(~,ld,~)muy(ld),...
    'Weight',1e-0,...
    'RefPoints',(1:arcend),...
    'Min',muyid,...
    'Max',muyid);

rm12constr=struct(...
    'Fun',@(~,ld,~)RM44(ld,1,2),...
    'Weight',1e-0,...
    'RefPoints',[DispBumInd(1),DispBumInd(2)],...
    'Min',rm44_12,...
    'Max',rm44_12); % 1

rm34constr=struct(...
    'Fun',@(~,ld,~)RM44(ld,3,4),...
    'Weight',1e-0,...
    'RefPoints',[DispBumInd(1),DispBumInd(2)],...
    'Min',rm44_34,...
    'Max',rm44_34);%

c=[ atlinconstraint(arcend,{{'alpha',{1}}},axend,axend,1e-0)...
    atlinconstraint(arcend,{{'alpha',{2}}},ayend,ayend,1e-0)...
    atlinconstraint(arcend,{{'Dispersion',{2}}},0,0,1e-0)...
    muxconstr,... % horizontal phase advance
    muycontsr...  % vertical phase advance
    rm12constr,...
    rm34constr,...
    ];

% set some field in QD5B/D
indQD5B=find(atgetcells(arcc,'FamName','QD5B'));
indQD5D=find(atgetcells(arcc,'FamName','QD5D'));
indq1=indQD5D(find(indQD5D<indidc,1,'last'));
indq2=indQD5B(find(indQD5B>indidc,1,'first'));

qd5=[ indq1 indq2 ];

Kqd5=atgetfieldvalues(rc,qd5,'PolynomB',{1,2});
rcm=atsetfieldvalues(rc,qd5,'PolynomB',{1,2},Kqd5-0.0);

%rcm=atmatch(rcm,v,c,10^-15,200,3,@fminsearch);
verb=0;
if verbose, verb=3; end

arcc=atmatch(arcc,v,c,10^-15,50,verb,@lsqnonlin,inittwiss);

% set K in following cell
[indidc2,indidendc2]=sr.getcellstartend(rc,idnum);

arcc2=rc(indidc2:indidendc2);
arcc2 = setQFam(arcc,'QF1E',arcc2,'QF1A');
arcc2 = setQFam(arcc,'QD2E',arcc2,'QD2A');
arcc2 = setQFam(arcc,'QD3E',arcc2,'QD3A');
arcc2 = setQFam(arcc,'QF4E',arcc2,'QF4A');
arcc2 = setQFam(arcc,'QF4D',arcc2,'QF4B');
arcc2 = setQFam(arcc,'QD5D',arcc2,'QD5B');
arcc2 = setQFam(arcc,'QF6D',arcc2,'QF6B');

% reconstruct lattice with new cell
rcm=[rc(1:indidc-1);...
    arcc;...  % arcwith Canting
    arcc2;... % arcwith Canting assumes symmetry for canted cell. This is not ok for 2PW.
    rc(indidendc2+1:end)];

end


function arc2=setQFam(arc1,fam1,arc2,fam2)
% find first QF1A after indid and first QF1E before indid
% and set them in a variable
ind1=find(atgetcells(arc1,'FamName',[fam1 '']));
ind2=find(atgetcells(arc2,'FamName',[fam2 '']));
K1=atgetfieldvalues(arc1,ind1,'PolynomB',{1,2});
arc2=atsetfieldvalues(arc2,ind2,'PolynomB',{1,2},K1);
end

function m=mux(lindata)

m=lindata(end).mu(1)/2/pi;

end

function m=muy(lindata)

m=lindata(end).mu(2)/2/pi;

end

function [r11]=RM44(lindata,ind1,ind2)
% get value of of indeces ind1 and ind2 (1 to 4) of 
% M44 between two points first and last

Mlast=lindata(2).M44;
Mfirst=lindata(1).M44;

Mfirstinv=[[Mfirst(2,2),-Mfirst(1,2);-Mfirst(2,1),Mfirst(1,1)],...
            zeros(2,2);...
            zeros(2,2),...
            [Mfirst(4,4),-Mfirst(3,4);-Mfirst(4,3),Mfirst(3,3)]];

R=Mlast*Mfirstinv;
%R=Mlast/Mfirst;

r11=R(ind1,ind2);

end


