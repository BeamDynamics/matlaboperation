function [rok,LDR1,LDR2] = PlaceElementAtXY(...
    r,...
    ind,...
    Pos,...
    indDriftUp,...
    indDriftDown)
% PLACEELEMENTATXY place element of index ind at location POS=(X,Y) 
% by changing the lengths of the drifts indDriftUp indDriftDown
%
% INPUT:
% r             : Nx1 cell array, AT lattice
% ind           : 1x1 double, index to be placed at position Pos
% Pos           : 1x2 double, S position of index ind
% indDriftUp    : 1x1 double / cell array of Strings, UpStream Drift(s) to vary
% indDriftDown  : 1x1 double / cell array of Strings, DownStream Drift(s) to vary
%
% OUTPUT:
% rok           : Nx1 cell array, AT lattice with ind at position Pos
% 
% Example:
% >> Line = {atdrift('Up',1);atmarker('Element');atdrift('Down',1)};
% >> newLine = sr.matching.PlaceElementAtXY(Line,2,[0.5 0],{'Up'},{'Down'});
% 
%see also: atmatch atgeometry

% % % get drift indexes
if iscell(indDriftUp) 
    
    indDR1=findcells(r,'FamName',indDriftUp{:});
    
elseif isnumeric(indDriftUp) 
    
    indDR1 = indDriftUp;
    
else
    error('Drift UpStream must be either cell array or indexes')
end
    
if  iscell(indDriftDown)
    
    indDR2=findcells(r,'FamName',indDriftDown{:});
    
elseif  isnumeric(indDriftDown)
    
    indDR2 = indDriftDown;
    
else
    error('Drift DownStream must be either cell array or indexes')
end
    
% variable = drift lengths, keeping total constant
v=atVariableBuilder(r,...
    {@(ar,dl)VaryDriftLengths(ar,dl,indDR1,indDR2)},{0.0-1e-2});

% if strcmp(r{ind}.PassMethod,'IdentityPass')
%     r{ind}.PassMethod,'DriftPass';
% end
% 
% v2=atVariableBuilder(r,...
%     {@(ar,deltaL)atsetfieldvalues(ar,ind,'Length',deltaL)},{0.0});
% 
% constraint : position of ind is Pos
dif= getposdif(r,ind,Pos);
zz=zeros(size(dif));
oo= ones(size(dif));

c=struct(...
    'Fun',@(r,~,~)getposdif(r,ind,Pos),...
    'Weight',oo,...
    'RefPoints',[1],...
    'Min',zz,...
    'Max',zz...
    );


[rok,~,~]=atmatch(r,[v],c,10^-16,100,3,@lsqnonlin);
%[rok,~,d]=atmatch(r,[v,v2],c,10^-16,100,3,@fminsearch);


LDR1old = r{indDriftUp}.Length;
LDR2old = r{indDriftDown}.Length;

LDR1 = rok{indDriftUp}.Length;
LDR2 = rok{indDriftDown}.Length;

disp(['UpStream: ' num2str(LDR1old) ' -> ' num2str(LDR1)  ' delta: ' num2str(LDR1old-LDR1)]);
disp(['DownStream: ' num2str(LDR2old) ' -> ' num2str(LDR2)  ' delta: ' num2str(LDR2old-LDR2)]);

end


function ar=VaryDriftLengths(ar,dl,indDR1,indDR2)
% vary dirft length  keeping total length constant
DL1A=getcellstruct(ar,'Length',indDR1);
DL2A=getcellstruct(ar,'Length',indDR2);

ar=setcellstruct(ar,'Length',indDR1,DL1A+dl);
ar=setcellstruct(ar,'Length',indDR2,DL2A-dl); % all plus! 

end


function posdif=getposdif(r,idpos,Pos)

of=0;

p=atgeometry(r,idpos,of);

xx=arrayfun(@(p)p.x,p);
yy=arrayfun(@(p)p.y,p);

xx0=Pos(1);
yy0=Pos(2);

posdif=[xx-xx0;yy-yy0]';

end


    