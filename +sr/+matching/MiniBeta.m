function [ rcm ] = MiniBeta( r, idnum, minbeta_on, varargin )
% Mini-Beta inserts canting in ring 'r' at id 'idnum'
%
%Inputs:
% r: EBS upgrade lattice
% idnum: ID number where canting is done
% on_off: if true place magents and match strengths, 
%            false only place magnets with zero strengths. 
% 
%Optional Inputs:
% Lss : length of available straigth section
% ploton: prints out plots, true/false (default)
% verbose: prints matching process true/false (default)
%
%Output:
% r: EBS upgrade lattice with canting at id idnum
%
%Description:
% 1) Minibeta quadrupoles inserted in DR_01 DR_45
% 2) QF1, QD2, QD3, QF4, QD5, QF6
%    matched to flatten dispersion in following ID
%    note: this implies different beta at these IDs
% 3) (optional, SextFix) adapt sextupole strenghts about canted ID
%
%Example:
% >> rc = sr.matching.MniBeta(LOW_EMIT_RING_INJ,5,true,'ploton',true)
%
%Warning:
% 1) this function is not supposed to be used inside sr.model, as it changes
% the number of elements in the lattice
% 2) it assumes that cells IDx and IDx+1 are symmetric
% 
%see also: sr.matching.MiniBetaOptics 

% Canting parameters

Lss_def = 1.2961565*2; % m

p=inputParser;
addRequired(p,'r',@iscell);
addRequired(p,'idnum',@isnumeric);
addRequired(p,'minbeta_on',@islogical);
addOptional(p,'Lss',Lss_def,@isnumeric);
addOptional(p,'SextFix',false,@islogical); % removed from help, still to adapt
addOptional(p,'ploton',false,@islogical);
addOptional(p,'verbose',false,@islogical);

parse(p,r,idnum,minbeta_on,varargin{:});

Lss=p.Results.Lss;
SextFix=p.Results.SextFix;
ploton=p.Results.ploton;
verbose=p.Results.verbose;

% no canting in injection cells.
if ismember(idnum, [3 4 5 7 25])
    error('Minibeta Forbidded for cells 3,4,5,7,25');
end

% get cell index
[indid,~]=sr.getcellstartend(r,idnum);

% add minibeta qaudrupoles about ID center
QS3mb=r{find(atgetcells(r,'FamName','QF1A\w*'),1,'first')};
QS3mb.FamName = 'QS3';
QS3mb.PolynomB(2) = 0.0;
QS1mb=r{find(atgetcells(r,'FamName','QD2A\w*'),1,'first')};
QS1mb.FamName = 'QS1';
QS1mb.PolynomB(2) = 0.0;

drl = r{indid-1}.Length - Lss/2;
L2 = drl/2 - 0.0103; % distance QF1 - QF1mb
L1 = drl/2 + 0.0103; % distance QF1mb - QD2mb

DRmb=r{find(atgetcells(r,'FamName','DR_45\w*'),1,'first')};

DR0mb = DRmb; 
DR0mb.Length = Lss/2; %-L1-L2;
DR0mb.FamName = 'DR0mb';

DR1mb = DRmb; 
DR1mb.Length = L1-QS1mb.Length;
DR1mb.FamName = 'DR1mb';

DR2mb = DRmb; 
DR2mb.Length = L2-QS3mb.Length;
DR2mb.FamName = 'DR2mb';

rc=r;
minibetamark=rc{indid};
minibetamark.FamName=['MiniBetaID' num2str(idnum,'%.2d')];
rc=[rc(1:indid-2);...
    {DR2mb};{QS3mb};{DR1mb};{QS1mb};{DR0mb};...
    rc(indid);minibetamark;...
    {DR0mb};{QS1mb};{DR1mb};{QS3mb};{DR2mb};...
    rc(indid+2:end)];

if minbeta_on
    % rematch optics using quadrupoles.
    rcm=sr.matching.MiniBetaOptics(rc,r,idnum,ploton,verbose);
else
    rcm = rc ;
end

%% sextupole adjustment
if SextFix

    error('This option is not updated for MiniBeta Optics')
    indidc=indid+1;
    
    addpath('/machfs/liuzzo/EBS/S28D/LATTICE/AT/InjectionSextupoles');
    ringi=rcm; % lattice to be corrected
    ring0=r;   % reference lattice
    
    indSext0=find(atgetcells(ring0,'FamName','S[FDIJ]\w*'))';%nSext0=length(indSext0);
    indSexti=find(atgetcells(ringi,'FamName','S[FDIJ]\w*'))';%nSexti=length(indSexti);
    indmoni0=find(atgetcells(ring0,'Class','Monitor'))';
    indmonii=find(atgetcells(ringi,'Class','Monitor'))';
    
    % this are the indices where you want to have the quantities corrected.
    % I want to correct in the cells 2:31, in all the sextupoles and in all the
    % bpm
    indWhereCorr0=[ indSext0([1:(find(indSext0<indid,1,'last')-3),(find(indSext0>indid,1,'first')+3):end]),...
        indmoni0([1:(find(indmoni0<indid,1,'last')-5),(find(indmoni0>indid,1,'first')+5):end]) ];
    indWhereCorri=[ indSexti([1:(find(indSexti<indidc,1,'last')-3),(find(indSexti>indidc,1,'first')+3):end]),...
        indmonii([1:(find(indmonii<indidc,1,'last')-5),(find(indmonii>indidc,1,'first')+5):end]) ];
    
    % I put the sextupoles of ring0 in the ringi lattice
    indSextCant0=((find(indSext0<indid,1,'last')-2):(find(indSext0>indid,1,'first')+2));
    indSextCanti=((find(indSexti<indidc,1,'last')-2):(find(indSexti>indidc,1,'first')+2));
    sextinj_noinj=atgetfieldvalues(ring0,indSext0(indSextCant0),'PolynomB',{1,3});
    ringi_wrongSext=atsetfieldvalues(ringi,indSexti(indSextCanti),'PolynomB',{1,3},sextinj_noinj);
    
    indQuad0=findQuad(ring0); %nquad0=length(indQuad0);
    indQuadi=findQuad(ringi); %nquadi=length(indQuadi);
    indoct0=[];indocti=[];
    
    % Weights=[...
    %    0;0; 0;0; 0;0; 0;0; 0;0;...
    %    1;1; ...
    %    1;1; 1;1; 1;1; ...
    %    1;1;0 ];
    
    Weights=[...
        0;0; 0;0; 0;0; 0;0; 0;0;...
        10000;10000; ...
        1;1; 1;1; 1;1; ...
        1;1;0 ];
    
    % The cells you want to have with different sextupoles, [1 32] is my
    % personal suggestion, but you can try [1 4 29 32] as it was in past
    
    indSextCorr=indSextCanti;
    
    [ ringicor, b3L_Corr, L_corr ]= ...
        CalcSextCorrections(...
        ring0,ringi_wrongSext,...
        indWhereCorr0,indWhereCorri,...
        indQuad0,indQuadi,...
        indSext0,indSexti,...
        indoct0,indocti,...
        indSextCorr,...
        Weights,.9);
    
    s0=[1,2,3,6,7,8]/10;
    s=[];
    DeltaSext=zeros(1,length(indSexti));
    for i=1:length(indSexti)
        DeltaSext(i)=ringicor{indSexti(i)}.PolynomB(1,3)-ring0{indSext0(i)}.PolynomB(1,3);
        disp([ringicor{indSexti(i)}.FamName ' : '...
            num2str(ringicor{indSexti(i)}.PolynomB(1,3),'%2.3f') ' -> '...
            num2str(ring0{indSext0(i)}.PolynomB(1,3),'%2.3f') ' Delta '...
            num2str(DeltaSext(i),'%2.3f') ' 1/m3 '...
            ])
    end
    
    if ploton
        
        figure; hold on; grid on;
        for i=1:32
            line([i i], [-10 10],'LineWidth',1,'Color',[.8 .8 .8]);
            s=[s,s0+i-1];
        end
        bar(s,DeltaSext,'r');
        xlim([0,32]);
        xlabel('Cell number');
        ylabel('Sextupole change (m^{-3})')
        storefigure('SextupoleVariations')
        %  figure; atplot(ring0,@plotRDT_rm,'geometric1');
        %  figure; atplot(ringi_wrongSext,@plotRDT_rm,'geometric1');
        %  figure; atplot(ringicor,@plotRDT_rm,'geometric1');
        
        figure; atplot(ring0,'comment',[],@plotChromFunctions,[1,2]); storefigure('NominalChromfunctions');
        figure; atplot(ringi_wrongSext,'comment',[],@plotChromFunctions,[1,2]); storefigure('CantingChromfunctions');
        figure; atplot(ringicor,'comment',[],@plotChromFunctions,[1,2]);  storefigure('CantingAdjustedChromfunctions');
        
        figure; atplot(ring0,'comment',[],@plotWdispP); storefigure('NominalWfunctions');
        figure; atplot(ringi_wrongSext,'comment',[],@plotWdispP); storefigure('CantingWfunctions');
        figure; atplot(ringicor,'comment',[],@plotWdispP);  storefigure('CantingAdjustedWfunctions');
        
        % store corrections
        rcm=ringicor;
    end
    
end

if ploton
    % show canting and matching
    nelshow=150;
    figure('Units','normalized','Position',[0.3 0.2 0.6 0.6]);
    atplot(r,[findspos(r,dip10-nelshow) findspos(r,dip30+nelshow)],'comment',[]);
    storefigure(['nominalID' num2str(idnum,'%.2d')]);
    figure('Units','normalized','Position',[0.3 0.2 0.6 0.6]);
    atplot(rc,[findspos(rc,dip1-nelshow) findspos(rc,dip3+nelshow)],'comment',[]);
    storefigure(['CantedID' num2str(idnum,'%.2d')]);
    figure('Units','normalized','Position',[0.3 0.2 0.6 0.6]);
    atplot(rcm,[findspos(rcm,dip1-nelshow) findspos(rcm,dip3+nelshow)],'comment',[]);
    storefigure(['MatchedCantedID' num2str(idnum,'%.2d')]);
    
    %
    [twi,~,~]=atlinopt(r,0,dip10+(-nelshow:nelshow));
    s0=arrayfun(@(a)a.SPos,twi,'un',1);
    bx0=arrayfun(@(a)a.beta(1),twi,'un',1);
    by0=arrayfun(@(a)a.beta(2),twi,'un',1);
    dx0=arrayfun(@(a)a.Dispersion(1),twi,'un',1);
    [twic,~,~]=atlinopt(rc,0,dip1+(-nelshow:nelshow));
    sc=arrayfun(@(a)a.SPos,twic,'un',1);
    bxc=arrayfun(@(a)a.beta(1),twic,'un',1);
    byc=arrayfun(@(a)a.beta(2),twic,'un',1);
    dxc=arrayfun(@(a)a.Dispersion(1),twic,'un',1);
    [twim,~,~]=atlinopt(rcm,0,dip1+(-nelshow:nelshow));
    sm=arrayfun(@(a)a.SPos,twim,'un',1);
    bxm=arrayfun(@(a)a.beta(1),twim,'un',1);
    bym=arrayfun(@(a)a.beta(2),twim,'un',1);
    dxm=arrayfun(@(a)a.Dispersion(1),twim,'un',1);
    
    %
    figure('Units','normalized','Position',[0.3 0.2 0.6 0.6]);
    subplot(3,1,1);
    plot(s0,bx0,'-','LineWidth',2); hold on;
    plot(sc,bxc,'--','LineWidth',2); hold on;
    plot(sm,bxm,'-.','LineWidth',2); hold on;
    ylabel('\beta_x [m]');
    legend('Nominal','Canting','Matched');
    subplot(3,1,2);
    plot(s0,by0,'-','LineWidth',2); hold on;
    plot(sc,byc,'--','LineWidth',2); hold on;
    plot(sm,bym,'-.','LineWidth',2); hold on;
    ylabel('\beta_y [m]');
    subplot(3,1,3);
    plot(s0,dx0,'-','LineWidth',2); hold on;
    plot(sc,dxc,'--','LineWidth',2); hold on;
    plot(sm,dxm,'-.','LineWidth',2); hold on;
    ylabel('\eta_x [m]');
    xlabel('s [m]');
    
    storefigure('MatchedCantingOptics')
    % % whole lattice
    [twi,~,~]=atlinopt(r,0,atgetcells(r,'Class','Quadrupole','Sextupole','Monitor','Drift'));
    s0=arrayfun(@(a)a.SPos,twi,'un',1);
    bx0=arrayfun(@(a)a.beta(1),twi,'un',1);
    by0=arrayfun(@(a)a.beta(2),twi,'un',1);
    dx0=arrayfun(@(a)a.Dispersion(1),twi,'un',1);
    [twic,~,~]=atlinopt(rc,0,atgetcells(rc,'Class','Quadrupole','Sextupole','Monitor','Drift'));
    sc=arrayfun(@(a)a.SPos,twic,'un',1);
    bxc=arrayfun(@(a)a.beta(1),twic,'un',1);
    byc=arrayfun(@(a)a.beta(2),twic,'un',1);
    dxc=arrayfun(@(a)a.Dispersion(1),twic,'un',1);
    [twim,~,~]=atlinopt(rcm,0,atgetcells(rcm,'Class','Quadrupole','Sextupole','Monitor','Drift'));
    sm=arrayfun(@(a)a.SPos,twim,'un',1);
    bxm=arrayfun(@(a)a.beta(1),twim,'un',1);
    bym=arrayfun(@(a)a.beta(2),twim,'un',1);
    dxm=arrayfun(@(a)a.Dispersion(1),twim,'un',1);
    
    figure('Units','normalized','Position',[0.3 0.1 0.6 0.8]);
    ax1=subplot(3,1,1);
    %plot(s0,bx0,'-','LineWidth',2); hold on;
    plot(sc,(bxc-bx0)./bx0,'--','LineWidth',2); hold on;
    plot(sm,(bxm-bx0)./bx0,'-.','LineWidth',2); hold on;
    ylabel('(\beta_x-\beta_{x,0})/\beta_{x,0} [m]');
    legend('Canting','Matched');
    ax2=subplot(3,1,2);
    %plot(s0,by0,'-','LineWidth',2); hold on;
    plot(sc,(byc-by0)./by0,'--','LineWidth',2); hold on;
    plot(sm,(bym-by0)./by0,'-.','LineWidth',2); hold on;
    ylabel('(\beta_y-\beta_{y,0})/\beta_{y,0} [m]');
    ax3=subplot(3,1,3);
    %plot(s0,dx0,'-','LineWidth',2); hold on;
    plot(sc,(dxc-dx0),'--','LineWidth',2); hold on;
    plot(sm,(dxm-dx0),'-.','LineWidth',2); hold on;
    ylabel('\eta_x -\eta_{x,0} [m]');
    xlabel('s [m]');
    storefigure('MatchedCantingOpticsDistortion')
    
    linkaxes([ax1,ax2,ax3],'x')
    xlim([sid-20,sid+20]);
    storefigure('MatchedCantingOpticsDistortionZoom')
end


end
