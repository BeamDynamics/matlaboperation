function rcm = MiniBetaOptics(rc,r,idnum, ploton, verbose)
%CantingOptics match optics for canted lattice rc to those of r at idnum
%
%Inputs: 
% rc: EBS  lattice with minibeta beamlines
% r : EBS lattice without minibeta beamlines
% ploton: print figures, true/false(default)
% verbose: print text, true/false(default)
% 
%Output:
% rmc: EBS lattice with canted bemalines matched. 
%
%Description:
% vary gradients of QF1, QD2, QD3, QF4A, QF4B, QD5, QF6 
% in couples about minibeta ID, to match: 
% 1) beta_x, beta_y, dispersion prime = 0 at ID minibeta, 
% 2) dx and dxp at 2 ID before canted ID, 
% 3) RM 12 and RM34 unmodified.
%
%see also: sr.matching.MiniBeta sr.opticsmatching

% minibeta beamline center
[indidc,~]=sr.getcellstartend(rc,idnum);

% non canted ID center
[indid,~]=sr.getcellstartend(r,idnum);

% index of id before
[indidbc,~]=sr.getcellstartend(rc,idnum-2);

% get target parametes (from lattice without canted beamline)
[twi,~,~]=atlinopt(r,0,1:indid);
muxid=twi(end).mu(1)/2/pi;
muyid=twi(end).mu(2)/2/pi;

% get indexes to compute RM before and after optics matching
indsfcrm=[find(atgetcells(rc(1:indidc),'FamName','SF2[A]'),1,'last');...
          find(atgetcells(rc(1:indidc),'FamName','SF2[E]'),1,'last')];

indsfrm=[find(atgetcells(r(1:indid),'FamName','SF2[A]'),1,'last');...
         find(atgetcells(r(1:indid),'FamName','SF2[E]'),1,'last')];

lsf=atlinopt(r,0,indsfrm);

rm44_12a=RM44(lsf,1,2);
rm44_34a=RM44(lsf,3,4);

DispBumInd=indsfcrm;

% get quadrupoles about canted ID for matching

k1q1 = -2.73195e+00;
k1q3 = 2.75930e+00;
k1qf1 = 6.03851e-01;
k1qd2 = -2.20133e+00;
k1qd3 = -2.40325e+00;

v=[];
% find first QF1A after indid and first QF1E before indid
[v12,~,~]=getqvariab(rc,'QF1A','QF1E',indidc);
v=[v,v12];
rc = atsetfieldvalues(rc,v12.Indx,'PolynomB',{1,2},k1qf1);

[v12,~,~]=getqvariab(rc,'QD2A','QD2E',indidc);
v=[v,v12];
rc = atsetfieldvalues(rc,v12.Indx,'PolynomB',{1,2},k1qd2);

[v12,~,~]=getqvariab(rc,'QD3A','QD3E',indidc);
v=[v,v12];
rc = atsetfieldvalues(rc,v12.Indx,'PolynomB',{1,2},k1qd3);

[v12,~,~]=getqvariab(rc,'QS1','QS1',indidc);
v=[v,v12];
rc = atsetfieldvalues(rc,v12.Indx,'PolynomB',{1,2},k1q1);

[v12,~,~]=getqvariab(rc,'QS3','QS3',indidc);
v=[v,v12];
rc = atsetfieldvalues(rc,v12.Indx,'PolynomB',{1,2},k1q3);


muxconstr=struct(...
    'Fun',@(~,ld,~)mux(ld),...
    'Weight',1e-3,...1,...
    'RefPoints',(1:indidc+1),...
    'Min',muxid,...
    'Max',muxid);

muycontsr=struct(...
    'Fun',@(~,ld,~)muy(ld),...
    'Weight',1e-3,...1,...
    'RefPoints',(1:indidc+1),...
    'Min',muyid,...
    'Max',muyid);

rm12constr=struct(...
    'Fun',@(~,ld,~)RM44(ld,1,2),...
    'Weight',1e-0,...
    'RefPoints',[DispBumInd(1),DispBumInd(2)],...
    'Min',rm44_12a,...
    'Max',rm44_12a); % 1

rm34constr=struct(...
    'Fun',@(~,ld,~)RM44(ld,3,4),...
    'Weight',1e-0,...
    'RefPoints',[DispBumInd(1),DispBumInd(2)],...
    'Min',rm44_34a,...
    'Max',rm44_34a);%

c=[ atlinconstraint(indidc,{{'beta',{1}}},5.802,5.802,1e-2)...1)...%
    atlinconstraint(indidc,{{'beta',{2}}},0.9655,0.9655,1e-2)...1)...%
    atlinconstraint(indidc,{{'alpha',{1}}},0,0,1e-2)...1)...%
    atlinconstraint(indidc,{{'alpha',{2}}},0,0,1e-2)...1)...%
    atlinconstraint(indidc,{{'Dispersion',{2}}},0,0,1e-2)...1)...%
    atlinconstraint(indidbc,{{'alpha',{1}}},0,0,1e-2)...1)...%
    atlinconstraint(indidbc,{{'alpha',{2}}},0,0,1e-2)...1)...%
    atlinconstraint(indidbc,{{'Dispersion',{2}}},0,0,1e-2)...1)...%
    ...muxconstr,... % horizontal phase advance
    ...muycontsr...  % vertical phase advance
    ];



[twiin, ~, ~ ] = atlinopt(r,0,1);
verb=0;
if verbose, verb=3; end

rcm=atmatch(rc,v,c,10^-12,100,verb,@lsqnonlin,twiin); 


end


function [v,vq1,vq2]=getqvariab(rc,famname1,famname2,indidc)
% find first QF1A after indid and first QF1E before indid
% and set them in a variable
indQF1A=find(atgetcells(rc,'FamName',[famname1 '']));
indQF1E=find(atgetcells(rc,'FamName',[famname2 '']));
indq1=indQF1E(find(indQF1E<indidc,1,'last'));
indq2=indQF1A(find(indQF1A>indidc,1,'first'));

q=[ indq1 indq2 ];

v=atVariableBuilder(rc,q,{'PolynomB',{1,2}});
vq1=atVariableBuilder(rc,indq1,{'PolynomB',{1,2}},-3,3);
vq2=atVariableBuilder(rc,indq2,{'PolynomB',{1,2}},-3,3);
end

function m=mux(lindata)

m=lindata(end).mu(1)/2/pi;

end

function m=muy(lindata)

m=lindata(end).mu(2)/2/pi;

end


function [r11]=RM44(lindata,ind1,ind2)
% get value of of indeces ind1 and ind2 (1 to 4) of 
% M44 between two points first and last

Mlast=lindata(2).M44;
Mfirst=lindata(1).M44;

Mfirstinv=[[Mfirst(2,2),-Mfirst(1,2);-Mfirst(2,1),Mfirst(1,1)],...
            zeros(2,2);...
            zeros(2,2),...
            [Mfirst(4,4),-Mfirst(3,4);-Mfirst(4,3),Mfirst(3,3)]];

R=Mlast*Mfirstinv;
%R=Mlast/Mfirst;

r11=R(ind1,ind2);

end

