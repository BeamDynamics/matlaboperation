function out=skewname(idx,format)
%SKEWNAME	returns the name of Steerer with index idx
%
%STEERNAME=SKEWNAME(IDX,format)		returns the name of skew with index IDX
% IDX: vector of indexes in the range 1:288 (1 is sqp-sh1/c04-a)
%       logical array of length 
%       or n x 2 matrix in the form [cell index] with
%       or 1 x n array for the names of n magnets
%       cell in 1:32, index in 1:9
%                   
%[NM1,NM2...]=SKEWNAME(...)		May be used to generate several names
%
% for skew quadrupole names
% sr.skewname([1,23,59]) % array
% sr.skewname([23,1]) % cell idx
% 
%   See also sr.STEERNAME
if nargin==1
    format = '';
end

out=sr.steername(idx,[format 'sqp-']);

end
