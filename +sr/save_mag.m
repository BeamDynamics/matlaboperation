function save_mag(filename,strength,varargin)
%sr.SAVE_MAG    Store the family currents in a text file
%
%sr.SAVE_MAG(FILENAME,STRENGTH)
%
%STRENGTHS:  Family strengths [rad, m^-1, m^-2]
%
%see also sr.magname sr.read_mag

% no currents for devices in sr.
%sr.SAVE_MAG(FILENAME,CURRENTS)
%
%CURRENTS:  Family currents [A]

devlist=sr.magname();
if ~isempty(varargin)
    Bro=6.00e9/2.99792458e8;
    %strength=cellfun(@(nm,cur) gl2i(nm,cur),devlist,num2cell(strength.*Bro));
end

[fp,message]=fopen(filename,'wt');
if fp < 0
    error('File:Open','%s: %s',message,filename);
else
    for dev=1:length(devlist)
        fprintf(fp,'%s\t%.6f\n',devlist{dev},strength(dev));
    end
    fclose(fp);
end
end

