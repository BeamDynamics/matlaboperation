function save_correction(filename,devlist,strength,varargin)
%SAVE_CORRECTION(filename,devlist,strength)
%devlist : device names (sr.skewname, sr.quadname....)
%STRENGTHS:	corrector strengths

[fp,message]=fopen(filename,'wt');
if fp < 0
    error('File:Open','%s: %s',message,filename);
else
    for dev=1:length(devlist)
        fprintf(fp,'%s\t%.6f\n',devlist{dev},strength(dev));
    end
    fclose(fp);
end
