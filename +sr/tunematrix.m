function matr = tunematrix(atmodel)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

s=warning('off','atmodel:getid');
famtunes=cellfun(@getsum,{'QF1[AE]','QD2[AE]','QD3[AE]','QF4[ABDE]','QD5[BD]',...
    'QF6[BD]','QF8[BD]','QF1[IJ]','QF2[IJ]','QD2[IJ]','QD3[IJ]','QF4[IJ]'},...
    'UniformOutput',false);
warning(s.state,s.identifier);
matr=cat(2,famtunes{:});

    function v=getsum(famname)
        bxbz=sum(atmodel.get([3 8],famname));
        v=[bxbz(:,1) -bxbz(:,2)]'/4/pi;
    end
end
