function [resp,devs]=chromresponse(atmodel)

% get initial tunes
r0 = atradoff(atmodel.ring);

indsext = find(atgetcells(r0,'Class','Sextupole'))';
Lsext = atgetfieldvalues(r0,indsext,'Length');

Sext0 = atgetfieldvalues(r0,indsext,'PolynomB',{1,3}).*Lsext;

% get initial chromaticity
[~,~,Q0]=atlinopt(r0,0,1);

chrommode='SF2SD1';
%chrommode='InjSext';

Mat=[];

% change horizontal chromaticity
DQ=.1;

Q1=Q0+[DQ 0.0];

latnewchrom= atfitchrom(r0,Q1,'S[IJD]1\w*','S[IJF]2\w*');
SextP = atgetfieldvalues(latnewchrom,indsext,'PolynomB',{1,3}).*Lsext;

DGL=SextP - Sext0;

Mat(:,1)=DGL/DQ;

% change vertical chromaticity

Q1=Q0+[0.0 DQ];

latnewchrom= atfitchrom(r0,Q1,'S[IJD]1\w*','S[IJF]2\w*');
SextP = atgetfieldvalues(latnewchrom,indsext,'PolynomB',{1,3}).*Lsext;

DGL = SextP - Sext0;

Mat(:,2)=DGL/DQ;

% select changed gradients and get device familiy names.
%[uMat,iq,~]=unique(Mat,'rows');
%selnonzero=uMat(:,1)~=0 & uMat(:,2)~=0;
%devs=sr.sxfamname(iq(selnonzero));
%resp=[uMat(selnonzero,1),uMat(selnonzero,2)];
devs=sr.sxname(1:length(DGL));
resp=Mat;

% save file .csv

chromfile = 'chrommat';
fff=fopen([chromfile '.csv'],'w+');
for iq=1:length(devs)
    fprintf(fff,'%s,%2.10f,%2.10f,\n',devs{iq},resp(iq,1),resp(iq,2));
end
fclose(fff);

% save matlab file
DKL_DChromaticty = resp; %#ok<NASGU>
save(chromfile,'DKL_DChromaticty');
end
