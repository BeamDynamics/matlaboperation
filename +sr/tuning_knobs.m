function tuning_knobs()
% prepare files for tuning knobs devices

% knob SD1
name = 'SD1AE';
allname = 'srmag/m-s/all';
ind = sort([1:6:192,6:6:192]);
val= zeros(1,192);
val(ind)=1;
ff =fopen([name '_knob.txt'],'w');
fprintf(ff,'%s\n',allname);
fprintf(ff,'%f\n',val);
fclose(ff);




% knob SD1
name = 'SD1BD';

allname = 'srmag/m-s/all';
ind = sort([3:6:192,4:6:192]);
val= zeros(1,192);
val(ind)=1;
ff =fopen([name '_knob.txt'],'w');
fprintf(ff,'%s\n',allname);
fprintf(ff,'%f\n',val);
fclose(ff);



% knob SF2
name = 'SF2';
allname = 'srmag/m-s/all';
ind = sort([2:6:192,5:6:192]);
val= zeros(1,192);
val(ind)=1;
ff =fopen([name '_knob.txt'],'w');
fprintf(ff,'%s\n',allname);
fprintf(ff,'%f\n',val);
fclose(ff);


% knob 4cells
name = 'Sext4CellsV1';
allname = 'srmag/m-s/all';
Nallsext=192;
sel = sort([2:6:Nallsext, 5:6:Nallsext]); % select some sextupoles amond all
Nsext=length(sel);
ind = 1:Nsext;
vals = - ones(size(ind));
vals([1:8:Nsext,3:8:Nsext,6:8:Nsext,8:8:Nsext]) = +1;
val= zeros(1,Nallsext);
val(sel)=vals;
ff =fopen([name '_knob.txt'],'w');
fprintf(ff,'%s\n',allname);
fprintf(ff,'%f\n',val);
fclose(ff);



% knob 4cells
name = 'Sext4CellsV2';

allname = 'srmag/m-s/all';
Nallsext=192;
sel = sort([2:6:Nallsext, 5:6:Nallsext]); % select some sextupoles amond all
Nsext=length(sel);
ind = 1:Nsext;
vals = - ones(size(ind));
vals([1:8:Nsext,8:8:Nsext]) = +2;
vals([2:8:Nsext,7:8:Nsext]) = +1;
vals([3:8:Nsext,6:8:Nsext]) = -1;
vals([4:8:Nsext,5:8:Nsext]) = -2;
val= zeros(1,Nallsext);
val(sel)=vals;
ff =fopen([name '_knob.txt'],'w');
fprintf(ff,'%s\n',allname);
fprintf(ff,'%f\n',val);
fclose(ff);



% knob 4cells
name = 'Oct4CellsV1';

allname = 'srmag/m-o/all';
Nall=64;
sel = 1:Nall; % select some sextupoles amond all
N=length(sel);
ind = 1:N;
vals = - ones(size(ind));
vals([1:8:N,3:8:N,6:8:N,8:8:N]) = +1;
val= zeros(1,Nall);
val(sel)=vals;
ff =fopen([name '_knob.txt'],'w');
fprintf(ff,'%s\n',allname);
fprintf(ff,'%f\n',val);
fclose(ff);

% knob 4cells
name = 'Oct4CellsV2';
allname = 'srmag/m-o/all';
Nall=64;
sel = 1:Nall; % select some sextupoles amond all
N=length(sel);
ind = 1:N;
vals = ones(size(ind));
vals([1:8:N,8:8:N]) = +2;
vals([2:8:N,7:8:N]) = +1;
vals([3:8:N,6:8:N]) = -1;
vals([4:8:N,5:8:N]) = -2;
val= zeros(1,Nall);
val(sel)=vals;
ff =fopen([name '_knob.txt'],'w');
fprintf(ff,'%s\n',allname);
fprintf(ff,'%f\n',val);
fclose(ff);



% knobs inj cells
name = 'SD1AEInjInt';
ind = sort([1 192]);
allname = 'srmag/m-s/all';
Nall=192;
val= zeros(1,Nall);
val(ind)=1;
ff =fopen([name '_knob.txt'],'w');
fprintf(ff,'%s\n',allname);
fprintf(ff,'%f\n',val);
fclose(ff);


name = 'SF2InjInt';
ind = sort([2 191]);
allname = 'srmag/m-s/all';
Nall=192;
val= zeros(1,Nall);
val(ind)=1;
ff =fopen([name '_knob.txt'],'w');
fprintf(ff,'%s\n',allname);
fprintf(ff,'%f\n',val);
fclose(ff);


name = 'SD1BDInjInt';
ind = sort([3 190]);
allname = 'srmag/m-s/all';
Nall=192;
val= zeros(1,Nall);
val(ind)=1;
ff =fopen([name '_knob.txt'],'w');
fprintf(ff,'%s\n',allname);
fprintf(ff,'%f\n',val);
fclose(ff);

name = 'SD1BDInjExt';
ind = sort([4 189]);
allname = 'srmag/m-s/all';
Nall=192;
val= zeros(1,Nall);
val(ind)=1;
ff =fopen([name '_knob.txt'],'w');
fprintf(ff,'%s\n',allname);
fprintf(ff,'%f\n',val);
fclose(ff);

name = 'SF2InjExt';
ind = sort([5 188]);
allname = 'srmag/m-s/all';
Nall=192;
val= zeros(1,Nall);
val(ind)=1;
ff =fopen([name '_knob.txt'],'w');
fprintf(ff,'%s\n',allname);
fprintf(ff,'%f\n',val);
fclose(ff);

name = 'SD1AEInjExt';
ind = sort([6 187]);
allname = 'srmag/m-s/all';
Nall=192;
val= zeros(1,Nall);
val(ind)=1;
ff =fopen([name '_knob.txt'],'w');
fprintf(ff,'%s\n',allname);
fprintf(ff,'%f\n',val);
fclose(ff);


name = 'OFInt';
ind = sort([1 64]);
allname = 'srmag/m-o/all';
Nall=64;
val= zeros(1,Nall);
val(ind)=1;
ff =fopen([name '_knob.txt'],'w');
fprintf(ff,'%s\n',allname);
fprintf(ff,'%f\n',val);
fclose(ff);


name = 'OFExt';
ind = sort([2 63]);
allname = 'srmag/m-o/all';
Nall=64;
val= zeros(1,Nall);
val(ind)=1;
ff =fopen([name '_knob.txt'],'w');
fprintf(ff,'%s\n',allname);
fprintf(ff,'%f\n',val);
fclose(ff);


% fixed chromaticity unbalance
name = 'SD1unbalance';
Nall=192;
allname = 'srmag/m-s/all';
ind = 1:Nall;
val= zeros(1,Nall);

val([1:6:Nall,6:6:Nall]) = +1;
val([3:6:Nall,4:6:Nall]) = -1;

ff =fopen([name '_knob.txt'],'w');
fprintf(ff,'%s\n',allname);
fprintf(ff,'%f\n',val);
fclose(ff);


end
