function resp = orm(ring,plane,stidx,bpmidx)
%ATORM Compute an orbit response matrix [m/rad]
%
% RESPONSE=ATORM(RING,PLANE,STIDX,BPMIDX)
%
%RING:  AT structure
%PLANE: plane selection, must be  h|H|x|X|1 or v|V|z|Z|2
%STIDX: steerer selection (array of indexes or logical mask)
%BPMIDX:BPM selection (array of indexes or logical mask)
%
%The PassMethod of steerers must be CorrectorPass or StrMPoleSymplectic4Pass

persistent sleh slev
if isempty(sleh)
    sleh.Corrector=@hcorrector;
    sleh.Bend=@hmultipole;
    sleh.Sextupole=@hmultipole;
    sleh.Multipole=@hmultipole;
    sleh.Octupole=@hmultipole;
    sleh.Quadrupole=@hquadmotion;  
    slev.Corrector=@vcorrector;
    slev.Bend=@vmultipole;
    slev.Sextupole=@vmultipole;
    slev.Multipole=@vmultipole;
    slev.Octupole=@vmultipole;
    slev.Quadrupole=@vquadmotion;  
    
end

if islogical(stidx)
    stidx=find(stidx);
end
orbit=selectplane(plane,{@(st) horbit(st,0.33e-4),@(st) vorbit(st,0.25e-4)});
res=arrayfun(orbit,stidx,'UniformOutput',false);
resp=cat(2,res{:});

    function r=horbit(idst,kick)
        elsave=ring{idst};
        ring{idst}=sleh.(elsave.Class)(elsave,-0.5*kick);
        orbp=findsyncorbit(ring,0,bpmidx);
        ring{idst}=sleh.(elsave.Class)(elsave,+0.5*kick);
        orbm=findsyncorbit(ring,0,bpmidx);
        ring{idst}=elsave;
        r=(orbp(1,:)-orbm(1,:))'/kick;
    end

    function r=vorbit(idst,kick)
       elsave=ring{idst};
        ring{idst}=slev.(elsave.Class)(elsave,-0.5*kick);
        orbp=findsyncorbit(ring,0,bpmidx);
        ring{idst}=slev.(elsave.Class)(elsave,+0.5*kick);
        orbm=findsyncorbit(ring,0,bpmidx);
        ring{idst}=elsave;
        r=(orbp(3,:)-orbm(3,:))'/kick;
    end

    function el=hcorrector(el,kh)
        
         [KH,KV]=kick_transformations({el},1,kh,0);

         el.KickAngle(1)=el.KickAngle(1)+KH;
         el.KickAngle(2)=el.KickAngle(2)+KV;

       
    end
    function el=hmultipole(el,kh)
        
        [KH,KV]=kick_transformations({el},1,kh,0);

        ll=el.Length;
        el.PolynomB(1)=el.PolynomB(1)-KH/ll;
        el.PolynomA(1)=el.PolynomA(1)+KV/ll;
    end

    function el=hquadmotion(el0,kh)
        el=atshiftelem(el0,kh,0);
    end
    function el=vcorrector(el,kv)
        
         [KH,KV]=kick_transformations({el},1,0,kv);
                
         el.KickAngle(1)=el.KickAngle(1)+KH;
         el.KickAngle(2)=el.KickAngle(2)+KV;

    end
    function el=vmultipole(el,kv)
        
        [KH,KV]=kick_transformations({el},1,0,kv);
        
        ll=el.Length;
        el.PolynomB(1)=el.PolynomB(1)-KH/ll;
        el.PolynomA(1)=el.PolynomA(1)+KV/ll;
        
    end
    function el=vquadmotion(el0,kv)
        el=atshiftelem(el0,0,kv);
    end
end


    function [KH,KV]= kick_transformations(r,st,kh,kv)
               
        
        if any(atgetcells(r(st),'KickAngleRotation'))
            
            Krot = atgetfieldvalues(r,st,'KickAngleRotation');
            
            kh = kh .* cos(Krot) - kv .* sin(Krot);
            kv = kh .* sin(Krot) + kv .* cos(Krot);
            
        end
        
        if any(atgetcells(r(st),'KickAngleScale'))
            KHscale = atgetfieldvalues(r,st,'KickAngleScale',{1,1});
            KVscale = atgetfieldvalues(r,st,'KickAngleScale',{1,2});
            
            kh = kh .* KHscale;
            kv = kv .* KVscale;
        end
        
        kh0 = atgetfieldvalues(r,st,'KickAngle',{1,1});
        kv0 = atgetfieldvalues(r,st,'KickAngle',{1,2});
         
        if isnan(kh0)
            kh0 = 0;
        end
        if isnan(kv0)
            kv0 = 0;
        end

        % if any NaN set to zero everything
        excl_initial_NaN = isnan(kh0) | isnan(kv0);
        excl_delta_NaN = isnan(kh) | isnan(kv);
        useind = ~(excl_initial_NaN | excl_delta_NaN);
        
        % initialize to no change
        KH = kh0;
        KV = kv0;
        
        
        % change only those that deserve it.
        KH(useind) = kh0(useind) + kh(useind);
        KV(useind) = kv0(useind) + kv(useind);
        
    end
