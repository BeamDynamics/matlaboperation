function [hl,vl]=steerfamily(varargin)
%STEERFAMILY    Give indexes of steerer families in the steerer list
%
%[HLIST,VLIST]=STEERERFAMILY(FAMNAME)
%[HLIST,VLIST]=STEERERFAMILY({FAMNAME1,FAMNAME2,...})
%
%FAMNAME: name of a steerer family ('sh1', 'sd1a',...)
%
%HLIST:     horizontal steerer indexes (in 1:12)
%VLIST:     vertical steerer indexes (in 1:9)

args=cellfun(@string,varargin,'UniformOutput',false);
strs=lower(cat(2,args{:}));
hlabels=["sh1","sd1a","sf2a","sd1b","dq1b","sh2","dq2c","dq1d","sd1d","sf2e","sd1e","sh3"];
vlabels=["sh1","sd1a","sf2a","sd1b","sh2","sd1d","sf2e","sd1e","sh3"];
hl=find(repmat(ismember(hlabels,strs),1,32));
vl=find(repmat(ismember(vlabels,strs),1,32));
end
