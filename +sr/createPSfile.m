function createPSfile(ring)
% Create a file with PS currents for each device and strength in
% pwd/DesignStrength.csv
% 
% input: 
% ring = AT lattice. if not provided sr.model is used.
% 
%see also: addcalibrationcurves


% load lattice
if nargin ==0
    r = sr.model('reduce',true);
    ring = r.ring;
end

rc = addcalibrationcurves(ring);
brho = 6e9./PhysConstant.speed_of_light_in_vacuum.value;

%%
A=readtable(fullfile(pwd,'DesignStrengths.csv'));

devnames = A.Var1;
devstren = A.Var2;

devps_name = {};
devcurrent = [];

for id = 1:length(devnames)
    disp([ 'Computing Design Current for: ' devnames{id} '. ' num2str(id/length(devnames)*100) '%'] )
    
    
    try
        inddev = atgetcells(rc,'Device',devnames{id});
        
        C = rc{inddev}.Calibration;
        
        if size(C,2)~=2 % sextupoles
             C = rc{inddev}.Calibration(1:9:end,[1,9]);
             C(:,2)=abs(C(:,2))*1e3; %T/mm to T/m
        end
        
        STR = devstren(id)*brho;%[T]
        
        theo_cur = interp1(C(:,2),C(:,1),abs(STR));
        
        if isnan(theo_cur)
            disp('wait');
        end
        
        devps_name{id} = [strrep(devnames{id},'/m-','/ps-') '/Current:'];
        devcurrent(id) = theo_cur;
        
    catch err
        disp(err);
        disp(['no cur for:' devnames{id}]);
    end
end


t =cell2table([devps_name;num2cell(devcurrent)]');

writetable(t,'DesignPSCurrents.txt',...
    'WriteVariableNames',0,'Delimiter','\t');

