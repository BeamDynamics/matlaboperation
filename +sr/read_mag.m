function [strength,devlist] = read_mag(varargin)
%sr.READ_MAG    Read the values of magnet families from the control system
%
%CURRENT=sr.READ_MAG()
%   Return the family currents [A]
%
%CURRENT=sr.READ_MAG(MAGLIST)
%   Return the family currents for the selected magnets [A]
%
%MAGLIST:   List of magnet family names
%
%[...,DEVNAME]=sr.READ_MAG(...)
%   Return in addition the device names
strength_attribute_name = 'CorrectionStrength';
devlist=getargs(varargin,{sr.magname()});
strength=cellfun(@(d)tango.Attribute([d,'/',strength_attribute_name]).setpoint('',NaN),devlist,'un',1);

end
