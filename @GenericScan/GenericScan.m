classdef GenericScan < handle %& GridScan
    %GenericScan
    %
    % vary 2 knobs in a grid of points to seek for optimum.
    % measures lifetime, emittances, beamcurrent, total losses
    %
    %
    %see also:
    
    properties
        
        machine         % specify lattice if any peculiar settings are needed
        
        initial_tunes    % tunes as measured at initialization
        measured_Q_h         % hor. tune attribute
        measured_Q_v         % ver. tune attribute
        Tune_Adjust          % handle to tune adjust command. must have attributed Qh Qv Desired_Qh Desired_Qv
        
        SextInitial
        SextDev
        
        Hemittance      %
        Vemittance      %
        Lifetime        %
        InjEfficency    %
        TotalLosses     %
        BeamCurrent     %

        Hemittance0      %
        Vemittance0     %
        Lifetime0        %
        InjEfficency0    %
        TotalLosses0     %
        BeamCurrent0     %

        number_of_data_acquisitions % 
        
        VacuumLifetime  % vacuum lifetime to compute toushcek lifetime 
        
        scan_name       % label for the present scan.
        scan_figure     %
        scan_automatic  % do not ask any questions. just go.
        x_label 
        y_label

        % scan parameters
        grid_points % 2xN array of min and max Q_h
        grid_size   % 2x1 array , size of the tune scan grid
        measured_grid_points % grid points measured after moving the tunes
        number_of_removed_points % points removed from grid due to resonance crossing
        
        
        coupstop % size of couplign stop band to be excluded
        
        TuneIntegerPart % integer part of the tunes
        
        C_h_range  % 2x1 array of min and max Q_h
        C_v_range  % 2x1 array of min and max Q_v
        
        C_h_steps  % 1x1 double minimum step between two diffent Q_h
        C_v_steps  % 1x1 double minimum step between two diffent Q_h
        
        store_actual_tunes % flag to store or not the measured tunes as HV axis
        tune_change_mode % string: 'tango' or for large tune change, but slow 'QF1QD2' or 'Matching'
        wait_for_tune  % seconds to wait between tune settings.
        wait_for_data  % time to wait between 2 data acquisitions
        
    end
    
    
    methods
        
        % creator
        function obj = GenericScan(machine)
            %GenericScan Construct an instance of this class
            %
            % for help type:
            % >> doc GenericScan
            %
            %see also: TuneScan, ChromaScan
            
            obj.machine=machine;
            
            try
                a = load('/operation/beamdyn/matlab/optics/sr/theory/betamodel.mat');
                l = atlinopt(a.betamodel,0,1:length(a.betamodel)+1);
                obj.TuneIntegerPart = floor(l(end).mu/2/pi);
            catch
                warning('could not read /operation/beamdyn/matlab/optics/sr/theory/betamodel.mat tunes integer part [76 27]');
                obj.TuneIntegerPart = [76 27]; % tune integer part. (later take from lattice model)
            end
            
            obj.x_label = 'dim 1';
            obj.y_label = 'dim 2';

            % default properties
            obj.Tune_Adjust = 'srdiag/beam-tune/main';
            obj.store_actual_tunes = false;
            
            obj.SextDev = tango.Device('srmag/m-s/all');
            obj.SextInitial = obj.SextDev.CorrectionStrengths.set;
            
            obj.measured_Q_h = NaN;
            obj.measured_Q_v = NaN;
            obj.initial_tunes = obj.GetTune;
            
            obj.scan_name = datestr(now,'WP_YYYY_mm_dd_HH_MM');    % label for the present scan.
            %obj.scan_figure = figure('name',['Working Point Scan: ' obj.scan_name]);
            obj.scan_automatic = false;
            
            obj.coupstop =0.0; % size of couplign stop band to be excluded
            
            obj.VacuumLifetime = 1e6; % default to very large vacuum lifetime (infinity)
            
            obj.number_of_data_acquisitions = 5; % number of acquisitions
            
            obj.C_h_range =  [-2 1]; % 2x1 array of min and max Q_h
            obj.C_v_range =  [-2 1]; % 2x1 array of min and max Q_v
            
            obj.C_h_steps =5; % 1x1 double number of steps within C_h_range
            obj.C_v_steps =5; % 1x1 double number of steps within C_h_range
            
            obj.DefineGridPoints; % 2xN array of min and max Q_h
            
            obj.tune_change_mode='tango';
            obj.wait_for_tune = 5; % seconds to wait between tune settings.
            obj.wait_for_data = 1; % seconds to wait between data acquisitions.
            
            
            disp('measure relevant quantities Initial Values');
            obj.MeasureData(0);
            
            
            % machine specific settings.
            switch obj.machine
                case 'sr'
                    
                case 'ebs-simu'
                    
                otherwise
                    error('GenericScan is not supported for this machine')
                    
            end
            
        end
        
        function Reset(obj)
            % reset grid and measurement arrays for a new scan, returns to the initial WP
            obj.DefineGridPoints;
            obj.SextDev.CorrectionStrengths = obj.SextInitial;
            pause(obj.wait_for_tune)
            % go back to initial WP.
            obj.movetune(obj.initial_tunes,obj.GetTune);
            % no automatic.
            obj.scan_automatic = false;
            
        end
        
        function obj = DefineGridPoints(obj)
            % define the grid of points to scan
            
            %             if any(obj.C_h_range<1) | any(obj.C_v_range<1)
            %                 error('Please specify absolute chroma ex ts.C_h_range = [76.20 76.30];');
            %             end
            
            % define initial linear list
            Qx=linspace(obj.C_h_range(1),obj.C_h_range(2),obj.C_h_steps);
            Qy=linspace(obj.C_v_range(1),obj.C_v_range(2),obj.C_v_steps);
            
            % 2D list.
            [qy,qx]=meshgrid(Qy,Qx);
            
            % sort zig-zag low vertical to high vertical tune, to minimize steps in tune
            qx(:,2:2:end)=qx(end:-1:1,2:2:end);
            
            % remove points to avoid (coupling resonance)
            
            %            fracqx = qx-floor(qx);
            %            fracqy = qy-floor(qy);
            
            %            sel=(fracqx<(fracqy-obj.coupstop) | fracqx>(fracqy+obj.coupstop));
            sel=1:length(qx(:));
            
            % exclude coupling stopband
            qx=qx(sel');
            qy=qy(sel');
            
            obj.number_of_removed_points = sum(sum(sel==0));
            
            obj.grid_points = [qx qy];
            obj.grid_size = size(obj.grid_points);
            
            obj.measured_grid_points = NaN(obj.grid_size); % measured tune grid
            
            % intialize scan
            obj.Hemittance      = NaN(obj.grid_size(1),1);    %
            obj.Vemittance      = NaN(obj.grid_size(1),1);    %
            obj.Lifetime        = NaN(obj.grid_size(1),1);    %
            obj.InjEfficency    = NaN(obj.grid_size(1),1);    %
            obj.TotalLosses     = NaN(obj.grid_size(1),1);    %
            obj.BeamCurrent     = NaN(obj.grid_size(1),1);    %
            
            obj.PlotGridPoints;
            
            disp([num2str(obj.grid_size(1)) ...
                ' points will be tested. if 10s are needed for each point, '...
                num2str(obj.grid_size(1)*10/60) ' minutes to wait']);
            
            % grid has been updated, uptade measurement name as well.
            obj.scan_name = datestr(now,'WP_YYYY_mm_dd_HH_MM');    % label for the present scan.
            %obj.scan_figure = figure('name',['Working Point Scan: ' obj.scan_name]);
            
        end
        
        function PlotGridPoints(obj)
            % display the points that will be scanned. smallest the dot,
            % earlier it will appear in the scan.
            
            % display points to be scanned.
            try
                set(0,'CurrentFigure',obj.scan_figure);
            catch
                obj.scan_figure = figure('name',obj.scan_name);
            end
            %figure('name',['Points to scan: ' obj.scan_name ', dot size = order']);
            scatter(obj.grid_points(:,1),obj.grid_points(:,2),(1:obj.grid_size(1))*5,'LineWidth',2);
            hold on;
            plot(obj.grid_points(:,1),obj.grid_points(:,2),'--','LineWidth',2);
            %plot(obj.measured_Q_h, obj.measured_Q_v,'rx','MarkerSize',10,'LineWidth',3);
            grid on;
            hold off;
            
            xlim(obj.C_h_range);
            ylim(obj.C_v_range+[-1e-10 1e-10]);
            xlabel(obj.x_label); ylabel(obj.y_label);
            legend('points','order');
            % saveas(gca,['PointsToScanOrder' obj.scan_name '.fig']);
        end
        
        function PlotScan(obj,varargin)
            % display all the measurements taken up to now.
            %
            % Lifetime, H and V emittance and beam current. Blue is best.
            %
            % by default the spots are drawn at the measured values.
            % obj.PlotScan(false) to plot at the location specified by
            % obj.grid_points
            %
            % the empty black circles are the points to scan
            % the red square the present optimum overall
            % the green squares the optimum of each parameter
            %
            %
            
            
            if length(varargin)>=1
                measured = varargin{1};
                plot_multi = false;
            else
                measured = true;
                plot_multi = false;
            end
           
            
            % display points to be scanned.
            if ~isempty(obj.scan_figure)
                try
                    set(0,'CurrentFigure',obj.scan_figure);
                catch err
                    % figure has been deleted by user.
                    obj.scan_figure = figure('name',['W.P. scan :' obj.scan_name],...
                        'units','normalized','position',[0.05 0.1 0.5 0.6]);
                end
                
            else
                obj.scan_figure = figure('name',['W.P. scan :' obj.scan_name],...
                    'units','normalized','position',[0.05 0.1 0.5 0.6]);
            end
            
            qx = obj.grid_points(:,1);
            qy = obj.grid_points(:,2);
            
            qx0=qx; qy0=qy;
            
            extraborder = [-0.01 0.01];
            
            if measured & find(obj.measured_grid_points)
                extraborderval = max([ ...
                    abs(min(obj.measured_grid_points(:,1)) - obj.C_h_range(1)),...
                    abs(min(obj.measured_grid_points(:,2)) - obj.C_v_range(1)),...
                    abs(max(obj.measured_grid_points(:,1)) - obj.C_h_range(2)),...
                    abs(max(obj.measured_grid_points(:,2)) - obj.C_v_range(2))...
                    ]);
                extraborder = [-extraborderval extraborderval];
            end
            
            if measured
                
                % find last measured:
                indlast = find(isnan(obj.measured_grid_points(:,1)),1,'first');
                
                if isempty(indlast)
                    indlast = obj.grid_size(1);
                end
                
                qx(1:indlast) = obj.measured_grid_points(1:indlast,1);
                qy(1:indlast) = obj.measured_grid_points(1:indlast,2);
                
                clf; % clear current figure;
            end
            
            markersize = 850;
            fontsize = 14 ;
%            plot_multi = false;
             
            if plot_multi
                subplot(2,2,1);
                scatter(qx,qy,markersize,...
                    obj.Lifetime(:),'filled');     % color is lifetime
                
                %plot points to be yet scanned
                hold on;
                scatter(qx0,qy0,markersize,[0 0 0]);  %
                
                axis equal;
                ax= gca;
                ax.FontSize = fontsize;
                hold on;
                
                colormap(ax,flipud(jet));
                xlabel(obj.x_label); ylabel(obj.y_label);
                xlim(obj.C_h_range + extraborder);
                ylim(obj.C_v_range + extraborder);
                c=colorbar;
                title( 'Lifetime [h]');
                hold off;
                
                subplot(2,2,2);
                scatter(qx,qy,markersize,...
                    obj.TotalLosses(:),'filled');     % color is lifetime
                hold on;
                scatter(qx0,qy0,markersize,[0 0 0]);  %
                
                if measured
                    quiver(qx0,qy0,qx-qx0,qy-qy0);
                end
                
               
                axis equal;
                ax= gca;
                ax.FontSize = fontsize;
                
                hold on;
                
                colormap(gca,flipud(jet));
                
                xlabel(obj.x_label); ylabel(obj.y_label);
                xlim(obj.C_h_range + extraborder);
                ylim(obj.C_v_range + extraborder);
                c=colorbar;
                title( 'Beam Total Losses [a.u.]');
                hold off;
                
                subplot(2,2,3);
                scatter(qx,qy,markersize,...
                    obj.Hemittance(:)*1e12,'filled');     % color is lifetime
                hold on;
                scatter(qx0,qy0,markersize,[0 0 0]);  %
                
                axis equal;
                ax= gca;
                ax.FontSize = fontsize;
                
                hold on;
                
                colormap(gca,jet);
                
                xlabel(obj.x_label); ylabel(obj.y_label);
                xlim(obj.C_h_range + extraborder);
                ylim(obj.C_v_range + extraborder);
                c=colorbar;
                title('Hor. Emittance [pm]');
                hold off;
                
                subplot(2,2,4);
                scatter(qx,qy,markersize,...
                    obj.Vemittance(:)*1e12,'filled');     % color is lifetime
                hold on;
                scatter(qx0,qy0,markersize,[0 0 0]);  %
                
                axis equal;
                ax= gca;
                ax.FontSize = fontsize;
                
                hold on;
                colormap(gca,jet);
                
                xlabel(obj.x_label); ylabel(obj.y_label);
                xlim(obj.C_h_range + extraborder);
                ylim(obj.C_v_range + extraborder);
                title('Ver. Emittance [pm]');
                c=colorbar;
                hold off;
            else % plot summary
                
               tvac =obj.VacuumLifetime; % h
               tmeas = obj.Lifetime(:)/3600;
               tcor = 1./(1./tmeas-1./tvac);
              
               SUMMARY_VAL0 = ...
                    obj.Lifetime0.*3600./... s
                 sqrt(obj.Hemittance0)./... m
                 sqrt(obj.Vemittance0);    % A
                 
              SUMMARY_VAL = ...
                   tcor.*sqrt(0.130).*sqrt(0.01)./... s
                 sqrt(obj.Hemittance(:))./... m
                 sqrt(obj.Vemittance(:)) ;    % A
             
             SUMMARY_VAL = ...
                 tcor.*sqrt(0.130).*sqrt(0.01).*...
                 obj.BeamCurrent./obj.BeamCurrent(1)./... s
                 sqrt(obj.Hemittance(:))./... m
                 sqrt(obj.Vemittance(:)) ;    % A


                scatter(qx,qy,markersize,...
                   SUMMARY_VAL,'filled');     % color is lifetime
                hold on;
                scatter(qx0,qy0,markersize,[0 0 0]);  %
                % plot present best v emit point
                %             hold on;
                %             indbest=obj.BestVemittanceWP;
                %             if ~isnan(indbest)
                %                 plot(qx(indbest),qy(indbest),'gs','MarkerSize',markersize/3);
                %                 plot(qx(indbest),qy(indbest),'gs','MarkerSize',markersize/4);
                %             end
                axis equal;
                ax= gca;
                ax.FontSize = fontsize;
                
                hold on;
                % obj.tunediagLF(1:4,[obj.C_h_range obj.C_v_range],[1 1],32);
                colormap(gca,jet);
                
                xlabel(obj.x_label); ylabel(obj.y_label);
                %xlim(obj.C_h_range + extraborder);
                %ylim(obj.C_v_range + extraborder);
                title({['Lifetime *(I/I0) / sqrt((eh/eh0)*(ev/eh0)) [h] '],[]});
                c=colorbar;
                hold off;
            end
            
            % saveas(gca,['Scan' obj.scan_name '.fig']);
        end
        
        function tunes = GetTune(obj)
            % read present tunes, return absolute values adding [76 27]
            
            obj.measured_Q_h = tango.Attribute([obj.Tune_Adjust '/Qh']).value;
            obj.measured_Q_v = tango.Attribute([obj.Tune_Adjust '/Qv']).value;
            tunes = [obj.measured_Q_h obj.measured_Q_v];
            
            if any(tunes<1)
                tunes = tunes + obj.TuneIntegerPart; % use absolute tunes
                obj.measured_Q_h = obj.TuneIntegerPart(1) + obj.measured_Q_h;
                obj.measured_Q_v = obj.TuneIntegerPart(2) + obj.measured_Q_v;
            end
        end
        
        function movetune(obj,desired_tune,initial_tune)
            % function to change tune
            
            present_tune = obj.GetTune;
            count =1;
            
            % loop tune change untill it reaches the desired values.
            while any( abs(desired_tune - present_tune )>0.001)  & count < 2
                
                switch obj.tune_change_mode
                    case{'QF1QD2','Matching'}
                        
                        % set markers
                        if ~obj.scan_automatic
                            input('place markers on tunes and press enter?','s');
                        end
                        
                        movetune(desired_tune,present_tune,'setcur',true,'mode',obj.tune_change_mode);
                        
                    case 'tango'
                        
                        tunedev = tango.Device([obj.Tune_Adjust]);
                        
                        tunedev.Desired_Qh = desired_tune(1);
                        tunedev.Desired_Qv = desired_tune(2);
                        
                        tunedev.SetTune();
                        
                    otherwise
                        error('tune_change_mode not correct')
                end
                
                % check if beam is present, if not, exit.
                if ~ ( getTotCurrent() > 0.01 | ~any(isnan(obj.GetTune)) )
                    
                    error('no beam! or tunes unavailable. stopping loop. restart when ready, the points already done will not be measured again')
                    
                end
                
                % wait setting and measurement
                pause(obj.wait_for_tune);
                
                % update present tunes
                present_tune = obj.GetTune;
                
                % disp(count)
                % disp(present_tune);
                % abs(desired_tune - present_tune )
                % any( abs(desired_tune - present_tune )>0.001)
                
                count = count +1;
            end
            
            % read tunes
            disp(['Initial Tunes: ' num2str(initial_tune)]);
            disp(['Desired Tunes: ' num2str(desired_tune)]);
            disp(['Obtained tunes: ' num2str(obj.GetTune)]);
            
        end
        
        function check_grid(obj)
            % check if grid points are consistent ot something has been
            % modified.
            
            %check size
            if obj.C_h_steps * obj.C_v_steps - obj.number_of_removed_points ~= obj.grid_size(1)
                error('Inconsistent grid size. ts.DefineGridPoints or ts.Reset to redefine the grid and start a new measurement');
            end
            % check range
            if obj.C_h_range(1) ~= min(obj.grid_points(:,1))
                error('Inconsistent min Q_h range. ts.DefineGridPoints or ts.Reset to redefine the grid and start a new measurement');
            end
            if obj.C_h_range(2) ~= max(obj.grid_points(:,1))
                error('Inconsistent max Q_h range. ts.DefineGridPoints or ts.Reset to redefine the grid and start a new measurement');
            end
            
            if obj.C_v_range(1) ~= min(obj.grid_points(:,2))
                error('Inconsistent min Q_v range. ts.DefineGridPoints or ts.Reset to redefine the grid and start a new measurement');
            end
            if obj.C_v_range(2) ~= max(obj.grid_points(:,2))
                error('Inconsistent max Q_v range. ts.DefineGridPoints or ts.Reset to redefine the grid and start a new measurement');
            end
            
        end
        
        function ExecuteChange(obj,DesiredVal)
            % change chroma and recover tunes
            % change chroma
            disp('desired change:')
            disp(DesiredVal);
 
        end
        
        function MeasureData(obj,itun)
           % measure data
           %
           % if itun = 0, the initial data is stored in Lifetime0,
           % BeamCurrent0, HEmittance0. 
           % if itun >0 the vectors of acquired scan data are filled at the
           % itun index
                        
                        disp('measure relevant quantities');
                        nave = obj.number_of_data_acquisitions;
                        % get lifetime
                        %                        obj.Lifetime(itun)=tango.Attribute('srdiag/beam-current/total/Lifetime').value; % vac, averaging time,
                        try
                            val =[];
                            for iii=1:nave
                                val(iii) = tango.Attribute('srdiag/bpm/lifetime/Lifetime').value;
                                pause(obj.wait_for_data)
                            end
                             LT=mean(val); % vac, averaging time,
                            
                        catch
                            disp('no libera lifetime, using CT lifetime')
                            LT=tango.Attribute('srdiag/beam-current/total/Lifetime').value; % vac, averaging time,
                        end

                        
                        % get emittances
                        try
                            valh =[];
                            valv =[];
                            for iii=1:nave
                                valh(iii) = tango.Attribute('srdiag/beam-emittance/average/Emittance_H').value;
                                valv(iii) = tango.Attribute('srdiag/beam-emittance/average/Emittance_V').value;
                                pause(obj.wait_for_data)
                            end
                            HE= mean(valh);% 10 waits 1 second and makes 10 averages
                            VE= mean(valv); % 10 waits 1 second and makes 10 averages
                            
                            
                        catch
                            disp('no average, using id07')
                            HE=tango.Attribute('srdiag/emittance/id07/Emittance_h').value; % 10 waits 1 second and makes 10 averages
                            VE=tango.Attribute('srdiag/emittance/id07/Emittance_v').value; % 10 waits 1 second and makes 10 averages
                        end
                        
                        % get stored current
                        BC=tango.Attribute('srdiag/beam-current/total/Current').value;
                        
                        % get stored current
                        try
                            TL=tango.Attribute('srdiag/blm/all/TotalLoss').value;
                        catch
                            disp('no losses')
                            TL=0;
                        end
                        
                        
                        % store in vector or in initial values
                        if itun>0
                            obj.BeamCurrent(itun) = BC;
                            obj.Lifetime(itun) = LT;
                            obj.Hemittance(itun) =HE;
                            obj.Vemittance(itun) =VE;
                            obj.TotalLosses(itun) = TL;
                        elseif itun ==0
                            obj.BeamCurrent0 = BC;
                            obj.Lifetime0 = LT;
                            obj.Hemittance0 =HE;
                            obj.Vemittance0 =VE;
                            obj.TotalLosses0 = TL;
                        else
                            error('negative index not supported in MeasureData')
                        end
                        
        end
                            
        function RunScan(obj)
            % runs the working point scan
            
            % here a call to obj.DefineGridPoints could make safer the use of
            % the code, however, omitting this call, allows to run the scan
            % in multipole times, stopping and restarting from the last unmeasured point.
            
            obj.check_grid;
            
            qx = obj.grid_points(:,1);
            qy = obj.grid_points(:,2);
            
            % start loop
            for itun=1:length(qx)
                
                if any( isnan( obj.measured_grid_points(itun,:) ) )
                    
                    % get present tunes.
                    
                    inittune =  obj.GetTune; % read tunes
                    disp(['Presently at: ' num2str(inittune) ': ' num2str((itun-1)/obj.grid_size(1)*100,'%2.0f') '% completed' ]);
                    
                    % ask if point is to be measured or skipped
                    if obj.scan_automatic
                        measure_or_skip='y'; % contiunue always
                    else
                        measure_or_skip = input(['Go to: '...
                            num2str([qx(itun),qy(itun)]) ...
                            ' / skip and move to next point /'...
                            ' stop now /'...
                            ' continue without asking? '...
                            '[(y)es / (n)ext / (s)top / (a)uto ]'],'s');
                    end
                    
                    if measure_or_skip == 's'
                        disp('Stopped');
                        return
                    end
                    
                    if measure_or_skip == 'a'
                        obj.scan_automatic = true;
                        disp('running in automatic mode.');
                        disp('To return to ask questions, Ctrl-C and restart scan (points measured are kept).')
                    end
                    
                    if measure_or_skip == 'n' % skipped point, all data will be NaN
                        
                        disp(['Skipped tune: ' num2str([qx(itun),qy(itun)]) ]);
                        obj.measured_grid_points(itun,:) = [qx(itun),qy(itun)] ;
                        
                    else
                        
                        % change
                        obj.ExecuteChange([qx(itun),qy(itun)]);

                        
                        % measure
                        if ~obj.scan_automatic
                            input('press to measure data at this point','s');
                        end
                        
                        obj.MeasureData(itun);
                        
                        
                        % update measured tune
                        actual_tunes = obj.GetTune;
                        
                        % get quadrupole currents
                        curquad(itun,:)=tango.Attribute('srmag/m-s/all/Strengths').value;
                        
                        % get tunes and spectrum
                        tspec(itun,:)= NaN; %tunespec.Spectrum.read;
                        fmin(itun)= NaN; % tunespec.FreqMin.read;
                        fmax(itun)= NaN; % tunespec.FreqMax.read;
                        
                        if ~obj.store_actual_tunes
                            QX(itun)=qx(itun);%actual_tunes(1);
                            QY(itun)=qy(itun);%actual_tunes(2);
                        else
                            QX(itun)=actual_tunes(1);
                            QY(itun)=actual_tunes(2);
                        end
                        
                        obj.measured_grid_points(itun,1)= QX(itun);
                        obj.measured_grid_points(itun,2)= QY(itun);
                        
                    end
                    
                    % save
                    save(['temp_' obj.scan_name '.mat'],'obj');
                    
                    % plot present scan status
                    obj.PlotScan;
                    
                else
                    
                    disp('Chroma: ')
                    disp([qx(itun),qy(itun)])
                    disp('already measured');
                    
                end
                
                
            end
            
            
            % go back to initial WP.
            % obj.movetune(obj.initial_tunes,obj.GetTune)
            % obj.SextDev.CorrectionStrengths = obj.SextInitial
            
            % save final data
            save(['Data_' obj.scan_name '.mat'],'obj');
            
            % plot figures with measured tunes.
            obj.PlotScan;
            
            saveas(gca,['TuneScanLifetime' obj.scan_name '.fig']);
            
        end
        
        function CancelLastPoints(obj,n)
            % CANCELLASTPOINTS erases the data taken in the last n measurements.
            %
            %example: obj.CancelLastPoints(3);
            
            nmeas = find(isnan(obj.measured_grid_points(:,1)),1,'first');
            disp([num2str(nmeas) ' points where measured, removing last ' num2str(n)]);
            if isempty(nmeas)
                nmeas = obj.grid_size(1);
            end
            ind2cancel = nmeas:-1:(nmeas-n+1);
            
            obj.measured_grid_points(ind2cancel,:)=NaN;
            
            obj.Lifetime(ind2cancel)=NaN; % vac, averaging time,
            
            % get emittances
            obj.Hemittance(ind2cancel)=NaN; % 10 waits 1 second and makes 10 averages
            obj.Vemittance(ind2cancel)=NaN; % 10 waits 1 second and makes 10 averages
            
            % get stored current
            obj.BeamCurrent(ind2cancel)=NaN;
            
            % get stored current
            obj.TotalLosses(ind2cancel)=NaN;
            
            nmeas = find(isnan(obj.measured_grid_points(:,1)),1,'first');
            disp([num2str(nmeas) ' valid points ']);
            
            obj.PlotScan(true); % plot measured W.P.
            
        end
        
        
        function ind_best=CurrentBestWP(obj)
            % guess of best WP from max(TLT*Current/hemit/vemit)
            %
            
            %val =  obj.Lifetime .* obj.BeamCurrent ./ obj.Hemittance ./ obj.Vemittance ;
            val =  obj.TotalLosses ./ obj.BeamCurrent ./ obj.Hemittance ./ obj.Vemittance ;
            
            [~,ind_best] = max(val(~isnan(val)));
            disp(['Best W.P. ' num2str(obj.measured_grid_points(ind_best,:))]);
            
        end
        function ind_best=BestLifetimeWP(obj)
            % guess of best WP from max(TLT)
            %
            
            val =  obj.Lifetime  ;
            
            [~,ind_best] = max(val(~isnan(val)));
            disp(['Best Lifetime W.P. ' num2str(obj.measured_grid_points(ind_best,:))]);
            
        end
        
        function ind_best=BestTotalLossesWP(obj)
            % guess of best WP from max(TLT)
            %
            
            val =  obj.TotalLosses  ;
            
            [~,ind_best] = min(val(~isnan(val)));
            disp(['Best total losses W.P. ' num2str(obj.measured_grid_points(ind_best,:))]);
            
        end
        
        function ind_best=BestBeamCurrentWP(obj)
            % guess of best WP from max(Current)
            %
            
            val =  obj.BeamCurrent  ;
            
            [~,ind_best] = max(val(~isnan(val)));
            disp(['Best BeamCurrent W.P. ' num2str(obj.measured_grid_points(ind_best,:))]);
            
        end
        function ind_best=BestHemittanceWP(obj)
            % guess of best WP from min(hemit)
            %
            
            val =  obj.Hemittance  ;
            
            [~,ind_best] = min(val(~isnan(val)));
            disp(['Best hor. emittance W.P. ' num2str(obj.measured_grid_points(ind_best,:))]);
            
        end
        function ind_best=BestVemittanceWP(obj)
            % guess of best WP from mmin(vemit)
            %
            
            val =  obj.Vemittance  ;
            
            [~,ind_best] = min(val(~isnan(val)));
            disp(['Best ver. emittance W.P. ' num2str(obj.measured_grid_points(ind_best,:))]);
            
        end
        
        % signature of methods defined in seprated functions below
        
        
    end
    
    
end

