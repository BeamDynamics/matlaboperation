function [ErrorsH, ErrorsV] = CheckPolArrayQuads(obj,indexes)
%CHECKPOLARRAYQuads Check the polarity of many quadrupoles
%
% Inputs:
% indexes , index of quadrupoles (1-514) for which to ehck polarity
%
%  see also: CheckPolarityOneQuad

ErrorsH=nan(size(indexes));
ErrorsV=nan(size(indexes));
for ii=1:length(indexes)
    pause(2);
    ErrorsH(ii)=obj.CheckPolOneQuad(indexes(ii),'H');
    pause(2);
    ErrorsV(ii)=obj.CheckPolOneQuad(indexes(ii),'V');
    disp(['~~~~~~~~~~~   Quadrupole ' num2str(indexes(ii)) ': '])
    disp(['H error = ' num2str(ErrorsH(ii)*100) '%, V error = ' num2str(ErrorsV(ii)*100) '%']);
    disp('~~~~~~~~~~~')
end

figure;
plot(indexes,ErrorsH*100,'-*','DisplayName','Horizontal','LineWidth',3)
hold on; grid on;
plot(indexes,ErrorsV*100,'-*','DisplayName','Vertical','LineWidth',3)
xlabel('steerer number');
ylabel('Focusing error (%)')
legend('Location','NorthEast')

end
