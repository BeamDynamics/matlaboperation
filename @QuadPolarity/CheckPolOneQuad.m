function Error = CheckPolOneQuad(obj,index,plane)
%CHECKPOLONEQUAD Checks polarity of a single quadrupole
%   Detailed explanation goes here
%  see also: CheckPolArrayQuads

sQuad=obj.sQuad(index);

indbpmafter=find(obj.sBPM>(sQuad+1),1,'first');
indHstbefore=find(obj.sHst<(sQuad-1),1,'last');
indVstbefore=find(obj.sVst<(sQuad-1),1,'last');

if isempty(indHstbefore) | isempty(indVstbefore)
    error('no upstream steerer available, try next quadrupole');
end

kicks= linspace(-obj.kickmax,obj.kickmax,obj.numkicksperquad);
x1=zeros(size(kicks));
x2=zeros(size(kicks));
% y1=zeros(size(kicks));
% y2=zeros(size(kicks));

% Simulation
for ii=1:length(kicks)
    if plane=='H'
        traj=obj.SimulateTrajectoryKick(indHstbefore,kicks(ii),plane);
    else
        traj=obj.SimulateTrajectoryKick(indVstbefore,kicks(ii),plane);
    end
    if plane=='H'
        x1(ii)=traj(1,indbpmafter);
        x2(ii)=traj(1,indbpmafter+1);
    else
        x1(ii)=traj(2,indbpmafter);
        x2(ii)=traj(2,indbpmafter+1);
    end
end

% Measurement
if plane=='H'
    v_zero=obj.sh.get();
else
    v_zero=obj.sv.get();
end
% v=v_zero;
traj0=obj.measuretrajectory();
for ii=1:length(kicks)
    v=v_zero;
    if plane=='H'
        v(indHstbefore)=v(indHstbefore)+kicks(ii);
        obj.sh.set(v);
    else
        v(indVstbefore)=v(indVstbefore)+kicks(ii);
        obj.sv.set(v);
    end
    pause(2)
    traj=obj.measuretrajectory()-traj0;
    if plane == 'H'
        x1_m(ii)=traj(1,indbpmafter);
        x2_m(ii)=traj(1,indbpmafter+1);
    else
        x1_m(ii)=traj(2,indbpmafter);
        x2_m(ii)=traj(2,indbpmafter+1);
    end
end

if plane=='H'
    obj.sh.set(v_zero);
else
    obj.sv.set(v_zero);
end

[fit_s1,gf_s1]=fit(kicks',x1','poly1');
[fit_s2,gf_s2]=fit(kicks',x2','poly1');
[fit_m1,gf_m1]=fit(kicks',x1_m','poly1');
[fit_m2,gf_m2]=fit(kicks',x2_m','poly1');

if obj.plot
    figure;
    hold on; grid on;
    p1=plot(kicks,x1,'.','MarkerSize',20,'DisplayName','Simu first BPM');
    pf1=plot(fit_s1);pf1.Color=p1.Color;
    p2=plot(kicks,x2,'.','MarkerSize',20,'DisplayName','Simu second BPM');
    pf2=plot(fit_s2);pf2.Color=p2.Color;
    p3=plot(kicks,x1_m,'.','MarkerSize',20,'DisplayName','Meas first BPM');
    pf3=plot(fit_m1);pf3.Color=p3.Color;
    p4=plot(kicks,x2_m,'.','MarkerSize',20,'DisplayName','Meas second BPM');
    pf4=plot(fit_m2);pf4.Color=p4.Color;
    legend([p1 p2 p3 p4],'Location','NorthEast');
    xlabel(['Kick at ' plane ' steerer before Quad number ' num2str(index)])
    ylabel([plane ' position BPMs'])
end
Error=sqrt(((fit_m1.p1-fit_s1.p1)/fit_s1.p1)^2 + ((fit_m2.p1-fit_s2.p1)/fit_s2.p1)^2);

disp(['Focalization error for quad n. ' num2str(index) ' in ' plane ' plane is: ' num2str(100*Error) ' %']);
end

