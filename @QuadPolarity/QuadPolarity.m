classdef QuadPolarity < RingControl
    %QuadPolarity is a Class to check if the quads have the good polarity.
    %
    %   The class can be used in the sr, sy or ebs-simu
    %
    %   Example on how to use it:
    %   ...   
    %
    %  see also: RingControl, SteerersPolarity
    
    properties
        plot;
        sBPM;
        sHst;
        sVst;
        sQuad;
        kickmax;
        numkicksperquad;

    end
    
    methods
        function obj = QuadPolarity(machine)
            %QUADPOLARITY Construct an instance of the class
            %QuadPolarity
            %   
            %   QP=QuadPolarity(machine)
            %   machine can be: 'ebs-simu', 'esrf-sr', 'esrf-sy'
            %   
            %   see also: RingControl
            
            
            % call the construct of RingControl
            obj@RingControl(machine);
            
            % put the default values in the properties
            obj.plot=true;
            %obj.kick_rad=1e-4;
            %obj.nbpm_after=4;
            
            % find s positions of BPMs and steerers
            obj.sBPM=findspos(obj.rmodel,obj.indBPM);
            obj.sHst=findspos(obj.rmodel,obj.indHst);
            obj.sVst=findspos(obj.rmodel,obj.indVst);
            obj.sQuad=findspos(obj.rmodel,obj.indQuad);
            obj.kickmax=5e-5; %rad
            obj.numkicksperquad=4;
        end

        %this function checks the polarity a single quadrupole
        Error=CheckPolOneQuad(obj,index,plane);
        
        %this function checks the polarity of an array of quadrupoles
        Errors=CheckPolArrayQuads(obj,indexes);
        
    end
end

