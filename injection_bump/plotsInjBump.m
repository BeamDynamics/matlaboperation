close all
clear all

atmod = sr.model;
r = atradoff(atmod.ring);
[in,en] = sr.getcellstartend(atmod.ring,3); ARCB_INJ = atmod.ring(in:en);
[in,en] = sr.getcellstartend(atmod.ring,4); ARCA_INJ = atmod.ring(in:en);
[in,en] = sr.getcellstartend(r,6); ARCA = r(in:en);

[in,en] = sr.getcellstartend(r,3); ARCB_INJ = r(in:en);
rrot = atrotatelattice(r,in);
ainj=rrot(1:246);

%% match optics plot

% match to default parameters
%ainj=[ARCB_INJ;ARCA_INJ];
arcas=repmat(ARCA,30,1);
% match ARCA cell
indK1=findcells(ainj,'FamName','DR_K1\w*');
indK2=findcells(ainj,'FamName','DR_K2\w*');
indK3=findcells(ainj,'FamName','DR_K3\w*');
indK4=findcells(ainj,'FamName','DR_K4\w*');

indK=[indK1;indK2;indK3;indK4];

% make kicker drifts StrMPoles
ainj=setcellstruct(ainj,'PassMethod',indK,'StrMPoleSymplectic4Pass');
ainj=setcellstruct(ainj,'PolynomB',indK,0,1,1);
ainj=setcellstruct(ainj,'PolynomA',indK,0,1,1);
ainj=setcellstruct(ainj,'PolynomB',indK,0,1,2);
ainj=setcellstruct(ainj,'PolynomA',indK,0,1,2);
ainj=setcellstruct(ainj,'MaxOrder',indK,1,1,1);
ainj=setcellstruct(ainj,'NumIntSteps',indK,10,1,1);

% remove S3 apertures
 indS3=findcells(ainj,'FamName','S3_\w*');
ainj{indS3}.RApertures(1) = -0.044;
ainj{indS3-1}.RApertures(1) = -0.044;

 % create variables
Variab=atVariableBuilder(ainj,{'DR_K1','DR_K2','DR_K3','DR_K4'},{{'PolynomB',{1,1}}});
 
% Variab=struct(...
%     'Indx',{indK1,...
%     indK2,...
%     indK3,...
%     indK4},...
%     'Parameter',{...
%     {'PolynomB',{1,1}},...
%     {'PolynomB',{1,1}},...
%     {'PolynomB',{1,1}},...
%     {'PolynomB',{1,1}},...
%     },...
%     'LowLim',{[],[],[],[]},...
%     'HighLim',{[],[],[],[]}...
%     );
    
kickers=findcells(ainj,'FamName','DR_K1','DR_K2','DR_K3','DR_K4');

ainj1=ainj;

bumpH=[1,5,10,11,12,13,14,15,16.5,17.5]*1e-3; % 4 kikcers independent
bumpH=-bumpH;

injANGLE = 0.2e-3; 

for ii=1:length(bumpH)

LinConstr1=atlinconstraint(...
    findcells(ainj,'FamName','S3\w*')+1,...
    {{'ClosedOrbit',{1}},{'ClosedOrbit',{2}}},...
    [bumpH(ii),injANGLE],...
    [bumpH(ii),injANGLE],...
    [1 1]);

sjind=findcells(ainj,'FamName','SJ2\w*');

LinConstr2=atlinconstraint(...
    sjind(1),...
    {{'ClosedOrbit',{1}},{'ClosedOrbit',{2}}},...
    [0,0],...
    [0,0],...
    [1 1]);

kicklimit = struct(...
        'Fun',@(r,~,~) max(abs(atgetfieldvalues(r,kickers([3,4]),'PolynomB',{1,1}).*atgetfieldvalues(r,kickers([3,4]),'Length'))),...
        'Min',0,...
        'Max',2.69e-3,...
        'RefPoints',kickers(1),...
        'Weight',1e-6); % correctors within 0.4mrad . very important


Constr=[LinConstr1,LinConstr2,kicklimit];

ainj1=atmatch(ainj1,Variab,Constr,10^-12,100,3,@lsqnonlin);%,'fminsearch');%
%ainj1=atmatch(ainj1,Variab,Constr,10^-12,1000,3);%,'fminsearch');%
disp(bumpH(ii))

[l1,t1,c1]=atlinopt([ainj1;arcas],0,1:length([ainj1;arcas]));
cx1(ii,:)=arrayfun(@(a)a.ClosedOrbit(1),l1);

kick(ii,:)=getcellstruct(ainj1,'PolynomB',kickers,1,1);

% linear kick orbit
ainj1lin=setcellstruct(ainj1,'PolynomB',kickers,kick(1,:)*ii,1,1);
[l1lin,t1,c1]=atlinopt([ainj1lin;arcas],0,1:length([ainj1;arcas]));
cx1lin(ii,:)=arrayfun(@(a)a.ClosedOrbit(1),l1lin);

disp('kick strengths in [rad]')
disp(cellfun(@(a)[a.FamName ' K0L: ' num2str(a.PolynomB(1,1)*a.Length) ' rad'],ainj1(kickers),'un',0))

figure;
atplot(ainj1,[0,56],@plClosedOrbit)

end

%% 15mm +/- 1mm
% linear kick orbit
ainj1lin=setcellstruct(ainj1,'PolynomB',kickers,kick(end,:),1,1);
[l1lin,t1,c1]=atlinopt([ainj1lin;arcas],0,1:length([ainj1;arcas]));
cx1lin15(1,:)=arrayfun(@(a)a.ClosedOrbit(1),l1lin);
ainj1lin=setcellstruct(ainj1,'PolynomB',kickers,kick(end,:)*14/15,1,1);
[l1lin,t1,c1]=atlinopt([ainj1lin;arcas],0,1:length([ainj1;arcas]));
cx1lin15(2,:)=arrayfun(@(a)a.ClosedOrbit(1),l1lin);
ainj1lin=setcellstruct(ainj1,'PolynomB',kickers,kick(end,:)*16/15,1,1);
[l1lin,t1,c1]=atlinopt([ainj1lin;arcas],0,1:length([ainj1;arcas]));
cx1lin15(3,:)=arrayfun(@(a)a.ClosedOrbit(1),l1lin);
ainj1lin=setcellstruct(ainj1,'PolynomB',kickers,kick(end,:)*7.5/15,1,1);
[l1lin,t1,c1]=atlinopt([ainj1lin;arcas],0,1:length([ainj1;arcas]));
cx1lin15(4,:)=arrayfun(@(a)a.ClosedOrbit(1),l1lin);

%%

[l,t,c]=atlinopt([ainj;arcas],0,1:length([ainj;arcas]));
cx=arrayfun(@(a)a.ClosedOrbit(1),l);

s=findspos([ainj;arcas],1:length([ainj;arcas]));
sk=findspos(ainj,kickers);

figure;
atplot(ainj1,[12,40.5],@plClosedOrbit)

saveas(gca,['bump' '.fig']);
export_fig(['bump' '.jpg']);

%%
[lin,t,c]=atlinopt(ainj1,0,1:length(ainj1));
s=findspos(ainj1,1:length(ainj1));
fff=fopen('InjBump15mm.txt','w+');

for ii=1:length(ainj1)
fprintf(fff,'%s\t%f\t%f\n',ainj1{ii}.FamName,s(ii),lin(ii).ClosedOrbit(1))
end

fclose('all');

%% match static bump to reach -21mm for injection on axis

% match to default parameters
ainj=ainj1;
arcas=repmat(ARCA,30,1);
% match ARCA cell
indK1=findcells(ainj,'FamName','SI1D\w*');
indK2=findcells(ainj,'FamName','SI2E\w*');
indK3=findcells(ainj,'FamName','SI1E\w*');
indK4=findcells(ainj,'FamName','SH3E\w*'); indK4 = indK4(1);
indK5=findcells(ainj,'FamName','SH1A\w*'); indK5 = indK5(2);
indK6=findcells(ainj,'FamName','SJ1A\w*');
indK7=findcells(ainj,'FamName','SJ2A\w*');
indK8=findcells(ainj,'FamName','SJ1B\w*');

indKc=findcells(ainj,'FamName','SH2B\w*');

indK1e=findcells(ainj,'FamName','SH1A\w*'); indK1e = indK1e(1);
indK2e=findcells(ainj,'FamName','SI1A\w*');
indK3e=findcells(ainj,'FamName','SI2A\w*');
indK4e=findcells(ainj,'FamName','SI1B\w*');
indK5e=findcells(ainj,'FamName','SJ1D\w*');
indK6e=findcells(ainj,'FamName','SJ2E\w*');
indK7e=findcells(ainj,'FamName','SJ1E\w*');
indK8e=findcells(ainj,'FamName','SH3E\w*'); indK8e = indK8e(2);

indK=[indK1e;indK2e;indK3e;indK4e;...
    indKc';....
    indK1;indK2;indK3;indK4;...
    indK5;indK6;indK7;indK8;...
    indK5e;indK6e;indK7e;indK8e;];

%indK=[indK3;indK4;indK5;indK6;];

% make kicker drifts StrMPoles
ainj=setcellstruct(ainj,'PassMethod',indK,'StrMPoleSymplectic4Pass');
ainj=setcellstruct(ainj,'PolynomB',indK,0,1,1);
ainj=setcellstruct(ainj,'PolynomA',indK,0,1,1);
ainj=setcellstruct(ainj,'PolynomB',indK,0,1,2);
ainj=setcellstruct(ainj,'PolynomA',indK,0,1,2);
ainj=setcellstruct(ainj,'MaxOrder',indK,1,1,1);
ainj=setcellstruct(ainj,'NumIntSteps',indK,10,1,1);

 
%Variab=atVariableBuilder(ainj,...
%    {'SI1D\w*','SI2E\w*','SI1E\w*','SJ1E\w*','SJ2E\w*','SJ1B\w*'},{{'PolynomB',{1,1}}});

 Variab=atVariableBuilder(ainj,...
     {...
     [indK1e, indK8e],...
     [indK2e, indK7e],...
     [indK3e, indK6e],...
     [indK4e, indK5e]...
     [indK1, indK8],...
     [indK2, indK7],...
     [indK3, indK6],...
     [indK4],...
     [indK5],...
     [indKc'],...
     },{{'PolynomB',{1,1}}});

% Variab=struct(...
%     'Indx',{indK1,...
%     indK2,...
%     indK3,...
%     indK4},...
%     'Parameter',{...
%     {'PolynomB',{1,1}},...
%     {'PolynomB',{1,1}},...
%     {'PolynomB',{1,1}},...
%     {'PolynomB',{1,1}},...
%     },...
%     'LowLim',{[],[],[],[]},...
%     'HighLim',{[],[],[],[]}...
%     );
    
clear kick_static

kickers=indK;%findcells(ainj,'FamName','S[IJ][12][DE]\w*');

ainj2=ainj;

bumpS=linspace(0.5,5,8)*1e-3;
bumpS=-bumpS;

gotbump = bumpH(end);
kickerbump = bumpH(end);
wishedbump = -0.020;
presentbump = kickerbump;

bumpS = -1e-3; % initial Dbump value
ii=1;

while gotbump>wishedbump & ii<2
    
%     if mod(ii,9)==0
%         Variab2 = Variab;
%     elseif mod(ii,9)==8
%         Variab2 = Variab;
%     elseif mod(ii,9)==7
%         Variab2 = Variab;
%     elseif mod(ii,9)==6
%         Variab2 = Variab;
%     elseif mod(ii,9)==5
%         Variab2 = Variab([2:10]);
%     elseif mod(ii,9)==4
%         Variab2 = Variab([3:10]);
%     elseif mod(ii,9)==3
%         Variab2 = Variab([4:10]);
%     elseif mod(ii,9)==2
%         Variab2 = Variab([5:10]);
%     elseif mod(ii,9)==1
%         Variab2 = Variab;%([5:9]);
%     end
    
    Variab2 = Variab([7 8 9]);

    lastkicker=Variab2(1).Indx(end);

    lastkicker

    bind = findcells(ainj,'FamName','DR_01I');
    
LinConstr1=atlinconstraint(...
    bind,...
    {{'ClosedOrbit',{1}},{'ClosedOrbit',{2}}},...
    [wishedbump,injANGLE],...[presentbump+bumpS,0],...
    [wishedbump,injANGLE],...[presentbump+bumpS,0],...
    [1 1]);

sjind=findcells(ainj,'FamName','SJ1B\w*')+20; % last kicker

LinConstr2=atlinconstraint(...
    lastkicker+1,...
    {{'ClosedOrbit',{1}},{'ClosedOrbit',{2}}},...
    [0,0],...
    [0,0],...
    [1 1]);

LinConstr3=atlinconstraint(...
    length(ainj2),...
    {{'ClosedOrbit',{1}},{'ClosedOrbit',{2}}},...
    [0,0],...
    [0,0],...
    [1e-6 1e-6]); % closed out of injection cell.

kicklimit = struct(...
        'Fun',@(r,~,~) max(abs(atgetfieldvalues(r,kickers,'PolynomB',{1,1}).*atgetfieldvalues(r,kickers,'Length'))),...
        'Min',0,...
        'Max',4e-4,...
        'RefPoints',kickers(1),...
        'Weight',1e-6); % correctors within 0.4mrad . very important

Constr=[LinConstr1,LinConstr3,kicklimit];

ainj2=atmatch(ainj2,Variab2,Constr,10^-20,100,3,@lsqnonlin);%,'fminsearch');%
ainj2=atmatch(ainj2,Variab2,Constr,10^-12,1000,3);%,'fminsearch');%
ainj2=atmatch(ainj2,Variab2,Constr,10^-20,100,3,@lsqnonlin);%,'fminsearch');%
disp('request')
%disp(kickerbump+bumpS)
disp(wishedbump)

[l1,t1,c1]=atlinopt([ainj2;arcas],0,1:length([ainj2;arcas]));
cx1(ii,:)=arrayfun(@(a)a.ClosedOrbit(1),l1);
disp('obtained')
disp(cx1(ii,bind))
presentbump = cx1(ii,bind);
gotbump = cx1(ii,bind);
bumpS= -min(1e-3,abs(wishedbump-gotbump));

kick_static(ii,:)=getcellstruct(ainj2,'PolynomB',kickers,1,1);

% linear kick orbit
ainj2lin=setcellstruct(ainj2,'PolynomB',kickers,kick_static(1,:)*ii,1,1);
[l2lin,t2,c2]=atlinopt([ainj2lin;arcas],0,1:length([ainj2;arcas]));
cx1lin(ii,:)=arrayfun(@(a)a.ClosedOrbit(1),l1lin);


disp('kick strengths in [rad]')
disp(cellfun(@(a)[a.FamName ' K0L: ' num2str(a.PolynomB(1,1)*a.Length) ' rad'],ainj2(kickers),'un',0))

figure;
atplot(ainj2,[0,56],@plClosedOrbit)
ii = ii+1;
end

%%
kickers1234=findcells(ainj,'FamName','DR_K1','DR_K2','DR_K3','DR_K4');
scorK1234 = findspos(ainj1,kickers1234);
KLpercK1234 = cellfun(@(a)-a.PolynomB(1,1)*a.Length,ainj2(kickers1234),'un',1);
KLpercK1234 = 100*KLpercK1234./max(abs(KLpercK1234(3)));

ainjk = atsetfieldvalues(ainj2,kickers1234,'PolynomB',{1,1},0);

figure('units','normalized','Position',[0.1 0.1 0.8 0.6]);

yyaxis right;
scor = findspos(ainj1,indK);
KLperc = cellfun(@(a)-a.PolynomB(1,1)*a.Length,ainj2(kickers),'un',1)./0.0004*100;
b1 = bar(scor,KLperc,'FaceAlpha',0.5,'FaceColor',[0.8 0.0 0.0]);
hold on;
b2 =bar(scorK1234,KLpercK1234,0.3,'FaceAlpha',0.5,'FaceColor',[0 0.6 0]);
ylim([-110 110]);
ylabel('correctors and kickers strengths [%]');
ax=gca;
ax.FontSize=12;
yyaxis left
[l,~,~]=atlinopt([ainj2;arcas],0,1:length([ainj2;arcas]));
cod=arrayfun(@(a)a.ClosedOrbit(1),l);
s=findspos([ainj2;arcas],1:length([ainj2;arcas]));
p1=plot(s,cod,'--','Color',[0 0.0 0.8],'LineWidth',3);
hold on;
[l,~,~]=atlinopt([ainj1;arcas],0,1:length([ainj1;arcas]));
codK1234=arrayfun(@(a)a.ClosedOrbit(1),l);
s=findspos([ainj1;arcas],1:length([ainj1;arcas]));
p2=plot(s,codK1234,'--','Color',[0 0.8 0],'LineWidth',3);
[l,~,~]=atlinopt([ainjk;arcas],0,1:length([ainjk;arcas]));
codcoronly=arrayfun(@(a)a.ClosedOrbit(1),l);
s=findspos([ainjk;arcas],1:length([ainjk;arcas]));
p3=plot(s,codcoronly,'--','Color',[0.8 0.0 0],'LineWidth',3);
xlim([26,56]);
ylim([-22e-3,5e-3]);
ax=gca;
ax.FontSize=12;

xlabel('s [m]');
ylabel('horizontal position [m]');
indS3 = find(atgetcells(ainj2,'FamName','S3\w*'))'+1;
tr = linepass(ainj2(indS3:end),[wishedbump injANGLE 0 0 0 0]',1:length(ainj2(indS3:end)));

s_tr = findspos(ainj2,(indS3:length(ainj2)));

ptr = plot(s_tr,tr(1,:),'-','Color',[0.0 0.0 0],'LineWidth',3);

p4=patch([27 27.5 27.5 27],[-18 -18 -15 -15]*1e-3,[0.1 0.1 0.1],'FaceAlpha',0.5);
grid on;
atplotsyn(gca,ainj2);
legend([p1,p2,p3,ptr,p4,b2,b1],'Total bump','Kickers only','Correctors only','injected beam','Septum','Kickers','Correctors','Location','SouthEast');

saveas(gca,'InjectionBumpOnAxis.fig');
export_fig('InjectionBumpOnAxis.pdf','-r300');
export_fig('InjectionBumpOnAxis.png','-r300');


%% save steerers file
rinj = [ainj2;arcas];
rc4 = atreduce(atrotatelattice(rinj,find(atgetcells(rinj,'FamName','S3_\w*'))));
indsh = find(atgetcells(rc4,'FamName','S[HFDIJ]\w*','DQ\w*'))';

steername = sr.hsteername(1:384,'srmag/')';
kl = -cellfun(@(a)a.PolynomB(1,1)*a.Length,rc4(indsh),'un',1); % rad for kick angle, opposite sign.
k = cellfun(@(a)a.PolynomB(1,1),rc4(indsh),'un',1);

ff = fopen('HorSteererForInjOnAxis.csv','w');
fprintf(ff,'%% device, rad\n');
for ii= 1:length(steername)
fprintf(ff,'%s, %f\n',steername{ii},kl(ii));
end
fclose(ff);
save('horSteererForInjOffAxis.mat','steername','kl','k');

return


