% set OnAxis Static Bump
close all
clear 

addpath('./injection_bump');

ft = FirstTurns('ebs-simu');

ft.InitEbsSimulatorOnAxis; % also computes trajectory RM and saves it

% set correctors to zero.
ft.sh.set(ft.sh.get*0);
ft.sv.set(ft.sv.get*0);

a = load('horSteererForInjOffAxis.mat');

ft.sh.set(a.kl')


