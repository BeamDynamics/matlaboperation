function [Losses, dLosses]=getTotalLosses(average)
%  [Losses, dLosses]=getTotalLosses(average)
%
%  get Beam Loss Monitor signal
%  Losses is the average of 'average' acquisitions, dLosses is the standard
%  deviation
%  Each acquisition is 2 seconds times average
%

if nargin<1
    average=1;
end
try
    dev_BLM=tango.Device('srdiag/blm/all');
    
    for ii=1:average
        if ii>1
            pause(2);
        else
            pause(2);
        end
        loss(ii)=dev_BLM.TotalLoss.read;
        Losses=mean(loss);
        dLosses=std(loss);
    end

catch exc
    char(exc)
    getReport(exc,'extended');
    error('could not access to TotalLoss attribute of srdiag/blm/all');
end
end
