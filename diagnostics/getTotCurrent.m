function Cur=getTotCurrent()
%
%
% get Total Current
%
%

try
    
    %open 
    Cur = tango.Attribute('srdiag/beam-current/total/Current').value;
     
catch exc
    char(exc)
    getReport(exc,'extended');
    error('could not read Current')
end

