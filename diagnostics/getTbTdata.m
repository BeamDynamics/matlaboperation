function [h,v,s] = getTbTdata(obj)
% read TbT data



% read BPM TbT buffer counter
data_counter = obj.bpm_trigger_counter();

% pause(1.0) % necessary!! or later trigger counter re-reading will be already after KE shot!
% necessary!! or could read two identical buffers!

% trigger injection and data acquisition
if strcmp(obj.machine,'sr') % & ~simulator
    Ke      = tango.Device(obj.ke);
    Ke.On(); % dvcmd(Ke(1),'DevOn');
end

% define expected trigger increase value
if obj.bpm_trigger_counter_step == length(obj.indBPM)
    deltatrig = obj.bpm_trigger_counter_step - sum(obj.status_bpm()==0); % all counter increase
end

% wait for trigger couter to change
integralwaited = 0.0;
dt =0.2;
while obj.bpm_trigger_counter() == data_counter ||...
        obj.bpm_trigger_counter() < data_counter + deltatrig
    disp('waiting for fresh data');
    pause(dt);
    integralwaited = integralwaited + dt;
    if integralwaited>10
        warning('Waiting too long for new data, take what is available');
        disp(['trigger counter before acquisition: ' num2str(data_counter)]);
        disp(['trigger counter during acquisition: ' num2str(obj.bpm_trigger_counter())]);
        disp(['expected step: ' num2str(obj.bpm_trigger_counter_step)]);
        disp('try disable-enable srdiag/bpm/all/TBT_Enable');
        TBT_enable = tango.Attribute('srdiag/bpm/all/TBT_Enable');
        TBT_enable.set = False; 
        pause(1);
        TBT_enable.set = True;
        break
    end
    disp(['next data ' num2str(obj.bpm_trigger_counter()) ' before Ke: ' num2str(data_counter)]);
end
 pause(1.0)
% read BPM Turn-by-Turn
h = obj.hor_bpm_TBT();
v = obj.ver_bpm_TBT();
s = obj.sum_bpm_TBT();

% normalize sum signal for spark and brilliance
%compsumsig = load('/machfs/MDT/2019/2019_11_29/forSimoneTBTSumCompensation.mat');
%comp = repmat(compsumsig.TBTcomp,1,50);

compsumsig = load('/mntdirect/_machfs/MDT/2019/2019_12_03/NEWSUMcompensationFactors.mat');
comp = repmat(compsumsig.RENEWTBTcomp',1,size(s,2));


s = s .* comp;

% sumsignal = sig(:,obj.first_turn_index); % measured signal
% 
% s = (sumsignal-obj.sum_signal_background)./...
%     (obj.sum_signal_beam-obj.sum_signal_background);

end