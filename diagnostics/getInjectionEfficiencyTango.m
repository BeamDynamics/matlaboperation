function IE=getInjectionEfficiencyTango(averagingtime)
% IE=getInjectionEfficiencyTango(averagingtime)
%
%

iedev=tango.Device('sr/xfrefflibera/1');
for ii=1:averagingtime

    IE(ii)=iedev.SY_SR_InjectionEfficiency.read;

    pause(1)

end

IE(isnan(IE))=0;

IE=mean(IE);

return
