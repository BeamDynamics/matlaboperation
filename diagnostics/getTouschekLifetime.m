function LT_corrected=getTouschekLifetime(tauvac,averagingtime)
%LT_corrected=getTouchekLifetime(tauvac,averagingtime)
%
% corrects measured lifetime of 7/8 multibuynch considering current in
% single bunch, lifetime of single bunch and current decay 
% normalized to 5pm vertical emittance.
%
% tauvac= vacuum lifetime in [h]
% averagingtime= time to average lifetime measurement [s]
% 
%see also: getLifetimeLibera, getVEmittance, getTotCurrent, BunchLength



% get  lifetime
LT0 = getLifetimeLibera(averagingtime); % 

% correct for current decay (rescale to 1mA/bunch)
Itot=getTotCurrent()/1000;
ey=getVEmittance(1)*1e12; %[pm] 10 waits 1 second and makes 10 averages

try
    fillid=tango.Device('sys/machstat/tango');
catch err
    disp(err)
    disp('could not read machine mode from sys/machstat/tango assuming not 16bunch');
    fillid.Sr_mode.read = 1; % multibunch
end

if fillid.Sr_mode.read==2
    
    modefill='16 bunch';%getFillingMode();
    LTs = Inf;
    Isin=0.0;
    
else
    
    modefill='multibunch';
    LTs = getLifetimeSingle(); %
    Isin=getSingleBunchCurrent()/1000;
    
end

switch modefill
    case '16 bunch'
    nbunch=16;
    IbNorm=(0.092)/nbunch; % mA
    otherwise 
    nbunch=992*7/8;
    IbNorm=(0.198)/nbunch; % mA
end

% normalization values
%IbsNorm=(4)/1;       % mA
eyNorm=5;        % pm

% bunch length parameters
Zn=.67;
Vrf=6.5e6; % reference voltage

latfile = load('/operation/beamdyn/matlab/optics/sr/theory/betamodel.mat');
rp = ringpara(latfile.betamodel);

U0 = rp.U0; % 2.53e6;
E0 = rp.E0; % 6.00e9;
alpha = rp.alphac; % 1.78e-4;
sigdelta = rp.sigma_E; % 1.06e-3;
circ = findspos(latfile.betamodel,length(latfile.betamodel)+1); % 844.039;
h = rp.harm;

Ib=Itot/nbunch;
%Ibs=Isin/1;

BL = abs(BunchLength(Ib,Zn,Vrf,U0,E0,h,alpha,sigdelta,circ));
BLNorm = abs(BunchLength(IbNorm,Zn,Vrf,U0,E0,h,alpha,sigdelta,circ));
%BLs = BunchLength(Ibs,Zn,Vrf,U0,E0,h,alpha,sigdelta,circ);
%BLsNorm = BunchLength(IbsNorm,Zn,Vrf,U0,E0,h,alpha,sigdelta,circ);

LTs_tou=(LTs*tauvac)/(tauvac-LTs); %remove vacuum lifetime component
%LTs_tou_cor = LTs_tou * (Ibs/IbsNorm) * (BLsNorm/BLs) * sqrt(eyNorm/ey); % assume same emittnace (not sure)

LT_tou=(Itot-Isin)/(Itot/LT0-Isin/LTs_tou-Itot/tauvac);
%LT_tou=(Itot)/(Itot/LT0-Itot/tauvac);

% normalize to current, bunch length, emittance
LT_tou_cor = LT_tou * (Ib/IbNorm) * (BLNorm/BL) * sqrt(eyNorm/ey);

LT_corrected=LT_tou_cor;

return
