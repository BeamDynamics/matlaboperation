function [BLM,BLM1,BLM2]=getBLM(averagingsamples)
%[LT]=getBLM
%
% get Beam Loss Monitor signal
%
%
%see also: 

try
    dev_BLM=tango.Device('test/d-libera-blm/2');
    
    pause(0.185*(averagingsamples+5));
    
    BLM1=dev_BLM.SaHistoryA.read;
    % BLM2=dev_BLM.SaHistoryC.read;
        
    BLM1=-sum(BLM1((end-averagingsamples+1):end))/averagingsamples;
    % BLM2=-sum(BLM2((end-averagingsamples+1):end))/averagingsamples;
        
    BLM=abs((BLM1)-4.4e4);


catch exc
    char(exc)
    getReport(exc,'extended');
    error('could not read BLM C4 A (sum) ')
    
end

