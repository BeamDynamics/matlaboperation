function [hor,ver,sig] = getSAdata_EBS_SIMU(obj)


% read BPM TbT buffer counter
    data_counter = obj.bpm_trigger_counter();
    
    pause(1.0) % necessary!! or later trigger counter re-reading will be already after KE shot!
               % necessary!! or could read two identical buffers!
               
    % wait for trigger couter to change
    integralwaited = 0.0;
    dt =0.2;
    while obj.bpm_trigger_counter() == data_counter
        disp('waiting for fresh data');
        pause(dt);
        integralwaited = integralwaited + dt;
        if integralwaited>10
            warning('Waiting too long for new data, take what is available');
            break
        end
        disp(['next data ' num2str(obj.bpm_trigger_counter()) ' last measure: ' num2str(data_counter)]);
    end
    pause(2.0);
    
    hor = obj.hor_bpm_SA();
    ver = obj.ver_bpm_SA();
    sig = obj.sum_bpm_SA(); % signal not available in simulator tango.Attribute(obj.sum_bpm_SA).value;
                
    if isempty(sig)
        sig = 1e6*ones(size(hor));
    end
    
    
end

