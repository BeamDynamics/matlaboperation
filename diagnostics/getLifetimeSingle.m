function [LT]=getLifetimeSingle()
%[LT]=getLifetimeSingle
%
% get Lifetime from single bunch
%
% LT [hours]: lifetime
%
%see also: 



try
   %  error('missing device for SINGLE BUNCH lifetime!')
   
    %open device and get initial lifetime
    
    LT = tango.Attribute('srdiag/beam-current/single/Lifetime').value/3600;
    
catch exc
    char(exc)
    getReport(exc,'extended');
    error('could not read single bunch Lifetime')
    
end

