function Cur=getSingleBunchCurrent()
%
%
% get Total Current
%
%

try
    
  Cur = tango.Attribute('srdiag/beam-current/single/Current').value;
    
catch exc
    char(exc)
    getReport(exc,'extended');
    error('could not read Current')
end

