function [h,v,s] = getTbTdata_ESRF_SR(obj)
% read TbT data



% read BPM TbT buffer counter
data_counter = obj.bpm_trigger_counter();

pause(1.0) % necessary!! or later trigger counter re-reading will be already after KE shot!
% necessary!! or could read two identical buffers!

% trigger injection and data acquisition
if strcmp(obj.machine,'sr') & ~simulator
    Ke.On(); % dvcmd(Ke(1),'DevOn');
end

% wait for trigger couter to change
integralwaited = 0.0;
dt =0.2;
while obj.bpm_trigger_counter() == data_counter
    disp('waiting for fresh data');
    pause(dt);
    integralwaited = integralwaited + dt;
    if integralwaited>10
        warning('Waiting too long for new data, take what is available');
        break
    end
    disp(['next data ' num2str(obj.bpm_trigger_counter()) ' before Ke: ' num2str(data_counter)]);
end

% read BPM Turn-by-Turn
h = obj.hor_bpm_TBT();
v = obj.ver_bpm_TBT();
sig = obj.sum_bpm_TBT();
sumsignal = sig(:,obj.first_turn_index); % measured signal

s = (sumsignal-obj.sum_signal_background)./...
    (obj.sum_signal_beam-obj.sum_signal_background);

end