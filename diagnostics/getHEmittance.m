function [XEmitAv,XSigma,XEmit,XS,XE]=getHEmittance(integrationtime)
% get emittances
% 
% function taken from getZSigmaSomeCams by Friederike Ewald

camera_list = {'d01' 'd17'  'd27' 'id07' 'id25'};

XS = zeros(length(camera_list),integrationtime);
XE = zeros(length(camera_list),integrationtime);
XEAv=zeros(integrationtime,1);

for k = 1:integrationtime

%   camera_list = {'c3' 'c5' 'c10' 'c11' 'c14' 'c18' 'c21' 'c25' 'c26' 'c29' 'c31' 'd9-a' 'd9-b' 'id25-a' 'id25-b'};
   for i = 1:length(camera_list)
       try
           %disp(camera_list{i})
%            dev_XSigma  =   machdev.cache(['sigmaXcamera_',strrep(camera_list{i},'-','_')],['sr/d-emit/',camera_list{i},'']);
%            dev_XEmit   =   machdev.cache(['emittanceXcamera_',strrep(camera_list{i},'-','_')],['sr/d-emit/',camera_list{i},'']);
% 
%            XS(i,k)  = dev_XSigma.XSigmaScreen.read;
%            XE(i,k)  = dev_XEmit.XEmittance.read;
%            
           XS(i,k)  = tango.Attribute(['srdiag/emittance/' camera_list{i} '/Sigma_h']).value; %dev_ZSigma.ZSigmaScreen.read;
           XE(i,k)  = tango.Attribute(['srdiag/emittance/' camera_list{i} '/Emittance_h']).value; %dev_ZEmit.ZEmittance.read;
          
           
       catch exc
           getReport(exc,'extended');
           XS(i,k)  = nan;
           XE(i,k)  = nan;
           disp('caught')
       end
   end
   try
     %  dev_XAverageEmit   =   machdev.cache('aveXemit',['sr/d-emit/','average']);
     %  XEAv(k)  = dev_XAverageEmit.Xemittance.read;
     XEAv(k)  = mean(XE(:,k)); 
   catch exc
       getReport(exc,'extended');
       XEAv(k) = nan;
   end
   pause(1)
   disp(['waited: ' num2str(k) '/' num2str(integrationtime) ' seconds']);
end
XSigma = mean(XS,2);
XEmit = mean(XE,2);
XEmitAv = mean(XEAv);


return

