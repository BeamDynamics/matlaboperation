function AcquireData(fileLabel,nsec,naq)
% 
%   function AcquireData(fileLabel,nsec,nmeas)
%
%   This function acquires LT, Current, Emittances, Orbit, tunes, energy
%   spread and losses from the accelerator and saves the data in  matlab
%   file.
%
%   input fileLabel is a label attached to file name. The file name will
%   also include date and time of the measurement.
%
%   nsex is the seconds to wait before lifetime reading, default is 7
%   nmeas is the number of measurement for each acquisition (losses), default is 5
%
%


%% get data

%%
if nargin<2
    nsec=7; % lifetime
    nmeas=nsec;
end
if nargin<3
    naq=5; % losses
end

filename = [fileLabel '_' datestr(now)];
try
    eh07 = tango.Attribute('srdiag/emittance/id07/Emittance_H').value;
catch
    eh07 = NaN;
end
try
    eh25 = tango.Attribute('srdiag/emittance/id25/Emittance_H').value;    
catch
    eh25 = NaN;
end

try
    ev07 = tango.Attribute('srdiag/emittance/id07/Emittance_V').value;
catch
    ev07 = NaN;
end
try
    ev25 = tango.Attribute('srdiag/emittance/id25/Emittance_V').value;
catch
    ev25 = NaN;
end
try
    eh09 = tango.Attribute('srdiag/emittance/d09/Emittance_H').value;
catch
    eh09 = NaN;
end
try
    eh27 = tango.Attribute('srdiag/emittance/d27/Emittance_H').value;    
catch
    eh27 = NaN;
end

try
    ev09 = tango.Attribute('srdiag/emittance/d09/Emittance_V').value;
catch
    ev09 = NaN;
end
try
    ev27 = tango.Attribute('srdiag/emittance/d27/Emittance_V').value;
catch
    ev27 = NaN;
end

try
    es1 = tango.Attribute('srdiag/beam-espread/1/EnergySpread').value;
catch
    es1=NaN;
end
try
    es2 = tango.Attribute('srdiag/beam-espread/2/EnergySpread').value;
catch
    es2=NaN;
end
em07=tango.Device('srdiag/emittance/id07');
sigmax_screen=em07.SigmaScreen_H.read;
sigmay_screen=em07.SigmaScreen_V.read;
sigmax_source=em07.SigmaSource_H.read;
sigmay_source=em07.SigmaSource_V.read;


shakerh=tango.Device('srdiag/wave/shaker-c26-blowup-h');
shakerv=tango.Device('srdiag/wave/shaker-c26-blowup-v');
Ampl_shaker_h = shakerh.Amplitude.set;
Ampl_shaker_v = shakerv.Amplitude.set;
I = tango.Attribute('srdiag/beam-current/total/Current').value;
I_single = tango.Attribute('srdiag/ict/c10/Current').value;

% LT = nan(nmeas,1);
% LT_ct = nan(nmeas,1);
% LT_single = nan(nmeas,1);
% TotLosses = nan(nmeas,1);
% Losses=zeros(nmeas,128);
libLT=tango.Device('srdiag/bpm/lifetime');
it =nsec ;
% for ii=1:it
%     pause(1);
%     try
%         Output_LT = libLT.ReadLifetime(int32(20*1));
%         LT(ii)=Output_LT(1);
%         eLT(ii)=Output_LT(3); % std of measurement divided by number of averaged data (brilliance station)
%         
%     catch
%         LT(ii) = NaN;
%     end
% end

for it = 1: naq
    try
        LT(it) = libLT.Lifetime.read;
        eLT(it) = 0.1;
    catch
        LT(it) = NaN;
        eLT(it) = NaN;
    end
    pause(1);
    try
        LT_single(it) = tango.Attribute('srdiag/ict/c10/Lifetime').value;
    catch
        LT_single(it) = NaN;
    end
     try
        LT_ct(it) = tango.Attribute('srdiag/beam-current/total/Lifetime').value;
    catch
        LT_ct(it) = NaN;
    end
    TotLosses(it)=tango.Attribute('srdiag/blm/All/TotalLoss').value;
    Losses(it,:)=tango.Attribute('srdiag/blm/All/Losses').value;
%     pause(2)
end


% BLD RF losses
%LossesC25RF(1,:)=[];%tango.Attribute('srdiag/blm/c25rf-2/SaA').value;
%LossesC25RF(2,:)=[];%tango.Attribute('srdiag/blm/c25rf-2/SaB').value;
%LossesC25RF(3,:)=[];%tango.Attribute('srdiag/blm/c25rf-2/SaC').value;
%LossesC25RF(4,:)=[];%tango.Attribute('srdiag/blm/c25rf-2/SaD').value;

% orbit 
orbh=tango.Attribute('srdiag/bpm/all/All_SA_HPosition').value;
orbv=tango.Attribute('srdiag/bpm/all/All_SA_VPosition').value;

th=tango.Attribute('srdiag/beam-tune/main/Qh').value;
tv=tango.Attribute('srdiag/beam-tune/main/Qv').value;

%FillPatt=tango.Attribute(...
%    'srdiag/current-per-bunch/wphd/UnmaskedCurrentsMilliAmpere').read;

try
FillPatt=tango.Attribute(...
    'srdiag/current-per-bunch/wr620zi/UnmaskedCurrentsMilliAmpere').read;
catch
    FillPatt = NaN;
end

% RF
RF_VoltTRA1=tango.Attribute('srrf/anodloop/tra1/Voltage').set;

RF_VoltCAV1=tango.Attribute('srrf/ssa-driver/c25-1/RFAmplitude').set;
RF_VoltCAV2=tango.Attribute('srrf/ssa-driver/c25-2/RFAmplitude').set;
RF_VoltCAV3=tango.Attribute('srrf/ssa-driver/c25-3/RFAmplitude').set;

RF_Volt = RF_VoltTRA1 + RF_VoltCAV1 +RF_VoltCAV2 +RF_VoltCAV3;

% Collimators
coll13_int=tango.Attribute('srdiag/collimator/c13-int/Position').value;
coll13_ext=tango.Attribute('srdiag/collimator/c13-ext/Position').value;
coll24_int=tango.Attribute('srdiag/collimator/c24-int/Position').value;
coll24_ext=tango.Attribute('srdiag/collimator/c24-ext/Position').value;

%scrapers
c11int = tango.Device('srdiag/scraper/c11-int');
c04int = tango.Device('srdiag/scraper/c04-int');
c04ext = tango.Device('srdiag/scraper/c04-ext');
c06up = tango.Device('srdiag/scraper/c06-up');
c06down = tango.Device('srdiag/scraper/c06-down');

scr_c11int_pos = c11int.Position.set;
scr_c04int_pos = c04int.Position.set;
scr_c04ext_pos = c04ext.Position.set;
scr_c06up_pos = c06up.Position.set;
scr_c06down_pos = c06down.Position.set;

% halo monitors stuff
halo1_dev = tango.Device('srdiag/halo-monitor/c10-1');
halo2_dev = tango.Device('srdiag/halo-monitor/c11-1');

halo1.Beam = halo1_dev.Beam.read;
halo1.BeamNoOffset = halo1_dev.BeamNoOffset.read;
halo1.Halo = halo1_dev.Halo.read;
halo1.HaloNoOffset = halo1_dev.HaloNoOffset.read;
halo1.OffsetDown = halo1_dev.OffsetDown.read;
halo1.OffsetUp = halo1_dev.OffsetUp.read;

halo2.Beam = halo2_dev.Beam.read;
halo2.BeamNoOffset = halo2_dev.BeamNoOffset.read;
halo2.Halo = halo2_dev.Halo.read;
halo2.HaloNoOffset = halo2_dev.HaloNoOffset.read;
halo2.OffsetDown = halo2_dev.OffsetDown.read;
halo2.OffsetUp = halo2_dev.OffsetUp.read;



save(filename,'eh07','eh25','ev07','ev25',...
    'eh09','eh27','ev09','ev27',...
    'I','I_single','es1','es2',...
    'LT','eLT','LT_ct','LT_single','Losses','TotLosses',...'LossesC25RF',...
    'th','tv','orbh','orbv','FillPatt','Ampl_shaker_h','Ampl_shaker_v',...
    'RF_VoltTRA1','RF_VoltCAV1','RF_VoltCAV2','RF_VoltCAV3','RF_Volt',...
    'coll13_int','coll13_ext','coll24_int','coll24_ext',...
    'scr_c11int_pos','scr_c04int_pos','scr_c04ext_pos',...
    'scr_c06up_pos','scr_c06down_pos',...
    'sigmax_screen','sigmay_screen','sigmax_source','sigmay_source',...
    'halo1','halo2');

end
