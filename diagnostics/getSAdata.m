function [hor,ver,sig] = getSAdata(obj)
% read BPMs Slow Acquisition

% check stutus of BPM DS. wait 5s and in case countinue nevertheless 
count = 1;
while ~isempty(find(obj.status_bpm() ~= 1, 1)) && count < 3
    
    pause(1)
    disp("bpm status not ok, waiting" + count)
    
    count = count +3;
end

hor = obj.hor_bpm_SA();
ver = obj.ver_bpm_SA();
sig = obj.sum_bpm_SA(); % signal not available in simulator tango.Attribute(obj.sum_bpm_SA).value;

if isempty(sig)
    sig = 1e6*ones(size(hor));
end


end

