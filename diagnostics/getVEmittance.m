function [ZEmitAv,ZSigma,ZEmit,ZS,ZE]=getVEmittance(integrationtime)
% get emittances
%
% integrationtime must be an integer
%
% function taken from getZSigmaSomeCams by Friederike Ewald
camera_list = {'d01' 'd17'  'd27' 'id07' 'id25'}; %'c14' 'c5'

ZS = zeros(length(camera_list),integrationtime);
ZE = zeros(length(camera_list),integrationtime);
ZEAv=zeros(integrationtime,1);

for k = 1:integrationtime

%   camera_list = {'c3' 'c5' 'c10' 'c11' 'c14' 'c18' 'c21' 'c25' 'c26' 'c29' 'c31' 'd9-a' 'd9-b' 'id25-a' 'id25-b'};
   for i = 1:length(camera_list)
       try
           % %disp(camera_list{i})
           %dev_ZSigma  =   machdev.cache(['sigmaZcamera_',strrep(camera_list{i},'-','_')],['sr/d-emit/',camera_list{i},'']);
           %dev_ZEmit   =   machdev.cache(['emittanceZcamera_',strrep(camera_list{i},'-','_')],['sr/d-emit/',camera_list{i},'']);
           % ZS(i,k)  = dev_ZSigma.ZSigmaScreen.read;
           % ZE(i,k)  = dev_ZEmit.ZEmittance.read;
           
           ZS(i,k)  = tango.Attribute(['srdiag/emittance/' camera_list{i} '/Sigma_v']).value; %dev_ZSigma.ZSigmaScreen.read;
           ZE(i,k)  = tango.Attribute(['srdiag/emittance/' camera_list{i} '/Emittance_v']).value; %dev_ZEmit.ZEmittance.read;
           
       catch exc
           char(exc)
           getReport(exc,'extended');
           ZS(i,k)  = nan;
           ZE(i,k)  = nan;
           disp('caught')
       end
   end
   try
       % 
       %dev_ZAverageEmit   =   machdev.cache('avgZemit',['sr/d-emit/','average']);
       ZEAv(k)  = mean(ZE(:,k)); % dev_ZAverageEmit.Zemittance.read;
   catch exc
       char(exc)
       getReport(exc,'extended');
       ZEAv(k) = nan;
   end
   pause(0.1)
   %disp(['waited: ' num2str(k*0.1) '/' num2str(integrationtime) ' seconds']);
end
% mean over cameras
ZSigma = mean(ZS,2); % average in time
ZEmit = mean(ZE,2); % average in time
ZEmitAv = mean(ZEAv);% time average of average emittance 


return

