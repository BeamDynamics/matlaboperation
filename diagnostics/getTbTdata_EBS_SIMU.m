function [h,v,s] = getTbTdata_EBS_SIMU(obj)
% read TbT data



% read BPM TbT buffer counter
data_counter = obj.bpm_trigger_counter();

pause(1.0) % necessary!! or later trigger counter re-reading will be already after KE shot!
% necessary!! or could read two identical buffers!

% wait for trigger couter to change
integralwaited = 0.0;
dt =0.2;
while obj.bpm_trigger_counter() == data_counter
    disp('waiting for fresh data');
    pause(dt);
    integralwaited = integralwaited + dt;
    if integralwaited>10
        warning('Waiting too long for new data, take what is available');
        break
    end
    disp(['next data ' num2str(obj.bpm_trigger_counter()) ' before Ke: ' num2str(data_counter)]);
end


try
    if (obj.noise)
        
        obj.initial_coordinates.set( [obj.injoff, 0,0,0,0,0]+randn(1,6).*obj.injectorRMSNoise);
    end
    pause(1);
    % read BPM Turn-by-Turn
    h = obj.hor_bpm_TBT();
    v = obj.ver_bpm_TBT();
    
    % warning('logic/scalar error && -> & !')
    s = ~isnan(h) & ~isnan(v) ;
catch err
    disp(err)
    error('ERROR RingControl.measuretrajectory: did you set BPM TbT?')
    return
end

            

end