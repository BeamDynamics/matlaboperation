function [LTlib,dLTlib]=getLifetimeLibera(n_meas)
%[LT,LTparams]=getLifetimeLibera(n_meas)
%
% get Lifetime from Libera
%
% n_meas  (seconds) = number of measurements
% averaging time is a property of the device
%
% LT [h]: lifetime
% dLT [h]: standard deviation of n_meas measurements of LT
%
% see also: getTotalLosses

if nargin<1
    n_meas=1;
end
LT_=nan(n_meas,1);
for ii=1:n_meas
    try
        pause(1);
        LT_(ii) = tango.Attribute('srdiag/bpm/lifetime/Lifetime').value/3600;% h
        LTct_(ii) = tango.Attribute('srdiag/pct/c10/Lifetime').value/3600;% h
    catch 
        try
            pause(1);
            LT_(ii) = tango.Attribute('srdiag/bpm/lifetime/Lifetime').value/3600;% h
            LTct_(ii) = tango.Attribute('srdiag/pct/c10/Lifetime').value/3600;% h
        catch
            try
                pause(1);
                LT_(ii) = tango.Attribute('srdiag/bpm/lifetime/Lifetime').value/3600;% h
                LTct_(ii) = tango.Attribute('srdiag/pct/c10/Lifetime').value/3600;% h
            catch
                
                LTct_(ii) = tango.Attribute('srdiag/pct/c10/Lifetime').value/3600;% h
                LT_(ii)=LTct_(ii);
                warning('LT was not measured');
            end
        end
    end
end
LTlib=mean2(LT_);
LTct=mean2(LTct_);

dLTlib=std2(LT_);
dLTct=std2(LTct_);

LT=mean([LTlib,LTct]);
dLT=mean([dLTlib,dLTct]);
end

