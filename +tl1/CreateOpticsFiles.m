function CreateOpticsFiles(r,opticsname,varargin)
% CREATEOPTICSFUILES(r,opticsname,'property',value,...), 
% given an AT lattice R creates the relevant files for operation
%
% operation folder 2018 is APPHOME/tl1/optics/settings/(OPTICSNAME)
% operation folder 2019 could become APPHOME/<commissioingtoolsrepository>/optics/tl1/(OPTICSNAME) 
%
% optional input ('NAME',val,'Name2',val2,...)
% 'OperationFolder'  : commissioningtools folder where optics/ebssr/theory
%                      link is present.
% 'MeasuredResponse' : folder containing ouput of full RM measurement
% 'errtab' : table of errors
% 'cortab' : table of corrections
%
% 
% example:
% load /machfs/liuzzo/EBS/beamdyn/matlab/+tl1/TL1_EBS.mat
% tl1.CreateOpticsFiles(atsetenergy(THERING,0.2e9),'TL1');
% 
%see also: LoadOpticsConfiguration

global APPHOME
p=inputParser;
addRequired(p,'r',@iscell);
addRequired(p,'opticsname',@ischar);
addOptional(p,'OperationFolder',APPHOME);
addOptional(p,'MeasuredResponse',[]);
addOptional(p,'errtab',[],@istable);
addOptional(p,'cortab',[],@istable);

parse(p,r,opticsname,varargin{:});
r           = p.Results.r;
opticsname  = p.Results.opticsname;
OpeFolder   = p.Results.OperationFolder;
rm          = p.Results.MeasuredResponse;
errtab      = p.Results.errtab;
cortab      = p.Results.cortab;

if isempty(errtab)
    errtab = atcreateerrortable(r);
end

if isempty(cortab)
    cortab = atgetcorrectiontable(r,r); % zeros
end

fulfold=fullfile(OpeFolder,'optics','tl1',opticsname);

curdir=pwd;
mkdir(fulfold);
cd(fulfold);

% AT lattice
betamodel=r;
save('betamodel.mat','betamodel');

% AT lattice for simulator with errors and corrections
disp('errors and correction lattice for simulator (provide errtab and cortab or zeros will be set)')
errmodel=atseterrortable(r,errtab);
errmodel=atsetcorrectiontable(errmodel,cortab);
save('errmodel.mat','errmodel','cortab','errtab');


% save image of lattice optics
f=figure;
atplot(r);
saveas(gca,[opticsname '.jpg']);
close(f);

rfull=tl2.model(r);

% compute fast model once for all
% merge DQ
indDQmark=atgetcells(r,'FamName','PINHOLE_D\w*','CellCenter');
r(indDQmark)=[]; % checklattice should merge all splitted elements.

% merge 2PW and transform back to quadrupole
indQF8dip = find(atgetcells(r,'FamName','QF8\w*') & atgetcells(r,'Class','Bend'))';
r=atsetfieldvalues(r,indQF8dip,'BendingAngle',0);
r=atsetfieldvalues(r,indQF8dip,'Class','Quadrupole');
r=atsetfieldvalues(r,indQF8dip,'PassMethod','StrMPoleSymplectic4Pass');

% get fast model (QF8D merged, not ok for optics computations)
rmod=tl2.model(r,'reduce',true,'keep','BPM.*|ID.*');

% % store family magnet strengths to file (or in the format for magnet application, to be loaded)

% orbit response, theory always,
% 
% tl2.autocor_model(rfull);
% 
% % measured if required
% if ~isempty(rm)
% tl2.autocor_model(rfull,'exp',rm);
% end

  
cd(curdir);

end
