function [hsc,vsc,hs0,vs0]=CloseTrajectoryAfter1Turn(obj,varargin)
% this functions uses the last N steerers before the last BPM with signal
% to make the last N BPMS readings equal to the previous turn
%
% optional inputs
% ...,'N_bpms_to_match',2,... (int, default = 2)
%                             number of BPMS to be matched to the reding of
%                             the preceeding turn. also number of
%                             correctros used for this purpose (before the
%                             last-1 Bpm.
% ...,'ignore_on_axis',false,... (boolean, default = false)
%                               performs correction also if not on axis.
% ...,'set_h_cor',flase,...  (boolean, default = false)
%                             sets in the accelerator the computed
%                             horizontal correction
% ...,'set_v_cor',flase,...  (boolean, default = false)
%                             sets in the accelerator the computed
%                             vertical correction
%
% Outputs:
% hsc,vsc horizontal and vertical steerers after correction
% hs0,vs0 horizontal and vertical steerers before correction 
% (use obj.sh.set(hs0) to go back to initial values)
%
% Example of usage:
%
% [hsc,vsc,hs0,vs0] = ft.CloseTrajectoryAfter1Turn(...
%                       'ignore_on_axis',true,...
%                       'set_h_cor',true,...
%                       'set_v_cor',true,...
%                       'N_bpms_to_match',15)
%
% Conditions:
% obj.injoff = 0.0 (not strict requirement)
% available signal on 320 + N BPMs
%
% Notice: 
% from simulations this correction might seem to lead to a worst situation.
% Followed by ft.correction, the improvement is evident.
% 
%see also:

p = inputParser;

addRequired(p,'obj');
addOptional(p,'N_bpms_to_match',2,@(x)x>=2);
addOptional(p,'ignore_on_axis',false);
addOptional(p,'h_cor',true);
addOptional(p,'v_cor',true);
addOptional(p,'set_h_cor',false);
addOptional(p,'set_v_cor',false);

parse(p,obj,varargin{:});

N = p.Results.N_bpms_to_match;
ignonax = p.Results.ignore_on_axis;
h_cor = p.Results.h_cor;
v_cor = p.Results.v_cor;
set_h_cor = p.Results.set_h_cor;
set_v_cor = p.Results.set_v_cor;


if obj.injoff ~= 0.0 && ~ignonax
    error('not on axis. use ...,''ignore_on_axis'',true,... option to override this test.')
end

% get reference trajectory if not on axis
if ~obj.injoff == 0
    [t0] = obj.findNturnstrajectory6Err(obj.rmodel,[0,0],[0 0],[0 0]);
    reforbit = t0([1,3],1:N);
else
    reforbit = zeros(2,N);
end

if isempty(obj.last_measured_trajectory)
    
    t = obj.measuretrajectory;
else
    t = obj.last_measured_trajectory;
end

% get number of BPMs with signal
nbpms_with_signal = find(~isnan(t(1,:)),1,'last');
minimum_nbpms = length(t(1,:))/obj.nturns + N;

% return error if too few BPMs
if nbpms_with_signal < minimum_nbpms
    error(['not enough BPMs with signal: ' ...
        num2str(nbpms_with_signal) ' < ' num2str(minimum_nbpms)])
end


selbpm = false(size(repmat(obj.getEnabledBPM,1,obj.nturns)));
selbpm(length(obj.indBPM) + [1:N])=true;
selbpm = selbpm & repmat(obj.getEnabledBPM,1,obj.nturns);

spos_last_bpm = findspos(obj.rmodel,obj.indBPM(N));
spos_cors = findspos(obj.rmodel,obj.indHst);
ind_last_usable_cor = find(spos_cors<spos_last_bpm,1,'last');

selcor = false(size(obj.indHst));
selcor([1:ind_last_usable_cor,(length(obj.indHst)-(N-ind_last_usable_cor)):length(obj.indHst)])=true;
selh = obj.getEnabledHsteerers & selcor;

selcor = false(size(obj.indVst));
selcor([1:ind_last_usable_cor,(length(obj.indVst)-(N-ind_last_usable_cor)):length(obj.indVst)])=true;
selv = obj.getEnabledVsteerers & selcor;


% get target trajectory = turn 1 trajectory - reforbit
cortraj = t([1,2],1:N) - reforbit;

% get RM for steerers
RMH  = obj.ModelRM.TrajHCor{1}(selbpm,selh);
RMV  = obj.ModelRM.TrajVCor{3}(selbpm,selv);

%correct
hs0 = obj.sh.get;
vs0 = obj.sv.get;
hsc = hs0;
vsc = vs0;
 
if h_cor
    
    hcor = obj.computecorrection('e',RMH,cortraj(1,:)',floor(N/2));
    
    hc = zeros(size(hs0));
    hc(selh)=hcor;
    disp('correction computed');
    disp(hcor)
    if set_h_cor
        
        hsc = hs0-hc;
        obj.sh.set(hsc);
        
        disp('correction set');
    
    end
end


if v_cor
    
    vcor = obj.computecorrection('e',RMV,cortraj(2,:)',floor(N/2));
    vc = zeros(size(vs0));
    vc(selv)=vcor;
    disp('correction computed');
    disp(vcor)
    
    if set_v_cor
        vsc = vs0-vc;
        obj.sv.set(vsc);
        disp('correction set');
   
    end
end




end
