function ModelRM = MeasureTrajectoryRM(obj,varargin)
% measure trajectory RM (single sided)
% optional inputs:
% 'h',true
% 'v',true
% 'cv',true
% 'sp',true
% 'RF',true
% 
% 'indBPM'  : indexes among BPMs to use  (default: all)
% 'indHCor' : indexes among hor correctors to use  (default: all)
% 'indVCor' : indexes among verr correctors to use  (default: all)
% 'kick'    : [rad] kick for RM computation
% 'delta'   : [] energy variation for frequency change RM computation
%
% acquire data for obj.nturns.
%
%see also: 

simulator = strcmp(obj.machine,'ebs-simu');

initturns = obj.nturns;
% obj.setTurns(1);

switch_on_gun_rips = true;

if simulator
    switch_on_gun_rips = false;
end

sfi = obj.savefile;

obj.savefile = false;

waitPStime = 6; % [s]
%
p = inputParser;

addRequired(p,'obj'); % default empty
addOptional(p,'h',true,@islogical); % default empty
addOptional(p,'v',true,@islogical); % default empty
addOptional(p,'cv',true,@islogical); % default empty
addOptional(p,'sp',true,@islogical); % default empty
addOptional(p,'RF',true,@islogical); % default empty
addOptional(p,'indBPM',[],@isnumeric); % default empty =  all BPM
addOptional(p,'indHCor',[],@isnumeric); % default empty =  all correctors
addOptional(p,'indVCor',[],@isnumeric); % default empty = all correctors
addOptional(p,'kick',2e-4,@isnumeric); % default empty 
addOptional(p,'delta',1e-3,@isnumeric); % default empty
addOptional(p,'kick_CV',[1e-5 1e-5],@isnumeric); % default empty
addOptional(p,'kick_SP',[1e-5 1e-5],@isnumeric); % default empty

parse(p,obj,varargin{:});

h = p.Results.h;
v = p.Results.v;
cv = p.Results.cv;
sp = p.Results.sp;
measrf = p.Results.RF;
indBPM = p.Results.indBPM;
indHCor = p.Results.indHCor;
indVCor = p.Results.indVCor;
kick = p.Results.kick;
delta = p.Results.delta;
kick_CV = p.Results.kick_CV;
kick_SP = p.Results.kick_SP;

% h=false;
% v=false;
% cv=true;
% sp=true;
% 
% measrf = false;
   
alpha_c = load('/operation/beamdyn/matlab/optics/sr/theory/alpha');
deltarf = -delta/alpha_c;
   
% start innjection once and for all

if switch_on_gun_rips
    
    gun  = tango.Device(obj.gun);
    rips = tango.Device(obj.rips);
    
    if ~strcmp(rips.State,'Running')
    rips.StartRamping()
    end
    
    gun.On();
    
    while ~strcmp(rips.State,'Running')
        disp('rips moving!')
        pause(1);
    end
    
end
if ~simulator
    deltak = kick; % rad
else
    deltak = kick; % rad
end
deltaA = 0.01; % Ampere

t0 = obj.measuretrajectory;

enabledcorH = obj.getEnabledHsteerers;
enabledcorV = obj.getEnabledVsteerers;

% remove indexes not selected
if isempty(indHCor)
    useh = true(size(enabledcorH));
else
    useh = false(size(enabledcorH));
    useh(indHCor) = true;
end

enabledcorH = enabledcorH & useh;

if isempty(indVCor)
    usev = true(size(enabledcorV));
else
    usev = false(size(enabledcorV));
    usev(indVCor) = true;
end

enabledcorV = enabledcorV & usev;

% % for quick test
% enabledcorH(4:end)=false;
% enabledcorV(4:end)=false;

% H steerers RM
ModelRM.TrajHCor{1} = [];
ModelRM.TrajHCor{3} = [];
ModelRM.TrajVCor{1} = [];
ModelRM.TrajVCor{3} = [];

if h
    hs0 = obj.sh.get;
    for ik = 1:length(hs0) %
        if enabledcorH(ik)% only if eneabled corrector
             
            disp(['H steerer' num2str(ik)])
            hs = hs0;
            hs(ik) = hs(ik) + deltak;
            
            obj.sh.set(hs);
            
            pause(waitPStime);
            
            t = obj.measuretrajectory;
            
            obj.sh.set(hs0);
            
            rmh(:,ik) = (t(1,:) - t0(1,:))./deltak; % m/rad
            rmv(:,ik) = (t(2,:) - t0(2,:))./deltak;
        else
            disp(['H steerer' num2str(ik) ' is disabled or not selected'])
            rmh(:,ik) = NaN(size(t0(1,:)));
            rmv(:,ik) = NaN(size(t0(2,:)));
            
        end
    end
    
    ModelRM.TrajHCor{1} = rmh;
    ModelRM.TrajHCor{3} = rmv;
    
    
end
obj.ModelRM = ModelRM; %temporary store of ModelRM

if measrf
    % RF RM
    disp('RF RM');
    rf0 = obj.rf.get;
    rf = rf0 + deltarf;
    
    obj.rf.set(rf);
    
    pause(waitPStime);
    
    t = obj.measuretrajectory;
    
    obj.rf.set(rf0);
    
    ModelRM.TrajHDPP = (t(1,:) - t0(1,:))./deltarf;
    ModelRM.TrajVDPP = (t(2,:) - t0(2,:))./deltarf;
else
    ModelRM.TrajHDPP = [];
    ModelRM.TrajVDPP = [];
end
obj.ModelRM = ModelRM; %temporary store of ModelRM


% V steerers RM
if v
    vs0 = obj.sv.get;
    for ik = 1:length(vs0) %
        if enabledcorV(ik) % only if eneabled corrector
            
           disp(['V steerer' num2str(ik) ''])
             vs = vs0;
            vs(ik) = vs(ik) + deltak;
            
            obj.sv.set(vs);
            
            pause(waitPStime);
            
            t = obj.measuretrajectory;
            
            obj.sv.set(vs0);
            
            rmh(:,ik) = (t(1,:) - t0(1,:))./deltak;
            rmv(:,ik) = (t(2,:) - t0(2,:))./deltak;
        else
           disp(['V steerer' num2str(ik) ' is disabled or not selected'])
             rmh(:,ik) = NaN(size(t0(1,:)));
            rmv(:,ik) = NaN(size(t0(2,:)));
            
        end
    end
    ModelRM.TrajVCor{1} = rmh;
    ModelRM.TrajVCor{3} = rmv;
end
obj.ModelRM = ModelRM; %temporary store of ModelRM


% septa
disp('septa RM')
if sp
    hs0 = [obj.s12.get obj.s3.get];
    for ik = 1:length(hs0)
        
        hs = hs0;
        hs(ik) = hs(ik) + kick_SP(ik);
        
        obj.s12.set(hs(1));
        obj.s3.set(hs(2));
        
        pause(waitPStime);
        
        t = obj.measuretrajectory;
        
        obj.s12.set(hs0(1));
        obj.s3.set(hs0(2));
        
        ModelRM.RMSP(ik,:) = (t(1,:) - t0(1,:))./kick_SP(ik);
        
    end
    
end
obj.ModelRM = ModelRM; %temporary store of ModelRM

% CV TL2 steerers RM
disp('CV RM')
if cv
    vs0 = [obj.cv8.get obj.cv9.get];
    for ik = 1:length(vs0)
        
        vs = vs0;
        vs(ik) = vs(ik) + kick_CV(ik);
        
        obj.cv8.set(vs(1));
        obj.cv9.set(vs(2));
        
        pause(waitPStime);
        
        t = obj.measuretrajectory;
        
        obj.cv8.set(vs0(1));
        obj.cv9.set(vs0(2));
        
        ModelRM.RMCV(ik,:) = (t(2,:) - t0(2,:))./kick_CV(ik);
        
    end
end
obj.ModelRM = ModelRM; %temporary store of ModelRM

if switch_on_gun_rips
    pause(0.25); % rips still moving.
    try
        rips.StopRamping()
        gun.Off(); % limit dose
    catch
        disp('stop rips by hand')
    end
    %Ke.PulseNumber = pn; % set back initial number of pulses for extraction
end

obj.savefile = sfi;
obj.ModelRM = ModelRM;

% % remove BPMs not selected
% nbpm = length(obj.indBPM);
% usebpm= false(size(1,nbpm));
% usebpm(indBPM)=true;
% 
% obj.ModelRM.TrajHCor = obj.ModelRM.TrajHDPP(:,usebpm);
% obj.ModelRM.TrajHDPP = obj.ModelRM.TrajHDPP(:,usebpm);
% obj.ModelRM.TrajVCor = obj.ModelRM.TrajVCor(:,usebpm);
% obj.ModelRM.RMCH = obj.ModelRM.RMCH(:,usebpm);
% obj.ModelRM.RMCV = obj.ModelRM.RMCV(:,usebpm);
% obj.ModelRM.RMSP = obj.ModelRM.RMSP(:,usebpm);

obj.setTurns(initturns);

end

