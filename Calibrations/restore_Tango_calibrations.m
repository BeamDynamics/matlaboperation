function restore_Tango_calibrations(saved_calibrations_folder)
% load a folder of saved calibtration.
% loop all files contained and reload the alibration settings in tango
%
% the folder of settings to be loaded is created by
% >> setcalibrations('st',false);
%
%see also: setcalibrations

% get list of saved calibrations
d =dir(saved_calibrations_folder);

dd = d(3:end); %remove . and ..

for f = 1:length(dd)
    
    try
        isOCT = false;
        if strfind(dd(f).name,'of1')
            isOCT=true;
        end
        file_data = load(fullfile(saved_calibrations_folder,dd(f).name));
        
        % change family calibration
        try
            famdev=tango.Device(file_data.cal_devname);
            famdev.set_property('Currents',arrayfun(@num2str,file_data.cal_tango(:,1),'un',0)');
            famdev.set_property('IntegratedFieldGradient',arrayfun(@num2str,file_data.cal_tango(:,2),'un',0)');
            famdev.Init();
            disp(['Set calibration curves for: ' file_data.cal_devname]);
            
        catch err
            disp(err);
            disp(['could not set calibration curves for: ' file_data.cal_devname]);
            
        end
        
        % change individual magnets scale factor (as red from tango, so
        % order is correct)
        for m = 1:length(file_data.magdevs)
            
            try
                
                dev = tango.Device(file_data.magdevs{m});
                dev.SetStrengthCorrectionCoeff(file_data.cal_scale_fact_tango(m));
                dev.Init();
                disp(['calibration scale factor set for: ' [file_data.magdevs{m}]]);
        
            catch err
                
                disp(err);
                disp(['could not set calibration scale factor for: ' file_data.magdevs{m}]);
                
            end
            
            try
                
                dev = tango.Device(file_data.magdevs{m});
                dev.SetStrengthCorrectionOffset(file_data.cal_offset_tango(m));
                dev.Init();
                        
                disp(['calibration offset set for: ' [file_data.magdevs{m}]]);

            catch err 
                
                disp(err);
                disp(['could not set calibration offset for: ' file_data.magdevs{m}]);
                
            end
            
        end
        
    catch
        disp(['Could not load data ' dd(f).name]);
    end
    
end


end