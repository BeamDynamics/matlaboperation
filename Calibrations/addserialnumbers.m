function r = addserialnumbers(r)
%addserialnumbers assign Serial Numbers to magnets
% 
% serial numbers are exported from:
% jlogbook.esrf.fr/ebs/view/ap_girders_components
% 
% The tables of components are stored in ./SerialNumbers/*****.xls
% 
% If a serial number is not found, then "Unknown" is stored as serial
% number
% 
% LAST UPDATE: 4/OCT/2019
% 
%see also: 

global APPHOME

%diary('callog.txt');

% rotate lattice to start in cell 01

filename ={ './SerialNumbers/Q.xls';...
     './SerialNumbers/DQ.xls';...
     './SerialNumbers/OF.xls';...
     './SerialNumbers/S.xls';...
     './SerialNumbers/SH1A.xls';...
     './SerialNumbers/SH2B.xls';...
     './SerialNumbers/SH3E.xls';...
     };
 
for ifile = 1:length(filename) 
    
    [~,f]=xlsread(fullfile(APPHOME,'Calibrations',[filename{ifile,1}]));
    names = [f(2:end,3)];
    cellnumber = [f(2:end,4)];
    serialnumber = [f(2:end,7)];
    
    for celn = 1:32
        % disp('cell #') ; disp(celn)
        
        % get indexes in this cell
        [initial,final] = sr.getcellstartend(r,celn);
        cel_inds= initial:final;
        
        % get indexes of magnets in this cell
        mag_in_cel_ind = strcmp(['Cell' num2str(celn,'%02d')],cellnumber);
        
        names_incell = names(mag_in_cel_ind);
        serialnumber_incell = serialnumber(mag_in_cel_ind);
        
        for in = 1:length(names_incell)

            
            atsearchname = names_incell{in};
            if strcmp(atsearchname,'SH')
                atsearchname = 'SH2B';
            elseif strcmp(atsearchname(1:2),'SH')
            end
            
            magind = find(atgetcells(r(cel_inds),'FamName',atsearchname),1);
            
            if isempty(magind)
               % disp('not found: '); magind 
                if celn == 3
                    atsearchname=[atsearchname(1), '[I]', atsearchname(3:4),'\w*'];
                elseif celn ==4
                    atsearchname=[atsearchname(1), '[J]', atsearchname(3:4),'\w*'];
                end
               % disp('looking for first: '); atsearchname
                magind = find(atgetcells(r(cel_inds),'FamName',atsearchname),1);
                
            end
                
            if ~isempty(magind)
              %  disp('component table name');      names_incell{in}
              %  disp('AT name')  ;     atsearchname
              %  disp('found fam name') ; r{cel_inds(magind)}.FamName
              %  disp('SN: ');       serialnumber_incell{in}
                r{cel_inds(magind)}.SerialNumber = serialnumber_incell{in};
            else
                disp('missing')
                disp(names_incell{in})
                disp(celn)
            end
        end
        
    end
    
    
    
end

% add UNKNOWN serial number to all element tha have a Devices field but do 
% not have a SerialNumber field

bpms = atgetcells(r,'Class','Monitor','Marker','Drift');
dev_mags = atgetcells(r,'Device') & ~bpms;
sn_mags = atgetcells(r,'SerialNumber');
r = atsetfieldvalues(r, dev_mags & ~sn_mags, 'SerialNumber','Unknown');
disp(['No serial numnber for : ' num2str(length(find(dev_mags & ~sn_mags))) ' magnets']);
cellfun(@(a)[a.Device],r(dev_mags & ~sn_mags),'un',0)

fd=fopen('device_serialnumber.csv','w');
cellfun(@(a)fprintf(fd,'%s,%s\n',a.Device,a.SerialNumber),r(atgetcells(r,'Device')&~bpms),'un',0);
fclose(fd);

end
