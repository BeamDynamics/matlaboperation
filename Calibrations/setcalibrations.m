function setcalibrations(r,varargin)
% SETCALIBRATION  set the calibration curves from the AT lattice
% in the tango devices and the scaling/offset of each independent magnet
%
% reads the calibration curves set in the lattice elements and the scaling
% factors, assigns them to the correct tango device
%
% a folder is created with the present calibrations in tango
% a figure is plotted with the comparison between the AT values and the
% Tango values.
%
% needs
% - access to TANGO control systesm "acs" or "ebs-simu....."
% - lattice with calibration curves (see addcalibrationcurves)
%
% OPTIONAL INPUT:
% ...,'set',true,...    replaces the tango DS calibration curves with those
%                       stored in the AT lattice structure
% ...,'filename','filename',...  adds a string to the folder name.  
%
% last updated:
% * 07 october 2019
% * 09 october 2019
%
%see also: addcalibrationcurves restore_Tango_calibrations

% parse inputs

p =inputParser;

addRequired(p,'r',@iscell);
addOptional(p,'set',false);
addOptional(p,'filename','');

parse(p,r,varargin{:});

setATcalibrationsToTango = p.Results.set; % flag to determine if the calibrations must be set or not
save_file_label = p.Results.filename; % flag to determine if the calibrations must be set or not

brho =  atenergy(r)/PhysConstant.speed_of_light_in_vacuum.value;

%% quadrupoles

% atindexed concerned by calibration curves (same order as above vector)
qf1cal_ind = find(atgetcells(r,'FamName','QF1[ABDE]\w*'))';
qf1injcal_ind = find(atgetcells(r,'FamName','QF1[IJ]\w*'))';
qf2cal_ind = find(atgetcells(r,'FamName','QF2\w*'))';
qd3cal_ind = find(atgetcells(r,'FamName','QD3[ABDE]\w*'))';
qd3injcal_ind = find(atgetcells(r,'FamName','QD3[IJ]\w*'))';
qd2cal_ind = find(atgetcells(r,'FamName','Q[FIJD][2]\w*'))';
sd1cal_ind = find(atgetcells(r,'FamName','S[IJD][1]\w*'))';
qf4aecal_ind = find(atgetcells(r,'FamName','Q[FD][4][AEIJ]\w*'))';
qf4bdcal_ind = find(atgetcells(r,'FamName','Q[FD][4][BDIJ]\w*'))';
sf2cal_ind = find(atgetcells(r,'FamName','S[IJF][2]\w*'))';
of1cal_ind = find(atgetcells(r,'FamName','O[IJF][1]\w*'))';
qd5cal_ind = find(atgetcells(r,'FamName','Q[FIJD][5]\w*'))';
qf6cal_ind = find(atgetcells(r,'FamName','QF6\w*'))';
dq1cal_ind = find(atgetcells(r,'FamName','DQ1\w*'))';
qf8cal_ind = find(atgetcells(r,'FamName','QF8\w*'))';
dq2cal_ind = find(atgetcells(r,'FamName','DQ2\w*'))';
sh2cal_ind = find(atgetcells(r,'FamName','SH2\w*'))';
sh13cal_ind = find(atgetcells(r,'FamName','SH[13]\w*'))';
shinj_ind = sh13cal_ind([1,end]);
sh13cal_ind([1,end])=[];

% loop devices and assign calibratio scale factor
strcur_devs ={...
    ...'srmag/m-qf1-inject/str-curr';...
    'srmag/m-qf1/str-curr';...
    'srmag/m-qd2/str-curr';...
    'srmag/m-qf2/str-curr';...
    'srmag/m-qd3/str-curr';...
    'srmag/m-qd3-inject/str-curr';...
    '';...'srmag/m-sd1/str-curr';... sextupole calibration is done by a device server model
    'srmag/m-qf4ae/str-curr';...
    'srmag/m-qf4bd/str-curr';...
    '';...'srmag/m-sf2/str-curr';... sextupole is done by a device server model
    'srmag/m-of1/str-curr';... octupole does not have Current/IntegratedFieldGradient
    'srmag/m-qd5/str-curr';...
    'srmag/m-qf6/str-curr';...
    'srmag/m-dq1q/str-curr';...
    'srmag/m-dq1d/str-curr';...
    'srmag/m-qf8/str-curr';...
    'srmag/m-dq2q/str-curr';...
    'srmag/m-dq2d/str-curr';...
    'srmag/m-hst-sh/str-curr';...
    'srmag/m-vst-sh/str-curr';...
    'srmag/m-sqp-sh/str-curr';...
    'srmag/m-hst-ssh/str-curr';...
    'srmag/m-vst-ssh/str-curr';...
    'srmag/m-sqp-ssh/str-curr';...
    'srmag/m-sext-inj/str-curr';... % injection cell sextupole in corrector
    };

at_inds ={...
    ...qf1injcal_ind;...
    qf1cal_ind;...
    qd2cal_ind;...
    qf2cal_ind;...
    qd3cal_ind;...
    qd3injcal_ind;...
    sd1cal_ind;...
    qf4aecal_ind;...
    qf4bdcal_ind;...
    sf2cal_ind;...
    of1cal_ind;...
    qd5cal_ind;...
    qf6cal_ind;...
    dq1cal_ind;...
    dq1cal_ind;...
    qf8cal_ind;...
    dq2cal_ind;...
    dq2cal_ind;...
    sh2cal_ind;...
    sh2cal_ind;...
    sh2cal_ind;...
    sh13cal_ind;...
    sh13cal_ind;...
    sh13cal_ind;...
    shinj_ind;...
    };


scale_cross_talk=[....1.0;......qf1injcal_ind;...
     1.0;... qf1cal_ind;...
     1.0-0.088*1e-2;... qd2cal_ind;...
     1.0;... qf2cal_ind;...
     1.0-0.141*1e-2;... qd3cal_ind;...
     1.0;... qd3injcal_ind;...
     1.0;... sd1cal_ind;...
     1.0-0.03*1e-2;... qf4aecal_ind;...
     1.0-0.03*1e-2;... qf4bdcal_ind;...
     1.0;... sf2cal_ind;...
     1.0;... of1cal_ind;...
     1.0-0.056*1e-2;... qd5cal_ind;...
     1.0-0.082*1e-2;... qf6cal_ind;...
     1.0+(0.04 + 0.042)*1e-2;... dq1cal_ind;...
     1.0;... dq1cal_ind steerer;...
     1.0-0.061*1e-2;... qf8cal_ind;...
     1.0;... dq2cal_ind;...
     1.0;... dq2cal_ind steerer;...
     1.0;... sh2cal_ind;...
     1.0;... sh2cal_ind;...
     1.0;... sh2cal_ind;...
     1.0;... sh13cal_ind;...
     1.0;... sh13cal_ind;...
     1.0;... sh13cal_ind;...
     1.0;... shinj_ind;...
   ];
% 
% 
% strcur_devs ={...
%      'srmag/m-of1/str-curr';... octupole does not have Current/IntegratedFieldGradient
%     };
% 
% at_inds ={...
%     of1cal_ind;...
%     };

% read and plot existing calibration curves and AT stored curves:
figure('name','calibrations',...
    'units','normalized',...
    'Position',[0.05 0.05 0.9 0.8]);
ns = ceil(sqrt(length(strcur_devs)+1)); %  6 additional for the correctors calibration curves (SH13 and SH2)
subplot(ns,ns,1);

savefold = [save_file_label 'Calib_' datestr(now,'YYYY-mm-DD-HH-MM-SS')];
mkdir(savefold); % directory to store tango data, to be able to revert changes.

for ical = 1:length(strcur_devs)
    
    isSHcor =false;
    
    if strfind(strcur_devs{ical},'sh')
        isSHcor = true;
        if strfind(strcur_devs{ical},'hst')
            cortype = 'H';
        end
        if strfind(strcur_devs{ical},'vst')
            cortype = 'V';
        end
        if strfind(strcur_devs{ical},'sqp')
            cortype = 'S';
        end
        if strfind(strcur_devs{ical},'sext')
            cortype = 'X';
        end
        
    end
    
    isOCT = false;
    if strfind(strcur_devs{ical},'of1')
        isOCT=true;
    end
    
    isDQ = false;
    if strfind(strcur_devs{ical},'dq1q') 
        isDQ=true;
    end
    if strfind(strcur_devs{ical},'dq2q') 
        isDQ=true;
    end
    
    
    isDQSteerer = false;
    if strfind(strcur_devs{ical},'dq1d') 
        isDQSteerer=true;
    end
    if strfind(strcur_devs{ical},'dq2d') 
        isDQSteerer=true;
    end
    
    % present tango calibration
    if ~isempty(strcur_devs{ical})
        dev = tango.Device(strcur_devs{ical}); % load calibration device 
        
        % if ~isOCT
            
            cal_tango(:,1) = cellfun(@str2double,dev.get_property('Currents'))';
            cal_tango(:,2) = cellfun(@str2double,dev.get_property('IntegratedFieldGradient'));
            
%         else
%             disp('Octupole has only conversion coefficent 1/m3 - Ampere')
%             
%             convcoef = cellfun(@str2double,dev.get_property('ConversionCoeff'))';
%             
%             cal_tango(:,1) = 0:5:110;
%             
%             cal_tango(:,2) =1/convcoef*cal_tango(:,1);
%             
%         end
    else
        dev=[];
        cal_tango=NaN(2,2);
    end
    
    % calibrations stored in the AT lattice
    try
        if ~(isSHcor|isDQSteerer)
            cal_at = r{at_inds{ical}(1)}.Calibration;
        
        elseif isSHcor
            % SH have three/four calibration curves each
            switch cortype
                case 'H'
                    cal_at = r{at_inds{ical}(1)}.CalibrationH_pseudoA_Tm;
                case 'V'
                    cal_at = r{at_inds{ical}(1)}.CalibrationV_pseudoA_Tm;
                case 'S'
                    cal_at = r{at_inds{ical}(1)}.CalibrationS_pseudoA_T;
                case 'X'
                    cal_at = r{at_inds{ical}(1)}.CalibrationX_pseudoA_ToverM;
            end
            cal_matrix = r{at_inds{ical}(1)}.CalibrationMatrix_pseudoA_channelA;
       
        elseif isDQSteerer
            cortype = 'H';
                cal_at = r{at_inds{ical}(1)}.CalibrationH_pseudoA_Tm;
                cal_matrix = r{at_inds{ical}(1)}.CalibrationMatrix_pseudoA_channelA;
        end
        
    catch
        if ~(isSHcor|isDQSteerer)
            disp(['no calibration in AT for: ' r{at_inds{ical}(1)}.FamName]);
        else
            % SH have three calibration curves each
            disp(['no calibration in AT for: ' r{at_inds{ical}(1)}.FamName ' ' cortype]);
        end
        cal_at = [];
        
    end
    
    % apply scale factor due to cross talk to main excitation curve
    if ~isempty(cal_at)
        cal_at(:,2) = cal_at(:,2)*scale_cross_talk(ical);
    end
    
    % get calibration coefficents from tango
    cal_scale_fact_tango = NaN(size(at_inds{ical}));
    cal_offset_tango = NaN(size(at_inds{ical}));
    
    for ind = 1: length(at_inds{ical})
        mag = r{at_inds{ical}(ind)};
        try
            if ~(isSHcor|isDQSteerer)
                devmag = tango.Device(mag.Device); %open device
                cal_scale_fact_tango(ind) = devmag.StrengthCorrectionCoeff.read; % read coefficent
                cal_offset_tango(ind) = devmag.StrengthCorrectionOffset.read; % read offset
                delete(devmag); % close connection to device
            else
                cal_scale_fact_tango(ind) = 1;
                cal_offset_tango(ind) = 0;
            end
        catch err
            disp(err);
            disp(['Could not get calibtration  coefficent for: ' mag.FamName]);
        end
    end
    
    if ~(isSHcor|isDQSteerer)
        cal_scale_fact_at = atgetfieldvalues(r,at_inds{ical},'CalibrationScaleFactor');
        cal_offset_at = atgetfieldvalues(r,at_inds{ical},'CalibrationOffset');
    else
        cal_scale_fact_at = ones(size(at_inds{ical}));
        cal_offset_at = zeros(size(at_inds{ical}));
        
    end
    
    subplot(ns,ns,ical);
    
    % main reference curve
    % if ~isOCT
        p1= plot(cal_tango(:,1),cal_tango(:,2),'LineWidth',2);
    % else
    %     % octupoles calibrations is in 1/m3 / A and not in T/m2 /A
    %     p1= plot(cal_tango(:,1),cal_tango(:,2)*(-brho),'LineWidth',2);
    % end
    hold on;
    
    
    isSEXT = false;
    if ~isempty(cal_at)
        
        if size(cal_at,2)==2
            cur = cal_at(:,1);
            stren = cal_at(:,2);
        else % sextupoles
            isSEXT = true;
            cur = cal_at(cal_at(:,5)~=0,1);
            stren = cal_at(cal_at(:,5)~=0,9); %T/mm2
            stren = stren * 1e6 /13^2 * r{at_inds{ical}(1)}.Length; % T/m2 *r0^2 *L
        end
        
        % main reference curve
        p2= plot(cur,abs(stren),'LineWidth',2);
        
        % curves for each magnet
        for csat = 1:length( cal_scale_fact_at )
            plot(cur,abs(stren)*cal_scale_fact_at(csat)+cal_offset_at(csat),':','Color',[0.2 0 0],'LineWidth',0.1);
            plot(cal_tango(:,1),cal_tango(:,2)*cal_scale_fact_tango(csat)+cal_offset_tango(csat),':','Color',[0 0 0.2],'LineWidth',0.1);
        end
    else
        p2=[];
    end
    
    
    K0 = max(abs(r{at_inds{ical}(1)}.PolynomB*r{at_inds{ical}(1)}.Length*brho)); % quad T, sext T/m , oct T/m2
    disp(r{at_inds{ical}(1)}.FamName);
    disp(K0)
    xx = [min(cal_tango(:,1)) max(cal_tango(:,1))];
    if isnan(xx), xx = [min(cal_at(:,1)) max(cal_at(:,1))]; end
    l1= line(xx,[K0 K0],'Color',[ 0.1 0.1 0.1]);
    
    if ~(isSHcor|isDQSteerer)
        title(r{at_inds{ical}(1)}.FamName);
    %    legend([p1,p2,l1],'Tango','Reference','Nominal Field','Location','SouthEast');
    else
        title([ r{at_inds{ical}(1)}.FamName ' ' cortype]);
    %    legend([p1,p2,l1],'Tango','Reference','Nominal Field','Location','NorthWest');
    end
    xlabel('Current [A]');
    ylabel('Integrated Field [T]');
    
    if isSEXT
        ylabel('Integrated Field [T/m]');
    end
    
    if isOCT
        ylabel('Integrated Field [T/m^2]');
    end
    
    if isSHcor
        switch cortype
            case {'H','V'}
                ylabel('Integrated Field [Tm]');
                
            case 'S'
                ylabel('Integrated Field [T]');
        end
    end
    
    if isDQSteerer
       
        ylabel('Integrated Field [Tm]');
   
    end
    
    grid on;
    
    cal_devname = strcur_devs{ical};
    magdevs = atgetfieldvalues(r,at_inds{ical},'Device');
    save(fullfile(savefold,...
                  [strrep(strcur_devs{ical},'/','_') '.mat']),...
        'cal_tango',...
        'cal_at',...
        'cal_devname','magdevs',...
        'cal_offset_tango',...
        'cal_offset_at',...
        'cal_scale_fact_tango',...
        'cal_scale_fact_at');
    
    %% set calibration in str-curr devices and scaling of each single magnet.
    if setATcalibrationsToTango & ~isempty(dev)
        
        if ~isempty(cal_at)
            
            warning(['CURRENTS will change. To restore a given SR state,'...
                ' reload a jebsmag file. (This will load the saved currents'...
                ' and determine new strengths'...
                ' reading according to the new calibration curves)']);
            
            % str-curr devices, change calibration curve property, Init device.
            if ~(isSHcor) %& ~isOCT
                
                % quadrupoles 
                dev.set_property('Currents',arrayfun(@num2str,cal_at(end:-1:1,1),'un',0)');
                dev.set_property('IntegratedFieldGradient',arrayfun(@num2str,cal_at(end:-1:1,2),'un',0)');
            
%             elseif isOCT
%                 
%                 % octupoles have at the moment just a scaling factor 1/m3
%                 % to Current
%                 cur = interp1(cal_at(:,2),cal_at(:,1),K0);
%                 coef = cur / (-K0/brho);
%                 dev.set_property('ConversionCoeff',coef);
                
            elseif isSHcor
                
                % correctors
                dev.set_property('Currents',arrayfun(@num2str,cal_at(:,1),'un',0)');
                dev.set_property('IntegratedFieldGradient',arrayfun(@num2str,abs(cal_at(:,2)),'un',0)');
                % isdefocusing = false;
                % if sign(cal_at(end,2)) == -1
                %    isdefocussing=true;
                %    dev.set_property('DefocusingMagnet','true');
                % end
                
            end
            dev.Init();
            
            % loop single magnets, change scaling and Init.
            for ind = 1: length(at_inds{ical})
                
                mag = r{at_inds{ical}(ind)};
                
                try
                    if ~isSHcor
                        devmag = tango.Device(mag.Device); %open device
                        devmag.SetStrengthCorrectionCoeff(mag.CalibrationScaleFactor); % set new scaling factor
                        devmag.SetStrengthCorrectionOffset(mag.CalibrationOffset); % set new offset
                        if isDQ
                            devmodmag = tango.Device([mag.Device(1:7) 'odel' mag.Device(8:end)]); %open device
                            devmodmag.set_property('SerialNumber',mag.SerialNumber);
                            devmodmag.set_property('DipolePseudoCurrOffset',mag.Current);
                        end
                        devmag.Init(); % init device with new scaling
                        % (warning. this changes the currents for a given
                        % strenght. to change the strengths,
                        % reload a jebsmag file (currents))
                        
                        delete(devmag); % close connection to device
                    else
                        disp('SH correctors Vert , skew, sext: set property DefocusingMagnet = true')
                        disp('SET CLASS PROPERTY for EBSCorrector:')
                        
                        disp('SH correctors have no scaling and offset.')
                        disp('SET CLASS PROPERTY for EBSCorrector:')
                        
                        if strfind(strcur_devs{ical},'sext-ssh')
                            disp('Inj_Sextu_Corr2Chan')
                            disp(inv(cal_matrix));
                            disp('Inj_Sextu_Chan2Corr')
                            disp(cal_matrix);
                        elseif strfind(strcur_devs{ical},'ssh')
                            disp('SA13_Corr2Chan')
                            disp(inv(cal_matrix));
                            disp('SA13_Chan2Corr')
                            disp(cal_matrix);
                        else
                            disp('SA13_Corr2Chan')
                            disp(inv(cal_matrix));
                            disp('SA13_Chan2Corr')
                            disp(cal_matrix);
                        end
                        
                    end
                catch err
                    
                    disp(err);
                    disp(['Could not set calibtration  coefficent for: ' mag.FamName]);
                    disp(['Could not set calibtration  offset for: ' mag.FamName]);
                    
                end
            end
        else
            
            disp(['Calibration missing for: '  r{at_inds{ical}(1)}.FamName ]);
            
        end
    else
        if ~isSHcor
            disp(['Tango calibration properties have NOT changed for: '  r{at_inds{ical}(1)}.FamName ]);
            disp(scale_cross_talk(ical))
        else
            disp(['Tango calibration properties have NOT changed for: '  r{at_inds{ical}(1)}.FamName ' ' cortype]);
        end
    end
    
    clear cal_tango cal_at;
    
end


subplot(ns,ns,ical+1);
p1 = plot(1,1,'LineWidth',2); hold on;
p2 = plot(1,1,'LineWidth',2);
l1 = line([1 1],[1 1],'Color',[0.1 0.1 0.1]);
set(gca,'visible','off');
l=legend('Tango','Reference','Nominal Field','Location','SouthEast');
l.FontSize = 16; 

disp('----------------------------------------------------------------');
disp('Stop Start all servers that make use of these calibration curves')
disp('----------------------------------------------------------------');
