README


Files in SerialNumbers are exports from:

jlogbook.esrf.fr/ebs/view/ap_girders_components

Curves from Confluence page of IDM

to store present calibrations in Tango and in AT: 
>> r = sr.model().ring;
>> rc =addcalibrationcurves(r)
>> setcalibrations(rc)


to load a new set of calibrations from IDM to AT (ordered) to Tango:
1) Download new files from IDM confluence page https://confluence.esrf.fr/display/IDMWS/Calibration to ./Curves
2) in matlab: 
>> r = sr.model().ring;
>> rc =addcalibrationcurves(r)
>> setcalibrations(rc,'filename','BeforeChange')
>> setcalibrations(rc,'set',true)
>> setcalibrations(rc,'filename','AfterChange')

to restore calibrations:
in matlab:
>> restore_Tango_calibrations('BeforeChange....fill with full folder name....')

