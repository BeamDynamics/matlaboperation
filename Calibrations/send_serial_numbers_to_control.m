function send_serial_numbers_to_control(filename,magtype)
%
%filename = matlab/optics/theroy/device_serialnumber.csv
% magtype = {'m-q','m-s','m-o',...}
%
% example: send all quadrupoles serial numbers
% send_serial_numbers_to_control('device_serialnumber_2020.csv',{'m-q'})

A = textscan(fopen(filename,'r'),'%s %s','Delimiter',',');

for mm = 1:length(magtype)
    
    mask = ~cellfun(@(a)isempty(a),strfind(A{1},magtype{mm}),'un',1);
    
    devs = A{1}(mask);
    sn = A{2}(mask);
    
    for  dd = 1:length(devs)
        disp("Setting " + devs{dd} + " : " +sn{dd})
        
        d = tango.Device(devs{dd});
        d.set_property('SerialNumber',sn{dd})
        d.Init();
        
    end
    
end
