function r = addcalibrationcurves(r)
% add to the AT lattice all the known calibration curves and coefficients
% based on serial number of the magnet
%
% The calibration curves are obtained from
% confluence.esrf.fr/display/IDMWS/Calibrations
%
% These files are downloaded (9/Oct/2019) in the foldere ./Curves/***.csv
% or ./Curves/***.dat for sextupoles
% 
% The AT structure is populated with two additional fields for each magnet
% that has a serial number related to a given calibration curve.
%   
% If serial numbers are missing in the input lattice they are added using
% addserialnumbers.m
% 
% EXAMPLE of element with 2 additional fields set by this function:
% 
%                      FamName: 'QD2J'
%                   PassMethod: 'StrMPoleSymplectic4RadPass'
%                       Length: 0.2277
%                        Class: 'Quadrupole'
%                     PolynomB: [0 -1.9277]
%                  NumIntSteps: 20
%                     PolynomA: [0 0]
%                     MaxOrder: 1
%           FringeQuadEntrance: 0
%               FringeQuadExit: 0
%                   EApertures: [0.0250 0.0100]
%                       Energy: 6.0000e+09
%                       Device: 'srmag/m-qd2/c04-a'
%                 SerialNumber: 'QD2-207'    ( added by addserialnumbers.m) 
%       CalibrationScaleFactor: 1.0035        <<< ---- ADDED by this function
%            CalibrationOffset: 0.0           <<< ---- ADDED by this function
%                  Calibration: [22×2 double] <<< ---- ADDED by this function
%            Calibration_units: {'A' 'T'}     <<< ---- ADDED by this function
% 
% for SH correctors the fields 
% CalibrationH_pseudoA_Tm
% CalibrationV_pseudoA_Tm
% CalibrationS_pseudoA_Tm
% CalibrationX_pseudoA_Tm (where relevant)
%
% 
% if pinholes are present, thery are first removed, then calibrations are set and pinholes are added back 
% 
%see also: addserialnumber sr.model.addpinholes

global APPHOME

brho = atenergy(r)./PhysConstant.speed_of_light_in_vacuum.value;

% remove pinholes and reduce lattice (added back at end of file)
l0= length(r);
r = atreduce(r,~atgetcells(r,'FamName','D[LQ]\w*','PINHOLE\w*'));
l1 = length(r);
pinholes_removed = false;
if l0-l1==10
    pinholes_removed = true; % to add them later
    disp('pinholes have been removed');
elseif l1<l0
    disp('WARNING! not only pinholes removed!');
end
    
% add serial number
ind_serial_num = atgetfieldvalues(r,atgetcells(r,'SerialNumber'),'SerialNumber');

if isempty(ind_serial_num)
    disp('adding serial numbers')
    r = addserialnumbers(r);
else
    disp('serial numbers are present');
end



% name of file, index of concerned magnets in AT
filename_ind = {...
    './Curves/QF1',find(atgetcells(r,'FamName','QF1[ABDE]\w*','QD3[IJ]\w*'))';...   1
    './Curves/QD3',find(atgetcells(r,'FamName','QD3[ABDE]\w*'))';...                2
    './Curves/QD2-QF4-QD5',find(atgetcells(r,'FamName','Q[FIJD][245]\w*'))';...     3
    './Curves/QF4E',find(atgetcells(r,'FamName','QF4[E]\w*'))';...                    4
    './Curves/QF6',find(atgetcells(r,'FamName','QF6\w*'))';...                      5    
    './Curves/QF8',find(atgetcells(r,'FamName','QF8\w*'))';...                      6
    './Curves/SD1',find(atgetcells(r,'FamName','S[DIJ]1\w*'))';...                  7
    './Curves/SF2',find(atgetcells(r,'FamName','S[FIJ]2\w*'))';...                  8
    './Curves/DQ1',find(atgetcells(r,'FamName','DQ1\w*'))';...                      9
    './Curves/DQ2',find(atgetcells(r,'FamName','DQ2\w*'))';...                      10 
    './Curves/QF1IJ',find(atgetcells(r,'FamName','QF1[IJ]\w*'))';...                11
    './Curves/O',find(atgetcells(r,'FamName','O[FIJ]\w*'))';...                     12  
    };

    function sn = getsn(str)
        sn='-';
        if ~isempty(str) & ~strcmp(str,'Unknown')
            str = strrep(str,'_','-'); % typo errors
            parts = strsplit(str,'-'); % 
            sn = parts{2};
        end
    end

for ifn = 1:length(filename_ind)
    
    switch ifn
        case {1,2,3,4,5,6} % quadrupoles
            strfile = fullfile(APPHOME,'Calibrations',[filename_ind{ifn,1} '_strength.csv']);
            parfile = fullfile(APPHOME,'Calibrations',[filename_ind{ifn,1} '_params.csv']);
            
            calib = load(strfile);
            parameters = textscan(fopen(parfile,'r'),'%s%s','Delimiter',',');
            offset = zeros(size(parameters{1}));
            fact_params = cellfun(@(a)str2double(a),parameters{2},'un',1);
        
        case {7,8} % sextupoles
            
            strfile = fullfile(APPHOME,'Calibrations',[filename_ind{ifn,1} '_meas_strengths.dat']);
            parfile = fullfile(APPHOME,'Calibrations',[filename_ind{ifn,1} '_params.dat']);
            
            calib = load(strfile);
            parameters = textscan(fopen(parfile,'r'),'%s%s%s%s','HeaderLines',2);
            offset = zeros(size(parameters{1}));
            fact_params = cellfun(@(a)str2double(a),parameters{2},'un',1);
       
        case {9,10} % DQ
            
%             strfile = fullfile(APPHOME,'Calibrations',[filename_ind{ifn,1} '_strength.csv']);
%             parfile = fullfile(APPHOME,'Calibrations',[filename_ind{ifn,1} '_currents.csv']);
%             
%             calib = load(strfile);
%             calib(:,2) = -calib(:,2)./brho; % get 1/m for quadrupole component
%             
%             % compute calibration offset
%             parameters = textscan(fopen(parfile,'r'),'%s%f','Delimiter',',');
%             K0L = cellfun(@(a)a.PolynomB(2)*a.Length,r(filename_ind{ifn,2}),'un',1);
%             
%             offset = interp1(calib(:,1),calib(:,2),parameters{2})-K0L(1); %zeros(size(factor));
%             currents = parameters{2};
%             parameters{2} = ones(size(parameters{1}));
%             fact_params = parameters{2};
%             
%             calib(:,2) = -calib(:,2).*brho; % revert to sign and units in Gael File
% restore also line 217 if uncommenting above
            
            % in the DS the offset is applied to the Strengths units 1/m
            % offset = offset.*brho; 
            strfile = fullfile(APPHOME,'Calibrations',[filename_ind{ifn,1} '_strength.csv']);
            parfile = fullfile(APPHOME,'Calibrations',[filename_ind{ifn,1} '_params.csv']);
            
            calib = load(strfile);
            parameters = textscan(fopen(parfile,'r'),'%s%s%s','Delimiter',',');
            offset = zeros(size(parameters{1}));
            fact_params = cellfun(@(a)str2double(a),parameters{3},'un',1);
            currents = cellfun(@(a)str2double(a),parameters{2},'un',1);
        
        case {11} % quadrupoles injection
            strfile = fullfile(APPHOME,'Calibrations',[filename_ind{ifn,1} '_strength.csv']);
            
            calib = load(strfile);
            parameters{1} = [''; ''];
            offset = [0; 0];
            fact_params = [1; 1];
            
        
        case {12} % octupoles
            strfile = fullfile(APPHOME,'Calibrations',[filename_ind{ifn,1} '_strength.csv']);
            
            calib = load(strfile);
            offset = zeros(64,1);
            fact_params = ones(64,1);
        
    end
    
    if exist(strfile,'file') % quadrupole
    else 
        error([strfile ' does not exist.']);
    end
    
    maginds = filename_ind{ifn,2};
    disp(['N mag AT: ' num2str(length(maginds))]);
    disp(['N mag file: ' num2str(length(parameters{1}))]);
            
    switch ifn
        case  {11,12} % magnets without serial number
            
            ordered_sn_params = 1:length(fact_params);
            
        otherwise % generic magnet with serial number
            
            sn = atgetfieldvalues(r,maginds,'SerialNumber');
            
            has_sn = ~cellfun(@(a)strcmp(a,'Unknown'),sn);
           
            % sn = sn(has_sn);
            if find(~has_sn)
                disp('Magnets without Serial Number: ')
                disp(atgetfieldvalues(r,maginds(~has_sn),'FamName'))
                serial1scale = parameters{1}{find(fact_params==1,1,'first')}; % get a scale factor 1 serial number
                r = atsetfieldvalues(r,maginds(~has_sn),'SerialNumber',serial1scale);
                sn = atgetfieldvalues(r,maginds,'SerialNumber');
            
            end
            % maginds = maginds(has_sn);
    
            % get magnet order in params.csv
            sn_params = cellfun(@(a)getsn(a),parameters{1},'un',0);
            
            sn_at = cellfun(@(a)getsn(a),sn,'un',0);
            
            [~,ordered_sn_params] = ismember(sn_at,sn_params);
            
            ordered_sn_params(ordered_sn_params==0)=[]; % remove not found serial numbes
    end
    
    
    % assign scaling to elements in the AT lattice
    r = atsetfieldvalues(r,maginds,'CalibrationScaleFactor',fact_params(ordered_sn_params));
    r = atsetfieldvalues(r,maginds,'CalibrationOffset',offset(ordered_sn_params));
    if ifn ==9 ||ifn ==10 % DQ
        r = atsetfieldvalues(r,maginds,'Current',currents(ordered_sn_params));
    end
    
    % set calibration curve A-T
    for imag = 1:length(maginds)
        try
            switch ifn
                case {7,8} % sextupoles
                    r{maginds(imag)}.Calibration=calib;
                    r{maginds(imag)}.Calibration_units={'A' 'A' 'A' 'A' 'A' 'Tm' 'Tm' 'T' 'T/m'};
                case {9,10} % DQ
                    r{maginds(imag)}.Calibration=[calib(end:-1:1,:);[0,0]];
                    r{maginds(imag)}.Calibration_units={'A' 'T'};
                case {11} % QF1IJ and Octupoles currents ordered from low to high
                    r{maginds(imag)}.Calibration=[calib(end:-1:1,:);[0,0]];
                    r{maginds(imag)}.Calibration_units={'A' 'T'};
                case {12} % QF1IJ and Octupoles currents ordered from low to high
                    r{maginds(imag)}.Calibration=[calib(end:-1:1,:);[0,0]];
                    r{maginds(imag)}.Calibration_units={'A' 'T/m2'};
                otherwise
                    r{maginds(imag)}.Calibration=[calib(end:-1:1,:);[0,0]];
                    r{maginds(imag)}.Calibration_units={'A' 'T'};           
            end
        catch err
            disp(err)
        end
    end
end


% SH correctors

maginds = find(atgetcells(r,'FamName','SH[123]\w*'))';
injsh = maginds([1,end]);
maginds([1,end]) =[];
        
filename = './Curves/SH';
calibH = load(fullfile(APPHOME,'Calibrations',[filename '_h.csv'])); % Hor field!, ver corrector
calibV = load(fullfile(APPHOME,'Calibrations',[filename '_v.csv'])); % Ver field!, hor corrector
calibS = load(fullfile(APPHOME,'Calibrations',[filename '_s.csv']));
calibMATRIX = load(fullfile(APPHOME,'Calibrations',[filename '_matrix.csv']));

% SH2
filename = './Curves/SH2';
calibV2 = load(fullfile(APPHOME,'Calibrations',[filename '_v.csv'])); % Ver field!, hor corrector
calibS2 = load(fullfile(APPHOME,'Calibrations',[filename '_s.csv']));

% convert units from Tmm to Tm, remove thrid column compute with brho = 20
calibH(:,2) = calibH(:,2).*1e-3 ; calibH(:,3)=[];
calibV(:,2) = calibV(:,2).*1e-3 ; calibV(:,3)=[];
calibS(:,2) = calibS(:,2); calibS(:,3)=[];
calibV2(:,2) = calibV2(:,2).*1e-3; calibV2(:,3)=[];
calibS2(:,2) = calibS2(:,2); calibS2(:,3)=[];

% there is no serial number distinction, all identical. SH2B and injection
% SH will be modified later
for im = 1:length(maginds)
    if ~strcmp(r{maginds(im)}.FamName(1:3),'SH2')
        
        r{maginds(im)}.CalibrationH_pseudoA_Tm = calibH;% [-calibV(end:-1:2,:);calibV];
        r{maginds(im)}.CalibrationV_pseudoA_Tm = calibV;%[-calibH(end:-1:2,:);calibH];
        r{maginds(im)}.CalibrationS_pseudoA_T = calibS;%[-calibS(end:-1:2,:);calibS];
        r{maginds(im)}.CalibrationMatrix_pseudoA_channelA = calibMATRIX;
        
    elseif strcmp(r{maginds(im)}.FamName(1:3),'SH2')
        
        r{maginds(im)}.CalibrationH_pseudoA_Tm = calibH;% [-calibV(end:-1:2,:);calibV];
        r{maginds(im)}.CalibrationV_pseudoA_Tm = calibV2;%[-calibH(end:-1:2,:);calibH];
        r{maginds(im)}.CalibrationS_pseudoA_T = calibS2;%[-calibS(end:-1:2,:);calibS];
        r{maginds(im)}.CalibrationMatrix_pseudoA_channelA = calibMATRIX;
        
    end
end

% injection 
filename = './Curves/SH-INJ';
calibH = load(fullfile(APPHOME,'Calibrations',[filename '_h.csv'])); % Hor field!, ver corrector
calibV = load(fullfile(APPHOME,'Calibrations',[filename '_v.csv'])); % Ver field!, hor corrector
calibS = load(fullfile(APPHOME,'Calibrations',[filename '_s.csv']));
calibM = load(fullfile(APPHOME,'Calibrations',[filename '_m.csv']));
calibMATRIX = load(fullfile(APPHOME,'Calibrations',[filename '_matrix.csv']));

% convert units from Tmm to Tm and T/m to T/m, remove thrid column compute with brho = 20
calibH(:,2) = calibH(:,2).*1e-3 ; calibH(:,3)=[];
calibV(:,2) = calibV(:,2).*1e-3 ; calibV(:,3)=[];
calibS(:,2) = calibS(:,2); calibS(:,3)=[];
calibM(:,2) = calibM(:,2); calibM(:,3)=[];

for im = 1:length(injsh)
        
        r{injsh(im)}.CalibrationH_pseudoA_Tm = calibH;% [-calibV(end:-1:2,:);calibV];
        r{injsh(im)}.CalibrationV_pseudoA_Tm = calibV;%[-calibH(end:-1:2,:);calibH];
        r{injsh(im)}.CalibrationS_pseudoA_T = calibS;%[-calibS(end:-1:2,:);calibS];
        r{injsh(im)}.CalibrationX_pseudoA_ToverM = [ [0,0]; calibM];%[-calibS(end:-1:2,:);calibS];
        r{injsh(im)}.CalibrationMatrix_pseudoA_channelA = calibMATRIX;
    
end



% DQ1 steerer
maginds = find(atgetcells(r,'FamName','DQ1\w*'))';

filename = './Curves/DQ1';
calibH = load(fullfile(APPHOME,'Calibrations',[filename '_steerer_strength.csv'])); % Hor field!, ver corrector
calibMATRIX = load(fullfile(APPHOME,'Calibrations',[filename '_corr_matrix.csv']));

% convert units from Tmm to Tm and T/m to T/m, remove thrid column compute with brho = 20
calibH(:,2) = calibH(:,2).*1e-3 ; 

for im = 1:length(injsh)
        
        r{maginds(im)}.CalibrationH_pseudoA_Tm = calibH;% [-calibV(end:-1:2,:);calibV];
        r{maginds(im)}.CalibrationMatrix_pseudoA_channelA = calibMATRIX;
    
end




% DQ2 steerer
maginds = find(atgetcells(r,'FamName','DQ2\w*'))';

filename = './Curves/DQ2';
calibH = load(fullfile(APPHOME,'Calibrations',[filename '_steerer_strength.csv'])); % Hor field!, ver corrector
calibMATRIX = load(fullfile(APPHOME,'Calibrations',[filename '_corr_matrix.csv']));

% convert units from Tmm to Tm and T/m to T/m, remove thrid column compute with brho = 20
calibH(:,2) = calibH(:,2).*1e-3 ; 

for im = 1:length(injsh)
        
        r{maginds(im)}.CalibrationH_pseudoA_Tm = calibH;% [-calibV(end:-1:2,:);calibV];
        r{maginds(im)}.CalibrationMatrix_pseudoA_channelA = calibMATRIX;
    
end


% add pinholes again
if pinholes_removed
    r = sr.model.addpinholes(r);
    disp('pinholes have been restored');
end
end