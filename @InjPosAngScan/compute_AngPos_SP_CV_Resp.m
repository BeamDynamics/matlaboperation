function [SP_A_Ang_Pos,CV_A_Ang_Pos]=compute_AngPos_SP_CV_Resp()
% comptue Septa CV ang pos response

%lat = sr.model();
%indbpm = lat.get(0,'bpm');
%s=findspos(lat.ring,indbpm([1,2]));

%L = s(2)-s(1); % [m] distance between first 2 BPMs
L = 3.6759;

RM = load('/machfs/MDT/2019/2019_12_06/MeasuredSeptaCVRM_3acquisitions.mat');

% measured septa response at first 2 BPMS
SP_A = RM.MeasuredSPRM.RMSP(:,[1,2]); % m/A

% measured CV response at first 2 BPMS
CV_A = RM.MeasuredCVRM.RMCV(:,[1,2]); % m/A

X1 = SP_A(:,1)';
X2 = SP_A(:,2)';
SP_A_Ang_Pos = [X1;... % position at first BPM [m/A]
                (X2 - X1)/L]; % angle at first BPM [rad/A]

Y1 = CV_A(:,1)';
Y2 = CV_A(:,2)';
CV_A_Ang_Pos = [Y1;... % position at first BPM [m/A]
                (Y2 - Y1)/L]; % angle at first BPM [rad/A]

end


