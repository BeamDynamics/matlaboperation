classdef InjPosAngScan < RingControl %& GridScan
    %InjPosAngScan
    %
    % vary Hor. and Ver. position angle in a grid of points to seek for optimum.
    %
    %
    %
    %see also:
    
    properties
        
        initial_pos_ang % pos ang as measured at initialization
        present_pos_ang % pos ang as form 
        
        initial_SP_A    % initial septa or SP currents 
        initial_CV_A    % initial septa or CV currents 
        present_SP_A    % present septa or SP currents 
        present_CV_A    % present septa or CV currents 

        plane           % plane of scan, H or V
        
        Hemittance      %
        Vemittance      %
        Lifetime        %
        InjEfficency    %
        BeamCurrent     %
        
        scan_name       % label for the present scan.
        scan_figure     %
        scan_automatic  % do not ask any questions. just go.
        
        % scan parameters
        grid_points % 2xN array of min and max Q_h
        grid_size   % 2x1 array , size of the pos angle scan grid
        measured_grid_points % grid points measured after moving the pos and angle
        number_of_removed_points % points removed from grid due to resonance crossing
        
        
        pos_range  % 2x1 array of min and max Q_h
        ang_range  % 2x1 array of min and max Q_v
        
        pos_steps  % 1x1 double minimum step between two diffent Q_h
        ang_steps  % 1x1 double minimum step between two diffent Q_h
        
        pos_ang_change_mode % string: 'matrix' 
        wait_for_ps  % seconds to wait between PS settings.
        
    end
    
    
    methods
        
        % creator
        function obj = InjPosAngScan(machine,varargin)
            %InjPosAngScan Construct an instance of this class
            %
            % for help type:
            % >> doc InjPosAngScan
            %
            %see also: RingControl
            
            % inherit from ring control
            obj@RingControl(machine,varargin{:});
            
            
            % default properties
            obj.plane = 'H'; 
            
            obj.initial_SP_A = [obj.s12.get;obj.s3.get];
            obj.initial_CV_A = [obj.cv8.get;obj.cv9.get];
            obj.present_SP_A = obj.initial_SP_A;
            obj.present_CV_A = obj.initial_CV_A;
            
            obj.initial_pos_ang = [0;0];
            obj.present_pos_ang = [0;0];
            
            obj.scan_name = datestr(now,'WP_YYYY_mm_dd_HH_MM');    % label for the present scan.
            %obj.scan_figure = figure('name',['Working Point Scan: ' obj.scan_name]);
            obj.scan_automatic = false;
            
            obj.pos_range = [-0.5 0.5]*1e-3; % position range
            obj.ang_range = [-1 1]*1e-4; % angle range
            
            obj.pos_steps = 3; % 1x1 double number of steps within pos_range
            obj.ang_steps = 3; % 1x1 double number of steps within ang_range
            
            obj.DefineGridPoints; % 2xN array of min and max Q_h
            
            obj.pos_ang_change_mode='matrix';
            obj.wait_for_ps = 5; % seconds to wait between PS settings.
            
            % machine specific settings.
            switch obj.machine
                case 'sr'
                    
                case 'ebs-simu'
                    
                otherwise
                    error('injection pos ang scan is not supported for this machine')
                   
            end
            
        end
        
        function Reset(obj)
            % reset grid and measurement arrays for a new scan, returns to the initial WP
            obj.DefineGridPoints;
            % go back to initial WP.
            obj.move_pos_ang(obj.initial_pos_ang);
            % no automatic.
            obj.scan_automatic = false;
            
        end
        
        function obj = DefineGridPoints(obj)
            % define the grid of points to scan

            % define initial linear list
            Qx=linspace(obj.pos_range(1),obj.pos_range(2),obj.pos_steps);
            Qy=linspace(obj.ang_range(1),obj.ang_range(2),obj.ang_steps);
            
            % 2D list.
            [qy,qx]=meshgrid(Qy,Qx);
            
            % sort zig-zag low vertical to high vertical angle, to minimize
            % steps in PS current
            qx(:,2:2:end)=qx(end:-1:1,2:2:end);
            
            % remove points to avoid (coupling resonance)
            
            sel=(1:numel(qx))';
            
            % exclude coupling stopband
            qx=qx(sel);
            qy=qy(sel);
            
            obj.number_of_removed_points = sum(sum(sel==0));
            
            obj.grid_points = [qx qy];
            obj.grid_size = size(obj.grid_points);
            
            obj.measured_grid_points = NaN(obj.grid_size); % measured position angle grid
            
            % intialize scan
            obj.Hemittance      = NaN(obj.grid_size(1),1);    %
            obj.Vemittance      = NaN(obj.grid_size(1),1);    %
            obj.Lifetime        = NaN(obj.grid_size(1),1);    %
            obj.InjEfficency    = NaN(obj.grid_size(1),1);    %
            obj.BeamCurrent     = NaN(obj.grid_size(1),1);    %
            
            obj.PlotGridPoints;
            
            disp([num2str(obj.grid_size(1)) ...
                ' points will be tested. if 10s are needed for each point, '...
                num2str(obj.grid_size(1)*10/60) ' minutes to wait']);
            
            % grid has been updated, uptade measurement name as well.
            obj.scan_name = datestr(now,'WP_YYYY_mm_dd_HH_MM');    % label for the present scan.
            %obj.scan_figure = figure('name',['Working Point Scan: ' obj.scan_name]);
            
        end
        
        function PlotGridPoints(obj)
            % display the points that will be scanned. smallest the dot,
            % earlier it will appear in the scan.
            
            % display points to be scanned.
            try
                set(0,'CurrentFigure',obj.scan_figure);
            catch
                obj.scan_figure = figure('name',obj.scan_name);
            end
            %figure('name',['Points to scan: ' obj.scan_name ', dot size = order']);
            scatter(obj.grid_points(:,1),obj.grid_points(:,2),(1:obj.grid_size(1))*5,'LineWidth',2);
            hold on;
            plot(obj.grid_points(:,1),obj.grid_points(:,2),'--','LineWidth',2);
            grid on;
            hold off;
            
            xlim(obj.pos_range);
            ylim(obj.ang_range);
            switch obj.plane
                case 'H'
                    xlabel('x'); ylabel('x''');
                case 'V'
                    xlabel('y'); ylabel('y''');
            end
            legend('points','order','present WP');
            % saveas(gca,['PointsToScanOrder' obj.scan_name '.fig']);
        end
        
        function PlotScan(obj,varargin)
            % display all the measurements taken up to now.
            % 
            % Lifetime, H and V emittance and beam current. Blue is best.
            % 
            % by default the spots are drawn at the measured values.
            % obj.PlotScan(false) to plot at the location specified by
            % obj.grid_points
            % 
            % the empty black circles are the points to scan
            % the red square the present optimum overall
            % the green squares the optimum of each parameter
            % 
            %
            
            if length(varargin)>=1
                measured = varargin{1};
            else
                measured = true;
            end
            
            % display points to be scanned.
            if ~isempty(obj.scan_figure)
                try
                    set(0,'CurrentFigure',obj.scan_figure);
                catch err
                    % figure has been deleted by user.
                    obj.scan_figure = figure('name',['W.P. scan :' obj.scan_name],...
                        'units','normalized','position',[0.05 0.1 0.5 0.6]);
                end
                
            else
                obj.scan_figure = figure('name',['W.P. scan :' obj.scan_name],...
                    'units','normalized','position',[0.05 0.1 0.5 0.6]);
            end
            
            qx = obj.grid_points(:,1);
            qy = obj.grid_points(:,2);
            
            qx0=qx; qy0=qy;
            
            extraborder = [-0.01 0.01];
            extraborderang = [-0.001 0.001];
            
            if measured & find(obj.measured_grid_points)
                extraborderval = max([ ...
                    abs(min(obj.measured_grid_points(:,1)) - obj.pos_range(1)),...
                    abs(max(obj.measured_grid_points(:,1)) - obj.pos_range(2))...
                    ]);
            extraborder = [-extraborderval extraborderval];
            extraborderval = max([ ...
                    abs(min(obj.measured_grid_points(:,2)) - obj.ang_range(1)),...
                    abs(max(obj.measured_grid_points(:,2)) - obj.ang_range(2))...
                ]);
            extraborderang = [-extraborderval extraborderval];
            
            end
            
            if any(isnan(extraborder))
                extraborder = [0 0];
                extraborderang = [0 0];
            end
                
            if measured
                
                % find last measured:
                indlast = find(isnan(obj.measured_grid_points(:,1)),1,'first');
             
                if isempty(indlast)
                    indlast = obj.grid_size(1);
                end
                
                qx(1:indlast) = obj.measured_grid_points(1:indlast,1);
                qy(1:indlast) = obj.measured_grid_points(1:indlast,2);
                
                clf; % clear current figure;
            end
            
            markersize = 50;
            fontsize = 14 ;
            
            scatter(qx,qy,markersize,...
                obj.BeamCurrent(:),'filled');     % color is lifetime
            
            %plot points to be yet scanned
            hold on;
            scatter(qx0,qy0,markersize,[0 0 0]);  %
            
            % plot present best  point
            hold on;
            
            indbest=obj.CurrentBestWP;
            if ~isnan(indbest)
                plot(qx(indbest),qy(indbest),'rs','MarkerSize',markersize/2);
            end
            
            ax= gca;
            ax.FontSize = fontsize;
            hold on;
            
            switch obj.plane
                case 'H'
                    xlabel('x'); ylabel('x''');
                case 'V'
                    xlabel('y'); ylabel('y''');
            end
            
            xlim(obj.pos_range + extraborder);
            ylim(obj.ang_range + extraborderang);
            c=colorbar;
            title( 'Current [mA]');
            hold off;
            
            
            % saveas(gca,['Scan' obj.scan_name '.fig']);
        end
        
        function posang = GetPosAng(obj)
            % read present position and angle variation compared to intial
            
            obj.present_SP_A = [obj.s12.get;obj.s3.get];
            obj.present_CV_A = [obj.cv8.get;obj.cv9.get];
            
            switch obj.plane
                case 'H'
                    pres_cur = obj.present_SP_A;
                    init_cur = obj.initial_SP_A;
                    [resp,~]  = obj.compute_AngPos_SP_CV_Resp;
                case 'V'
                    pres_cur = obj.present_CV_A;
                    init_cur = obj.initial_CV_A;
                    [~,resp]  = obj.compute_AngPos_SP_CV_Resp;
            end
            
            posang = resp * [pres_cur-init_cur];
            
        end
        
        function move_pos_ang(obj,desired_pos_ang,initial_pos_ang)
            % function to change position and angle
            if nargin<3
                initial_pos_ang =[0; 0];
            end
            
            obj.present_SP_A = [obj.s12.get;obj.s3.get];
            obj.present_CV_A = [obj.cv8.get;obj.cv9.get];
            
            switch obj.plane
                case 'H'
                    pres_cur = obj.present_SP_A;
                    init_cur = obj.initial_SP_A;
                    [resp,~]  = obj.compute_AngPos_SP_CV_Resp;
                case 'V'
                    pres_cur = obj.present_CV_A;
                    init_cur = obj.initial_CV_A;
                    [~,resp]  = obj.compute_AngPos_SP_CV_Resp;
            end
            
            deltacur = resp \ desired_pos_ang; %delta for desired position/angle
            disp(['delta currents: ' num2str(deltacur')]);
            
            switch obj.plane
                case 'H'
                    disp(['setting septa to: ' num2str((obj.initial_SP_A + deltacur)')]);
                    
                    obj.s12.set(obj.initial_SP_A(1)+deltacur(1));
                    
                    obj.s3.set(obj.initial_SP_A(2)+deltacur(2));
                    
                case 'V'
                    disp(['setting CV8-9 to: ' num2str((obj.initial_CV_A + deltacur)')]);
                   
                    obj.cv8.set(obj.initial_CV_A(1)+deltacur(1));
                    
                    obj.cv9.set(obj.initial_CV_A(2)+deltacur(2));
            end
                
            % wait setting and measurement
            pause(obj.wait_for_ps);
            
            final_pos_ang=obj.GetPosAng;
            
            % read position ang angle
            disp(['Initial pos ang: ' num2str(initial_pos_ang')]);
            disp(['Desired pos ang: ' num2str(desired_pos_ang')]);
            disp(['Obtained pos ang: ' num2str(final_pos_ang')]);
            
        end
        
        function check_grid(obj)
            % check if grid points are consistent ot something has been
            % modified.
            
            %check size
            if obj.pos_steps * obj.ang_steps - obj.number_of_removed_points ~= obj.grid_size(1)
                error('Inconsistent grid size. ts.DefineGridPoints or ts.Reset to redefine the grid and start a new measurement');
            end
            % check range
            if obj.pos_range(1) ~= min(obj.grid_points(:,1))
                error('Inconsistent min Q_h range. ts.DefineGridPoints or ts.Reset to redefine the grid and start a new measurement');
            end
            if obj.pos_range(2) ~= max(obj.grid_points(:,1))
                error('Inconsistent max Q_h range. ts.DefineGridPoints or ts.Reset to redefine the grid and start a new measurement');
            end
            
            if obj.ang_range(1) ~= min(obj.grid_points(:,2))
                error('Inconsistent min Q_v range. ts.DefineGridPoints or ts.Reset to redefine the grid and start a new measurement');
            end
            if obj.ang_range(2) ~= max(obj.grid_points(:,2))
                error('Inconsistent max Q_v range. ts.DefineGridPoints or ts.Reset to redefine the grid and start a new measurement');
            end
            
        end
        
        
        function RunScan(obj)
            % runs the working point scan
            
            % here a call to obj.DefineGridPoints could make safer the use of
            % the code, however, omitting this call, allows to run the scan
            % in multipole times, stopping and restarting from the last unmeasured point.
            
            obj.check_grid;
            
            qx = obj.grid_points(:,1);
            qy = obj.grid_points(:,2);
            
            % start loop
            for itun=1:length(qx)
                
                if any( isnan( obj.measured_grid_points(itun,:) ) )
                    
                    % get present pos angle.
                    
                    inittune =  obj.GetPosAng; % read tunes
                    disp(['Presently at: ' num2str(inittune') ': ' num2str((itun-1)/obj.grid_size(1)*100,'%2.0f') '% completed' ]);
                    
                    % ask if point is to be measured or skipped
                    if obj.scan_automatic
                        measure_or_skip='y'; % contiunue always
                    else
                        measure_or_skip = input(['Go to: '...
                            num2str([qx(itun),qy(itun)]) ...
                            ' / skip and move to next point / stop now / continue without asking? [(y)es / (n)ext / (s)top / (a)uto ]'],'s');
                    end
                    
                    if measure_or_skip == 'a'
                        obj.scan_automatic = true;
                        disp('running in automatic mode.');
                        disp('To return to ask questions, Ctrl-C and restart scan (points measured are kept).')
                    end
                    
                    if measure_or_skip == 'n' % skipped point, all data will be NaN
                        
                        disp(['Skipped (position,angle): ' num2str([qx(itun),qy(itun)]) ]);
                        obj.measured_grid_points(itun,:) = [qx(itun),qy(itun)] ;
                        
                    else
                        
                        
                        
                        % change tunes
                        obj.move_pos_ang([qx(itun);qy(itun)],inittune);
                        
                        if ~obj.scan_automatic
                            input('press to measure data at this point?','s');
                        end
                        
                        
                        
                        disp('measure relevant quantities');
                        
                        % get lifetime
                        obj.Lifetime(itun)=NaN;%getTouschekLifetime(300,5); % vac, averaging time,
                        
                        % get emittances
                        obj.Hemittance(itun)=NaN;%getHEmittance(1); % 10 waits 1 second and makes 10 averages
                        obj.Vemittance(itun)=NaN;%getVEmittance(1); % 10 waits 1 second and makes 10 averages
                        
                        % get stored current
                        obj.BeamCurrent(itun)=getTotCurrent();
                        
                        % update measured tune
                        actual_pos_ang = obj.GetPosAng;
                        
                        QX(itun)=actual_pos_ang(1);
                        QY(itun)=actual_pos_ang(2);
                        
                        obj.measured_grid_points(itun,1)= QX(itun);
                        obj.measured_grid_points(itun,2)= QY(itun);
                        
                    end
                    
                    % save
                    save(['temp_' obj.scan_name '.mat'],'obj');
                    
                    % plot present scan status
                    obj.PlotScan;
                    
                else
                    
                    disp('Pos Ang: ')
                    disp([qx(itun),qy(itun)])
                    disp('already measured');
                    
                end
                
                
            end
            
            
            % go back to initial WP.
            obj.move_pos_ang(obj.initial_pos_ang,obj.GetPosAng)
            
            % save final data
            save(['Data_' obj.scan_name '.mat'],'obj');
            
            % plot figures with measured pos angle.
            obj.PlotScan;
            
            saveas(gca,['PosAngeScan_' obj.plane '_' obj.scan_name '.fig']);
            
        end
        
        function CancelLastPoints(obj,n)
            % CANCELLASTPOINTS erases the data taken in the last n measurements.
            %
            %example: obj.CancelLastPoints(3);
            
            nmeas = find(isnan(obj.measured_grid_points(:,1)),1,'first');
            disp([num2str(nmeas) ' points where measured, removing last ' num2str(n)]);
            if isempty(nmeas)
                nmeas = obj.grid_size(1);
            end
            ind2cancel = nmeas:-1:(nmeas-n+1);
            
            obj.measured_grid_points(ind2cancel,:)=NaN;
            
            obj.Lifetime(ind2cancel)=NaN; % vac, averaging time,
            
            % get emittances
            obj.Hemittance(ind2cancel)=NaN; % 10 waits 1 second and makes 10 averages
            obj.Vemittance(ind2cancel)=NaN; % 10 waits 1 second and makes 10 averages
            
            % get stored current
            obj.BeamCurrent(ind2cancel)=NaN;
            
            nmeas = find(isnan(obj.measured_grid_points(:,1)),1,'first');
            disp([num2str(nmeas) ' valid points ']);
            
            obj.PlotScan(true); % plot measured W.P.
            
        end
        
        
        function ind_best=CurrentBestWP(obj)
            % guess of best WP from max(TLT*Current/hemit/vemit)
            %
            
            %val =  obj.Lifetime .* obj.BeamCurrent ./ obj.Hemittance ./ obj.Vemittance ;
            val =  obj.BeamCurrent ;
            
            [~,ind_best] = max(val(~isnan(val)));
            disp(['Best W.P. ' num2str(obj.measured_grid_points(ind_best,:))]);
            
        end
        
        
        % signature of methods defined in seprated functions below
        
        
    end
    
    methods (Static) % methods that do not need to know about this class, no input "obj"
        [RMSP,RMCV]=compute_AngPos_SP_CV_Resp() % method to draw resonance lines
    end
    
end

