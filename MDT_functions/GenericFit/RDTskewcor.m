function [rcor,resout,corq0,dcq]=RDTskewcor(r0,rerr,indBPM,indcor,aa)

rcor=rerr;

[kn,ks,ind]=EquivalentGradientsFromAlignments(r0,rerr);
indAllQuad=ind;
indQuadCor=indcor;%indAllQuad(1:1:end);
neigQuadCor=50;

corq0=getcellstruct(rerr,'PolynomA',indQuadCor,1,2); % skew quadrupole

% integrated strengths
L=cellfun(@(a)a.Length,r0(ind));

knL=kn.*L;
ksL=ks.*L;

[respx,respz]=semrdtresp_mod(r0,indBPM,ind);

fx=respx*ksL;
fz=respz*ksL;

% get current horizontal dispersion
[~,~,dx0,dy0,bx0,by0,t0,mx0,my0]=getorbdispbeta(r0,indBPM);
[~,~,dx,dy,bx,by,t,mx,my]=getorbdispbeta(rerr,indBPM);

kval=1e-5;
% dispersion  RM
delta=1e-3;
rmD=findrespm_mod(r0,indBPM,indQuadCor,kval,...
    'PolynomA',1,2,'getDispersion4',delta);
disp(' --- computed dispersion RM from fitted model --- ')

[~,skcor]=ismember(indQuadCor,indAllQuad); % elements from RM to be used for correction
[~,skcorrm]=ismember(indQuadCor,indQuadCor); % elements from RM to be used for correction

rdtmat=[...
    real(respx(:,skcor));...
    imag(respx(:,skcor));...
    real(respz(:,skcor));...
    imag(respz(:,skcor))];

rdtvec=[...
    real(fx);...
    imag(fx);...
    real(fz);...
    imag(fz)];
if nargin<5
aa=0.3;
end

R=[...
    aa*rdtmat;...
    (1-aa)*[rmD{3}(:,skcorrm)]./kval;...
    ];

w=[...
    aa*rdtvec;...
    (1-aa)*[(dy-dy0)]';...
    ];

dcq=zeros(size(corq0));% initialize to no change
%try
dcq=qemsvd_mod(R,w,neigQuadCor);

rcor=setcellstruct(rcor,'PolynomA',indQuadCor,corq0-dcq,1,2);
[~,~,dxc,dyc,bxc,byc,tc,mxc,myc]=getorbdispbeta(rcor,indBPM);
[knc,ksc,indc]=EquivalentGradientsFromAlignments(r0,rcor);
% integrated strengths
L=cellfun(@(a)a.Length,r0(ind));

kncL=knc.*L;
kscL=ksc.*L;

fxc=respx*kncL;
fzc=respz*kscL;

resout(1).fx=fx;
resout(2).fx=fxc;
resout(1).fz=fz;
resout(2).fz=fzc;
resout(1).dy=dy-dy0;
resout(2).dy=dyc-dy0;
resout(1).by=by-by0;
resout(2).by=byc-by0;
resout(1).kn=knL;
resout(2).kn=kncL;
resout(1).ks=ksL;
resout(2).ks=kscL;
resout(1).indk=ind;
resout(2).indk=indc;

% figure;
% plot(indBPM,abs(fx),'r',indBPM,abs(fz),'b',indBPM,(dx-dx0),'g');hold on;
% plot(indBPM,abs(fxc),'m',indBPM,abs(fzc),'c',indBPM,(dxc-dx0),'y');
% figure;
% plot(indBPM,(bx-bx0)./bx0,'r',indBPM,(by-by0)./by0,'b');hold on;
% %plot(indBPM,abs(bx),'y',indBPM,abs(by),'g');
% plot(indBPM,(bxc-bx0)./bx0,'m',indBPM,(byc-by0)./by0,'c');

 figure;
 subplot(3,1,1)
 plot(indBPM,abs(fz),'r',indBPM,abs(fzc),'b');hold on;
 ylabel('RDT skew')
 subplot(3,1,2)
 plot(indBPM,(dy-dy0),'r',indBPM,(dyc-dy0),'b');
 ylabel('vertical dispersion')
 subplot(3,1,3)
 plot(indBPM,(by-by0)./by0,'r');hold on;
 plot(indBPM,(byc-by0)./by0,'b');
 ylabel('vertical beta')
 
 xlabel('BPM number');
 legend('before cor','after cor');
 
 