function [ox,oy,dx,dy,bx,by,t,mx,my,c]=getorbdispbeta(r,refpts,delta)

if nargin<3
    delta=0;
end


%ox=arrayfun(@(s)s.ClosedOrbit(1),l);
%oy=arrayfun(@(s)s.ClosedOrbit(3),l);
%dx=arrayfun(@(s)s.Dispersion(1),l);
%dy=arrayfun(@(s)s.Dispersion(3),l);

[ox,oy,dx,dy,t]=getorbdisp(r,refpts,delta);

[l,t,c]=atlinopt(r,delta,refpts);

bx=arrayfun(@(s)s.beta(1),l);
by=arrayfun(@(s)s.beta(2),l);
mx=arrayfun(@(s)s.mu(1),l);
my=arrayfun(@(s)s.mu(2),l);


return