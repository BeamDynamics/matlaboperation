function [rcor,resout]=RDTquadcor(r0,rerr,indBPM,indcor,aa)

rcor=rerr;

[kn,ks,ind]=EquivalentGradientsFromAlignments(r0,rerr);
indAllQuad=ind;
indQuadCor=indcor;%indAllQuad(1:1:end);
neigQuadCor=15;

corq0=getcellstruct(rerr,'PolynomB',indQuadCor,1,2); % skew quadrupole

% integrated strengths
L=cellfun(@(a)a.Length,r0(ind));

knL=kn.*L;
ksL=ks.*L;

[respx,respz]=qemrdtresp_mod(r0,indBPM,ind);

fx=respx*knL;
fz=respz*knL;

% get current horizontal dispersion
[~,~,dx0,dy0,bx0,by0,t0,mx0,my0]=getorbdispbeta(r0,indBPM);
[~,~,dx,dy,bx,by,t,mx,my]=getorbdispbeta(rerr,indBPM);

kval=1e-5;
% dispersion  RM
delta=1e-3;
rmD=findrespm_mod(r0,indBPM,indQuadCor,kval,...
    'PolynomB',1,2,'getDispersion4',delta);
disp(' --- computed dispersion RM from fitted model --- ')

[~,skcor]=ismember(indQuadCor,indAllQuad); % elements from RM to be used for correction
[~,skcorrm]=ismember(indQuadCor,indQuadCor); % elements from RM to be used for correction

rdtmat=[...
    real(respx(:,skcor));...
    imag(respx(:,skcor));...
    real(respz(:,skcor));...
    imag(respz(:,skcor))];

rdtvec=[...
    real(fx);...
    imag(fx);...
    real(fz);...
    imag(fz)];

if nargin<5
aa=0.3;
end

R=[...
    aa*rdtmat;...
    (1-aa)*[rmD{1}(:,skcorrm)]./kval;...
    ];

w=[...
    aa*rdtvec;...
    (1-aa)*[(dx-dx0)]';...
    ];

dcq=zeros(size(corq0));% initialize to no change
%try
dcq=qemsvd_mod(R,w,neigQuadCor);

rcor=setcellstruct(rcor,'PolynomB',indQuadCor,corq0-dcq,1,2);
[~,~,dxc,dyc,bxc,byc,tc,mxc,myc]=getorbdispbeta(rcor,indBPM);
[knc,ksc,indc]=EquivalentGradientsFromAlignments(r0,rcor);
% integrated strengths
L=cellfun(@(a)a.Length,r0(indc));

kncL=knc.*L;
kscL=ksc.*L;

fxc=respx*kncL;
fzc=respz*kscL;


resout(1).fx=fx;
resout(2).fx=fxc;
resout(1).fz=fz;
resout(2).fz=fzc;
resout(1).dx=dx-dx0;
resout(2).dx=dxc-dx0;
resout(1).bx=bx-bx0;
resout(2).bx=bxc-bx0;
resout(1).kn=knL;
resout(2).kn=kncL;
resout(1).ks=ksL;
resout(2).ks=kscL;
resout(1).indk=ind;
resout(2).indk=indc;

 figure;
 subplot(3,1,1)
 plot(indBPM,abs(fx),'r',indBPM,abs(fxc),'b');hold on;
 ylabel('Normal RDT');
 subplot(3,1,2)
 plot(indBPM,(dx-dx0),'r',indBPM,(dxc-dx0),'b');
 ylabel('hor. dispersion');
 subplot(3,1,3)
 plot(indBPM,(bx-bx0)./bx0,'r');hold on;
 plot(indBPM,(bxc-bx0)./bx0,'b');
 ylabel('hor. beta')
 xlabel('BPM number');
 legend('before cor','after cor');