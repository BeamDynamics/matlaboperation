function [W,Ddp,bdp]=getWdispP(ring,DE,idx,delta)
%
% compute W functions and dispersion and beta derivative
%
if nargin<4
    delta=0;
end
if nargin<3
    idx=1:length(ring);
end
if nargin<2
    DE=1e-8;
end

[lz,~,~]=atlinopt(ring,delta,idx);
[lpd,~,~]=atlinopt(ring,delta+DE,idx);
[lmd,~,~]=atlinopt(ring,delta-DE,idx);
bz=cat(1,lz.beta);
bp=cat(1,lpd.beta);
bm=cat(1,lmd.beta);   % left axis
az=cat(1,lz.alpha);
ap=cat(1,lpd.alpha);
am=cat(1,lmd.alpha);   % left axis

aa=(bp-bm)./2./DE./bz;
bb=(ap-am)./2./DE-az./bz.*(bp-bm)./2./DE;
W=sqrt(aa.^2+bb.^2);

dp=cat(2,lpd.Dispersion)'; % right axis
dm=cat(2,lmd.Dispersion)';
Ddp=(dp(:,1)-dm(:,1))./2/DE;
bdp=(bp-bm)./2./DE;

return
