function [kn,ks,ind]=EquivalentGradientsFromAlignments(r0,r)
%
% the function finds the closed orbit at sextupoles and converts it to
% equivalent quadrupole and skew quadrupole gradients for the computation
% of skew and normal quadrupole RDT 
% 
% it returns the complete list of normal (kn) and skew (ks) quadrupole
% gradients at the given indexes (ind) ( not integrated, PolynomB)
% 
% 
%see also:  

% quadrupole and skew quadrupole errors are introduced via COD in
% sextupoles
indsext=findcells(r,'Class','Sextupole');

b3=getcellstruct(r,'PolynomB',indsext,1,3);

oin=findorbit4Err(r,0.0,indsext);% orbit at entrance of sextupole 
oout=findorbit4Err(r,0.0,indsext+1); % orbit at exit of sextupole
xmisal=-cellfun(@(a)a.T1(1),r(indsext));
ymisal=-cellfun(@(a)a.T1(3),r(indsext));


Dx=(oout(1,:)+oin(1,:))/2-xmisal'; % orbit average in sextupole
Dy=(oout(3,:)+oin(3,:))/2-ymisal'; % orbit average in sextupole

% quarupole errors in sextupoles
kn_sext_err=cellfun(@(a)a.PolynomB(2),r0(indsext));
ks_sext_err=cellfun(@(a)a.PolynomA(2),r0(indsext));

kn_sext=2.*b3.*Dx'+kn_sext_err;
ks_sext=-2.*b3.*Dy'+ks_sext_err;

% quadrupole rotations
indquad=findcells(r,'Class','Quadrupole');

b2=getcellstruct(r,'PolynomB',indquad,1,2);
try
srot=asin(cellfun(@(a)a.R1(1,3),r(indquad)));
catch
    srot=0*b2;
    warning('no rotation found at least in one quadrupole. all set to zero')
end

kn_quad=zeros(size(b2));
ks_quad=-srot.*b2;

% all elements with PolynomB
indPolB=findcells(r,'PolynomB');

NpolB=cellfun(@(a)length(a.PolynomB),r0(indPolB));
NpolA=cellfun(@(a)length(a.PolynomA),r0(indPolB));

indPolB=indPolB(NpolB>=2 & NpolA>=2);

kn0_all=cellfun(@(a)a.PolynomB(2),r0(indPolB));
ks0_all=cellfun(@(a)a.PolynomA(2),r0(indPolB));

kn_all=cellfun(@(a)a.PolynomB(2),r(indPolB))-kn0_all;
ks_all=cellfun(@(a)a.PolynomA(2),r(indPolB))-ks0_all;

[ind,ord]=sort([indsext,indquad,indPolB]);

kn=[kn_sext;kn_quad;kn_all];
ks=[ks_sext;ks_quad;ks_all];

% integrated strengths
% L=cellfun(@(a)a.Length,r(ind));

kn=kn(ord);%.*L;
ks=ks(ord);%.*L;


return