function [oxp,oyp,dxp,dyp]=getangorbdisp(r,refpts,delta)

if nargin<3
    delta=0;
end

% [l,t,c]=atlinopt(r,0,refpts);
% 
% ox=arrayfun(@(s)s.ClosedOrbit(1),l);
% oy=arrayfun(@(s)s.ClosedOrbit(3),l);
% dx=arrayfun(@(s)s.Dispersion(1),l);
% dy=arrayfun(@(s)s.Dispersion(3),l);

o=findorbit4Err(r,delta,refpts);
oxp=o(2,:);
oyp=o(4,:);

d=getDispersion4Err(r,delta+1e-4,refpts);

dxp=d(3,:);
dyp=d(4,:);


return