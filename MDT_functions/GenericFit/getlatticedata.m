function [data,data0,datae]=getlatticedata(r0,r,ind,indHCor,indVCor,indQCor,indSCor,delta)

if nargin<8
    delta=0;% delta for the nominal energy model!
end

%rp=ringpara(r0);
%E0=rp.E0;
E0=atenergy(r0);

Brho=3.3356*E0*1e-9;

Lh0=getcellstruct(r0,'Length',indHCor);
Lv0=getcellstruct(r0,'Length',indVCor);
Lq0=getcellstruct(r0,'Length',indQCor);
Ls0=getcellstruct(r0,'Length',indSCor);
ch0=getcellstruct(r0,'PolynomB',indHCor,1,1);
cv0=getcellstruct(r0,'PolynomA',indVCor,1,1);
cq0=getcellstruct(r0,'PolynomB',indQCor,1,2);
cs0=getcellstruct(r0,'PolynomA',indSCor,1,2);

ch0=ch0.*Lh0; % rad
cv0=cv0.*Lv0; % rad
cq0=cq0.*Lq0.*Brho; % T
cs0=cs0.*Ls0.*Brho; % T

[monh0,monv0,dish0,disv0,bbh0,bbv0,~,mh0,mv0,c0]=getorbdispbeta(r0,ind,delta);
[monhp0,monvp0,dishp0,disvp0]=getangorbdisp(r0,ind,delta);

[W0,Ddp0,Bdp0]=getWdispP(r0,1e-5,ind,delta);

try
    [~,b0]=atx(atsetcavity(r0,6e6,0,992),delta,1);
catch exc
    getReport(exc,'extended');
    warning('atx failed');
    b0.modemittance=[NaN NaN];
    b0.fulltunes=[NaN NaN];
end
tune0=b0.fulltunes;
ch=getcellstruct(r,'PolynomB',indHCor,1,1);
cv=getcellstruct(r,'PolynomA',indVCor,1,1);
cq=getcellstruct(r,'PolynomB',indQCor,1,2);
cs=getcellstruct(r,'PolynomA',indSCor,1,2);

ch=ch.*Lh0; % rad
cv=cv.*Lv0; % rad
cq=cq.*Lq0.*Brho; % T
cs=cs.*Ls0.*Brho; % T

[monh,monv,dish,disv,bbh,bbv,~,mh,mv,c]=getorbdispbeta(r,ind,delta);
[monhp,monvp,dishp,disvp]=getangorbdisp(r,ind,delta);
[W,Ddp,Bdp]=getWdispP(r,1e-5,ind,delta);

try
    [~,b]=atx(atsetcavity(r,6e6,0,992),delta,1);
catch exc
    getReport(exc,'extended');
    warning('atx failed');
    b.modemittance=[NaN NaN];
 b.fulltunes=[NaN NaN];
end
tune=b.fulltunes;

% RDT
[respxs0,respzs0]=semrdtresp_mod(r0,ind,indSCor);    % RDT response matrix assumes K=1
[respxs,respzs]=semrdtresp_mod(r,ind,indSCor);    % RDT response matrix assumes K=1

QfLs=getcellstruct(r0,'Length',indSCor);          % quadrupole lengths
QfLs(QfLs==0)=1;% thin lens magnets
lengthsmat=repmat(QfLs',length(ind),1);
respxs0=respxs0.*lengthsmat;
respzs0=respzs0.*lengthsmat;
QfLs=ones(size(QfLs)); % not used, to make sure that no rescaling is done...
dkns=getcellstruct(r0,'PolynomA',indSCor,1,2); % quadrupole gradients difference from model
strengths=[QfLs.*dkns]; % %  integrated strength to compute RDT
fxs0=respxs0*strengths;
fzs0=respzs0*strengths;
rdtvec0=[real(fxs0);...
    imag(fxs0);...
    real(fzs0);...
    imag(fzs0);...
    ];

QfLs=getcellstruct(r,'Length',indSCor);          % quadrupole lengths
QfLs(QfLs==0)=1;% thin lens magnets
lengthsmat=repmat(QfLs',length(ind),1);
respxs=respxs.*lengthsmat;
respzs=respzs.*lengthsmat;
QfLs=ones(size(QfLs)); % not used, to make sure that no rescaling is done...
dkns=getcellstruct(r,'PolynomA',indSCor,1,2); % quadrupole gradients difference from model
strengths=[QfLs.*dkns]; % %  integrated strength to compute RDT
fxs=respxs*strengths;
fzs=respzs*strengths;
rdtvec=[real(fxs);...
    imag(fxs);...
    real(fzs);...
    imag(fzs);...
    ];


% R13
Rmn0=getRmn4(r0,0,ind);
Rmne=getRmn4(r,0,ind);


data.ox=monh-monh0; % m
data.oy=monv-monv0; % m
data.dx=dish-dish0; % m
data.dy=disv-disv0; % m
data.oxp=monhp-monhp0; % rad
data.oyp=monvp-monvp0; % rad
data.dxp=dishp-dishp0; % rad
data.dyp=disvp-disvp0; % rad
data.bx=(bbh-bbh0)./bbh0; % 1
data.by=(bbv-bbv0)./bbv0; % 1
data.mx=mh-mh0;  % m
data.my=mv-mv0;  % m
data.tune=tune-tune0;  % 1
data.chrom=c-c0;  % 1
data.ex=b.modemittance(1)-b0.modemittance(1);  % mrad
data.ey=b.modemittance(2)-b0.modemittance(2);  % mrad
data.hc=ch-ch0; % rad
data.vc=cv-cv0; % rad
data.sc=cs-cs0; % T
data.qc=cq-cq0; % T
data.Wx=W(:,1)-W0(:,1); % W function
data.Wy=W(:,2)-W0(:,2); % W function
data.Bdpx=Bdp(:,1)-Bdp0(:,1); % W function
data.Bdpy=Bdp(:,2)-Bdp0(:,2); % W function
data.DxDp=Ddp-Ddp0; % second order dispersion
data.rdt=rdtvec-rdtvec0;
data.R13=Rmne(3,:)-Rmn0(3,:);

data0.ox=monh0; % m
data0.oy=monv0; % m
data0.dx=dish0; % m
data0.dy=disv0; % m
data0.oxp=monhp0; % rad
data0.oyp=monvp0; % rad
data0.dxp=dishp0; % rad
data0.dyp=disvp0; % rad
data0.bx=bbh0; % 1
data0.by=bbv0; % 1
data0.mx=mh0;  % m
data0.my=mv0;  % m
data0.tune=tune0;  % 1
data0.chrom=c0;  % 1
data0.ex=b0.modemittance(1);  % mrad
data0.ey=b0.modemittance(2);  % mrad
data0.hc=ch0; % rad
data0.vc=cv0; % rad
data0.sc=cs0; % T
data0.qc=cq0; % T
data0.Wx=W0(:,1); % W function
data0.Wy=W0(:,2); % W function
data0.Bdpx=Bdp0(:,1); % W function
data0.Bdpy=Bdp0(:,2); % W function
data0.DxDp=Ddp0; % second order dispersion
data0.rdt=rdtvec;
data0.R13=Rmn0(3,:);

datae.ox=monh; % m
datae.oy=monv; % m
datae.dx=dish; % m
datae.dy=disv; % m
datae.oxp=monhp; % rad
datae.oyp=monvp; % rad
datae.dxp=dishp; % rad
datae.dyp=disvp; % rad
datae.bx=bbh; % 1
datae.by=bbv; % 1
datae.mx=mh;  % m
datae.my=mv;  % m
datae.tune=tune;  % 1
datae.chrom=c;  % 1
datae.ex=b.modemittance(1);  % mrad
datae.ey=b.modemittance(2);  % mrad
datae.hc=ch; % rad
datae.vc=cv; % rad
datae.sc=cs; % T
datae.qc=cq; % T
datae.Wx=W(:,1); % W function
datae.Wy=W(:,2); % W function
datae.Bdpx=Bdp(:,1); % W function
datae.Bdpy=Bdp(:,2); % W function
datae.DxDp=Ddp; % second order dispersion
datae.rdt=rdtvec;
datae.R13=Rmne(3,:);
