function [ox,oy,dx,dy,t]=getorbdisp(r,refpts,delta,inCOD)

if nargin<4
    inCOD=[0,0,0,0];
end

if nargin<3
    delta=0;
end

% [l,t,c]=atlinopt(r,0,refpts);
% 
% ox=arrayfun(@(s)s.ClosedOrbit(1),l);
% oy=arrayfun(@(s)s.ClosedOrbit(3),l);
% dx=arrayfun(@(s)s.Dispersion(1),l);
% dy=arrayfun(@(s)s.Dispersion(3),l);

o=findorbit4Err(r,delta,refpts,[inCOD,0,0]');
ox=o(1,:);
oy=o(3,:);

d=getDispersion4Err(r,delta+1e-4,refpts,[inCOD,0,0]');

dx=d(1,:);
dy=d(3,:);

[~,t,~]=atlinopt(r,delta,1,[inCOD,0,0]');

return