function [D]=getDispersion4Err(THERING,DE,bpmindx,codguess)
% function D=getDispersion4Err(THERING,DE,bpmindx)
% 
% determines dispersion taking two orbits at plus and minus DE
% This function may be used in the findrespm.m function
%
% uses findorbit4Err with bpm reading errors
%
%see also: getDispersion4 findorbit4 ApplyBPMErr

if nargin<4
    codguess=[0,0,0,0,0,0]';
end

Op=findorbit4Err(THERING,DE,bpmindx,codguess);

Om=findorbit4Err(THERING,-DE,bpmindx,codguess);

D=(Op-Om)./(2*DE);

return