%% prepare lattice and compute RM derivative  

close all
clear all

addpath('/mntdirect/_machfs/liuzzo/EBS/beamdyn/OARfunctions');
addpath('/mntdirect/_machfs/liuzzo/EBS/beamdyn/OARfunctions/GenericResponseMatrixDerivative');

%% load simulator lattice and set quad correctors to zero
sim = load('/machfs/liuzzo/EBS/beamdyn/matlab/optics/ebs/S28F_restart/errcormodel.mat');
%initialize bpm offset
%rs=atsetfieldvalues(sim.errcormodel,indBPM,'Offset',{1,1},0);
%rs=atsetfieldvalues(rs,indBPM,'Offset',{1,2},0);


%% load measured RM
rmdirect='/machfs/liuzzo/EBS/beamdyn/MDT/2019/2019_09_24/resp1/';

[rmmeas0,qemres0,semres0]=qemextract(rmdirect);
[qemres0.resph,qemres0.respv,qemres0.frespx,semres0.frespz]=qemcheckresp(qemres0,semres0,[]);
[semres0.resph,semres0.respv]=semloadresp(qemres0,semres0,[]);
orbit0=load(fullfile(rmdirect,'settings'));

rerr=rmmeas0(2).at;%CTRM fit
r0err=qemres0.at;%CTRM no err

indBPMe=qemres0.bpmidx;
indHCore=qemres0.steerhidx;
indVCore=qemres0.steervidx;
indQuadsDerive=qemres0.qpidx;
indSCore=qemres0.skewidx;

r0=r0err;
ring=r0;
rp=ringpara(r0);

bpmindex=1:length(indBPMe);%[1:1:58,60:79,81:length(indBPM)];

[~,~,ch]=atlinopt(rerr,0,1);
% RMfull=[-qemres0.resph(:)./brx(:),semres0.respv(:)./bry(:);...
%      -semres0.resph(:)./brx(:),qemres0.respv(:)./bry(:)];FitCorQuadKnKsDipAngRotCoupX5

 W(1) = 1; % rm hh vv
 W(2) =1 ; % rm hv vh
 W(3) = 100; % disp h
 W(4) = 1000; % disp v
 W(5) =1; % orbit h
 W(6) =1; % orbit v
 W(7) =100; % tune
 W(8) =1; % chrom
 
wrh = W(1); % rm hh vv
wrv = W(2); % rm hv vh
wdh = W(3); % disp h
wdv = W(4); % disp v
woh = W(5); % orbit h
wov = W(6); % orbit v
wt = W(7); % tune
wc = W(8); % chrom

% fulldisp
RMfull=[wrh *qemres0.resph(bpmindex,:),wrv *semres0.resph(bpmindex,:);...
     wrv *semres0.respv(bpmindex,:),wrh *qemres0.respv(bpmindex,:)];

% error('here instead of multiplying by the lengths, modify the RM simulation to also use integrated strenghts.')
RMmeasured=[RMfull(:);...
    wdh*qemres0.frespx(bpmindex,:)*(-rp.alphac);..../bpmresx(bpmindex);...
    wdv*semres0.frespz(bpmindex,:)*(-rp.alphac);..../bpmresy(bpmindex);...
    ...1000*orbit0.horbit(bpmindex);..../bpmresx(bpmindex);...
    ...1000*orbit0.vorbit(bpmindex);..../bpmresx(bpmindex);...
    wt*qemres0.tunes'];

% skewdisp
rmskew =[ wrv *semres0.resph(bpmindex,:);...
     wrv *semres0.respv(bpmindex,:)];
RMmeasuredSkew=[rmskew(:);...
    wdv*semres0.frespz(bpmindex,:)*(-rp.alphac);..../bpmresy(bpmindex);...
    ];


RMat = [rmmeas0(2).atresph,rmmeas0(2).atresph2v;...
    rmmeas0(2).atrespv2h,rmmeas0(2).atrespv];
RMqempanel = [RMat(:);rmmeas0(2).frespx*(-rp.alphac);rmmeas0(2).frespz*(-rp.alphac);rmmeas0(2).tunes'];
%%

%pathtolatticefile='ESRF_fortol.mat';
%a=load(pathtolatticefile);
%ring=a.ring;

indBPM=qemres0.bpmidx;
indHCor=qemres0.steerhidx;
indVCor=qemres0.steervidx;
indSCor=qemres0.skewidx;

% indBPM=findcells(ring,'Class','Monitor');
% indHCor=qemres0.qcoridx;%findcells(ring,'iscorH','H');
% indVCor=findcells(ring,'iscorV','V');
% indSCor=findcells(ring,'iscorS','S');
indQuadsDeriv=qemres0.qpidx(qemres0.qpfit);%findcells(ring,'Class','Quadrupole');
indSextDeriv=findcells(ring,'Class','Sextupole');
indDip=qemres0.dipidx;

indhsel=indHCor(qemres0.hlist);
indvsel=indVCor(qemres0.vlist);
indbsel=indBPM(bpmindex);

% load('BPMnoiseJun10_2015.mat','H','V'); %measured BPM noise
% bpmresx=std(H);
% bpmresy=std(V);
% brx=repmat(bpmresx,length(indhsel),1)';
% bry=repmat(bpmresy,length(indhsel),1)';

%initialize bpm offset
ring=atsetfieldvalues(ring,indBPM,'Offset',{1,1},0);
ring=atsetfieldvalues(ring,indBPM,'Offset',{1,2},0);

% initialize alignment errors
alerind=[indQuadsDeriv indSCor indSextDeriv];
ring=atsetshift(ring,alerind,zeros(size(alerind)),zeros(size(alerind)));
ring=atsettilt(ring,alerind,zeros(size(alerind)));
% 
% % sextupole in dipoles -1.77T/m
% Ldip=cellfun(@(a)a.Length,ring(indDip));
% ring=atsetfieldvalues(ring,indDip,'PolynomB',{1,3},-1.77/getBrho(ring)./Ldip);
% ring=atsetfieldvalues(ring,indDip,'PolynomA',{1,3},0);
% ring=atsetfieldvalues(ring,indDip,'MaxOrder',{1,1},2);

% figure; atplot(ring);
% return

% define perturbation indexes and values (not all are used)
curdir=pwd;
speclab='FitCorQuadKnKsDipAngRotCoupRecompute';
addpath(curdir)
mkdir(speclab)
cd(speclab)

% relative fit of errors
rwithcor=ring;

% error('commands below do not work for corertors on main magnets')
%rwithcor=atsetfieldvalues(ring,qemres0.qcoridx,'PolynomB',{1,2},rmmeas0(2).cor./qemres0.qcorl);
%rwithcor=atsetfieldvalues(rwithcor,qemres0.skewidx,'PolynomA',{1,2},rmmeas0(2).skewcor./qemres0.skewl);

save('LatticeAT.mat','ring','rwithcor'); %ring with initialized fields for errors and offsets.
pathtolatticefile='LatticeAT.mat';

% dymea=semres0.frespz(bpmindex,:)*(-rp.alphac);
% [l,t,ch]=atlinopt(rwithcor,0,indBPM);
% Dy=arrayfun(@(a)a.Dispersion(3),l);
% std2(Dy-dymea')


QuadKn.index=indQuadsDeriv;
QuadKn.perturbation={'PolynomB',[1,2]}; 
QuadKn.delta=0;
QuadKn.dk=1e-4;

% sext gradient error
SextKn.index=indSCor;
SextKn.perturbation={'PolynomB',[1,3]}; 
SextKn.delta=0;
SextKn.dk=1e-4;

% quad gradient error skew
QuadKs.index=indQuadsDeriv;
QuadKs.perturbation={'PolynomA',[1,2]}; 
QuadKs.delta=0;
QuadKs.dk=1e-4;

QuadRot.index=indQuadsDeriv;
QuadRot.perturbation=@(rr,ind,val)atsettilt(rr,ind,val); % Dipole rotation psiPAds(ipads).delta=0;
QuadRot.delta=0;
QuadRot.dk=1e-4;

%
% sext gradient error skew
SextKs.index=indSCor; 
SextKs.perturbation={'PolynomA',[1,3]}; 
SextKs.delta=0;
SextKs.dk=1e-5;

% alignment errors
QuadDx.index=indQuadsDeriv;
QuadDx.perturbation=@(rr,ind,val)setANYshift(rr,ind,1,val); % horizontal alignment error
QuadDx.delta=0;
QuadDx.dk=1e-6;

QuadDy.index=indQuadsDeriv;
QuadDy.perturbation=@(rr,ind,val)setANYshift(rr,ind,3,val); % vertical alignment error
QuadDy.delta=0;
QuadDy.dk=1e-6;

SextDx.index=indSextDeriv;
SextDx.perturbation=@(rr,ind,val)setANYshift(rr,ind,1,val); % horizontal alignment error
SextDx.delta=0;
SextDx.dk=1e-6;

SextDy.index=indSextDeriv;
SextDy.perturbation=@(rr,ind,val)setANYshift(rr,ind,3,val); % vertical alignment error
SextDy.delta=0;
SextDy.dk=1e-6;

% dipole angle
DipAng.index=indDip;
DipAng.perturbation={'BendingAngle',[1,1]}; 
DipAng.delta=0;
DipAng.dk=1e-4;

% dipole errors hor
DipH.index=indDip;
DipH.perturbation={'PolynomB',[1,1]}; 
DipH.delta=0;
DipH.dk=1e-4;

% dipole errors ver
DipV.index=indDip;
DipV.perturbation={'PolynomA',[1,1]}; 
DipV.delta=0;
DipV.dk=1e-4;
 
% dipole rotations
DipRot.index=indDip;
DipRot.perturbation=@(rr,ind,val)atsettilt(rr,ind,val); % Dipole rotation psiPAds(ipads).delta=0;
DipRot.delta=0;
DipRot.dk=1e-4;

% bpm offsets
BPMOffX.index=indBPM;
BPMOffX.perturbation=@(rr,ind,val)atsetfieldvalues(rr,ind,'Offset',{1,1},val); % Dipole rotation psiPAds(ipads).delta=0;
BPMOffX.delta=0;
BPMOffX.dk=1e-5;

BPMOffY.index=indBPM;
BPMOffY.perturbation=@(rr,ind,val)atsetfieldvalues(rr,ind,'Offset',{1,2},val); % Dipole rotation psiPAds(ipads).delta=0;
BPMOffY.delta=0;
BPMOffY.dk=1e-5;

% bpm offsets
HCor.index=indHCor;
HCor.perturbation={'PolynomB',[1,1]}; % H correctors
HCor.delta=0;
HCor.dk=1e-5;

VCor.index=indVCor;
VCor.perturbation={'PolynomA',[1,1]}; % V correctors
VCor.delta=0;
VCor.dk=1e-5;


%% define response matrix function (or anything else) to be fitted
%rmvec=[rm(:);10*Dx';100*Dy';t'];
warning('any modification to getfullrespmatrixvectorintegrated must be complied together with the GenericRM derivative to be accessible to the cluster');

rmfunctonen =@(r,ib,ih,iv,~,txt)getfullrespmatrixvectorintegrated(r,ib,ih,iv,0.0,txt,'fulldisp',W);

rmfunctonenskew =@(r,ib,ih,iv,~,txt)getfullrespmatrixvectorintegrated(r,ib,ih,iv,0.0,txt,'skewdisp',W);

offendelta=1e-3;
rmfunctoffen=@(r,ib,ih,iv,~,txt)getfullrespmatrixvectorintegrated(r,ib,ih,iv,offendelta,txt,'fulldisp',W);


%% run OAR cluster RM derivative
tic;
RMFoldQOnEn=RunRMGenericDerivArray(...
    pathtolatticefile,...
    'rwithcor',...'ring',...
    100,...
    [QuadKn],... 
    indhsel,...
    indvsel,...
    indbsel,...
    rmfunctonen,...
    [speclab 'Qkn_OnEn']);            % dpp


disp('Waiting for all files to be appropriately saved');
pause(10);

CollectRMGenericDerivOARData(RMFoldQOnEn,fullfile(RMFoldQOnEn,['../' speclab 'Qkn_OnEn.mat']));
%rmdir(RMFoldQOnEn);
%rmdir(RMFoldQsOnEn);
%rmdir(RMFoldDOnEn);
toc;

%%
RMtheo = rmfunctonen(rwithcor,indbsel,indhsel,indvsel,'','theo');

rs= sim.errcormodel;
indbs = find( atgetcells(rs,'Class','Monitor'))';
indhsim = find(atgetcells(rs,'FamName','S[HDFIJ]\w*'))';
rs = atsetfieldvalues(rs,indhsim,'PolynomA',{1,2},0); % skew correctors at zero

indqsim = find(atgetcells(rs,'FamName','Q[DFIJ]\w*'))';
Kcor = sim.cortab.KL1n./sim.cortab.Lengths;
Kn = atgetfieldvalues(rs,indqsim,'PolynomB',{1,2}); 
rs = atsetfieldvalues(rs,indqsim,'PolynomB',{1,2},Kn-Kcor(indqsim)); % skew correctors at zero
RMsimulator = rmfunctonen(rs,indbs,indhsim(1:9:end),indhsim(1:9:end),'','simulator'); % should be identical to the measured RM

%% fit
close all

disp('fitting RM (please wait)...')
%[~,rfit,DK1,residualRM,residualRM0]=GenericRMFit(RMmeasured,ring,[speclab
%'OnEn.mat'],450,3); % fit modello senza correttori

[~,rfit,DK1q,residualRM,residualRM0]=GenericRMFit(RMmeasured,rwithcor,[speclab 'Qkn_OnEn.mat'],50,2,1); % fit modello senza correttori

save('LatticeATfitQn.mat','ring','rfit'); %ring with initialized fields for errors and offsets.
pathtolatticefileQn='LatticeATfitQn.mat';

RMFoldQsOnEn=RunRMGenericDerivArray(...
    pathtolatticefileQn,...
    'rfit',...'ring',...
    100,...
    [QuadKs QuadRot],...],... 
    indhsel,...
    indvsel,...
    indbsel,...
    rmfunctonen,...
    [speclab 'Qs_OnEn']);            % dpp

CollectRMGenericDerivOARData(RMFoldQsOnEn,fullfile(RMFoldQsOnEn,['../' speclab 'Qs_OnEn.mat']));

[~,rfit,DK1qs,residualRM,residualRM0]=GenericRMFit(RMmeasured,rfit,[speclab 'Qs_OnEn.mat'],100,2,1); % fit modello senza correttori


save('LatticeATfitQs.mat','ring','rfit'); %ring with initialized fields for errors and offsets.
pathtolatticefileQs='LatticeATfitQs.mat';

RMFoldQOnEn=RunRMGenericDerivArray(...
    pathtolatticefileQs,...
    'rfit',...'ring',...
    100,...
    [QuadKn],... 
    indhsel,...
    indvsel,...
    indbsel,...
    rmfunctonen,...
    [speclab 'Qkn_OnEn']);            % dpp


disp('Waiting for all files to be appropriately saved');
pause(10);

CollectRMGenericDerivOARData(RMFoldQOnEn,fullfile(RMFoldQOnEn,['../' speclab 'Qkn_OnEn.mat']));


[~,rfit,DK1q,residualRM,residualRM0]=GenericRMFit(RMmeasured,rfit,[speclab 'Qkn_OnEn.mat'],200,2,1); % fit modello senza correttori
%[~,rfit,DK1qs,residualRM,residualRM0]=GenericRMFit(RMmeasured,rfit,[speclab 'Qs_OnEn.mat'],100,2,1); % fit modello senza correttori


save('LatticeATfitD.mat','ring','rfit'); %ring with initialized fields for errors and offsets.
pathtolatticefileD='LatticeATfitD.mat';

RMFoldDOnEn=RunRMGenericDerivArray(...
    pathtolatticefileD,...
    'rfit',...'ring',...
    100,...
    [DipAng DipRot],...],... 
    indhsel,...
    indvsel,...
    indbsel,...
    rmfunctonen,...
    [speclab 'D_OnEn']);            % dpp

CollectRMGenericDerivOARData(RMFoldDOnEn,fullfile(RMFoldDOnEn,['../' speclab 'D_OnEn.mat']));



[~,rfit,DK1d,residualRM,residualRM0]=GenericRMFit(RMmeasured,rfit,[speclab 'D_OnEn.mat'],100,2,1); % fit modello senza correttori
%[~,rfit,DK1d,residualRM,residualRM0]=GenericRMFit(RMmeasured,rfit,[speclab 'D_OnEn.mat'],350,2,1); % fit modello senza correttori

DK1 = [DK1q;DK1d];
disp('finished fit of measured RM')

% plot present difference
RMfit = rmfunctonen(rfit,indbsel,indhsel,indvsel,'','fit');
figure;  
plot(RMmeasured(:)); hold on; plot(RMtheo(:)); hold on; plot(RMfit(:)); 
legend('measured','theo','fit','simulator');

figure;  
plot(RMmeasured(:)-RMtheo(:)); hold on; plot(RMfit(:)-RMtheo(:)); 
legend('delta measured','delta fit');


%% display residual
nbpmrm=length(residualRM0(1:end-2))/((length(indvsel)+length(indhsel))+2); % n bpm not nan in rm

rm0=reshape(residualRM0(1:length(indbsel)*2*(length(indvsel)+length(indhsel))),[],length(indhsel)+length(indvsel));

rmdiag0=[rm0(1:size(rm0,1)/2,1:size(rm0,2)/2);rm0((size(rm0,1)/2+1):end,(size(rm0,2)/2+1):end)];
rmcoup0=[rm0(1:size(rm0,1)/2,(size(rm0,2)/2+1):end);rm0((size(rm0,1)/2+1):end,1:(size(rm0,2)/2))];

%dh0=RMmeasured((end-size(rm,2)/2*4+1):(end-size(rm,2)/2*3));
%dv0=RMmeasured((end-size(rm,2)/2*3+1):(end-size(rm,2)/2*2));
dh0=residualRM0((end-size(rm0,1)/2*2+1-2):(end-size(rm0,1)/2*1-2));
dv0=residualRM0((end-size(rm0,1)/2*1+1-2):(end-size(rm0,1)/2*0-2));

rm=reshape(residualRM(1:(end-length(indbsel)*2-2)),length(indbsel)*2,[]);
rmdiag=[rm(1:size(rm,1)/2,1:size(rm,2)/2);rm((size(rm,1)/2+1):end,(size(rm,2)/2+1):end)];
rmcoup=[rm(1:size(rm,1)/2,(size(rm,2)/2+1):end);rm((size(rm,1)/2+1):end,1:(size(rm,2)/2))];

%dh=residualRM((end-length(indbsel)*4-2+1):(end-length(indbsel)*3-2));
%dv=residualRM((end-length(indbsel)*3-2+1):(end-length(indbsel)*2-2));
dh=residualRM((end-length(indbsel)*2-2+1):(end-length(indbsel)*1-2));
dv=residualRM((end-length(indbsel)*1-2+1):(end-length(indbsel)*0-2));

disp(['rm diag: ' num2str(std2(rmdiag0(:))) ' -> ' num2str(std2(rmdiag(:)))]);
disp(['rm coup: ' num2str(std2(rmcoup0(:))) ' -> ' num2str(std2(rmcoup(:)))]);
disp(['disp h: '  num2str(std2(dh0/10)) ' -> ' num2str(std2(dh/10))]);
disp(['disp v: '  num2str(std2(dv0/100)) ' -> ' num2str(std2(dv/100))]);
%disp(['orb h: '   num2str(std(oh0(~isnan(oh0))/1000)) ' -> ' num2str(std(oh(~isnan(oh))/1000))]);
%disp(['orb v: '   num2str(std(ov0(~isnan(ov0))/1000)) ' -> ' num2str(std(ov(~isnan(ov))/1000))]);

[~,t0,c0]=atlinopt(rwithcor,0,1);
[~,te,ce]=atlinopt(rerr,0,1);
[~,tf,cf]=atlinopt(rfit,0,1);
disp(['tune: ' num2str(t0) ' -> ' num2str(tf)]);
disp(['chrom: ' num2str(c0) ' -> ' num2str(cf)]);


%% display fit results
%close all
DK=DK1;


LD=atgetfieldvalues(r0err,qemres0.dipidx,'Length');
LQ=atgetfieldvalues(r0err,qemres0.qpidx,'Length');
LC=atgetfieldvalues(r0err,qemres0.steerhidx,'Length');


figure('units','normalized','position',[0.1 0.3 0.8 0.5]);

plot(DK)
yl=get(gca,'YLim');
text(length(indQuadsDeriv)/2,yl(2)*0.8,'Quad Kn');
text(length(indQuadsDeriv)+length(indQuadsDeriv)/2,yl(2)*0.8,'Quad Ks');
text(2*length(indQuadsDeriv)+length(indDip)/2,yl(2)*0.8,'Diple Angle');
text(2*length(indQuadsDeriv)+length(indDip)+length(indDip)/2,yl(2)*0.8,'Diple Rot');
  
 title('fitted params');
 saveas(gca,['FIT_' speclab '.png']);

% compare results

%d0=getlatticedata(r0,r0,indBPM,indHCor,indVCor,indQuadsDeriv,indSCor);
df=getlatticedata(r0,rfit,indBPM,indHCor,indVCor,indQuadsDeriv,indSCor);
de=getlatticedata(r0err,rerr,indBPMe,indHCore,indVCore,indQuadsDerive,indSCore);
%dc=getlatticedata(r0,rcor,indBPM,indHCor,indVCor,indQuadsDeriv,indSCor);
indbpm=indBPM;
qc=indQuadsDeriv;


figure;
plot(indbpm,df.bx,'gs-.',indbpm,de.bx,'r.:');
legend('rfit','rctrm');title('\Delta\beta_{x}/\beta_{x}');
saveas(gca,['betax_' speclab '.png']);
figure;
plot(indbpm,df.by,'gs-.',indbpm,de.by,'r.:');
legend('rfit','rctrm');title('\Delta\beta_{y}/\beta_{y}');
saveas(gca,['betay_' speclab '.png']);

figure;
[l,t,c]=atlinopt(r0err,0,indBPMe);
DX0=arrayfun(@(a)a.Dispersion(1),l);
DY0=arrayfun(@(a)a.Dispersion(3),l);
plot(indbpm,qemres0.frespx(:,:)*(-rp.alphac)-DX0','k-o',indbpm,df.dx,'gs-.',indbpm,de.dx,'r.:');
legend('measured-model','rfit','rctrm');title('\Delta\eta_{x}');
saveas(gca,['dispx_' speclab '.png']);
figure;
plot(indbpm,semres0.frespz(:,:)*(-rp.alphac)-DY0','k-o',indbpm,df.dy,'gs-.',indbpm,de.dy,'r.:');
legend('measured-model','rfit','rctrm');title('\Delta\eta_{y}');
saveas(gca,['dispy_' speclab '.png']);
% figure;
% plot(indbpm,orbit0.horbit,'k-o',indbpm,df.ox,'gs-.',indbpm,de.ox,'r.:');
% legend('measured-model','rfit','rctrm');title('\Delta x ');
% saveas(gca,['orbx_' speclab '.png']);
% figure;
% plot(indbpm,orbit0.vorbit,'k-o',indbpm,df.oy,'gs-.',indbpm,de.oy,'r.:');
% legend('measured-model','rfit','rctrm');title('\Delta y');
% saveas(gca,['orby_' speclab '.png']);

figure;
plot(indbpm,df.Wx,'gs-.',indbpm,de.Wx,'r.:');
legend('rfit','rctrm');title('\Delta W_{x} ');
saveas(gca,['Wx_' speclab '.png']);
figure;
plot(indbpm,df.Wy,'gs-.',indbpm,de.Wy,'r.:');
legend('rfit','rctrm');title('\Delta W_{y}');
saveas(gca,['Wy_' speclab '.png']);
figure;
plot(indbpm,df.DxDp,'gs-.',indbpm,de.DxDp,'r.:');
legend('rfit','rctrm');title('\Delta \eta_{x}/\delta');
saveas(gca,['DxDp_' speclab '.png']);

disp('tunes')
disp(['ctrm tune: ' num2str(de.tune)]);
disp([' fit tune: ' num2str(df.tune)]);
disp('chrom')
disp(['ctrm chrom: ' num2str(de.chrom)]);
disp([' fit chrom: ' num2str(df.chrom)]);


%% correct lattice 
% correct lattice 

indqcor=qemres0.qcoridx;
indscor=qemres0.skewidx;

rcor=RDTquadcor(ring,rfit,indBPM,indqcor,0.6);
 saveas(gca,['HorBetaDispRDTCor_' speclab '.png']);

rcor=RDTskewcor(ring,rcor,indBPM,indscor,0.3);
 saveas(gca,['VerBetaDispRDTCor_' speclab '.png']);

%% set currents in SR. 

kse=cellfun(@(a)a.PolynomA(2)*a.Length,rerr(indscor)); % get previous K
ksc=cellfun(@(a)a.PolynomA(2)*a.Length,rcor(indscor)); % get new values
kne=cellfun(@(a)a.PolynomB(2)*a.Length,rerr(indqcor)); % get previous K
knc=cellfun(@(a)a.PolynomB(2)*a.Length,rcor(indqcor)); % get new values

figure;
plot(indscor,kse,'k.-',indscor,ksc,'r.-');
legend('rinit','rcor');title('K1s*L*Brho');
saveas(gca,['K1SCor_' speclab '.png']);
figure;
plot(indqcor,knc,'r.-');
legend('rcor');title('K1n*L*Brho');
saveas(gca,['K1NCor_' speclab '.png']);

%save_correction(['newQuadCor' speclab '.dat'],6,knc); % 32 normal quad correctors
%save_correction(['newSkewCor' speclab '.dat'],10,ksc);% 64 skew quad correctors


currentDKN=knc./reshape(load_corcalib('sr',6),size(knc));
currentDKS=ksc./reshape(load_corcalib('sr',10),size(ksc));

save([ speclab '_correction.mat'],'currentDKN','currentDKS');
%copyfile(['' speclab '_correction.mat'],'/mntdirect/_tmp_14_days/liuzzo/');


% % get existing currents and add correction
% SaveCurrents(['initKS' speclab ],'skewcorlatord');
% SaveCurrents(['initKN' speclab ],'quadcor');
% 
% % modify currents for correction
% load(['initKS' speclab ],'currents','devgroup');
% currents=currents+currentDKS;
% save(['corKS' speclab ],'currents','devgroup');
% 
% load(['initKN' speclab ],'currents','devgroup');
% currents=currents+currentDKN;
% save(['corKN' speclab ],'currents','devgroup');
% 
% % load currents to the accelerator
% LoadCurrents(['corKS' speclab ]);
% LoadCurrents(['corKN' speclab ]);

%% commands to send currents from sempanel
% bindir=[getenv('MACH_HOME') '/bin/' getenv('MACHARCH') '/'];
% corfile=fullfile(qemres.datadir, 'skewnew.dat');
% [s,w]=unix([bindir 'dvset <' corfile]); %#ok<ASGLU>
% disp(w);
% [s,w]=unix([bindir 'newfile -f mag_cor ' ...
%     fullfile(APPHOME,'sr','optics','settings','theory','base_skew2') ' <' ...
%     corfile]); %#ok<ASGLU>
% disp(w);

save([speclab '_DATA'])




cd(curdir)

