% disable DQ quadrupoles and steerers

nq = tango.Device('srmag/hst/all');

m = nq.CorrectorNames.read;
st = nq.CorrectorStates.read;
st(1:end)='On';
st([5:12:end,7:12:end,8:12:end])='Disabled';

for im = 1:length(m)
    q = tango.Device(m{im});
    if st(im)=='Disabled'
        q.Frozen = true;
    else
        q.Frozen = false;
    end
   % q.Init();
end

nq = tango.Device('srmag/m-q/all');

m = nq.MagnetNames.read;
st = nq.MagnetStates.read;
st(1:end)='On';
st([8:19:end,10:19:end,12:19:end])='Disabled';

for im = 1:length(m)
    q = tango.Device(m{im});
    if st(im)=='Disabled'
        q.Frozen = true;
    else
        q.Frozen = false;
    end
   % q.Init();
end
