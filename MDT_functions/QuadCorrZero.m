% quadrupole and skew quadrupole correction to zero

attr = tango.Attribute('srmag/m-q/all/CorrectionStrengths');
attr.set = zeros(size(attr.value)); 

attr = tango.Attribute('srmag/sqp/all/CorrectionStrengths');
attr.set = zeros(size(attr.value)); 

