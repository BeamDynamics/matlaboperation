% init all quadrupoles

nq = tango.Device('srmag/m-q/all');

m = nq.MagnetNames.read;

for im = 1:length(m)
    q = tango.Device(m{im});
   % q.On();
    q.Init();
end