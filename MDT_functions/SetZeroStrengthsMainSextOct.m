
sxt = tango.Device('srmag/m-s/all');

sxt.CorrectionStrengths = [sxt.CorrectionStrengths.read*0]; % set to design strengths
S0 = sxt.Strengths.read;
sxt.CorrectionStrengths = [-S0*0.98]; % not exactly zero

oct = tango.Device('srmag/m-o/all');
pause(6);
oct.CorrectionStrengths = [oct.CorrectionStrengths.read*0]; % set to design strengths
pause(6);
OC0 = oct.Strengths.read;
oct.CorrectionStrengths = [-OC0*0.99]; % not exactly zero
pause(6);

