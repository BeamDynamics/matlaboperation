% init all h steerers
nq = tango.Device('srmag/hst/all');
nq.Strengths = nq.Strengths.read*0;

nq = tango.Device('srmag/vst/all');
nq.Strengths = nq.Strengths.read*0;

nq = tango.Device('srmag/sqp/all');
nq.CorrectionStrengths = nq.CorrectionStrengths.read*0;

nq = tango.Device('srmag/m-q/all');
nq.CorrectionStrengths = nq.CorrectionStrengths.read*0;

nq = tango.Device('srmag/m-s/all');
nq.CorrectionStrengths = nq.CorrectionStrengths.read*0;

nq = tango.Device('srmag/m-o/all');
nq.CorrectionStrengths = nq.CorrectionStrengths.read*0;
