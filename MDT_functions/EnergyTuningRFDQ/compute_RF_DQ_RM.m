function compute_RF_DQ_RM()
% computes the closed orbit response matrix for RF, DQ1, DQ2 
% 
% lattice loaded from sr.model
% 
%see also: 

atmod = sr.model('reduce',true);

r = atsetRFCavity(atmod.ring,6.5e6,0,992,0);

indBPM = atmod.get(0,'bpm')';

indDQ = atmod.get(0,'dq')';
indDQ1 = indDQ( [1:3:end,3:3:end] );
indDQ2 = indDQ( [2:3:end] );

indRF = find(atgetcells(r,'Frequency'));


%% set BPM offsets
orbitfun=@(a,b)findorbit6(a,b);

% get nominal Orbit and RF
F0 = r{indRF(1)}.Frequency;
o0 = orbitfun(r,indBPM);

% change RF
DRF = 10;
F1 = F0 + DRF;
rrf = atsetRFCavity(atmod.ring,6.5e6,0,992,DRF);
orf = orbitfun(rrf,indBPM);
rrf = atsetRFCavity(atmod.ring,6.5e6,0,992,-DRF);
orfm = orbitfun(rrf,indBPM);

% change DQ angle
T1 = 1e-4;
rdip = atsetfieldvalues(r,indDQ1,'PolynomB',{1,1},T1);
odq1 = orbitfun(rdip,indBPM);
rdip = atsetfieldvalues(r,indDQ1,'PolynomB',{1,1},-T1);
odq1m = orbitfun(rdip,indBPM);

rdip = atsetfieldvalues(r,indDQ2,'PolynomB',{1,1},T1);
odq2 = orbitfun(rdip,indBPM);
rdip = atsetfieldvalues(r,indDQ2,'PolynomB',{1,1},-T1);
odq2m = orbitfun(rdip,indBPM);

%%
figure('units','normalized','position',[0.3 0.3 0.6 0.4]); 
plot(orf(1,:)-o0(1,:),'LineWidth',3);
hold on;
plot(odq1(1,:)-o0(1,:),'LineWidth',3);
hold on;
plot(odq2(1,:)-o0(1,:),'LineWidth',3);
l = legend(['RF: ' num2str(F1-F0) ' Hz'],...
    ['DQ1: ' num2str(T1*r{indDQ1(1)}.Length) ' rad'],...
    ['DQ2: ' num2str(T1*r{indDQ2(1)}.Length) ' rad']);
l.FontSize = 14;
ax =gca;
ax.FontSize = 14;
xlabel('BPM #');
ylabel(' h - h0 [m]');
saveas(gca,'Response_RF_DQ12.fig')


respEn = [(orf(1,:)-orfm(1,:))/(2*DRF);...
    (odq1(1,:)-odq1m(1,:))/(2*T1);...
    (odq2(1,:)-odq2m(1,:))/(2*T1)];

save('RespRFDQ_Hz_1overM','respEn','DRF','T1');


end