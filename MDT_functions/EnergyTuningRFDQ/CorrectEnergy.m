
%load RM
load('RespRFDQ_Hz_1overM','respEn','DRF','T1');

% meas
ho = tango.Attribute('srdiag/bpm/all/All_SA_HPosition').value; 

cor = qemsvd(respEn',ho',3); 

%%
Brho = 6e9/PhysConstant.speed_of_light_in_vacuum.value;
LDQ1 = 1.0447;
LDQ2 = 0.82515;


DRF = cor(1); % Hz

D_DQ1 = cor(2)*LDQ1; % rad

D_DQ2 = cor(3)*LDQ2; % rad


% DQ calibration
I_m = 82;
I_c = 0;

DQ_IntFields = @(I_m,I_c)(([-46.19; -2.932] + [-6.3248 -0.5119; -0.4071 0.087] *[I_m; I_c]).*[1e-3; 1]./Brho); % [rad 1/m]


% response field/A
M(1,:) = DQ_IntFields(I_m+1 ,I_c   ) -DQ_IntFields(I_m,I_c);
M(2,:) = DQ_IntFields(I_m   ,I_c +1) -DQ_IntFields(I_m,I_c);

DI_DQ1 = M'\[D_DQ1;0.0];  % Ampere for requested field change

disp('DQ1 requested')
[D_DQ1;0.0]
disp('DQ1 obtained')
DQ_IntFields(I_m+DI_DQ1(1),I_c+DI_DQ1(2)) -DQ_IntFields(I_m,I_c)

DI_DQ2 = M'\[D_DQ2;0.0];  % Ampere for requested field change

disp('DQ1 requested')
[D_DQ2;0.0]
disp('DQ1 obtained')
DQ_IntFields(I_m+DI_DQ2(1),I_c+DI_DQ2(2)) -DQ_IntFields(I_m,I_c)


disp('Delta I [A] DQ1, I_m, I_cor')
DI_DQ1
disp('Delta I [A] DQ2, I_m, I_cor')
DI_DQ2

% set devices
disp('Apply with a minus sign to this devices:')
'srmag/ps-dq1/c01-b'
'srmag/ps-cor-dq1/c01-b'
'srmag/ps-dq1/c01-d'
'srmag/ps-cor-dq1/c01-d'
'srmag/ps-dq2/c01-c'
'srmag/ps-cor-dq2/c01-c'

