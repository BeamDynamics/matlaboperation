% resonance strengths to zero



nq = tango.Device('srmag/sqp/all');
nq.ResonanceStrengths = nq.CorrectionStrengths.read*0;

nq = tango.Device('srmag/m-q/all');
nq.ResonanceStrengths = nq.CorrectionStrengths.read*0;

nq = tango.Device('srmag/m-s/all');
nq.ResonanceStrengths = nq.CorrectionStrengths.read*0;

nq = tango.Device('srmag/m-o/all');
nq.ResonanceStrengths = nq.CorrectionStrengths.read*0;
