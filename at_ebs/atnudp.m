function varargout=atnudp(ring,dp)
%ATNUDP	computes tune shift with momentum
%
%[NUX,NUZ]=ATNUDP(RING,DPP)
%   Computes tunes for the specified momentum deviations
%
%ATNUDP(RING,DPP)
%   Plots the computed tunes

sizd=size(dp);
nd=prod(sizd);
tunes=NaN(nd,2);
orbit=NaN(4,length(dp));
[~,nbper]=atenergy(ring);
[lindata,fractune0]=atlinopt(ring,0,1:length(ring)+1);
tune0=nbper*lindata(end).mu/2/pi;
offset=round(tune0-nbper*fractune0);
for i=1:numel(dp)
    [lindata,fractune]=atlinopt(ring,dp(i),1);
    tunes(i,:)=nbper*fractune+offset;
    orbit(:,i)=lindata(1).ClosedOrbit;
end
if nargout > 0
    varargout={reshape(tunes(:,1),sizd),reshape(tunes(:,2),sizd),orbit};
else
    inttunes=floor(tune0);
    plot(dp',tunes-inttunes(ones(nd,1),:),'o-');
    legend('\nu_x','\nu_z');
    xlabel('\deltap/p');
    ylabel('\nu');
    grid on
end
