function CreateOpticsFilesSR(r,opticsname,varargin)
% CREATEOPTICSFUILES(r,opticsname,...), 
% given an AT lattice R creates the relevant files for
% operation in APPHOME/ebs/optics/settings/(OPTICSNAME)
% 
% optional input ('NAME',val,'Name2',val2,...)
% 'MeasuredResponse' : folder containing ouput of full RM measurement
%
% example:
% load /machfs/liuzzo/esrfupgradelatticefiles-code/S28D/MagneticLengths/AT/errors/WP_021_034__S28D_Z0p67_newErrFunct_1_seed1_.mat
% CreateOpticsFiles(ring_err,'Seed1MeasResp','MeasuredResponse','/machfs/liuzzo/EBS/Simulator/responsematrix/resp2')
% 
%see also: LoadOpticsConfiguration

p=inputParser;
addRequired(p,'r',@iscell);
addRequired(p,'opticsname',@ischar);
addOptional(p,'MeasuredResponse','',@ischar);

parse(p,r,opticsname,varargin{:});
r           = p.Results.r;
opticsname  = p.Results.opticsname;
rm          = p.Results.MeasuredResponse;

fulfold=fullfile(pwd,'sr','optics','settings',opticsname);

curdir=pwd;
mkdir(fulfold);
cd(fulfold);

% AT lattice
betamodel=r;
save('betamodel.mat','betamodel');

% save image of lattice optics
f=figure;
atplot(r);
saveas(gca,[opticsname '.jpg']);
close(f);

% functions below should take ebs.model as input instead of lattice r.

% % orbit response, theory always,
% ebs.autocor_model(r);

% measured if required
if ~isempty(rm)
sr.autocor_model(r,'exp',rm);
end

% resonance response
%sr.reson_model(r);

% tune response
sr.tune_model(r);

% chromaticity response
% ebs.chrom_model(r);

% % store family magnet strengths to file (or in the format for magnet application, to be loaded)
% strengths=[]; % family stregths in ebs.magname order
% ebs.save_mag(fullfile(dirname,'familymag.dat'),strengths);
    
cd(curdir);

end
