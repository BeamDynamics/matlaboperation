function atwriteacuonlinesimulator(r, filename)


% Octupoles are class octupole
indoct = find(atgetcells(r,'FamName','O\w*'))';
r = atsetfieldvalues(r,indoct,'Class','Octupole');

inddev = find( (atgetcells(r,'Device') | ...
                atgetcells(r,'Class','Bend') |...
                atgetcells(r,'Class','Marker') ) ...
                & ~(atgetcells(r,'FamName','BMHARD','W1','ml\w*','Rotated\w*')));
                
spos = findspos(r,inddev);
rr = r(inddev);


C = {};

for el = 1:length( inddev)
    if isfield(rr{el},'Device')
        C{el,1} = rr{el}.Device;
    else
        C{el,1} = rr{el}.FamName;
    end
    
    if ~isfield(rr{el},'Length')
        rr{el}.Length = 0.0;
    end
    
    C{el,2} = spos(el) + rr{el}.Length/2;
    C{el,3} = rr{el}.Length;
    
    C{el,4} = rr{el}.Class;
    
    if isfield(rr{el},'PolynomB')
        [~, ipbmax] = max(abs(rr{el}.PolynomB));
    
        C{el,5} = sign(rr{el}.PolynomB(ipbmax));
    else
        C{el,5} = 0;
    end
end

C = [{'# Name','S-pos','Length','Class','sign'};C];
writecell(C,filename,'Delimiter','comma');

return