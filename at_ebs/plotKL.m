function plotdata=plotKL(lindata,ring,~,varargin) 
%PLPOLYNOMBCOMP PlotBn coefficient with normalization
% PLPOLYNOMBCOMP default plotting function for ATPLOT
%
% Plots polynomB for ring and ring1

CoD=cat(2,lindata.ClosedOrbit);

PolynomBVal1=zeros(size(CoD(1,:)));
PolynomBVal2=zeros(size(CoD(1,:)));
PolynomBVal3=zeros(size(CoD(1,:)));
PolynomBVal4=zeros(size(CoD(1,:)));
L=zeros(size(CoD(1,:)));
ind=findcells(ring,'PolynomB');

L(ind)=getcellstruct(ring,'Length',ind);

PolynomBVal1(ind)=getcellstruct(ring,'PolynomB',ind,1,1)'.*L(ind);
PolynomBVal2(ind)=getcellstruct(ring,'PolynomB',ind,1,2)'.*L(ind);
PolynomBVal3(ind)=getcellstruct(ring,'PolynomB',ind,1,3)'.*L(ind);
PolynomBVal4(ind)=getcellstruct(ring,'PolynomB',ind,1,4)'.*L(ind);


plotdata(1).values=[PolynomBVal1' PolynomBVal2' ...];%...
                     PolynomBVal3' PolynomBVal4' ];
plotdata(1).labels={'dipole','quadrupole',...
                    'sextuople','octupole'};
plotdata(1).axislabel='KL';
% dispersion=cat(2,lindata.Dispersion)';
% plotdata(2).values=dispersion(:,3);
% plotdata(2).labels={'\eta_y'};
% plotdata(2).axislabel='vertical dispersion [m]';

end
