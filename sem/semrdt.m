function [f1,f2]=semrdt(mach,qemres,ks,skewcor)
%SEMRDT compute resonance driving terms at BPM locations
%
%[F1,F2]=SEMRDT(MACH,QEMRES,SEMRES,KS,SKEWCOR)
%

[resp1,resp2]=semrdtresp(mach,qemres.bpmidx,[qemres.qpidx qemres.skewidx]);
strength=[qemres.ql.*ks;skewcor];
f1=resp1*strength;
f2=resp2*strength;
