function [k,tilts,stdf,meanf]=sqcheck(qemres,ks,kn,diptilt,ax,largefig)
%SQCHECK		Analyzes quadrupole strength errors
%
%DKK=SQCHECK(KL,KL0)
%
dqmask=qemres.atmodel.select('dq');
quadmask=qemres.atmodel.select('qp');
errquadmask=qemres.errmask & quadmask;
errdqmask=qemres.errmask & dqmask;

nm=size(ks,2);
nq=size(ks,1);
try
    labels=sr.qpname(1:length(ks));
catch
    labels=num2cell(1:length(ks));
end
labels{nq+1} = 'diptilt';

k=zeros(size(ks));
tilts=zeros(size(ks));

stdf=NaN*ones(nq,nm);
meanf=NaN*ones(nq,nm);
for i=1:nm
    [k(:,i),tilts(:,i)]=semtilt(kn,ks(:,i));
    tiltf=tilts(:,i);
    stdf(:,i)=std(tiltf);
    meanf(:,i)=mean(tiltf);
end

qptilt=NaN(1,sum(quadmask));
dqtilt=NaN(1,sum(dqmask));
qptilt(errquadmask(quadmask))=tilts(errquadmask(qemres.errmask),end);
dqtilt(errdqmask(dqmask))=tilts(errdqmask(qemres.errmask),end);
% stda=std(tilts,1,1);
% stdf(nq+1,:)=std(diptilt);
% meanf(nq+1,:)=mean(diptilt);
% fprintf('%s:\t% .3e\n','all',stda(1));
% for i=1:nq+1
%     fprintf('%s:\t% .3e\t% .3e\n',labels{i},stdf(i,1),meanf(i,1));
% end

if nargin >= 4             % displays quad rotations
    
    
    if  largefig
        
        figure(31);
        subplot(2,1,1);     extfigax = gca;
%        plot(extfigax,tilts);
        
%         title(extfigax,['Magnet rotation errors: quad (' num2str(length(tilts)) ...
%             ') ']);
%         ylabel(extfigax,'\Theta');
%         extfigax.XLim=[0 length([tilts])];
%         grid(extfigax,'on');
%                 

plot(extfigax,mean(ks,2));
 
title(extfigax,['Magnet skew quad errors: (' num2str(length(tilts)) ...
            ') ']);
        ylabel(extfigax,'Ks');
        extfigax.XLim=[0 length([mean(ks,2)])];
        grid(extfigax,'on');
        
        subplot(2,1,2);     extfigax = gca;
        plot(extfigax,diptilt);
        
        title(extfigax,['Magnet rotation errors: dip (' num2str(length(diptilt)) ') ']);
        ylabel(extfigax,'\Theta');
        extfigax.XLim=[0 length([diptilt])];
        grid(extfigax,'on');
        %% set(ax,'Xlim',[0 nq+1],'XTickLabel',labels,'XTickLabelRotation',30);
        
    end
    
    stdquad=std(sr.fold(qptilt),1,2,'omitnan');
    stddq=std(sr.fold(dqtilt),1,2,'omitnan');
    bar(ax,[stdquad(1:8);stddq]);
    ylabel(ax,'tilt');
    grid(ax,'on');
    ax.XTickLabel={'QF1','QD2','QD3','QF4','QF4','QD5','QF6','QF8','DQ1','DQ2'};
    ax.XTickLabelRotation=90;
% 
%     Nc = length([diptilt]);
%     extracor = mod(Nc,32);
%     cor_per_cell_dip = squeeze(std(reshape(diptilt((1+extracor/2):(Nc-extracor/2)),[],32),1,1));
%     
%     Nc = length([ks]);
%     extracor = mod(Nc,32);
%     cor_per_cell_quad = squeeze(std(reshape(ks((1+extracor/2):(Nc-extracor/2)),[],32),1,1));
%     
%     % colormap default;
%     yyaxis(ax,'left');
%     bar(ax,[cor_per_cell_quad],0.9,'EdgeColor','none');
%     ylabel(ax,'Ks');
%     yyaxis(ax,'right');
%     bar(ax,[cor_per_cell_dip],0.5,'EdgeColor','none');
%     ax.XTick=[1:32];
%     ax.XTickLabel=arrayfun(@(a)num2str(a),[4:32,1:3],'un',0);
%     ax.XTickLabelRotation=90;
%     
%     legend(ax,'std quad','std dip');
%     grid(ax,'on');
%     set(ax,'Xlim',[0 32+1],'FontSize',8);
    
    
    
    
end
