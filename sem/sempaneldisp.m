function [qemb,semres]=sempaneldisp(qemres,semres,qemb,qemb0,handles)

if nargin < 5, handles=struct(); end

qemb.at=qemat(qemres,qemb,true);
qemb=qemoptics(qemres,qemb,qemb0);
sbpm=cat(1,qemb.lindata.SPos);

if isfield(handles,'axes1')
    sqcheck(qemres,qemb.ks,qemb.kn,qemb.diptilt,handles.axes1,get(handles.largefigure,'Value'));
else
    sqcheck(qemres,qemb.ks,qemb.kn,qemb.diptilt,[],get(handles.largefigure,'Value'));
end

mess={};
if (isfield(handles,'compare') && handles.compare) || ...
        (isfield(handles,'pushbutton13') && strcmpi(get(handles.pushbutton13,'Enable'),'Off'))
    if all(isfield(semres,{'resph','respv','frespz'}))
        residual=qem_residual(qemb,qemres,semres);
        semres.fithist=[semres.fithist;[residual.v2h,residual.h2v,residual.vdisp]];
        mess={['orbit residual = '  num2str(residual.residual)];...
            ['V disp. residual = ' num2str(residual.vdisp)]};
        qemplotresp(4,qemb.atrespv2h-semres.resph,qemb.atresph2v-semres.respv,'deviation');
    end
end


% PLOT EMITTANCE compared to measurement (see qemoptics.m and
% sr.pinholenames(1:5)
mess0=semdisp(qemb,qemres,semres);      % figures 1 and 2
mess=[mess;...
    sprintf('tunes H/V: %.4f/%.4f',qemb.pm.fractunes);...
    mess0];

if isfield(handles,'text3')             % displays values
    set(handles.text3,'String',mess,'FontSize',10);
else
    fprintf('%s\n',mess{:});
end

if isfield(handles,'axes4')             % displays corr. strengths
    
    if  get(handles.largefigure,'Value')
        figure(21);
        extfigax = gca;
        plot(extfigax,qemb.skewcor);
        legend(extfigax,'initial','corrected');
        title(extfigax,'Corrector strengths');
        grid(extfigax,'on');
    end
    % colormp default;
    Nc = length(qemb.skewcor);
    extracor = mod(Nc,32); % remove extra correctors from injection cells
    cor_per_cell_std = squeeze(std(reshape(...
        qemb.skewcor((1+extracor/2):(Nc-extracor/2),:),...
        [],32,size(qemb.skewcor,2)),1,1));
    % cor_per_cell_mean = squeeze(mean(reshape(qemb.cor((1+extracor/2):(Nc-extracor/2),:),32,[],2),2));
    %bar(handles.axes4,cor_per_cell_std,'EdgeColor','none');
    cla(handles.axes4);
    hold off;
    try
        wc= [0.9,0.5]; % superimpose ba
        yyaxis rigth
        bar(handles.axes4,cor_per_cell_std(:,1),wc(1),'EdgeColor','none');
        
        yyaxis left
        bar(handles.axes4,cor_per_cell_std(:,2),wc(2),'EdgeColor','none');
        yyaxis rigth
        
    catch
        bar(handles.axes4,cor_per_cell_std,0.9,'EdgeColor','none');
    end
    handles.axes4.XTick=1:32;
    handles.axes4.XTickLabel=arrayfun(@(a)num2str(a),[4:32,1:3],'un',0);
    handles.axes4.XTickLabelRotation=90;
    
    title(handles.axes4,'std Corrector strengths (per cell)');
    legend(handles.axes4,'initial','corrected');
    grid(handles.axes4,'on');
    
end

if isfield(handles,'axes2')             % displays vertical dispersion
    if isfield(semres,'frespz')
        plot(handles.axes2,sbpm,-qemb.pm.alpha*[semres.bvscale*semres.frespz qemb.frespz]);
        leg = {'measured','fit'};
    else
        plot(handles.axes2,sbpm,-qemb.pm.alpha*qemb.frespz);
        leg = {'measured'};
    end
    title(handles.axes2,'Vertical dispersion');
    ax=axis(handles.axes2);
    ym=max(abs(ax([3 4])));
    axis(handles.axes2,[0 844.39 -ym ym]);
    ylabel(handles.axes2,'\eta_z [m]');
    grid(handles.axes2,'on');
    legend(handles.axes2,leg);
end

if isfield(handles,'axes3')             % displays horizontal dispersion
    if isfield(qemres,'frespx')
        plot(handles.axes3,sbpm,-qemb.pm.alpha*[qemres.bhscale*qemres.frespx-qemb0.frespx qemb.frespx-qemb0.frespx]);
    else
        plot(handles.axes3,sbpm,-qemb.pm.alpha*(qemb.frespx-qemb0.frespx));
    end
    title(handles.axes3,'Horizontal dispersion');
    ax=axis(handles.axes3);
    ym=max(abs(ax([3 4])));
    axis(handles.axes3,[0 844.39 -ym ym]);
    ylabel(handles.axes3,'\eta_x [m]');
    grid(handles.axes3,'on');
end


% in sempaneldisp.m
figure(61);
yyaxis left
hold off;
plot(qemres.fithist(:,1),'bx-','LineWidth',3,'MarkerSize',10);
hold on;
plot(qemres.fithist(:,2),'ro-','LineWidth',3,'MarkerSize',10);
if ~isempty(semres.fithist)
    plot(semres.fithist(:,1),'cx-','LineWidth',3,'MarkerSize',10);
    plot(semres.fithist(:,2),'mo-','LineWidth',3,'MarkerSize',10);
end
xlabel('fit step #');
ylabel('std RM residual');
grid on;
yyaxis right 
hold off;
plot(qemres.fithist(:,3),'gs-','LineWidth',3,'MarkerSize',10);
if ~isempty(semres.fithist)
hold on;
plot(semres.fithist(:,3),'ys-','LineWidth',3,'MarkerSize',10);
end
ylabel('disp. residual');
legend('HH','VV','V2H','H2V','DH','DV');


if isfield(handles,'selplot')           % additional plot
    sempanelplot(5,handles.selplot,qemb0,qemb,qemres);
elseif isfield(handles,'popupmenu3')
    sempanelplot(5,get(handles.popupmenu3,'Value'),qemb0,qemb,qemres);
end
end
