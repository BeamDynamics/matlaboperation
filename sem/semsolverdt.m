function [cor,skewresponse]=semsolverdt(qemb,qemres,semres,cor0,dispweight,varargin)
%SEMRDT compute resonance driving terms at BPM locations
%
%SKEWCOR=SEMSOLVERDT(QEMB,QEMRES,SEMRES,SKEWCOR,DISPWEIGHT)
%

mach=qemat(qemres,qemb,true);
a2=mcf(mach)*dispweight;
if isfield(semres,'skewresponse')
    skewresponse=semres.skewresponse;
else
    % skewresponseN=semskewresp(qemb,qemres,semres,varargin{:});
    skewresponse=semskewrespAnalytic(qemb,qemres,semres,varargin{:});
    semres.skewresponse = skewresponse;
end

% quad  v-disp response
[S]=dDxyDskew(qemb.at(:),qemres.bpmidx,qemres.qpidx);
Lq=cellfun(@(a)a.Length(1),qemb.at(qemres.qpidx),'un',1)';
S=-S./mcf(qemb.at(:)).*repmat(Lq,length(qemres.bpmidx),1);
quadskewetaresp=S./qemres.ql(:,ones(1,size(S,1)))';

% RDT resp
[resp1,resp2]=semrdtresp(mach,qemres.bpmidx,[qemres.qpidx qemres.skewidx]);
strength=[qemres.ql.*qemb.ks;cor0];
f1=resp1*strength;
f2=resp2*strength;

nbpm=length(qemres.bpmidx);
if isfield(semres,'skewkeep')
    skmask=semres.skewkeep;
else
    skmask=true(size(qemres.skewidx));
end
orbitrange=size(skewresponse,1)-nbpm;
sk=[false(size(qemres.qpidx)) skmask];
a1=1-a2;
rsp=[...
    a1*real(resp1(:,sk));...
    a1*imag(resp1(:,sk));...
    a1*real(resp2(:,sk));...
    a1*imag(resp2(:,sk));...
    a2*skewresponse(orbitrange+(1:nbpm),skmask)];
b=[...
    a1*real(f1);...
    a1*imag(f1);...
    a1*real(f2);...
    a1*imag(f2);...
    a2*qemb.frespz];

cor=cor0;
% cor(skmask)=cor(skmask)-rsp\b;
cor(skmask)=cor(skmask)-qemsvd_mod(rsp,b,semres.neigQuadRDTcor,true);

strength=[qemres.ql.*qemb.ks;cor]; % expecxted quantities after correction

fxc=resp1*strength;
fzc=resp2*strength;
%dfrhc = [quadskewetaresp,skewresponse(orbitrange+(1:nbpm),skmask)] *strength;

disp(['f1001: ' num2str(std(f1)) ' - > ' num2str(std(fxc))]);
disp(['f1010: ' num2str(std(f2)) ' - > ' num2str(std(fzc))]);
%disp(['dv: ' num2str(std(qemb.frespz)) ' - > ' num2str(std(dfrhc))]);

end