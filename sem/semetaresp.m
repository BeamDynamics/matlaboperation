function r=semetaresp(varargin)

dCT=1.0e-8;
[r1,dpp1]=semrotresp(dCT,varargin{:});
[r0,dpp0]=semrotresp(  0,varargin{:});
norm=1/(dpp1-dpp0);
r=(r1-r0).*norm;

function [r,dpp]=semrotresp(dCT,func,bpmidx,rotidx,val,coord)

if nargin < 6, coord=3; end
norm=1/val;
nk=length(bpmidx);
orbit=findsyncorbit(func([],val),dCT,bpmidx);
dpp=orbit(5);
%ref=reshape(orbit(1:4,:)',nk,1);
ref=orbit(coord,:)';
r=zeros(nk,length(rotidx));
for i=1:length(rotidx)
    orbit=findsyncorbit(func(rotidx(i),val),dCT,bpmidx);
%   r(:,i)=(reshape(orbit(1:4,:)',nk,1)-ref)*norm;
    r(:,i)=(orbit(coord,:)'-ref).*norm;
end
