function deltaset=semerrfit(nhst,nvst,resp,dresp,mode,okbpm,ploteig)
%QEMPANELFIT Vary lattice parameters to fit the measured response matrix
%DELTAV=QEMPANELFIT(NHST,NVST,RESP,DRESP,MODE,OKVAR,OKBPM)
%
%NHST:      Number of horizontal kicks
%NHST:      Number of vertical kicks
%RESP:      Deviation of measured reponse matrices from the model
%DRESP:     Derivatives of response matrices vs. variable elements
%MODE:      Structure allowing control of SVD inversion. (default:)
%OKVAR:     Select a subset of variable parameters (default: all)
%OKBPM:     Select a subset of valid BPMS (default:all)
%
%DELTAV:    Variation of selected parameters

[no,nq]=size(dresp);
nbpm=no/(nhst+nvst+1);
orbitrange=nbpm*(nhst+nvst);
if nargin<7, ploteig=true; end
if nargin<6, okbpm=true(nbpm,1); end
if nargin<5 || isempty(mode), mode=struct; end
if ~isfield(mode,'nsets'), mode.nsets=4; end
if ~isfield(mode,'neigs'), mode.neigs=100; end
if ~isfield(mode,'dispweight'), mode.dispweight=0; end
if ~isfield(mode,'vnorm')
     
    mode.vnorm=ones(1,size(dresp,2));%1./std(dresp(1:orbitrange,:),1,1);
    
end
inisel=false(size(resp));
if mode.dispweight>0, inisel(orbitrange+(1:nbpm))=true; end  % Keep dispersion
bok=repmat(okbpm,nhst+nvst+1,1) & isfinite(resp);
v=mode.vnorm;
w=[ones(orbitrange,1);mode.dispweight*ones(nbpm,1)];
try 
    hsets=reshape(1:nhst,mode.nsets,[]);
    vsets=reshape(1:nvst,[],mode.nsets)';
catch
    warning('Error dividing in subsets')
    disp([ 'NHst/Nsets = ' num2str(nhst) ' / ' num2str(mode.nsets) ' = ' num2str(nhst/mode.nsets) '<-should be integer'])
    disp([ 'NVst/Nsets = ' num2str(nvst) ' / ' num2str(mode.nsets) ' = ' num2str(nvst/mode.nsets) '<-should be integer'])
    disp('setting nsets = 1');
    mode.nsets = 1;
    hsets=reshape(1:nhst,mode.nsets,[]);
    vsets=reshape(1:nvst,mode.nsets,[]);
end
% hsets=reshape(1:nhst,[],mode.nsets)';
% vsets=reshape(1:nvst,mode.nsets,[]);
% hsets = 7:11;
% vsets = 7:11;

% remove correctors with NaN (DQ missing in vertical plane for example)
resp2d = reshape(resp,nbpm,[]);
corok = find(~isnan(resp2d(1,1:nhst)));

sk=semsolvex(dresp.*(w*v),resp.*w,bok,hsets,vsets,mode.neigs);
deltaset=mean(sk,2).*v';
devdev=std(sk,1,2).*v';
fprintf('Deviation between sets: %g\n',sqrt(mean(devdev.*devdev)));
nbpm = length(okbpm);
 
    function sel=lselected(sh,sv)
        sel=inisel;
        sel(semselect(sh,sv,nbpm,nhst))=true;
    end

    function sk = semsolvex(dresp,resp,ok,hsets,vsets,neig)
        [nl,nc] = size(dresp);
        ngs = min([nl nc neig]);
        [nsets, nh] = size(hsets); %#ok<ASGLU>
        [nsets, nv] = size(vsets);
        fprintf(['Solving %i sets(%i H steerers, %i V steerers) with %i' ...
            ' Eigen vectors\n'],nsets,nh,nv,ngs);
        sk=zeros(nc,nsets);
        for i=1:nsets
            % logical mask of correctors to be use and ok in the RM (not
            % NaN) 
            hsetsok = ismember(corok,hsets(i,:));
            vsetsok = ismember(corok,vsets(i,:));
            
            lok=lselected(corok(hsetsok),corok(vsetsok)) & ok;
            sk(:,i)=qemsvd_mod(dresp(lok,:),resp(lok),ngs,ploteig);
        end
    end
end
