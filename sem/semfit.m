function [qave,qstab]=semfit(neig,dirname,steerlist)
%[sqmean,sqrms]=SEMFIT(neigen,dirname,steerlist)	Average fit for a list of steerers
%
%SEMFIT:
% - reads responses from $DIRNAME/steer[HV]xx
% - returns the average of fitted strengths
%
% NEIGEN : number of Eigen vectors (default 0)
% DIRNAME : directory of response matrix files (default $APPHOME/sem/skewresp)
% STEERLIST : list of steerer numbers (default 1:12:96)
%
% SQMEAN : fitted quadrupole strengths
% SQRMS : rms fluctuation of skew quad strengths for different steerers
%
%See also SEMDATA, SEMSOLVE
%
if nargin < 3, steerlist=1:12:96; end	% first steerer of each pair
if nargin < 2, dirname=[]; end
if nargin < 1, neig=0; end

q=zeros(320,length(steerlist));
count=0;
for steerer1=steerlist
   steerer2=steerer1+6;
   rh=getorbit(dirname,steerer1);
   rv=getorbit(dirname,steerer2);
   try
      disp(['Steerer ' int2str([steerer1 steerer2]) ': Processing.']);
      [qq,rms]=semsolve(neig,steerer1,rh,steerer2,rv);
      count=count+1;
      q(:,count)=qq;
      disp(['Steerer ' int2str([steerer1 steerer2]) ': fit ' num2str(rms)]);
   catch
      [err,errid]=lasterr;
      disp(err);
   end
end
qave=mean(q(:,1:count),2);
qstab=std(q(:,1:count),1,2);
%qstab=sqrt(sum(var(q',1)));

function resp=getorbit(dir,steerer)
for wait=1:200
   try
      resp=semdata(dir,steerer);
      break;
   catch
      disp(['Steerer ' int2str(steerer) ': Waiting for input file.']);
      pause(4);
   end
end
