function skewresponse = semskewresp(qemb,qemres,semres,dispfunc)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if nargin>=4
    vargs1={@(i,itot) nselect(2,dispfunc,i,5*itot)};
    vargs2={@(i,itot) nselect(4,dispfunc,itot+4*i,5*itot)};
else
    fprintf('\n          ');
    vargs1={@(i,itot) nselect(2,@(j,jtot) fprintf('\b\b\b\b\b\b\b\b\b%4d/%4d',[j,jtot]),i,3*itot)};
    vargs2={@(i,itot) nselect(4,@(j,jtot) fprintf('\b\b\b\b\b\b\b\b\b%4d/%4d',[j,jtot]),itot+2*i,3*itot)};
end

skewresp=semderiv(qemb.at(:),qemres.dpp,qemres.skewidx,qemres.bpmidx,...
    qemres.steerhidx(semres.hlist),qemres.steervidx(semres.vlist),vargs1{:});
[~,skewetaresp]=qemdispderiv(qemb.at(:),qemres.ct,@setskew,1.e-4,...
    qemres.skewidx,qemres.bpmidx,vargs2{:});
skewresponse=[skewresp;skewetaresp./qemres.skewl(:,ones(1,size(skewetaresp,1)))'];

    function elt=setskew(elt,dval)
        vini=elt.PolynomA(2);
        elt.PolynomA(2)=vini+dval;
    end
end
