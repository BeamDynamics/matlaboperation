function semres = semresponse(qemres,semres,qemb,dispfunc)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if nargin>=4
    vargs1={@(i,itot) nselect(16,dispfunc,i,5*itot)};
    vargs2={@(i,itot) nselect(16,dispfunc,itot+i,5*itot)};
    vargs3={@(i,itot) nselect(8,dispfunc,2*itot+2*i,5*itot)};
    vargs4={@(i,itot) nselect(8,dispfunc,4*itot+i,5*itot)};
else
    vargs1={};
    vargs2={};
    vargs3={};
    vargs4={};
end

shidx=qemres.steerhidx(semres.hlist);
svidx=qemres.steervidx(semres.vlist);
if ~isfield(semres,'quadresponse')
    quadresp=semderiv(qemb.at(:),qemres.dpp,qemres.qpidx,qemres.bpmidx,shidx,svidx,vargs1{:});
    [~,quadetaresp]=qemdispderiv(qemb.at(:),qemres.ct,@setskew,1.e-4,qemres.qpidx,...
        qemres.bpmidx,vargs3{:});
    lq=size(quadresp,1)+size(quadetaresp,1);
    semres.quadresponse=[quadresp;quadetaresp].*qemres.ql(:,ones(1,lq))';
end
if ~isfield(semres,'skewresponse')
    skewresp=semderiv(qemb.at(:),qemres.dpp,qemres.skewidx,qemres.bpmidx,shidx,svidx,vargs2{:});
    [~,skewetaresp]=qemdispderiv(qemb.at(:),qemres.ct,@setskew,1.e-4,qemres.skewidx,...
        qemres.bpmidx,vargs4{:});
    semres.skewresponse=[skewresp;...
        skewetaresp./qemres.skewl(:,ones(1,size(skewetaresp,1)))'];
end

    function elt=setskew(elt,dval)
        vini=elt.PolynomA(2);
        elt.PolynomA(2)=vini+dval;
    end

end
