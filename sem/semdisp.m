function mess=semdisp(qemb,qemres,semres)

nbpm=length(qemres.bpmidx);
alllist=1:nbpm;                 % all BPMS

% this order MUST be identical to the one in qemoptics.m and to
% sr.pinholenames(1:5);, the LATTICE order starting frommjnection (cell
% 04)
id07=nbpm+1;				% ID07
d09=nbpm+2;					% D09 pinhole
d17=nbpm+3;				    % D17 
id25=nbpm+4;				% ID25 
d27=nbpm+5;				    % D27
pinholelist =[id07 d09 d17 id25 d27 ] ; % this order should be identical to the one in qemoptics.m
pinholename={'ID07';'D09';'D17';'ID25';'D27'};

emittances=qemb.emittances;
sbpm=cat(1,qemb.lindata.SPos);
sskew=findspos(qemb.at(:),qemres.skewidx);
spinhole=[qemb.id07data.SPos; qemb.d09data.SPos; qemb.d17data.SPos; qemb.id25data.SPos; qemb.d27data.SPos];

mess={...
    sprintf('em. H [nm]: %7.3f %7.3f %7.3f',1.e9*mean(emittances(pinholelist,1:2:end)));...
    sprintf('em. V [pm]: %7.3f %7.3f %7.3f',1.e12*mean(emittances(pinholelist,2:2:end)));...
    sprintf('V. dispersion [m]: %g',qemb.pm.alpha*std(qemb.frespz,1));...
    'pinhole V [pm]:';...
    sprintf('ID07:%8.4f',1.e12*emittances(id07,6));...
    sprintf(' D09:%8.4f',1.e12*emittances( d09,6));...
    sprintf(' D17:%8.4f',1.e12*emittances( d17,6));...
    sprintf('ID25:%8.4f',1.e12*emittances(id25,6));...
    sprintf(' D27:%8.4f',1.e12*emittances( d27,6))};

figure(1234);	% emittances
plot(sbpm,1.e12*emittances(alllist,2:2:end));
hold on
plot(spinhole,1.e12*emittances(pinholelist,end),'ko','MarkerFaceColor','k');
hold off
ax=axis;
axis([0 844.39 0 max([ax(4);1.e-12])]);
plotskew(sskew);
xlabel('s [m]');
ylabel('\epsilon_z [pm]');
legend('Eigen emittance','projected emittance','measured emittance');
grid on

figure(2345) ;clf;  % Comparison model / iax
bar(1.e12*[semres.iaxemz qemb.emittances(pinholelist,6)]);
set(gca,'XTick',1:length(pinholename),'XTickLabel',pinholename);
legend('measurement','model');
grid on

function plotskew(sskew)
ns=size(sskew,2);
ylim=get(gca,'Ylim');
hold('on');
plot([sskew;sskew],repmat([0 0;-.1 .1]*ylim',1,ns),'k');
hold('off');
