function [rh,rv,dpp]=rotresp(mach,dCT,rotidx,bpmidx)

rot=1.e-3;
norm=1/rot;
orbit=findsyncorbit(mach,dCT,bpmidx);
dpp=orbit(5);
refh=orbit(1,:)*norm;
refv=orbit(3,:)*norm;
rh=zeros(length(bpmidx),length(rotidx));
rv=zeros(length(bpmidx),length(rotidx));
for i=1:length(rotidx)
    orbit=findsyncorbit(atsettilt(mach,rotidx(i),rot),dCT,bpmidx);
    rh(:,i)=orbit(1,:)*norm - refh;
    rv(:,i)=orbit(3,:)*norm - refv;
end
