function semdata=sempanelset(datadir,handles)
%SEMDATA=SEMPANELSET(DATADIR,HANDLES)
%
%DATADIR:	Data directory
%

errok=[handles.pushbutton10 handles.pushbutton11 handles.pushbutton16];		% zero, save, load
semdata.errdef=[handles.pushbutton10 handles.pushbutton16];		% zero, load
errno=[handles.pushbutton3 handles.pushbutton9];				% fitq, fitd
semdata.cordef=[handles.pushbutton13 handles.pushbutton14];		% initial, zero
corok=[handles.pushbutton14 handles.pushbutton12 handles.pushbutton15 ...
    handles.pushbutton17 handles.pushbutton20];					% zero, fit, save, load, retune
corno=handles.pushbutton13;							% initial
if exist(fullfile(datadir, 'skewnew.dat'),'file') == 2
    corok=[corok handles.pushbutton18];
else
    corno=[corno handles.pushbutton18];
end
set([errok corok],'Enable','On');
set([errno corno],'Enable','Off');
end
