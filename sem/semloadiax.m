function iaxemz = semloadiax(datadir)
%IAXEMZ=SEMLOADIAX(DATADIR) load IAX values

iaxemz=NaN(5,1);
try
    dest=[1:5];      % destination of the values read from file
    iax=load(fullfile(datadir, 'iaxemittance'));
    iaxemz(dest)=iax.iax_values; % [m]
catch
    warning('sem:noiax','Cannot read IAX data from the data directory');
end
end

