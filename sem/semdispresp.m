function semdispresp(resp,hlist,vlist)

nh=length(hlist);
nv=length(vlist);
for col=1:size(resp,2)
    orbvh(:,col)=std2(reshape(resp(:,col),224,nh+nv),1)';
end
subplot(2,1,1);
bar(orbvh(1:nh,:));
xlabel('H steerer');
ylabel('rms V response');
legend('Measured','Model');
subplot(2,1,2);
bar(orbvh(nh+(1:nv),:));
xlabel('V steerer');
ylabel('rms H response');
legend('Measured','Model');
