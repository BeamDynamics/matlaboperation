function varargout = sempanel2(varargin)
% SEMPANEL2 M-file for sempanel2.fig
%      SEMPANEL2, by itself, creates a new SEMPANEL2 or raises the existing
%      singleton*.
%
%      H = SEMPANEL2 returns the handle to a new SEMPANEL2 or the handle to
%      the existing singleton*.
%
%      SEMPANEL2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SEMPANEL2.M with the given input arguments.
%
%      SEMPANEL2('Property','Value',...) creates a new SEMPANEL2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before sempanel2_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to sempanel2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help sempanel2

% Last Modified by GUIDE v2.5 02-Oct-2019 12:44:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @sempanel2_OpeningFcn, ...
    'gui_OutputFcn',  @sempanel2_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before sempanel2 is made visible.
function sempanel2_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to sempanel2 (see VARARGIN)

% Choose default command line output for sempanel2
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes sempanel2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);
set(handles.popupmenu2,'Value',2);
set(handles.edit4,'String','0.4');
set([handles.text5 handles.edit4],'Visible','on');
handles.semdata=[];
handles.corfrac=1;
guidata(hObject,handles);


    
% --- Outputs from this function are returned to the command line.
function varargout = sempanel2_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1: select data directory
function pushbutton1_Callback(hObject, eventdata, handles) %#ok<*DEFNU>
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
%wrongbpms=sr.bpmindex(6,4);	% Eliminate BPM C6-4
semres.iaxemz=semloadiax(qemres.datadir);
handles.semdata=sempanelset(qemres.datadir,handles);

% A=textscan(fopen(fullfile(qemres.datadir,'skewcor.dat'),'r'),'%s %f','HeaderLines',7); %read from setting manager saved file
% qemb(2).skewcor=A{2}; %warning('zero initial skew quadrupoles')

% get value of 
semres.neigDipFit=str2double(handles.neigDipFit.String);
semres.neigQuadFit=str2double(handles.neigQuadFit.String);
semres.neigQuadRDTcor=str2double(handles.neigQuadRDTcor.String);

set(handles.pushbutton3,'Enable','On');
set(handles.pushbutton9,'Enable','On');
[qemb(2),semres]=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
set(handles.edit1,'String',qemres.datadir);
guidata(hObject,handles);
set(handles.statustext,'String','initialized working directory');


function edit1_Callback(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton3: Errors->Fit quads
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
set(handles.statustext,'String','fitting quadrupoles');
semres.wdisp=str2double(get(handles.DispFitWeigth,'String'));
if get(handles.AnalyticFitCheck,'Value') == 1
    hw=waitbar(0,'Fitting quadrupole rotations (Analytic)...');
else
    hw=waitbar(0,'Fitting quadrupole rotations...');
end
dispfunc=@(i,itot) waitbar(i/itot,hw);
okbpm=true(length(qemres.bpmidx),1);
okbpm(qemres.wrongbpms) = false;

if get(handles.AnalyticFitCheck,'Value') == 1
    [qemb(2).ks,semres]=sempanelfitqAnalytic(qemb(2),qemres,semres,qemres.qpfit,okbpm,dispfunc);
else
    [qemb(2).ks,semres]=sempanelfitq(qemb(2),qemres,semres,qemres.qpfit,okbpm,dispfunc);
end
delete(hw);
[qemb(2),semres]=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
set([handles.semdata.errdef handles.pushbutton9],'Enable','On');
set(handles.statustext,'String','finished quadrupoles fit');


% --- Executes on button press in pushbutton9: Errors->Fit dipoles
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
set(handles.statustext,'String','fitting dipoles');
if get(handles.AnalyticFitCheck,'Value') == 1
    qemb(2).diptilt=qemb(2).diptilt+semfitdipoleAnalytic(qemb(2).at,qemres,semres);
else
    qemb(2).diptilt=qemb(2).diptilt+semfitdipole(qemb(2).at,qemres,semres);
end
[qemb(2),semres]=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
set(handles.statustext,'String','finished dipoles fit');


% --- Executes on button press in pushbutton10: Errors->Zero
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
set(handles.statustext,'String','Setting errors to 0');
qemb(2).ks=qemb(1).ks;
qemb(2).diptilt=qemb(1).diptilt;
qemb(2).dzs=qemb(1).dzs;
[qemb(2),semres]=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
set(handles.semdata.errdef,'Enable','On');
semres.fithist=[]; % empty error fit residuals history
%set(hObject,'Enable','Off');
set(handles.statustext,'String','Errors at 0');

% --- Executes on button press in pushbutton11: Errors->Save
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
set(handles.statustext,'String','saving errors');
qfile=fullfile(qemres.datadir, 'skewerrors.mat');
gfile=fullfile(qemres.datadir, 'gainfit.mat');
ok=(exist(qfile,'file') ~= 2);
if ~ok
    [fname,fpath]=uiputfile('*.mat','Save errors:',qfile);
    if ischar(fname)
        qfile=fullfile(fpath,fname);
        gfile=fullfile(fpath,'gainfit.mat');
        ok=true;
    end
end
if ok
    s=struct('ks',qemb(2).ks,'diprot',qemb(2).diptilt,'dzs',qemb(2).dzs); %#ok<NASGU>
    save(qfile,'-struct','s');
    savebhscale=qemres.bhscale;
    qemres.bhscale=1;
    w=struct('bhscale',qembhscale(qemres,qemb(2).at));
    [w.khrot,w.khgain,w.kvrot,w.kvgain,w.bhrot,w.bhgain,w.bvrot,w.bvgain]=...
        qemguess(qemb(2).at,qemres,...
        qemres.resph,semres.respv,semres.resph,qemres.respv);
    qemres.bhscale=savebhscale;
    save(gfile,'-struct','w');
end

SaveErrorModel_Callback(hObject, eventdata, handles); % save error model

set(handles.statustext,'String','errors saved');


% --- Executes on button press in pushbutton16: Errors->load
function pushbutton16_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
qfile=fullfile(qemres.datadir, 'skewerrors.mat');
[fname,fpath]=uigetfile('*.mat','Load errors:',qfile);
if ischar(fname)
    s=load(fullfile(fpath,fname));
    qemb(2).ks=s.ks;
    qemb(2).diptilt=s.diprot;
    qemb(2).dzs=zeros(length(qemres.sextidx),1);
    if isfield(s,'dzs'), qemb(2).dzs=s.dzs; end
    [qemb(2),semres]=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
    set(handles.semdata.errdef,'Enable','On');
end


% --- Executes on button press in pushbutton12: Corrections->Correct
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
set(handles.statustext,'String','computing correction');
method=get(handles.popupmenu2,'Value');

% get initial correctors
if strcmp(qemres.relative_or_absolute_fit,'absolute')
        corval = qemb(2).skewcor(:,end);
    elseif strcmp(qemres.relative_or_absolute_fit,'relative')
        corval = qemb(2).skewcor(:,end)-qemb(2).skewcor(:,1);
end


if method == 1
elseif method == 2
    dispweight=str2double(get(handles.edit4,'String'));
    hw=waitbar(0,'Fitting skew correctors...');
    dispfunc=@(i,itot) waitbar(i/itot,hw);
    newcor=semsolverdt(qemb(2),qemres,semres,...
        corval,dispweight,dispfunc);
    delete(hw);
    set(handles.semdata.cordef,'Enable','On');
else
    mach=qemat(qemres,qemb(2),true);
    options=optimset(optimset('fminsearch'),'Display','iter',...
        'OutputFcn',@(x,y,z) semcorout(x,y,z,handles),'TolFun',1.e-7);
    [fun,options]=semcorargs(method,mach,qemres.bpmidx,qemres.skewidx,options);
    set(handles.pushbutton19,'Visible','on'); drawnow;
    inicor=corval./qemres.skewl;
    endcor=fminsearch(fun,inicor,options);
    newcor=endcor.*qemres.skewl;
    set(handles.pushbutton19,'Visible','off');
    set(handles.semdata.cordef,'Enable','On');
end

% recompute total correctors values
if strcmp(qemres.relative_or_absolute_fit,'absolute')
    qemb(2).skewcor(:,2) = newcor;
    
elseif strcmp(qemres.relative_or_absolute_fit,'relative')
    qemb(2).skewcor(:,2) = qemb(2).skewcor(:,1) + newcor;
    
end


[qemb(2),semres]=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
set(handles.statustext,'String','correction computed');


% --- Executes on button press in pushbutton13: Corrections->Initial
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres

a=textscan(fopen(fullfile(qemres.datadir,'skewcor.dat'),'r'),'%s %f\n','HeaderLines',7);
k0 = a{2};
qemb(2).skewcor(:,2)=k0;
set(handles.semdata.cordef,'Enable','On');
%set(hObject,'Enable','Off');
[qemb(2),semres]=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);


% --- Executes on button press in pushbutton14: Corrections->Zero
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
qemb(2).skewcor(:,2)=qemb(1).skewcor;
set(handles.semdata.cordef,'Enable','On');
%set(hObject,'Enable','Off');
[qemb(2),semres]=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);


% --- Executes on button press in pushbutton15: Corrections->Save
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres
set(handles.statustext,'String','saving correction');
corfile=fullfile(qemres.datadir, 'skewnew.dat');
ok=(exist(corfile,'file') ~= 2);
if ~ok
    [fname,fpath]=uiputfile('*.dat','Save correction:',corfile);
    if ischar(fname), corfile=fullfile(fpath,fname); ok=true; end
end
if ok
%    save_correction(corfile,qemres.skcode,,...
%        qemres.opticsdir);
sr.save_correction( corfile, ... filename
    sr.skewname(1:length(qemres.skewidx)),... device names
    qemb(2).skewcor(:,end)); % correction values

    set(handles.pushbutton18,'Enable','On');
end

SaveErrorModel_Callback(hObject, eventdata, handles); % save error model

set(handles.statustext,'String',['correction saved in: ' corfile]);

% --- Executes on button press in pushbutton17: Corrections->Load
function pushbutton17_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
corfile=fullfile(qemres.datadir, 'skewcor.dat');
[fname,fpath]=uigetfile('*.dat','Load correction:',corfile);
if ischar(fname)

    try
        a=textscan(fopen(fullfile(fpath,fname),'r'),'%s %f\n','HeaderLines',0); % sempanel saved file
    catch
        a=textscan(fopen(fullfile(fpath,fname),'r'),'%s %f\n','HeaderLines',7);
    end
    
    k0 = a{2};
    
    qemb(2).skewcor(:,2)=k0;
    
    set(handles.semdata.cordef,'Enable','On');
    [qemb(2),semres]=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
end


% --- Executes on button press in pushbutton18: Correction->Apply
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemres qemb semres
% bindir=[getenv('MACH_HOME') '/bin/' getenv('MACHARCH') '/'];
% corfile=fullfile(qemres.datadir, 'skewnew.dat');

% sr.setcorbase(corfile,'sr/qp-s/all','sr/reson-skew/m1_n1_p50','base_skew2');	% Set resonance device
% [s,w]=unix([bindir 'dvset <' corfile]); %#ok<ASGLU>         % Set values
% disp(w);
all_skew_ind = find(atgetcells(qemres(1).at,'FamName','S[FDIJH]\w*'))'; 
skewind = ismember(all_skew_ind,qemres.skewidx);
skewcor = zeros(size(all_skew_ind));
skewcor(skewind) = qemb(2).skewcor(:,end);
skewcor0 = skewcor;
skewcor0(skewind) = qemb(1).skewcor(:,1); % always zero.

% skew quadrupoles as saved before measurement
Sq=textscan(fopen(fullfile(qemres.datadir,'skewcor.dat'),'r'),'%s %f','HeaderLines',7);
skewcor0 = Sq{2}';%zeros(length(qemres.skewidx),1);

sqpdev = tango.Device('srmag/sqp/all');
% skewcor(~semres.skewkeep) = sqpdev.CorrectionStrengths.set(~semres.skewkeep);

%skewcor0 = sqpdev.CorrectionStrengths.set;
%val = skewcor0 + (skewcor-skewcor0)*handles.corfrac;
val = skewcor0 + (skewcor)*handles.corfrac;

setpointok = sqpdev.SetpointCheck(val);

if isempty(setpointok)
    try
        sqpdev.CorrectionStrengths = val;
        
        set(handles.statustext,'String',[num2str(handles.corfrac*100) '% correction applied']);
    catch err
        disp(err);
        set(handles.statustext,'String','correction NOT applied, probably too strong correctors.');
        
    end
else
    set(handles.statustext,'String','correction NOT applied, too strong correctors.');
end



% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
method=get(hObject,'Value');
if method == 2
    set([handles.text5 handles.edit4],'Visible','on');
else
    set([handles.text5 handles.edit4],'Visible','off');
end


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function DispFitWeigth_Callback(hObject, eventdata, handles)
% hObject    handle to DispFitWeigth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DispFitWeigth as text
%        str2double(get(hObject,'String')) returns contents of DispFitWeigth as a double
set(handles.statustext,'String',['Disp Weigth set to: ' get(hObject,'String')]);


% --- Executes during object creation, after setting all properties.
% DISPERSION WEIGTH FOR quadrupole FIT
function DispFitWeigth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DispFitWeigth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton19: Stop correction
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global interrupt
interrupt=true;



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton20: retune
function pushbutton20_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemb qemres semres
qemb(2)=qemretune(qemb(2),qemres,qemres.tunes);
[qemb(2),semres]=sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);


function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3
global qemb qemres
sempanelplot(5,get(hObject,'Value'),qemb(1),qemb(2),qemres);

% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in AnalyticFitCheck.
function AnalyticFitCheck_Callback(hObject, eventdata, handles)
% hObject    handle to AnalyticFitCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AnalyticFitCheck
set(handles.checkbox2,'Value',0.0)
set(handles.AnalyticFitCheck,'Value',1.0)


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2
set(handles.checkbox2,'Value',1.0)
set(handles.AnalyticFitCheck,'Value',0.0)



function neigQuadRDTcor_Callback(hObject, eventdata, handles)
% hObject    handle to neigQuadRDTcor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of neigQuadRDTcor as text
%        str2double(get(hObject,'String')) returns contents of neigQuadRDTcor as a double
global semres
semres.neigQuadRDTcor = str2double(get(hObject,'String'));
if ~isempty(handles)
    set(handles.statustext,'String',[ num2str(semres.neigQuadRDTcor) ' correction eigenvectors ']);
end


% --- Executes during object creation, after setting all properties.
function neigQuadRDTcor_CreateFcn(hObject, eventdata, handles)
% hObject    handle to neigQuadRDTcor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function neigQuadFit_Callback(hObject, eventdata, handles)
% hObject    handle to neigQuadFit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of neigQuadFit as text
%        str2double(get(hObject,'String')) returns contents of neigQuadFit as a double

global semres
semres.neigQuadFit = str2double(get(hObject,'String'));
if ~isempty(handles)
    set(handles.statustext,'String',[ num2str(semres.neigQuadFit) ' quad. fit eigenvectors ']);    
end
    

% --- Executes during object creation, after setting all properties.
function neigQuadFit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to neigQuadFit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function neigDipFit_Callback(hObject, eventdata, handles)
% hObject    handle to neigDipFit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of neigDipFit as text
%        str2double(get(hObject,'String')) returns contents of neigDipFit as a double
global semres
semres.neigDipFit = str2double(get(hObject,'String'));
if ~isempty(handles)
    set(handles.statustext,'String',[ num2str(semres.neigDipFit) ' dip. fit eigenvectors ']);
end

% --- Executes during object creation, after setting all properties.
function neigDipFit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to neigDipFit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in largefigure.
function largefigure_Callback(hObject, eventdata, handles)
% hObject    handle to largefigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of largefigure
global qemres qemb semres 
waitfor(set(handles.statustext,'String','figures are being redrawn'));

if get(hObject,'Value') == 1
    sempaneldisp(qemres,semres,qemb(2),qemb(1),handles);
    set(handles.statustext,'String','new figures done');
end



function correction_fraction_Callback(hObject, eventdata, handles)
% hObject    handle to correction_fraction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of correction_fraction as text
%        str2double(get(hObject,'String')) returns contents of correction_fraction as a double
frac = str2double(get(hObject,'String'));
if frac>100
    frac = 100;
    set(hObject,'String','100')
elseif frac<-100
    frac = -100;
    set(hObject,'String','-100')
end
handles.corfrac = frac/100;

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function correction_fraction_CreateFcn(hObject, eventdata, handles)
% hObject    handle to correction_fraction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in SaveErrorModel.
function SaveErrorModel_Callback(hObject, eventdata, handles)
% hObject    handle to SaveErrorModel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qemres qemb semres APPHOME

try
    r = qemb(1).at;
    rerr = qemb(2).at;
    rcor = qemat(qemres,qemb(2),true);
    
    % errtab and cortab are set here for use in the simulator with the lattice
    % varaible rcor only!
    errtab = atcreateerrortable(rcor); % empty error table
    cortab = atgetcorrectiontable(rcor,rcor); % zeros correction table
    
    tnow = now();
    errmodpath = qemres.datadir;
    %errmodpath=fullfile(qemres.opticsname(1:end-1),'FittedErrorModel',datestr(tnow,'yyyy'),datestr(tnow,'yyyy_mm_dd'));
    %mkdir(errmodpath); % create directory to save to
    qemres.latfilename = ['FittedErrorModel' datestr(tnow,'yyyy_mm_dd')];

    % errtab and cortab are set here for use in the simulator with the lattice
    % varaible rcor only!
    save(fullfile(errmodpath,qemres.latfilename),'r','rerr','rcor','errtab','cortab')

    warning('updated properties of sys/ringsimulator/fittedmodel');
    
    set(handles.statustext,'String',['Fitted model saved in: ' fullfile(errmodpath,qemres.latfilename)]);
    
catch
    set(handles.statustext,'String','Fitted model NOT saved. Either fit not performed or no permission to write in theory folder');
end