function lines=semselect(hfit,vfit,nbpm,nh)
%LINES=SEMSELECT(HLIST,VLIST)		Select lines in response matrix

[hx,hy]=meshgrid(nbpm*(hfit-1),(1:nbpm)');
[vx,vy]=meshgrid(nbpm*(vfit-1),(1:nbpm)'+nh*nbpm);
res=[vx+vy hx+hy];
lines=res(:)';
