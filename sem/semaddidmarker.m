function newmach = semaddidmarker(mach,marker,loc)
%SEMADDIDMARKER Add skew components at the location of ids
%NEWMACH=SEMADDIDMARKER(MACH,MARKER,loc)
%	creates a new AT structure with new elements inserted in straights,
%
%	MARKER: new element to be inserted
%   LOC: Distance of markers from middle of straight section
%

if isempty(marker)
   marker=struct('FamName','MK%c%d','Length',0,'PassMethod','IdentityPass');
end
if loc(1) == 0
   dist=diff(loc(:))';
else
   dist=diff([0;loc(:)])';
end
namepat=marker.FamName;
lsub=loc(end);
high=findcells(mach(:),'FamName','SDH[I1]');	% "1" for 6m sections
low=findcells(mach(:),'FamName','SDL[O1]');		% "1" for 6m sections
if isempty(high) || isempty(low), return; end
sdh=atelem(mach{high(1)},'FamName','SDHK');
sdl=atelem(mach{low(2)},'FamName','SDLK');
seqh={};
seql={};
idn=0;
for lg=dist(end:-1:1)
   idn=idn+1;
   seqh=[seqh;{atelem(marker,'FamName',sprintf(namepat,'H',idn));atelem(sdh,'Length',lg)}]; %#ok<AGROW>
   seql=[seql;{atelem(marker,'FamName',sprintf(namepat,'L',idn));atelem(sdl,'Length',lg)}]; %#ok<AGROW>
end
if loc(1) == 0
   idn=idn+1;
   seqh=[seqh;{atelem(marker,'FamName',sprintf(namepat,'H',idn))}];
   seql=[seql;{atelem(marker,'FamName',sprintf(namepat,'L',idn))}];
end
for lg=dist
   idn=idn+1;
   seqh=[seqh;{atelem(sdh,'Length',lg);atelem(marker,'FamName',sprintf(namepat,'H',idn))}]; %#ok<AGROW>
   seql=[seql;{atelem(sdl,'Length',lg);atelem(marker,'FamName',sprintf(namepat,'L',idn))}]; %#ok<AGROW>
end
t1=seqh(2*length(dist)+1:end,:);
t2=seql;
t3=seqh(1:2*length(dist),:);
ncells=size(mach,2);
lh=getcellstruct(mach,'Length',high);
ll=getcellstruct(mach,'Length',low);
newmach=setcellstruct(setcellstruct(   mach,'Length',high,lh-lsub),'FamName',high,'SDHX');
newmach=setcellstruct(setcellstruct(newmach,'Length',low ,ll-lsub),'FamName',low ,'SDLX');

newmach=[t1(:,ones(1,ncells));newmach(high(1):low(1),:);...
   t2(:,ones(1,ncells));newmach(low(2):high(2),:);...
   t3(:,ones(1,ncells));newmach(high(2)+1:end,:)];
end
