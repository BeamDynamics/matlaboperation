function [diprot,dipresponse]=semfitdipole(mach,qemres,semres,okfit,okbpm)
%SEMFITDIPOLE Fits dipole beanding angle to match the measured dispersion
%[DIPROT,DIPRESPONSE]=SEMFITDIPOLE(QEMB,QEMRES,SEMRES,OKVAR,OKBPM)

if nargin<5, okbpm=[]; end
if nargin<4, okfit=[]; end
if isempty(okbpm), okbpm=true(length(qemres.bpmidx),1); end
if isempty(okfit), okfit=true(1,length(qemres.dipidx)); end
[~,frv]=qembpmdecode(qemres.frespx,semres.frespz,...
    qemres.brot,qemres.bhgain,qemres.bvgain);
[~,frv0]=qemfresp(mach,qemres.ct,qemres.bpmidx);
dfrv=semres.bvscale*frv-frv0;
[~,dipresponse]=qemdispderiv(mach,qemres.ct,@tiltelem,1.e-4,qemres.dipidx,qemres.bpmidx);
bok=isfinite(dfrv)&okbpm;
%diprot=dipresponse(bok,okfit)\dfrv(bok);
diprot = qemsvd(dipresponse(bok,okfit),dfrv(bok),semres.neigDipFit);%floor(length(find(okfit))/2));

    function elt=tiltelem(elt,dval)
        vini=atan2(elt.R1(1,3),elt.R1(1,1));
        elt=attiltelem(elt,vini+dval);
    end
        
end
