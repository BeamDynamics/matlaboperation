function [rh,rv]=dispresp(mach,rotidx,bpmidx)

dCT=1.0e-8;
[rh1,rv1,dpp1]=rotresp(mach,dCT,rotidx,bpmidx);
[rh0,rv0,dpp0]=rotresp(mach,0,rotidx,bpmidx);
norm=1/(dpp1-dpp0);
rh=(rh1-rh0)*norm;
rv=(rv1-rv0)*norm;
