classdef SkewPolarity < RingControl
    %SKEWPOLARITY checks polarity of skew quadrupole correctors assuming a
    %closed orbit
    %
    %
    
    properties
        skewkick;
        bumpH;
        bumpV;
        plot;
        sBPM;
        sSkew;
        sSH;
        sHst;
        SkewNames;
        DeltaFreq;
        indSDF;
        indSH;
        indCAV;
        RMh;
        RMv;
    end
    
    methods
        function obj = SkewPolarity(machine)
            %SKEWPOLARITY Construct an instance of this class
            %   Detailed explanation goes here
            TANGO_HOST=['tango://' getenv('TANGO_HOST') '/'];

            obj@RingControl(machine);
            obj.bumpH    = movable([TANGO_HOST 'sr/beam-bump/all-h/BumpAmplitudes'],'absolute',true,'limits',[-1.0e-3 1.0e-3]); 
            obj.bumpV    = movable([TANGO_HOST 'sr/beam-bump/all-v/BumpAmplitudes'],'absolute',true,'limits',[-1.0e-3 1.0e-3]);

            obj.n_acquisitions=1;
            % put the default values in the properties
            obj.plot=true;
            obj.skewkick=0.7e-2;
            % find s positions of BPMs and steerers
            obj.sBPM=findspos(obj.rmodel,obj.indBPM);
            obj.sSkew=findspos(obj.rmodel,obj.indSkew);
            obj.n_acquisitions=1;
            skews=tango.Device('srmag/sqp/all');
            obj.SkewNames=skews.CorrectorNames.read;
            obj.DeltaFreq=-250; % change of rf freq for bumps in SD and SF
            obj.indSDF=obj.indSext;
            obj.indSH=obj.indSkew(~ismember(obj.indSkew,obj.indSDF));
            obj.sSH=findspos(obj.rmodel,obj.indSH);
            obj.sHst=findspos(obj.rmodel,obj.indHst);
            obj.indCAV=findcells(obj.rmodel,'Class','RFCavity');
            load('/machfs/carmigna/EBS/Commissioning/OrbCorr/Orb_misalignment/RMs.mat','RMh','RMv');
            obj.RMh=RMh;
            obj.RMv=RMv;
        end
        
        [Dtune,Dphase,Dampl] = CheckOne_SDF(obj,index);
        [Dtune,Dphase,Dampl] = CheckOne_SH(obj,index);
        orb_pm = SimulateResp_SkewSDF(obj,index);
        orb_pm = SimulateResp_SkewSH(obj,index,indHStToChange,SteererKick);
        [Dtune,Dphase,Dampl] = CheckMany_SDF(obj,indexes);
        [Dtune,Dphase,Dampl] = CheckMany_SH(obj,indexes);
    end
end

