function orb_pm = SimulateResp_SkewSH(obj,index, indHSt, SteererKick)
%SIMULATERESP_SKEWSH simulates the polarity check in the model in skew
%located on correctors SH: apply skew quad, power a steerer before and then at 
%, return orbit distortion for + and - value rf change
%
%  orb_pm = SimulateResp_SkewSH(obj,index, indHSt)
%
%  The simulation is done with findorbit6.
%  disabled bpms (obj.getEnabledBPM) give nan as orbit
%
% see also: SimulateResp_SkewSDF, findorbit6, getEnabledBPM

bpm_enabled=obj.getEnabledBPM;

r=obj.rmodel;
%f0=obj.rmodel{obj.indCAV(1)}.Frequency;  
sh_ind=ismember(obj.indSkew,obj.indSH(index));
skew0=r{obj.indSkew(sh_ind)}.PolynomA(2);

%move HSt at indHSt
r{obj.indHst(indHSt)}.KickAngle=[SteererKick,0];
% r=atsetfieldvalues(r,obj.indCAV,'Frequency',f0+obj.DeltaFreq);
%send skew at skew0+skewkick
r{obj.indSkew(sh_ind)}.PolynomA(2)=skew0+obj.skewkick./r{obj.indSkew(sh_ind)}.Length;

%% skew plus orbit plus
orb_splus_oplus=findorbit6(r,obj.indBPM);

%move HSt at indHSt at minus kick
r{obj.indHst(indHSt)}.KickAngle=[-SteererKick,0];

%% skew plus orbit minus
orb_splus_ominus=findorbit6(r,obj.indBPM);


%send skew at skew0-skewkick
r{obj.indSkew(sh_ind)}.PolynomA(2)=skew0-obj.skewkick./r{obj.indSkew(sh_ind)}.Length;

%% skew minus orbit minus
orb_sminus_ominus=findorbit6(r,obj.indBPM);

%move HSt at indHSt at plus kick
r{obj.indHst(indHSt)}.KickAngle=[+SteererKick,0];

%% skew minus orbit plus
orb_sminus_oplus=findorbit6(r,obj.indBPM);



orb=(orb_splus_oplus-orb_sminus_oplus)-(orb_splus_ominus-orb_sminus_ominus);

orb_pm=zeros(2,length(obj.indBPM));
orb_pm(1,:)=orb(1,:);
orb_pm(1,~bpm_enabled)=nan;

orb_pm(2,:)=orb(3,:);
orb_pm(2,~bpm_enabled)=nan;
orb_pm(2,isnan(orb_pm(1,:)))=nan;

end