function [Dtune, Dphase, Dampl] = CheckOne_SDF(obj,index)
%  [Dtune, Dphase, Dampl] = CheckOne_SDF(index)
%
%
%  This function checks the polarity of the skew quadrupole
%  correctors assuming the presence of a closed orbit in the
%  machine. This function is only for skew quadrupoles in the
%  SD and SF magnets, not in SH, for which there is another
%  function.
%
%  error is the calibration error, abs(error)<~0.2 if polarity
%  is good.
%
%  index is between 1 and 192
%
%  see also:


f0=obj.rf.get;  % store initial rf freq
skew_init=obj.skew.get;  % store initial skew values
try
    
    [Dtune, Dphase, Dampl] =tryPolwithSkewKick(obj,index,obj.skewkick,f0,skew_init);

catch
    try
        obj.rf.set(f0);
        obj.skew.set(skew_init);
        [Dtune, Dphase, Dampl] =tryPolwithSkewKick(obj,index,obj.skewkick/2,f0,skew_init);
    catch
        try
            obj.rf.set(f0);
            obj.skew.set(skew_init);
            [Dtune, Dphase, Dampl] =tryPolwithSkewKick(obj,index,obj.skewkick/4,f0,skew_init);
        catch
            obj.rf.set(f0);
            obj.skew.set(skew_init);
            disp('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
            disp(['Skew in S[DF] number ' num2str(index) ' failed.']);
            disp('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
            Dtune=nan;
            Dphase=nan;
            Dampl=nan;
        end
    end
end
end

function [Dtune, Dphase, Dampl] =tryPolwithSkewKick(obj,index,skewkick,f0,skew_init)

%% Prepare vectors of skew_plus and skew_minus
%calc skew values with pos kick and neg kick at index
skew_plus=skew_init; skew_minus=skew_init;
sdf_ind=find(ismember(obj.indSkew,obj.indSDF(index)));
skew_plus(sdf_ind)=skew_plus(sdf_ind)+skewkick;
skew_minus(sdf_ind)=skew_minus(sdf_ind)-skewkick;



%% apply skew plus

obj.skew.set(skew_plus);
 pause(3);
applied_skew_plus=obj.skew.get;
%measure orbit with skew plus and f0  (not used)
orb_meas_f0_skewplus=obj.measureorbit;

%% skew+ and f0+delta
obj.rf.set(f0+obj.DeltaFreq);
pause(3);
%measure orbit with skew plus and f0+delta
orb_meas_fplus_skewplus=obj.measureorbit;

%% skew+ and f0-delta
obj.rf.set(f0-obj.DeltaFreq);
pause(3);
%measure orbit with skew plus and f0+delta
orb_meas_fminus_skewplus=obj.measureorbit;


%% skew- and f0-delta
obj.skew.set(skew_minus);
 pause(3);
applied_skew_minus=obj.skew.get;
%measure orbit with skew minus and f0-delta
orb_meas_fminus_skewminus=obj.measureorbit;

%% skew- and f0+delta
obj.rf.set(f0+obj.DeltaFreq);
pause(3);
%measure orbit with skew minus and f0+delta
orb_meas_fplus_skewminus=obj.measureorbit;


applied_skew_kick=...
    (applied_skew_plus(sdf_ind)-applied_skew_minus(sdf_ind))/2;

% %measure orbit with skew minus and f0-DELTA
% orb_meas_fminus_skewminus=obj.measureorbit;
% 
% %change rf freq to f0-DELTA
% obj.rf.set(f0-obj.DeltaFreq);
%  pause(3);
% %measure orbit with skew minus and f0-DELTA
% orb_meas_fminus_skewminus=obj.measureorbit;


%calc dorb
dorb_meas=((orb_meas_fplus_skewplus-orb_meas_fplus_skewminus)-...
    (orb_meas_fminus_skewplus-orb_meas_fminus_skewminus))*...
    skewkick/applied_skew_kick;

% if applied_skew_kick<0.7*skewkick
%     disp('here');
% end
dorb_simu = SimulateResp_SkewSDF(obj,index);

% orblarge=abs(dorb_simu(2,:))>mean(abs(dorb_simu(2,:)))/2;
%error=mean(dorb_simu(2,orblarge)./dorb_meas(2,orblarge))-1;

nbpm=length(dorb_simu(2,:));
[fr_simu,~,~,phase_simu,~,~]=findtunetom2(dorb_simu(2,:)',200);
[fr_meas,~,~,phase_meas,~,~]=findtunetom2(dorb_meas(2,:)',200);
ampl_simu=rms(dorb_simu(2,:)');
ampl_meas=rms(dorb_meas(2,:)');
tune_simu=fr_simu*nbpm;
tune_meas=fr_meas*nbpm;

Dtune=(tune_simu-tune_meas)/tune_simu;
Dphase=phase_simu-phase_meas;
Dampl=(ampl_simu-ampl_meas)/ampl_simu;

    obj.rf.set(f0);
    obj.skew.set(skew_init);

if obj.plot
    close(gcf);
    figure;
    
    subplot(2,1,1);
    plot(1e6*dorb_meas(1,:),'LineWidth',2);
    hold on; grid on;
    plot(1e6*dorb_simu(1,:),'LineWidth',2);
    xlabel('bpm number')
    ylabel('x (\mum)')
    title(['Skew in S[DF] number ' num2str(index) ]);
    subplot(2,1,2);
    plot(1e6*dorb_meas(2,:),'LineWidth',2)
    hold on; grid on;
    plot(1e6*dorb_simu(2,:),'LineWidth',2);
    xlabel('bpm number')
    ylabel('x (\mum)')
    l=legend('Location','NorthEast','Measured','Simulated');
    title(['Dtune ' num2str(Dtune,'%1.3f') ...
        ' - Dphase ' num2str(Dphase,'%1.3f') ...
        'rad - Dampl ' num2str(Dampl,'%1.3f')])
    if ~exist('./plots','dir')
        mkdir('plots');
    end
    saveas(gcf,['./plots/skewS_DF_' num2str(index) '.fig']);
end

disp('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
disp(['Errors for skew in S[DF] number ' num2str(index)]);
disp(['Relative tune err = ' num2str(Dtune,'%1.3f') ]);
disp(['Phase error = ' num2str(Dphase,'%1.3f') ' rad']);
disp(['Relative amplitude error = ' num2str(Dampl,'%1.3f') ]);
disp('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');


end