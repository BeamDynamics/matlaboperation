function [Dtune,Dphase,Dampl] = CheckMany_SDF(obj,indexes)
% errors = CheckMany_SDF(obj,indexes) calls many times CheckOne_SDF
%
%

if obj.plot
    if ~exist('./plots','dir')
        mkdir('./plots');
    end
end
% errors=zeros(size(indexes));
Dtune=zeros(size(indexes));
Dphase=zeros(size(indexes));
Dampl=zeros(size(indexes));

for ii=1:length(indexes)
    [Dtune(ii),Dphase(ii),Dampl(ii)]=CheckOne_SDF(obj,indexes(ii));
end

figure;
subplot(3,1,1)
plot(indexes,Dtune,'.-','MarkerSize',22,'LineWidth',2);
ylabel('Tune error');
title(['Tune errors skew in S[DF]']);
subplot(3,1,2)
plot(indexes,Dphase,'.-','MarkerSize',22,'LineWidth',2);
ylabel('Phase error (rad)');
title(['Phase errors skew in S[DF]']);
subplot(3,1,3)
plot(indexes,Dampl,'.-','MarkerSize',22,'LineWidth',2);
xlabel('skew S[DF] number');
ylabel('Ampl error');
title(['Ampl errors skew in S[DF]']);

saveas(gca,['plots/errors_st_SDF.fig']);

end