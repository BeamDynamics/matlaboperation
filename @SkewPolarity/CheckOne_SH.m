function [Dtune, Dphase, Dampl] = CheckOne_SH(obj,index)
%CHECKONE_SH Summary of this function goes here
%
%  This function checks the polarity of the skew quadrupole
%  correctors assuming the presence of a closed orbit in the
%  machine. This function is only for skew quadrupoles in the
%  SH magnets, not in SD or SF, for which there is another
%  function.
%
%
%  index is between 1 and 96
%

s_skew=obj.sSH(index);
iclosebpm=find(abs(obj.sBPM-s_skew)<1);

% disp(['sskew = ' num2str(s_skew) ', sbpm = ' num2str(obj.sBPM(iclosebpm)) ]);
% figure; plot(obj.RMh{1}(iclosebpm,:))

indsh=(s_skew-28)<obj.sHst & obj.sHst<(s_skew-1);
if sum(indsh)<12
    indsh=s_skew-28<obj.sHst & obj.sHst<s_skew-1 | obj.sHst>820;
end

% figure; 
% subplot(2,1,1)
% hold on; grid on;
% plot(obj.RMh{1}(iclosebpm,indsh));

indsh=find(indsh);

if mod(index,3)==1
    indsh=indsh([end-3,end-2,end-1]);
    %threeSteerers: -1 -2 -3
elseif mod(index,3)==2
    indsh=indsh([end-3,end-2,end-1]);
    %threeSteerers: -1 -2 -3
elseif mod(index,3)==0
    indsh=indsh([end-11,end-2,end-1]);
    %threeSteerers: -1 -2 -11
end

% subplot(2,1,2)
% plot(obj.RMh{1}(iclosebpm,indsh),'*-');

HSt_init=obj.sh.get;
Hst_init3=HSt_init(indsh);
[valmin,indmin]=min(abs(Hst_init3));
SteererKick=0.3e-3 - valmin;
indHStToChange=indsh(indmin);
skew_init=obj.skew.get;  % store initial skew values
HSt_init=obj.sh.get;
% try
    
    [Dtune, Dphase, Dampl] =tryPolwithSkewKick_SH(obj,index,obj.skewkick,SteererKick,indHStToChange,skew_init,HSt_init);
% [Dtune, Dphase, Dampl] =tryPolwithSkewKick_bump_SH(obj,index,obj.skewkick,iclosebpm,skew_init,HSt_init);
% catch
%     try
%         obj.rf.set(f0);
%         obj.skew.set(skew_init);
%         [Dtune, Dphase, Dampl] =tryPolwithSkewKick_SH(obj,index,obj.skewkick/2,indHStToChange,skew_init,HSt_init);
%     catch
%         try
%             obj.rf.set(f0);
%             obj.skew.set(skew_init);
%             [Dtune, Dphase, Dampl] =tryPolwithSkewKick_SH(obj,index,obj.skewkick/4,indHStToChange,skew_init,HSt_init);
%         catch
%             obj.rf.set(f0);
%             obj.skew.set(skew_init);
%             disp('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
%             disp(['Skew in S[DF] number ' num2str(index) ' failed.']);
%             disp('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
%             Dtune=nan;
%             Dphase=nan;
%             Dampl=nan;
%         end
%     end
% end
end

function [Dtune, Dphase, Dampl] = tryPolwithSkewKick_bump_SH(obj,...
    index,skewkick,iclosebpm,skew_init,HSt_init)

bumpval=0.5e-3;
%calc skew values with pos kick and neg kick at index
skew_plus=skew_init; skew_minus=skew_init;
sh_ind=find(ismember(obj.indSkew,obj.indSH(index)));
skew_plus(sh_ind)=skew_plus(sh_ind)+skewkick;
skew_minus(sh_ind)=skew_minus(sh_ind)-skewkick;

%apply skew plus
obj.skew.set(skew_plus);
% pause(3);
applied_skew_plus=obj.skew.get;
%measure orbit with skew plus and orb 0
orb_meas_orb0_skewplus=obj.measureorbit;
% pause(3);

% change orbit in bpm  iclosebpm
bumps0=obj.bumpH.get();
HSt0=obj.sh.get;
bumps_plus=bumps0; bumps_plus(iclosebpm)=bumps_plus(iclosebpm)+bumpval;
obj.bumpH.set(bumps_plus);
%measure orbit with skew plus and orb+delta
HStplus=obj.sh.get;
orb_meas_orbplus_skewplus=obj.measureorbit;

%apply skew minus
obj.skew.set(skew_minus);
% pause(3);
applied_skew_minus=obj.skew.get;

applied_skew_kick=...
    (applied_skew_plus(sh_ind)-applied_skew_minus(sh_ind))/2;

%measure orbit with skew minus and f0+DELTA
orb_meas_orbplus_skewminus=obj.measureorbit;

%change steerer to go back to orb 0
obj.bumpH.set(bumps0);
% pause(3);
%measure orbit with skew minus and orb 0
orb_meas_orb0_skewminus=obj.measureorbit;


%calc dorb
dorb_meas=((orb_meas_orbplus_skewplus-orb_meas_orbplus_skewminus)-...
    (orb_meas_orb0_skewplus-orb_meas_orb0_skewminus))*...
    skewkick/applied_skew_kick;

%%%%%%%%%%%%%%%%%%%%%%
% simulated 
%%%%%%%%%%%%%%%%%%%%%%
dorb_simu = SimulateResp_BumpSkewSH(obj,index,HStplus-HSt0,SteererKick);
          
% orblarge=abs(dorb_simu(2,:))>mean(abs(dorb_simu(2,:)))/2;
%error=mean(dorb_simu(2,orblarge)./dorb_meas(2,orblarge))-1;

nbpm=length(dorb_simu(2,:));
[fr_simu,~,~,phase_simu,~,~]=findtunetom2(dorb_simu(2,:)',200);
[fr_meas,~,~,phase_meas,~,~]=findtunetom2(dorb_meas(2,:)',200);
ampl_simu=rms(dorb_simu(2,:)');
ampl_meas=rms(dorb_meas(2,:)');
tune_simu=fr_simu*nbpm;
tune_meas=fr_meas*nbpm;

Dtune=(tune_simu-tune_meas)/tune_simu;
Dphase=phase_simu-phase_meas;
Dampl=(ampl_simu-ampl_meas)/ampl_simu;

    obj.sh.set(HSt_init);
    obj.skew.set(skew_init);

if obj.plot
    close(gcf);
    figure;
    
    subplot(2,1,1);
    plot(dorb_meas(1,:),'LineWidth',2);
    hold on; grid on;
    plot(dorb_simu(1,:),'LineWidth',2);
    title(['Skew in S[DF] number ' num2str(index) ]);
    subplot(2,1,2);
    plot(dorb_meas(2,:),'LineWidth',2)
    hold on; grid on;
    plot(dorb_simu(2,:),'LineWidth',2);
    l=legend('Location','NorthEast','Measured','Simulated');
    title(['Dtune ' num2str(Dtune,'%1.3f') ...
        ' - Dphase ' num2str(Dphase,'%1.3f') ...
        'rad - Dampl ' num2str(Dampl,'%1.3f')])
%     saveas(gcf,['./plots/skewS_DF_' num2str(index) '.fig']);
end

disp('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
disp(['Errors for skew in S[DF] number ' num2str(index)]);
disp(['Relative tune err = ' num2str(Dtune,'%1.3f') ]);
disp(['Phase error = ' num2str(Dphase,'%1.3f') ' rad']);
disp(['Relative amplitude error = ' num2str(Dampl,'%1.3f') ]);
disp('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');


end

function [Dtune, Dphase, Dampl] =tryPolwithSkewKick_SH(obj,index,skewkick,SteererKick,indHStToChange,skew_init,HSt_init)


% SteererKick=2.5e-4;
%calc skew values with pos kick and neg kick at index
skew_plus=skew_init; skew_minus=skew_init;
sh_ind=find(ismember(obj.indSkew,obj.indSH(index)));
skew_plus(sh_ind)=skew_plus(sh_ind)+skewkick;
skew_minus(sh_ind)=skew_minus(sh_ind)-skewkick;

%apply skew plus
obj.skew.set(skew_plus);
pause(3);
applied_skew_plus=obj.skew.get;
% %measure orbit with skew plus and orb 0  (unused)
% orb_meas_orb0_skewplus=obj.measureorbit;

% change orbit in steerer indHStToChange 
newHst=HSt_init; newHst(indHStToChange)=newHst(indHStToChange)+SteererKick;
obj.sh.set(newHst);
pause(3);

%% measure orbit with skew plus and orb+delta
orb_meas_orbplus_skewplus=obj.measureorbit;

%change steerer to go to orb-DELTA
newHst=HSt_init; newHst(indHStToChange)=newHst(indHStToChange)-SteererKick;
obj.sh.set(newHst);
pause(3);
%% measure orbit with skew plus and orb-delta
orb_meas_orbminus_skewplus=obj.measureorbit;


%apply skew minus
obj.skew.set(skew_minus);
pause(3);
applied_skew_minus=obj.skew.get;

applied_skew_kick=...
    (applied_skew_plus(sh_ind)-applied_skew_minus(sh_ind))/2;

%% measure orbit with skew minus and orb-DELTA
orb_meas_orbminus_skewminus=obj.measureorbit;

%change steerer to go to orb+DELTA
newHst=HSt_init; newHst(indHStToChange)=newHst(indHStToChange)+SteererKick;
obj.sh.set(newHst);

pause(3);
%% measure orbit with skew minus and orb+DELTA
orb_meas_orbplus_skewminus=obj.measureorbit;


%% send everything to init
obj.sh.set(HSt_init);
obj.skew.set(skew_init);
pause(3);

%calc dorb
dorb_meas=((orb_meas_orbplus_skewplus-orb_meas_orbplus_skewminus)-...
    (orb_meas_orbminus_skewplus-orb_meas_orbminus_skewminus))*...
    skewkick/applied_skew_kick;

%%%%%%%%%%%%%%%%%%%%%%
% simulated 
%%%%%%%%%%%%%%%%%%%%%%
dorb_simu = SimulateResp_SkewSH(obj,index,indHStToChange,SteererKick);
            
% orblarge=abs(dorb_simu(2,:))>mean(abs(dorb_simu(2,:)))/2;
%error=mean(dorb_simu(2,orblarge)./dorb_meas(2,orblarge))-1;

nbpm=length(dorb_simu(2,:));
[fr_simu,~,~,phase_simu,~,~]=findtunetom2(dorb_simu(2,:)',200);
[fr_meas,~,~,phase_meas,~,~]=findtunetom2(dorb_meas(2,:)',200);
ampl_simu=rms(dorb_simu(2,:)');
ampl_meas=rms(dorb_meas(2,:)');
tune_simu=fr_simu*nbpm;
tune_meas=fr_meas*nbpm;

Dtune=(tune_meas-tune_simu)/tune_simu;
Dphase=phase_meas-phase_simu;
Dampl=(ampl_meas-ampl_simu)/ampl_simu;

    obj.sh.set(HSt_init);
    obj.skew.set(skew_init);

if obj.plot
    close(gcf);
    figure;
    
    subplot(2,1,1);
    plot(1e6*dorb_meas(1,:),'LineWidth',2);
    hold on; grid on;
    plot(1e6*dorb_simu(1,:),'LineWidth',2);
    xlabel('bpm number')
    ylabel('x (\mum)')
    title(['Skew in SH number ' num2str(index) ]);
    subplot(2,1,2);
    plot(1e6*dorb_meas(2,:),'LineWidth',2)
    hold on; grid on;
    plot(1e6*dorb_simu(2,:),'LineWidth',2);
    xlabel('bpm number')
    ylabel('y (\mum)')
    l=legend('Location','NorthEast','Measured','Simulated');
    title(['Dtune ' num2str(Dtune,'%1.3f') ...
        ' - Dphase ' num2str(Dphase,'%1.3f') ...
        'rad - Dampl ' num2str(Dampl,'%1.3f')])
%     saveas(gcf,['./plots/skewS_DF_' num2str(index) '.fig']);
end

disp('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
disp(['Errors for skew in SH number ' num2str(index)]);
disp(['Relative tune err = ' num2str(Dtune,'%1.3f') ]);
disp(['Phase error = ' num2str(Dphase,'%1.3f') ' rad']);
disp(['Relative amplitude error = ' num2str(Dampl,'%1.3f') ]);
disp('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');


end
