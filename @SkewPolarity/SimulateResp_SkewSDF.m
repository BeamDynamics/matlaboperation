function orb_pm = SimulateResp_SkewSDF(obj,index)
%SIMULATERESP_SKEWSDF simulates the polarity check in the model in skew
%located on sextupoles: apply skew quad, go at f0+DELTA and then at 
%f0-DELTA, return orbit distortion for + and - value rf change
%
%  orb_pm = SimulateResp_SkewSDF(obj,index,kick_skew)
%
%  The simulation is done with findorbit6.
%  disabled bpms (obj.getEnabledBPM) give nan as orbit
%
% see also: findorbit6, getEnabledBPM

bpm_enabled=obj.getEnabledBPM;

r=obj.rmodel;
f0=obj.rmodel{obj.indCAV(1)}.Frequency;
sdf_ind=ismember(obj.indSkew,obj.indSDF(index));
skew0=r{obj.indSkew(sdf_ind)}.PolynomA(2);
%move rf freq
r=atsetfieldvalues(r,obj.indCAV,'Frequency',f0+obj.DeltaFreq);

%send skew at skew0+skewkick
r{obj.indSkew(sdf_ind)}.PolynomA(2)=skew0+obj.skewkick./r{obj.indSkew(sdf_ind)}.Length;

orb_splus_fplus=findorbit6(r,obj.indBPM);

%send skew at skew0-skewkick
r{obj.indSkew(sdf_ind)}.PolynomA(2)=skew0-obj.skewkick./r{obj.indSkew(sdf_ind)}.Length;


orb_sminus_fplus=findorbit6(r,obj.indBPM);

%move rf freq to fminus
r=atsetfieldvalues(r,obj.indCAV,'Frequency',f0-obj.DeltaFreq);

orb_sminus_fminus=findorbit6(r,obj.indBPM);

%send skew at skew0+skewkick
r{obj.indSkew(sdf_ind)}.PolynomA(2)=skew0+obj.skewkick./r{obj.indSkew(sdf_ind)}.Length;

orb_splus_fminus=findorbit6(r,obj.indBPM);


orb_simu=(orb_splus_fplus-orb_sminus_fplus)-(orb_splus_fminus-orb_sminus_fminus);
% dorb_meas=((orb_meas_fplus_skewplus-orb_meas_fplus_skewminus)-...
%     (orb_meas_fminus_skewplus-orb_meas_fminus_skewminus))*...
%     skewkick/applied_skew_kick;


orb_pm=zeros(2,length(obj.indBPM));
orb_pm(1,:)=orb_simu(1,:);
orb_pm(1,~bpm_enabled)=nan;

orb_pm(2,:)=orb_simu(3,:);
orb_pm(2,~bpm_enabled)=nan;
orb_pm(2,isnan(orb_pm(1,:)))=nan;

end