function idx=getmask(npercell,ncells,celloffset,varargin)
%GETMASK	find index of elements in a periodic lattice
%
%MASK=GETMASK(NPERCELL,NCELLS,CELLOFFSET,CELL,NUM)
%MASK=GETMASK(NPERCELL,NCELLS,CELLOFFSET,[CELL NUM])
%
% NPERCELL:     Number of elements per cell
% NCELLS:       Total number of cells
% CELLOFFSET:	Number of cells to skip
% CELL:         Cell number (1..NCELLS)
% NUM:          Index in the cell (1..NPERCELL)
%
%MASK=GETMASK(NPERCELL,NCELLS,CELLOFFSET,KDX)
%
% KDX:          Index starting in cell 1
%
%MASK=GETMASK(NPERCELL,NCELLS,CELLOFFSET,SCANFUNCTION,NAME)
%
% SCANFUNCTION: Function returning [CELL,NUM] from the name
% NAME:         string, char array or cell array of strings
%
%See also: GETINDEX GETCELLNUM

msk=false(npercell,ncells);
if isa(varargin{1},'function_handle')
    [bfunc,name]=deal(varargin{1:2});
    [nrows,ncols]=size(name);
    if iscell(name)		% cell array of strings
        for ik=1:nrows*ncols, bfind(name{ik}); end
    elseif nrows > 1		% char array
        for row=1:nrows, bfind(name(row,:)); end
    else					% string
        bfind(name);
    end
else
    arg1=varargin{1};
    if ~isvector(arg1)          % [cellnum id]
        msk(npercell*(arg1(:,1)-1)+arg1(:,2))=true;
    elseif length(varargin) < 2	% "hardware" index
        msk(arg1)=true;
    else                        % cellnum,id
        msk(npercell*(arg1-1)+varargin{2})=true;
    end
end
idx=reshape(circshift(msk,[0 -celloffset]),1,[]);

    function bfind(name)
        [cellnum,id]=bfunc(name);
        msk(id,cellnum)=true;
    end
end
