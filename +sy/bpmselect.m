function idx=bpmselect(varargin)
%BPMINDEX	find index of BPM from its name
%
%IDX=BPMINDEX(BPMBAME)
%   BPMNAME:	string, char array or cell array of strings
%
%IDX=BPMINDEX(KDX)
%   KDX:        "hardware" index of the BPM
%
%[IDX,KDX]=BPMINDEX(...)	returns in addition the "hardware" index
%
%   See also BPMNAME

persistent bkeep kexpand
if isempty(bkeep)
    kexpand=1:78;
    kexpand([2 28 77])=[];
    bkeep=true(78,1);
    bkeep([23 26 52])=false;
end

if isnumeric(varargin{1})
    if isvector(varargin{1}) && length(varargin)<2
        id78=getmask(2,39,27,kexpand(varargin{1}));
    else
        id78=getmask(2,39,27,varargin{:});
    end
else
    id78=getmask(2,39,27,@bindex,varargin{:});
end
idx=id78(bkeep);

    function [cellnum,id]=bindex(name)
        bpattern='^(?:(tango:)?sy/d-bpm(libera)?(-sp)?/)?q([df])(\d{1,2})$';
        toks=regexpi(name,bpattern,'tokens');
        if isempty(toks),error('BPM:wrongName','Wrong BPM name'); end
        cellnum=str2double(toks{1}{2});
        id=double(toks{1}{1}=='d')+1;
    end
end
