function CreateOpticsFiles(r,opticsname,varargin)
% CREATEOPTICSFUILES(r,opticsname,'property',value,...), 
% given an AT lattice R creates the relevant files for operation
%
% operation folder: APPHOME/optics/sy/(OPTICSNAME) 
%
% optional input ('NAME',val,'Name2',val2,...)
% 'OperationFolder'  : commissioningtools folder where optics/sy/theory
%                      link is present.
% 'MeasuredResponse' : folder containing ouput of full RM measurement
% 'errtab' : table of errors
% 'cortab' : table of corrections
% 'dfrf_Hz': frequency variation for lattice off energy
% 
% example:
% load('/mntdirect/_machfs/liuzzo/EBS/beamdyn/matlab/optics/sy/booster_2018/betamodel.mat')
% sy.CreateOpticsFiles(betamodel,'test')
% sy.CreateOpticsFiles(getbooster(1),'test','dfrf_Hz',0)
%see also: SetOperationOptics

global APPHOME
p=inputParser;
addRequired(p,'r',@iscell);
addRequired(p,'opticsname',@ischar);
addOptional(p,'OperationFolder',APPHOME);
addOptional(p,'MeasuredResponse',[]);
addOptional(p,'ErrTab',[],@istable);
addOptional(p,'CorTab',[],@istable);
addOptional(p,'dfrf_Hz',0,@isnumeric);

parse(p,r,opticsname,varargin{:});
r           = p.Results.r;
opticsname  = p.Results.opticsname;
OpeFolder   = p.Results.OperationFolder;
rm          = p.Results.MeasuredResponse;
errtab      = p.Results.ErrTab;
cortab      = p.Results.CorTab;
dfrf_Hz      = p.Results.dfrf_Hz;

if isempty(errtab)
    errtab = atcreateerrortable(r);
end

if isempty(cortab)
    cortab = atgetcorrectiontable(r,r); % zeros
end

fulfold=fullfile(OpeFolder,'optics','sy',opticsname);

curdir=pwd;
mkdir(fulfold);
cd(fulfold);

% AT lattice
betamodel=r;
save('betamodel.mat','betamodel');

% AT lattice for simulator with errors and corrections
disp('errors and correction lattice for simulator (provide errtab and cortab or zeros will be set)')
errmodel=atseterrortable(r,errtab);
errmodel=atsetcorrectiontable(errmodel,cortab);
save('errmodel.mat','errmodel','cortab','errtab');


% save image of lattice optics
f=figure;
atplot(r);
saveas(gca,[opticsname '.jpg']);
close(f);

% layout file for closed orbit application
atwriteacu([r;r(1:ceil(length(r)/3)+10)],[opticsname '_layout.txt']);

rfull=sy.model(r);

% get fast model (QF8D merged, not ok for optics computations)
rmod=sy.model(r,'reduce',true,'keep','BPM.*|ID.*');

% orbit response, theory always,
sy.autocor_model(rfull,'dfrf_Hz',dfrf_Hz);

% measured if required
if ~isempty(rm)
sy.autocor_model(rfull,'exp',rm);
end

  
cd(curdir);

end
