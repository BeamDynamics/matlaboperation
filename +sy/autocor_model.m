function [h_resp,v_resp,h_rmot,v_rmot]=autocor_model(varargin)
%AUTOCOR_MODEL Creates data files for the autocor device server
%
%[RESPH,RESPV]=SY.AUTOCOR_MODEL(PATH)
%PATH:  Data directory (Default: pwd)
%
%[RESPH,RESPV]=SY.AUTOCOR_MODEL(..,'exp',...)
%   Use a measured response matrix
%
%[RESPH,RESPV]=SY.AUTOCOR_MODEL(..,'small',...)
%   selects a subset of BPMs for each plance
%
%[RESPH,RESPV]=SY.AUTOCOR_MODEL(..,'dfrf_Hz',43e3...)
%   RM for lattice off energy by 43kHz

args={pwd};
[exper,options]=getflag(varargin,'exp');
[small,options]=getflag(options,'small');
[dfrf_Hz,options]=getoption(options,'dfrf_Hz', 0.0);
args(1:length(options))=options;
pth=args{1};

[coefh,coefv]=load_corcalib('sy');

% lattice on energy
atmodel=sy.model(pth);
dfrf_lab ='';
if dfrf_Hz~=0
    dfrf_lab =['_' num2str(dfrf_Hz*1e-3,'%2.0fkHz')];
end
ring= atmodel.ring;
RF_Voltage=9e6; % change RF voltage if needed
ring=atsetcavity(ring,RF_Voltage,0,352); % set nominal frequency (on energy)
indcav=findcells(ring,'Class','RFCavity');
f0=ring{indcav(1)}.Frequency;
for ii=1:length(indcav)
    ring{indcav}.Frequency=f0+dfrf_Hz; 
end
% compute reference orbit
[indbpm]=atmodel.select('bpm');
orb=findorbit6(ring,indbpm);
orbh=orb(1,:)';
orbv=orb(3,:)';
dlmwrite(['reforbit' dfrf_lab '.csv'],[orbh,orbv]);

% lattice off energy
atmodel=sy.model(ring);

[bpmidx,sthidx,stvidx]=atmodel.select('bpm','steerh','steerv');
[bpm,steerh,steerv,qp]=atmodel.get('bpm','steerh','steerv','qp');
qstrength=atmodel.get(1,'qp');
nb=size(bpm,1);
nsth=size(steerh,1);
nstv=size(steerv,1);

if small                                            % For old server
    [h_bpms,v_bpms]=atmodel.get('h_bpms','v_bpms');	% For old server
else                                                % For old server
    h_bpms=true(nb,1);                              % For old server
    v_bpms=true(nb,1);                              % For old server
end                                                 % For old server

if ~exist('./alpha','file')
    savetext('alpha','%.4e',atmodel.alpha);
end

fprintf('Computing horizontal response\n');
h_resp=sy.orm(atmodel.ring,'h',sthidx,bpmidx).*coefh;
fprintf('Computing vertical response\n');
v_resp=sy.orm(atmodel.ring,'v',stvidx,bpmidx).*coefv;
fprintf('Computing frequency response\n');
dct=4.e-4;
forb=findsyncorbit(atmodel.ring,dct,bpmidx);
h_fresp=-forb(1,:)'/dct.*atmodel.ll*atmodel.ll/352/2.997924e8;
if exper
    [hr,vr,b_ok]=sy.load_resp(fullfile(pth,'expresp.mat'));
    sh_ok=true(1,nsth);
    sv_ok=true(1,nstv);
    h_resp(b_ok,:)=hr(b_ok,:);
    v_resp(b_ok,:)=vr(b_ok,:);
    matmode='measured';
else
    b_ok=true(nb,1);
    sh_ok=true(1,nsth);
    sv_ok=true(1,nstv);
    matmode='theoretical';
end

hv_bpms=[bpm(:,1:2) 2*pi*bpm(:,3) bpm(:,4:5) 2*pi*bpm(:,6)];
bpm_name=sy.bpmname((1:nb)');
store_bpm(['bpms' dfrf_lab ''],hv_bpms,bpm_name);

h_steerers = [steerh(:,1:2) 2*pi*steerh(:,3) coefh*ones(nsth,1)];
v_steerers = [steerv(:,[1 5]) 2*pi*steerv(:,6) coefv*ones(nstv,1)];
steerh_name=sy.steername((1:nsth)','h');
steerv_name=sy.steername((1:nstv)','v');
store_steerer(['h_steerers' dfrf_lab ''],h_steerers,steerh_name);
store_steerer(['v_steerers' dfrf_lab ''],v_steerers,steerv_name);

[b,f,pu,npu]=bump_coefs(h_resp,bpm(:,1),steerh(:,1),1);
store_bump(['h_bumps' dfrf_lab ''],[b' pu-1 npu f],steerh_name);

[b,f,pu,npu]=bump_coefs(v_resp,bpm(:,1),steerv(:,1),1);
store_bump(['v_bumps' dfrf_lab ''],[b' pu-1 npu f],steerv_name);

qind=atmodel.get(0,'qp');

qplist = {'QF30','QF37','QF39','QF4','QF10','QF17','QF22','QF26'};
ih=sy.qpselect(qplist);

[h_rmot]=sy.orm(atmodel.ring,'h',qind(ih),bpmidx);

qplist = {'QD29','QD3','QD12','QD18'};
iv=sy.qpselect(qplist);
[v_rmot]=sy.orm(atmodel.ring,'v',qind(iv),bpmidx);

h_motors = [qp(ih,1:2) 2*pi*qp(ih,3) qstrength(ih)];
v_motors = [qp(iv,[1 5]) 2*pi*qp(iv,6) -qstrength(iv)];
motorh_name=sy.qpname(ih','sy/d-mot/%s%d');
motorv_name=sy.qpname(iv','sy/d-mot/%s%d');
store_steerer(['h_motors' dfrf_lab ''],h_motors,motorh_name);
store_steerer(['v_motors' dfrf_lab ''],v_motors,motorv_name);

[b,f,pu,npu]=bump_coefs(h_rmot,bpm(:,1),qp(ih,1),1);
store_bump(['h_motorbumps' dfrf_lab ''],[b' pu-1 npu f],motorh_name);

[b,f,pu,npu]=bump_coefs(v_rmot,bpm(:,1),qp(iv,1),1);
store_bump(['v_motorbumps' dfrf_lab ''],[b' pu-1 npu f],motorv_name);

dlmwrite(['h_resp' dfrf_lab '.csv'],[h_resp,h_fresp]);
dlmwrite(['v_resp' dfrf_lab '.csv'],v_resp);
dlmwrite(['h_motresp' dfrf_lab '.csv'],[h_rmot,h_fresp]);
dlmwrite(['v_motresp' dfrf_lab '.csv'],v_rmot);
dlmwrite(['h_allresp' dfrf_lab '.csv'],[h_resp,h_rmot,h_fresp;zeros(size(h_resp)),h_rmot,h_fresp]);
dlmwrite(['v_allresp' dfrf_lab '.csv'],[v_resp,v_rmot;zeros(size(v_resp)),v_rmot]);


end

