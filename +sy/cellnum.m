function [kdx,celnum,id]=cellnum(npercell,varargin)
%CELLNUM   gives cell number and index in cell
%
%[KDX,CELL,ID]=CELLNUM(NPERCELL,IDX) returns the cell number and the index in cell
%   of the IDX'th elements in a vector with NPERCELL elements per cell,
%   starting at the beginning of cell 28
%
% IDX is any of:
%       1) logical array of length 39*NPERCELL
%       2) vector of indexes in the range [1 39*NPERCELL] (1 is Cell 28/1st item)
%
%[KDX,CELL,ID]=CELLNUM(NPERCELL,CELL,ID)
%[KDX,CELL,ID]=CELLNUM(NPERCELL,[CELL,ID])
%       CELL in [1 32], ID in [1 NPERCELL]
%
%See also: bpmname, bpmindex, qpname, sxname

[celnum,id,kdx]=getcellnum(npercell,39,27,varargin{:});
end
