function [idx,kdx,plane] = steerindex(arg1)
%STEERINDEX	find index of Steerer with name steername
%
%IDX=STEERINDEX(STEERNAME)
%   STEERNAME:    string, char array or cell array of strings
%
%[IDX,KDX,PLANE]=STEERINDEX(...)
%   Returns the hardware index and the plane of the last steerer in the list
%

if iscell(arg1)
    kdx=cellfun(@bfind,arg1);
else
    kdx=cellfun(@bfind,cellstr(arg1));
end
idx=kdx-27;
neg=(idx<=0);
idx(neg)=idx(neg)+39;

    function kdx=bfind(name)
        bpattern='^(?:.+/.+/)?c([hv])(\d{1,2})$';
        toks=regexpi(name,bpattern,'tokens');
        plane=upper(toks{1}{1});
        kdx=str2double(toks{1}{2});
    end
end

