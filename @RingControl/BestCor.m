function [dc, ibest,corstd,Xaftercor]= BestCor(RM,X)
% get best corrector and its strength 
%
% INPUTS:
% -RM : response matrix (NxM)
% - X : vector to be corrected (Nx1)
% 
% OUTPUTS:
% - dc : delta correctors strengths, all zeros apart the one of the best cor.
% - idbest : index of the best corrector
%
%

scalefact = RM\X; % vector of corrections factors [m]/[m/rad] = [rad]

corstd = zeros(size(scalefact));
for ic = 1:length(scalefact)
    Y = scalefact(ic)*RM(:,ic);
    corstd(ic) = std(X-Y);
end

[~,ibest] = min(corstd); % 

dc = zeros(size(scalefact));

dc(ibest) = scalefact(ibest);

Xaftercor = X-scalefact(ibest)*RM(:,ibest);

doplot = true;
if doplot
    
    if size(RM,2) == 384 || size(RM,2) == 385
        name = sr.hsteername(ibest);
    elseif size(RM,2)== 288 || size(RM,2)== 289
        name = sr.vsteername(ibest);
    else
        name = [num2str(ibest) 'th '];
    end
    
    figure;
    plot(X,'LineWidth',2);
    hold on; 
    plot(scalefact(ibest)*RM(:,ibest),'LineWidth',2); 
    plot(X-scalefact(ibest)*RM(:,ibest),'LineWidth',2);
    legend(['measured: ' num2str(std(X))],...
        [name ': ' num2str(std(scalefact(ibest)*RM(:,ibest)))],...
        ['final: ' num2str(std(X-scalefact(ibest)*RM(:,ibest)))]);
    xlabel('BPM #'); 
end

end
