classdef RingControl < handle
    %RINGCONTROL for common Beam Dynamics experiments
    %
    %   Base class to store methods and properties often used in
    %   BeamDynamics Applications.
    %
    %   Allows to:
    %   read and set correctors, septa123, CV89 (TL2),
    %   read orbit
    %   read trajectory
    %   read and set RF frequency
    %
    %   can be used for: ebs-simu, sr, sy
    %
    %   includes:
    %   commonly used methods such as svd
    %   calibration for magnets (sr, sy only)
    %
    %
    %see also: First2Turns
    
    properties
        
        machine
        
        MeasurementFolder % folder where to store data
        first_turn_index % index of first turn in TbT
        
        s12 %  S12
        s3  %  S3
        cv8 %  CV8
        cv9 %  CV9
        sh % horizontal SR steerers
        sv % vertical SR steerers
        rf % RF frequency
        quad % quadrupoles
        sext % sextupoles
        octu % octupoles
        skew % skew quadrupole correctors
        
        ke % device to control KE
        gun % device to control Gun
        rips % device to control RIPS
        
        scraper % scraper device
        
        hor_scraper % horizontal scraper device
        
        % TbT Data
        hor_bpm_TBT % horizontal BPM attribute
        ver_bpm_TBT % vertical BPM attribute
        sum_bpm_TBT % BPM sum attribute
        getTbTdata  % function to retrive the TbT data (wait for trigger counter and readout)
        
        % Slow Acquisition Data
        hor_bpm_SA % horizontal BPM attribute
        ver_bpm_SA % vertical BPM attribute
        sum_bpm_SA % BPM sum attribute
        getSAdata  % function to retrive the TbT data (wait for trigger counter and readout)
        
        bpm_trigger_counter % counter that changes with fresh BPM data
        bpm_trigger_counter_step % step in counter to consider new data
        
        sum_signal_beam % sum signal with beam (sensitivity of each BPM)
        sum_signal_background % sum signal without beam (background)
        sum_signal_threshold % sum signal below this value is not considered
        
        % bpm tilt and offset
        hor_bpm_tilt
        ver_bpm_tilt
        hor_bpm_offset
        ver_bpm_offset
        
        
        status_bpm % BPM status (1 means no error, so ok)
        state_hst % H steerer state (ON OFF FAULT,...)
        state_vst % V steerer state (ON OFF FAULT,...)
        state_skew % skew quad state (ON OFF FAULT,...)
        state_quad % normal quad state (ON OFF FAULT,...)
        state_sext % sextupoles state (ON OFF FAULT,...)
        state_oct % octupoles state (ON OFF FAULT,...)
        
        orbit_cor_disabled_bpm  % BPM not in orbit correction
        orbit_cor_disabled_hst  % H steerer not in orbit correction
        orbit_cor_disabled_vst  % V steerer not in orbit correction
        autocor                 % autocor device
        
        blm_TBT         % losses turn-by-turn
        blm_SA          % integrated losses
        
        h_tune          % horizontal tune
        v_tune          % vertical tune
        h_tune_shaker          % horizontal tune shaker
        v_tune_shaker          % vertical tune shaker
        
        h_chromaticity          % horizontal chromaticity 
        v_chromaticity          % vertical chromaticity 
        
        
        h_emittance     % hor. emittance
        v_emittance     % ver. emittance
        h_emittance_err     % hor. emittance absolute error
        v_emittance_err     % ver. emittance absolute error
        lifetime        % total lifetime
        
        neutrons        % neutron detectors (SAFETY)
        
        stored_current % current monitor device
        max_stored_current % maximum accepted current before scraper acts (used by KillBeamAtGivenCurrent)
        initial_coordinates % device to control initial coordinates given to the simulator
        
        kick % injection bump kickers strengths
        K3 % K3 strenght to set for simulated trajectory
        K4 % K4 strenght to set for simulated trajectory
        injoff % injected - stored beam offset
        on_axis % boolean defined at init only to add static bump at inj.
        
        noise % boolean to add noise to the measurements in the simulator
        injectorRMSNoise % (1x6) array of rms noise on the injecting coordinates
        
        rmodel % AT lattice model to use for simulation (RM)
        tl2model % AT lattice model of Transferline to ring
        
        savefile % flag to determine file saving on acquisition
        selturns % turns to use for correction
        n_acquisitions  % number of acquisitions
        last_measured_trajectory % store last measured trajectory
        TrajFileName % label to append to measurements
        
        last_measured_orbit % store last measured closed orbit
        OrbitFileName % label to append to measurements
        
        % kill beam if above current limit
        KillBeamAtGivenCurrent% @(obj,varargin)fun;
        
        indHst  % indexes of horizontal steerers in AT rmodel
        indVst  % indexes of vertical steerers in AT rmodel
        indBPM  % indexes of BPM in AT rmodel
        indQuad % indexes of Quadrupoles in AT rmodel
        indSext % indexes of Sextupoles in AT rmodel
        indOctu % indexes of Octupoles in AT rmodel
        indSkew % indexes of Skew Quadrupole Correctors in AT rmodel
        indKickers % injection kicker indexes
        
        % properties below should be private
        
        nturns % number of turns to measure (use setTurns)
        
        % tune estimate and expected
        phase_advance_measured
        phase_advance_expected
        
        full_tune_measured
        full_tune_expected
        
        fast_response_matrix % device name to pilot fast respone matrix measurement
        
        lastbpm
        
    end
    
    methods
        
        function obj = RingControl(machine,varargin)
            %RINGCONTROL Construct an instance of this class
            %
            %   Base class to store methods and properties often used in
            %   BeamDynamics Applications.
            %
            %   Allows to:
            %   read and set correctors, septa123, CV89 (TL2),
            %   read orbit
            %   read trajectory
            %   read and set RF frequency
            %
            %   can be used for: ebs-simu, sr, sy
            %
            %   optional input:
            %   ...,'on_axis', false,...  if true a static bump is added at
            %                             injection
            %
            %   includes:
            %   commonly used methods such as svd
            %   calibration for magnets (esrf-sr, esrf-sy only)
            %
            %
            %see also: First2Turns
            
            validmachines={'sr','sy','ebs-simu','none'};
            p = inputParser;
            addRequired(p,'machine',@(x)any(validatestring(x,validmachines)));
            addOptional(p,'on_axis',false,@islogical);
            addOptional(p,'atlattice',[],@iscell);
            %addParameter(p,'machine',validmachines{1},);
            
            parse(p,machine,varargin{:});
            
            obj.rmodel = p.Results.atlattice;
            
            obj.on_axis = p.Results.on_axis;
            
            obj.machine = machine;
            obj.n_acquisitions = 5;
            obj.nturns=2;
            obj.last_measured_trajectory = [];
            obj.bpm_trigger_counter_step = 1;
            
            obj.max_stored_current = 1;
            
            obj.TrajFileName='';
            
            % chose lattice to initialize
            switch machine
                case 'ebs-simu'
                    obj.InitEbsSimulator;
                    
                case 'sr'
                    obj.InitEsrfSR;
                    
                case 'sy'
                    obj.InitEsrfSY;
                    
                case 'soleil'
                    obj.InitSoleil;
                    
                otherwise
                    warning(['Suported machines are: '...
                        '''sr'', ''sy'', ''ebs-simu'',''soleil'''])
                    obj.InitEmpty;
            end
            
            % set selturns correctly
            obj.setTurns;
            
        end
        % methods defined in seprated functions
        
        %Switch Initialization to EBS simulator
        InitEbsSimulator(obj);
        
        InitEbsSimulatorOnAxis(obj);
        
        InitEsrfSR(obj);
        InitEsrfSR_Offaxis(obj);
        
        InitEsrfSY(obj);
        
        InitEmpty(obj);
        
        %% functions lattice and Control System dependent! ESRF SR 2018
        function setTurns(obj,n)
            % define number of turns to keep for trajectory measurement
            % after the first_turn_index turn.
            % with no inputs, property nturns is used as number of turns.
            %
            if nargin<2
                n = obj.nturns;
            end
            obj.nturns = n;
            
            obj.selturns = obj.first_turn_index + (0:(obj.nturns-1)); % first 2 turns parentheses needed.
        end
        
        
        
        function AddSteererPolarityErrors(obj,ErrCalibHSt,ErrCalibVSt)
            % AddSteererPolarityErrors(obj,ErrCalibHSt,ErrCalibVSt)
            %
            %   multiply the present calibration of the steerers by the
            %   value passed as input.
            %
            %   ErrCalibHst is a vector with the same size of getHSteerersValues
            %   ErrCalibVst is a vector with the same size of getVSteerersValues
            %
            
            obj.sh.calibration = obj.sh.calibration.*ErrCalibHSt;
            obj.sv.calibration = obj.sv.calibration.*ErrCalibVSt;
            
        end
        
        
        function enabledhst = getEnabledHsteerers(obj)
            % returns a vector of true/false for correctors On and enabled
            % in orbit correction
            
            enabledhst = obj.state_hst();
            
            try
                codisab = obj.orbit_cor_disabled_hst();
            catch
                warning(['could not read hor disabled: ']);
                codisab=[];
            end
            
            enabledhst(codisab+1) = false; % Device not in orbit correction (+1 to convert from C indexeing)
            
        end
        
        function enabledvst = getEnabledVsteerers(obj)
            % returns a vector of true/false for correctors On and enabled
            % in orbit correction
            
            enabledvst = obj.state_vst(); % device not On
            
            try
                codisab = obj.orbit_cor_disabled_vst();
            catch
                warning(['could not read ver disabled : ']);
                codisab=[];
            end
            
            enabledvst(codisab+1) = false; % Device not in orbit correction (+1 to convert from C indexeing)
            
        end
        
        function enabledbpm= getEnabledBPM(obj)
            % returns a vector of true/false for BPM with Status=0 (no error)
            %  and enabledin orbit correction
            
            enabledbpm = obj.status_bpm() ;
            
            try
                codisab = obj.orbit_cor_disabled_bpm();
            catch
                warning(['could not read bpm disabled: ' ]);
                codisab=[];
            end
            
            enabledbpm(codisab+1) = false; % Device not in orbit correction (+1 to convert from C indexeing)
            
        end
        
        function setNoise(obj,injectorRMSNoise)
            %  setNoise(obj,injectorRMSNoise)
            %  set noise on the injected coordinates
            %
            obj.injectorRMSNoise=injectorRMSNoise;
            obj.noise=true;
        end
        
        
        function UpdateATLattice(obj,ring)
            % update AT lattice indexes if needed.
            
            r0 =obj.rmodel; % store lattice before replacing it.
            
            if isempty(ring)
                ring = obj.rmodel;
            end
            
            if isempty(ring)
                error('no input lattice nor obj.rmodel property');
            end
            
%             % reduce
%             ring = atreduce(ring,....
%                 atgetcells(ring,'FamName',...
%                 '^BPM_..|^ID.*|JL1[AE].*|CA[02][57]'));

            
            if obj.compare_at_lattices(ring,r0)
                
                if ~any([isempty(obj.indQuad),...
                     isempty(obj.indSext),...
                     isempty(obj.indOctu),...
                     isempty(obj.indHst),...
                     isempty(obj.indVst),...
                     isempty(obj.indBPM),...
                     isempty(obj.indKickers),...
                     isempty(obj.indSkew)...
                     ])
                 
                    disp(['Indexes already exist for identical lattices. '...
                        'No index recomputation needed']);
                    
                    return
                else
                    disp('Some indexes missing. Updating all.')
                end
                
            else
                % transform cell to string to compare
                disp(['Using user defined lattice instead of '...
                    '/operation/beamdyn/matlab/optics/sr/theory. '...
                    'Recomputing indexes.']);
            end
            
           
            mod = sr.model(ring);
            
            % % mod.settune([76.675 27.251])
            %
            
            obj.indKickers = mod.get(0,'kickers');
            obj.indBPM = mod.get(0,'bpm')';
            obj.indHst = sort([mod.get(0,'steerh'); mod.get(0,'dq')])';
            obj.indVst = mod.get(0,'steerv')';
            obj.indSkew = mod.get(0,'skew')';
            
            % make sure steerers have KickAngle.
            ring = atsetfieldvalues(ring,obj.indHst,'KickAngle',{1,1},0);
            ring = atsetfieldvalues(ring,obj.indHst,'KickAngle',{1,2},0);
            ring = atsetfieldvalues(ring,obj.indVst,'KickAngle',{1,1},0);
            ring = atsetfieldvalues(ring,obj.indVst,'KickAngle',{1,2},0);
            
            % quadrupole indexes (BAD FOR QF8D of 2PW cells)
            qpind = mod.get(0,'qp');
            qf8dind = mod.get(0,'qf8d');
            obj.indQuad=sort([qpind; qf8dind(1:50:end)])';
            
            obj.indSext = mod.get(0,'sx')';
            obj.indOctu = mod.get(0,'oc')';
            
            ring=atsetcavity(atradon(ring),6.5e6,1,992);
            
            obj.rmodel = ring;
            
        end
        
        % trajectory measurement method
        [t,errt] = measuretrajectory(obj);
        
        % compute n turns reference trajectory
        [t0] = findNturnstrajectory6Err(obj,ring,K,CV,S,incod);
        
        % display last trajectory measurement
        plottrajectory(obj,varargin);
        
        % orbit measurement method
        [t,errt] = measureorbit(obj,varargin);
        
        % display last orbit measurement
        plotorbit(obj,varargin);
        
        % booster TDP data reading and reshaping
        [h,v,s,pt,st] = ReadTDPSpark(obj);
        
        %trajectory simulation method
        [t] = SimulateTrajectoryKick(obj,indSt,kick,plane);
        
        % generic SVD to chose among different options
        dq = svd(obj,svd_mode,varargin);
        
        estimatetune(obj);
        measuretune(obj,varargin);
        
        % set cod bump
        [shn,svn,sh0,sv0]=set_cod_bump(obj,varargin);
        
        [shn,svn,sh0,sv0,r]=set_any_cod_bump(obj,varargin);
    end
    
    methods (Static)
        
        % from other software in CTRM
        dq = qemsvd_Tikhonov(a,b,lambda,plot);
        dq = qemsvd(a,b,neig);
        [dq, ibest,corstd,XafterCor]= BestCor(RM,X);
        
        % from matlab central: Per Christian Hansen, IMM, 06/22/93.
        [U,s,V] = csvd(A,tst)
        [x_lambda,rho,eta] = tikhonov(U,s,V,b,lambda,x_0);
         
        function [are_equal, reason ]= compare_at_lattices(r1,r2)
            % compare to AT lattices. 
            % are_equal =  boolean. true if lattices are identical.
            % reason = [] if they are identical
            % reason = 0 if length is different
            % reason = [array of ideces] list of different elements
            
            L1 = length(r1);
            L2 = length(r2);
            
            if L1 ~= L2
                are_equal=false;
                reason = 0; % length
            else % compare element by element
                reason = cellfun(@(a,b)isequaln(a,b),r1,r2,'un',1)==0;
                are_equal = isempty(find(reason,1));
            end
        end
        
    end
    
end

