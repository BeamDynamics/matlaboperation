function measuretune(obj,varargin)
% MEASURETUNE measures the tune from the difference of two trajectories
% 
% optional inputs:
% ...,'h_steerer_ind',34,...        scalar index 1 - 384 of the h steerer
%                                   to use instead of Septum S3
% ...,'v_steerer_ind',47,...        scalar index 1 - 288 of the v steerer to use
%                                   to use instead of CV9
% ...,'h_bpm_ind',1:70,...          array of index 1 - 320 of the h bpm to use for tune estimate
% ...,'v_bpm_ind',89:200,...        array of index 1 - 320 of the h bpm to use for tune estimate
% ...,'KickAngle',[2e-4 2e-4],...   amplitudes of kick in rad
% ...,'plot',true,...               logical flag to plot or not
% 
% Example of usage:
% 1) 
% rc = RingControl('sr');
% rc.measuretune()
% 
% 2) 
% rc = RingControl('sr');
% rc.measuretune('h_steerer_ind',34,'v_steerer_ind',47,'h_bpm_ind',1:70,'v_bpm_ind',89:200,'KickAngle',[2e-4 2e-4],'plot',true)
% 
%see also: findtunetom2 FirstTurns.findNturnstrajectory6Err
%RingControl.plotmeasuredtune 

p=inputParser;
addRequired(p,'obj');
addParameter(p,'h_steerer_ind',[],@isnumeric);
addParameter(p,'v_steerer_ind',[],@isnumeric);
addParameter(p,'h_bpm_ind',[1:320],@isnumeric);
addParameter(p,'v_bpm_ind',[1:320],@isnumeric);
addParameter(p,'KickAngle',[2e-4 1e-4],@isnumeric);
addParameter(p,'plot',true,@islogical);
parse(p,obj,varargin{:});

indsh = p.Results.h_steerer_ind;
indsv = p.Results.v_steerer_ind;
indbh = p.Results.h_bpm_ind;
indbv = p.Results.v_bpm_ind;
KLval = p.Results.KickAngle;
doplot = p.Results.plot;

disp('measure reference trajectory')
t = obj.measuretrajectory;

selh = ~isnan(t(1,:));
selv = ~isnan(t(2,:));

h0 = t(1,selh)';
v0 = t(2,selv)';

% store last measured trajectory without kick
last_traj = obj.last_measured_trajectory;

% set corrector H to different values
if ~isempty(indsh)
    disp(['measure trajectory with Hor kick of ' num2str(KLval(1)*1e6,'%2.1f') ' murad @ ' sr.hsteername(indsh)])
    hcor = obj.sh.get;
    hcormod = hcor;
    hcormod(indsh) = hcormod(indsh) + KLval(1);
    obj.sh.set(hcormod);
    
else % use s3
    s30 = obj.s3.get;
    obj.s3.set(s30+KLval(1));
end

pause(5); disp('wait for PS')

t = obj.measuretrajectory;
h = t(1,selh)';

if ~isempty(indsh)
    obj.sh.set(hcor);
else
    obj.s3.set(s30);
end

if ~isempty(indsv)
    disp(['measure trajectory with Ver kick of ' num2str(KLval(2)*1e6,'%2.1f') ' murad @ ' sr.vsteername(indsv)])
    vcor = obj.sv.get;
    vcormod = vcor;
    vcormod(indsv) = vcormod(indsv) + KLval(2);
    obj.sv.set(vcormod);
else
    cv90 = obj.cv9.get;
    obj.cv9.set(cv90 + KLval(2)/(-0.085*1e-3));
end
pause(5); disp('wait for PS')

% get trajectory with kick
t = obj.measuretrajectory;

v = t(2,selv)';

% restore last measured trajectory without kick
obj.last_measured_trajectory = last_traj ;

if  ~isempty(indsv)
    % resotre corrector
    obj.sv.set(vcor);
else
    % restore CV9
    obj.cv9.set(cv90);
end

% estimate tune from difference of measured trajectories
disp('fft for tune and phase advance from measurement')
DH_traj_measured = h-h0;
DV_traj_measured = v-v0;

lastbpm = length(h);
[fftx] = findtunetom2(DH_traj_measured,200,1);
[ffty] = findtunetom2(DV_traj_measured,200,1);
mufft =[fftx ffty]*lastbpm;
[fftx] = findtunetom2(DH_traj_measured(indbh),200,1);
[ffty] = findtunetom2(DV_traj_measured(indbv),200,1);
Qfft =[fftx ffty]*320;

% estimate tune from model
disp('model reference trajectory')
o_in=findorbit4(obj.rmodel,0,1);
    
trmodel = obj.findNturnstrajectory6Err(obj.rmodel,[0 0],[0 0],[0 0],[o_in; 0; 0]);
h0 = trmodel(1,1:lastbpm)' ;
v0 = trmodel(3,1:lastbpm)' ;

disp(['model trajectory with Hor kick of ' num2str(KLval(1)*1e6,'%2.1f') ' murad @ ' sr.vsteername(indsv)])

rkick=obj.rmodel;
indsteer = obj.indHst(indsh); 
rkick{indsteer}.PolynomB(1) = -KLval(1)./rkick{indsteer}.Length; % minus sign to convert kick angle to polynomB
 
trmodelkick = obj.findNturnstrajectory6Err(rkick,[0 0],[0 0],[0 0],[o_in; 0; 0]);
h = trmodelkick(1,1:lastbpm)' ;

disp(['model trajectory with Ver kick of ' num2str(KLval(2)*1e6,'%2.1f') ' murad @ ' sr.vsteername(indsv)])

rkick=obj.rmodel;
indsteer = obj.indVst(indsv); 
rkick{indsteer}.PolynomA(1) = KLval(2)./rkick{indsteer}.Length;

trmodelkick = obj.findNturnstrajectory6Err(rkick,[0 0],[0 0],[0 0],[o_in; 0; 0]);
v = trmodelkick(3,1:lastbpm)' ;

% estimate tune from model
DH_traj_model=h-h0;
DV_traj_model=v-v0;
[fftx] = findtunetom2(DH_traj_model(indbh),200,1);
[ffty] = findtunetom2(DV_traj_model(indbv),200,1);
Qmodel =[fftx ffty]*320;
[fftx] = findtunetom2(DH_traj_model,200,1);
[ffty] = findtunetom2(DV_traj_model,200,1);
mumodel =[fftx ffty]*lastbpm;

if any( abs(Qmodel - Qfft) > 0.5 )
    
    disp('suggeseted tune correction. use this matlab command:');
    disp(['movetune([' num2str(Qmodel) '],[' num2str(Qfft) '],''setcur'',true)']);
    
end

obj.phase_advance_measured = mufft;
obj.phase_advance_expected = mumodel;

obj.full_tune_measured = Qfft;
obj.full_tune_expected = Qmodel;

obj.lastbpm = lastbpm;

disp(['Measured Tune: ' num2str(obj.full_tune_measured)]);
disp(['Expected Tune: ' num2str(obj.full_tune_expected)]);
disp(['Delta Tune (Measured- Expected): ' num2str(obj.full_tune_measured - obj.full_tune_expected)]);

disp(['Measured Phase: ' num2str(obj.phase_advance_measured)]);
disp(['Expected Phase: ' num2str(obj.phase_advance_expected)]);
disp(['Delta Phase (Measured- Expected): '...
    num2str(obj.phase_advance_measured - obj.phase_advance_expected)]);

% display measurement 
if doplot
    nt = ceil(lastbpm/320);
    C = findspos(obj.rmodel,length(obj.rmodel)+1);
    s = findspos(obj.rmodel,obj.indBPM);
    sall = []; shb =[]; svb=[];
    for ii=1:nt
        sall = [sall s+C*(ii-1)];
    end
    sall_measured = sall(1:length(DH_traj_measured));
    sall_model = sall(1:length(DH_traj_model));
    
    figure('units','normalized','position',[0.2 0.2 0.7 0.6]);
    subplot(2,1,1)
    ax =gca;
    ax.FontSize = 12;
    plot(sall_measured,DH_traj_measured*1e6,'x-','LineWidth',2);hold on;
    plot(sall_model,DH_traj_model*1e6,'x-','LineWidth',2);hold on;
    line(sall_model([min(indbh) min(indbh)]),[-5 5]*1e3,'Color',[0 0 0]);
    line(sall_model([max(indbh) max(indbh)]),[-5 5]*1e3,'Color',[0 0 0]);
    %ax.XLim=[min(indbh) max(indbh)] + [-1 +1];
    
    xlabel('BPM #');
    ylabel('trajectory kick H - ref [\mum]')
    legend('\Delta hor. measured',...
        '\Delta hor. model');
    title({['Measured \phi: ' num2str(obj.phase_advance_measured) '    Measured Q: ' num2str(obj.full_tune_measured)],...
        ['Expected \phi: ' num2str(obj.phase_advance_expected) '    Expected Q: ' num2str(obj.full_tune_expected)],...
        [],['Hor kick of ' num2str(KLval(1)*1e6,'%2.1f') ' murad @ ' sr.hsteername(indsh)]});
    grid on;
    
    subplot(2,1,2)
    ax =gca;
    ax.FontSize = 12;
    plot(sall_measured,DV_traj_measured*1e6,'s-','LineWidth',2);hold on;
    plot(sall_model,DV_traj_model*1e6,'s-','LineWidth',2);hold on;
    xlabel('BPM #');
    ylabel('trajectory kick V - ref [\mum]')
    legend('\Delta ver.  measured',...
        '\Delta ver. model');
    title({[],['Ver. kick of ' num2str(KLval(2)*1e6,'%2.1f') ' murad @ ' sr.vsteername(indsv)]});
    line(sall_model([min(indbv) min(indbv)]),[-5 5]*1e3,'Color',[0 0 0]);
    line(sall_model([max(indbv) max(indbv)]),[-5 5]*1e3,'Color',[0 0 0]);
    %ax.XLim=[min(indbv) max(indbv)] + [-1 +1];
    
    grid on;
    
end

% save data
if obj.savefile
    save(fullfile(obj.MeasurementFolder,...
        ['FirstTurnsTuneMeasurement' datestr(now,'YYYYmmmDD_HHMMSS') obj.TrajFileName]),...
        'DV_traj_measured','DH_traj_measured',...
        'DV_traj_model','DH_traj_model',...
        'KLval',...
        'mufft','mumodel','Qfft','Qmodel',...
        'indsh','indsv','indbh','indbv');
end