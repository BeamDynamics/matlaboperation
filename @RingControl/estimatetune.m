function estimatetune(obj)
% ESTIMATETuNE estimates the tune from the measured trajectory
%
%
%see also: findtunetom2 FirstTurns.findNturnstrajectory6Err

Qfft = [0 0];
Qmodel = [0  0];

mufft = [0 0];
mumodel = [0 0];


if ~isempty(obj.last_measured_trajectory)
    
    % get tunes
    h = obj.last_measured_trajectory(1,~isnan(obj.last_measured_trajectory(1,:)))' ;
    v = obj.last_measured_trajectory(2,~isnan(obj.last_measured_trajectory(2,:)))' ;
    lastbpm = length(h);
    [fftx] = findtunetom2(h,200,1)*lastbpm;
    [ffty] = findtunetom2(v,200,1)*lastbpm;
    mufft =[fftx ffty];
    
    trmodel = obj.findNturnstrajectory6Err(obj.rmodel,[0 0],[0 0],[0 0],[0 0 0.00001 0 0 0]');
    h0 = trmodel(1,1:lastbpm)' ;
    v0 = trmodel(3,1:lastbpm)' ;
    [fftx] = findtunetom2(h0,200,1)*lastbpm;
    [ffty] = findtunetom2(v0,200,1)*lastbpm;
    mumodel =[fftx ffty];
    
    
    if lastbpm > 320
        lastbpmt = 320; % tune instead of total phase advance
        integercell = floor(lastbpm/10)*10; % use trajectory over an integr number of cells
        
        [fftx] = findtunetom2(h(1:integercell),200,1)*lastbpmt;
        [ffty] = findtunetom2(v(1:integercell),200,1)*lastbpmt;
        Qfft =[fftx ffty];
        
        [fftx] = findtunetom2(h0(1:integercell),200,1)*lastbpmt;
        [ffty] = findtunetom2(v0(1:integercell),200,1)*lastbpmt;
        Qmodel =[fftx ffty];
        
    end
  
    if any( abs(Qmodel - Qfft) > 0.5 )
    
        disp('suggeseted tune correction. use this matlab command:');
        disp(['movetune([' num2str(Qmodel) '],[' num2str(Qfft) '],''setcur'',true)']);
        
    end
    
end

obj.phase_advance_measured = mufft;
obj.phase_advance_expected = mumodel;

obj.full_tune_measured = Qfft;
obj.full_tune_expected = Qmodel;

obj.lastbpm = lastbpm;

