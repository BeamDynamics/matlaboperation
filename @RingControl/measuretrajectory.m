function [t,errt] = measuretrajectory(obj)
%MEASURETRAJECTORY measures trajectory for 2 turns
%
% read BPMs TbT to obtain trajectory in ring at BPM
%
% average of obj.n_acquisitions reading
% first turn obj.first_turn_index data stored for obj.nturns.
% to change number of turns use obj.setTurns(Nturns)
%
% OUTPUT: 
% 3x(NbpmxNturns) matrix.
% first row     = n horizontal turns mean of obj.n_acquisitions reading
% second row    = n vertical turns  mean of obj.n_acquisitions reading
% third row     = n sum signals, NAN if not good BPM  mean of obj.n_acquisitions reading
%
% 3x(NbpmxNturns) matrix.
% first row     = n horizontal turns std of obj.n_acquisitions reading
% second row    = n vertical turns std of obj.n_acquisitions reading
% third row     = n sum signals, NAN if not good BPM std of obj.n_acquisitions reading
%
% Requires :
% Gun On and RIPS Running. in not in good state, required to start
% Ke in single pulse, (switched on only for obj.machine='esrf-sr')
% Linac, syinj, syrf, RIPS, (srinj) : On
%
%see also: RingControl.plottrajectory RingControl.KillBeamAtGivenCurrent

% read n times orbit for 2 turns

filename = obj.TrajFileName;

% get data synchronized to KE for 2 turns, average over n acquisitions

average = obj.n_acquisitions;
selturns = obj.selturns; %[18,19]; % first and second turn in buffer

simulator = ~strcmp(obj.machine,'sr') & ~strcmp(obj.machine,'sy');

switch_on_gun_rips = false;
    
%% set up injector
if ~simulator % if real accelerator
    
    switch_on_gun_rips = true;
    
    gun  = tango.Device(obj.gun);
    rips = tango.Device(obj.rips);
    
    ripsrunning = strcmp(rips.State , 'Running');
    gunfiring = strcmp(gun.State , 'On');
    
    if  ripsrunning && gunfiring
        switch_on_gun_rips = false;
    end
    
    if  ripsrunning && ~gunfiring
        answer = questdlg('Gun is NOT On', ...
            'Switch On Gun please:', ...
            'I did it','Set Gun On','Stop measure','Set Gun On');
        
        switch answer
            case 'I did it'
                disp('Thank you for switching on the gun. ');
            case 'Set Gun On'
                gun.On();
                disp('gun On command sent');
            case 'Stop measure'
                disp('stop measurement');
                t = NaN(3,length(obj.indBPM)*obj.nturns);
                errt = t;
                return
        end
        disp('In this case this function will not handle gun On/Off and Rips Start/Stop.')
           
        switch_on_gun_rips = false;
    end
    
    if  ~ripsrunning && gunfiring
        questdlg('Rips is NOT Running', ...
            'Start Rips Rump please:', ...
             'I did it','Start RIPS Ramp','Stop measure','Start RIPS Ramp');
        
        switch answer
            case 'I did it'
                disp('Thank you for switching on the gun. ');
            case 'Start RIPS Ramp'
                rips.StartRamping();
                disp('RIPS StartRamping command sent');
            case 'Stop measure'
                disp('stop measurement');
                t = NaN(3,length(obj.indBPM)*obj.nturns);
                errt = t;
                return
        end
        disp('In this case this function will not handle gun On/Off and Rips Start/Stop.')
           
        switch_on_gun_rips = false;
    end
    
    % shoot beam: KE 1 shot, gun
    
    % set up devices for single shot, TbT injection
   if  strcmp(obj.machine,'sr')
       Ke      = tango.Device(obj.ke);
   end
    gun     = tango.Device(obj.gun);
    rips    = tango.Device(obj.rips);
  
end
%pn = Ke.PulseNumber.read;
%Ke.PulseNumber = 1; % set single pulse for extraction


if switch_on_gun_rips & ~simulator
    pause(1); % rips still moving.
    rips.StartRamping()
    gun.On();
    pause(1); % wait fordev_CT=tango.Device('sr/d-ct/1');
end

%% average BPM readings
for i=1:average
    
    % wait for trigger and then read TbT data
    [hor,ver,sig]=obj.getTbTdata(obj);
    
    % store raw data.
    h_raw(:,i)=reshape(hor(:,1:end),1,[]);
    v_raw(:,i)=reshape(ver(:,1:end),1,[]);
    s_raw(:,i)=reshape(sig(:,1:end),1,[]);
     
    % chop off first useless turns
    hor(:,1:selturns(1)-1) = [];
    ver(:,1:selturns(1)-1) = [];
    sig(:,1:selturns(1)-1) = [];
    
    % select only data where signal is above threshold
    bpm_with_beam = ones(size(sig));
    
    bpm_with_beam(sig < obj.sum_signal_threshold) = 0 ;
    
    index_holes = find_first_n_bad(bpm_with_beam(:),3);
    
    if isempty(index_holes) 
        index_holes = length(sig(:));
    end
    indlastBPM = index_holes(1);
    
    %indlastBPM = find(bpm_with_beam>0.15,1,'last');
    
    
    % display
    NBPM = size(hor,1);
    nturns = size(hor,2);
    % indlastBPM = NBPM *nturns;
   
    disp(['aqn ' num2str(i) ' of ' int2str(nturns) ...
        ' turns, signal on ' int2str(length(find(bpm_with_beam>0))) ...
        ' / ' int2str(NBPM*nturns) ' BPMs '...
        ' last bpm ' int2str(indlastBPM) ' ']);
    
    %[~,indlastBPM]=max(abs(diff(bpm_with_beam)));
    %indlastBPM = indlastBPM-numel(hor(:,1:selturns(1)-1));
    
    thi(:,i)=reshape(hor(:,selturns-selturns(1)+1),1,[]);
    tvi(:,i)=reshape(ver(:,selturns-selturns(1)+1),1,[]);
    tsi(:,i)=reshape(sig(:,selturns-selturns(1)+1),1,[]);
    tsi = double(tsi);
    
    
    %if indlastBPM < length(obj.indBPM)
         thi(indlastBPM:end,i)=NaN;
         tvi(indlastBPM:end,i)=NaN;
         tsi(indlastBPM:end,i)=NaN;
    %end
    
    
    % kill beam if stored current
    obj.KillBeamAtGivenCurrent;
    
    obj.lastbpm = indlastBPM;
end

if switch_on_gun_rips & ~simulator
    pause(0.25); % rips still moving.
    rips.StopRamping()
    gun.Off(); % limit dose
    %Ke.PulseNumber = pn; % set back initial number of pulses for extraction
end

th = mean2(thi,2)';
tv = mean2(tvi,2)';
ts = mean2(tsi,2)';
t = [th;tv;ts];

eth = std2(thi,1,2)';
etv = std2(tvi,1,2)';
ets = std2(tsi,1,2)';

errt = [eth;etv;ets];

% convert to m
% if ~simulator & ~strcmp(obj.machine,'sy')
%    % warning('reading in mm or m?')
% 
%     t = t*1e-3; % [m]
%     errt = errt*1e-3; % [m]
%     
% end

%% save and store data

% read correctors settings
hs = obj.sh.get;
vs = obj.sv.get;
sp = [obj.s12.get, obj.s3.get];
cv = [obj.cv8.get, obj.cv9.get];
freq = obj.rf.get;

% blm= obj.blm_TBT();
blm=nan;
    
Istored = obj.stored_current();
    
% save data
if obj.savefile
    save(fullfile(obj.MeasurementFolder,...
        ['FirstTurnsTrajectory' datestr(now,'YYYYmmmDD_HHMMSS') filename]),...
        't','errt','hs','vs','sp','cv','freq','blm','h_raw','v_raw','s_raw','Istored');
end

obj.last_measured_trajectory = t;

end




function indices = find_first_n_bad(x,n)
transitions = diff([0; x == 0; 0]); %find where the array goes from non-zero to zero and vice versa
runstarts = find(transitions == 1);
runends = find(transitions == -1); %one past the end
runlengths = runends - runstarts;
%keep only those runs of length 4 or more:
runstarts(runlengths < n) = [];
runends(runlengths < n) = [];
%expand each run into a list indices:
indices = arrayfun(@(s, e) s:e-1, runstarts, runends, 'UniformOutput', false);
indices = [indices{:}];  %concatenate the list of indices into one vector
%x(indices) = 2 %replace the indices with 2
end
