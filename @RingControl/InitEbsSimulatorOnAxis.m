function InitEbsSimulatorOnAxis(obj)
%SWITCHTOEBSSIMULATOR turns parameters of First2Turns class to EBS
%simulator
%
% 
%
%see also: FirstTurns RingControl


TANGO_HOST=['tango://' getenv('TANGO_HOST') '/'];

%% 
obj.MeasurementFolder = pwd;
obj.first_turn_index = 1;

%% movable objects (the movable class takes care of max-min limits, calibration, set point reached)
obj.ke      = movable(''); % movable('sy/ps-ke/1'); 
obj.gun     = movable(''); % 'elin/beam/run';%% missing
obj.rips    = movable('');
obj.hor_scraper = movable('');
obj.scraper = movable('');
obj.s12     = movable('');
obj.s3      = movable('');
obj.cv8     = movable('');
obj.cv9     = movable('');

getfunction = @(devname)tango.Attribute(devname).value;
setfunction = @(devname,val)setfield(tango.Attribute(devname),'set',val);

obj.sh      = movable([TANGO_HOST 'srmag/hst/all/Strengths'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',true,'limits',[-4e-4 4e-4]);

obj.sv      = movable([TANGO_HOST 'srmag/vst/all/Strengths'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',true,'limits',[-4e-4 4e-4]);

% limits can be higher, but they depend on the strengths of the steerers
obj.skew    = movable([TANGO_HOST 'srmag/sqp/all/CorrectionStrengths'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',true,'limits',[-1.5e-2 1.5e-2]);

obj.rf      = movable([TANGO_HOST 'srrf/master-oscillator/1/Frequency'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',false,'limits',[0.99 1.01]);

obj.quad    =  movable([TANGO_HOST 'srmag/m-q/all/CorrectionStrengths'],...
    'get_fun',getfunction,'set_fun',setfunction);

obj.sext    = movable([TANGO_HOST 'srmag/m-s/all/CorrectionStrengths'],...
    'get_fun',getfunction,'set_fun',setfunction);

obj.octu    = movable([TANGO_HOST 'srmag/m-o/all/CorrectionStrengths'],...
    'get_fun',getfunction,'set_fun',setfunction);

obj.initial_coordinates = movable([TANGO_HOST 'sys/ringsimulator/ebs/TbT_InCoord'],...
    'get_fun',getfunction,'set_fun',setfunction);


%% function to measure trajectory, takes as input the class instance.
obj.getTbTdata = @(obj)getTbTdata_EBS_SIMU(obj); 
% inputs to function getTbTdata
obj.hor_bpm_TBT = @()tango.Attribute([TANGO_HOST 'srdiag/beam-position/all/TBT_HPositions']).value;
obj.ver_bpm_TBT = @()tango.Attribute([TANGO_HOST 'srdiag/beam-position/all/TBT_VPositions']).value;
obj.sum_bpm_TBT = @()tango.Attribute([TANGO_HOST 'srdiag/beam-position/all/TBT_Sums']).value;
% bpm fresh data counter
obj.bpm_trigger_counter = @()tango.Attribute('sys/ringsimulator/ebs/Counter').value;
% load reference sum signal
obj.sum_signal_beam = ones(1,320); % sum signal with beam (sensitivity of each BPM)
obj.sum_signal_background = zeros(1,320);
obj.sum_signal_threshold = 0.5e-6; % sum signal below this value is considered not good

% bpm offset and tilt attributes
obj.hor_bpm_tilt = movable('');
obj.ver_bpm_tilt = movable('');
obj.hor_bpm_offset = movable([TANGO_HOST 'srdiag/beam-position/all/HOffsets'],...
    'get_fun',getfunction,'set_fun',setfunction);
obj.ver_bpm_offset = movable([TANGO_HOST 'srdiag/beam-position/all/VOffsets'],...
    'get_fun',getfunction,'set_fun',setfunction);

%% function to measure COD, takes as input the class instance.
obj.getSAdata = @(obj)getSAdata_EBS_SIMU(obj); 
% inputs to function getTbTdata
obj.hor_bpm_SA = @()tango.Attribute([TANGO_HOST 'srdiag/beam-position/all/SA_HPositions']).value;
obj.ver_bpm_SA = @()tango.Attribute([TANGO_HOST 'srdiag/beam-position/all/SA_VPositions']).value;
obj.sum_bpm_SA = @()tango.Attribute([TANGO_HOST 'srdiag/beam-position/all/SA_Sums']).value;

%% state and enabled disabled for RingControl
% mask of correctors to exclude
obj.status_bpm =@()(arrayfun(@(a)isequal(a,0),tango.Attribute([TANGO_HOST 'srdiag/beam-position/all/All_Status']).value)); % BPM status (0 means no error, so ok)
obj.state_hst =@()(strcmp(tango.Attribute([TANGO_HOST 'srmag/hst/all/CorrectorStates']).value,'Disabled')==0); % H steerer status (ON OFF FAULT,...)
obj.state_vst =@()(strcmp(tango.Attribute([TANGO_HOST 'srmag/vst/all/CorrectorStates']).value,'Disabled')==0); % V steerer status (ON OFF FAULT,...)

% index of correctors to exclude [0,n-1]
obj.orbit_cor_disabled_bpm =@()([]);% @()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-h/DisabledBPMsIndex']).value); % H steerer not in orbit correction
obj.orbit_cor_disabled_hst =@()([]);% @()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-h/DisabledActuatorsIndex']).value); % H steerer not in orbit correction
obj.orbit_cor_disabled_vst =@()([]);% @()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-v/DisabledActuatorsIndex']).value); % V steerer not in orbit correction

obj.autocor =@()(tango.Device([TANGO_HOST 'sr/beam-orbitcor/svd-auto']).State);

%% diagnostics, read only attributes
obj.stored_current = @()tango.Attribute([TANGO_HOST 'srdiag/beam-current/total/Current']).value;
obj.KillBeamAtGivenCurrent = @(obj)KillBeamAtGivenCurrentESRF(obj);

obj.h_tune = @()tango.Attribute([TANGO_HOST 'srdiag/beam-tune/1/Tune_h']).value;
obj.v_tune = @()tango.Attribute([TANGO_HOST 'srdiag/beam-tune/1/Tune_v']).value;
obj.h_emittance = @()tango.Attribute([TANGO_HOST 'srdiag/emittance/id07/Emittance_h']).value;
obj.v_emittance = @()tango.Attribute([TANGO_HOST 'srdiag/emittance/id07/Emittance_v']).value;
obj.lifetime = @()tango.Attribute([TANGO_HOST 'srdiag/beam-current/total/Lifetime']).value;
obj.neutrons = '';

%% lattice related informations
kick = [...
    0.000489845903684   0.000426168812966   0.000426168820758   0.000489845969593;... -1 mm
    0.002451840946955   0.002131227999229   0.002131228196820   0.002451842601757;... -5 mm
    0.004910225944962   0.004263696516143   0.004263697305621   0.004910232596004;... -10 mm
    0.007375181085586   0.006397873450655   0.006397875223752   0.007375196110139]; % -15 mm

% - 10mm kickers bump
K3 = 0.0;% kick(3,3);
K4 = 0.0;% kick(3,4);

obj.kick = kick;
obj.K3 = K3;
obj.K4 = K4;
obj.injoff = -0.0060; % set simulator TbT input to [injoff, 0,0,0,0,0]

mod = sr.model('reduce',true,'keep','QF8D');
ring = mod.ring;
obj.indBPM = mod.get(0,'bpm')';
obj.indHst = sort([mod.get(0,'steerh'); mod.get(0,'dq')])';
obj.indVst = mod.get(0,'steerv')';
obj.indSkew = mod.get(0,'skew')';

% make sure steerers have KickAngle.
ring = atsetfieldvalues(ring,obj.indHst,'KickAngle',{1,1},0);
ring = atsetfieldvalues(ring,obj.indHst,'KickAngle',{1,2},0);
ring = atsetfieldvalues(ring,obj.indVst,'KickAngle',{1,1},0);
ring = atsetfieldvalues(ring,obj.indVst,'KickAngle',{1,2},0);

% set KickAngle for on axis injection to have correct reference
% cell 03 and 04 to close bump for second turn adn following
a = load('horSteererForInjOffAxis.mat');
ring = atsetfieldvalues(ring,obj.indHst,'KickAngle',{1,1},a.kl);


% quadrupole indexes (BAD FOR QF8D of 2PW cells)
qpind = mod.get(0,'qp');
qf8dind = mod.get(0,'qf8d');
obj.indQuad=sort([qpind; qf8dind(1:50:end)])';

obj.indSext = mod.get(0,'sx')';
obj.indOctu = mod.get(0,'oc')';

ring=atsetcavity(atradon(ring),6.5e6,1,992);
obj.rmodel = ring;

% compute trajectory RM, obj.rmodel must be defined.
if ~exist('OnAxisTrajectoryRM_SR.mat','file')
    ModelRM = SimulateTrajectoryRM(obj);
    save('OnAxisTrajectoryRM_SR.mat','ModelRM');
else
    a=load('OnAxisTrajectoryRM_SR.mat');
    obj.ModelRM = a.ModelRM;
end

obj.tl2model ={};

try
    %% set up simulator
    obj.initial_coordinates.set([obj.injoff, 0,0,0,0,0]);
catch err
    disp(err)
    error('Impossible to set off axis injection in simulator. Is Tango Available? (raki, CTRM)')
end

end