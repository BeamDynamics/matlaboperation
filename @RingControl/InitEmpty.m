function InitEmpty(obj)
%SWITCHTOEBSSIMULATOR turns parameters of First2Turns class to EBS
%simulator
%
% 
%
%see also: FirstTurns RingControl

%% 
obj.MeasurementFolder = pwd;
obj.first_turn_index = 1;

%% movable objects (the movable class takes care of max-min limits, calibration, set point reached)
obj.ke      = movable(''); % movable('sy/ps-ke/1'); 
obj.gun     = movable(''); % 'elin/beam/run';%% missing
obj.rips    = movable('');
obj.hor_scraper = movable('');
obj.scraper = movable('');
obj.s12     = movable('');
obj.s3      = movable('');
obj.cv8     = movable('');
obj.cv9     = movable('');
obj.sh      = movable('');
obj.sv      = movable('');
obj.skew    = movable('');
obj.quad    = movable('');
obj.sext    = movable('');
obj.octu    = movable('');

obj.rf      = movable('');

obj.initial_coordinates = movable('');


%% function to measure trajectory, takes as input the class instance.
obj.getTbTdata = @(obj)getTbTdata_EBS_SIMU(obj); 

% bpm fresh data counter
obj.bpm_trigger_counter = @now;
% load reference sum signal
obj.sum_signal_beam = ones(1,10); % sum signal with beam (sensitivity of each BPM)
obj.sum_signal_background = zeros(1,10);
obj.sum_signal_threshold = 0.5e-6; % sum signal below this value is considered not good

% bpm offset and tilt attributes
obj.hor_bpm_tilt = movable('');
obj.ver_bpm_tilt = movable('');
obj.hor_bpm_offset = movable('');
obj.ver_bpm_offset = movable('');

% index of correctors to exclude [0,n-1]
obj.orbit_cor_disabled_bpm =@()([]);% @()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-h/DisabledBPMsIndex']).value); % H steerer not in orbit correction
obj.orbit_cor_disabled_hst =@()([]);% @()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-h/DisabledActuatorsIndex']).value); % H steerer not in orbit correction
obj.orbit_cor_disabled_vst =@()([]);% @()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-v/DisabledActuatorsIndex']).value); % V steerer not in orbit correction

obj.autocor = 'Off';

%% diagnostics, read only attributes
obj.blm_TBT = @()([]);
obj.blm_SA = @()([]);

obj.stored_current = @nan;
obj.KillBeamAtGivenCurrent = @()([]);

obj.h_tune =@nan;
obj.v_tune = @nan;
obj.h_emittance = @nan;
obj.v_emittance = @nan;
obj.lifetime = @nan;
obj.neutrons = '';

%% lattice related informations
kick = zeros(10,4);

K3 = 0.0;% kick(3,3);
K4 = 0.0;% kick(3,4);

obj.kick = kick;
obj.K3 = K3;
obj.K4 = K4;
obj.injoff = -0.0; % set simulator TbT input to [injoff, 0,0,0,0,0]

if isempty(obj.rmodel)
    r = {atringparam('no_lattice',0,0)};
end

mod = sr.model(r,'CheckLattice',false);
ring = mod.ring; 
obj.indBPM = mod.get(0,'bpm')';
obj.indHst = sort([mod.get(0,'steerh'); mod.get(0,'dq')])';
obj.indVst = mod.get(0,'steerv')';
obj.indSkew = mod.get(0,'skew')';

% % make sure steerers have KickAngle.
% ring = atsetfieldvalues(ring,obj.indHst,'KickAngle',{1,1},0);
% ring = atsetfieldvalues(ring,obj.indHst,'KickAngle',{1,2},0);
% ring = atsetfieldvalues(ring,obj.indVst,'KickAngle',{1,1},0);
% ring = atsetfieldvalues(ring,obj.indVst,'KickAngle',{1,2},0);

% quadrupole indexes 
obj.indQuad = mod.get(0,'qp');
obj.indSext = mod.get(0,'sx')';
obj.indOctu = mod.get(0,'oc')';

obj.rmodel = ring;

obj.tl2model ={};

try
    %% set up simulator
    obj.initial_coordinates.set([obj.injoff, 0,0,0,0,0]);
catch err
    disp(err)
    warning('Impossible to set off axis injection in simulator. Is Tango Available? (raki, CTRM)')
end


%% function to measure COD, takes as input the class instance.
obj.getSAdata = @(obj)getSAdata_EBS_SIMU(obj); 
% inputs to function getTbTdata
obj.hor_bpm_SA = @()zeros(size(obj.indBPM));
obj.ver_bpm_SA = @()zeros(size(obj.indBPM));
obj.sum_bpm_SA = @()ones(size(obj.indBPM));

% inputs to function getTbTdata
obj.hor_bpm_TBT = @()zeros(size(obj.indBPM));
obj.ver_bpm_TBT = @()zeros(size(obj.indBPM));
obj.sum_bpm_TBT = @()ones(size(obj.indBPM));

%% state and enabled disabled for RingControl
% mask of correctors to exclude
obj.status_bpm = @()true(size(obj.indBPM));
obj.state_hst = @()true(size(obj.indVst));
obj.state_vst = @()true(size(obj.indHst));


end