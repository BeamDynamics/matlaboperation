function [sh,sv,sh0,sv0,rbump] = set_any_cod_bump(obj,bpms,zeros_ind,amplH,amplV,SteerH,SteerV,varargin)
%SET_COD_BUMP sets a closed orbit bump at the specified location
%
% required inputs:
% bpms      1 index where to create the cod bump
% zero_inds  Nx1 index where COD shuold be zero
% amplH      1 hor. amplitude at the specified bpms
% amplV      1 ver. amplitude at the specified bpms
% SteerH    Nx1 index of steerers to use (1-288)
% SteerV    Nx1 index of steerers to use (1-288)
%
% optional input
% ...,'plot',true,...    display computed and measured bump in model
% ...,'set',false,...    display computed and measured bump in model
%
% Example:
% 0) instantiate RingControl class
% rc = RingControl('sr')
%
% 1) compute without setting the bump
% [shn,svn,sh0,sv0]=rc.set_cod_bump([10 11],[1e-4 0],[0 0],'plot',true)
% rc.sh.set(sh0)
% rc.sv.set(sv0)
%
% 2) compute at 4 locations, set and measure before and after setting
% [shn,svn,sh0,sv0]=rc.set_cod_bump(...
%                                   [10 11 20 21],...
%                                   [1e-4 1e-4 0 0],...
%                                   [0 0 1e-4 -1e-4],...
%                                   'plot',true,...
%                                   'set',true)
%
%
%see also: RingControl.RingControl atcorrectorbit

p=inputParser;
addRequired(p,'obj');
addRequired(p,'bpms',@isnumeric); % cell05 default
addRequired(p,'zeros_ind',@isnumeric); % cell05 default
addRequired(p,'amplH',@isnumeric);
addRequired(p,'amplV',@isnumeric);
addRequired(p,'SteerH',@isnumeric);
addRequired(p,'SteerV',@isnumeric);
addParameter(p,'set',false,@islogical);
addParameter(p,'plot',true,@islogical);
parse(p,obj,bpms,zeros_ind,amplH,amplV,SteerH,SteerV,varargin{:});

set_steerers = p.Results.set;
doplot = p.Results.plot;

if ~(strcmp(obj.machine,'sr') || strcmp(obj.machine,'ebs-simu'))
    disp(['method not valid for machine= ' obj.machine]);
    return
end

selh=true(size(obj.indHst));
selh([5:12:end,7:12:end,8:12:end]) = false; %remove DQ
sh = zeros(size(obj.indHst));
sv = zeros(size(obj.indVst));

if doplot
    figure;
end

rr = obj.rmodel;

s=findspos(rr,obj.indBPM);
s_h=findspos(rr,obj.indHst(selh));
s_v=findspos(rr,obj.indVst);

% fit bump at BPM ibump
[hs1,vs1,rbump]=fit_bump(rr,bpms,zeros_ind,amplH,amplV,SteerH,SteerV,obj.indBPM,obj.indHst(selh),obj.indVst);

sh(selh) = sh(selh) + hs1 ; % keep size of h steeres correct.
sv = sv + vs1;

% store steerers values
sh0 = obj.sh.get;
sv0 = obj.sv.get;

if set_steerers
    
    % measure trajectory
    [t]=obj.measuretrajectory;
    init_traj = t;
    
    % sign inversion
    %     warning('changing signs!')
    sign = ones(1,288);
    sign([1:9:end,5:9:end,9:9:end])=1;
    sign([9:9:end])=0.6;
    sign([1:9:end])=0.7;
    
    % set bump
    obj.sh.set( sh0 + sh );
    obj.sv.set( sv0 + sv.*sign );
    pause(5);
    % measure trajectory
    [t]=obj.measuretrajectory;
    final_traj = t;
    
    dh = final_traj(1,:)-init_traj(1,:);
    dv = final_traj(2,:)-init_traj(2,:);
    
    if doplot
        
        subplot(3,1,1);
        hold on;
        plot(s,dh(1:length(s)));
        subplot(3,1,2);
        hold on;
        plot(s,dv(1:length(s)));
        
    end
    
end


    function [hsl,vsl,rbump]=fit_bump(r,selbpm,zeroind,amplH,amplV,selcorh,selcorv,bpmidx,sthidx,stvidx)
        % returns steerers strengths vector in rad
        %
        %
        
        % initialize KickAngle
        r = atsetfieldvalues(r,sthidx,'KickAngle',{1,1},0);
        r = atsetfieldvalues(r,sthidx,'KickAngle',{1,2},0);
        r = atsetfieldvalues(r,stvidx,'KickAngle',{1,1},0);
        r = atsetfieldvalues(r,stvidx,'KickAngle',{1,2},0);
        
        LH = atgetfieldvalues(r,sthidx,'Length');
        LV = atgetfieldvalues(r,stvidx,'Length');
        
        
        % compute bumps
        bumph=amplH;
        bumpv=amplV;
        
        % initialize file strings
        hr={};
        vr={};
        
        % display computed bumps
        
        s=findspos(r,bpmidx);
        s_b=findspos(r,selbpm);
        s_h=findspos(r,sthidx);
        s_v=findspos(r,stvidx);
        
        ib = selbpm;
        
        % COMPUTE BUMP
        
        
        % select BPM for bump,
        %selbpm = AllBpm(ib,:) ;
        refx=[ bumph zeros(size(zeroind))];
        refy=[ bumpv zeros(size(zeroind))];
        
        refbumppos = [ib zeroind];
        
        belowlen=refbumppos<0;
        refbumppos(belowlen) = length(r)+refbumppos(belowlen);
        
        overlen=refbumppos>length(r);
        refbumppos(overlen) = -length(r)+refbumppos(overlen);
        
        % sort BPM indexes (findorbit6 accepts only increasing BPM indexes)
        [refbumppossort,ordbumppos]=sort(refbumppos);
        
        % % the steerers to use
        % selcorh = true(size(sthidx)) ;
        % selcorv = true(size(stvidx)) ;
        
        % initial closed orbit guess
        inCOD =zeros(6,1);
        
        
        disp('ref. positions')
        disp(refbumppos)
        disp('correctors used')
        disp(sthidx(selcorh)')
        disp(stvidx(selcorv)')
        
        % no radiation, with RF
        r=atsetcavity(r,6.5e6,0,992); % needs RF cavity for findorbit6Err
        
        rbump = r;
        for it = 1
            % invert matrix
            [rbump,inCOD,hs,vs]=atcorrectorbit(...
                rbump,...    lattice
                refbumppossort,...  % BPMS to use
                sthidx(selcorh),... % horizontal steerers to use
                stvidx(selcorv),... % vertical steerers to use
                inCOD,...           % initial closed orbit
                repmat([length(selcorh) length(selcorv)],4,1),... % correction iterations and eigenvectors
                [false false false],...   % no energy, no average zero, rad
                1.0,...             % scale factor = 100%
                [],... ModelRMbump,...     % response matrix
                [refx(ordbumppos);...   % reference orbit (wished bump)
                refy(ordbumppos)],...  %
                [],...              % steerers Limit
                true);              % display output
        end
        
        % read orbit
        o=findorbit4(rbump,0,bpmidx,inCOD);
        
        % convert length 288
        hsl = zeros(size(sthidx));
        hsl(selcorh) = hs;%.*LH(selcorh);
        
        vsl = zeros(size(stvidx));
        vsl(selcorv) = vs;%.*LV(selcorv);
        
        if doplot
            % display orbit (for debug)
            subplot(3,1,1)
            ax1=gca;
            ax1.YLim=[-bumph bumph];
            plot(s,o(1,:),'DisplayName',num2str(ib)); hold on;
            plot(s_b,bumph,'ro');
            ylabel('hor. cod [m]');
            subplot(3,1,2)
            ax2 = gca;
            %ax2.YLim=[-1 1]*1e-3;
            plot(s,o(3,:),'DisplayName',num2str(ib)); hold on;
            plot(s_b,bumpv,'ro');
            ylabel('ver. cod [m]');
            subplot(3,1,3)
            ax3 = gca;
            bar([s_h([1,selcorh,length(sthidx)])],...
                [hsl([1,selcorh,length(sthidx)])]*1e3);
            hold on;
            bar([s_v([1,selcorv,length(stvidx)])+0.1],...
                [vsl([1,selcorv,length(stvidx)])]*1e3);
            
            xlabel('s [m]'); ylabel('steerers [milli rad]');
            
            drawnow;
            linkaxes([ax1,ax2,ax3],'x');
            %linkaxes([ax1,ax2],'xy');
        end
        
        
    end

end
