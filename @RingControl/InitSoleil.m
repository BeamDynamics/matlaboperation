function InitSoleil(obj)
%SWITCHTOSoleil turns parameters of RingControl class to Soleil
%
% 
%
%see also: FirstTurns RingControl


% TANGO_HOST=['tango://' getenv('TANGO_HOST') '/'];

%% 
obj.MeasurementFolder = pwd;
obj.first_turn_index = 7;

%% movable objects (the movable class takes care of max-min limits, calibration, set point reached)
obj.ke      = movable(''); % movable('sy/ps-ke/1'); 
obj.gun     = movable(''); % 'elin/beam/run';%% missing
obj.rips    = movable('');
obj.scraper = movable('');
obj.s12     = movable('');
obj.s3      = movable('');
obj.cv8     = movable('');
obj.cv9     = movable('');

getfunction = @(devname)tango.Attribute(devname).value;
setfunction = @(devname,val)setfield(tango.Attribute(devname),'set',val);


%%% change all get and set function 

obj.sh      = movable('HCOR',...
    'get_fun',@()getam('HCOR','Physics'),...
    'set_fun',@(values)setsp('HCOR',values',obj.indHst','Physics'),...
    'absolute',true,'limits',[-0.980e-3 0.980e-3]);  %%%check limits

obj.sv      = movable('VCOR',...
    'get_fun',@()getam('VCOR','Physics'),...
    'set_fun',@(values)setsp('VCOR',values',obj.indVst','Physics'),...
    'absolute',true,'limits',[-.711e-3 .711e-3]);

% empty movable for the skew quads
obj.skew   = movable('');
% = movable([TANGO_HOST 'srmag/sqp/all/CorrectionStrengths'],...
%     'get_fun',getfunction,'set_fun',setfunction,...
%     'absolute',true,'limits',[-1.5e-2 1.5e-2]);

obj.rf      = movable('RF_freq',...
    'get_fun',getfunction,...    %%% getRF missing!
    'set_fun',@(f_RF_MHz)steprf(f_RF_MHz),...
    'absolute',false,'limits',[0.99 1.01]);


%%% we can ignore
obj.quad   = movable('');
% =  movable([TANGO_HOST 'srmag/m-q/all/CorrectionStrengths'],...
%     'get_fun',getfunction,'set_fun',setfunction);

obj.sext   = movable(''); 
% = movable([TANGO_HOST 'srmag/m-s/all/CorrectionStrengths'],...
%     'get_fun',getfunction,'set_fun',setfunction);

obj.octu   = movable(''); 
% = movable([TANGO_HOST 'srmag/m-o/all/CorrectionStrengths'],...
%     'get_fun',getfunction,'set_fun',setfunction);

% obj.initial_coordinates = movable([TANGO_HOST 'sys/ringsimulator/ebs/TbT_InCoord'],...
%     'get_fun',getfunction,'set_fun',setfunction);


%% function to measure trajectory, takes as input the class instance.
obj.getTbTdata = @(obj)getfreshbpmdata(obj.nturns); % might or migth not use the following properties
% inputs to function getTbTdata
obj.hor_bpm_TBT = zeros(1,122);
obj.ver_bpm_TBT = zeros(1,122);
obj.sum_bpm_TBT = zeros(1,122);

% bpm fresh data counter
obj.bpm_trigger_counter = zeros(1,122);
%= @()tango.Attribute('sys/ringsimulator/ebs/Counter').value;
% load reference sum signal
obj.sum_signal_beam = ones(1,122); % sum signal with beam (sensitivity of each BPM)  %not used
obj.sum_signal_background = zeros(1,122);
obj.sum_signal_threshold = 0.5e-6; % sum signal below this value is considered not good


% %% function to measure COD, takes as input the class instance.
% obj.getSAdata = @(obj)getSAdata_EBS_SIMU(obj); % might or migth not use the following properties
% % inputs to function getTbTdata
% obj.hor_bpm_SA = @()tango.Attribute([TANGO_HOST 'srdiag/beam-position/all/SA_HPositions']).value;
% obj.ver_bpm_SA = @()tango.Attribute([TANGO_HOST 'srdiag/beam-position/all/SA_VPositions']).value;
% obj.sum_bpm_SA = @()tango.Attribute([TANGO_HOST 'srdiag/beam-position/all/SA_Sums']).value;

%% state and enabled disabled for RingControl
% mask of correctors to exclude
obj.status_bpm = @()family2status('BPMx'); % true means on  %%% and BPMz?
obj.state_hst =@()family2status('HCOR');
obj.state_vst =@()family2status('VCOR');

% index of correctors to exclude [0,n-1]
obj.orbit_cor_disabled_bpm =@()([]);% @()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-h/DisabledBPMsIndex']).value); % H steerer not in orbit correction
obj.orbit_cor_disabled_hst =@()([]);% @()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-h/DisabledActuatorsIndex']).value); % H steerer not in orbit correction
obj.orbit_cor_disabled_vst =@()([]);% @()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-v/DisabledActuatorsIndex']).value); % V steerer not in orbit correction

obj.autocor = false; %=@()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-auto']).state);

%% diagnostics, read only attributes
obj.stored_current = @()NaN(1);% @()tango.Attribute([TANGO_HOST 'srdiag/beam-current/total/Current']).value;
obj.KillBeamAtGivenCurrent =@()NaN(1);%  @(obj)KillBeamAtGivenCurrentESRF(obj);

obj.h_tune = @()NaN(1);% @()tango.Attribute([TANGO_HOST 'srdiag/beam-tune/1/Tune_h']).value;
obj.v_tune = @()NaN(1);% @()tango.Attribute([TANGO_HOST 'srdiag/beam-tune/1/Tune_v']).value;
obj.h_emittance = @()NaN(1);% @()tango.Attribute([TANGO_HOST 'srdiag/emittance/id07/Emittance_h']).value;
obj.v_emittance = @()NaN(1);% @()tango.Attribute([TANGO_HOST 'srdiag/emittance/id07/Emittance_v']).value;
obj.lifetime = @()NaN(1);% @()tango.Attribute([TANGO_HOST 'srdiag/beam-current/total/Lifetime']).value;
obj.neutrons = '';

%% lattice related informations
kick = []; 

% - 10mm kickers bump
K3 = 0.0;% kick(3,3);
K4 = 0.0;% kick(3,4);

obj.kick = kick;
obj.K3 = K3;
obj.K4 = K4;
obj.injoff = -0.0060; % set simulator TbT input to [injoff, 0,0,0,0,0]

% addpath('/machfs/liuzzo/soleil/')
ring = lattice_SOLEIL_2019();

obj.indBPM = find(atgetcells(ring,'FamName','BPM'))';
indPossibleCorrectors=find(atgetcells(ring,'FamName','COR'))';

obj.indHst =   indPossibleCorrectors(family2elem('HCOR')) ;
%%% should we change with this?
obj.indVst = indPossibleCorrectors(family2elem('VCOR'));
%%% should we change with this?
obj.indSkew = find(atgetcells(ring,'FamName','SkewQuad'))';

% make sure steerers have KickAngle.
ring = atsetfieldvalues(ring,obj.indHst,'KickAngle',{1,1},0);
ring = atsetfieldvalues(ring,obj.indHst,'KickAngle',{1,2},0);
ring = atsetfieldvalues(ring,obj.indVst,'KickAngle',{1,1},0);
ring = atsetfieldvalues(ring,obj.indVst,'KickAngle',{1,2},0);

% quadrupole indexes (BAD FOR QF8D of 2PW cells)
obj.indQuad=find(atgetcells(ring,'Class','Quadrupole'))';

obj.indSext = find(atgetcells(ring,'Class','Sextupole'))';
obj.indOctu = [];

% atrfcavity('RF',0,2900000,352201954.08583,416,2739100000);
ring=atsetenergy(ring,2739100000);
ring=atsetcavity(atradon(ring),2900000,1,416);
obj.rmodel = ring;

obj.tl2model ={};


end