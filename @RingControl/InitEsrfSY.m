function InitEsrfSY(obj)

% TANGO_HOST='';

TANGO_HOST=['tango://' getenv('TANGO_HOST') '/'];

obj.first_turn_index = 4;

obj.MeasurementFolder = pwd; %'/mntdirect/_machfs/liuzzo/EBS/Simulator/FirstTurn';


getfunction = @(devname)tango.Attribute(devname).set;
setfunction = @(devname,val)setfield(tango.Attribute(devname),'set',val);

%% devices
% for automated injection
obj.ke = '';% 'sy/ps-ke/1'; %% missing
obj.gun = [TANGO_HOST 'elin/beam/run'];% 'elin/beam/run';%% missing
obj.rips = [TANGO_HOST 'sy/ps-rips/manager'];% 'sy/ps-rips/manager';%% missing
obj.scraper = '';% 'sr/d-scr/c4-ext';%% missing


%% movables
% septa and CV not in simulator

obj.s12      = movable([TANGO_HOST 'sy/ps-ki/1/Current'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',true,'limits',[260 299],'calibration',1);

obj.s3      = movable([TANGO_HOST 'sy/ps-si/1/Current'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',true,'limits',[2600 2800],'calibration',1);

obj.cv8      = movable([TANGO_HOST 'tl1/ps-c1/cv2/Current'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',true,'limits',[-0.5 0.5],'calibration',1);

obj.cv9      = movable([TANGO_HOST 'tl1/ps-c1/cv3/Current'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',true,'limits',[-0.5 0.5],'calibration',1);



obj.sh      = movable([TANGO_HOST 'sy/st-h/all/Current'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',true,'limits',[-3e-4 3e-4],'calibration',load_corcalib('sy','h')*ones(1,39));

obj.sv      = movable([TANGO_HOST 'sy/st-v/all/Current'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',true,'limits',[-3e-4 3e-4],'calibration',load_corcalib('sy','v')*ones(1,39));

% obj.rf      = movable([TANGO_HOST 'sy/ms/1/Frequency'],...
%     'get_fun',getfunction,'set_fun',setfunction,...
%     'absolute',false,'limits',[0.99 1.01]);

obj.rf = movable('');

obj.quad = movable('');
obj.sext = movable('');
obj.octu = movable('');

%% diagnostics
obj.getTbTdata = @(obj)ReadTDPSpark(obj); 
% inputs to function getTbTdata
obj.hor_bpm_TBT = @()tango.Attribute([TANGO_HOST 'sy/d-bpm/all/XPosition']).value;
obj.ver_bpm_TBT = @()tango.Attribute([TANGO_HOST 'sy/d-bpm/all/ZPosition']).value;
obj.sum_bpm_TBT = @()tango.Attribute([TANGO_HOST 'sy/d-bpm/all/Sum']).value;

% bpm fresh data counter
obj.bpm_trigger_counter = @()tango.Attribute([TANGO_HOST 'sy/d-bpm/all/FillCounter']).value;

%load reference sum signal
obj.sum_signal_beam = []; % sum signal with beam (sensitivity of each BPM)
obj.sum_signal_background = [];
obj.sum_signal_threshold = 500000;
%% state and enabled disabled for RingControl
% mask of correctors to exclude
obj.status_bpm =@()(arrayfun(@(a)isequal(a,0),tango.Attribute([TANGO_HOST 'sy/d-bpm/all/All_Status']).value)); % BPM status (0 means no error, so ok)
obj.state_hst =@()(strcmp(tango.Attribute([TANGO_HOST 'sy/st-h/all/States']).value,'Disabled')==0); % H steerer status (ON OFF FAULT,...)
obj.state_vst =@()(strcmp(tango.Attribute([TANGO_HOST 'sy/st-h/all/States']).value,'Disabled')==0); % V steerer status (ON OFF FAULT,...)

% index of correctors to exclude [0,n-1]
obj.orbit_cor_disabled_bpm =@()([]);% @()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-steer-h/DisabledBPMsIndex']).value); % H steerer not in orbit correction
obj.orbit_cor_disabled_hst =@()([]);% @()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-steer-h/DisabledActuatorsIndex']).value); % H steerer not in orbit correction
obj.orbit_cor_disabled_vst =@()([]);% @()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-steer-v/DisabledActuatorsIndex']).value); % V steerer not in orbit correction

obj.autocor =@()(tango.Device([TANGO_HOST 'sy/beam-orbitcor/svd-steer-h']).State);


% bpm offset and tilt attributes
obj.hor_bpm_tilt = movable('');
obj.ver_bpm_tilt = movable('');
obj.hor_bpm_offset = movable('');
obj.ver_bpm_offset = movable('');

%% function to measure COD, takes as input the class instance.
obj.getSAdata = @(obj)getSAdata_EBS_SIMU(obj); % this is ok, name to be changed
% inputs to function getTbTdata
obj.hor_bpm_SA = @()tango.Attribute([TANGO_HOST 'sy/d-bpm/all/XPosition']).value;
obj.ver_bpm_SA = @()tango.Attribute([TANGO_HOST 'sy/d-bpm/all/ZPosition']).value;
obj.sum_bpm_SA = @()([]);


%%

obj.blm_TBT = @()([]);
obj.blm_SA = @()([]);

obj.stored_current = @()tango.Attribute([TANGO_HOST 'sy/d-ct/1/Current']).value;
obj.KillBeamAtGivenCurrent = @()([]);% @(obj)KillBeamAtGivenCurrentESRF(obj);

obj.h_tune_shaker      = movable('');
obj.v_tune_shaker      = movable('');

obj.h_tune = @()([]);% @()tango.Attribute([TANGO_HOST 'srdiag/beam-tune/1/Tune_h']).value;
obj.v_tune = @()([]);% @()tango.Attribute([TANGO_HOST 'srdiag/beam-tune/1/Tune_v']).value;
obj.h_chromaticity = @()([]);% @()tango.Attribute([TANGO_HOST 'srdiag/beam-tune/1/Tune_h']).value;
obj.h_chromaticity = @()([]);% @()tango.Attribute([TANGO_HOST 'srdiag/beam-tune/1/Tune_v']).value;
obj.h_emittance = @()([]);% @()tango.Attribute([TANGO_HOST 'srdiag/emittance/id07/Emittance_h']).value;
obj.v_emittance = @()([]);% @()tango.Attribute([TANGO_HOST 'srdiag/emittance/id07/Emittance_v']).value;
obj.h_emittance_err = @()([]);% @()tango.Attribute([TANGO_HOST 'srdiag/emittance/id07/Emittance_h']).value;
obj.v_emittance_err = @()([]);% @()tango.Attribute([TANGO_HOST 'srdiag/emittance/id07/Emittance_v']).value;
obj.lifetime = @()([]);% @()tango.Attribute([TANGO_HOST 'srdiag/beam-current/total/Lifetime']).value;
obj.neutrons = '';
obj.fast_response_matrix = '';

%% lattice
kick = [0.0 0.0 0.0 0.0]; % -15 mm

% - 10mm kickers bump
K3 = 0.0;% kick(3,3);
K4 = 0.0;% kick(3,4);

obj.kick = kick;
obj.K3 = K3;
obj.K4 = K4;
obj.injoff = -0.0; % set simulator TbT input to [injoff, 0,0,0,0,0]


mod = sy.model; % get lattice from default location (/operation/appdata/sy/optics/settings/theory/betamodel.mat)
ring = atradoff(mod.ring,'','auto','auto'); 
ring{end}.PassMethod='IdentityPass';
obj.indBPM = mod.get(0,'bpm')';
obj.indHst = mod.get(0,'steerh')';
obj.indVst = mod.get(0,'steerv')';
obj.indQuad = mod.get(0,'qp')';
obj.indSext = mod.get(0,'sx')';
obj.indOctu = [];

ring = atsetfieldvalues(ring,[obj.indHst,obj.indVst],'PassMethod','CorrectorPass');

obj.rmodel = ring;
obj.TrajFileName = '';
load('/mntdirect/_machfs/MDT/2019/2019_11_16/TL1_input.mat');
obj.tl2model=TL1;
end 