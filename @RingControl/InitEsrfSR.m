function InitEsrfSR(obj)
%SWITCHTOEBSSIMULATOR turns parameters of FirstTurns class to EBS
%simulator
%
% 
%
%see also: First2Turns

TANGO_HOST='tango://acs:10000/';

obj.MeasurementFolder = pwd;

obj.first_turn_index = 6; % sample 5 starting from 0 K.Scheidt 25/Nov/2019

%% movables

getfunction = @(devname)tango.Attribute(devname).set; % get setpoint
setfunction = @(devname,val)setfield(tango.Attribute(devname),'set',val); %set setpoint

% S2: 55mrad @12kA % same for Sy Se2
% S3: 31.1mrad@10kA
% S2 measured : -4.4494 mrad/kA  (53mrad@12kA)
% S3 measured : -3.1863 mrad/kA  (31.9mrad@10kA)
% CV8 measured : -0.078 mrad/A  
% CV9 measured : -0.085 mrad/A  

% attributes to use. Defaults for: ESRF SR Aug 2018
% obj.s12 = movable([TANGO_HOST 'sr/ps-si/2/Current'],...
%     'get_fun',getfunction,'set_fun',setfunction,...
%     'absolute',true,...
%     'calibration',-4.4494*1e-6,'limits',[9000 11000]*(-4.4494*1e-6));
% obj.s3 = movable([TANGO_HOST 'sr/ps-si/3/Current'],...
%     'get_fun',getfunction,'set_fun',setfunction,...
%     'absolute',true,...
%     'calibration',-3.1863*1e-6,'limits',[6200 7900]*(-3.1863*1e-6)); % 21.6431e-3 / 8e3

obj.s12 = movable([TANGO_HOST 'sr/ps-si/2/Current'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',true,...
    'calibration',1,'limits',[9000 11000]);
obj.s3 = movable([TANGO_HOST 'sr/ps-si/3/Current'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',true,...
    'calibration',1,'limits',[6200 7900]); % 21.6431e-3 / 8e3

% obj.cv8 = movable([TANGO_HOST 'tl2/ps-c1/cv8/Current'],...
%     'get_fun',getfunction,'set_fun',setfunction,...
%     'calibration',-0.078*1e-3,'limits',[-6 6]*(-0.078*1e-3));
% obj.cv9 = movable([TANGO_HOST 'tl2/ps-c1/cv9/Current'],...
%     'get_fun',getfunction,'set_fun',setfunction,...
%     'calibration',-0.085*1e-3,'limits',[-6 6]*(-0.085*1e-3));
obj.cv8 = movable([TANGO_HOST 'tl2/ps-c1/cv8/Current'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'calibration',1,'limits',[-6 6]);
obj.cv9 = movable([TANGO_HOST 'tl2/ps-c1/cv9/Current'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'calibration',1,'limits',[-6 6]);

% corrector  ANGLE = current * 161e-6  
% quadrupole K * 1/92.1623 = current

obj.sh =  movable([TANGO_HOST 'srmag/hst/all/Strengths'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'limits',[-4e-4 4e-4]);
obj.sv =  movable([TANGO_HOST 'srmag/vst/all/Strengths'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'limits',[-4e-4 4e-4]);

% limits can be higher, but they depend on the strengths of the steerers
obj.skew    = movable([TANGO_HOST 'srmag/sqp/all/CorrectionStrengths'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',true,'limits',[-1.5e-2 1.5e-2]);

obj.rf      = movable([TANGO_HOST 'sy/ms/1/Frequency'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',false,'limits',[0.99 1.01]);

obj.quad    =  movable([TANGO_HOST 'srmag/m-q/all/CorrectionStrengths'],...
    'get_fun',getfunction,'set_fun',setfunction);

obj.sext    = movable([TANGO_HOST 'srmag/m-s/all/CorrectionStrengths'],...
    'get_fun',getfunction,'set_fun',setfunction);

obj.octu    = movable([TANGO_HOST 'srmag/m-o/all/CorrectionStrengths'],...
    'get_fun',getfunction,'set_fun',setfunction);

obj.initial_coordinates = movable(''); 



%% devices to use
obj.ke = [TANGO_HOST 'sy/ps-ke/1'];
obj.gun = [TANGO_HOST 'elin/beam/run'];
obj.rips = [TANGO_HOST 'sy/ps-rips/manager'];

obj.scraper = [TANGO_HOST 'srdiag/scraper/c11-int'];
obj.scraper = movable([TANGO_HOST 'srdiag/scraper/c11-int/Position'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',true,...
    'calibration',1,'limits',[-25 -0.9]);
%% diagnostics

%% function to measure trajectory, takes as input the class instance.
obj.getTbTdata = @(obj)getTbTdata(obj); 
% inputs to function getTbTdata
obj.hor_bpm_TBT = @()(reshape(tango.Attribute([TANGO_HOST 'srdiag/bpm/all/All_TBT_HPosition']).value,[],320)');
obj.ver_bpm_TBT = @()(reshape(tango.Attribute([TANGO_HOST 'srdiag/bpm/all/All_TBT_VPosition']).value,[],320)');
obj.sum_bpm_TBT = @()(reshape(tango.Attribute([TANGO_HOST 'srdiag/bpm/all/All_TBT_Sum']).value,[],320)');
obj.bpm_trigger_counter = @()sum(tango.Attribute([TANGO_HOST 'srdiag/bpm/all/All_TBT_TriggerCounter']).value);
obj.bpm_trigger_counter_step = 320; % each BPM increase of one unit trigger counter

%load reference sum signal
% a=load('/users/beamdyn/dev/commissioningtools/@RingControl/referenceSRBPMsumsignal.mat','sumsignal');
% obj.sum_signal_beam = a.sumsignal; % sum signal with beam (sensitivity of each BPM)
% obj.sum_signal_background = zeros(size(a.sumsignal));
obj.sum_signal_beam = ones(1,320); % sum signal with beam (sensitivity of each BPM)
obj.sum_signal_background = zeros(1,320); % may be used or not in obj.getTbTdata() 
obj.sum_signal_threshold = 1e7; % sum signal below this value is considered not good

obj.blm_TBT = @()(tango.Attribute([TANGO_HOST 'srdiag/blm/tbt/TBT_data']).value);

%%  function to measure COD, takes as input the class instance.
obj.getSAdata = @(obj)getSAdata(obj); 

% inputs to function getSAdata
% set soruce to DEVICE not cache
bpms = tango.Device([TANGO_HOST 'srdiag/bpm/all']);
bpms.Source = tango.DevSource.DEV; % default is CACHE_DEV
    
obj.hor_bpm_SA = @()bpms.All_SA_HPosition.read;
obj.ver_bpm_SA = @()bpms.All_SA_VPosition.read;
obj.sum_bpm_SA = @()bpms.All_SA_Sum.read;

%obj.hor_bpm_SA = @()tango.Attribute([TANGO_HOST 'srdiag/bpm/all/All_SA_HPosition']).value;
%obj.ver_bpm_SA = @()tango.Attribute([TANGO_HOST 'srdiag/bpm/all/All_SA_VPosition']).value;
%obj.sum_bpm_SA = @()tango.Attribute([TANGO_HOST 'srdiag/bpm/all/All_SA_Sum']).value;

obj.status_bpm =@()(arrayfun(@(a)isequal(a,0),tango.Attribute([TANGO_HOST 'srdiag/bpm/all/All_Status']).value)); % BPM status (0 means no error, so ok)
obj.state_hst =@()(strcmp(tango.Attribute([TANGO_HOST 'srmag/hst/all/CorrectorStates']).value,'Disabled')==0); % H steerer status (ON OFF FAULT,...)
obj.state_vst =@()(strcmp(tango.Attribute([TANGO_HOST 'srmag/vst/all/CorrectorStates']).value,'Disabled')==0); % V steerer status (ON OFF FAULT,...)

obj.state_skew =@()(strcmp(tango.Attribute([TANGO_HOST 'srmag/sqp/all/CorrectorStates']).value,'Disabled')==0); % H steerer status (ON OFF FAULT,...)
obj.state_quad =@()(strcmp(tango.Attribute([TANGO_HOST 'srmag/m-q/all/MagnetStates']).value,'Disabled')==0); % V steerer status (ON OFF FAULT,...)
obj.state_sext =@()(strcmp(tango.Attribute([TANGO_HOST 'srmag/m-s/all/MagnetStates']).value,'Disabled')==0); % H steerer status (ON OFF FAULT,...)
obj.state_oct =@()(strcmp(tango.Attribute([TANGO_HOST 'srmag/m-o/all/MagnetStates']).value,'Disabled')==0); % V steerer status (ON OFF FAULT,...)

% index of correctors to exclude [0,n-1]
obj.orbit_cor_disabled_bpm =@()(unique([...
    tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-h/DisabledBPMsIndex']).value,...
    tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-v/DisabledBPMsIndex']).value,...
    ])); % H steerer not in orbit correction
obj.orbit_cor_disabled_hst =@()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-h/DisabledActuatorsIndex']).value); % H steerer not in orbit correction
obj.orbit_cor_disabled_vst =@()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-v/DisabledActuatorsIndex']).value); % V steerer not in orbit correction

obj.autocor =@()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-auto/State']).value);

obj.blm_SA = @()([]);

%% other usefull devices

obj.stored_current = @()tango.Attribute([TANGO_HOST 'srdiag/beam-current/total/Current']).value;
obj.KillBeamAtGivenCurrent = @()([]); % @(obj)KillBeamAtGivenCurrentESRF(obj);

obj.h_tune = @()tango.Attribute([TANGO_HOST 'srdiag/beam-tune/main/Qh']).value;
obj.v_tune = @()tango.Attribute([TANGO_HOST 'srdiag/beam-tune/main/Qv']).value;

obj.h_chromaticity = @()tango.Attribute([TANGO_HOST 'srdiag/beam-chromaticity/source/Qp_H']).value;
obj.v_chromaticity = @()tango.Attribute([TANGO_HOST 'srdiag/beam-chromaticity/source/Qp_V']).value;

obj.h_tune_shaker      = movable([TANGO_HOST 'srdiag/tm/spark-h/NoiseStrength'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',true,'limits',[0.0 20]);
obj.v_tune_shaker      = movable([TANGO_HOST 'srdiag/tm/spark-v/NoiseStrength'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',true,'limits',[0.0 20]);

obj.h_emittance = @()tango.Attribute([TANGO_HOST 'srdiag/beam-emittance/main/Emittance_h']).value;
obj.v_emittance = @()tango.Attribute([TANGO_HOST 'srdiag/beam-emittance/main/Emittance_v']).value;

obj.h_emittance_err = @()tango.Attribute([TANGO_HOST 'srdiag/beam-emittance/main/AbsoluteEmittanceError_H']).value;
obj.v_emittance_err = @()tango.Attribute([TANGO_HOST 'srdiag/beam-emittance/main/AbsoluteEmittanceError_V']).value;

obj.lifetime = @()tango.Attribute([TANGO_HOST 'srdiag/beam-current/total/Lifetime']).value;
obj.neutrons = '';

%% fast RM device (pilots FOFB to measure fast RM)
obj.fast_response_matrix = 'test/fast_rm/1';

%% lattice

mod = sr.model('reduce',true,'keep','QF8D');
% mod.settune([76.675 27.251])

ring = mod.ring;


global APPHOME

kicker_bump = load(fullfile(APPHOME,'optics','sr','theory','kicker_bump'));

obj.indKickers = mod.get(0,'kickers');
L = mod.ring{obj.indKickers(1)}.Length;

% K1: 2.3mrad@2kA
% K2: 2.28mrad@2kA
% K3: 2.27mrad@2kA
% K4: 2.31mrad@2kA
kick = kicker_bump(:,2:end).*0.00229./2000 / L;  

% - 10mm kickers bump
K3 = 0.0;% kick(3,3);
K4 = 0.0;% kick(3,4);

obj.kick = kick;
obj.K3 = K3;
obj.K4 = K4;
obj.injoff = -0.0060; % set simulator TbT input to [injoff, 0,0,0,0,0]

obj.indBPM = mod.get(0,'bpm')';
obj.indHst = sort([mod.get(0,'steerh'); mod.get(0,'dq')])';
obj.indVst = mod.get(0,'steerv')';
obj.indSkew = mod.get(0,'skew')';

% make sure steerers have KickAngle.
ring = atsetfieldvalues(ring,obj.indHst,'KickAngle',{1,1},0);
ring = atsetfieldvalues(ring,obj.indHst,'KickAngle',{1,2},0);
ring = atsetfieldvalues(ring,obj.indVst,'KickAngle',{1,1},0);
ring = atsetfieldvalues(ring,obj.indVst,'KickAngle',{1,2},0);

if obj.on_axis
    
    disp('ON AXIS: adding static bump at injection to lattice model')
    obj.injoff = -0.0; % set simulator TbT input to [injoff, 0,0,0,0,0]

    % set KickAngle for on axis injection to have correct reference
    % cell 03 and 04 to close bump for second turn adn following
    a = load('/operation/beamdyn/matlab/injection_bump/horSteererForInjOffAxis.mat');
    ring = atsetfieldvalues(ring,obj.indHst,'KickAngle',{1,1},a.kl);
    
    disp('please set static bump in SR:')
    disp('a = load(''/operation/beamdyn/matlab/injection_bump/horSteererForInjOffAxis.mat'');')
    disp('rc = RingControl(''sr'');');
    disp('h0 = rc.sh.get;');    
    disp('rc.sh.set(h0+a.kl'')');
    
end

% quadrupole indexes (BAD FOR QF8D of 2PW cells)
qpind = mod.get(0,'qp');
qf8dind = mod.get(0,'qf8d');
obj.indQuad=sort([qpind; qf8dind(1:50:end)])';

obj.indSext = mod.get(0,'sx')';
obj.indOctu = mod.get(0,'oc')';

ring=atsetcavity(atradon(ring),6.5e6,1,992);
obj.rmodel = ring;

obj.tl2model ={};

% tl2 model for CV and Septa tracking
obj.tl2model = tl2.model().ring;

end
