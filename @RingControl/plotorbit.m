function plotorbit(obj,varargin)
%PLOTTRAJECTORY display last measurement of trajectory
%
% optional inputs:
% ...,'file','FirstTurns---.mat',... to plot a saved dataset 
% ...,'bpmsel',to compute RMS STD and AVerage of H and V orbit for a
% selection of BPMS ([1:Nbpms])
% ...,'figure','FirstTurns---.mat',... to plot a saved dataset 
% 
%see also: RingControl
bpmsel_def = [1:320] ;
p=inputParser;
addRequired(p,'obj');
addParameter(p,'figure',true,@islogical);
addParameter(p,'bpmsel',bpmsel_def,@isnumeric);
addParameter(p,'file','',@ischar);

parse(p,obj,varargin{:});

datafile = p.Results.file;
bpmsel = p.Results.bpmsel;

if p.Results.figure
    figure('units','normalized','position',[0.1 0.4 0.8 0.3]);
end

if isempty(datafile)
    data = obj.last_measured_orbit;
else
    a = load(datafile);
    data = a.t;
end

if ~isempty(p.Results.figure)
    subplot(2,1,1)
    plot(data([1],:)','x-','LineWidth',2); 
    ylabel('hor. [m]');
    ylim([-5 +5]*1e-4);
    legend(['H rms:' num2str(rms(data([1],bpmsel))*1e6) ', std:'...
        num2str(std(data([1],bpmsel))*1e6) ', <'...
        num2str(mean(data([1],bpmsel))*1e6) '> um'])
    
    grid on;
    ax = gca;
    ax.FontSize = 14;
    subplot(2,1,2)
    plot(data([2],:)','ro-','LineWidth',2);
    xlabel('BPM #')
    ylabel('ver. [m]');
    ylim([-5 +5]*1e-4);
    legend(['V rms:' num2str(rms(data([2],bpmsel))*1e6) ' , std:'...
        num2str(std(data([2],bpmsel))*1e6) ', <'...
        num2str(mean(data([2],bpmsel))*1e6) '> um'])
    %yyaxis right
    %plot(data(3,:)');
    %ylabel('sum signal');
    if ~isempty(datafile)
        title([strrep(datafile,'_',' ')]);
    end
    grid on;
    ax = gca;
    ax.FontSize = 14;
else
    disp('no orbit has been measured since initialization');
end