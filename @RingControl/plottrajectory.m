function plottrajectory(obj,varargin)
%PLOTTRAJECTORY display last measurement of trajectory
% 
% optional inputs:
% ...,'file','FirstTurns---.mat',... to plot a saved dataset 
%
%see also: RingControl

p=inputParser;
addRequired(p,'obj');
addParameter(p,'figure',true,@islogical);
addParameter(p,'file','',@ischar);
parse(p,obj,varargin{:});

datafile = p.Results.file;
if p.Results.figure
    figure('units','normalized','position',[0.4 0.4 0.5 0.3]);
end


if isempty(datafile)
    % estimate tunes from trajetory
    try
        obj.estimatetune;
        mufft = obj.phase_advance_measured;
        mumodel = obj.phase_advance_expected;
        
        Qfft = obj.full_tune_measured;
        Qmodel = obj.full_tune_expected;
    catch
        mufft =[NaN NaN];
        mumodel =[NaN NaN];
        Qfft =[NaN NaN];
        Qmodel =[NaN NaN];
    end
    data = obj.last_measured_trajectory;
else % load from file
    a = load(datafile);
    data = a.t;
    obj.lastbpm = length(data(1,:));
    mufft =[NaN NaN];
    mumodel =[NaN NaN];
    Qfft =[NaN NaN];
    Qmodel =[NaN NaN];
end

plot(data([1,2],:)','LineWidth',3);
xlabel('BPM #')
ylabel('[m]');
ax =gca;
ax.FontSize =14;
ax.Color='None';
ax.XLim=[0,obj.lastbpm];
ax.YLim=[-10e-3, 10e-3];
if isempty(datafile)
    ax.Title.String={['\Delta Q_{expected-measured}: ' num2str(Qmodel - Qfft,'%2.2f  ')];...
        ['measured \mu@BPM' num2str(obj.lastbpm,'%d') ': ' num2str(mufft,'%2.2f  ')];...
        ['expected \mu@BPM' num2str(obj.lastbpm,'%d') ': ' num2str(mumodel,'%2.2f  ')];...
        ['']};
else
    ax.Title.String={[strrep(datafile,'_',' ')],[]};
end
yyaxis right
plot(data(3,:)','LineWidth',3);
ylabel('sum signal');
grid on;
ax =gca;
ax.FontSize =12;
ax.Color='None';
ax.XLim=[0,obj.lastbpm];
ax.YLim=[0,max(data(3,:))*1.1];
ax.YLim=[0, 1e8];
f = gcf;
f.Color=[1 1 1];
legend('H','V','Sum','Location','NorthWest');