function KillBeamAtGivenCurrent(obj,varargin)
%KILLBEAMATGIVENCURRENT kills beam with scraper if current > limit
%
% optional inptus:
% 'maxstorecurrent' scalar, value of maximum allowed stored current. if
%                   above, beam is killed with obj.dev_scraper scraper
%                   (default 1e-6 A)
%
%
%see also: RingControl

storecurrent_def=obj.max_stored_current;%1e-6;

p = inputParser();
addRequired(p,'obj');
addOptional(p,'maxstorecurrent',storecurrent_def,@isnumeric);

parse(p,obj,varargin{:});

storecurrent = p.Results.maxstorecurrent;

% in simulator scrapers do no exist at the moment.
simulator = strcmp(obj.machine,'ebs-simu');

if ~simulator
    c4ext   = tango.Device(obj.scraper);
    dev_CT  = tango.Device(obj.stored_current);
    
    
    initpos=c4ext.Position.set;
    
    % check current limit
    Cur=dev_CT.Current.read;
    
    if Cur>storecurrent
        
        disp('Maximum stored current is reached. closing C4 ext to -5 mm')
        % shot vertical kicker to kill beam
        
        c4ext.Position=-1;
        pause(2);
        try
            while strcmp(c4ext.State,'Moving')
                pause(1);
                disp('wait scraper');
            end
        catch
            pause(10)
        end
        
        c4ext.Position=initpos;
        pause(2);
        try
            while strcmp(c4ext.State,'Moving')
                pause(1);
                disp('wait scraper');
            end
        catch
            pause(10)
        end
    end
    
end

end





