%HDB.CELLSERIES     HDB output
%HDB.CELLSERIES is used for HDB signal values which are not scalar numeric

classdef cellseries
    
    properties
        Name='unnamed'
        SignalInfo=struct('DisplayUnit',1,'YLim',[-Inf Inf],'typeStr','none');    %Signal properties (unit, format...)
        TimeInfo=struct()
        DataInfo=struct()
    end
    
    properties
        Data
        Time
    end
    
    properties (Dependent=true, SetAccess=private)
        StartDate   % Starting date of the dataset
    end
    
    methods
        function this=cellseries(d,t,varargin)
            %cellseries(data,time,'optname',optval,...)
            this.Data=d;
            this.Time=t;
            for k=1:2:length(varargin)
                this.(varargin{k})=varargin{k+1};
            end
        end
        
        function h1=getsamples(this,range)
            %getsamples Obtain a subset of hdb.cellseries samples as another hdb.cellseries
            %using a subscript/index array.
            %
            %   TSOUT = getsamples(TSIN,I) returns a timeseries obtained from the samples
            %	of the timeseries TSIN corresponding to the time(s) TSIN.TIME(I).
            h1=hdb.cellseries(this.Data(range,:),this.Time(range),...
                'Name',this.Name,'SignalInfo',this.SignalInfo,...
                'TimeInfo',this.TimeInfo,'DataInfo',this.DataInfo);
        end
        
        function v=cellstr(this)
            %Cell array of strings, one line per sample
            dt=this.Data;
            fmt=['\t' this.SignalInfo.Format];
            ct=arrayfun(@char,this.datetime,'UniformOutput',false);
            cd=cellfun(@sp,dt,'UniformOutput',false);
            v=[ct cd];
            function strg=sp(ln)
                if iscell(ln)
                    items=cellfun(@(v) sprintf(fmt,v),ln,'UniformOutput',false);
                elseif ischar(ln)
                    items={sprintf(fmt,ln)};
                else
                    items=arrayfun(@(v) sprintf(fmt,v),ln,'UniformOutput',false);
                end
                strg=cat(2,items{:});
                strg(1)=[];
            end
        end
        
        function varargout=datetime(this,varargin)
            %Gives datatime values corresponding to each sample
            varargout=arrayfun(@dt,this,'UniformOutput',false);
            function tm=dt(h)
                tm=datetime(h.TimeInfo.StartDate,'convertFrom','datenum',varargin{:})+...
                    seconds(h.Time);
            end
        end
        
        function tm=get.StartDate(this)
            tm=datetime(this.TimeInfo.StartDate,'convertFrom','datenum');
        end
        
        function this = setprop(this,propName,propVal)
            this.(propName) = propVal;
        end
        
        function propval = getprop(this,propName)
            propval = this.(propName);
        end
    end
    
end
