function zz = typeinit()
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
import org.tango.jhdb.data.*

zoh=tsdata.interpolation('zoh');
linear=tsdata.interpolation('linear');
v={...
    2, false,   zoh,    NaN,	@double, @HdbData;...           %   0	    'NONE'
    1, false, linear,   NaN,	@double, @HdbDouble;...         %   1	    'TYPE_SCALAR_DOUBLE_RO'
    2, false, linear,   NaN,	@double, @HdbDouble;...         %   2	    'TYPE_SCALAR_DOUBLE_RW'
    1, true,    zoh,    NaN,	@double, @HdbDoubleArray;...	%   3	    'TYPE_ARRAY_DOUBLE_RO'
    2, true,    zoh,    NaN,	@double, @HdbDoubleArray;...	%   4	    'TYPE_ARRAY_DOUBLE_RW'
    1, false,   zoh,    0,      @int64,  @HdbLong64;...        %   5	    'TYPE_SCALAR_LONG64_RO'
    2, false,   zoh,	0,      @int64,  @HdbLong64;...        %   6	    'TYPE_SCALAR_LONG64_RW'
    1, true,    zoh,    0,      @int64,  @HdbLong64Array;...	%   7	    'TYPE_ARRAY_LONG64_RO'
    2, true,    zoh,    0,      @int64,  @HdbLong64Array;...	%   8	    'TYPE_ARRAY_LONG64_RW'
    1, false,   zoh,    0,      @int8,   @HdbByte;...          %   9	    'TYPE_SCALAR_CHAR_RO'
    2, false,   zoh,    0,      @int8,   @HdbByte;...          %   10	    'TYPE_SCALAR_CHAR_RW'
    1, true,    zoh,    0,      @int8,   @HdbByteArray;...     %   11	    'TYPE_ARRAY_CHAR_RO'
    2, true,    zoh,    0,      @int8,   @HdbByteArray;...     %   12	    'TYPE_ARRAY_CHAR_RW'
    1, true,    zoh,    '',     @char,   @HdbString;...        %   13	    'TYPE_SCALAR_STRING_RO'
    2, true,    zoh,    '',     @char,   @HdbString;...        %   14	    'TYPE_SCALAR_STRING_RW'
    1, true,    zoh,    '',     @cell,   @HdbStringArray;...	%   15	    'TYPE_ARRAY_STRING_RO'
    2, true,    zoh,    '',     @cell,   @HdbStringArray;... 	%   16	    'TYPE_ARRAY_STRING_RW'
    1, false, linear,   NaN,	@single, @HdbFloat;...         %   17	    'TYPE_SCALAR_FLOAT_RO'
    2, false, linear,   NaN,	@single, @HdbFloat;...         %   18	    'TYPE_SCALAR_FLOAT_RW'
    1, true,    zoh,    NaN,	@single, @HdbFloatArray;...	%   19	    'TYPE_ARRAY_FLOAT_RO'
    2, true,    zoh,    NaN,	@single, @HdbFloatArray;...	%   20	    'TYPE_ARRAY_FLOAT_RW'
    1, false,   zoh,    0,      @uint8,  @HdbUChar;...         %   21	    'TYPE_SCALAR_UCHAR_RO'
    2, false,   zoh,    0,      @uint8,  @HdbUChar;...         %   22	    'TYPE_SCALAR_UCHAR_RW'
    2, true,    zoh,	0,      @uint8,  @HdbUCharArray;...	%   23	    'TYPE_ARRAY_UCHAR_RO'
    2, true,    zoh,    0,      @uint8,  @HdbUCharArray;...	%   24	    'TYPE_ARRAY_UCHAR_RW'
    1, false,   zoh,    0,      @int16,  @HdbShort;...         %   25	    'TYPE_SCALAR_SHORT_RO'
    2, false,   zoh,    0,      @int16,  @HdbShort;...         %   26	    'TYPE_SCALAR_SHORT_RW'
    1, true,    zoh,    0,      @int16,  @HdbShortArray;...	%   27	    'TYPE_ARRAY_SHORT_RO'
    2, true,    zoh,    0,      @int16,  @HdbShortArray;...	%   28	    'TYPE_ARRAY_SHORT_RW'
    1, false,   zoh,    0,      @uint16, @HdbUShort;...        %   29	    'TYPE_SCALAR_USHORT_RO'
    2, false,   zoh,    0,      @uint16, @HdbUShort;...        %   30	    'TYPE_SCALAR_USHORT_RW'
    1, true,    zoh,    0       @uint16, @HdbUShortArray;...	%   31	    'TYPE_ARRAY_USHORT_RO'
    2, true,    zoh,    0,      @uint16, @HdbUShortArray;...	%   32	    'TYPE_ARRAY_USHORT_RW'
    1, false,   zoh,    0,      @int32,  @HdbLong;...          %   33	    'TYPE_SCALAR_LONG_RO'
    2, false,   zoh,    0,      @int32,  @HdbLong;...          %   34	    'TYPE_SCALAR_LONG_RW'
    1, true,    zoh,    0,      @int32,  @HdbLongArray;...     %   35	    'TYPE_ARRAY_LONG_RO'
    2, true,    zoh,    0,      @int32 , @HdbLongArray;...     %   36	    'TYPE_ARRAY_LONG_RW'
    1, false,   zoh,    0,      @uint32, @HdbULong;...         %   37	    'TYPE_SCALAR_ULONG_RO'
    2, false,   zoh,    0,      @uint32, @HdbULong;...         %   38	    'TYPE_SCALAR_ULONG_RW'
    1, true,    zoh,    0,      @uint32, @HdbULongArray;...	%   39	    'TYPE_ARRAY_ULONG_RO'
    2, true,    zoh,    0,      @uint32, @HdbULongArray;...	%   40	    'TYPE_ARRAY_ULONG_RW'
    1, false,   zoh,    13,     @tango.DevState, @HdbState;...  %   41	    'TYPE_SCALAR_STATE_RO'
    2, false,   zoh,    13      @tango.DevState, @HdbState;...  %   42	    'TYPE_SCALAR_STATE_RW'
    1, true,    zoh,    13,     @tango.DevState, @HdbStateArray;...  %   43	    'TYPE_ARRAY_STATE_RO'
    2, true,    zoh,    13,     @tango.DevState, @HdbStateArray;...  %   44	    'TYPE_ARRAY_STATE_RW'
    1, false,   zoh,    0,      @logical,@HdbBoolean;...      %   45	    'TYPE_SCALAR_BOOLEAN_RO'
    2, false,   zoh,    0,      @logical,@HdbBoolean;...      %   46	    'TYPE_SCALAR_BOOLEAN_RW'
    1, true,    zoh,    0,      @logical,@HdbBooleanArray;...	%   47	    'TYPE_ARRAY_BOOLEAN_RO'
    2, true,    zoh,    0,      @logical,@HdbBooleanArray;...	%   48	    'TYPE_ARRAY_BOOLEAN_RW'
    1, false,   zoh,    0,      @double, @HdbData;...          %   49	    'TYPE_SCALAR_ENCODED_RO'
    2, false,   zoh,    0,      @double, @HdbData;...          %   50	    'TYPE_SCALAR_ENCODED_RW'
    1, true,    zoh,    0,      @double, @HdbData;...          %   51	    'TYPE_ARRAY_ENCODED_RO'
    2, true,    zoh,    0,      @double, @HdbData;...          %   52	    'TYPE_ARRAY_ENCODED_RW'
    1, false,   zoh,    0,      @uint64, @HdbULong64;...       %   53	    'TYPE_SCALAR_ULONG64_RO'
    2, false,   zoh,    0,      @uint64, @HdbULong64;...       %   54	    'TYPE_SCALAR_ULONG64_RW'
    1, true,    zoh,    0,      @uint64, @HdbULong64Array;...  %   55	    'TYPE_ARRAY_ULONG64_RO'
    2, true,    zoh,    0,      @uint64, @HdbULong64Array;...  %   56	    'TYPE_ARRAY_ULONG64_RW'
    };

zz=struct('dsize',v(:,1),'isarray',v(:,2),'interp',v(:,3),'vini',v(:,4),'dclass',v(:,5),...
    'hdbType',v(:,6));
end

