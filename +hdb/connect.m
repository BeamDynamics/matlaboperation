function rvdr=connect(type)
%HDB.CONNECT return a hdb.reader object for hdb access
%
%READER=HDB.CONNECT(TYPE)
%
%TYPE:      'ORACLE' for legacy HDB, 'CASSANDRA' for hdb++ (default: 'CASSANDRA')
%
%READER:    object used for extracting datafrom HDB
%
%See also: HDB.READER, HDB.SERIES, HDB.EXTRACT

persistent reader

if nargin < 1
    type='POSTGRESQL';
end
if ~isfield(reader,type)
    hdb.reader(type);
end
rvdr=hdb.reader(type);

end

