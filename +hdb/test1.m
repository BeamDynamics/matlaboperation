%==== external 
tstart = '09-Sep-2019 12:00:00'
tstop  = '10-Sep-2019 06:00:00'
ndev   =['srvac/v-pen/c01-ch01-1/pressure' ; 'srvac/v-pen/c01-ch03-1/pressure']
bars = false;
ylog = true;
yran = [4e-11 1e-9];
yname= 'pressure [mbar]'; % [mbar] is specified in device unit [TBC]
picname='pressure'

%==== internal
lw=3;    % plot line width
bw=1e3;  % plot bar width
clear Table c

%================ SETUP FROM JEAN-LUC PONS ================================
% 1. /home/esrf/franchi/.matlab/R2018a/javaclasspath.txt mut be copied
%   in user's matlab folder ~/.matlab/R2018a/
% 2. /home/esrf/franchi/.matlab/R2018a/java.opts (-Djdbc.drivers=org.postgresql.Driver skdjfs)
%    must be copied in /mntdirect/_sware/com/matlab_2018a/bin/glnxa64/
% 3. urlsplit.m shall be in copied in the matlab function folder (in path)
%==========================================================================


%Table = hdb.extract(tstart,tstop, 'tango://acs.esrf.fr:10000/srvac/v-pen/c01-ch01-1/pressure');
clf()
ndata=size(ndev);
for i=1:ndata(1)
  Table(i) = hdb.extract(tstart,tstop,ndev(i,:));
  if bars
      bar(Table(i).datetime,Table(i).Data,bw);
  else
      plot(Table(i).datetime,Table(i).Data,'LineWidth',lw);
  end
  if ndata(1) >1 hold on; end
end
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0.4, 0.2, 0.6, 0.7]); % to resize plot caption
LEG=legend(ndev,'location','northwest');LEG.FontSize = 17;              % legend pos. & size
xtickangle(45);                                                         % rotation of dates on X
set(gca,'XLim',[datetime(tstart) datetime(tstop)]);                     % set X limits
ylabel(yname,'FontSize',29);                                            % set Y label & font 
if ylog 
    set(gca, 'YScale', 'log');
    ylabel([yname ' (log)'],'FontSize',29);    
end
ylim(yran);                                                             % set Y limits
c = get(gca,'YTickLabel');
set(gca,'YTickLabel',c,'fontsize',20);                                  % set X & Y label font 

%=============== export figure
export_fig([picname '.jpg'],'-transparent');






