function varargout = extract(self,s1,s2,varargin)
%EXTRACT Extract data from HDB
%
%[DATA1,DATA2,...]=EXTRACT(T1,T2,SIGNAL1,SIGNAL2,...)
%
%T1:        Start time, as a date string or date number
%T2:        End time, as a date string or date number
%SIGNALn:   String identifying the signal (ex.: 'sr/d-ct/1/current')
%
%DATAn:     hdb.series containing the data
%
%DATA=EXTRACT(T1,T2,SIGNAL1,SIGNAL2,...)
%
%DATA       1xN array of hdb.series containing the data
%
%[...]=EXTRACT(T1,T2,INTERVAL,...)
%           Resample the data at the given interval [s]
%
%See also:  hdb.series

t1=self.datim(s1);
t2=self.datim(s2);

if isnumeric(varargin{1})       % With resampling
    interval=varargin{1};
    dt=seconds(120);
    range=0:interval:seconds(t2-t1);
    nout=nargin-4;
    tic
    [data,siginfos]=self.getdata(t1,t1-dt,t2+dt,varargin{2:end}); toc
    out=cellfun(@samp,data,siginfos,'UniformOutput',false); toc
else                            % No resampling
    nout=nargin-3;
    tic;
    [data,siginfos]=self.getdata(t1,t1,t2,varargin{:}); toc
    out=cellfun(@hdb.reader.process,data,siginfos,'UniformOutput',false); toc
end
if nargout==nout
    varargout=out;
else
    varargout={cat(2,out{:})};
end

    function ts=samp(dataset,siginfo)
        u=hdb.reader.process(dataset,siginfo);
        try
            ts=u.resample(range);
        catch
            warning('Hdb:NotEnoughdata','Cannot resample because there are less that 2 data points');
            ts=[];
        end
    end

end