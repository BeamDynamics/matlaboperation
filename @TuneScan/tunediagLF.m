function ph=tunediagLF(orderrange,tunes,select,Ncells)
%Plot resonances in the tune diagram
%
%tunediag(order,tunes,select)
%
%order: array of resonance orders
%tunes: [nuxmin nuxmax nuzmin nuzmax]
%select: array of 2 logicals, choose to plot normal or skew (default both)
%Ncells: number of cells to get systematic resonances
%
%Usage: tunediag(1:6,[36 36.5 13 13.5],[true false])

if nargin<4
    Ncells=1;
end
makeleg=0;
if nargin < 3, select=true(1,2); end
if isempty(gca)% make figure if no figure
    makeleg=1;
fig1=findobj('Type','figure','Name','Tune Diagram');
	if isempty(fig1)
		fig1=figure('Name','Tune Diagram'); %#ok<*NASGU>
	else 
		figure(fig1)
	end
clf
%set(gcf,'Color','none');
axis(tunes);
set(gca,'DataAspectRatio',[1 1 1]);
xlabel('\nu_x','FontSize',14);
ylabel('\nu_z','FontSize',14);
end

hold on

color={ [0 0 0],[1 0 0],[0 0 1],...
        [0 0.5 0],[0.75 0 0.75],[0 0.75 0.75],...
        [0.5 0.5 0.5],[1 0.5 0.5],[0.5 0.5 1]};
style={'-','--'};
ph=[];
for order=orderrange(end:-1:1)
    line=color{mod(order-1,length(color))+1};
    h=plotxz(0,order,tunes,line,style,select,Ncells);
    ph=[ph,h];
    for m=1:order-1
        h=plotxz(m,order-m,tunes,line,style,select,Ncells);
     ph=[ph,h];
       h=plotxz(m,m-order,tunes,line,style,select,Ncells);
    ph=[ph,h];
    end
    h=plotxz(order,0,tunes,line,style,select,Ncells);
    ph=[ph,h];
    
end
if makeleg
    makeleg
gridLegend(ph,order,'location','eastoutside');
end

function hh=plotxz(m,n,tunes,line,style,select,Ncells)
hh=[];
system=mod(n,2)+1;
if select(system)
    plist=[m*tunes(1)+n*tunes(3) m*tunes(2)+n*tunes(3) m*tunes(1)+n*tunes(4) m*tunes(2)+n*tunes(4)];
   % disp([m n ceil(min(plist)) floor(max(plist))])
    for p=ceil(min(plist)):floor(max(plist))
        x=[tunes(1:2) (p-n*tunes(3:4))./m];
        z=[(p-m*tunes(1:2))./n tunes(3:4)];
        ok=(x>=tunes(1)) & (x<=tunes(2)) & (z>=tunes(3)) & (z<=tunes(4));
        if sum(ok) > 2
            [x,mm,nn]=unique(x(ok));
            z=z(ok);
            z=z(mm);
            ok=true(1,2);
        end
        
        pp=m*x(ok)+n*z(ok);
        
        if mod(pp(1),Ncells)==0 % systematic, thicker
            thicklin=1;
            systflag='syst';
        else
            thicklin=0.5;
            systflag=' ';
        end
        h=plot(x(ok),z(ok),'Color',line,...
            'LineStyle',style{system},...
            'LineWidth',thicklin,...
            'DisplayName',...
            [num2str(m) '\nu_{x}+ ' ...
             num2str(n) '\nu_{y} = ' ...
             num2str(pp(1)) ...
             ' (' systflag ')']);
         hh=[hh,h];
    end
end
