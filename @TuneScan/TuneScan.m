classdef TuneScan < GenericScan %& GridScan
    %TUNESCAN
    %
    % vary Hor. and Ver. tune in a grid of points to seek for optimum.
    %
    %
    %
    %see also: GenericScan
    
    properties
        
        
    end
    
    
    methods
        
        % creator
        function obj = TuneScan(machine)
            %OctSDUnbScan Construct an instance of this class
            %
            % for help type:
            % >> doc TuneScan
            %
            %see also: RingControl
            
            obj@GenericScan(machine);
            
            obj.x_label = 'Q_x';
            obj.y_label = 'Q_y';
            
            obj.C_h_range =  76.1900 + [-0.05 0.05]; % 2x1 array of min and max Q_h
            obj.C_v_range =  27.2300 + [-0.05 0.05]; % 2x1 array of min and max Q_v
            obj.store_actual_tunes = true; % use actual tunes as positions on 2D scan
            obj.DefineGridPoints;
        end
        
        function ExecuteChange(obj,DesiredChroma)
            % change chroma
            
            % get present tunes.
            
            inittune =  obj.GetTune; % read tunes
            disp(['Present tune: ' num2str(inittune)]);

            
            disp('desired change:')

            disp(DesiredChroma)
        
            % change tunes to initial
            obj.movetune(DesiredChroma,inittune);
            
        end
        
        
        function PlotScan(obj,varargin)
            
            % super class method
            PlotScan@GenericScan(obj,varargin{:});
            % add resonance line plot
            hold on;
            obj.tunediagLF(1:4,[obj.Q_h_range obj.Q_v_range],[1 1],32);
            
            
        end
    end
    
    methods (Static) % methods that do not need to know about this class, no input "obj"
        ph=tunediagLF(orderrange,tunes,select,Ncells) % method to draw resonance lines
    end
    
    
end

