function store_bpm(fname,bpm,bpm_name)

fid=fopen(fname,'w');
for i=1:size(bpm,1)
fprintf(fid,'%s\t%.6f\t%.4f\t%.6f\t%.4f\t%.4f\t%.6f\n',bpm_name{i},bpm(i,1:6));
end
fclose(fid);
