function [kicks,eff,first_bpm,nb_bpm]=bump_coefs(resp,sb,sk,periods,flag)
%[kicks,eff,first_bpm,nb_bpm]=BUMP_COEFS(resp,sb,sk,periods,averaging)
%	computes local bumps
%
%	resp: response matrix
%	sb: BPM abscissa
%	sk: steerer abscissa
%	periods: number of periods in the machine
%	averaging: (optional) uses average over periods if true

if nargin < 5, flag=false; end
[nbpm,nc]=size(resp); %#ok<ASGLU>
nkpercell=floor(nc/periods); % frf possibly added to real correctors
ncor=periods*nkpercell;
if nc > ncor
    kf=nc*ones(ncor,1);
else
    kf=[];
end
copyr=ones(1,periods);

% maxpu=7;
maxpu=50;

k=(1:ncor)';
refk=[circshift(k,1) k circshift(k,-1) kf];
nk=size(refk,2);
kicks=zeros(nk,ncor);
eff=NaN(maxpu,ncor);
first_bpm=zeros(ncor,1);
nb_bpm=zeros(ncor,1);
%                   compute non-normalized bumps (least square)
for i=1:ncor
   [kicks(:,i),eff2,in_bpm] = local_bump(resp,sb,sk,refk(i,:));
   neff=min([maxpu;length(eff2)]);
   eff(1:neff,i)=eff2(1:neff);
   nb_bpm(i)=neff;
   if neff > 0, first_bpm(i)=in_bpm(1);end
end
%                   compute average values over periods
meaneff=mean2(reshape(eff,nkpercell*maxpu,periods),2);
avereff=reshape(meaneff*copyr,maxpu,ncor);
averkick=reshape(mean2(reshape(kicks,nkpercell*nk,periods),2)*copyr,nk,ncor);

undef=find(~isfinite(eff));		% replace missing with average
eff(undef)=avereff(undef);

[fs,js]=sort(-reshape(meaneff,maxpu,nkpercell));%#ok<ASGLU> %compute normalization values
k=reshape(js(1,:)'*copyr,1,ncor)+maxpu*(0:ncor-1);

if flag
   best=avereff(k(:));          % most efficient	AVERAGE
   kicks=averkick./(ones(nk,1)*best');	% normalize kicks
   eff=avereff'./(best*ones(1,maxpu));	% normalize efficiencies
else
   best=eff(k(:));              % most efficient	INDEPENDENT
   kicks=kicks./(ones(nk,1)*best');	% normalize kicks
   eff=eff'./(best*ones(1,maxpu));	% normalize efficiencies
end
