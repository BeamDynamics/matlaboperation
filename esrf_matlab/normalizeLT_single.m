function [LT_norm, ErrorLT] = normalizeLT_single(datafile,varargin)
% touchek lifetime without single, no vacuum and normalized to 140pm, 10pm
% input is a datafile = load(file_saved_by_AcquireData)
% output array of 3 values for: [LT-ErrorLT, LT, LT+ErrorLT]
%
% optional inputs
% ...,'VacuumLifetime',120,... [hours]
% ...,'VerticalEmittanceOffset',0.0,.... [nano meter]
%
%

p = inputParser;
addRequired(p,'datafile',@isstruct);
addOptional(p,'VacuumLifetime',120,@isnumeric);
addOptional(p,'VerticalEmittanceOffset',0.0,@isnumeric);

parse(p,datafile,varargin{:});

vaclifetime = p.Results.VacuumLifetime*3600; % 120h in seconds

if ~isfield(datafile,'eLT')
    % old file format
    datafile.eLT = std(datafile.LT);  
    datafile.LT = mean(datafile.LT);
    datafile.LT_single = mean(datafile.LT_single);
    
end

LT = [datafile.LT-datafile.eLT,   datafile.LT,  datafile.LT + datafile.eLT];

% remove single
% remove vacuum

if datafile.I_single>3.0
    LT_tou_train = datafile.I ./ ( datafile.I./LT - datafile.I_single./datafile.LT_single(end) - datafile.I./vaclifetime) ;
else
    LT_tou_train = datafile.I ./ ( datafile.I./LT - datafile.I./vaclifetime) ;
end

% normalize to 200mA
LT  = LT_tou_train *datafile.I/200;


% normalize to ev = 10pm
ev = datafile.ev25;
if isnan(ev), ev = datafile.ev07; end
if isnan(ev), ev = 1e-4; warning('no vertical emittance, default to 0.1pm '); end

if isnan(ev), ev = 1e-4; end % avoid NaN. set to 0.1 pm

ev = ev + p.Results.VerticalEmittanceOffset;

LT  = LT / sqrt(ev) * sqrt(0.010);

% normalize to eh = 140pm
eh = datafile.eh25;
if isnan(eh), eh = datafile.eh07; end
if isnan(eh), eh = 1.4e-1; warning('no vertical emittance, default to 0.1pm '); end


LT  = LT / sqrt(eh) * sqrt(0.140);

% convert to h
LT_norm = LT/3600;

ErrorLT = LT_norm(3) - LT_norm(2);
LT_norm = LT_norm(2);


end