function [pth,args,mach] = getmodelpath(varargin)
%GETMODELPATH	% Get the full path of an optics directory
%
%[PTH,ARGS,MACH]=GETMODELPATH(VARARGIN) scans the input arguments looking for:
%
%- 'tl2'|'sy'|'sr'
%           machine name, path defaults to $(APPHOME)/mach/optics/settings/theory
%- ['tl2'|'sy'|'sr',]'opticsname'
%           machine and optics name
%- ['tl2'|'sy'|'sr',]'/machfs/appdata/sr/optics/settings/opticsname':
%           full path of optics directory
%
%PTH:	Full path of the optics directory
%ARGS:	remaining arguments
%MACH:	'sr', 'sy' or 'sr'

global APPHOME

narg=1;
mach='sr';
pth='';
while narg <= nargin && isempty(pth)
    arg=varargin{narg};
    if ~ischar(arg) || any(strcmpi(arg,{'h','v','x','z','h2v','v2h'}))
        break;
    else
        if any(strcmpi(arg,{'tl2','sy','sr'}))
            mach=lower(arg);
        else
            pth=arg;
        end
    end
    narg=narg+1;
end

if isempty(pth)
    pth=fullfile(APPHOME,'optics',mach,'theory');
elseif isempty(fileparts(pth))
    pth=fullfile(APPHOME,'optics',mach,pth);
else
    dirs=regexp(pth,filesep,'split');
    if any(strcmp('sy',dirs))
        mach='sy';
    elseif any(strcmp('sr',dirs))
        mach='sr';
    end
end

args=varargin(narg:end);
end

