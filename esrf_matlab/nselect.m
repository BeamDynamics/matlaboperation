function varargout=nselect(step,func,i,varargin)
%NSELECT decimate calls to a function
%
persistent is
if i==1
    is=step-1;
end
if is==0
    [varargout{1:nargout}]=func(i,varargin{:});
    is=step;
else
    varargout=cell(nargout,1);
end
is=is-1;

end

