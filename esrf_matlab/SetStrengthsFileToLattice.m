function [rfile, Quad,Sh,Sv,Skew,Sext,Oct,Kq0L,Ks0L,Ko0L]=...
    SetStrengthsFileToLattice(file,varargin)
% SETSTRENGTHSFILETOLATTICE
% 
% [rfile, Quad,Sh,Sv,Skew,Sext,Oct,Kq0L,Ks0L,Ko0L]...
%       = SetStrengthsFileToLattice(...
%         'operation',... % file to read, name or full path
%         'quad',true,...% set quadrupoles (default: true)
%         'sext',true,... % set sextupoles (default: true)
%         'skew',true,... % set skew (default: true)
%         'hor',false,... % set hor steerers (default: false) 
%         'ver',true,...  % set ver steerers (default: false)
%         'oct',true)     % set octupoles (default: true)
% 
% Quad,Sh,Sv,Skew,Sext,Oct,  are the integrated gradients in the SR
% Kq0L,Ks0L,Ko0L are the integrated gradients in the lattice model
% 
% if ...,'hor',true,... will return a "reduced" lattice with DQ merged
% 
% strengths files in:  '/opt/infra_rw/settings/m-strengths/'
%
%see also: sr.model

p= inputParser;
addRequired(p,'file',@ischar)
addOptional(p,'quad',true,@islogical)
addOptional(p,'sext',true,@islogical)
addOptional(p,'octu',true,@islogical)
addOptional(p,'skew',true,@islogical)
addOptional(p,'hor',false,@islogical)
addOptional(p,'ver',false,@islogical)
addOptional(p,'plot',false,@islogical)
addOptional(p,'r',[],@iscell)

parse(p,file,varargin{:});

r = p.Results.r;
do_q = p.Results.quad;
do_s = p.Results.sext;
do_o = p.Results.octu;
do_k = p.Results.skew;
do_h = p.Results.hor;
do_v = p.Results.ver;
do_plot = p.Results.plot;

[a,b,c]=fileparts(file);

if isempty(a)
    a='/opt/infra_rw/settings/m-strengths/';
end

if isempty(c)
    c='.ts';
end

Kq0L=[];
Sh=[];
Sv=[];
Quad=[];
Skew=[];
Sext=[];
Oct=[];
Ks0L = [];
Ko0L= [];


%get lattice
if isempty(r)
    r = sr.model('reduce',true).ring;
end

if do_h % reduce to have single element DQs
    precious=atgetcells(r,'FamName','ID.*$|^JL1[AE].*') |...
        atgetcells(r,'Class','Monitor');
    r = atreduce(r,precious);
end

inddev = find(atgetcells(r,'Device'))';
devnames = atgetfieldvalues(r,inddev,'Device');

% count header lines
fid = fopen(fullfile(a,[b c]),'r');
tline = fgetl(fid);
headlines = 0;
while tline(1)=='#'
    headlines = headlines+1;
    tline = fgetl(fid);
end
fclose(fid);

A = textscan(fopen(fullfile(a,[b c]),'r'),'%s %f','HeaderLines',headlines);

devfile = cellfun(@(a)fileparts(a),A{1},'un',0);
strfile = A{2};
rfile = r;

if do_q
    quads = atgetcells(r(inddev),'FamName','Q\w*','DQ\w*');
    L = atgetfieldvalues(r(inddev(quads)),'Length');
    
    [~,ind]=ismember(devnames(quads),devfile);
    ind(ind==0)=[];
    
    rfile=...
        atsetfieldvalues(rfile,inddev(quads),'PolynomB',{1,2},strfile(ind)./L);
    K0 = atgetfieldvalues(r(inddev(quads)),'PolynomB',{1,2});
    Quad=strfile(ind);
    Kq0L = K0.*L;
    if do_plot
        K0 = atgetfieldvalues(r(inddev(quads)),'PolynomB',{1,2});
        figure; plot([strfile(ind) - Kq0L]./(Kq0L),'.-');
        ylabel('SR - Model, D KL quad / KL');
        xlabel('s [m]')
        ax = gca;
        ax.XTick=1:length(r(inddev(quads)));
        ax.XTickLabel = cellfun(@(a)a.Device,r(inddev(quads)),'un',0);
        ax.XTickLabelRotation = 90;
        grid on;
        xlim([-1 length(r(inddev(quads)))+1])
        std([strfile(ind) - K0.*L]./(K0.*L))
    end
end


if do_s
    sext = atgetcells(r(inddev),'FamName','S[FDIJ]\w*');
    L = atgetfieldvalues(r(inddev(sext)),'Length');
    
    [~,ind]=ismember(devnames(sext),devfile);
    ind(ind==0)=[];
     Sext=strfile(ind);
     K0 = atgetfieldvalues(r(inddev(sext)),'PolynomB',{1,3});
    Ks0L = K0.*L;
    rfile=...
        atsetfieldvalues(rfile,inddev(sext),'PolynomB',{1,3},strfile(ind)./L);
end

if do_o
    oct = atgetcells(r(inddev),'FamName','O[FDIJ]\w*');
    L = atgetfieldvalues(r(inddev(oct)),'Length');
    
    [~,ind]=ismember(devnames(oct),devfile);
    ind(ind==0)=[];
      Oct=strfile(ind);
        K0 = atgetfieldvalues(r(inddev(oct)),'PolynomB',{1,4});
    Ko0L = K0.*L;
   
    rfile=...
        atsetfieldvalues(rfile,inddev(oct),'PolynomB',{1,4},strfile(ind)./L);
end

if do_k
   
    kLskew=A{2}([1545:1545+288-1]-6);
    skew = atgetcells(r,'FamName','S[HFDIJ]\w*');
    L = atgetfieldvalues(r(skew),'Length');
      Skew=kLskew;
    rfile=...
        atsetfieldvalues(rfile,skew,'PolynomA',{1,2},kLskew./L);
 
end

if do_h
   
    kL=A{2}([873:873+384-1]-6);
    hor = atgetcells(r,'FamName','S[HFDIJ]\w*','DQ\w*');
    L = atgetfieldvalues(r(hor),'Length');
      Sh=kL;
      rfile=...
        atsetfieldvalues(rfile,hor,'PolynomB',{1,1},kL./L);
 
end

if do_v
   
    kL=A{2}([1257:1257+288-1]-6);
    ver = atgetcells(r,'FamName','S[HFDIJ]\w*');
    L = atgetfieldvalues(r(ver),'Length');
    Sv=kL;
    rfile=...
        atsetfieldvalues(rfile,ver,'PolynomA',{1,1},kL./L);
 
end


end
