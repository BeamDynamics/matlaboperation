function store_bump(fname,bump,st2name)

nb=size(bump,1);
if nb <= 0, return, end
st1name={st2name{nb} st2name{1:nb-1}};
st3name={st2name{2:nb} st2name{1}};

fid=fopen(fname,'w');
for i=1:nb
listf=5+(1:bump(i,5));
fprintf(fid,'%s\t%.3f\t',st1name{i},bump(i,1));
fprintf(fid,'%s\t%.3f\t',st2name{i},bump(i,2));
fprintf(fid,'%s\t%.3f\t',st3name{i},bump(i,3));
fprintf(fid,'%.0f\t%.0f',bump(i,4:5));
fprintf(fid,'\t%.6f',bump(i,listf));
fprintf(fid,'\n');
end
fclose(fid);
