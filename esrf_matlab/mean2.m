function [y,xok]=mean2(x,varargin)
%MEAN2   Average or mean value. Ignores NaN and outliers.
%   For vectors, MEAN2(X) is the mean value of the elements in X. For
%   matrices, MEAN2(X) is a row vector containing the mean value of
%   each column.  For N-D arrays, MEAN2(X) is the mean value of the
%   elements along the first non-singleton dimension of X.
%
%   MEAN2(X,DIM) takes the mean along the dimension DIM of X. 
%
%   Example: If X = [0 1 2
%                    3 4 5]
%
%   then mean2(X,1) is [1.5 2.5 3.5] and mean2(X,2) is [1
%                                                     4]
%
%   MEAN2(X,DIM,LIMIT) Eliminates points with deviation larger than LIMIT*STD(X)
%
%   [Y,OK]=MEAN2(X,...) returns a logical array OK indicating the accepted values
%
%   See also MEDIAN, STD2, MIN2, MAX2, VAR, COV, MODE.

[y,xs,xok]=statok2(x,1,varargin{:});
