function r=responsem(bpm,st,nu,periods,alphal)
%R=RESPONSEM(OBS,EXC,NU) builds response matrix from EXC to OBS
%	OBS,EXC: [beta Phi/2PiNu]
%
%R=RESPONSEM(OBS,EXC,{NU,ALPHA}) computes the response for a fixed RF freq.
%   OBS,EXC: [beta Phi/2PiNu dispersion]

if iscell(nu)
    alphal=nu{2};
    nu=nu{1};
end

nbpm=size(bpm,1);
nst=size(st,1);
a=sqrt(bpm(:,1)*st(:,1)');
if nargin < 4
    b=0.5 - abs(bpm(:,2*ones(nst,1)) - st(:,2*ones(nbpm,1))');
else
    a=repmat(a,periods,periods);
    k=0:(periods-1);
    fbpm=reshape((bpm(:,2*ones(1,periods))+k(ones(1,nbpm),:)),nbpm*periods,1);
    fst=reshape((st(:,2*ones(1,periods))+k(ones(1,nst),:)),nst*periods,1);
    b=0.5 - abs(fbpm(:,ones(nst*periods,1)) - fst(:,ones(nbpm*periods,1))');
    nu=nu*periods;
end

r=a.*cos(b*(2*pi*nu))./(2*sin(pi*nu));

if size(bpm,2)>=3 && size(st,2)>= 3
    dispcor=bpm(:,3)*st(:,3)'./alphal;
    if nargin >4
        dispcor=repmat(dispcor,periods,periods);
    end
    r=r+dispcor;
end
