% measured values: 
t_ref = 26; % h  % refernce total lifetime measured
t_vac_ref = 120; % h
t_single_ref = 5; % h
I_ref = 0.200; % A
I_single_ref = 0.004; % A
ev_ref = 10*1e-12; % m rad
eh_ref = 130*1e-12; % m rad
nbunch_ref = 992 *7/8 ; % number
 

%r = sr.model('CheckLattice',false).ring;
%ringpara(r)

Zn = 0.67;
Vrf = 6.0e6;
U0 = 2.5641e+06;
E0 = 6.0e9;
h = 992;
alpha = 8.6160e-05;
sigdelta = 9.4757e-04;
circ =843.97732;

BL_ref = BunchLength((I_ref-I_single_ref)/nbunch_ref, Zn, Vrf, U0, E0, h, alpha, sigdelta, circ);

% wished scaling values: 
% lifetime at different current per bunch

% 62 bunch
I_tot = 0.065; % A
I_single = 0.0;
t_single = t_single_ref;
t_vac = 54; % h see confluence 2021 07 05
nbunch = 62; % number
ev = 20*1e-12 ;
eh = 145*1e-12;
% % 16 bunch
% I_tot = 0.033; % A
% I_single = 0.0;
% t_single = t_single_ref;
% t_vac = 100;% h
% nbunch = 16; % number
% ev = 20*1e-12 ;

BL = BunchLength((I_tot-I_single)/nbunch, Zn, Vrf, U0, E0, h, alpha, sigdelta, circ);

% 1/t_tot = (Itrain/t_tou + Itot/t_vac + Is/t_sing)
t_tou = (I_ref-I_single)/(I_ref/t_ref-I_ref/t_vac_ref-I_single_ref/t_single_ref);

% touschek lifetime scaling
t_tou_new = t_tou * (I_ref/nbunch_ref) / (I_tot/nbunch) *sqrt(ev/ev_ref) *sqrt(eh/eh_ref) * (BL/BL_ref);

% recomputation of total lifetime (assuming Vacuum lifetime is unchanged)
t_tot = I_tot / ( I_tot/t_vac + (I_tot-I_single)/t_tou_new + I_single/t_single);

disp('---------')
disp('Scaling 7/8+1 parameters')
disp(' ')
disp(['At ' num2str(I_tot*1e3) ' mA in '  num2str(nbunch) ' bunches'] )
disp(['vertical emittance ' num2str(ev*1e12) ' pmrad'])
disp(['horizontal emittance ' num2str(eh*1e12) ' pmrad'])
disp(['vacuum lifetime ' num2str(t_vac) ' h'])
disp(' ')
disp("Expected Touschek lifetime: " + t_tou_new + " h")
disp("Expected total lifetime: " + t_tot + " h")
disp(' ')
disp('---------')
