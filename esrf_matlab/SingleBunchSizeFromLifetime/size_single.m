function size_single(eh_t,ev_t,T_tot,T_vac,T_tot_sing,EmitScale,lambda,L)
% function size_single(...
%     eh_t,...   measured hor emittance
%     ev_t,...   measured ver emittance
%     T_tot,...   measured total beam lifetime (196mA train 4mA single, 200mA total)
%     T_vac,...   measured vacuum lifetime
%     T_tot_sing,...   measured total single lifetime (vac + tou)
%     EmitScale,...   scaling factors for emittance at high current X 1+ [0.2 0 1] 20% more in H, vertical unchanged, longitudinal x2
%     lambda,...   wave length
%     L )  % length of source
% 
% example:
% size_single(135*1e-12,10*1e-12,22,80,4,[0.2 0 1],6e-11,2) % 20keV
% photons, 2m long undulator
% 
%see also:


r = sr.model('CheckLattice',false).ring;
rp = ringpara(r);

I_s = 0.004;
I_t = 0.00022;

bl_t = BunchLength(I_t,0.67,6.5e6,rp.U0,rp.E0,rp.harm,rp.alphac,rp.sigma_E,findspos(r,length(r)+1));
bl_s = BunchLength(I_s,0.67,6.5e6,rp.U0,rp.E0,rp.harm,rp.alphac,rp.sigma_E,findspos(r,length(r)+1));


[~,beta_ID,~,disp_ID,~,~]=atavedata(r,0,find(atgetcells(r,'FamName','ID05'),1,'first'));
[~,avebeta_ID,~,avedisp_ID,~,~]=atavedata(r,0,find(atgetcells(r,'FamName','DR_45'),1,'first'));

ensp_t = rp.sigma_E;

% beta at center
sh_t = sqrt(eh_t* beta_ID(1) + (ensp_t*disp_ID(1))^2);
sph_t = sqrt(eh_t / beta_ID(1) + (ensp_t*disp_ID(2))^2);
sv_t = sqrt(ev_t * beta_ID(2));
spv_t = sqrt( ev_t/ beta_ID(2) + (ensp_t*disp_ID(4))^2);

T_s = 1/(1/T_tot_sing - 1/T_vac); % single
T_t = 196/(200/T_tot -4/T_tot_sing - 200/T_vac); % train

ensp_s = ensp_t*(1+EmitScale(3));

eh_s = (1+EmitScale(1)) * eh_t;

sh_s = sqrt(eh_s * beta_ID(1) + (ensp_s*disp_ID(1))^2);
sph_s = sqrt(eh_s / beta_ID(1) + (ensp_s*disp_ID(2))^2);

ev_s = (T_s / T_t * I_s/I_t * sh_t/sh_s * bl_t/bl_s)^2 * ev_t;

sv_s = sqrt(ev_s * beta_ID(2) + (ensp_s*disp_ID(3))^2);
spv_s = sqrt(ev_s / beta_ID(2) + (ensp_s*disp_ID(4))^2);

s_r = sqrt(lambda*L)/2/pi;
spr = 1/2*sqrt(lambda/L);

disp("Emittances multiplication factors: " + EmitScale'*100 + "\% \\");
disp("\epsilon_{V,train} = " + ev_t*1e12 + " pm\\");
disp("\epsilon_{H,train} = " + eh_t*1e12 + " pm\\");
disp("\epsilon_{V,single} = " + ev_s*1e12 + " pm\\");
disp("\epsilon_{H,single} = " + eh_s*1e12 + " pm\\");
disp("\tau_{Vac} = " + T_vac + " h\\");
disp("\tau_{Tou,train} = " + T_t + " h\\");
disp("\tau_{Tou,single} = " + T_s + " h\\");

disp("beta_{v,ID}: " + beta_ID(2) + " m\\");
disp("\sigma_{V,train} = " + sv_t*1e6 + ' um\\');
disp("\sigma_{H,train} = " + sh_t*1e6 + ' um\\');
disp("\sigma_{V,single} = " + sv_s*1e6 + ' um\\');
disp("\sigma_{H,single} = " + sh_s*1e6 + ' um\\');
disp("\sigma'_{V,train} = " + spv_t*1e6 + ' urad\\');
disp("\sigma'_{H,train} = " + sph_t*1e6 + ' urad\\');
disp("\sigma'_{V,single} = " + spv_s*1e6 + ' urad\\');
disp("\sigma'_{H,single} = " + sph_s*1e6 + ' urad\\');

delete(fullfile(pwd,'CenterIDbeta.txt'));
diary CenterIDbeta.txt
diary on

disp("\begin{table}[hbt]");
disp("\centering");
disp("\begin{tabular}{rcc}");
%disp("\tau_{Vac} = " + T_vac + " h &");
%disp("\tau_{Tou,train} = " + T_t + " h &");
%disp("\tau_{Tou,single} = " + T_s + " h\\");
disp("\toprule")
disp("\sigma_{rad} & " + s_r*1e6 + " \mum & L = " + L + "\\");
disp("\sigma'_{rad} & " + spr*1e6 + " \mum & \lambda = " + lambda + "\\");
disp("\midrule")
disp("       &   H   &   V \\")
disp("\midrule")
disp("\beta_{ID,center}& " + beta_ID(1) + " m & " + beta_ID(2) + " m\\");
disp("\midrule")
disp("\epsilon_{train} & " + eh_t*1e12 + " pm &" + ev_t*1e12 + " pm\\");
disp("bunch length (train) & " + bl_t*1e3 + " mm & \\");
disp("\delta_E (train) & " + ensp_t*1e2 + " \% & \\");
disp("\midrule")
disp("\sigma_{train} & " + sh_t*1e6 + ' \mum &' + sv_t*1e6 + ' um\\');
disp("\sigma'_{train} & " + sph_t*1e6 + ' \murad &' + spv_t*1e6 + ' urad\\');
disp("\midrule")
disp("\Sigma_{train} $=\sqrt{\sigma^2 + \sigma^2_{rad}}$ & " + sqrt(sh_t.^2+s_r.^2)*1e6 + ' \mum &' + sqrt(sv_t.^2+s_r.^2)*1e6 + ' um\\');
disp("\Sigma'_{train} $=\sqrt{\sigma'^2 + \sigma'^2_{rad}}$ & " + sqrt(sph_t.^2+s_r.^2)*1e6 + ' \murad &' + sqrt(spv_t.^2+s_r.^2)*1e6 + ' urad\\');
disp("\midrule")
disp("\epsilon_{single} & " + eh_s*1e12 + " pm &" + ev_s*1e12 + " pm\\");
disp("bunch length (single) & " + bl_s*1e3 + " mm & I=4mA \\");
disp("\delta_E (single) & " + ensp_s*1e2 + " \% & \\");
disp("\midrule")
disp("\sigma_{single} & " + sh_s*1e6 + ' \mum &' + sv_s*1e6 + ' um\\');
disp("\sigma'_{single} & " + sph_s*1e6 + ' \murad &' + spv_s*1e6 + ' urad\\');
disp("\midrule")
disp("\Sigma_{single} $=\sqrt{\sigma^2 + \sigma^2_{rad}}$ & " + sqrt(sh_s.^2+s_r.^2)*1e6 + ' \mum &' + sqrt(sv_s.^2+s_r.^2)*1e6 + ' um\\');
disp("\Sigma'_{single} $=\sqrt{\sigma'^2 + \sigma'^2_{rad}}$ & " + sqrt(sph_s.^2+s_r.^2)*1e6 + ' \murad &' + sqrt(spv_s.^2+s_r.^2)*1e6 + ' urad\\');
disp("\bottomrule")
disp("\end{tabular}");
disp("\caption{$\beta$ center ID}")
disp("\end{table}");
diary off

figure;
subplot(2,2,1); title("single \lambda " + lambda + " L=" + L);
plotel(sh_s,sph_s,0,0,0,[1,0,0],0.1);
plotel(sqrt(sh_s.^2+s_r.^2),sqrt(sph_s.^2+s_r.^2),0,0,0,[0.5,0.5,0],0.1);
ax=gca; ax.FontSize = 12; ax.XLabel.String='x';ax.YLabel.String='x''';
legend('e^-','\gamma');
subplot(2,2,2); title('single'); 
plotel(sv_s,spv_s,0,0,0,[0,0,1],0.1);
plotel(sqrt(sv_s.^2+s_r.^2),sqrt(spv_s.^2+s_r.^2),0,0,0,[0,0.5,0.5],0.1);
ax=gca; ax.FontSize = 12; ax.XLabel.String='y';ax.YLabel.String='y''';
legend('e^-','\gamma');
subplot(2,2,3); title('train hor');
plotel(sh_t,sph_t,0,0,0,[1,0,0],0.1);
plotel(sqrt(sh_t.^2+s_r.^2),sqrt(sph_t.^2+s_r.^2),0,0,0,[0.5,0.5,0],0.1);
ax=gca; ax.FontSize = 12; ax.XLabel.String='x';ax.YLabel.String='x''';
legend('e^-','\gamma');
subplot(2,2,4); title('train ver');
plotel(sv_t,spv_t,0,0,0,[0,0,1],0.1);
plotel(sqrt(sv_s.^2+s_r.^2),sqrt(spv_t.^2+s_r.^2),0,0,0,[0,0.5,0.5],0.1);
ax=gca; ax.FontSize = 12; ax.XLabel.String='y';ax.YLabel.String='y''';
legend('e^-','\gamma');


sh_t = sqrt(eh_t * avebeta_ID(1) + (rp.sigma_E*avedisp_ID(1))^2);
sph_t = sqrt(eh_t / avebeta_ID(1) + (rp.sigma_E*avedisp_ID(2))^2);

sv_t = sqrt(ev_t * avebeta_ID(2));
spv_t = sqrt( ev_t/ avebeta_ID(2) + (rp.sigma_E*avedisp_ID(4))^2);


sh_s = sqrt(eh_s * avebeta_ID(1) + (ensp_s*avedisp_ID(1))^2);
sph_s = sqrt(eh_s / avebeta_ID(1) + (ensp_s*avedisp_ID(2))^2);

sv_s = sqrt(ev_s * avebeta_ID(2) + (ensp_s*avedisp_ID(3))^2);
spv_s = sqrt(ev_s / avebeta_ID(2) + (ensp_s*avedisp_ID(4))^2);

L =diff(findspos(r,find(atgetcells(r,'FamName','ID08')) +[-2 +2]));
s_r = sqrt(lambda*L)/2/pi;
spr = 1/2*sqrt(lambda/L);

disp("Emittances multiplication factors: " + EmitScale'*100 +" \% \\");
disp("\epsilon_{V,train} = " + ev_t*1e12 + " pm\\");
disp("\epsilon_{H,train} = " + eh_t*1e12 + " pm\\");
disp("\epsilon_{V,single} = " + ev_s*1e12 + " pm\\");
disp("\epsilon_{H,single} = " + eh_s*1e12 + " pm\\");
disp("\tau_{Vac} = " + T_vac + " h\\");
disp("\tau_{Tou,train} = " + T_t + " h\\");
disp("\tau_{Tou,single} = " + T_s + " h\\");
disp("< beta_{v,ID} >: " + avebeta_ID(2) + " m\\");
disp("\sigma_{V,train} = " + sv_t*1e6 + ' um\\');
disp("\sigma_{H,train} = " + sh_t*1e6 + ' um\\');
disp("\sigma_{V,single} = " + sv_s*1e6 + ' um\\');
disp("\sigma_{H,single} = " + sh_s*1e6 + ' um\\');
disp("\sigma'_{V,train} = " + spv_t*1e6 + ' urad\\');
disp("\sigma'_{H,train} = " + sph_t*1e6 + ' urad\\');
disp("\sigma'_{V,single} = " + spv_s*1e6 + ' urad\\');
disp("\sigma'_{H,single} = " + sph_s*1e6 + ' urad\\');

%diary AveIDbeta.txt
diary on
disp("\begin{table}[]");
disp("\centering");
disp("\begin{tabular}{rcc}");
%disp("\tau_{Vac} = " + T_vac + " h &");
%disp("\tau_{Tou,train} = " + T_t + " h &");
%disp("\tau_{Tou,single} = " + T_s + " h\\");
disp("\toprule")
disp("\sigma_{rad} & " + s_r*1e6 + " \mum & L = " + L + "\\");
disp("\sigma'_{rad} & " + spr*1e6 + " \mum & \lambda = " + lambda + "\\");
disp("\midrule")
disp("       &   H   &   V \\")
disp("\midrule")
disp("\langle\beta_{ID}\rangle& " + avebeta_ID(1) + " m & " + avebeta_ID(2) + " m\\");
disp("\midrule")
disp("\epsilon_{train} & " + eh_t*1e12 + " pm &" + ev_t*1e12 + " pm\\");
disp("bunch length (train) & " + bl_t*1e3 + " mm & \\");
disp("\delta_E (train) & " + ensp_t*1e2 + " \% & \\");
disp("\midrule")
disp("\sigma_{train} & " + sh_t*1e6 + ' \mum &' + sv_t*1e6 + ' um\\');
disp("\sigma'_{train} & " + sph_t*1e6 + ' \murad &' + spv_t*1e6 + ' urad\\');
disp("\midrule")
disp("\Sigma_{train} $=\sqrt{\sigma^2 + \sigma^2_{rad}}$ & " + sqrt(sh_t.^2+s_r.^2)*1e6 + ' \mum &' + sqrt(sv_t.^2+s_r.^2)*1e6 + ' um\\');
disp("\Sigma'_{train} $=\sqrt{\sigma'^2 + \sigma'^2_{rad}}$ & " + sqrt(sph_t.^2+s_r.^2)*1e6 + ' \murad &' + sqrt(spv_t.^2+s_r.^2)*1e6 + ' urad\\');
disp("\midrule")
disp("\epsilon_{single} & " + eh_s*1e12 + " pm &" + ev_s*1e12 + " pm\\");
disp("bunch length (single) & " + bl_s*1e3 + " mm & I = 4mA \\");
disp("\delta_E (single) & " + ensp_s*1e2 + " \% & \\");
disp("\midrule")
disp("\sigma_{single} & " + sh_s*1e6 + ' \mum &' + sv_s*1e6 + ' um\\');
disp("\sigma'_{single} & " + sph_s*1e6 + ' \murad &' + spv_s*1e6 + ' urad\\');
disp("\midrule")
disp("\Sigma_{single} $=\sqrt{\sigma^2 + \sigma^2_{rad}}$ & " + sqrt(sh_s.^2+s_r.^2)*1e6 + ' \mum &' + sqrt(sv_s.^2+s_r.^2)*1e6 + ' um\\');
disp("\Sigma'_{single} $=\sqrt{\sigma'^2 + \sigma'^2_{rad}}$ & " + sqrt(sph_s.^2+s_r.^2)*1e6 + ' \murad &' + sqrt(spv_s.^2+s_r.^2)*1e6 + ' urad\\');
disp("\bottomrule")
disp("\end{tabular}");
disp("\caption{$\beta$ average}")
disp("\end{table}");
diary off

figure;
subplot(2,2,1); title("single \lambda " + lambda + " L=" + L);
plotel(sh_s,sph_s,0,0,0,[1,0,0],0.1);
plotel(sqrt(sh_s.^2+s_r.^2),sqrt(sph_s.^2+s_r.^2),0,0,0,[0.5,0.5,0],0.1);
ax=gca; ax.FontSize = 12; ax.XLabel.String='x';ax.YLabel.String='x''';
legend('e^-','\gamma');
subplot(2,2,2); title('single'); 
plotel(sv_s,spv_s,0,0,0,[0,0,1],0.1);
plotel(sqrt(sv_s.^2+s_r.^2),sqrt(spv_s.^2+s_r.^2),0,0,0,[0,0.5,0.5],0.1);
ax=gca; ax.FontSize = 12; ax.XLabel.String='y';ax.YLabel.String='y''';
legend('e^-','\gamma');
subplot(2,2,3); title('train hor');
plotel(sh_t,sph_t,0,0,0,[1,0,0],0.1);
plotel(sqrt(sh_t.^2+s_r.^2),sqrt(sph_t.^2+s_r.^2),0,0,0,[0.5,0.5,0],0.1);
ax=gca; ax.FontSize = 12; ax.XLabel.String='x';ax.YLabel.String='x''';
legend('e^-','\gamma');
subplot(2,2,4); title('train ver');
plotel(sv_t,spv_t,0,0,0,[0,0,1],0.1);
plotel(sqrt(sv_s.^2+s_r.^2),sqrt(spv_t.^2+s_r.^2),0,0,0,[0,0.5,0.5],0.1);
ax=gca; ax.FontSize = 12; ax.XLabel.String='y';ax.YLabel.String='y''';
legend('e^-','\gamma');


end

