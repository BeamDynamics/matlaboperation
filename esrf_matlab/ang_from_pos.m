function [angle_forward,angle_backward,pos_forward,pos_backward,...
    ANG_COEFF_H,ANG_COEFF_V,POS_COEFF_H,POS_COEFF_V]=...
    ang_from_pos(r,indang,indbpm,bpm1,bpm2,varargin)
% function ang_from_pos(r,indang,indbpm,x1,x2)
% 
% Input: 
% r : (cell array of structures) AT lattice
% indang: (int) index in r where to compute the angle
% indbpm: (int array 2x1), index in r where of bpms upstream and downstream
% bpm1 : (double array 2x1) reading of bpm1 (x,y)
% bpm2 : (double array 2x1) reading of bpm2 (x,y)
%
%
% Output:
% angle_forward: (double array 2x1) (x,y) value of the angle computed from position reading and optics at
% indang
% angle_backward: (double array 2x1) value of the angle computed from position reading and optics at
% indang
% 
%
%
% created: 26 Jan 2021
% authors: A.Franchi (formulas) S.Liuzzo (code)
%
%see also:


if indbpm(1) > indang
    error('first element of indbpm is more than indang');
end
if indbpm(2) < indang
    error('second element of indbpm is less than indang');
end
    
% transfer matrices
[l,~,~]=atlinopt(r,0,1:indbpm(2));

M = RM44(l([indbpm(1),indang]),1:4,1:4);
Nm1 = RM44(l([indang,indbpm(2)]),1:4,1:4);

N = inv(Nm1);


% optics
[l,~,~] = atlinopt(r,0,1:indbpm(2));
index_sel = [indbpm(1),indbpm(2)];
l_sel = l(index_sel);

bx = arrayfun(@(a)a.beta(1),l_sel,'un',1);
ax = arrayfun(@(a)a.alpha(1),l_sel,'un',1);
mx = arrayfun(@(a)a.mu(1),l_sel,'un',1);

by = arrayfun(@(a)a.beta(2),l_sel,'un',1);
ay = arrayfun(@(a)a.alpha(2),l_sel,'un',1);
my = arrayfun(@(a)a.mu(2),l_sel,'un',1);

pos_forward = NaN(2,1);
angle_forward = NaN(2,1);
pos_backward = NaN(2,1);
angle_backward = NaN(2,1);

%% forward

% horizontal
pos_forward(1) = ...
    ( M(1,1) -M(1,2)/bx(1)*( ax(1) + cot(mx(2)-mx(1)) ) )*bpm1(1) +...
    ( M(1,2)/( sqrt( bx(1)*bx(2) )*sin(mx(2)-mx(1)) ) )*bpm2(1); 

POS_COEFF_H = [( M(1,1) -M(1,2)/bx(1)*( ax(1) + cot(mx(2)-mx(1)) ) ),...
    ( M(1,2)/( sqrt( bx(1)*bx(2) )*sin(mx(2)-mx(1)) ) )];

angle_forward(1) = ...
    ( M(2,1) -M(2,2)/bx(1)*( ax(1) + cot(mx(2)-mx(1))) )*bpm1(1) +...
    ( M(2,2)/ ( sqrt( bx(1)*bx(2) )*sin(mx(2)-mx(1)) ) )*bpm2(1); 

ANG_COEFF_H = [( M(2,1) -M(2,2)/bx(1)*( ax(1) + cot(mx(2)-mx(1))) )...
         ( M(2,2)/ ( sqrt( bx(1)*bx(2) )*sin(mx(2)-mx(1)) ) )];

% vertical
pos_forward(2) = ...
    ( M(3,3) -M(3,4)/by(1)*( ay(1) + cot(my(2)-my(1))) )*bpm1(2) +...
    ( M(3,4)/( sqrt( by(1)*by(2) )*sin(my(2)-my(1)) ) )*bpm2(2); 

POS_COEFF_V = [( M(3,3) -M(3,4)/by(1)*( ay(1) + cot(my(2)-my(1))) ) ,...
               ( M(3,4)/( sqrt( by(1)*by(2) )*sin(my(2)-my(1)) ) )];
    
angle_forward(2) = ...
    ( M(4,3) -M(4,4)/by(1)*( ay(1) + cot(my(2)-my(1)) ) )*bpm1(2) +...
    ( M(4,4)/( sqrt( by(1)*by(2) )*sin(my(2)-my(1)) ) )*bpm2(2); 

ANG_COEFF_V = [( M(4,3) -M(4,4)/by(1)*( ay(1) + cot(my(2)-my(1)) ) ) ...
    ( M(4,4)/( sqrt( by(1)*by(2) )*sin(my(2)-my(1)) ) )];

%% backward


% horizontal
pos_backward(1) = ...
    ( N(1,1) -N(1,2)/bx(2)*( ax(1) - cot(mx(2)-mx(1))) )*bpm2(1) +...
    - ( N(1,2)/( sqrt( bx(1)*bx(2) )*sin(mx(2)-mx(1)) ))*bpm1(1); 

angle_backward(1) = ...
    ( N(2,1) -N(2,2)/bx(2)*( ax(2) - cot(mx(2)-mx(1))) )*bpm2(1) +...
    - ( N(2,2)/ ( sqrt( bx(1)*bx(2) )*sin(mx(2)-mx(1)) ) )*bpm1(1); 

% vertical
pos_backward(2) = ...
    ( N(3,3) -N(3,4)/by(2)*( ay(1) - cot(my(2)-my(1))) )*bpm2(2) +...
    - ( N(3,4)/( sqrt( by(1)*by(2) )*sin(my(2)-my(1)) ))*bpm1(2); 

angle_backward(2) = ...
    ( N(4,3) -N(4,4)/by(2)*( ay(2) - cot(my(2)-my(1))) )*bpm2(2) +...
    - ( N(4,4)/ ( sqrt( by(1)*by(2) )*sin(my(2)-my(1)) ) )*bpm1(2); 





end


function [r11]=RM44(lindata,ind1,ind2)
% get value of of indeces ind1 and ind2 (1 to 4) of 
% M44 between two points first and last

Mlast=lindata(2).M44;
Mfirst=lindata(1).M44;

Mfirstinv=[[Mfirst(2,2),-Mfirst(1,2);-Mfirst(2,1),Mfirst(1,1)],...
            zeros(2,2);...
            zeros(2,2),...
            [Mfirst(4,4),-Mfirst(3,4);-Mfirst(4,3),Mfirst(3,3)]];

R=Mlast*Mfirstinv;
%R=Mlast/Mfirst;

r11=R(ind1,ind2);

end