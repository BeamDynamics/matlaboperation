function [ intens ] = load_sumdd()
%LOAD_sumDD Load turn-by-turn sum signal
%
%INTENS=LOAD_DD()
%  
%  INTENS= Turn-by-turn summ signal (nturnsx224), 1st bpm is C4-1
%


bpm=tango.cache('srdiag/bpm/all', @tango.SrBpmDevice);
intens=bpm.read_dd('s')';


