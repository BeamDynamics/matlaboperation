function components = urlsplit(url,varargin)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

r1='^(?<scheme>[^/]*:)?(?<authority>//[^/?#]*)?/?(?<path>[^?#]*)(?<query>\?[^#]*)?(?<fragment>#.*)?$';
components=regexp(url,r1,'names');
if isempty(components)
    error('URL:Malformed','Wrong URL');
end
if isempty(components.authority)
    components.userhost='';
    components.domain='';
    components.port='';
else
    r2='^(?<userhost>//[^.:/]*)(?<domain>[^:/]+)?(?<port>:\d+)?$';
    fe=regexp(components.authority,r2,'names');
    if isempty(fe)
        error('URL:Malformed','Wrong URL');
    end
    components.userhost=fe.userhost;
    components.domain=fe.domain;
    components.port=fe.port;
end
for i=1:2:length(varargin)
    if isempty(components.(varargin{i}))
        components.(varargin{i})=varargin{i+1};
    end
end
end

