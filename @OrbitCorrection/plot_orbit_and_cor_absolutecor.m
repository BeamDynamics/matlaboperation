h

initorb=oc.measureorbit;

% oc.correction  %(eig, egenvalues = 100, no RF)

afterorb=oc.measureorbit;
aftersh=oc.sh.get;
aftersv=oc.sv.get;
initsh = oc.sh.value_at_init;
initsv = oc.sv.value_at_init;

figure; 
subplot(2,2,1); plot(initorb([1],:)');hold on;plot(afterorb([1],:)');
legend(['before ' num2str(std(initorb([1],:)))],['after ' num2str(std(afterorb([1],:)))])
ylabel('hor. [m]');
subplot(2,2,2); bar([initsh;aftersh;]');
legend(['before ' num2str(std(initsh))],['after ' num2str(std(aftersh))])
ylabel('hor. [rad]');
subplot(2,2,4); bar([initsv;aftersv;]');
legend(['before ' num2str(std(initsv))],['after ' num2str(std(aftersv))])
ylabel('ver. [rad]');
xlabel('Corrector #')
subplot(2,2,3); plot(initorb(2,:)');hold on;plot(afterorb(2,:)');
legend(['before ' num2str(std(initorb(2,:)))],['after ' num2str(std(afterorb(2,:)))])
ylabel('ver. [m]');
xlabel('BPM #');
