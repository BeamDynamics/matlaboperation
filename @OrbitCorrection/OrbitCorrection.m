classdef OrbitCorrection < RingControl
    %OrbitCorrection 
    %   
    %see also: RingControl
    
    properties
             

        ModelRM % response matrix structure for trajectory correction
        
    end
    
    
    methods
        
        % creator
        function obj = OrbitCorrection(machine)
            %OrbitCorrection Construct an instance of this class
            %   
            %   can be used for: ebs-simu, esrf-sr, esrf-sy
            %
            %   Inherits all methods and properties from RingControl:
            %
            %see also: RingControl
            
            obj@RingControl(machine);
            
            obj.savefile = true;
            
            obj.n_acquisitions = 1;
                    
            switch obj.machine
                case 'ebs-simu'
                    
                    b = load('/machfs/liuzzo/EBS/Simulator/BeamDynamics/matlab/@OrbitCorrection/EBSOrbitRM.mat');
                    obj.ModelRM = b.ModelRM;

                case 'sr'
                    
                    b = load('/machfs/liuzzo/EBS/Simulator/BeamDynamics/matlab/@OrbitCorrection/EBSOrbitRM.mat');
                    obj.ModelRM = b.ModelRM;
                    
                 case 'sy'
                 
                    
            end
            
        end
        
        % methods defined in seprated functions

        % measure response matrix
        modelRM = MeasureOrbitRM(obj);
       
        % compute response matrix
        modelRM = SimulateOrbitRM(obj,varargin);
        % compute RM
        C = findrespmat(obj, PERTURB, PVALUE,FIELD,M,N, varargin)
        % compute closed orbit with BPM errs
        [t0] = findorbit(obj,ring,incod);
        
        % correction function
        [hs,vs,cv,sp,rf] = correction(obj,varargin);
       
    end
    
    methods (Static)
        
        % test function
        tryme
        
    end
    
end
