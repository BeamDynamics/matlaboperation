function [ o ]= findorbit(...
    obj,ring,inCOD)
%[o    6x(Nbpm) array of  orbit   
% ] = find2turnstrajectory6Err( 
% obj,...
% ring,.. 1) ring ( must be a separated input to be able to modify it)
% iCOD ) 5) 6x1 input coordinates (default: zero)
%
%
% 
% uses linepass to obtain trajectory in ring at indBPM
% if present, BPM errors are included on x(1st) and y(3rd) coordinates.
% 
% obj.injoff = -15 mm 
% obj.K3 obj.K4 set for -10mm bump,
% beam will oscillate by 5mm at injection
% 
%see also: linepass bpm_matrices bpm_process

if nargin<3
    inCOD=[0 0 0 0 0 0]';
end

refpos = obj.indBPM;

% find orbit no errors
outtr=findorbit6(ring,refpos,inCOD);
ox=outtr(1,:);
oy=outtr(3,:);

% set bpm errors
[rel,tel,trand] = bpm_matrices(ring(refpos));
bpmreading = bpm_process([ox; oy],rel,tel,trand);
o=outtr;
o(1,:)=bpmreading(1,:);
o(3,:)=bpmreading(2,:);

end

