function [hs,vs,freq]=...
    correction(...
    obj,...
    varargin)
%
% CORRECTION corrects orbit on first turns
% USING tango Devices on real accelerator (ESRF SR/SY).
%
% This function is a wizard. it will prompt you to continue
% correction, change parameters, compute/measure response matrices,...
%
% [ModelRM,hs,vs,cv,sp,freq]=...
%     correction(...
%     obj,...
%     varargin)
%
% OPTIONAL INPUTS:
% 'indBPM'                  bpm indexes
% 'indHCor'                 h. cor indexed
% 'indVCor'                 v. cor indexed
% 'max_BPM_good_reading'    maximum bpm accepted reading (lim+2e-3m for extra search)
%                           (default: [15e-3 (hor.) 3e-3m (ver.)])
% 'bpm_s_range'             BPM in this range are used. [m] (default [0,C])
% 'cor_s_range'             COR in this range are used. [m] (default [0,C])
% 'accepeted_turns'         accepted number of turns (default 10)
% 'correct_H'               correct using H steerers (default = true)
% 'correct_V'               correct using V steerers (default = true)
% 'correct_dpp'             correct dpp (default = false)
% 'correctors_mean_zero'    correctors mean constrained to zero
% 'svd_mode'                'Tikhonov' or 'eigenvectors' (defualt)
% 'Tikhonov'                Tikhonov parameter (ignored if mode eignvectors)
% 'ma_num_eig'              maximum number of eigenvectors (ignored if mode Tikhonov)
% 'ModelRM'                 orbit response matrix
%                               (if not provided it will be computed and
%                               saved in TrajRM.mat and given as output and
%                               overwrite the obj.ModelRM property).
% 'reference_orbit'         reference orbit xnbpm (default:0)
% 'steerers_max'            2x1 limit of steerers abs(steerer)<steererlimit
%                           (if not given, no limits), (default [3,3]e-4 rad)
% 'InjectedBeamFromSRaxis'  distance of injected beam from SR axis
%                           (default off axis -16mm)
% 'K3K4'                    kickers 3 and 4, elements DR_K3, DR_K4 in
%                           lattice (default -10mm bump for S28F)
% 'ask_to_continue'         ask message to keep iterating (default: true)
% 'verbose'                 print text ( default: true)
%     )
%
% finds closed orbit:
% correct first 2 turns orbit with available BPM and correctors
% if not found, increase amplitude limit for bpm reading
%               force use of TL2 CV8 CV9 and septa
%               increase number of eigenvectors used
%
%
% if measured orbit amplitude is different from imulated one, think
% about changing obj.injoff
%
% EXAMPLE USAGE:
% >> ft=FirstTurns('ebs-simu');
% >> ft.correction
%
% to call only h correction
% >> ft.correction('correct_h',true,'correct_v',false);
%
%see also: FirstTurns.SimulateorbitRM FirstTurns.MeasureorbitRM
%FirstTurns.setTurns FirstTurns.findNturnsorbit6Err

% variables requird by first two turns RM computation
% injection bump

r0 = obj.rmodel;
C = findspos(r0,length(r0)+1);

tl2=obj.tl2model;

%% parse inputs
p = inputParser();
addRequired(p,'obj');
addOptional(p,'indBPM',obj.indBPM,@isnumeric);
addOptional(p,'indHCor',obj.indHst,@isnumeric);
addOptional(p,'indVCor',obj.indVst,@isnumeric);

addOptional(p,'max_BPM_good_reading',[15e-3 3e-3],@isnumeric);
addOptional(p,'cor_s_range',[0 C],@isnumeric);
addOptional(p,'bpm_s_range',[0 C],@isnumeric);

expectedsvdmodes={'eigenvectors','Tikhonov'};
addParameter(p,'svd_mode',expectedsvdmodes{2},...
    @(x) any(validatestring(x,expectedsvdmodes)));
addOptional(p,'max_num_eigen',120,@isnumeric);
addOptional(p,'Tikhonov',10,@isnumeric);


addOptional(p,'correct_H',true,@islogical);
addOptional(p,'correct_V',true,@islogical);
addOptional(p,'correct_dpp',true,@islogic);
addOptional(p,'correctors_mean_zero',true,@islogical);

addOptional(p,'ModelRM',{},@(x) (iscell(x) || isstruct(x))); % if cell compute, if struct RM already computed
addOptional(p,'reference_orbit','compute',@isnumeric); %
addOptional(p,'steerers_max',[0.0003 0.0003],@isnumeric); % if cell compute, if struct RM already computed
addOptional(p,'verbose',true,@islogical); % if cell compute, if struct RM already computed
addOptional(p,'ask_to_continue',true,@islogical); % if cell compute, if struct RM already computed

parse(p,obj,varargin{:});

alpha = mcf(r0);
f0 = obj.rf.get;% atgetfieldvalues(r0,find(atgetcells(r0,'Frequency'),1,'first'),'Frequency');

inCOD           = zeros(6,1);
indBPM          = p.Results.indBPM;
indHCor         = p.Results.indHCor;
indVCor         = p.Results.indVCor;
lim             = p.Results.max_BPM_good_reading;
cor_s_range     = p.Results.cor_s_range;
bpm_s_range     = p.Results.bpm_s_range;
correct_H       = p.Results.correct_H;
correct_V       = p.Results.correct_V;
correctflags(1) = p.Results.correct_dpp;
correctflags(2) = p.Results.correctors_mean_zero;

ModelRM         = p.Results.ModelRM;
reforbit        = p.Results.reference_orbit;
steererlimit    = p.Results.steerers_max;
printouttext    = p.Results.verbose;
ask2cont        = p.Results.ask_to_continue;

max_num_eigen   = p.Results.max_num_eigen;
Tikhonov        = p.Results.Tikhonov;
svd_mode        = p.Results.svd_mode;


sbpm = findspos(r0,indBPM);
scorh = findspos(r0,indHCor);
scorv = findspos(r0,indVCor);


% trajmeasfun=@(varargin)obj.measureorbit(varargin);

obj.savefile = false;
warning('no file saving')

if ischar(reforbit)
    % get 2 turns reference orbit
    [t0] = obj.findorbit(obj.rmodel);
    reforbit = t0([1,3],:);
else
    t0(1,:) = reforbit(1,:);
    t0(3,:) = reforbit(2,:);
end

traj=0; % initial correction uses model lattice and default parameters.
% if needed will switch to traj=1 during the loop.
if printouttext
    if traj==1
        disp('correcting orbit');
    elseif traj==0
        disp('correcting orbit with model rm')
    end
end

%% compute model response matrices or compute them

if iscell(ModelRM) & isempty(obj.ModelRM)
    
    clear ModelRM
    
    if printouttext
        disp('computing model orbit RM')
    end
    
    obj.SimulateOrbitRM;
    
    save(['CODRM' obj.TrajFileName],'ModelRM');
    
    if printouttext
        disp('computed model orbit RM')
    end
    
end


% use only obj.nturns matrix
max_turns_RM = size(obj.ModelRM.OrbHCor{1},1)./length(indBPM);

%% initilize parameters

nbpmuprev=0;
countiter=0; % counuter for iterations.
countstack=0;% chack that correction did not get stack
deltacor = 0; % initializ RF correction value
stopped = false;
firstdata = true;

f = figure;
set(f,'WindowStyle','docked');

countatbpm=zeros(size(reforbit(1,:)));

% correction parameters modifiable by user during correction
corropt.number_of_acquisitions = obj.n_acquisitions;  % n of acquisitions to average
corropt.absolute = true;               % relative (false, default) or absolute (true) correction
corropt.fraction = 1.0;                 % fraction of correction to apply
corropt.limits = lim;                   % h and v limit for acceptable orbit in [m]
corropt.steererlimit = steererlimit;    % h and v steerer maximum strengths
corropt.max_turns_RM = max_turns_RM;    % user NOT allowed to change this.
corropt.cor_s_range = cor_s_range;      % range in m where to use correctors
corropt.bpm_s_range = bpm_s_range;      % range in m where to use BPMs
corropt.meanzero = correctflags(2);     % keep horizontal mean to zero
corropt.meanzeroWeigth = 1;             % wiegth for mean zero in correction
corropt.corRF = correctflags(1);        % correct RF
corropt.corH = correct_H;               % correct H
corropt.corV = correct_V;               % correct V
corropt.svdmode = svd_mode;             % method for svd regularization
switch corropt.svdmode
    case expectedsvdmodes{1}
        corropt.svdparam = max_num_eigen;
    case expectedsvdmodes{2}
        corropt.svdparam = Tikhonov;
end

%% get initial orbit measurement
[t] = obj.measureorbit;
ox=t(1,:)-reforbit(1,:);
oy=t(2,:)-reforbit(2,:);
usebpm = selectsignal(ox,oy,corropt);
nbpmu=length(find(usebpm));

% display initial orbit
obj.plotorbit('figure',false);

% change correction options
response='a';
disp(corropt);
while ~ismember(response, 'yn')  % & ~ischar(response)
    response = input('Change correction options? [y/n]: ','s');
    if isempty(response), response = 'a'; end
end
if response == 'y'
    corropt = CorrectionParametersUpdate(obj,corropt);
end

%% loop until a more than 2 turns are observed
while ~stopped
    
    if printouttext
        disp('Correct closed orbit');
    end
    
    countiter=countiter+1;
    
    %% get current orbit
    t_iter_start =t;
    if firstdata
        t00 = t;
        firstdata=false;
    end
    
    
    % select traejctory response matrix for requested number of turns
    disp('select RM elements');
    [RMH,RMV,RMHd,RMVd] = NturnsRM(obj,indBPM);
    
    
    
    if printouttext
        disp(['Orbit correction']);
    end
    
    % limit to selection of correctors
    rangehcorsel = scorh  > corropt.cor_s_range(1) & scorh  < corropt.cor_s_range(2);
    rangevcorsel = scorv  > corropt.cor_s_range(1) & scorv  < corropt.cor_s_range(2);
    rangebpmsel = sbpm > corropt.bpm_s_range(1) & sbpm < corropt.bpm_s_range(2);
    
    % get lists of disabled BPM and correctors
    % (here, so that BPM and correctors can be disabled on the fly, during correction)
    enabledbpm  = obj.getEnabledBPM;
    enabledcorH = obj.getEnabledHsteerers;
    enabledcorV = obj.getEnabledVsteerers;
    
    %        user selected s range     & enabled
    usecorH =  rangehcorsel               & enabledcorH;
    usecorV =  rangevcorsel               & enabledcorV;
    usebpm  =  rangebpmsel               & enabledbpm;
    
    if length(find(usebpm))==1
        disp('only 1 BPM with good signal. Including first 2 bpms (whatever their signal)');
        usebpm([1,2])=true;
    end
    %% ----- CORRECTION ----- %%
    
    % signal to correct
    X=ox(usebpm)';
    Y=oy(usebpm)';
    
    
    %% COMPUTE HORIZONTAL CORRECTION
    % septa, h steerers, frequency
     % get initial corrector values
    corh0=obj.sh.get;
    
    tothcor=corh0;
    
    % restrict RM to usable correctors
    
    if corropt.corH
        RespH=RMH(usebpm,usecorH);
        steermeanlist = ones(size(corh0(usecorH)));
    else
        RespH=[];
        steermeanlist=[];
    end
    
    if corropt.corH
    if printouttext
        disp('H PLANE');
    end
    
   
    
    
    % dpp correction, add after of correctors
    if corropt.corRF
        RespHd=RMHd(usebpm);
    end
    
    
    % weigth for correctors average =0
    steermeanlist = steermeanlist*corropt.meanzeroWeigth;
    
    % build RMs
    if      corropt.corRF &&  corropt.meanzero % dpp and mean0
        FRH=[ [RespH; steermeanlist] [RespHd';0] ];
    elseif  corropt.corRF && ~corropt.meanzero % dpp no mean 0
        FRH=[ RespH RespHd' ];
    elseif ~corropt.corRF &&  corropt.meanzero % no dpp mean0
        FRH=[RespH; steermeanlist];
    elseif ~corropt.corRF && ~corropt.meanzero % no dpp no mean0
        FRH=RespH;
    end
    
    % if there are NaN in the RM, prompt for RM Measurement or Simulation
    if any(isnan(FRH))
        
        response='a';
        while ~ismember(response, 'ms')  % & ~ischar(response)
            response = input(['Horizontal orbit RM contains NaNs. '....
                'The correction will be stopped.'....
                'Measure or Simulate RM? [m/s]: '],'s');
            if isempty(response), response = 'a'; end
        end
        
        if response == 'm' % Measure
            
            obj.MeasureOrbitRM;
            
        elseif response == 's' % Simulate
            
            obj.SimulateOrbitRM;
            
        end
        
        % end function here.
        break
    end
    
    
    
    % get present list of correctors
    c0 = corh0(usecorH)';
    
    
    % if absolute correction, add present correctors x RM
    
    % get reducen number of eigenvectors RM
    %         [U,S,V]=svd(FRH);
    %         S(corropt.svdparam+1:end,corropt.svdparam+1:end)=0;
    %         MM = U*S*V';
    MM= RespH;
    vec0 = MM*(c0);
    X1 = X - vec0;
    
    % if mean0 correction add 0 to correction vector
    if corropt.meanzero
        vec = [X;0];
        vec1 = [X1;0];
    else
        vec = X;
        vec1 = X1;
    end
    
    % SVD/bestcor Correction
    dch=obj.computecorrection(corropt.svdmode,FRH,vec,corropt.svdparam);
    dch1=obj.computecorrection(corropt.svdmode,FRH,vec1,corropt.svdparam);
    
    % apply only a fraction of the computed correction
    dch = dch*corropt.fraction;
    
    % if dpp correction separate dpp from correctors
    if corropt.corRF
        deltacor0=deltacor;
        if corropt.absolute
            deltacor=dch1(end);
            dch1=dch1(1:end-1);
        else
            %    inCOD(5)=inCOD(5)+dch(end);
            deltacor=dch(end);
            dch=dch(1:end-1);
        end
    end
    
    % limit steerers strengths
    if corropt.corH
        cc = corh0*0;
        
        if corropt.absolute
            cc(usecorH) = dch1;
            tothcor = -cc;
        else
            cc(usecorH) = dch;
            tothcor = corh0 - cc;
        end
        
        if ~isempty(corropt.steererlimit)
            nover=length(find(abs(tothcor)>corropt.steererlimit(1)));
            
            warning([ num2str(nover) ' horizontal Steerers above limit!'])
            
            tothcor(tothcor>corropt.steererlimit(1))=corropt.steererlimit(1);
            tothcor(tothcor<-corropt.steererlimit(1))=-corropt.steererlimit(1);
        end
        
    end
    
     end
    %% COMPUTE VERTICAL CORRECTION
    % get current correctors values
    corv0=obj.sv.get;
    
    totvcor=corv0;
    RespV=RMV(usebpm,usecorV);
    
    if corropt.corV
    if printouttext
        disp('V PLANE');
    end
    
     
    % restrict RM
    steermeanlist = ones(size(corv0(usecorV)));
    
    if corropt.corRF % dpp correction
        RespVd=RMVd(usebpm);
    end
    
    if ~corropt.corV
        RespV=[];
        steermeanlist=[];
    end
    
    % weigth for correctors average =0
    steermeanlist = steermeanlist*corropt.meanzeroWeigth;
    
    % build RMs
    if  corropt.meanzero % no dpp mean0
        FRV=[RespV;steermeanlist];
    elseif ~corropt.meanzero % no dpp no mean0
        FRV=RespV;
    end
    
    
    % if there are NaN in the RM, prompt for RM Measurement or Simulation
    if any(isnan(FRV))
        
        response='a';
        while ~ismember(response, 'ms')  % & ~ischar(response)
            response = input(['Vertical orbit RM contains NaNs. '....
                'The correction will be stopped.'....
                'Measure or Simulate RM? [m/s]: '],'s');
            if isempty(response), response = 'a'; end
        end
        
        if response == 'm' % Measure
            
            obj.MeasureOrbitRM;
            
        elseif response == 's' % Simulate
            
            obj.SimulateOrbitRM;
            
        end
        
        % end function here.
        break
    end
    
    % get present list of correctors
    c0 = corv0(usecorV)';
    
     MM= RespV;
    vec0 = MM*(c0);
    Y1 = Y - vec0;
    
    % if mean0 correction add 0 to correction vector
    if corropt.meanzero
        vec = [Y;0];
        vec1 = [Y1;0];
    else
        vec = Y;
        vec1 = Y1;
    end
    
    % SVD/bestcor Correction
    disp('relative')
    dcv=obj.computecorrection(corropt.svdmode,FRV,vec,corropt.svdparam);
    disp('absolute')
    dcv1=obj.computecorrection(corropt.svdmode,FRV,vec1,corropt.svdparam);
    
    % apply only a fraction of the computed correction
    dcv = dcv*corropt.fraction;
    
    % limit steerers strengths
    if corropt.corV
        cc = corv0*0;
        
        if corropt.absolute
            cc(usecorV) = dcv1;
            totvcor = -cc;
        else
            cc(usecorV) = dcv;
            totvcor = corv0 - cc;
        end
        
        
        if ~isempty(corropt.steererlimit)
            nover=length(find(abs(totvcor)>corropt.steererlimit(2)));
            
            warning([ num2str(nover) ' vertical Steerers above limit!'])
            
            totvcor(totvcor>corropt.steererlimit(2))=corropt.steererlimit(2);
            totvcor(totvcor<-corropt.steererlimit(2))=-corropt.steererlimit(2);
        end
    end    

    end
    %% APPLY CORRECTIONS19/C24 0.02
    
    % display proposed correction
    t0is = t_iter_start;
    
    clf(f);
    set(0,'currentfigure',f);
    ax1=subplot(2,2,1);
    plot(sbpm,reforbit(1,:),'k:'); hold on;
    plot(sbpm,t0is(1,:),'r+-'); hold on;
    %plot(sbpm(~usebpm),t0is(1,~usebpm),'mx','MarkerSize',15,'LineWidth',2);hold on;
    if corropt.corH
    %    plot(sbpm,t0is(1,:)+(RespH*(corh0(usecorH)-tothcor(usecorH))')','co-');
    end
    legend('objective','measured','proposed');
    ylabel('hor [m]');
    %grid on;
    ylim([-corropt.limits(1) corropt.limits(1)]);
    %xlabel('s [m]');
    ax1.XTick=sbpm;
    if strcmp(obj.machine,'ebs-simu')
        ax1.XTickLabel=repmat(cellfun(@(a)a(22:end),sr.bpmname(1:length(indBPM)),'un',0),1,obj.nturns);
    elseif strcmp(obj.machine,'esrf-sr')
        ax1.XTickLabel=repmat(cellfun(@(a)a(12:end),sr.bpmname(1:length(indBPM)),'un',0),1,obj.nturns);
    end
    ax1.XTickLabelRotation=90;
    ax1.FontSize=8;
    ax1.TickLength=[0.0 0.0];
    
    
    ax2=subplot(2,2,2);
    bar(scorh,[repmat(obj.sh.get,1,1); tothcor]','BarWidth',2);
    ylabel('hor [rad]');
    %xlabel('s [m]');
    ax2.XTick=scorh;%1:length(scorh);
    if strcmp(obj.machine,'ebs-simu')
        ax2.XTickLabel=cellfun(@(a)a(1:end),sr.hsteername(1:length(indHCor)),'un',0);
    elseif strcmp(obj.machine,'esrf-sr')
        ax2.XTickLabel=repmat(cellfun(@(a)a(1:end),sr.steername(1:length(indHCor)),'un',0),1,obj.nturns);
    end
    ax2.XTickLabelRotation=90;
    ax2.FontSize=8;
    ax2.TickLength=[0.0 0.0];
    ax3=subplot(2,2,3);
    plot(sbpm,reforbit(2,:),'k:'); hold on;
    plot(sbpm,t0is(2,:),'r+-'); hold on;
    % plot(sbpm(~usebpm),t0is(2,~usebpm),'mx','MarkerSize',15,'LineWidth',2); hold on;
    if corropt.corV
    %    plot(sbpm,t0is(2,:)+(RespV*(corv0(usecorV)-totvcor(usecorV))')','co-');
    end
    legend('objective','measured','proposed');
    ylabel('ver [m]'); grid on;
    ylim([-corropt.limits(2) corropt.limits(2)]);
    xlabel('s [m]');
    ax4=subplot(2,2,4);
    bar(scorv,[repmat(obj.sv.get,1,1); totvcor]');
    ylabel('ver [rad]');
    xlabel('s [m]');
    linkaxes([ax1 ax3],'x');
    linkaxes([ax2 ax4],'x');
    
    if printouttext
        % display results
        disp('Forseen change: ')
        if corropt.corH
        disp(['hor. : ' num2str(std2(ox(usebpm))*1e6,'%3.3f') ' -> '...
            num2str(std2(RespH*(corh0(usecorH)-tothcor(usecorH))')*1e6,'%3.3f') ' um'])
        end
        if corropt.corV
        disp(['ver. : ' num2str(std2(oy(usebpm))*1e6,'%3.3f') ' -> '...
            num2str(std2(RespV*(corv0(usecorV)-totvcor(usecorV))')*1e6,'%3.3f') ' um'])
        end
        disp(['H cor mean: ' num2str(std2(corh0*1e6),'%3.3f') ' -> ' num2str(std2(tothcor*1e6),'%3.3f') ' murad']);
        disp(['V cor mean: ' num2str(std2(corv0*1e6),'%3.3f') ' -> ' num2str(std2(totvcor*1e6),'%3.3f') ' murad']);
        
    end
    
    response='a';
    while ~ismember(response, 'yn')  % & ~ischar(response)
        response = input('Apply the proposed correction? [y/n]: ','s');
        if isempty(response), response = 'a'; end
    end
    
    if response == 'y' % APPLY
        
        if corropt.corH
            
            obj.sh.set(tothcor);
        end
        
        if corropt.corRF
            
            obj.rf.set(f0-alpha*(deltacor)*f0);
            
        end
        
        if corropt.corV
            
            obj.sv.set(totvcor);
            
        end
    elseif response=='n'
        disp('no correction applied');
    end
    
    if printouttext
        disp('correcting orbit'); end
    
    %% MEASURE AFTER CORRECTION and display small summary
    % get current orbit
        
    pause(3); % wait steerers PS and for new data from BPMs TbT (MuST BE LARGER THAN LOOP TIME IN SIMULATOR!)
    t0is = t_iter_start;
    disp('measure after correction');
    
    % measure and use orbit RM
    [t] = obj.measureorbit;
    tothcorc = obj.sh.get;
    totvcorc = obj.sv.get;
    
    % remove reference
    oxc=t(1,:)-reforbit(1,:);
    oyc=t(2,:)-reforbit(2,:);
    
    if printouttext
        % display results
        disp(['hor. : ' num2str(std2(ox(usebpm))*1e6,'%3.3f') ' -> '...
            num2str(std2(oxc(usebpm))*1e6,'%3.3f') ' um'])
        disp(['ver. : ' num2str(std2(oy(usebpm))*1e6,'%3.3f') ' -> '...
            num2str(std2(oyc(usebpm))*1e6,'%3.3f') ' um'])
        disp(['H cor mean: ' num2str(mean(tothcorc*1e6),'%3.3f') ' +/- (std)' num2str(std2(tothcorc*1e6),'%3.3f') ' murad']);
        disp(['V cor mean: ' num2str(mean(totvcorc*1e6),'%3.3f') ' +/- (std)' num2str(std2(totvcorc*1e6),'%3.3f') ' murad']);
        disp(['RF cor mean: ' num2str(alpha*(deltacor)*f0,'%3.3f') ' Hz'])
        
        clf(f);
        set(0,'currentfigure',f);
        ax1=subplot(2,2,1);
        plot(sbpm,reforbit(1,:),'k:'); hold on;
        plot(sbpm,t0is(1,:),'r+-'); hold on;
        plot(sbpm(~usebpm),t0is(1,~usebpm),'mx','MarkerSize',15,'LineWidth',2);
        plot(sbpm,t(1,:),'bo-');
        legend('objective','measured','not used','corrected');
        ylabel('hor [m]');
        %grid on;
        ylim([-corropt.limits(1) corropt.limits(1)]);
        %xlabel('s [m]');
        ax1.XTick=sbpm;
        if strcmp(obj.machine,'ebs-simu')
            ax1.XTickLabel=repmat(cellfun(@(a)a(22:end),sr.bpmname(1:length(indBPM)),'un',0),1,obj.nturns);
        elseif strcmp(obj.machine,'esrf-sr')
            ax1.XTickLabel=repmat(cellfun(@(a)a(12:end),sr.bpmname(1:length(indBPM)),'un',0),1,obj.nturns);
        end
        ax1.XTickLabelRotation=90;
        ax1.FontSize=8;
        ax1.TickLength=[0.0 0.0];
        
        
        ax2=subplot(2,2,2);
        bar(scorh,repmat(obj.sh.get,1,1));
        ylabel('hor [rad]');
        %xlabel('s [m]');
        ax2.XTick=scorh;
        if strcmp(obj.machine,'ebs-simu')
            ax2.XTickLabel=cellfun(@(a)a(1:end),sr.hsteername(1:length(indHCor)),'un',0);
        elseif strcmp(obj.machine,'esrf-sr')
            ax2.XTickLabel=repmat(cellfun(@(a)a(1:end),sr.steername(1:length(indHCor)),'un',0),1,obj.nturns);
        end
        ax2.XTickLabelRotation=90;
        ax2.FontSize=8;
        ax2.TickLength=[0.0 0.0];
        ax3=subplot(2,2,3);
        plot(sbpm,reforbit(2,:),'k:'); hold on;
        plot(sbpm,t0is(2,:),'r+-'); hold on;
        plot(sbpm(~usebpm),t0is(2,~usebpm),'mx','MarkerSize',15,'LineWidth',2);
        plot(sbpm,t(2,:),'bo-');
        legend('objective','measured','not used','corrected');
        ylabel('ver [m]'); grid on;
        ylim([-corropt.limits(2) corropt.limits(2)]);
        xlabel('s [m]');
        
        
        ax4=subplot(2,2,4);
        bar(scorv,repmat(obj.sv.get,1,1));
        ylabel('ver [rad]');
        xlabel('s [m]');
        linkaxes([ax1 ax3],'x');
        linkaxes([ax2 ax4],'x');
        
        
    end
    
    %% check final result and promp for next steps
    
    ox = oxc;
    oy = oyc;
    
    if countiter>1
        nbpmuprev=nbpmu;% store previous value
        lastsigprev = lastsig;
    end
    
    usebpm = selectsignal(ox,oy,corropt);
    nbpmu = length(find(usebpm));
    lastsig = find(~isnan(ox),1,'last');
    
    if any(std2(t0is([1,2],:)')<std2(t([1,2],:)'))
        
        disp('Correction regressing ');
        disp(['rms orbit not decreased: ' num2str(nbpmu)]);
        disp(['Before Correction: ' num2str(std2(t0is'))]);
        disp(['After Correction: ' num2str(std2(t'))]);
        
        response='a';
        while ~ismember(response, 'kr')  % & ~ischar(response)
            response = input('Keep or Revert? [k/r]: ','s');
            if isempty(response), response = 'a'; end
        end
        
        if response == 'r' % REVERT
            
            if corropt.corV
                obj.sv.set(corv0);
            end
            if corropt.corH
                obj.sh.set(corh0);
            end
            
            if corropt.corRF
                obj.rf.set(f0-alpha*(deltacor0)*f0);
            end
            disp('Change correction parameters to improve next correction')
            corropt = CorrectionParametersUpdate(obj,corropt);
        end
        
        
    end
    
    
    if ask2cont && ~stopped
        response='a';
        while ~ismember(response, 'yn')  % & ~ischar(response)
            response = input('Continue orbit correction? [y/n]: ','s');
            if isempty(response), response = 'a'; end
        end
        
        if response == 'y'
            
            stopped = false;
            
            % chang correction options
            response='a';
            while ~ismember(response, 'yn')  % & ~ischar(response)
                response = input('Change correction options? [y/n]: ','s');
                if isempty(response), response = 'a'; end
            end
            if response == 'y'
                corropt = CorrectionParametersUpdate(obj,corropt);
            end
            
            
        else
            stopped = true;
            disp('correction stopped')
        end
    end
    
    
end % loop until 2 turns

%% display final result
if printouttext
    
    ox=t00(1,:);
    oy=t00(2,:);
    
    t = obj.last_measured_orbit;
    
    oxc=t(1,:);
    oyc=t(2,:);
    
    
    % display results
    disp('absolute orbit measured (no reference subtracted)')
    disp(['Hor. : ' num2str(std2(ox)*1e6,'%3.3f') ' -> '...
        num2str(std2(oxc)*1e6,'%3.3f') ' um'])
    disp(['Ver. : ' num2str(std2(oy)*1e6,'%3.3f') ' -> '...
        num2str(std2(oyc)*1e6,'%3.3f') ' um'])
end

% % save final orbit and correctors
% fn=obj.TrajFileName ;
% obj.TrajFileName = [fn 'Final'];
% obj.measureorbit;
% obj.TrajFileName = fn ;

% read final steerer settings
if corropt.corH
    hs = obj.sh.get;
else
    disp('wished h steerers values, not set')
    hs = corh0-dch;
end

if corropt.corV
    vs = obj.sv.get;
else
    disp('wished h steerers values, not set')
    vs = corv0-dcv;
end



if corropt.corRF
    freq = obj.rf.get;
else
    disp('wished frequency change')
    freq = f0-alpha*(deltacor)*f0;
end

end

function corropt = CorrectionParametersUpdate(obj,corropt)
% CorrectionParametersUpdate update correction parameters at user request
%
% corropt.tikhonovParam = 5;
% corropt.cor_selection = true(size(indHCor));
% corropt.bpm_selection = true(size(indBPM));
% corropt.fraction = 0.5;
% corropt.limits = lim;
% corropt.steererlimit = steererlimit;

disp('current correction options')
disp(corropt)

corropt.limits(1)       = inputcor('Maximum horizontal excursion',corropt.limits(1));
corropt.limits(2)       = inputcor('Maximum vertical excursion' ,corropt.limits(2));
corropt.fraction        = inputcor('Fraction of computed correction to be set', corropt.fraction);
corropt.steererlimit(1) = inputcor('Maximum hor. steerer strength', corropt.steererlimit(1));
corropt.steererlimit(2) = inputcor('Maximum ver. steerer strength', corropt.steererlimit(2));
obj.sh.min_val = - corropt.steererlimit(1);
obj.sh.max_val = + corropt.steererlimit(1);
obj.sv.min_val = - corropt.steererlimit(2);
obj.sv.max_val = + corropt.steererlimit(2);

corropt.number_of_acquisitions        = inputcor('# of orbits to average', corropt.number_of_acquisitions);
obj.n_acquisitions = corropt.number_of_acquisitions;
corropt.cor_s_range     = inputcor('use correctors in this range [m]',corropt.cor_s_range);
corropt.bpm_s_range     = inputcor('use BPMs in this range [m]',corropt.bpm_s_range);
corropt.svdmode         = inputcor('svd mode [''eigenvectors/Tikhonov/BestCor''], type [e/t/b] ',corropt.svdmode,false,false,true);
switch corropt.svdmode
    case 't'
        corropt.svdparam       = inputcor('Tikhonov parameter ',corropt.svdparam);
    case 'e'
        corropt.svdparam       = inputcor('max num of eigenvectors ',corropt.svdparam);
    case 'b'
        disp('no parameter for best cor');
    otherwise
        disp('mode not ''e'' or ''t''. default to ''e''');
        corropt.svdmode = 'e';
        corropt.svdparam       = inputcor('max num of eigenvectors ',corropt.svdparam);
end
corropt.corH            = inputcor('compute horizontal correction',corropt.corH);
corropt.corV            = inputcor('compute vertical correction',corropt.corV);
corropt.corRF           = inputcor('compute RF correction',corropt.corRF);
corropt.absolute        = inputcor('Absolute (true) or Relative (false) correction',corropt.absolute);
if corropt.absolute
    disp('absolute correction, no partial setting of correctors, plese use a large number of eigenvalues (100/300) or Tikhonov parameter (5-10).')
end

corropt.meanzero        = inputcor('keep correctors mean to zero',corropt.meanzero);
corropt.meanzeroWeigth  = inputcor('keep correctors mean to zero weigth',corropt.meanzeroWeigth);

    function n = inputcor(msg,val,showdef,validatesize,textinput)
        if nargin<3, showdef = true; end
        if nargin<4, validatesize = true; end
        if nargin<5, textinput = false; end % 's'
        
        if showdef
            msg = [msg ' [' num2str(val) ']: '];
        else
            msg = [msg ': '];
        end
        
        if ~textinput
            n = input(msg);
        else
            n = input(msg,'s');
        end
        
        if isempty(n)
            n=val;
        end
        
        if validatesize
            if any(size(n)~=size(val))
                disp(['Size of input should be ' num2str(size(val)) ', found ' num2str(size(n))])
                n = inputcor(msg,val,showdef);
            end
        end
        
    end


end


function usebpm = selectsignal(ox,oy,corropt)
% select BPM readings to use for correction

usebpm=~isnan(ox) & ~isnan(oy) &....
    ox <  corropt.limits(1) &....
    ox > -corropt.limits(1) &...
    oy <  corropt.limits(2) &...
    oy > -corropt.limits(2);

% remove spikes from BPM readings
stdx=std(ox(usebpm));
stdy=std(oy(usebpm));

usebpm=~isnan(ox) & ~isnan(oy) &...
    ox < corropt.limits(1) & ox > -corropt.limits(1) &...
    oy < corropt.limits(2) & oy > -corropt.limits(2) &...
    abs(ox) < 3*stdx & abs(oy) < 3*stdy;

end


function [RMH,RMV,RMHd,RMVd] = ...
    NturnsRM(...
    obj,...
    indBPM)
% reduce ModelRM to n turns only

RMH  = obj.ModelRM.OrbHCor{1};
RMV  = obj.ModelRM.OrbVCor{3};
RMHd = obj.ModelRM.OrbHDPP;
RMVd = obj.ModelRM.OrbVDPP;


%%  check matrix size is correct
if exist('RMH','var')
    
    disp('Check MATRIX size:');
    disp(['tot.  # bpm: ' num2str(length(obj.indBPM))]);
    disp(['tot.  # hst: ' num2str(length(obj.indHst))]);
    disp(['tot.  # vst: ' num2str(length(obj.indVst))]);
    exprmsize=[length(indBPM) length(obj.indHst)];
    disp(['expected hor. RM size: ' num2str(exprmsize)]);
    disp([' ModelRM hor. RM size: ' num2str(size(RMH))]);
    if any(size(RMH) ~= exprmsize)
        
        response='a';
        while ~ismember(response, 'sme')  % & ~ischar(response)
            response = input(['RM size not correct. Simulate,'...
                ' Measure or Exit? [s/m/e]: '],'s');
            if isempty(response), response = 'a'; end
        end
        
        if response == 'm' % Measure
            
            obj.MeasureOrbitRM;
            
        elseif response == 's' % Simulate
            
            obj.SimulateOrbitRM;
            
        elseif response == 'e' % Simulate
            
            return
        end
        
    end
end

end



