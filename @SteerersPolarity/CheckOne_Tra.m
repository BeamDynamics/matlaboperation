function Error = CheckOne_Tra(obj,index,plane)
%CHECKPOLONESTEERER checks the polarity of steerer of index 'index'
%   
%   Error = CheckPolOneSteerer(index,plane)
%  index is the steerer number that you want to check
%  plane is 'H' or 'V'
%  Error is the error on the trajectory in the following bpms caused by a
%  wrong calibration of the steerer. If the sign is opposite, Error is
%  supposed to be ~2. If the calibration is ok, should be |Error|<~0.2 
%
%  see also: CheckPolArraySteerers

if plane=='H'
    sst=obj.sHst(index);
    indplane=1;
    v_zero=obj.sh.get();
elseif plane=='V'
    sst=obj.sVst(index);
    indplane=2;
    v_zero=obj.sv.get();
else
    error('plane must be ''H'' or ''V''')
end

%% plane H or V

bpmafter=find(obj.sBPM>sst);

%measurements of trajectories with steerer index at +kick and -kick
v=v_zero;
v(index)=v(index)+obj.kick_rad;
if plane=='H'
    obj.sh.set(v);   % set hor steerers with one at +kick
elseif plane=='V'
    obj.sv.set(v);   % set ver steerers with one at +kick
end
pause(2)
[tra_p_Meas, err_tra_p_Meas]=obj.measuretrajectory;
v=v_zero;
v(index)=v(index)-obj.kick_rad;
if plane=='H'
    obj.sh.set(v);   % set hor steerers with one at +kick
elseif plane=='V'
    obj.sv.set(v);   % set ver steerers with one at +kick
end
pause(2)
[tra_n_Meas, err_tra_n_Meas]=obj.measuretrajectory;
if plane=='H'
    obj.sh.set(v_zero);   % set hor steerers with one at +kick
elseif plane=='V'
    obj.sv.set(v_zero);   % set ver steerers with one at +kick
end
pause(2)

%simulations of trajectories with steerer index at +kick, 0 and -kick
tra_p_Sim=obj.SimulateTrajectoryKick(index,obj.kick_rad,plane);
tra_n_Sim=obj.SimulateTrajectoryKick(index,-obj.kick_rad,plane);

% compute dtraj_pos and dtraj_neg measured and simulated
dtra_Meas=tra_p_Meas(indplane,:)-tra_n_Meas(indplane,:);
err_dtra_Meas=sqrt(err_tra_p_Meas(indplane,:).^2 + err_tra_n_Meas(indplane,:).^2); %calc error from 3 measurements
maskbpm=~isnan(dtra_Meas)&~isnan(err_dtra_Meas);
dtra_Sim=tra_p_Sim(indplane,maskbpm)-tra_n_Sim(indplane,maskbpm);

% indbpmafter=bpmafter(1:obj.nbpm_after);
% indbpmafter=maskbpm.*[obj.sBPM>sst, ones(size(obj.sBPM))];
indbpmafter=maskbpm;
Error=100*rms((dtra_Meas(indbpmafter)-dtra_Sim(indbpmafter))/(err_dtra_Meas(indbpmafter)));

if(obj.plot)
    figure;
    errorbar(1e3*dtra_Meas(indbpmafter),1e3*err_dtra_Meas(indbpmafter),'.-','LineWidth',2,'MarkerSize',12,'DisplayName','Measured');
    hold on; grid on;
    plot(1e3*dtra_Sim(indbpmafter),'.-','LineWidth',2,'MarkerSize',12,'DisplayName','Simulated');
    l=legend('Location','NorthWest');
%     title(l,['Hor err=' num2str(Error,'%1.1f') '%']);
    ylabel('\Delta traj (mm)')
    xlabel('bpm number');
end
disp('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
disp(['Steerer number ' num2str(index) ' of ' plane ' plane']);
disp(['Error = ' num2str(Error,'%1.1f') '%']);
disp('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');

end

