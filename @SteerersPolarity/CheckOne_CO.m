function Error = CheckOne_CO(obj,index,plane)
%CHECKONE_CO checks the polarity of one steerer using closed orbit 
%   
%  Error = CheckPolOneSteerer_CO(plane,index)
%  index is the steerer number that you want to check
%  plane is 'H' or 'V'
%  Error is the error on the orbit in all bpms caused by a
%  wrong calibration of the steerer. If the sign is opposite, Error is
%  supposed to be ~2. If the calibration is ok, should be |Error|<~0.2 
%
%  see also: 

if plane=='H'
    sst=obj.sHst(index);
    indplane=1;
    v_zero=obj.sh.get();
elseif plane=='V'
    sst=obj.sVst(index);
    indplane=2;
    v_zero=obj.sv.get();
else
    error('plane must be ''H'' or ''V''')
end

%% plane H or V

% bpmafter=find(obj.sBPM>sst);

%measurements of orbits with steerer index at +kick and -kick
v=v_zero;
v(index)=v(index)+obj.kick_rad;
if plane=='H'
    obj.sh.set(v);   % set hor steerers with one at +kick
elseif plane=='V'
    obj.sv.set(v);   % set ver steerers with one at +kick
end
pause(2)

% read the values at positive kick to be sure
if plane=='H'
    valp=obj.sh.get;   % set hor steerers with one at +kick
elseif plane=='V'
    valp=obj.sv.get;   % set ver steerers with one at +kick
end

[orb_p_Meas, err_orb_p_Meas]=obj.measureorbit;
v=v_zero;
v(index)=v(index)-obj.kick_rad;
if plane=='H'
    obj.sh.set(v);   % set hor steerers with one at +kick
elseif plane=='V'
    obj.sv.set(v);   % set ver steerers with one at +kick
end
pause(2)
% read the values at negative kick to be sure
if plane=='H'
    valn=obj.sh.get;   % set hor steerers with one at +kick
elseif plane=='V'
    valn=obj.sv.get;   % set ver steerers with one at +kick
end
applied2kicks= valp(index)-valn(index);
[orb_n_Meas, err_orb_n_Meas]=obj.measureorbit;
if plane=='H'
    obj.sh.set(v_zero);   % set hor steerers with one at +kick
elseif plane=='V'
    obj.sv.set(v_zero);   % set ver steerers with one at +kick
end
pause(2)

%simulations of orbit with steerer index at +kick, 0 and -kick
orb_p_Sim=obj.SimulateOrbitKick(plane,index,obj.kick_rad);
orb_n_Sim=obj.SimulateOrbitKick(plane,index,-obj.kick_rad);

% compute dorb (positive - negative) measured and simulated
dorb_Meas=(orb_p_Meas(indplane,:)-orb_n_Meas(indplane,:))*2*obj.kick_rad/applied2kicks;
err_dorb_Meas=sqrt(err_orb_p_Meas(indplane,:).^2 + err_orb_n_Meas(indplane,:).^2); %calc error from 3 measurements
dorb_Sim=orb_p_Sim(indplane,:)-orb_n_Sim(indplane,:);

% indbpmafter=bpmafter(1:obj.nbpm_after);
% Error=rms((dorb_Meas-dorb_Sim)/(err_dorb_Meas));
indbig=find(abs(dorb_Sim)>mean(abs(dorb_Sim)/2));
Error=mean(dorb_Meas(indbig)./dorb_Sim(indbig))-1;

if(obj.plot)
    close(gcf)
    figure;
    errorbar(1e3*dorb_Meas,1e3*err_dorb_Meas,'.-','LineWidth',2,'MarkerSize',12,'DisplayName','Measured');
    hold on; grid on;
    plot(1e3*dorb_Sim,'.-','LineWidth',1.2,'MarkerSize',12,'DisplayName','Simulated');
    l=legend('Location','NorthWest');
    title(l,['Hor err=' num2str(Error,'%1.2f')]);
    ylabel('\Delta orb (mm)');
    xlabel('bpm number');
    title(['Corrector # ' num2str(index) ': ' obj.HstNames{index}]);
    saveas(gcf,['./plots/steerer_' plane num2str(index) '.fig']);
    %close(gcf);
end
disp('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
disp(['Steerer number ' num2str(index) ' of ' plane ' plane']);
disp(['Error = ' num2str(Error,'%1.2f') ]);
disp('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');

end

