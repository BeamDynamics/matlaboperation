function o = SimulateOrbitKick(obj,plane,indSt,kick)
%SIMULATEORBITKICK simulates the closed orbit on a perfect machine with a
%   kick on a steerer
%
%  t = SimulateOrbitKick(plane,indSt,kick)
%  t    2x(Nbpmx2) array of  trajectory (hor and vert)
%  
%  plane is 'H' or 'V'
%  indSt is the number of the steerer that you want to power
%  kick is the angle in radians you want to apply to the steerer
%
%  The simulation is done with findorbit6. 
%  steerer is horizonal or vertical depending on obj.plane
%  number of turns simulated is obj.nturns
%  disabled bpms (obj.getEnabledBPM) give nan as trajectory
%
% see also: findorbit6, getEnabledBPM

bpm_enabled=obj.getEnabledBPM;

r=obj.rmodel;
if strcmp(plane,'H')
    r{obj.indHst(indSt)}.KickAngle=[kick,0];
elseif strcmp(plane,'V')
    r{obj.indVst(indSt)}.KickAngle=[0, kick];
else
    error('plane must be ''H'' or ''V''');
end

orb=findorbit6(r,obj.indBPM);

o=zeros(2,length(obj.indBPM));
o(1,:)=orb(1,:);
o(1,~bpm_enabled)=nan;

o(2,:)=orb(3,:);
o(2,~bpm_enabled)=nan;
o(2,isnan(o(1,:)))=nan;

end