function errors = CheckMany_CO(obj,indexes,plane)
%CHECKMANY_CO checks polarity of many steerers using Closed Orbit
%
%   errors = CheckMany_CO(indexes,plane)
%   indexes is an array of indexes of steerers
%   plane is 'H' 'V'
%
%   errors is a vector with same length of indexes. 
%   
%   if calibation of corrector ii is good, we expect abs(errors(ii))<~0.2
%
%   see also: CheckOne_CO

if obj.plot
    mkdir('./plots');
end

errors=zeros(size(indexes));

for ii=1:length(indexes)
    errors(ii)=obj.CheckOne_CO(indexes(ii),plane);
end

figure;
plot(indexes,errors,'.-','MarkerSize',22,'LineWidth',2);
xlabel('steerer number');
ylabel('error');
title(['errors of steerers in ' plane 'plane']);
saveas(gca,['plots/errors_st_' plane '.fig']);

end
