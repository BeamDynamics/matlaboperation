function errors = CheckMany_Tra(obj,indexes,plane)
% CHECKMANY_TRA Checks the polarity of many steerers in H or V plane
%
%
%  see also: CheckOne_Tra

errors=zeros(size(indezes));

% ErrorsH=nan(size(indexes));
% ErrorsV=nan(size(indexes));
for ii=1:length(indexes)
    pause(2);
    errors=obj.CheckOne_Tra(indexes(ii),plane);
end

figure;
plot(indexes,errors,'.-','LineWidth',2,'MarkerSize',18)
hold on; grid on;
xlabel('steerer number');
ylabel('Error')
%legend('Location','NorthEast')

end