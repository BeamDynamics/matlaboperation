classdef SteerersPolarity < RingControl
    %STEERERSPOLARITY is a Class to check if the steerers have the good
    %calibration. 
    %   The class can be used in the esrf-sr, esrf-sy or ebs-simu
    %
    %   Example on how to use it:
    %   
    %   1. put the bpm of the machine you want in TBT mode.
    %   2. create a "SteerersPolarity" object
    %       SP=SteerersPolarity('ebs-simu') or
    %       SP=SteerersPolarity('esrf-sr')
    %   3. check the polarity of a single steerer
    %       error=SP.CheckPolOneSteerer(number,'H') or
    %       error=SP.CheckPolOneSteerer(number,'V'), where number is the
    %       steerer number and error is the error in trajectory on the next
    %       'obj.nbpm_after' bpms after a kick of 'obj.kick_rad' radians
    %       if |error|<~0.5 then you don't have sign problems
    %       A sign error in a steerer should give error~-2
    %   4. check the polarity of many steerers
    %       errors=SP.CheckPolArraySteerers([10 12 15 25 30]) checks the 
    %       polarity of the steerers of the list you pass
    %
    %  see also: RingControl
    
    properties
        kick_rad;
        nbpm_after;
        plot;
        sBPM;
        sHst;
        sVst;
        HstNames;
        VstNames;
    end
    
    methods
        function obj = SteerersPolarity(machine)
            %STEERERSPOLARITY Construct an instance of the class
            %SteerersPolarity
            %   
            %   SP=SteerersPolarity(machine)
            %   machine can be: 'ebs-simu', 'esrf-sr', 'esrf-sy'
            %   
            %   see also: RingControl
            
            % call the construct of RingControl
            obj@RingControl(machine);
            obj.max_stored_current=0.05;
            % put the default values in the properties
            obj.plot=true;
            obj.kick_rad=1e-5;
            obj.nbpm_after=50;
            obj.setTurns(2);
            % find s positions of BPMs and steerers
            obj.sBPM=findspos(obj.rmodel,obj.indBPM);
            obj.sHst=findspos(obj.rmodel,obj.indHst);
            obj.sVst=findspos(obj.rmodel,obj.indVst);
            obj.n_acquisitions=3;
            hst=tango.Device('srmag/hst/all');
            vst=tango.Device('srmag/vst/all');
            obj.HstNames=hst.CorrectorNames.read;
            obj.VstNames=vst.CorrectorNames.read;
            
        end
        

        %this function checks the polarity a single steerer
        Error=CheckOne_Tra(obj,index,plane);
        
        %this function checks the polarity an array of steerers
        Errors=CheckMany_Tra(obj,indexes,plane);
        
        %this function checks the polarity a single steerer, with closed
        %orbit
        Error=CheckOne_CO(obj,index,plane);
        
        %this function checks the polarity of many steerers (in H or V plane)
        Errors=CheckMany_CO(obj,indexes,plane);
        
    end
end

