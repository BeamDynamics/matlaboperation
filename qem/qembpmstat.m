function [bhgain,bhrot,bvgain,bvrot]=qembpmstat(dirlist)
% %DIRLIST=QEMBPMSTAT(FILENAME) returns all directories containing "FILENAME"
% %
% %[DISLIST,V1,V2,...]=QEMBPMSTAT(FILENAME,VAR1,VAR2,...)
% %   V1,V2...: cell array containing the variables VAR1,VAR2... of each selected file
% %
% nset=nargin-1;
% varargout=cell(1,nset);
% fls=dirfind(fullfile('/machfs','MDT','respmat','quad'),filename);
% nf=length(fls);
% for s=1:nset
%     varargout{s}=cell(1,nf);
% end
% for f=1:nf
%     a=load(fullfile(fls{f},filename));
%     for s=1:nset
%         if isfield(a,varargin{s})
%             varargout{s}{f}=a.(varargin{s});
%         end
%     end
% end
[vbhgain,vbhrot,vbvgain,vbvrot]=varextract(dirlist,'gainfit3.mat',...
    'bhgain','bhrot','bvgain','bvrot');
bhgain=cat(2,vbhgain{:});
bhrot=cat(2,vbhrot{:});
bvgain=cat(2,vbvgain{:});
bvrot=cat(2,vbvrot{:});
