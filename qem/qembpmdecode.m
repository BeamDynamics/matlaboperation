function [rh0,rv0] = qembpmdecode(rhbpm,rvbpm,brot,bhgain,bvgain)
%QEMBPMDECODE   Correct response matrices for BPM gain and rotation
%[RH0,RV0]=QEMBPMDECODE(RHBPM,RVBPM,ROTATION,HGAIN,VGAIN)
%R0=QEMBPMDECODE(RBPM,GAIN)

cb=cos(brot);
sb=sin(brot);
rh0= rhbpm.*cb.*bhgain - rvbpm.*sb.*bvgain;
rv0= rhbpm.*sb.*bhgain + rvbpm.*cb.*bvgain;
end
