function [mach,shl] = qemsetsext(mach,sextidx,shl0,chroms)
%MACH=QEMSETSEXT(MACH0,SXIDX,HL0) get/set integrated sextupole strengths

if nargin >= 3 && ~isempty(shl0)
    hl=getcellstruct(mach,'Length',sextidx);
    hl(hl==0)=1;
    mach=setcellstruct(mach,'PolynomB',sextidx,shl0./hl,3);
end
if nargin >= 4 && ~isempty(chroms)
    mach=atfitchrom(mach,chroms,'S19','S20');
    %   mach=atfitchrom(mach,chroms,'S19','S20');
end
if nargout >= 2
    if ~exist('hl','var')
        hl=getcellstruct(mach,'Length',sextidx);
        hl(hl==0)=1;
    end
    shl=getcellstruct(mach,'PolynomB',sextidx,3).*hl;
end
