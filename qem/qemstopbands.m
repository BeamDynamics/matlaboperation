function stpb=qemstopbands(qemres,qemb,qemb0)
% compute stopband from quadrupole gradients.

% added unique. correctors and quadrupoles are the same magnets!
[indbeta,~,ic]=unique([qemres.qpidx, qemres.qcoridx]);

[idx,ik]=sort(indbeta); 
[lind,avb,avm]=atavedata(qemb0.at,0,idx); %#ok<ASGLU>
beta(ik,:)=avb;
phas(ik,:)=avm./qemb0.tunes(ones(size(avm,1),1),:);
kl=[(qemb.kn-qemb0.kn).*qemres.ql;qemb.cor(:,end)];

disp('computing resonances: 2Qx=152, 153, 2Qy= 54, 55. should be the same of sr.resonquad.m')
stpb.h152=gline(152,kl,beta(ic,1),phas(ic,1));
stpb.v54=gline(54,kl,beta(ic,2),phas(ic,2));
stpb.h153=gline(153,kl,beta(ic,1),phas(ic,1));
stpb.v55=gline(55,kl,beta(ic,2),phas(ic,2));

    function vv=gline(harm,kl,beta,phas)
        vr=sum(kl.*beta.*cos(harm*phas));
        vi=sum(kl.*beta.*sin(harm*phas));
        vv=sqrt(vr.*vr+vi.*vi)/2/pi;
    end

end
