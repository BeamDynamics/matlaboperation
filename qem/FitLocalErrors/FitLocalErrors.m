function [knl, ksl, serr] = FitLocalErrors(....
    ReferenceRM,...
    PerturbedRM,...
    errorlocation,...
    errorlength,...
    neig)
%FITLOCALERRORS fit the best error to reproduced the difference between 
% Reference and Perturebed RM at errorlocation s positions
% 
% INPUT:
% ReferenceRM   : folder where the RM data is loaded or RM itself
% PerturbedRM   : folder where the RM data is loaded or RM itself
% errorlocation : list of s positions of error sources (
% errorlength   : length of the error source (default 1e-6)
% neig          : # of eigenvectors for svd (default: length(errorlocation))
%
% if a thick element fit fals into a non DriftPass element, that element is
% used as fit element.
% 
% OUTPUT:
% integrated normal (knl) and skew (ksl) quadrupole gradients (1/m) and
% serr ordered locations of errors
%
% EXAMPLE:
% 
% >> [knl, ksl] = FitLocalErrors('/machfs/MDT/2018/Sep26/respAC14','/machfs/MDT/2018/Sep26/respAC22',[286.34 584.438]);
%
% WARNING: 
% errorlocation should not be inside dipoles! an
% 
%see also: qemextract atsbreak atinsertelems
if nargin<4
    errorlength = 1e-6 * ones(size(errorlocation)); 
end
if nargin<5
    neig=length(errorlocation); 
end

if any(errorlength<0)
    error('Error source lengths must be positive');
end


global qemres

% load reference (normal and skew)
[qemb, qemres, semres] = qemextract(ReferenceRM,'complete',false);

% load perturbed (normal and skew)
[~, qemresPert, semresPert] = qemextract(PerturbedRM,'complete',false);

% introduce generic straight multipole element with zero strenghts at given
% s location
r = qemb(2).at;

C = findspos(r,length(r)+1);

if any(errorlocation>C) | any(errorlocation<0)
    error(['Some locations are below 0 or above length of lattice (' num2str(C) ')']);
end

Lthin = errorlength;
if  ~any(Lthin ~= 1e-6) % if all thin, keep existing behaviour 
    [rerr,errpos]=atsbreak(r,errorlocation);
    
    rerr = atsetfieldvalues(rerr,errpos,'PassMethod', 'StrMPoleSymplectic4Pass');%'ThinMPolePass';
    rerr = atsetfieldvalues(rerr,errpos,'Class', 'Quadrupole');% NECESSARY for FIT function
    rerr = atsetfieldvalues(rerr,errpos,'PolynomB',{1,1}, 0);
    rerr = atsetfieldvalues(rerr,errpos,'PolynomA',{1,1},0);
    rerr = atsetfieldvalues(rerr,errpos,'PolynomB',{1,2}, 0);
    rerr = atsetfieldvalues(rerr,errpos,'PolynomA',{1,2},0);
    rerr = atsetfieldvalues(rerr,errpos,'MaxOrder', 1);
    rerr = atsetfieldvalues(rerr,errpos','Length', Lthin');
    rerr = atsetfieldvalues(rerr,errpos,'NumIntSteps', 1);
else % long errors
    
    rerr =r; 
    
    % loop error locations
    for ierr = 1:length(errorlocation)
        [~,ipos]=atsbreak(rerr,errorlocation(ierr)); % find element where to insert 
    
        elem=atmultipole(['Error' num2str(ierr)],...
            errorlength(ierr),[0 0],[0 0],...
            'StrMPoleSymplectic4Pass',...
            'Class','Quadrupole',...
            'MaxOrder', 1,...
            'NumIntSteps', 20);
        
        existing_magnet_flag = false;
        PMins = rerr{ipos-1}.PassMethod; % Passmethod of element where to insert the new element.
        if strcmp(PMins, 'StrMPoleSymplectic4Pass') || strcmp(PMins, 'StrMPoleSymplectic4RadPass')
            disp(rerr{ipos-1});
            disp([PMins ' at location :' num2str(errorlength(ierr)) ' m']);
            disp(['use :' num2str(rerr{ipos-1}.FamName) ' as fit location']);
            existing_magnet_flag = true;
            rerr{ipos-1}.FamName=['Error' rerr{ipos-1}.FamName];
            errorlength(ierr) = rerr{ipos-1}.Length;
        elseif ~strcmp(PMins, 'DriftPass')
            disp(rerr{ipos-1});
            error(['Not a DriftPass nor a thick multipole at location :' num2str(errorlocation(ierr)) ' m']);
        end
        
        if ~existing_magnet_flag
            
            %find fraction to insert
            Lins = rerr{ipos-1}.Length; % length of element where to insert the new element.
            if Lins < errorlength(ierr)
                error(['Not enough space at location :' num2str(errorlength(ierr)) ' m']);
            end
        
            sp = findspos(rerr,ipos-1);
            frac = (errorlocation(ierr)-sp)./Lins;
            rerr = atinsertelems(rerr,ipos-1,frac,elem);
        end
    end
    
    % get error location indexes
    errpos= find(atgetcells(rerr,'FamName','Error\w*'));
       
    % check that no negative drift is created.
    poscheck = sort([errpos;errpos-1;errpos+1]);
    DriftLengths = atgetfieldvalues(rerr,poscheck,'Length');
    sposerr = findspos(rerr,poscheck);
    if any(DriftLengths<0)
        for ip = 1:length(poscheck)
            disp([rerr{poscheck(ip)}.FamName '@' num2str(sposerr(ip),'%3.2f')...
                ' : ' num2str(DriftLengths(ip),'%4.3f') ' m']);
        end
        error('Found Negative Drifts');
    end
    
    % check lattice length unchanged.
    C0 = findspos(r,length(r)+1);
    Cnew = findspos(rerr,length(rerr)+1);
    if Cnew-C0 > 1e6*length(errorlocation) 
        disp(C0);
        disp('became: ');
        disp(Cnew);
        error('Lattice length changed');
    end
end

% redefine fields of qemres and semres for fit of rm difference with local error
qemres.qpidx = errpos; % index of location to fit
qemres.qpfit = true(size(errpos));
qemres.skewidx = errpos;
semres.skewkeep = true(size(errpos));

qemb(2).at = rerr;
qemres.at = rerr;
% recompute indexes (new elements introduced)

thinsext=atgetcells(qemres.at(:),'Class',@isthinsext,'ThinMultipole');
thicksext=atgetcells(qemres.at(:),'Class','Sextupole');
qemres.sextidx=reshape(find(thinsext|thicksext),1,[]);
qemres.steerhidx= find(atgetcells(qemres.at(:),'FamName','S[FDHIJ]\w*','DQ\w*'))';
qemres.steervidx= find(atgetcells(qemres.at(:),'FamName','S[FDHIJ]\w*'))';
qemres.bpmidx=findcells(qemres.at(:),'Class','Monitor');

% get dipole names from original lattice:
dipnames = unique(atgetfieldvalues(r,qemres.dipidx,'FamName'));

qemres.dipidx = find(atgetcells(qemres.at(:),'FamName',dipnames{:}))';

qemres.ql=atgetfieldvalues(qemres.at(qemres.qpidx),'Length'); % this vector is used by semquadrespAnalytic
qemres.ql(qemres.ql==0) = 1; % integrated strengths;

% get analytic rm derivatives
% hw=waitbar(0,'Fitting quadrupole strengths (analytic)...');
% dispfunc=@(i,itot) waitbar(i/itot,hw);
dispfunc=@(a,b)disp(['Fitting quadrupole strengths (analytic): ' num2str(a) '/' num2str(b)]);
okbpm(1:320) = true; okbpm=okbpm';% added by Andrea
okbpm(qemres.wrongbpms) = false;

shidx=qemres.steerhidx(qemres.hlist);
svidx=qemres.steervidx(qemres.vlist);
nhst=length(shidx);
nvst=length(svidx);

[~,o0]=findsyncorbit(qemb(2).at,qemres.ct,qemres.bpmidx);
dpp=o0(5);
[~,fractunes]=atlinopt(qemb(2).at,dpp,[],o0);

dresp=qemderivAnalytic(qemb(2).at,dpp,qemres,@setk,0.0001,qemres.qpidx,qemres.bpmidx,shidx,svidx,dispfunc);
drespskew=semquadrespAnalytic(qemb(2),qemres,semres);
% save -ascii pippo.txt dresp;
% return; 

% load RM
wrongbpms=qemres.wrongbpms; % find(bpmm.All_Status.read~=0)
[semres.resph,semres.respv]=semloadresp(qemres,semres,wrongbpms);
[qemres.resph,qemres.respv,qemres.frespx,semres.frespz]=qemcheckresp(qemres,semres,wrongbpms);
%semres.respiax=load_sriaxresp(qemres.datadir,qemres.opticsdir,'h2v',semres.hlist);
%qemres.respiax=load_sriaxresp(qemres.datadir,qemres.opticsdir,'v',qemres.vlist);

wrongbpms=qemresPert.wrongbpms; % find(bpmm.All_Status.read~=0)
qemresPert.hlist = qemres.hlist;
qemresPert.vlist = qemres.vlist;
semresPert.hlist = semres.hlist;
semresPert.vlist = semres.vlist;

[semresPert.resph,semresPert.respv]=semloadresp(qemresPert,semresPert,wrongbpms);
[qemresPert.resph,qemresPert.respv,qemresPert.frespx,semresPert.frespz]=qemcheckresp(qemresPert,semresPert,wrongbpms);
%semresPert.respiax=load_sriaxresp(qemresPert.datadir,qemresPert.opticsdir,'h2v',semresPert.hlist);
%qemresPert.respiax=load_sriaxresp(qemresPert.datadir,qemresPert.opticsdir,'v',qemresPert.vlist);

% get perturbed RM
[rh,rh2v,rv2h,rv,frh,frv]=qemdecode(qemb(2).at,qemresPert.ct,qemresPert,...
    qemresPert.resph,semresPert.respv,semresPert.resph,qemresPert.respv,...
    qemresPert.frespx,semresPert.frespz,...
    qemres.khrot,qemres.khgain,qemres.kvrot,qemres.kvgain,...
    qemres.brot,qemres.bhgain,qemres.bvgain);

% get reference RM
[rh0,rh2v0,rv2h0,rv0,frh0,frv0]=qemdecode(qemb(2).at,qemres.ct,qemres,...
    qemres.resph,semres.respv,semres.resph,qemres.respv,...
    qemres.frespx,semres.frespz,...
    qemres.khrot,qemres.khgain,qemres.kvrot,qemres.kvgain,...
    qemres.brot,qemres.bhgain,qemres.bvgain);

resp0=[rh0(:);rv0(:);frh0;qemres.fractunes'];
resp=[rh(:);rv(:);qemres.bhscale*frh;qemresPert.fractunes'];
resps0=[rh2v0(:);rv2h0(:);frv0];
resps=[rh2v(:);rv2h(:);semres.bvscale*frv];

% return fitted errors and residual of fit before and after
mode = struct;
okfit=true(size(qemres.qpidx));
if ~isfield(mode,'vnorm')

end

mode.nsets=4; 
mode.neigs=neig;
mode.tuneweight=0; 
mode.dispweight=0; 

kn = qemerrfit(nhst,nvst,resp-resp0,dresp,mode,okbpm);

%mode.vnorm=sqrt([0.77;2;0.9;1.15;1.15;0.9;2;1]);

resps(isnan(resps))=0;
resps0(isnan(resps0))=0;

ks = semerrfit(length(semres.hlist),length(semres.vlist),resps-resps0,...
    drespskew,mode);

knl = kn *Lthin;
ksl = ks *Lthin;

% display fit residual
rm_in=(resp-resp0); rm_in(end-length(qemres.bpmidx)-2:end)=[];
dresp(end-length(qemres.bpmidx)-2:end,:)=[];
disp(['residual on- diagonal blocks: ' num2str(std2(rm_in)) '->'...
    num2str(std2(rm_in - dresp*kn))])
disp(['residual off-diagonal blocks: ' num2str(std2(resps-resps0)) '->'...
    num2str(std2(resps-resps0 - drespskew*ks))])

% display results
serr = findspos(rerr,sort(errpos)); % fitted gradient are sorted in lattice order
for il=1:length(serr)
    disp(['@ ' num2str(serr(il),'%6.2f') ' m : knl= '...
        num2str(knl(il),'%9.2e') ' 1/m, ksl= '...
        num2str(ksl(il),'%9.2e') ' 1/m']);
end

end


% ancillary functions
function ok=isthinsext(elem,fieldname,fieldvalue)
if isfield(elem,fieldname) && strcmp(elem.(fieldname),fieldvalue)
    pb=find(elem.PolynomB ~= 0,1);
    ok=~isempty(pb) && (pb == 3);
else
    ok=false;
end
end

function elem=setk(elem,dk)
strength=elem.PolynomB(2)+dk;
elem.K=strength;
elem.PolynomB(2)=strength;
end

