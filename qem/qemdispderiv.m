function [rx,rz]=qemdispderiv(mach,dct,elemfunc,dval,varidx,bpmidx,dispfunc)
%QEMDISPDERIV	compute derivatives of the frequency response
%
%[RX,RZ]=QEMDISPDERIV(MACH,ELEMFUNC,DVAL,VARIDX,BPMIDX,DISPFUNC)
%
%MACH:      AT machine structure
%ELEMFUNC:  function NEWELEM=ELEMFUNC(OLDELEM,DVAL) which modifies each
%           varying element
%DVAL:      Parameter increment to compute the derivative
%VARIDX:	Index of varying elements
%BPMIDX:	Index of BPMs
%DISPFUNC:  Function to display the progress: DISPFUNC(I,IMAX)
%
%RX:        derivative of the horizontal response to frequency
%RZ:        derivative of the vertical response to frequency

if nargin < 7
    fprintf('          ');
    dispfunc=@(i,itot) fprintf('\b\b\b\b\b\b\b\b\b%4d/%4d',[i itot]);
end
nb=length(bpmidx);
nq=length(varidx);
rx=zeros(nb,nq);
rz=zeros(nb,nq);
for ib=1:nq
    iq=varidx(ib);
    qsave=mach{iq};
    mach{iq}=elemfunc(qsave,0.5*dval);
    [oxp,ozp]=qemfresp(mach,dct,bpmidx);
    mach{iq}=elemfunc(qsave,-0.5*dval);
    [oxm,ozm]=qemfresp(mach,dct,bpmidx);
    rx(:,ib)=(oxp-oxm)/dval;
    rz(:,ib)=(ozp-ozm)/dval;
    mach{iq}=qsave;
    dispfunc(ib,nq);
end
