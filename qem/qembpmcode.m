function [rhbpm,rvbpm] = qembpmcode(rh0,rv0,brot,bhgain,bvgain)
%QEMBPMCODE     Apply BPM gain and rotation to response matrices
%[RHBPM,RVBPM]=QEMBPMCODE(RH0,RV0,ROTATION,HGAIN,VGAIN)

cb=cos(brot);
sb=sin(brot);
rhbpm= rh0.*cb./bhgain + rv0.*sb./bhgain;
rvbpm=-rh0.*sb./bvgain + rv0.*cb./bvgain;
end
