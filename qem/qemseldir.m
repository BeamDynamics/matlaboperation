function dirname=qemseldir(dirname)
%dirname=qemseldir(dirname)     Select and prepares data directory for QEM
%
%qemseldir checks the presence of the "optics" link and "magcor.dat"
%corrector data and prompts for values if necessary
%
% dirname:  data directory

global APPHOME MACHFS

measbase=fullfile(MACHFS,'MDT');
if nargin < 1
    dirname=uigetdir(fullfile(measbase,datestr(now(),'yyyy'),''),'Select the data directory:');
    if ~ischar(dirname), error('qem:nodir','No directory selected'); end
else
    if ~isdir(dirname), error('qem:nodir','Wrong directory'); end
end

% check files in datadirectory are correct
listing = dir(dirname);

if length({listing.name})>3 % not an empty directory
    needed_file_names ={....
        'quadcor.dat',...
        'skewcor.dat',...
        'sextcor.dat',...
        'steerers.dat',...
        'octcor.dat',...
        };
    
    for iiff = 1:length(needed_file_names)
        
        files_present(iiff) = ismember(needed_file_names{iiff},{listing.name});
        
    end
    
    if any(~files_present)
        
        disp('following files missing: ');
        disp(needed_file_names(~files_present))
        
        disp(['Nothing done. please chose a different directory usually */resp***']);
        error([dirname ' is not a qempanel response matrix data folder'])
    end
    
    
end

%% chose optics
if exist(fullfile(dirname,'optics'),'file') == 0        % Choose optics
    opticsbase=fullfile(APPHOME,'optics','sr','');
    d=dir(opticsbase);
    ddir=find(cat(1,d.isdir));
    opticslist={d(ddir(3:end)).name};
    [selection,ok]=listdlg('ListString',opticslist,'SelectionMode','single');
    if ~ok, error('qem:nooptics','No optics selected'); end
    pth=getrealpath(fullfile(opticsbase,opticslist{selection}));
    [status,result]=system(['cd ' dirname ' && ln -s ' pth ' optics']);
    if status ~= 0, error('qem:wrongoptics',result); end
end


%% save data files to be used also offline out of CTRM

globalfilename=['Settings' datestr(now,'yyyymmmdd_HHMMSS') ];

familyfilename=fullfile(dirname,'familymag.dat');
if exist(familyfilename,'file') == 0    % Select magnet file
    % % generate, copy, erase setting
    famsave=tango.Device('sys/settings/m-strengths');famsave.Timeout=10000;
    famsave.GenerateSettingsFile({['FILE:' globalfilename ''],'AUTHOR: qempanel','COMMENTS: '});
    copyfile(fullfile(famsave.SettingsPath.read,[globalfilename '.ts']),familyfilename);
    famsave.DeleteFile([globalfilename '.ts']);
    
    % save latest applied file
    % copyfile(fullfile(famsave.SettingsPath.read,[famsave.LastAppliedFile.read '.ts']),familyfilename);
    
end

% save correction strenghts
quadfilename=fullfile(dirname,'quadcor.dat');
if exist(quadfilename,'file') == 0    % Select magnet file
    quasave=tango.Device('sys/settings/m-quad');quasave.Timeout=10000;
    quasave.GenerateSettingsFile({['FILE:' globalfilename ''],'AUTHOR: qempanel','COMMENTS: '});
    copyfile(fullfile(quasave.SettingsPath.read,[globalfilename '.ts']),quadfilename);
    quasave.DeleteFile([globalfilename '.ts']);
    % copyfile(fullfile(quasave.SettingsPath.read,[quasave.LastAppliedFile.read '.ts']),quadfilename);
end

skewfilename=fullfile(dirname,'skewcor.dat');
if exist(skewfilename,'file') == 0    % Select magnet file
    skwsave=tango.Device('sys/settings/m-skew');skwsave.Timeout=10000;
    skwsave.GenerateSettingsFile({['FILE:' globalfilename ''],'AUTHOR: qempanel','COMMENTS: '});
    copyfile(fullfile(skwsave.SettingsPath.read,[globalfilename '.ts']),skewfilename);
    skwsave.DeleteFile([globalfilename '.ts']);
    % copyfile(fullfile(skwsave.SettingsPath.read,[skwsave.LastAppliedFile.read '.ts']),skewfilename);
    
end

sextfilename=fullfile(dirname,'sextcor.dat');
if exist(sextfilename,'file') == 0    % Select magnet file
    sxtsave=tango.Device('sys/settings/m-sext');sxtsave.Timeout=10000;
    sxtsave.GenerateSettingsFile({['FILE:' globalfilename ''],'AUTHOR: qempanel','COMMENTS: '});
    copyfile(fullfile(sxtsave.SettingsPath.read,[globalfilename '.ts']),sextfilename);
    sxtsave.DeleteFile([globalfilename '.ts']);
    % copyfile(fullfile(sxtsave.SettingsPath.read,[sxtsave.LastAppliedFile.read '.ts']),sextfilename);
end

octfilename=fullfile(dirname,'octcor.dat');
if exist(octfilename,'file') == 0    % Select magnet file
    octsave=tango.Device('sys/settings/m-octu');octsave.Timeout=10000;
    octsave.GenerateSettingsFile({['FILE:' globalfilename ''],'AUTHOR: qempanel','COMMENTS: '});
    copyfile(fullfile(octsave.SettingsPath.read,[globalfilename '.ts']),octfilename);
    octsave.DeleteFile([globalfilename '.ts']);
    % copyfile(fullfile(octsave.SettingsPath.read,[octsave.LastAppliedFile.read '.ts']),octfilename);
    
end

steerfilename=fullfile(dirname,'steerers.dat');
if exist(steerfilename,'file') == 0    % Select magnet file
    try
        strsave=tango.Device('sys/settings/ebsco');strsave.Timeout=10000;
    catch
        strsave=tango.Device('sys/settings/m-steerer');strsave.Timeout=10000;
    end
    strsave.GenerateSettingsFile({['FILE:' globalfilename ''],'AUTHOR: qempanel','COMMENTS: '});
    copyfile(fullfile(strsave.SettingsPath.read,[globalfilename '.ts']),steerfilename);
    strsave.DeleteFile([globalfilename '.ts']);
    
    % copyfile(fullfile(strsave.SettingsPath.read,[strsave.LastAppliedFile.read '.ts']),steerfilename);
    
end

steerstatefilename=fullfile(dirname,'SteerersStates.mat');
if exist(steerstatefilename,'file') == 0
    %hsteer_enab = true(size(indhcor));
    %vsteer_enab = true(size(indvcor));
    
    try % if tango not available, no check
        hsteer_enab = strcmp(tango.Attribute('srmag/hst/all/CorrectorStates').value,'On'); %if not 'On' = do not use.
        vsteer_enab = strcmp(tango.Attribute('srmag/vst/all/CorrectorStates').value,'On'); %if not 'On' = do not use.
        
    catch err
        hsteer_enab = [];
        vsteer_enab = [];
        disp(err)
        disp('probably tango is not available');
    end
    
    save(steerstatefilename,'hsteer_enab','vsteer_enab');
    
    disp('Steerers state file saved.');
end



    function rlpth=getrealpath(pth)
        here=pwd;
        cd(pth);
        loc=pwd;
        beg=strfind(loc,'machfs');
        if ~isempty(beg)
            rlpth = loc([1 beg:end]);
        else
            rlpth = pth;
        end
        cd(here);
    end

end
