function [rv2h,rh2v] = qemdeblur(rh0,rv0,rv2h0,rh2v0,khrot,kvrot,brot)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

ckh=cos(khrot);
skh=sin(khrot);
ckv=cos(kvrot);
skv=sin(kvrot);
cb=cos(brot);
sb=sin(brot);
rh2v=rh2v0+rh0.*(sb*ckh)-rv0.*(cb*skh);
rv2h=rv2h0+rh0.*(cb*skv)-rv0.*(sb*ckv);
end
