function mach=qemat(qemres,qemb,coupling)
%SEMBUILDAT		Updates the AT structure with strengths, tilts
%
%MACH=QEMAT(QEMRES,QEMB,COUPLING)
%
%QEMRES:	fields bpmidx,qpidx,dipidx,qcoridx,skewidx,qcorl,skewl
%QEMB:      fields kn,cor,dipdelta,ks,skewcor,diptilt
%COUPLING:  flag enabling voupled machine
%
%MACH : Resulting AT structure
%
if nargin < 3, coupling=true; end
if coupling
    %     mach=sembuildat(qemres.at(:),qemres,qemb.kn,qemb.cor(:,end),...
    %         qemb.dipdelta,qemb.ks,qemb.skewcor(:,end),qemb.diptilt);
    %[k,tilts]=semtilt(qemb.kn,qemb.ks);
    mach=atsetshift(qemres.at(:),qemres.sextidx,qemb.dxs,qemb.dzs);
    mach=atsettilt(mach,qemres.dipidx,qemb.diptilt); % dipole rotations
    
    %mach=atsettilt(mach,qemres.qpidx,tilts); % quadrupole tilts
    bend=atgetfieldvalues(mach,qemres.dipidx,'BendingAngle');
    mach=atsetfieldvalues(mach,qemres.dipidx,'BendingAngle',(1+qemb.dipdelta).*bend);
    %mach=setcellstruct(mach,'K',qemres.qpidx,k);
    mach=atsetfieldvalues(mach,qemres.qpidx,'PolynomB',{1,2},qemb.kn);
    mach=atsetfieldvalues(mach,qemres.qpidx,'PolynomA',{1,2},qemb.ks);
    
    % set quadrupole correctors
    if strcmp(qemres.relative_or_absolute_fit,'absolute')
        % if absolute, correctors values are set in the lattice. For large
        % errors this migth lead to unstable optics.
        corval = qemb.cor(:,end);
        skewcorval = qemb.skewcor(:,end);
    elseif  strcmp(qemres.relative_or_absolute_fit,'relative') % if relative
        
        if size(qemb.cor,2) == 1 % no correction computed yet.
            % initial correctors are zero
            corval = zeros(size(qemb.cor(:,end)));
        else % there is a computed correction (absolute values always)
            % set delta correction in the lattice optics
            corval = qemb.cor(:,end) - qemb.cor(:,1);
        end
    
        if size(qemb.skewcor,2) ==1% no correction computed yet.
            % initial correctors are zero
            skewcorval = zeros(size(qemb.skewcor(:,end)));
        else % there is a computed correction (absolute values always)
            % set delta correction in the lattice optics
            skewcorval = qemb.skewcor(:,end) - qemb.skewcor(:,1);
        end
    end
    
    ks0=atgetfieldvalues(mach,qemres.skewidx,'PolynomA',{1,2}); % quadrupole roations on skew fit indexes
    mach=atsetfieldvalues(mach,qemres.skewidx,'PolynomA',{1,2},ks0+skewcorval./qemres.skewl);
    
    k0=atgetfieldvalues(mach,qemres.qcoridx,'PolynomB',{1,2})';
    mach=atsetfieldvalues(mach,qemres.qcoridx,'PolynomB',{1,2},(k0 + (corval./qemres.qcorl)')');
    
else
    
    %     mach=sembuildat(qemres.at(:),qemres,qemb.kn,qemb.cor(:,end),...
    %         qemb.dipdelta);
    k=qemb.kn;
    mach=atsetshift(qemres.at(:),qemres.sextidx,qemb.dxs,0);
    bend=getcellstruct(mach,'BendingAngle',qemres.dipidx);
    mach=setcellstruct(mach,'BendingAngle',qemres.dipidx,(1+qemb.dipdelta).*bend);
    %mach=setcellstruct(mach,'K',qemres.qpidx,k);
    mach=setcellstruct(mach,'PolynomB',qemres.qpidx,k,2);
    
    if strcmp(qemres.relative_or_absolute_fit,'absolute')
        % if absolute, correctors values are set in the lattice. For large
        % errors this migth lead to unstable optics.
        corval = qemb.cor(:,end);
        
    elseif  strcmp(qemres.relative_or_absolute_fit,'relative') % if relative
        
        corval = qemb.cor(:,end) - qemb.cor(:,1);
        
    end
    
    k0=atgetfieldvalues(mach,qemres.qcoridx,'PolynomB',{1,2})';
    mach=atsetfieldvalues(mach,qemres.qcoridx,'PolynomB',{1,2},(k0 + (corval./qemres.qcorl)')');
    
end
