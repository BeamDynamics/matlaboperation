function [qemb,qemr,semr] = qembatch(dirname,varargin)
%QEMBATCH	Run unattended processing of response matrices
%
%[QEMB,QEMRES,SEMRES]=QEMBATCH(DIRNAME,SEQUENCE)
%
%	DIRNAME:    Response matrix directory
%	SEQUENCE:   List of (action,argument) pairs specifying the process
%
%  'fitquad',neigs      Fit normal quads with neigs Eigen vectors
%  'fitskew',neigs      Fit skew quads with neigs Eigen vectors
%  'readq',qerrfile     Load quad error file
%  'reads',serrfile     Load skew quad error file
%  'saveq',qerrfile     Save quad error file
%  'saves',serrfile     Save skew quad error file
%  'savegain',gainfile	Save fitted gain/rotation file
%
%	QEMB:               Fitted model
%	QEMRES:
%	SEMRES:
%
%[...]=QEMBATCH(...,'steerers',STEERNAMES) list of steerer names
%[...]=QEMBATCH(...,'shlist',SHLIST) list of H steerer indices (overrides %'steerers')
%[...]=QEMBATCH(...,'svlist',SVLIST) list of V steerer indices (overrides %'steerers')
%[...]=QEMBATCH(...,'outputdir',DIRNAME) directory where to save files (default: dirname)
%[...]=QEMBATCH(...,'analytic',TRUEFALSE) choose method (default: analytic)
%[...]=QEMBATCH(...,'wrongbpms',BPMLIST) list of wrong bpms (default: from qeminit)
%
%Example:
%>> [qb,qemr,semr]=qembatch('/machfs/MDT/2020/2020_02_29/resp2',...
%     'outputdir','/machfs/MDT/results','steerers',{'sf2a','sd1d'},...
%     'fitquad',96,'saveq','quaderrs96.mat',...
%     'fitskew',96,'saves','skewerrs96.mat',...
%     'savegain','gainfit96.mat');
%>> res=qem_residual(qb(2),qemr,semr);


[analytic,vargs]=getoption(varargin,'analytic',true);
[wrongbpms,vargs]=getoption(vargs,'wrongbpms',nan);
[shlist,vargs]=getoption(vargs,'shlist',nan);
[svlist,vargs]=getoption(vargs,'svlist',nan);
[steerers,vargs]=getoption(vargs,'steerers',nan);
[verbose,vargs]=getflag(vargs,'verbose');

setargs={};
if isstring(steerers) || ischar(steerers) || iscellstr(steerers)
    setargs=[setargs,{'steerers',steerers}];
end
if all(isfinite(shlist)), setargs=[setargs,{'shlist',shlist}]; end
if all(isfinite(svlist)), setargs=[setargs,{'svlist',svlist}]; end
if all(isfinite(wrongbpms)), setargs=[setargs,{'wrongbpms',wrongbpms}]; end

if analytic
    ffitq=@qempanelfitqAnalytic;
    ffits=@sempanelfitqAnalytic;
    ffitd=@qemfitdipoleAnalytic;
    ffitdt=@semfitdipoleAnalytic;
else
    ffitq=@qempanelfitq;
    ffits=@sempanelfitq;
    ffitd=@qemfitdipole;
    ffitdt=@semfitdipole;
end

funlist.fitquad=@fitquad;
funlist.fitskew=@fitskew;
funlist.fitdangle=@fitdangle;
funlist.fitdtilt=@fitdtilt;
funlist.readq=@readq;
funlist.reads=@reads;
funlist.saveq=@saveq;
funlist.saves=@saves;
funlist.savegain=@savegain;

[qemr,semr,qemb]=qempanelset(dirname,setargs{:});
qemb(2).at=qemat(qemr,qemb(2));
qemr.hdispWeigth=0.1;
qemr.tuneWeigth=1000000;
qemr.ndiv=1;
semr.wdisp=0.01;
[options.outputdir,vargs]=getoption(vargs,'outputdir',qemr.datadir);
okbpm=true(length(qemr.bpmidx),1);
okbpm(qemr.wrongbpms)=false;
fprintf('\n------------------ end quad panelset ---------------------\n\n');

if true
    [qemr.resph,qemr.respv,qemr.frespx,semr.frespz]=qemcheckresp(qemr,semr,qemr.wrongbpms);
    %    qemr.respiax=load_sriaxresp(qemr.datadir,qemr.opticsdir,'v',qemr.vlist);
    [semr.resph,semr.respv]=semloadresp(qemr,semr,qemr.wrongbpms);
    %    semres.respiax=load_sriaxresp(qemr.datadir,qemr.opticsdir,'h2v',semres.hlis    semres.iaxemz=semloadiax(qemr.datadir);
end
qemr=qemloadderivs(qemr);
fprintf('\n------------------ end load responses ---------------------\n\n');

for action=reshape(vargs,2,[])
    [act,argument]=deal(action{:});
    funlist.(act)(argument);
end

if verbose
    qemb(2)=qempaneldisp(qemr,semr,qemb(2),qemb(1),struct('compare',true));
    qemb(2)=sempaneldisp(qemr,semr,qemb(2),qemb(1),struct('compare',true));
else
    qemb(2)=qemoptics(qemr,qemb(2),qemb(1));
end

    function fitquad(neigs)
        qemr.neigQuadFit=neigs;
        %qemb(2)=qemoptics(qemr,qemb(2),qemb(1));  % To get qemb(2).fractunes
        [qemb(2).kn,qemr]=ffitq(qemb(2),qemr,semr,qemr.qpfit,okbpm);
        qemb(2).at=qemat(qemr,qemb(2));
        fprintf('\n-------------- end fit normal quads -----------------\n\n');
    end

    function fitdangle (neigs)
        qemr.neigDipFit=neigs;
        %qemb(2)=qemoptics(qemr,qemb(2),qemb(1));  % To get qemb(2).fractunes
        dipdelta = ffitd(qemb(2).at,qemr,semres,[],okbpm);
        qemb(2).dipdelta=qemb(2).dipdelta+dipdelta-mean(dipdelta);
        qemb(2).at=qemat(qemr,qemb(2));
        fprintf('\n-------------- end fit dipole angles ----------------\n\n');
    end

    function fitskew(neigs)
        semr.neigQuadFit=neigs;
        qemb(2).ks=ffits(qemb(2),qemr,semr,qemr.qpfit,okbpm);
        qemb(2).at=qemat(qemr,qemb(2)); % update at model
        fprintf('\n---------------- end fit skew quads------------------\n\n');
    end

    function fitdtilt(neigs)
        semr.neigDipFit=neigs;
        qemb(2).diptilt=qemb(2).diptilt+ffitdt(qemb(2).at,qemr,semres);
        fprintf('\n\n--------------- end fit dipoles tilts----------------\n\n');
    end

    function readq(fname)
        if isempty(fname)
            fname=fullfile(qemr.datadir,'quaderrors.mat');
        end
        [qemb(2).kn,qemb(2).dipdelta,qemb(2).dxs]=qemerrload(qemr,fname);
        qemb(2).at=qemat(qemr,qemb(2));
        fprintf('\n---------------- end load quad errors----------------\n\n');
    end

    function reads(fname)
        if isempty(fname)
            fname=fullfile(qemr.datadir,'skewerrors.mat');
        end
        smat=load(fname);
        qemb(2).ks=smat.ks;
        qemb(2).diptilt=smat.diprot;
        qemb(2).dzs=smat.dzs;
        qemb(2).at=qemat(qemr,qemb(2));
        fprintf('\n---------------- end load skew errors----------------\n\n');
    end

    function saveq(fname)
        if isempty(fname), fname='quaderrors.mat'; end
        qmat=struct('k',qemb(2).kn,'dipdelta',qemb(2).dipdelta,'dxs',qemb(2).dxs);
        save(fullfile(options.outputdir,fname),'-struct','qmat');
        fprintf('\n--------------- end save normal quad errors: %s------------\n\n',fullfile(options.outputdir,fname));
    end

    function saves(fname)
        if isempty(fname), fname='skewerrors.mat'; end
        smat=struct('kn',qemb(2).kn,'ks',qemb(2).ks,'diprot',qemb(2).diptilt,'dzs',qemb(2).dzs);
        save(fullfile(options.outputdir,fname),'-struct','smat');
        fprintf('\n--------------- end save skew quad errors: %s------------\n\n',fullfile(options.outputdir,fname));
    end

    function savegain(fname)
        if isempty(fname), fname='gainfit.mat';  end
        savebhscale=qemr.bhscale;
        qemr.bhscale=1;
        w=struct('bhscale',qembhscale(qemr,qemb(2).at));
        [w.khrot,w.khgain,w.kvrot,w.kvgain,w.bhrot,w.bhgain,w.bvrot,w.bvgain]=...
            qemguess(qemb(2).at,qemr,...
            qemr.resph,semr.respv,semr.resph,qemr.respv);
        qemr.bhscale=savebhscale;
        save(fullfile(options.outputdir,fname),'-struct','w');
        fprintf('\n--------------- end save gains: %s------------\n\n',fullfile(options.outputdir,fname));
    end
end
