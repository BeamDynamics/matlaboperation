function [newkn,qemres]=qempanelfitqAnalytic(qemb,qemres,semres,okfit,okbpm,varargin)
%QEMPANELFITQ Vary quadrupole strengths to fit the measured response matrix
%KN=QEMPANELFITQ(QEMB,QEMRES,SEMRES,OKVAR,OKBPM,DISPFUNC)
%
%QEMB:
%QEMRES:    global parameters (qpidx,bpmidx,steerhidx,steervidx,hlist,vlist,
%           khrot,khgain,kvrot,kvgain,brot,bhgain,bvgain,
%           tunes,resph,respv,bhscale,frespx)
%SEMRES:    global parameters (resph,respv,frespz)
%OKVAR:     Select a subset of variable parameters (default: all)
%OKBPM:     Select a subset of valid BPMS (default:all)
%DISPFUNC:  Function to display the progress: DISPFUNC(I,IMAX)
%
%KN=QEMPANELFITQ(QEMB,QEMRES,SEMRES,OKVAR,OKBPM,MODE,DISPFUNC)
%MODE:      Structure allowing control of SVD inversion. (default:)
%
%KN:        New quadrupole strengths

global GDRESPA

disp('ANALYTIC quadrupole fit QEMPANEL')

    function elem=setk(elem,dk)
        strength=elem.PolynomB(2)+dk;
        elem.K=strength;
        elem.PolynomB(2)=strength;
    end

if nargin<5, okbpm=[]; end
if nargin<4, okfit=[]; end
narg=length(varargin);
if narg>0 && isa(varargin{narg},'function_handle')
    disparg={@(i,itot) nselect(2,varargin{narg},i,itot)};
    narg=narg-1;
else
    disparg={};
end
if narg>0 && isstruct(varargin{narg})
    mode=varargin{narg};
    narg=narg-1; %#ok<NASGU>
else
    mode=struct;
end

if isempty(okfit), okfit=true(size(qemres.qpidx)); end



shidx=qemres.steerhidx(qemres.hlist);
svidx=qemres.steervidx(qemres.vlist);
nhst=length(shidx);
nvst=length(svidx);

[~,o0]=findsyncorbit(qemb.at,qemres.ct,qemres.bpmidx);
dpp=o0(5);
[~,fractunes]=atlinopt(qemb.at,dpp,[],o0);
%dresp=qemderivAnalyticDebuggingVersion(qemb.at,dpp,@setk,0.0001,qemres.qpidx(okfit),qemres.bpmidx,shidx,svidx,disparg{:});
[dresp,qemres]=qemderivAnalytic(qemb.at,dpp,qemres,@setk,0.0001,qemres.qpidx(okfit),qemres.bpmidx,shidx,svidx,disparg{:});
%drespN=qemderiv(qemb.at,dpp,@setk,0.0001,qemres.qpidx(okfit),qemres.bpmidx,shidx,svidx,disparg{:});
GDRESPA=dresp;
% drespA = dresp;

% % % lines for tests 
% drespA=qemderivAnalytic(qemb.at,dpp,@setk,0.0001,qemres.qpidx(okfit),qemres.bpmidx,shidx,svidx,disparg{:});
% drespN=        qemderiv(qemb.at,dpp,@setk,0.0001,qemres.qpidx(okfit),qemres.bpmidx,shidx,svidx,disparg{:});
% save('dispderivativecompare','drespA','drespN');
% figure; plot(drespA(:));hold on; plot(drespN(:)); plot(drespA(:)-drespN(:)); xlabel('RM derivative element'); ylabel('RM derivative [m/rad/K_{quad}]'); legend('analytic','qempanel','difference')

% measured RM
[rh,rh2v,rv2h,rv,frh,frv]=qemdecode(qemb.at,qemres.ct,qemres,...
    qemres.resph,semres.respv,semres.resph,qemres.respv,...
    qemres.frespx,semres.frespz,...
    qemres.khrot,qemres.khgain,qemres.kvrot,qemres.kvgain,...
    qemres.brot,qemres.bhgain,qemres.bvgain); %#ok<ASGLU>

% model RM with fitted errors and correctors in m/rad m/Hz
[rh0,rh2v0,rv2h0,rv0,frh0,frv0]=qemcode(qemb.at,qemres.ct,...
    qemres.steerhidx(qemres.hlist),qemres.steervidx(qemres.vlist),...
    qemres.bpmidx); %#ok<ASGLU>
resp0=[rh0(:);rv0(:);frh0;fractunes'];
resp=[rh(:);rv(:);qemres.bhscale*frh;qemres.fractunes'];

if ~isfield(mode,'vnorm')
    % vv=sqrt([0.2;1.1;1.95;2;2;1.05;0.64;0.9]);
    % vv=sr.unfold(repmat(vv,1,32));
    % mode.vnorm=vv(okfit)';
end
mode.neigs=qemres.neigQuadFit;
mode.dispweight=qemres.hdispWeigth;
mode.tuneweight=qemres.tuneWeigth;
mode.nsets = qemres.ndiv;

newkn=qemb.kn;
dkerr=0*qemb.kn;

[dk,dk_err] = qemerrfit(nhst,nvst,resp-resp0,dresp,mode,okbpm,qemres.ploteig);
dkerr(okfit)=dk_err;
newkn(okfit)=qemb.kn(okfit)+dk;
qemres.kn_err=dkerr;

% flkn = qemb.kn;
% tic;
% mdl = fitlm(dresp,resp-resp0,'RobustOpt','on','Intercept', false);
% toc
% 
% flkn(okfit)=qemb.kn(okfit)+mdl.Coefficients.Estimate(1:end);
% figure(111);
% plot(qemb.kn(okfit));hold on;
% plot(newkn(okfit));hold on;
% errorbar(flkn(okfit)+mdl.Coefficients.Estimate(1:end),mdl.Coefficients.SE(1:end));
% hold on;
% errorbar(mdl.Coefficients.Estimate(1:end),mdl.Coefficients.SE(1:end));
% plot(newkn(okfit)-qemb.kn(okfit));hold on;
% ax = gca; 
% ax.XTick=1:length(qemres.qpidx(okfit));
% ax.XTickLabel = cellfun(@(a)a.Device,qemb.at(qemres.qpidx(okfit)),'un',0);
% ax.XTickLabelRotation = 90;
% legend('Initial','SVD fit','fitlm Fit','Difference fitlm-initial','Difference SVD-initial');
% grid on;
% figure(222); plot([newkn,flkn]-qemb.kn);
% legend('svd','robust')
% ax = gca; 
% ax.XTick=1:length(qemres.qpidx);
% ax.XTickLabel = cellfun(@(a)a.Device,qemb.at(qemres.qpidx),'un',0);
% ax.XTickLabelRotation = 90;
% newkn = flkn ;

end
