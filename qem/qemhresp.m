function [rh,rh2v] = qemhresp(mach,bidx,sidx,orbit0,kick,gain,tilt)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here


if nargin < 7, tilt=zeros(size(sidx)); end
if nargin < 6, gain=ones(size(sidx)); end
if nargin < 5, kick=[]; end
if nargin < 4, orbit0=[]; end
ck=gain.*cos(tilt);
sk=gain.*sin(tilt);
sethsteerer=@(elem,ib,kick) qemsetsteerer(elem,kick*ck(ib),kick*sk(ib));
[rh,rh2v]=qemresp(mach,0,bidx,sidx,sethsteerer,orbit0,kick);
end
