function [resp] = qemdispresp(qemres)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% idx=qemres.dipidx;
% bpmdata=atlinopt(qemres.at(:),qemres.dpp,qemres.bpmidx);
% bpmbeta=cat(1,bpmdata.beta);
% bpmmu=cat(1,bpmdata.mu);
% [dipdata,dipbeta,dipmu,dipdisp,tunes,chrom]=atavedata(qemres.at(:),0,idx);
% scale=2*pi*tunes(1);
% angle=getcellstruct(qemres.at(:),'BendingAngle',qemres.dipidx)';
% nbpm=size(bpmbeta,1);
% resp=responsem([bpmbeta(:,1) bpmmu(:,1)/scale],[dipbeta(:,1) dipmu(:,1)/scale],tunes(1))./angle(ones(nbpm,1),:);

[lind,nu,xsi]=atlinopt(qemres.at(:),qemres.dpp,qemres.bpmidx);
d0=cat(2,lind.Dispersion)';
for dip=1:length(qemres.dipidx)
    idx=qemres.dipidx(dip);
    mach=setcellstruct(qemres.at(:),'BendingAngle',idx,1.001*qemres.at{idx}.BendingAngle);
    [lind,nu,xsi]=atlinopt(mach,qemres.dpp,qemres.bpmidx);
    d1=cat(2,lind.Dispersion)';
    resp(:,dip)=(d1(:,1)-d0(:,1))/0.001;
end
end
