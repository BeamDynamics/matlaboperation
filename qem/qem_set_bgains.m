function qinit=qem_set_bgains(varargin)
%QINIT=QEM_SET_BGAINS(DIRNAME)
%       Load the default qeminit.mat file and update the BPM gains and
%       rotations. Other fields are kept unchanged.
%
%   DIRNAME:    location of the gainfit files (see below).
%               Default: $MACHFS/MDT/gainfit
%
%
%QINIT=QEM_SET_BGAINS(DIRNAME,QINIT0)
%       Update QINIT0. If QINIT0 is empty, it will be read from
%       the default qeminit.mat file.
%
%   QINIT0:     Initial values for QINIT
%
%
% DIRNAME must contain subdirectories named after the steerers ('sh1',...).
%
% gain/rotation files are all files in these directories with a
% name starting with 'ga'. A file appearing in several subdirectories will
% be read only once.
%
%Example: with the following tree structure:
%
%/machfs/MDT/gainfit
%            |-- sd1a
%            |   |-- ga_0222-1_sd1a.mat
%            |   `-- ga_0222-1_sd1a_sd1e.mat
%            |-- sd1e
%            |   |-- ga_0222-1_sd1a_sd1e.mat
%            |   `-- ga_0222-1_sd1e.mat
%            |-- sf2a
%            |   |-- ga_0201-1_sf2a.mat
%            |   `-- ga_0201-2_sf2a.mat
%            |-- sf2e
%            |   `-- ga_0203-1_sf2e.mat
%            |-- sh1
%            |   |-- ga_0221-3_sh1.mat
%            |   |-- ga_0221-3_sh1_sh2.mat
%            |   |-- ga_0221-4_sh1.mat
%            |   `-- ga_0221-4_sh1_sh2.mat
%            |-- sh2
%            |   |-- ga_0221-3_sh1_sh2.mat
%            |   |-- ga_0221-3_sh2.mat
%            |   |-- ga_0221-4_sh1_sh2.mat
%            |   `-- ga_0221-4_sh2.mat
%            `-- sh3
%                `-- ga_0121-sh3_sh3.mat
%
%>> qinit=qem_set_bgains('/machfs/MDT/gainfit');
%
% updates the BPM info of the default qeminit and prints the following:
%
% Loaded /machfs/MDT/gainfit/sd1a/ga_0222-1_sd1a.mat
% Loaded /machfs/MDT/gainfit/sd1a/ga_0222-1_sd1a_sd1e.mat (loaded only once)
% Loaded /machfs/MDT/gainfit/sd1e/ga_0222-1_sd1e.mat
% Loaded /machfs/MDT/gainfit/sf2a/ga_0201-1_sf2a.mat
% Loaded /machfs/MDT/gainfit/sf2a/ga_0201-2_sf2a.mat
% Loaded /machfs/MDT/gainfit/sf2e/ga_0203-1_sf2e.mat
% Loaded /machfs/MDT/gainfit/sh1/ga_0221-3_sh1.mat
% Loaded /machfs/MDT/gainfit/sh1/ga_0221-3_sh1_sh2.mat (loaded only once)
% Loaded /machfs/MDT/gainfit/sh1/ga_0221-4_sh1.mat
% Loaded /machfs/MDT/gainfit/sh1/ga_0221-4_sh1_sh2.mat (loaded only once)
% Loaded /machfs/MDT/gainfit/sh2/ga_0221-3_sh2.mat
% Loaded /machfs/MDT/gainfit/sh2/ga_0221-4_sh2.mat
% Loaded /machfs/MDT/gainfit/sh3/ga_0121-sh3_sh3.mat
%

global MACHFS

args={fullfile(MACHFS,'MDT','gainfit'),[]};
args(1:length(varargin))=varargin;
[base,qinit0]=deal(args{1:2});
vlist=["sh1","sd1a","sf2a","sd1b","sh2","sd1d","sf2e","sd1e","sh3"];
dirnames=string([]);
for d=dir(base)'
    if d.isdir && ismember(d.name,vlist)
        dirnames=[dirnames d.name]; %#ok<AGROW>
    end
end
if isempty(qinit0), qinit0=load(fullfile(MACHFS,'MDT','respmat','qeminit.mat')); end

qinit=qinit0;
fs=arrayfun(@(d) dir(fullfile(base,d,'ga*')),dirnames,'UniformOutput',false);
fs=cat(1,fs{:})';
[~,ia]=unique(string({fs.name}),'stable');
vv=arrayfun(@fload,fs(ia));

bhrot=cat(2,vv.bhrot);
qinit.bhrot=mean(bhrot,2);
subplot(4,1,1);
bar([qinit0.bhrot bhrot qinit.bhrot]);
title("H rotation");

bhgain=cat(2,vv.bhgain);
qinit.bhgain=mean(bhgain,2);
subplot(4,1,2);
bar([qinit0.bhgain bhgain qinit.bhgain]);
title("H gain");

bvrot=cat(2,vv.bvrot);
qinit.bvrot=mean(bvrot,2);
subplot(4,1,3);
bar([qinit0.bvrot bvrot qinit.bvrot]);
title("V rotation");

bvgain=cat(2,vv.bvgain);
qinit.bvgain=mean(bvgain,2);
subplot(4,1,4);
bar([qinit0.bvgain bvgain qinit.bvgain]);
title("V gain");

    function v=fload(d)
        fn=fullfile(d.folder,d.name);
        v=load(fn);
        fprintf('Loaded %s\n',fn);
    end

end
