function [dkk,qpdkk,dqdkk]=qcheck(qemres,k,k0,dang_ang,ax,largefig)
%QCHECK		Analyzes quadrupole strength errors
%
%DKK=QCHECK(KL,KL0)
%
dqmask=qemres.atmodel.select('dq');
quadmask=qemres.atmodel.select('qp');
errquadmask=qemres.errmask & quadmask;
errdqmask=qemres.errmask & dqmask;

[nq,nm]=size(k);
nfamd=length(dang_ang);
% labels=sr.qpname(1:17);

k0l=k0(:,ones(1,nm));
dkk=(k-k0l)./k0l;
qpdkk=NaN(1,sum(quadmask));
dqdkk=NaN(1,sum(dqmask));
qpdkk(errquadmask(quadmask))=dkk(errquadmask(qemres.errmask),end);
dqdkk(errdqmask(dqmask))=dkk(errdqmask(qemres.errmask),end);


if  largefig
    
    figure(41); hold off;
    % subplot(2,1,1);
    extfigax = gca;
    bar(extfigax,dkk); % if sextupoles are fit, useless.
    hold on;
    errorbar(dkk,qemres.kn_err)%title(ax,'Gradient errors');
    %title(ax,'Gradient errors');
    ylabel(extfigax,'\DeltaK/K');
    grid(extfigax,'on');
    set(extfigax,'Xlim',[0 nq+1],'FontSize',12);
    ax1 = gca;
    ax1.XTick=1:length(qemres.qpidx);
    ax1.XTickLabel = cellfun(@(a)a.Device,qemres.at(qemres.qpidx),'un',0);
    ax1.XTickLabelRotation = 90;

    figure(411); hold off;
    % subplot(2,1,1);
    extfigax = gca;
    bar(extfigax,k-k0l); % if sextupoles are fit, useless.
    hold on;
    errorbar(k-k0l,qemres.kn_err)%title(ax,'Gradient errors');
    ylabel(extfigax,'\DeltaK');
    grid(extfigax,'on');
    set(extfigax,'Xlim',[0 nq+1],'FontSize',12);
    ax1 = gca;
    ax1.XTick=1:length(qemres.qpidx);
    ax1.XTickLabel = cellfun(@(a)a.Device,qemres.at(qemres.qpidx),'un',0);
    ax1.XTickLabelRotation = 90;

    
    figure(42); hold off;
    % subplot(2,1,2);
    extfigax = gca;
    bar(extfigax,dang_ang);
    %title(ax,'Gradient errors');
    ylabel(extfigax,'\Delta\Theta/\Theta');
    grid(extfigax,'on');
    set(extfigax,'Xlim',[0 nfamd+1],'FontSize',12);
    
end

% Nc = length(dang_ang);
% extracor = mod(Nc,32);
% cor_per_cell_dip = squeeze(std(reshape(dang_ang((1+extracor/2):(Nc-extracor/2)),[],32),1,1));
% yyaxis(ax,'right');
% bar(ax,[cor_per_cell_dip],0.5,'EdgeColor','none');
% ylabel(ax,'\Delta\Theta/\Theta');
% grid(ax,'on');
% set(ax,'Xlim',[0 32+1],'FontSize',6);
%cla(ax,'reset');
stdquad=std(sr.fold(qpdkk),1,2,'omitnan');
stddq=std(sr.fold(dqdkk),1,2,'omitnan');
bar(ax,[stdquad(1:8);stddq]);
ylabel(ax,'\DeltaK/K');
grid(ax,'on');
ax.XTickLabel={'QF1','QD2','QD3','QF4','QF4','QD5','QF6','QF8','DQ1','DQ2'};


return
%
%
%
% %stdf=NaN*ones(nfamq,nm);
% %meanf=NaN*ones(nfamq,nm);
% %for i=1:nm
% dkkf=sr.fold(dkk);
% nfamq=length(dkkf);
% for ii=1:nfamq
%     stdf(ii,1)=std(dkkf{ii});
%     meanf(ii,1)=mean(dkkf{ii});
% end
%
% stda=std(dkk,1,1);
% fprintf('%s:\t% .3e\n','all',stda(1));
% for i=1:nfamq
%     fprintf('%s:\t% .3e\t% .3e\n',labels{i},stdf(i,1),meanf(i,1));
% end
%
% if nargin >= 3                 % displays quad. errors
%     bar(ax,stdf);
%     title(ax,'Gradient errors');
%     ylabel(ax,'\DeltaK/K');
%     grid(ax,'on');
%     set(ax,'Xlim',[0 nfamq+1],'XTick',1:length(labels),...
%         'XTickLabel',labels,'XTickLabelRotation',45,'FontSize',8);
end
