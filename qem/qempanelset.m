function [qemres,semres,qemb]=qempanelset(dirname,varargin)
%QEMPANELSET    Initialize the processing of response matrices
%
%[QEMB,QEMRES,SEMRES]=QEMPANELSET((DIRNAME,FULL_PARTIAL)
%
%	DIRNAME:      name of the measurement directory
%	FULL_PARTIAL:	1 partial, 0 full
%
%	QEMB:       Table of structures with fields q,cor,bh,bv,eta
%	  QEMB(1): with no errors
%	  QEMB(2): with fitted errors
%	QEMRES:      Empty structure to save results
%	QEMRES:     Empty structure to save results
%
%[...]=QEMPANELSET(...,'steerers',STEERFAMILYNAME)
%   Set the list of active steerers
%
%	STEERFAMILYNAME:    'sh1,'sd1d',...
%
%[...]=QEMPANELSET(...,'shlist',SHLIST)
%   Set the list of horizontal active steerers. Overrides coded values
%   and values given by the 'steerers' keyword.
%
%	SHLIST:     indexes of horizontal active steerers (in 1:12)
%
%[...]=QEMPANELSET(...,'svlist',SVLIST)
%   Set the list of vertical active steerers. Overrides coded values
%   and values given by the 'steerers' keyword.
%
%	SVLIST:     indexes of vertical active steerers (in 1:12)

global APPHOME MACHFS

hlist=nan;
vlist=nan;

%   process keywords
[omit1st,vargs]=getflag(varargin,'omit1st');
[stf,vargs]=getoption(vargs,'steerers',nan);
if isstring(stf) || ischar(stf) || iscellstr(stf)   % steerer names
    [hlist,vlist]=sr.steerfamily(stf);
    if omit1st                                      % omit 1st one
        hlist=hlist(2:end);
        vlist=vlist(2:end);
    end
end
[hlist,vargs]=getoption(vargs,'shlist',hlist);      % override a specified steerer name
[vlist,vargs]=getoption(vargs,'svlist',vlist);      % override a specified steerer name
[wrongbpms,vargs]=getoption(vargs,'wrongbpms',NaN);
%   process arguments
args={true,true};       % default for full_partial, ComputeSimulatedRM
args(1:length(vargs))=vargs;
[full_partial, ComputeSimulatedRM]=deal(args{:});

localinit=fullfile(dirname,'qeminit.mat');
try
    qemres=load(localinit);
    fprintf('Initial data loaded from %s\n',localinit);
catch
    try
        globalinit=fullfile(MACHFS,'MDT','respmat','qeminit.mat');
        qemres=load(globalinit);
        fprintf('Initial data loaded from %s\n',globalinit);
        [success,message,messageid]=copyfile(globalinit,localinit,'f');
        if ~success
            warning(messageid,message);
        end
    catch
        qemres=struct();
    end
end
if ~isnan(wrongbpms)
    qemres.wrongbpms=wrongbpms;
end

qemres.ploteig = false; % plot eigenvectors
qemres.execute_gain_fit = false; % fit gain and scale errors after saving

qemres.relative_or_absolute_fit = 'absolute' ; 

if ~isfield(qemres,'bhscale'), qemres.bhscale=1; end
qemres.fithist = []; % history of fit
qemres.datadir=dirname;
[~,theodir]=system(['readlink ' fullfile(dirname,'optics','')]);
[~,opticsbase]=fileparts(theodir(1:end-1));
qemres.opticsdir=fullfile(APPHOME,'optics','sr',opticsbase);
qemres.opticsname = theodir(1:end-1);

if isempty(theodir) % in case optics folder is not a link assume it is a real folder
    disp('no link found, usning local optics copy');
    theodir = fullfile(dirname,'optics','');
    qemres.opticsdir = theodir;
    qemres.opticsname = theodir;
end

disp('RM directory:')
disp(qemres.datadir);
disp('reference optics directory:')
disp(qemres.opticsname);

try
    simu = tango.Device('sys/ringsimulator/ebs');
    qemres.simufile=simu.get_property('RingFile');
catch
    disp('tango ''sys/ringsimulator/ebs'' not available')
    qemres.simufile ='None';
end

a=load(fullfile(qemres.opticsdir,'betamodel.mat'),'betamodel');
ring=atradoff(a.betamodel,'IdentityPass','auto','auto');
% warning('setting inj bump')
% warning('setting inj bump')
% warning('setting inj bump')
% warning('setting inj bump')
% a = load('/machfs/MDT/2019/2019_12_16/str.mat');
% indVst = find(atgetcells(ring,'FamName','S[HDFIJ]\w*'))';
%
% ring = atsetfieldvalues(ring,indVst([1:4,end-3:end]),'KickAngle',{1,1},a.str([1:4,end-3:end])');
% warning('setting inj bump')
% warning('setting inj bump')
% warning('setting inj bump')
% warning('setting inj bump')
% warning('setting inj bump')

warning('check SB are here')

atmod=sr.model(ring,'reduce',true); % for number of DQ (96, not 131 splitted) for example
atmodpinholes=sr.model(sr.model.addpinholes(atmod.ring)); % this lattice has pinhole cameras but DL & DQ indexes are bad

indhcor = sort(atmod.get(0,'steerhdq'));
indvcor = sort(atmod.get(0,'steerv'));

if full_partial
    if isnan([hlist vlist])
        %[qemres.hlist,qemres.vlist]=sr.steerfamily('sh1','sh2');
        %[qemres.hlist,qemres.vlist]=sr.steerfamily('sh1');
        [qemres.hlist,qemres.vlist]=sr.steerfamily('sf2a','sd1e');
    else
        qemres.hlist=hlist;
        qemres.vlist=vlist;
    end
else
    qemres.hlist=1:1:length(indhcor);
    qemres.vlist=1:1:length(indvcor);
end

% get disabled correctors list and remove from RM list. the data related to
% these correctors, even if stored in the RM measurement as zeros, is
% ignored by qempanel.
% file is saved by qemseldir (as for magnets strengths )
steerer_states=load(fullfile(qemres.datadir,'SteerersStates.mat'));
if isempty(steerer_states.hsteer_enab)
    hsteer_enab = true(size(indhcor));
    vsteer_enab = true(size(indvcor));
else
    hsteer_enab = steerer_states.hsteer_enab;
    vsteer_enab = steerer_states.vsteer_enab;
end

% remove from list of steerers all those NOT 'On'
qemres.hlist = qemres.hlist(hsteer_enab(qemres.hlist));
qemres.vlist = qemres.vlist(vsteer_enab(qemres.vlist));
if isempty(qemres.hlist)
    error('no horizontal steerers available. check if SH1A Hor steerers are On.')
end
if isempty(qemres.vlist)
    error('no vertical steerers available. check if SH1A Ver. steerers are On.')
end

disp(['Removed ' num2str(length(find(hsteer_enab(qemres.hlist)==false))) ' H steerer that are not ''On'' (either alarm, disabled, fault,...)']);
disp(['Removed ' num2str(length(find(hsteer_enab(qemres.hlist)==false))) ' V steerer that are not ''On'' (either alarm, disabled, fault,...)']);

% initialize sempanel
semres=struct();
semres.hlist=qemres.hlist;
semres.vlist=qemres.vlist;
semres.iaxemz=NaN(5,1);
semres.wdisp=0;
semres.bvscale=1;
semres.fithist = []; % history of fit
semres.skewkeep=true(1,288); % to exclude skew from correction
%semres.skewkeep(1)=false;       % Do not touch kicker bump compensation
%semres.skewkeep(end)=false;     % Do not touch kicker bump compensation
semres.skewkeep=false(1,288);
%semres.skewkeep([2:9:end,3:9:end,4:9:end,6:9:end,7:9:end,8:9:end])=true(1,64*3);
semres.skewkeep=true(1,288);

try
    stgs=load(fullfile(qemres.datadir,'settings.mat'));
    qemres.tunes=stgs.tunes-fix(stgs.tunes);
    qemres.fulltunes=stgs.tunes-fix(stgs.tunes);
catch
    qemres.tunes=[0.21 0.34];
    qemres.fulltunes=[76.21 27.34];
end
% % retune the model to measured tune
% set tunes
atmod.settune(qemres.fulltunes);

mach = atmod.ring;

% store lattice in qemres model
qemres.at=mach;

% store atmodel full and reduced with sextupole and octupole strengths. 
qemres.atmodel = atmod;

try
    df=stgs.deltafrf;
catch
    df=0;
end
circ=findspos(qemres.at,length(qemres.at)+1);
[~,periods,~,harmnumber]=atenergy(qemres.at);
frf=harmnumber*PhysConstant.speed_of_light_in_vacuum.value/circ;
qemres.ct=-circ*df/(frf+df);
syncorb=findsyncorbit(qemres.at,qemres.ct);
qemres.dpp=syncorb(5,1);
[~,tunes]=atlinopt(qemres.at,qemres.dpp);
qemres.fractunes=periods*tunes-fix(periods*tunes);

% this first list is ok for dipoles, not ok for skew quadrupoles!
% qemres.dipidx=find(atgetcells(atmod.ring,'FamName','[DJ]L[12][AE]_[15]','DQ1[B]\w*','DQ2C\w*'))';%,'DQ1[BD]' atmod.get(0,'di');
dlmask=atgetcells(atmod.ring,'FamName','[DJ]L[12][ABDE]_5\w*');
dqmask=atmod.select('dq');
qemres.dipidx=find(dlmask | dqmask);

% AnalyticResponseMatrix.m must be modified to include DQ! also qemderiv*.m
% line 96 should be modified to get this working

quadmask=atmod.select('qp');
sfmask=atgetcells(atmod.ring,'FamName','S[DFIJ][12][ABDE]');
sf2mask=atgetcells(atmod.ring,'FamName','S[FIJ][2][ABDE]');
qf1mask=atgetcells(atmod.ring,'FamName','QF1[ABDEIJ]');
qf4mask=atgetcells(atmod.ring,'FamName','QF4[ABDEIJ]');
qf6mask=atgetcells(atmod.ring,'FamName','QF6[ABDEIJ]');
qf8mask=atgetcells(atmod.ring,'FamName','QF8[ABDEIJ]');
qd2mask=atgetcells(atmod.ring,'FamName','QD2[ABDEIJ]');
qd3mask=atgetcells(atmod.ring,'FamName','QD3[ABDEIJ]');
qd5mask=atgetcells(atmod.ring,'FamName','QD5[ABDEIJ]');
dq2mask=atgetcells(atmod.ring,'FamName','DQ2C');
% errmask=quadmask;                   % All QPs
% errmask=quadmask | dqmask;          % All QPs, DQs
errmask=quadmask | dqmask | sfmask;	% All QPs, DQs, SF2s
qemres.errmask = errmask;

qemres.qpidx=find(errmask)';

% % warning('only quadrupoles, no DQ!')
qemres.quadidx = find(quadmask);	% Only quadrupoles

qemres.dqidx=find(dqmask);

cormask=quadmask;
qemres.cormask = cormask;
qemres.qcoridx=find(cormask)';     % All quadrupoles
% qemres.qcorkeep=cormask(cormask);   % Keep all
qemres.qcorkeep=qd3mask(cormask) | qf4mask(cormask) | qd5mask(cormask);

qemres.qcorl=atgetfieldvalues(qemres.at(qemres.qcoridx),'Length');

% qemres.qpfit = errmask(errmask);        % All true
% qemres.qpfit = sfmask(errmask);        % Only sextupoles
% qemres.qpfit = ~(sfmask(errmask) | dqmask(errmask));	  % No DQ, no SF
% qemres.qpfit = ~(dqmask(errmask) | qf4mask(errmask));	  % No DQ, no QF4
% qemres.qpfit = ~(dqmask(errmask) | qf4mask(errmask) | quadmask(errmask));	  % only sextupoles  (192)
qemres.qpfit =  qd3mask(errmask) | qf4mask(errmask) | qd5mask(errmask);	  % only QD3 QF4 QD5  (256)
%qemres.qpfit =  qd3mask(errmask) | qf4mask(errmask) | qd5mask(errmask) | dq2mask(errmask);
% qemres.qpfit =  qf1mask(errmask) |  sfmask(errmask) | qf6mask(errmask);	  % only QF1 sext QF6  (256)
% qemres.qpfit =  qf1mask(errmask) | qf4mask(errmask) | qd3mask(errmask) | qd5mask(errmask) | qf6mask(errmask);	  % only QF1 QD3 QF4 QD5 Qf6  (288)
% qemres.qpfit = quadmask(errmask);     % All quads
% qemres.qpfit = ~qf4mask(errmask);       % No QF4

%qemres.qpfit =  sf2mask(errmask);

qemres.ql=atgetfieldvalues(qemres.at(qemres.qpidx),'Length');

qemres.sextidx=find(atmod.select('sx'));
sxl=atgetfieldvalues(qemres.at(qemres.sextidx),'Length');
sxl(~(sxl~=0))=1;   % Handles NaN correctly
qemres.sxl=sxl;

qemres.steerhidx=indhcor';
if ~isfield(qemres,'khgain'), qemres.khgain=ones(1,length(qemres.steerhidx)); end
if ~isfield(qemres,'khrot'), qemres.khrot=zeros(1,length(qemres.steerhidx)); end
qemres.hsteerl=atgetfieldvalues(qemres.at(qemres.steerhidx),'Length');

qemres.steervidx=indvcor';
if ~isfield(qemres,'kvgain'), qemres.kvgain=ones(1,length(qemres.steervidx)); end
if ~isfield(qemres,'kvrot'), qemres.kvrot=zeros(1,length(qemres.steervidx)); end
qemres.vsteerl=atgetfieldvalues(qemres.at(qemres.steervidx),'Length');



qemres.bpmidx=atmod.get(0,'bpm')';
nbpm=length(qemres.bpmidx);
if ~isfield(qemres,'bhgain'), qemres.bhgain=ones(nbpm,1); end
if ~isfield(qemres,'bvgain'), qemres.bvgain=ones(nbpm,1); end
if ~isfield(qemres,'bhrot'), qemres.bhrot=zeros(nbpm,1); end
if ~isfield(qemres,'bvrot'), qemres.bvrot=zeros(nbpm,1); end
qemres.brot=0.5*(qemres.bhrot+qemres.bvrot);


% store wrong BPMS
if ~isfield(qemres,'wrongbpms') % if this command is reached from Browse, the wrong BPMS must be those defined during the measurement and thus not changed.
    try
        bpms_dev = tango.Device('srdiag/bpm/all');
        actually_disabled_bpms = find(bpms_dev.All_Status.read~=0);
        qemres.wrongbpms = actually_disabled_bpms; % index of BPMS that are not green in libera manager.
    catch err
        disp(err);
        qemres.wrongbpms = [];
        warning('tango not available');
    end
end
disp('These BPMs are disabled: ');
disp(qemres.wrongbpms);
    
qemres.skewidx=atmod.get(0,'skew')';
qemres.skewl=atgetfieldvalues(qemres.at(qemres.skewidx),'Length');

% pinhole data from the model with pinholes
qemres.id07idx=atmodpinholes.get(0,'pinholeID07')';
qemres.id25idx=atmodpinholes.get(0,'pinholeID25')';
qemres.d09idx=atmodpinholes.get(0,'pinholeD09')';
qemres.d17idx=atmodpinholes.get(0,'pinholeD17')';
qemres.d27idx=atmodpinholes.get(0,'pinholeD27')';

qemres.qmode=struct;

if nargout >= 3
    % Fit and correctors quadrupoles are the same magnets.
    % For Fit quadrupoles we keep using absolute values.
    %                qemb.kn is K_q= K_model + DK_fit.
    % For corectors quadrupoles we move to delta.
    %                qemb.cor is DK_cor in K_q= K_model + DK_fit  + DK_cor.
    % this imply changes only in qemat.m
    
    qemb1.kn=atmod.getfieldvalue(qemres.qpidx,'PolynomB',{1,2});%atgetfieldvalues(qemres.at(qemres.qpidx),'PolynomB',{2});
    qemb1.dipdelta=zeros(length(qemres.dipidx),1);
    qemb1.dxs=zeros(length(qemres.sextidx),1);
    qemb1.cor=zeros(length(qemres.qcoridx),1); % correctors strenghts are used as DELTA in EBS.
    qemb1.skewcor=zeros(length(qemres.skewidx),1);
    qemb1.dzs=zeros(length(qemres.sextidx),1);
    qemb1.diptilt=zeros(length(qemres.dipidx),1);
    qemb1.ks=zeros(length(qemres.qpidx),1);
    qemb1.at=qemat(qemres,qemb1,false);
    
    if ComputeSimulatedRM
        qemb1=qemoptics(qemres,qemb1);
    else
        qemb1.lindata=NaN;       % BPM results
        qemb1.id07data=NaN;      % ID07 results
        qemb1.d09data=NaN;       % D09 results
        qemb1.d17data=NaN;	    % D17 results
        qemb1.id25data=NaN;      % ID25 results
        qemb1.d27data=NaN;       % D27 results
        qemb1.tunes=NaN;
        qemb1.beta=NaN;
        qemb1.eta=NaN;
        qemb1.emittances=NaN;
        qemb1.atresph=NaN;
        qemb1.atresph2v=NaN;
        qemb1.atrespv2h=NaN;
        qemb1.atrespv=NaN;
        qemb1.frespx=NaN;
        qemb1.frespz=NaN;
        
    end
    qemb(1) = qemb1;
    
    
    Nq=textscan(fopen(fullfile(qemres.datadir,'quadcor.dat'),'r'),'%s %f','HeaderLines',7);
    indq=quadmask(quadmask | dqmask);
    qemb(2).cor = Nq{2}(indq);	%select Quadrupoles
%     Sq=textscan(fopen(fullfile(qemres.datadir,'skewcor.dat'),'r'),'%s %f','HeaderLines',7);
%     qemb(2).skewcor = Sq{2};%zeros(length(qemres.skewidx),1);
    qemb(2).skewcor=qemb1.skewcor;
    try
        [kn,qemb(2).dipdelta,qemb(2).dxs]=qemerrload(qemres,fullfile(qemres.opticsdir,'quadinierrors.mat'));
    catch
        warning('qem:noinit','No initial errors in the optics directory');
        qp_and_cor = errmask & cormask;
        kmask=qp_and_cor(errmask);
        cmask=qp_and_cor(cormask);
        kn=qemb1.kn;
        if strcmp(qemres.relative_or_absolute_fit,'absolute')
            corval = qemb(2).cor./qemres.qcorl;
            kn(kmask) = kn(kmask) - corval(cmask);
        end
        qemb(2).dipdelta=qemb1.dipdelta;
        qemb(2).dxs=qemb1.dxs;
    end
    qemb(2).kn=kn;
    qemb(2).dzs=zeros(length(qemres.sextidx),1);
    qemb(2).diptilt=zeros(length(qemres.dipidx),1);
    qemb(2).ks=zeros(length(qemres.qpidx),1);
end
    
qemres.kn_err = zeros(size(qemb(2).kn)); %error of fitted parameters

qemres=orderfields(qemres);

% display directories used
disp('simulator optics file:')
disp(qemres.simufile);

end
