function qemres = qemloadderivs(qemres,varargin)

hlab={'sh1','sd1a','sf2a','sd1b','dq1b','sh2','dq2c','dq1d','sd1d','sf2e','sd1e','sh3'};

[inputdir,vargs]=getoption(varargin,'inputdir',qemres.opticsdir);

if qemres.ct == 0
    try
        ah=zeros(12,32);
        nh=length(qemres.hlist);
        nv=length(qemres.vlist);
        ah(qemres.hlist)=1:nh;
        av=zeros(9,32);
        av(qemres.vlist)=nv;
        av=[av(1:4,:);zeros(1,32);av(5,:);zeros(2,32);av(6:9,:)];
        needed=find(any([ah av],2))';
        dX_dq=nan(length(qemres.bpmidx),nh,length(qemres.qpidx));
        dY_dq=nan(length(qemres.bpmidx),nv,length(qemres.qpidx));
        shloaded=cellfun(@sload,num2cell(ah(needed,:),2)',num2cell(av(needed,:),2)',hlab(needed),'UniformOutput',false);
        qemres.quadorbitresponse=[reshape(dX_dq,[],size(dX_dq,3));reshape(dY_dq,[],size(dY_dq,3))];
        disp(join(["Orbit derivatives loaded for",string(join(shloaded,', '))]))
    catch
        warning('Orbit derivatives not available');
    end
    try
        v3=load(fullfile(inputdir,'quadhdisp.mat'));
        qemres.quadhdispresponse=v3.dDx_dq;
        disp('H dispersion derivatives loaded');
    catch
        warning('H dispersion derivatives not available');
    end
    try
        v3=load(fullfile(inputdir,'quadtunes.mat'));
        qemres.quadtunesresponse=v3.dTunes_dq;
        disp('Tunes derivatives loaded');
    catch
        warning('Tunes derivatives not available');
    end
else
    warning('Stored derivatives ignored for off-momentum measurements');
end

    function shname=sload(hdest,vdest,shname)
            vv=load(fullfile(inputdir,['quad' shname]));
            dX_dq(:,hdest,:)=vv.dX_dq(:,hdest>0,:);
            dY_dq(:,vdest,:)=vv.dY_dq(:,vdest>0,:);
    end

end
