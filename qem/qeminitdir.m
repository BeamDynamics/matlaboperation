function dirname=qeminitdir(dirname, handles)
%QEMINITDIR Initialize the measurement directory

nuh=str2double(get(handles.edit4,'String'));
nuv=str2double(get(handles.edit3,'String'));
sav.deltafrf=str2double(get(handles.edit5,'String'));
sav.tunes=[nuh nuv];
% sav.hsteerers=get_steerers(0);
% sav.vsteerers=get_steerers(1);
sav.hsteerers=get_steerval('sr/st-h/all');
sav.vsteerers=get_steerval('sr/st-v/all');
[sav.horbit,sav.vorbit]=get_orbits();
save(fullfile(dirname,'settings.mat'),'-struct','sav');

%     function setv=get_steerers(plane)
%         try
%             st=dvopensteer(plane);
%             [setv,readv]=dvreadsteer(st); %#ok<NASGU>
%             dvclose(st);
%         catch %#ok<CTCH>
%             warning('Qempanel:devaccess','Cannot access steerers');
%             setv=[];
%         end
%     end
    function setv=get_steerval(dname)
        try
            st=tango.Device(dname);
            setv=st.Current.set';
        catch
            warning('Qempanel:devaccess','Cannot access steerers');
            setv=[];
        end
    end

%     function [horb,vorb]=get_orbits()
%         try
%             bpm=dvopen('tango:SR/D-BPMLIBERA/ALL');
%             dvsetparam(bpm,'BpmAverage',3);
%             [horb,vorb]=dvreadbpm(bpm,'wait');
%             dvclose(bpm);
%         catch %#ok<CTCH>
%             warning('Qempanel:devaccess','Cannot access BPMs');
%             horb=[];
%             vorb=[];
%         end
%     end
    function [horb,vorb]=get_orbits()
        try
            bpm=tango.SrBpmDevice('sr/d-bpmlibera/all');
            [horb,vorb]=bpm.read_sa('Averaging',3);
        catch %#ok<CTCH>
            warning('Qempanel:devaccess','Cannot access BPMs');
            horb=[];
            vorb=[];
        end
    end

end
