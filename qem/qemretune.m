function qemb = qemretune(qemb,qemres,tunes)
%QEMB=QEMRETUNE(QEMB,QEMRES) Retune

% get initial gradients and rotations
[k,tilt]=semtilt(qemb.kn,qemb.ks); %#ok<ASGLU>

% fit tune in lattice model with errors
mach=atfittune(qemb.at,tunes,'QF1\w*','QD2\w*');

% get new gradients
knew=atgetfieldvalues(mach,qemres.qpidx,'PolynomB',{1,2});

if strcmp(qemres.relative_or_absolute_fit,'absolute')
% remove correctors strengths (later added by qemat) from any quadrupole that is both an error
% fitting and corrector quadrupole
    [is_main_quad,loc_main_quad] = ismember(qemres.qcoridx,qemres.qpidx);
    knew(loc_main_quad) = knew(loc_main_quad) - qemb.cor(is_main_quad,end)./qemres.qcorl;  
end

% re-apply rotations
qemb.kn=knew.*cos(2*tilt);
qemb.ks=-knew.*sin(2*tilt);
end
