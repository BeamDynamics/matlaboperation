function varargout=qemcode(mach,dct,shidx,svidx,bpmidx,varargin)
%QEMCODE	Compute the steerer response of the model
%[RH,RH2V,RV2H,RV]=QEMCODE(MACH,SHIDX,SVIDX,BPMIDX,...
%   KHROT,KHGAIN,KVROT,KVGAIN,BROT,BHGAIN,BVGAIN,...
%   DFF,HKICK,VKICK)
%
%[RH,RH2V,RV2H,RV,RFH,RFV]=QEMCODE(MACH,SHIDX,SVIDX,BPMIDX,...
%   KHROT,KHGAIN,KVROT,KVGAIN,BROT,BHGAIN,BVGAIN,...
%   DFF,HKICK,VKICK)
%
vargs=cell(1,10);
vargs(1:length(varargin))=varargin;
[khrot,khgain,kvrot,kvgain]=deal(vargs{1:4});
[brot,bhgain,bvgain]=deal(vargs{5:7});
if isempty(khrot), khrot=zeros(1,length(shidx)); end
if isempty(khgain), khgain=ones(1,length(shidx)); end
if isempty(kvrot), kvrot=zeros(1,length(svidx)); end
if isempty(kvgain), kvgain=ones(1,length(svidx)); end
if isempty(brot), brot=zeros(length(bpmidx),1); end
if isempty(bhgain), bhgain=ones(length(bpmidx),1); end
if isempty(bvgain), bvgain=ones(length(bpmidx),1); end

[rh,rh2v]=qemsteercode(mach,dct,khrot,khgain,shidx,bpmidx,vargs{9});
[rv2h,rv]=qemsteercode(mach,dct,kvrot+0.5*pi,kvgain,svidx,bpmidx,vargs{10});

[rh,rh2v]=qembpmcode(rh,rh2v,brot,bhgain,bvgain);
[rv2h,rv]=qembpmcode(rv2h,rv,brot,bhgain,bvgain);
if nargout>=5
    [frh,frv]=qemfresp(mach,dct,bpmidx,vargs{8});
    [frh,frv]=qembpmcode(frh,frv,brot,bhgain,bvgain);
    varargout={rh,rh2v,rv2h,rv,frh,frv};
else
    varargout={rh,rh2v,rv2h,rv};
end
end
