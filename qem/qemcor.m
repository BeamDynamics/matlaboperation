function cor=qemcor(qemres,q,cor0)
%QEMCOR computes quad correction (MATLAB method)
%   COR=QEMCOR(QEMRES,ERRORS,COR0)
%     Q:	quadrupole errors
%     COR0:	initial corrector strengths
%
%   see also: QEMCORFIT, QEMCORGRID, QEMCORRDT

dispgrid(q,cor0);
[nuh,nuv]=qemtunes;
f00=qemeval(cor0,nuh,nuv);
[cor,f]=fminsearch(@(x)qemeval(x,nuh,nuv),cor0,optimset(optimset(@fminsearch),'Display','iter','MaxIter',2000));
dispgrid(q,cor);
