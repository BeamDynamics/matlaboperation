function [rq]=qemtunederiv(mach,ddp,elemfunc,dval,varidx,bpmidx,dispfunc)
%QEMDISPDERIV	compute derivatives of the frequency response
%
%[RX,RZ]=QEMTUNEDERIV(MACH,ELEMFUNC,DVAL,VARIDX,BPMIDX,DISPFUNC)
%
%MACH:      AT machine structure
%ELEMFUNC:  fuqnction NEWELEM=ELEMFUNC(OLDELEM,DVAL) which modifies each
%           varying element
%DVAL:      Parameter increment to compute the derivative
%VARIDX:	Index of varying elements
%BPMIDX:	Index of BPMs
%DISPFUNC:  Function to display the progress: DISPFUNC(I,IMAX)
%
%Rq:        derivative of the tune to elemfunc

if nargin < 7
    fprintf('          ');
    dispfunc=@(i,itot) fprintf('\b\b\b\b\b\b\b\b\b%4d/%4d',[i itot]);
end
nq=length(varidx);
[~,Q0,~]=atlinopt(mach,ddp,1);
rq=zeros(2,nq);
for ib=1:nq
    iq=varidx(ib);
    qsave=mach{iq};
    mach{iq}=elemfunc(qsave,dval);
    [~,Q,~]=atlinopt(mach,ddp,1);
    rq(:,ib)=(Q-Q0)'/dval;
    mach{iq}=qsave;
    dispfunc(ib,nq);
end
