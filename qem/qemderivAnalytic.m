function [dresp,qemres]=qemderivAnalytic(mach,dpp,qemres,elemfunc,dv,varidx,bpmidx,hsidx,vsidx,dispfunc)
%QEMDERIV       Compute derivatives of response matrix
%
%[dresp,resp0]=qemderiv(mach,dpp,elemfunc,dval,varidx,bpmidx,hsteeridx,vsteeridx,dispfunc)
%
%MACH:      AT machine structure
%DPP:       Momentum deviation
%ELEMFUNC:  function NEWELEM=ELEMFUNC(OLDELEM,DVAL) which modifies each
%           varying element
%DVAL:      Parameter increment to compute the derivative
%VARIDX:	Index of varying elements
%BPMIDX:	Index of BPMs
%HSTEERIDX:	Index of horizontal steerers
%VSTEERIDX:	Index of vertical steerers
%DISPFUNC:  Function to display the progress: DISPFUNC(I,IMAX)
%
%DRESP: Derivative of response matrix for elements in fitidx:
%RESP0: Response matrix of the unperturbed mchine on 1 column:
%       [HRESPONSE(:);VRESPONSE(:);DFFRESPONSE;TUNES]

tstart=tic;
qerrmask=qemres.errmask;
qerrmask(qemres.errmask)=qemres.qpfit;      % Selected errors
qcormask=qemres.cormask;
qcormask(qemres.cormask)=qemres.qcorkeep;	% selected correctors
errcormask=qemres.errmask | qemres.cormask; % All errors and correctors
allactive=qemres.errmask| qcormask;         % All errors, selected corectors
geterrmask=qerrmask(errcormask);            % extract selected errors
nerrcor=sum(errcormask);
nbpms=length(bpmidx);

if nargin < 10, dispfunc=@defdisp; end
if nargin < 9, vsidx=hsidx; end

if ~isfield(qemres,'quadorbitresponse')
    dqmask=qemres.atmodel.select('dq');
    dipmask=dqmask;
%     dipmask=qemres.atmodel.select('di');
%     ba=atgetfieldvalues(mach(dipmask),'BendingAngle');
%     dipmask(dipmask)=abs(ba)>0;
    fitqpmask=qerrmask & ~dqmask;
    fitdqmask=qerrmask & dqmask;
    allsidxlat = sort(unique([hsidx vsidx]));
    szX=length(hsidx)*nbpms;
    szY=length(vsidx)*nbpms;
    aX=nan(szX,nerrcor);
    aY=nan(szY,nerrcor);
    disp('ANALYTIC QEMPANEL Response AT2.0/errors_corrections')
    [...
        dX_dq, dY_dq, ...
        dDx_dq, dDX_db, Dx, ...
        ~, ~, ...
        ~, ~, ...
        dDY_da...
        ]=AnalyticResponseMatrixDerivative(... compute derivative for ALL quadrupoles
        mach,dpp,...
        bpmidx',...             % bpms
        allsidxlat,...      	% steerers
        find(fitqpmask)',...	% quadrupoles
        find(dipmask)',...       % dipoles
        []);                    % skew quads
    
    % save deriv.mat dX_dq dY_dq dDx_dq dDX_db Dx dDY_da
    
    % select vertical and horizontal steerers from global vector
    qemres.quadhdisprespanal=dDx_dq;
    hst=ismember(allsidxlat,hsidx); % h steerers
    % aX=reshape(dX_dq(:,hst,:),[],size(dX_dq,3));
    % bXY=reshape(dXY_ds(:,hst,:),[],size(dXY_ds,3));
    aX(:,fitqpmask(errcormask))=reshape(dX_dq(:,hst,:),[],size(dX_dq,3));
    %bXY(:,fitskmask(errcormask))=reshape(dXY_ds(:,hst,:),[],size(dXY_ds,3));
    
    vst=ismember(allsidxlat,vsidx); % v steerers
    % aY=reshape(dY_dq(:,vst,:),[],size(dY_dq,3));
    % bYX=reshape(dYX_ds(:,vst,:),[],size(dYX_ds,3));
    aY(:,fitqpmask(errcormask))=reshape(dY_dq(:,vst,:),[],size(dY_dq,3));
    %bYX(:,fitskmask(errcormask))=reshape(dYX_ds(:,vst,:),[],size(dYX_ds,3));
    
    % Use numerical derivative for DQs
    dresp=qemderiv(mach,dpp,qemres,elemfunc,dv,find(fitdqmask)',bpmidx,hsidx,vsidx,dispfunc);
    aX(:,fitdqmask(errcormask))=dresp(1:szX,:);
    aY(:,fitdqmask(errcormask))=dresp(szX+(1:szY),:);
    
    qemres.quadorbitresponse=[aX;aY];
end

if ~isfield(qemres,'quadhdispresponse')
    disp('FIRST and only h.disp derivative with respect to quadrupoles');
    disp('computation starts on lattice without errors/correctors');
    [qemres.quadhdispresponse,~]=qemdispderiv(qemres(1).at,qemres.ct,elemfunc,1.e-3,...
        find(errcormask)',bpmidx,dispfunc);
end


% tune derivative analytic
if ~isfield(qemres,'quadtuneresponse')
    lerrcor=atgetfieldvalues(mach(errcormask),'Length');
    [~,beta,~,~,~,~]=atavedata(mach,dpp,allactive);
    qemres.quadtuneresponse=([lerrcor -lerrcor].*beta)'/4/pi;
end

dresp=[qemres.quadorbitresponse(:,geterrmask);qemres.quadhdispresponse(:,geterrmask);qemres.quadtuneresponse(:,geterrmask)];

fprintf('qemderivAnalytic:%8.3f s\n',toc(tstart));

    function defdisp(i,itot)
        if i==1, fprintf('          '); end
        fprintf('\b\b\b\b\b\b\b\b\b%4d/%4d',[i itot]);
        if i==itot, fprintf('\n'); end
    end
end
