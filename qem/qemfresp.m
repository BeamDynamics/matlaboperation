function [fresph,frespv] = qemfresp(mach,dct,bpmidx,varargin)
%QEMFRESP Computes the frequency response of the model
%[FRESPH,FRESPV]=QEMFRESP(MACH,DCT,BPMIDX,DFF)

vargs=cell(1,1);
vargs(1:length(varargin))=varargin;
dff=vargs{1};
if isempty(dff), dff=5e-7; end
circ=findspos(mach,length(mach)+1);
d2ct=-circ*dff;
orbitf=(findsyncorbit(mach,dct+d2ct/2,bpmidx)-...
    findsyncorbit(mach,dct-d2ct/2,bpmidx))/dff;
fresph=orbitf(1,:)';
frespv=orbitf(3,:)';
end
