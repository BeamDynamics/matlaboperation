function [khgain,khrot,kvgain,kvrot]=qemsteerstat(dirlist)

[vkhgain,vkhrot,vkvgain,vkvrot]=varextract(dirlist,'gainfit3.mat',...
    'khgain','khrot','kvgain','kvrot');
khgain=cat(1,vkhgain{:});
khrot=cat(1,vkhrot{:});
kvgain=cat(1,vkvgain{:});
kvrot=cat(1,vkvrot{:});
