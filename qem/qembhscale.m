function coef = qembhscale(qemres,mach)
%QEMBHSCALE Computes H bpm gain to fit the measured dispersion
%
%COEF=QEMBHSCALE(MEASURED,MODEL)

[fh,fv]=qemfresp(mach,qemres.ct,qemres.bpmidx);
[fh,~]=qembpmcode(fh,fv,qemres.brot,qemres.bhgain,qemres.bvgain);
ok=isfinite(qemres.frespx);
measok=qemres.frespx(ok);
coef=sum(measok.*fh(ok))/sum(measok.*measok);
fprintf('Bpm H scaling: %g\n',coef);
end
