function[ CorI,CorQ,first,last,length]  = Corfast_fda2( Cor,CorAmp ,mx)
%generate a signal CorQ pi/2 shifted in phase with respect to the sine signal Cor; CorI and corQ are true sine and cosine signals 
%at the frequencies used for each corrector by the script getAllSnifferXZandCorTestSigNew2. 
%ftab=[1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8,8,1,1,1,1,1,1,1,1,1];
sizeCor=size(Cor);
NSTE=sizeCor(1);
if NSTE ~= 96 
    error('in Corfast_fda2 # of steerers must be 96, because of reordering from cell 1 to cell 4 at the end of the script');
end
n=sizeCor(2);
amp=4*CorAmp;
CorQ=zeros(NSTE,n);
CorI=zeros(NSTE,n);
first=20*ones(1,NSTE);
last=zeros(1,NSTE);
length=zeros(1,NSTE);

for i=1:NSTE
    m=0;
    
    for j=5:n
        if (abs(Cor(i,j)<mx  && m==0 ))||((abs(Cor(i,j))>=mx && abs(Cor(i,j+25))<CorAmp*2 && m==0))
        first(i)=j+1;
        
       
                
            else
            first(i);
            length(i)=5071;
            m=1;
        end
    end
    %First=first(i) 
end

%for i=1:48
    %for j=1:2
        %if first(2*(i-1)+j)<74932
        %fir=first(2*(i-1)+j); 
        %ps=round(length(i)/256);
        
        %CorI(2*(i-1)+j,fir:fir+5066+j)=Cor(2*(i-1)+j,fir:fir+5066+j);
        %CorQ(2*(i-1)+j,fir+ps:fir+5066+j)=CorI(2*(i-1)+j,fir:fir+5066+j-ps);
        %CorQ(2*(i-1)+j,fir:fir+ps-1)=-CorI(2*(i-1)+j,fir+5066+j-ps+1:fir+5066+j);
        
        
        
        %else
            %CorI(2*(i-1)+j,:)=Cor(2*(i-1)+j,:);
        
       % end
    %end

%end
for i=1:NSTE
    if first(i)<n-5072
      sp=abs(fft(Cor(i,first(i):first(i)+5071)));
      [mf,k]=max(sp(1:5071));
      f=((k-1)*2);
        for j=1:5072
 %CorI(i,first(i)+j-1)=amp*sin(2*pi*j*(114+2*ftab(i))/10135) ;
 %CorQ(i,first(i)+j-1)=amp*cos(2*pi*j*(114+2*ftab(i))/10135) ;
 CorI(i,first(i)+j-1)=amp*sin(2*pi*j*(f)/10144) ;
 CorQ(i,first(i)+j-1)=amp*cos(2*pi*j*(f)/10144) ;
 
        end
    else
 CorI(i,:)=Cor(i,:);
    end
end
sizeCor=size(CorI);
Cor=zeros(NSTE,sizeCor(2));
Cor(1:87,:)=CorI(10:NSTE,:);
Cor(88:NSTE,:)=CorI(1:9,:);
CorI=Cor;
Cor=zeros(NSTE,sizeCor(2));
Cor(1:87,:)=CorQ(10:NSTE,:);
Cor(88:NSTE,:)=CorQ(1:9,:);
CorQ=Cor;



