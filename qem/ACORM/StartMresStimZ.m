function   [n_and_dateZ] =StartMresStimZ(n,Zamp,qemres)
%set_path
t1=clock
[IX, IZ, PX, PZ, N1X, N1Z ,N2X, N2Z]= savePID(qemres)
pause(.1)
setPID(0,0,0,0,0,0,0,0,2000,2000)
devaclock=dvopen ('tango:sr/d-mfdbk/aclock')
err=dvcmd(devaclock,'Stop')
devAutoCor=dvopen('tango:sr/svd-orbitcor/auto');
%err=dvcmd(devAutoCor,'Off')
devBilt=dvopen('tango:sr/bilt/all')
err=dvcmd(devBilt,'EnableAcCurrent') 
devSwitchTrig=dvopen('tango:sr/d-fofbcorrection/switch_trig/Source')
err=dvcmd(devSwitchTrig,'DevWrite',1);
devInjPDamp=dvopen('tango:sr/septa-perturb-damping/ctrl')
DampState=dvcmd(devInjPDamp,'devSTATE')
err=dvcmd(devInjPDamp,'devOFF')
namefofbcorh=['tango:sr/d-fofbcorrection/globalX'];
namefofbcorv=['tango:sr/d-fofbcorrection/globalZ'];
devfofbcorh=dvopen(namefofbcorh)
devfofbcorv=dvopen(namefofbcorv)
% Xamp or Zamp: 32000=200mA, 1000 is OK for V, 8000 is OK for H
%buffer=zeros(8,7168);

Zamp;
%pause(5)
err=dvcmd(devfofbcorh,'ResetErrors')
pause(2)

for i=1:8
%StimTest2(Xamp,Zamp,2*i-1,64+1,i);
StimTest2(0,Zamp,2*i-1,57+i,i);
end
pause(.5)
err=dvcmd(devfofbcorh,'WritePIDCoeffsToFPGA'); 
err=dvcmd(devfofbcorv,'WritePIDCoeffsToFPGA');
pause(1)
t2=clock

pause(2)
err=dvcmd(devfofbcorh,'StartCorrection');
pause(1)

 disp('Trig')

  time=clock 
 n_and_date2 = [int2str(floor(time(3)/10)),int2str(mod(floor(time(3)),10)), '/', int2str(floor(time(2)/10)),int2str(mod(floor(time(2)),10)), '/', int2str(floor(time(1))),' ' , int2str(floor(time(4)/10)),int2str(mod(floor(time(4)),10)) ,':', int2str(floor(time(5)/10)),int2str(mod(floor(time(5)),10)) ,':',int2str(floor(time(6)/10)),int2str(mod(floor(time(6)),10)),'.',int2str(floor(time(6)*1000000)-1000000*floor(time(6)))];
 n_and_dateZ={int2str(n),n_and_date2}
 err=dvcmd(devaclock,'ForcedOneShot')


clock
pause(1.5+n/10000);
err=dvcmd(devaclock,'Stop')


for j=1:8
    
StimTest2(0,0,2*j,64,j);

end


pause(1)

err=dvcmd(devfofbcorh,'ResetErrors')
pause(.1)
err=dvcmd(devSwitchTrig,'DevWrite',0);
err=dvcmd(devInjPDamp,'devON')
pause(.1)
[IX, IZ, PX, PZ, N1X, N1Z ,N2X, N2Z]= restorePID4OP


devaclock=dvopen ('tango:sr/d-mfdbk/aclock')
 err=dvcmd(devaclock,'Stop')
 save(fullfile(qemres.datadir,'lastMresV'),'n_and_dateZ','Zamp');
 time=time_label;
file=['TimeLabelZ',time];

save(fullfile(qemres.datadir,file),'n_and_dateZ','Zamp')

err=dvcmd(devfofbcorh,'StopCorrection');