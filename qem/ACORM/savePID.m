function  [IX, IZ, PX, PZ, N1X, N1Z ,N2X, N2Z]= savePID(qemres)
namefofbcorh=['tango:sr/d-fofbcorrection/globalX'];
namefofbcorv=['tango:sr/d-fofbcorrection/globalX'];
devIX=dvopen('tango:sr/d-fofbcorrection/globalX/IntegralGain');
devPX=dvopen('tango:sr/d-fofbcorrection/globalX/ProportionalGain');
devN1X=dvopen('tango:sr/d-fofbcorrection/globalX/Notch1Gain');
devN2X=dvopen('tango:sr/d-fofbcorrection/globalX/Notch2Gain');
devIZ=dvopen('tango:sr/d-fofbcorrection/globalZ/IntegralGain');
devPZ=dvopen('tango:sr/d-fofbcorrection/globalZ/ProportionalGain');
devN1Z=dvopen('tango:sr/d-fofbcorrection/globalZ/Notch1Gain');
devN2Z=dvopen('tango:sr/d-fofbcorrection/globalZ/Notch2Gain');
IX=dvcmd(devIX,'DevRead');
PX=dvcmd(devPX,'DevRead');
N1X=dvcmd(devN1X,'DevRead');
N2X=dvcmd(devN2X,'DevRead');
IZ=dvcmd(devIZ,'DevRead');
PZ=dvcmd(devPZ,'DevRead');
N1Z=dvcmd(devN1Z,'DevRead');
N2Z=dvcmd(devN2Z,'DevRead');
save(fullfile(qemres.datadir,'lastPIDset'),'IX',...
    'IZ', 'PX', 'PZ', 'N1X', 'N1Z', 'N2X', 'N2Z');

