function [XCor, ZCor] =getFDAreaderXZCor(n_and_date)
%uses the fda data extractor to get Intensity n data recorded at the date
%"date"
%Extractor argin description: (DevVarStringArray)
%[0] Number of samples to read
%[1] Start date (ex: 22/06/2015 16:15:00[.103647], les us sont optionnelles)
%for instance: n_and_date = {'100','28/09/2015 08:00:00'} or {'100','28/09/2015 08:00:00.178546'} 
n=str2double(n_and_date(1));
fda='tango:sr/d-bpmlibera/fda_reader';
devfda=dvopen(fda);
dvtimeout(devfda,50);

XZcor=dvcmd(devfda,'ReadXZCorrectionsSorted',n_and_date);

pause(5);
siz2=size(XZcor);
siz=siz2(2);
Xcor=XZcor(1:siz/2);
Zcor=XZcor(1+siz/2:siz);
XCor=reshape(Xcor,96,siz/2/96);
ZCor=reshape(Zcor,96,siz/2/96);

