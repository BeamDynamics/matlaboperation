function ReadFullRespMatData(dirrmdata, fileRMACraw, fileMeasuredGain)
% converts AC RM data from data archiver to qempanel usable format
%
% INPUT:
% fileRMACraw : name of AC RM file containing variables MresH MresV MresVH
%               MresHV
%
%
%see also:

if nargin<3
    fileMeasuredGain = 'Gain_new_180604.mat';
end

%======== AC ORM
load(fileRMACraw,'MresH','MresV','MresVH','MresHV') % load file with measured AC RM raw

ACresph=MresH; ACrespv=MresV; ACresphv=MresVH; ACrespvh=MresHV;

% calibration coefficents (empirically found during MDT)
ACresph_norm=-ACresph/2.8;  ACrespvh_norm=-ACrespvh/3.8;
ACrespv_norm=-ACrespv/3.8;  ACresphv_norm=-ACresphv/2.8;

% apply measured Gain (fine adjustment, cable lengths)
load(fileMeasuredGain,'GainX','GainZ');

% apply new calibration factors
ACresph_norm = bsxfun(@rdivide, ACresph_norm, GainX);
ACrespv_norm = bsxfun(@rdivide, ACrespv_norm, GainZ);

%%======== save for qempanel
fileformat=['%.4f\t%s,%d' repmat('\t%.5f',1,224)];
deltaH=0.08; % from respmat.py
deltaV=0.10; % from respmat.py

for ist=1:96
    f=fopen([dirrmdata '/steerH' num2str(ist,'%.2d')],'w');
    fprintf(f,fileformat,deltaH,sr.steername(ist,'SR/ST-H%i/C%i/CURRENT'),0,ACresph_norm(:,ist)*1e3);
    fclose(f);
    f=fopen([dirrmdata '/steerV' num2str(ist,'%.2d')],'w');
    fprintf(f,fileformat,deltaV,sr.steername(ist,'SR/ST-V%i/C%i/CURRENT'),0,ACrespv_norm(:,ist)*1e3);
    fclose(f);
    % off diagonal
    f=fopen([dirrmdata '/steerH2V' num2str(ist,'%.2d')],'w');
    fprintf(f,fileformat,deltaH,sr.steername(ist,'SR/ST-H%i/C%i/CURRENT'),0,ACresphv_norm(:,ist)*1e3);
    fclose(f);
    f=fopen([dirrmdata '/steerV2H' num2str(ist,'%.2d')],'w');
    fprintf(f,fileformat,deltaV,sr.steername(ist,'SR/ST-V%i/C%i/CURRENT'),0,ACrespvh_norm(:,ist)*1e3);
    fclose(f);
end





end

