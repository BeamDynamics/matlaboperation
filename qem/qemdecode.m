function varargout=qemdecode(mach,dct,qemres,varargin)
%QEMDECODE	Correct response matrices for steerer and BPM errors
%
%[RH,RH2V,RV2H,RV]=QEMDECODE(MACH,DCT,QEMRES,...
%   RHM,RH2VM,RV2HM,RVM,...
%   KHROT,KHGAIN,KVROT,KVGAIN,BROT,BHGAIN,BVGAIN)
%
%[RH,RH2V,RV2H,RV,FRH,FRV]=QEMDECODE(MACH,DCT,QEMRES,...
%   RHM,RH2VM,RV2HM,RVM,FRHM,FRVM,...
%   KHROT,KHGAIN,KVROT,KVGAIN,BROT,BHGAIN,BVGAIN)

if nargout>=5
    [rh,rh2v,rv2h,rv,frh,frv]=deal(varargin{1:6});
    [khrot,khgain,kvrot,kvgain]=deal(varargin{7:10});
    [brot,bhgain,bvgain]=deal(varargin{11:13});
    [rh,rh2v]=qembpmdecode(rh,rh2v,brot,bhgain,bvgain);
    [rv2h,rv]=qembpmdecode(rv2h,rv,brot,bhgain,bvgain);
    [frh,frv]=qembpmdecode(frh,frv,brot,bhgain,bvgain);
    [rh,rh2v,rv2h,rv]=qemsteerdecode(mach,dct,qemres,rh,rh2v,rv2h,rv,...
        khrot,khgain,kvrot,kvgain);
    varargout={rh,rh2v,rv2h,rv,frh,frv};
else
    [rh,rh2v,rv2h,rv]=deal(varargin{1:4});
    [khrot,khgain,kvrot,kvgain]=deal(varargin{5:8});
    [brot,bhgain,bvgain]=deal(varargin{9:11});
    [rh,rh2v]=qembpmdecode(rh,rh2v,brot,bhgain,bvgain);
    [rv2h,rv]=qembpmdecode(rv2h,rv,brot,bhgain,bvgain);
    [rh,rh2v,rv2h,rv]=qemsteerdecode(mach,dct,qemres,rh,rh2v,rv2h,rv,...
        khrot,khgain,kvrot,kvgain);
    varargout={rh,rh2v,rv2h,rv};
end
end
