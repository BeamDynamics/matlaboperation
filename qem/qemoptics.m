function qemb = qemoptics(qemres,qemb,qemb0)
%QEMOPTICS Compute optical parameters for the fitted machine

rph = sr.model.addpinholes(qemb.at);
bpmidx = find(atgetcells(rph,'Class','Monitor'))';
nbpm=length(bpmidx);
[idx,j,kdx]=unique([bpmidx qemres.id07idx qemres.d09idx qemres.d17idx qemres.id25idx qemres.d27idx]); %#ok<ASGLU>

orbit0=findsyncorbit(rph,qemres.ct,bpmidx);
dpp=orbit0(5,1);

[lindata,qemb.pm]=atx(rph,dpp,idx);
lindata=lindata(kdx);

qemb.lindata=lindata(1:nbpm);       % BPM results
qemb.id07data=lindata(nbpm+1);      % ID07 results
qemb.d09data=lindata(nbpm+2);       % D09 results
qemb.d17data=lindata(nbpm+3);	    % D17 results
qemb.id25data=lindata(nbpm+4);      % ID25 results
qemb.d27data=lindata(nbpm+5);       % D27 results
dispersion=cat(2,qemb.lindata.Dispersion)';
qemb.tunes=qemb.pm.fulltunes;
qemb.beta=cat(1,qemb.lindata.beta);
qemb.eta=dispersion(:,[1 3]);
% qemb.emittances=[cat(1,lindata.modemit) cat(1,lindata.emit44)];
modemit=cat(1,lindata.modemit);
qemb.emittances=[modemit(:,1:2) cat(1,lindata.emit44)];

% % use reduced lattice indexes
% disp('using reduced lattice for RM computation (correct indexes and lengths)')
% atmodred=sr.model(qemb.at,'reduce',true); % for number of DQ (96, not 131 splitted) for example
% r = atmodred.ring;
% steerhidx = atmodred.get(0,'steerhdq');
% steerhidx = steerhidx(qemres.hlist);
% steervidx = atmodred.get(0,'steerv');
% steervidx =steervidx(qemres.vlist);
% bpmidx = atmodred.get(0,'bpm');

r = qemb.at ;
steerhidx = qemres.steerhidx(qemres.hlist);
steervidx = qemres.steervidx(qemres.vlist);
bpmidx = qemres.bpmidx;

% apply BPM and Correctors, tilt and gain errors to measured RM
[qemb.atresph,qemb.atresph2v,qemb.atrespv2h,qemb.atrespv,...
    qemb.frespx,qemb.frespz]=qemcode(r,qemres.ct,...
    steerhidx,steervidx,bpmidx,...
    qemres.khrot(qemres.hlist),qemres.khgain(qemres.hlist),...
    qemres.kvrot(qemres.vlist),qemres.kvgain(qemres.vlist),...
    qemres.brot,qemres.bhgain,qemres.bvgain);

if nargin >= 3
    lindata0=[qemb0.lindata qemb0.id07data qemb0.d09data qemb0.d17data qemb0.id25data qemb0.d27data];
    beta0=cat(1,lindata0.beta);
    beam66=cat(3,lindata.beam66);
    sigma2=[squeeze(beam66(1,1,:)) squeeze(beam66(3,3,:))];
    qemb.emittances=[qemb.emittances sigma2./beta0];
end

end
