function [rh,rh2v,rv2h,rv]=qemsteerdecode(mach,dct,qemres,resph,resph2v,respv2h,respv,khrot,khgain,kvrot,kvgain)
%QEMSTEERDECODE	Correct response matrices for steerer gain and rotation
%
%[RH,RH2V,RV2H,RV]=QEMSTEERDECODE(MACH,QEMRES,RHM,RH2VM,RV2HM,RVM,...
%   KHROT,KHGAIN,KVROT,KVGAIN)

hmask=false(size(qemres.at));
vmask=hmask;
hmask(qemres.steerhidx(qemres.hlist))=true;
vmask(qemres.steervidx(qemres.vlist))=true;
horv=hmask | vmask;
selh=hmask(horv);
selv=vmask(horv);

% Expand measured responses with model ones
sh(:,selh)=resph;
sh2v(:,selh)=resph2v;
[sh(:,~selh),sh2v(:,~selh)]=qemsteercode(mach,dct,khrot,khgain,find(vmask&(~hmask)),qemres.bpmidx);
sv2h(:,selv)=respv2h;
sv(:,selv)=respv;
[sv2h(:,~selv),sv(:,~selv)]=qemsteercode(mach,dct,kvrot+0.5*pi,kvgain,find(hmask&(~vmask)),qemres.bpmidx);

roth=zeros(size(qemres.at))';
roth(qemres.steerhidx)=khrot;
cosh=cos(roth(horv));
sinh=sin(roth(horv));

rotv=zeros(size(qemres.at))';
rotv(qemres.steervidx)=kvrot;
cosv=cos(rotv(horv));
sinv=sin(rotv(horv));

chv=cos(roth(horv)-rotv(horv));

gh=ones(size(qemres.at))';
gh(qemres.steerhidx)=khgain;
gh=gh(horv);
gv=ones(size(qemres.at))';
gv(qemres.steervidx)=kvgain;
gv=gv(horv);

rh=  (cosv(selh).*gh(selh).*sh(:,selh) - sinh(selh).*gv(selh).*sv2h(:,selh))./chv(selh);
rh2v=(cosv(selh).*gh(selh).*sh2v(:,selh) - sinh(selh).*gv(selh).*sv(:,selh))./chv(selh);
rv2h=(sinv(selv).*gh(selv).*sh(:,selv) + cosh(selv).*gv(selv).*sv2h(:,selv))./chv(selv);
rv=  (sinv(selv).*gh(selv).*sh2v(:,selv) + cosh(selv).*gv(selv).*sv(:,selv))./chv(selv);
end

