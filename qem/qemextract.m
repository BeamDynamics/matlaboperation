function [qemb,qemres,semres] = qemextract(dirname,varargin)
%QEMEXTRACT extract measurement and model from directory
%
%[QEMB,QEMRES,SEMRES]=qemextract(DIRNAME) 
% 
% to trigger the computation of simulated RM (with errors)
% [QEMB,QEMRES,SEMRES]=qemextract(DIRNAME,'complete',true)
%
%see also: qempanelset qemat qemoptics

p = inputParser;

p.addRequired('dirname');
p.addOptional('complete',false); % 
p.addParameter('c',2);

p.parse(dirname,varargin{:});
dirname = p.Results.dirname;
complete = p.Results.complete;


filesRM=dir(fullfile(dirname,'steer*'));
if length(filesRM)==(3*32 + 3*32 + 3*1 + 1) % 3 files for hsteer, vsteer and frequency + steerers.dat
    full_partial=1; % case partial matrix
elseif length(filesRM)==(3*288 + 3*288 + 3*1 + 1)
    full_partial=0; % case full matrix
else
    full_partial=1;
    warning('wrong number of files for response matrix');
end
 
[qemres,semres,qemb]=qempanelset(dirname,full_partial,complete);      % load quad corrections
[qemb(2).kn,qemb(2).dipdelta,qemb(2).dxs]=...   % load quad errors
    qemerrload(qemres,fullfile(qemres.datadir,'quaderrors.mat'));
% warning('temporary hack! remove following line!')
% qemb(2).dipdelta = zeros(size(qemres.dipidx));

A=textscan(fopen(fullfile(qemres.datadir,'skewcor.dat'),'r'),'%s %f','HeaderLines',7); %read from setting manager saved file
qemb(2).skewcor=A{2};
try
    s=load(fullfile(qemres.datadir,'skewerrors.mat'));  % load skew errors
catch
    s.ks=zeros(514,1);
    s.diprot=zeros(352,1);
end
    qemb(2).ks=s.ks;
   qemb(2).diptilt=s.diprot;
   
    
qemb(2).at=qemat(qemres,qemb(2),true);

if complete

    qemb(2)=qemoptics(qemres,qemb(2),qemb(1));

end

end
