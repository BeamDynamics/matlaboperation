function [qk,qtilt,ddelta,dtilt,sdx,sdz,varargout] = qemerrstat(dirlist,qk0)
%QEMSTAT Summary of fitted errors
%
%[QK,QTILT,DDELTA,DTILT,SDX,SDZ]=QEMSTAT(DIRLIST)
%   Returns the errors measured on the different samples selected by DIRLIST

%[QK,QTILT,DDELTA,DTILT,SDX,SDZ,QDKK]=QEMSTAT(DIRLIST,QK0)
%   Returns in addition the relative quadrupole strength error DK/K

[vdipdelta,vdxs]=varextract(dirlist,'quaderrors2.mat','dipdelta','dxs');
[vkn,vks,vdiprot,vdzs]=varextract(dirlist,'skewerrors2.mat','kn','ks','diprot','dzs');
[qk,qtilt]=semtilt(cat(2,vkn{:}),cat(2,vks{:}));
ddelta=cat(2,vdipdelta{:});
dtilt=cat(2,vdiprot{:});
sdx=cat(2,vdxs{:});
sdz=cat(2,vdzs{:});
nout=max(nargout,6)-6;
if nout>0
    qk1=repmat(qk0,1,size(qk,2));
    varargout={(qk-qk1)./qk1};
end
