function [cor,qemres]=qemcorrdt(qemres,kn,cor0,hor_disp)
%QEMCORRDT computes quad correction (rdt method)
%   COR=QEMCORRDT(QEMRES,QERRORS,COR0)
%    Q:     quadrupole strengths
%    COR0:	initial corrector strengths
%
%   see also: QEMCOR, QEMCORFIT, QEMCORGRID

if nargin <4
    hor_disp = qemres.bhscale*qemres.frespx;
end

mach=qemres.at(:);

errcormask=qemres.errmask | qemres.cormask;
kmask=qemres.errmask(errcormask);
cmask=qemres.cormask(errcormask);
goodbpms=true(1,length(qemres.bpmidx));
goodbpms(qemres.wrongbpms)=false;

% Target values
kn0=atgetfieldvalues(mach(qemres.qpidx),'PolynomB',{2});
[frh0,~]=qemfresp(mach,qemres.ct,qemres.bpmidx);
[~,Q0,~]=atlinopt(mach,qemres.dpp,1);

%RDT
disp('RDT derivative');

[respx,respz]=qemrdtresp(mach,qemres.bpmidx,find(errcormask)');

ldkn=qemres.ql .* (kn0-kn);

warning('The quad disp deriv matrix should be copied where posible from the fit one, and recomputed only if cnecessary.')

if ~isfield(qemres,'quadhdispresponse')
    [qemres.quadhdispresponse,~]=qemdispderiv(mach,qemres.ct,@setk,1.e-3,...
        find(errcormask)',qemres.bpmidx); % h dispersion derivative respect to quadrupole
end
lerrcor=atgetfieldvalues(mach(errcormask),'Length');
rx=qemres.quadhdispresponse./lerrcor'; % as in semskewresp

    function elem=setk(elem,dk)
        k=elem.PolynomB(2)+dk;
        elem.K=k;
        elem.PolynomB(2)=k;
    end

if isfield(qemres,'quadtuneresponse')
else
    % numeric version
    % [resptune]=qemtunederiv(mach,qemres.dpp,@setk,1.e-3,...
    %     find(qpcormask)');
    [~,beta,~,~,~,~]=atavedata(mach,qemres.dpp,errcormask);
    qemres.quadtuneresponse=([lerrcor -lerrcor].*beta)'/4/pi;
end
resptune=qemres.quadtuneresponse;

a1 = qemres.hdispRDTcorWeight;
if a1 > 1 || a1<0
    error('hor. dispersion weigth should be between 0 and 1');
end

a2 = qemres.tuneRDTcorWeight;
if a2 > 1-a1 || a2<0
    error(['tune weigth '  num2str(a2) ' should be between 0 and ' num2str(1-a1)]);
end

fx=respx(:,kmask)*ldkn - respx(:,cmask)*cor0;
fz=respz(:,kmask)*ldkn - respz(:,cmask)*cor0;
dfrh = frh0 - hor_disp;
dtunes = (Q0 - qemres.tunes)';
cor=cor0;

disp('start correction');

rsp=[...
    (1-a1-a2)*real(respx(:,cmask));...
    (1-a1-a2)*imag(respx(:,cmask));...
    (1-a1-a2)*real(respz(:,cmask));...
    (1-a1-a2)*imag(respz(:,cmask));...
    a1*rx(goodbpms,cmask);...dispersion
    a2*resptune(:,cmask)]; % tune
b=[...
    (1-a1-a2)*real(fx);...
    (1-a1-a2)*imag(fx);...
    (1-a1-a2)*real(fz);...
    (1-a1-a2)*imag(fz);...
    a1*dfrh(goodbpms);... % dispersion
    a2*dtunes]; % tune

%     cor(qcorkeep)=cor(qcorkeep) + rsp\b;
cor(qemres.qcorkeep)=cor(qemres.qcorkeep) + qemsvd(rsp(:,qemres.qcorkeep),b,qemres.neigQuadRDTcor);

fxc=respx(:,kmask)*ldkn - respx(:,cmask)*cor;
fzc=respz(:,kmask)*ldkn - respz(:,cmask)*cor;
dfrhc = rx(:,kmask)*ldkn - rx(:,cmask)*cor;
dtunesc = resptune(:,kmask)*ldkn - resptune(:,cmask)*cor;

disp(['f1001: ' num2str(std(fx)) ' - > ' num2str(std(fxc))]);
disp(['f1010: ' num2str(std(fz)) ' - > ' num2str(std(fzc))]);
disp(['dh: ' num2str(std(dfrh)) ' - > ' num2str(std(dfrhc))]);
disp(['dtune h: ' num2str(dtunes(1)) ' - > ' num2str(dtunesc(1))]);
disp(['dtune v: ' num2str(dtunes(2)) ' - > ' num2str(dtunesc(2))]);

end
