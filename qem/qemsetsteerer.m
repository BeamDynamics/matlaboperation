function elem=qemsetsteerer(elem,kickh,kickv)
l=elem.Length;
if l==0, l=1; end
elem.PolynomB(1)=elem.PolynomB(1)-kickh/l;
elem.PolynomA(1)=elem.PolynomA(1)+kickv/l;
end
