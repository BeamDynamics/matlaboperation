function qemplotresp(fig,resph,respv,labl)

[nbpm,nsteer]=size(resph);
figure(fig);
h1=subplot(2,2,1,'Parent',fig);
bar(h1,std2(resph));
set(h1,'Xlim',[0 nsteer+1]);
title(h1,['H rms ' labl]);
ylabel('std h c.o.');
xlabel('h corrector');

h3=subplot(2,2,3,'Parent',fig);
bar(h3,std2(resph'));
set(h3,'Xlim',[0 nbpm+1]);
ylabel('std h c.o.');
xlabel('h bpm');

[nbpm,nsteer]=size(respv);
h2=subplot(2,2,2,'Parent',fig);
bar(h2,std2(respv));
set(h2,'Xlim',[0 nsteer+1]);
title(h2,['V rms' labl]);
ylabel('std v c.o.');
xlabel('v corrector');

h4=subplot(2,2,4,'Parent',fig);
bar(h4,std2(respv'));
set(h4,'Xlim',[0 nbpm+1]);
ylabel('std v c.o.');
xlabel('v bpm');
