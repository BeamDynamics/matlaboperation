% User interface for
% - measurement of response matrices
% - optics analysis
% - corrections
%
% Data stored in 3 stucture arrays:
%
% >> qemb(2)
% ans = 
%             cor: [32x1 double]		integrated quad correctors [m-1]
%         skewcor: [64x1 double]		integrated skew quad correctors [m-1]
%             dxs: [224x1 double]		sextu H displacement [m]
%             dzs: [224x1 double]		sextu V displacement [m]
%         diptilt: [64x1 double]		dipole tilt [rad]
%        dipdelta: [64x1 double]		dipole field error dB/B
%              ks: [256x1 double]		quad normal strength [m-2]
%              kn: [256x1 double]		quad skew strength [m-2]
%              at: {1639x1 cell}		at structure
%              pm: [1x1 struct]         global parameters
%           tunes: [36.4399 13.3895]
%         lindata: [1x224 struct]       data on BPMS
%            beta: [224x2 double]
%             eta: [224x2 double]
%      emittances: [227x6 double]
%         atresph: [224x16 double]
%       atresph2v: [224x16 double]
%       atrespv2h: [224x16 double]
%         atrespv: [224x16 double]
%          frespx: [224x1 double]
%          frespz: [224x1 double]
%          d9data: [1x1 struct]
%     d11lensdata: [1x1 struct]
%        id25data: [1x1 struct]
% 
% qemres
% qemres = 
%        bhscale: 1
%          hlist: [1x16 double]         list of H steerers (from 1 to 96)
%          vlist: [1x16 double]         list of V steerers (from 1 to 96)
%         skcode: 10
%        datadir: '/machfs/MDT/2013/Mar24/resp1'
%      opticsdir: '/machfs/MDT/2013/Mar24/resp1/optics'
%          tunes: [0.4400 0.3900]
%             at: {1639x1 cell}         reference at structure
%         dipidx: [1x64 double]         index of dipoles in at structure
%          qpidx: [1x256 double]        index of quadrupoles in at structure
%       qdlowidx: [971 974]             index of QDLOW magnet
%          qpfit: [1x256 logical]       mask of fitted quads
%             ql: [256x1 double]        quadrupole lengths
%        sextidx: [1x224 double]        index of sextupoles in at structure
%            sxl: [224x1 double]        sextupole lengths
%      steerhidx: [1x96 double]         index of H steerers in at structure
%         khgain: [1x96 double]
%          khrot: [1x96 double]
%        hsteerl: [96x1 double]         H steerer lengths
%      steervidx: [1x96 double]         index of V steerers in at structure
%         kvgain: [1x96 double]
%          kvrot: [1x96 double]
%        vsteerl: [96x1 double]         V steerer lengths
%        qcoridx: [1x32 double]         index of quad correctors
%          qcorl: [32x1 double]         quad corrector lengths
%         bpmidx: [1x224 double]        index of BPMS
%         bhgain: [224x1 double]
%         bvgain: [224x1 double]
%          bhrot: [224x1 double]
%          bvrot: [224x1 double]
%           brot: [224x1 double]
%        skewidx: [1x64 double]         index of skew quad correctors
%          skewl: [64x1 double]         skew corrector lengths
%        id25idx: 1091
%          d9idx: 292
%     d11lensidx: 394
%
% semres
% semres = 
%            hlist: [1x16 double]         list of H steerers (from 1 to 96)
%            vlist: [1x16 double]         list of V steerers (from 1 to 96)
%            wdisp: 0
%           iaxemz: [14x1 double]
%          iaxname: {14x1 cell}
%          bvscale: 1
%     quadresponse: [7392x256 double]
%     skewresponse: [7392x64 double]

global qemb qemres semres
h=qempanel2;
