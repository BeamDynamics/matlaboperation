function resp = qemsextchromaresp(mach,sextidx)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

[~,avebeta,avemu,avedisp]=atavedata(mach,0,sextidx);
resp=(avebeta.*avedisp(:,[1 1]))'./2./pi;
end

