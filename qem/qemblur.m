function [rh,rv,rv2h,rh2v] = qemblur(rh0,rv0,qemres)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

ckh=cos(qemres.khrot(qemres.hlist));
skh=sin(qemres.khrot(qemres.hlist));
ckv=cos(qemres.kvrot(qemres.vlist));
skv=sin(qemres.kvrot(qemres.vlist));
cb=cos(qemres.brot);
sb=sin(qemres.brot);
rh=rh0.*((cb./qemres.bhgain)*(ckh./qemres.khgain))+rv0.*((sb./qemres.bhgain)*(skh./qemres.khgain));
rh2v=-rh0.*((sb./qemres.bvgain)*(ckh./qemres.khgain))+rv0.*((cb./qemres.bvgain)*(skh./qemres.khgain));
rv2h=-rh0.*((cb./qemres.bhgain)*(skv./qemres.kvgain))+rv0.*((sb./qemres.bhgain)*(ckv./qemres.kvgain));
rv=rh0.*((sb./qemres.bvgain)*(skv./qemres.kvgain))+rv0.*((cb./qemres.bvgain)*(ckv./qemres.kvgain));
end
