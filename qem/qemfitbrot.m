function [alpha1,alpha2] = qemfitbrot(qemres,semres,qemb)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

rh=qemb.atresph;                        % normal response
rv=qemb.atrespv;
rv2h=semres.resph-qemb.atrespv2h;       % residual cross-response
rh2v=semres.respv-qemb.atresph2v;

k1=-sum(rh.*rh2v,2)./sum(rh.*rh,2).*qemres.bvgain;
k2=-sum(rv.*rv2h,2)./sum(rv.*rv,2).*qemres.bhgain;     % H residual
alpha1=atan(k1);
alpha2=-atan(k2);
% bhgain=sum(rh.*qemres.resph,2)./sum(qemres.resph.*qemres.resph,2);
% bvgain=sum(rv.*qemres.respv,2)./sum(qemres.respv.*qemres.respv,2);
end

