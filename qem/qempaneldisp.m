function [qemb,qemres]=qempaneldisp(qemres,semres,qemb,qemb0,handles)

cormask=false(length(qemres.at));
cormask(qemres.qcoridx)=true;
qpmask=false(length(qemres.at));
qpmask(qemres.qpidx)=true;
qpcormask=qpmask & cormask;

tic
if nargin < 5, handles=struct(); end

if isfield(handles,'axes1')
    qcheck(qemres,qemb.kn,qemb0.kn,qemb.dipdelta,handles.axes1,get(handles.largefigures,'Value'));
else
    qcheck(qemres,qemb.kn,qemb0.kn,qemb.dipdelta,[],get(handles.largefigures,'Value'));
end
toc;
qemb.at=qemat(qemres,qemb,true);    % builds the at structure
disp('qemat');toc;
try
    qemb=qemoptics(qemres,qemb);        % stores at optical parameters
    disp('qemoptics');toc;
catch err
    disp(err);
    error(['Impossible to compute optics parameters. ', ...
    'Try to re-INITIALize Errors and/or Correction, and compute with lower # of eigenvectors or different weigth factors.']);
end
%avebeta=srbpmaverage(qemb.beta,'unfold');
%modrnh=bmodul(qemb.beta(:,1),avebeta(:,1));
modulh=bmodul(qemb.beta(:,1),qemb0.beta(:,1));
%modrnv=bmodul(qemb.beta(:,2),avebeta(:,2));
modulv=bmodul(qemb.beta(:,2),qemb0.beta(:,2));

stpb=qemstopbands(qemres,qemb,qemb0);
disp('qemstopbands');toc;
mess={...
    sprintf('OPTICS: %s',qemres.opticsname);...
    sprintf('tunes H/V: %.4f/%.4f',qemb.tunes);...
    sprintf('deltap/p: %.3e',qemres.dpp);...
    %   [' modRN[HOR]= ' num2str(std(modrnh,1)) ',  modRN[VER]= ' num2str(std(modrnv,1))];...
    [' modul[HOR]  = ' num2str(std(modulh,1)*100,'%2.2f %%') ];...
    [' modul[VER]  = ' num2str(std(modulv,1)*100,'%2.2f %%')];...
    [' 2*NuX = 152  :' num2str(stpb.h152*1e3,'%2.2f')];...
    [' 2*NuZ = 54   :' num2str(stpb.v54*1e3,'%2.2f')];...
    [' 2*NuX = 153  :' num2str(stpb.h153*1e3,'%2.2f')];...
    [' 2*NuZ = 55   :' num2str(stpb.v55*1e3,'%2.2f')]};

% if all(isfield(handles,{'axes2','axes3'}))	% displays beta modulation
%     sbpm=cat(1,qemb.lindata.SPos);
%     qemmodulplot(handles.axes2, sbpm, modulh, '');hold on;
%     qemmodulplot(handles.axes2, sbpm, modulv, '');hold off;
%     legend('\beta_x','\beta_z')
%
%     %   qemd(handles.axes2, sbpm, modulv, '\eta_x');
% end
disp('qemstopbands msg');toc;


%%

if all(isfield(handles,{'axes2','axes3'}))	% displays beta modulation
    sbpm=cat(1,qemb.lindata.SPos);
    qemmodulplot(handles.axes2, sbpm, modulh, '\beta_x');
    qemmodulplot(handles.axes3, sbpm, modulv, '\beta_z');
end


%%
if all(isfield(qemres,{'resph','respv','frespx'}))
    residual=qem_residual(qemb,qemres,semres);
    qemres.fithist=[qemres.fithist;[residual.h,residual.v,residual.hdisp]];
    mess=[mess;' ';...
        ['orm res. = '  num2str(residual.residual,'%.2e') ...
        ', H: ' num2str(residual.h,'%.1e') ' ' ...
        'V: ' num2str(residual.v,'%.1e') ''];...
        ['H disp. residual = ' num2str(residual.hdisp,'%.2e')];...
%        ['\Chi^2/D.O.F. = ' ...
%        num2str(sum((diffh(~isnan(diffh(:))).^2+diffv(~isnan(diffv(:))).^2+hdispresidual(~isnan(hdispresidual(:))).^2 )./ sigbpm.^2)...
%          /(length(qemres.qpidx(qemres.qpfit))),'%.2e')];...length([diffh(:);diffv(:);hdispresidual(:)])-
        [];...
        [num2str(length(qemres.wrongbpms)) ' bpms not used: ' num2str(qemres.wrongbpms,'%d ')]];
    qemplotresp(4,qemb.atresph-qemres.resph,qemb.atrespv-qemres.respv,'deviation');
end
figure(61);
yyaxis left
hold off;
plot(qemres.fithist(:,1),'bx-','LineWidth',3,'MarkerSize',10);
hold on;
plot(qemres.fithist(:,2),'ro-','LineWidth',3,'MarkerSize',10);
try
plot(semres.fithist(:,1),'cx-','LineWidth',3,'MarkerSize',10);
plot(semres.fithist(:,2),'mo-','LineWidth',3,'MarkerSize',10);
catch
end
xlabel('fit step #');
ylabel('std RM residual');
grid on;
yyaxis right 
hold off;
plot(qemres.fithist(:,3),'gs-','LineWidth',3,'MarkerSize',10);
hold on;
try
plot(semres.fithist(:,3),'ys-','LineWidth',3,'MarkerSize',10);
ylabel('disp. residual');
legend('HH','VV','V2H','H2V','DH','DV');
catch
ylabel('disp. residual');
legend('HH','VV','DH');
end


disp('plot disp'); toc;

if isfield(handles,'text3')                 % displays values
    set(handles.text3,'String',mess,'FontSize',10);
else
    fprintf('%s\n',mess{:});
end

if isfield(handles,'axes4')                 % displays corr. strengths
    if  get(handles.largefigures,'Value')
        figure(51); extfigax = gca;
        plot(extfigax,qemb.cor);
        legend(extfigax,'initial','corrected');
        title(extfigax,'Corrector strengths');
        grid(extfigax,'on');
    end
    scor=corstd(qemb.cor(:,1));
    if size(qemb.cor,2) > 1
        scor=[scor corstd(qemb.cor(:,end))];
    end
    bar(handles.axes4,scor(1:8,:));  % forget injection correctors
        
%     Nc = length(qemb.cor);
%     extracor = mod(Nc,32); % remove extra correctors from injection cells
%     cor_per_cell_std = squeeze(std(reshape(qemb.cor((1+extracor/2):(Nc-extracor/2),:),[],32,size(qemb.cor,2)),1,1));
%     % cor_per_cell_mean = squeeze(mean(reshape(qemb.cor((1+extracor/2):(Nc-extracor/2),:),32,[],2),2));
%     wc= [0.9,0.5]; % superimpose ba
%     %set(gcf,'CurrentAxes',handles.axes4);
%     cla(handles.axes4);
%     hold off;
%     try
%         yyaxis rigth
%         bar(handles.axes4,cor_per_cell_std(:,1),wc(1),'EdgeColor','none');
%         
%         yyaxis left
%         bar(handles.axes4,cor_per_cell_std(:,2),wc(2),'EdgeColor','none');
%         yyaxis rigth
%     catch
%         bar(handles.axes4,cor_per_cell_std,0.9,'EdgeColor','none');    
%     end
%     handles.axes4.XTick=[1:32];
    handles.axes4.XTickLabel={'QF1','QD2','QD3','QF4','QF4','QD5','QF6','QF8'};
%     handles.axes4.XTickLabelRotation=90;
    ylabel(handles.axes4,'\Deltak/k');
    title(handles.axes4,'std Corrector strengths (per family)');
    legend(handles.axes4,'initial','corrected');
    grid(handles.axes4,'on');
    
    
end

    function stdval=corstd(corval)
        corstrength = corval./qemres.qcorl;
        dkk=corstrength(qpcormask(cormask))./qemb0.kn(qpcormask(qpmask));
        stdval=std(sr.fold(dkk),1,2,'omitnan');
    end

    function mdl=bmodul(beta,beta0)
        mdl=(beta-beta0)./beta0;
    end
end
