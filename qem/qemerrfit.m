function [deltaset,deltaset_err]=qemerrfit(nhst,nvst,resp,dresp,mode,okbpm,ploteig)
%QEMERRFIT Vary lattice parameters to fit the measured response matrix
%DELTAV=QEMPANELFIT(NHST,NVST,RESP,DRESP,MODE,OKVAR,OKBPM)
%
%NHST:      Number of horizontal kicks
%NHST:      Number of vertical kicks
%RESP:      Deviation of measured reponse matrices from the model
%DRESP:     Derivatives of response matrices vs. variable elements
%MODE:      Structure allowing control of SVD inversion. (default:)
%OKVAR:     Select a subset of variable parameters (default: all)
%OKBPM:     Select a subset of valid BPMS (default:all)
%
%DELTAV:    Variation of selected parameters
%

[no,nq]=size(dresp);
nbpm=(no-2)/(nhst+nvst+1);
orbitrange=nbpm*(nhst+nvst);
if nargin<7 , ploteig=true; end
if nargin<6 || isempty(okbpm), okbpm=true(nbpm,1); end
if nargin<5, mode=struct; end
if ~isfield(mode,'nsets'), mode.nsets=4; end
if ~isfield(mode,'neigs'), mode.neigs=100; end
if ~isfield(mode,'tuneweight'), mode.tuneweight=1000; end
if ~isfield(mode,'dispweight'), mode.dispweight=0.01; end
if ~isfield(mode,'vnorm')
%   mode.vnorm=1./std(dresp(1:orbitrange,:),1,1);
   mode.vnorm=ones(1,nq);           % no normalization
end
inisel=false(size(resp));
if mode.tuneweight>0, inisel(end-1:end)=true; end            % Keep tunes
if mode.dispweight>0, inisel(orbitrange+(1:nbpm))=true; end  % Keep dispersion
bok=[repmat(okbpm,nhst+nvst+1,1);true;true] & isfinite(resp);
v=mode.vnorm;
%w=[ones(orbitrange,1);ones(nbpm,1);100;100];
%sk=qemsolvex(dresp(:,qpfit).*(w*v(qpfit)'),resp.*w,bok,16,60);
%w=[ones(orbitrange,1);ones(nbpm,1);500;500];
%sk=qemsolvex(dresp(:,qpfit).*(w*v(qpfit)'),resp.*w,bok,8,100);
%w=[ones(orbitrange,1);ones(nbpm,1);1000;1000];
%sk=qemsolvex(dresp(:,qpfit).*(w*v(qpfit)'),resp.*w,bok,4,100);
w=[ones(orbitrange,1);mode.dispweight*ones(nbpm,1);mode.tuneweight*ones(2,1)];
try
    hsets=reshape(1:nhst,mode.nsets,[]);
    vsets=reshape(1:nvst,mode.nsets,[]);
catch
    warning('Error dividing in subsets')
    disp([ 'NHst/Nsets = ' num2str(nhst) ' / ' num2str(mode.nsets) ' = ' num2str(nhst/mode.nsets) '<-should be integer'])
    disp([ 'NVst/Nsets = ' num2str(nvst) ' / ' num2str(mode.nsets) ' = ' num2str(nvst/mode.nsets) '<-should be integer'])
    disp('setting nsets = 1');
    mode.nsets = 1;
    hsets=reshape(1:nhst,mode.nsets,[]);
    vsets=reshape(1:nvst,mode.nsets,[]);
end
[sk,dsk]=qemsolvex(dresp.*(w*v),resp.*w,bok,hsets,vsets,mode.neigs);
deltaset=mean(sk,2).*v';
deltaset_err = mean(dsk,2).*v';
devdev=std(sk,1,2).*v';
fprintf('Deviation between sets: %g\n',sqrt(mean(devdev.*devdev)));
nbpm = length(okbpm);

    function sel=lselected(sh,sv)
        sel=inisel;
        sel(semselect(sh,sv,nbpm,nhst))=true;
    end

    function [sk,dsk]=qemsolvex(dresp,resp,ok,hsets,vsets,neig)
        [nl,nc]=size(dresp);
        ngs=min([nl nc neig]);
        [nsets,nh]=size(hsets); %#ok<ASGLU>
        [nsets,nv]=size(vsets);
        fprintf('Solving %i sets(%i H steerers, %i V steerers) with %i Eigen vectors\n',nsets,nh,nv,ngs);
        sk=zeros(nc,nsets);
        dsk=zeros(nc,nsets);
        for i=1:nsets
            lok=lselected(hsets(i,:),vsets(i,:)) & ok;
            [sk(:,i),dsk(:,i)]=qemsvd_mod(dresp(lok,:),resp(lok),ngs,ploteig);
        end
    end
end

