function [rh,rv] = qemresp(mach,dct,bidx,sidx,elemfunc,varargin)
%QEMRESP Builds the model response matrix
%
%[RESPH,RESPV]=QEMRESP(MACH,DCT,BPMIDX,STEERIDX,ELEMFUNC,~,KICKS)
%
nb=length(bidx);
nq=length(sidx);
vargs={[],[]};
vargs(1:length(varargin))=varargin;
kick=0.000025*ones(1,nq);       % default kick: 25 urad
if ~isempty(vargs{2}), kick(:)=vargs{2}; end
rh=zeros(nb,nq);
rv=zeros(nb,nq);
for iq=1:nq
    idq=sidx(iq);
    esave=mach{idq};
    mach{idq}=elemfunc(esave,iq,0.5*kick(iq));
    orbitp=findsyncorbit(mach,dct,bidx);
    mach{idq}=elemfunc(esave,iq,-0.5*kick(iq)); %double sided computation
    orbitm=findsyncorbit(mach,dct,bidx);
    orbit=(orbitp-orbitm)/kick(iq);
    mach{idq}=esave;
    rh(:,iq)=orbit(1,:)';
    rv(:,iq)=orbit(3,:)';
end
end
