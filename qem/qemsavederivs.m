function qemsavederivs(opticsbase,varargin)
%QEMSAVEDERIVS	Store response matrix derivatives
%
%QEMSAVEDERIVS(OPTICSNAME,CODE1,CODE2...)
%   Compute the derivatives of the orbit, H_dispersion and tunes responses
%   with respect to quadrupole strengths, and store them in .mat files
%
%	OPTICSNAME: name of an optics directory in $APPHOME/optics/sr
%	CODE:       steerer name, or 'hdisp' or 'tunes': specify the matrices
%	            to be computed
%
%QEMSAVEDERIVS(...,'basetunes',[nux nuz])
%   Compute the matrices for the given tune, instead of the nominal model tunes
%
%Example:
%>> qemsavederivs('S28F_Capitalized_BM_05_16','hdisp','basetunes',[0.19 0.23]);
%
% Computes for the 'S28F_Capitalized_BM_05_16' optics, retuned to [0.19 0.23],
% the dispersion derivatives with respect to quadrupole strengths and
% creates 'quadhdisp.mat' in the current directory

global APPHOME

[tunes,vargs]=getoption(varargin,'basetunes',nan(1,2));
qemres.opticsdir=fullfile(APPHOME,'optics','sr',opticsbase);
a=load(fullfile(qemres.opticsdir,'betamodel.mat'),'betamodel');
ring=atradoff(a.betamodel,'IdentityPass','auto','auto');
atmod=sr.model(ring,'reduce',true); % for number of DQ (96, not 131 splitted) for example
if isfinite(tunes)
    atmod.settune(tunes);
end
fprintf('\nComputing response matrix derivatives for tunes: %g, %g\n',atmod.nuh,atmod.nuv);
qemres.atmodel=atmod;
qemres.at=atmod.ring;
qemres.ct=0.0;
syncorb=findsyncorbit(qemres.at,qemres.ct);
qemres.dpp=syncorb(5,1);

dlmask=atgetcells(atmod.ring,'FamName','[DJ]L[12][ABDE]_5\w*');
dqmask=atmod.select('dq');
quadmask=atmod.select('qp');
sfmask=atgetcells(atmod.ring,'FamName','S[DFIJ][12][ABDE]');
errmask=quadmask | dqmask | sfmask;	% All QPs, DQs, SF2s

qemres.errmask = errmask;
qemres.qpidx=find(errmask)';

qemres.quadidx = find(quadmask);	% Only quadrupoles
qemres.dipidx=find(dlmask | dqmask);
qemres.dqidx=find(dqmask);
qemres.steerhidx=find(atmod.select('steerhdq'))';
qemres.steervidx=find(atmod.select('steerv'))';
qemres.bpmidx=find(atmod.select('bpm'))';

qemres.qpfit = errmask(errmask);        % All true

cormask=quadmask;
qemres.cormask = cormask;
qemres.qcoridx=find(cormask)';     % All quadrupoles

qemres.qcorkeep=cormask(cormask);   % Keep all

qemres.quadorbitresponse=nan(0,802);
qemres.quadhdispresponse=nan(0,802);
qemres.quadtuneresponse=nan(0,802);


for steerer=string(vargs)
    [qemres.hlist,qemres.vlist]=sr.steerfamily(steerer);
	fname=join(["quad",steerer],'');
    if ~isempty(qemres.hlist)
        dresp=compute(rmfield(qemres,'quadorbitresponse'));
        lresp=320*32;
        dX_dq=reshape(dresp(1:lresp,:),320,32,[]);
        dY_dq=reshape(dresp(lresp+(1:lresp),:),320,32,[]);
        save(fname,'dX_dq','dY_dq');
        fprintf('\n"%s" created in the current directory.\n',fname);
        fprintf('This file must be copied to %s\n\n',qemres.opticsdir)
    elseif strcmp(steerer,'hdisp')
        dresp=compute(rmfield(qemres,'quadhdispresponse'));
        dDx_dq=dresp;
        save(fname,'dDx_dq');
        fprintf('\n%s created in the current directory.\n',fname);
        fprintf('This file must be copied to %s\n\n',qemres.opticsdir)
    elseif strcmp(steerer,'tunes')
        dresp=compute(rmfield(qemres,'quadtuneresponse'));
        dTunes_dq=dresp;
        save(fname,'dTunes_dq');
        fprintf('\n%s created in the current directory.\n',fname);
        fprintf('This file must be copied to %s\n\n',qemres.opticsdir)
    else
        warning('Unknown keyword %s, no file created',steerer);
    end
end

    function dresp=compute(qemres)
        shidx=qemres.steerhidx(qemres.hlist);
        svidx=qemres.steervidx(qemres.vlist);
        dresp=qemderivAnalytic(qemres.at,0.0,qemres,@setk,0.0001,qemres.qpidx(qemres.qpfit),qemres.bpmidx,shidx,svidx);
        function elem=setk(elem,dk)
            strength=elem.PolynomB(2)+dk;
            elem.K=strength;
            elem.PolynomB(2)=strength;
        end
    end
end

