function [hang,vang] = qemfitkrot(qemres,semres,qemb)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

rh=qemb.atresph;                        % normal response
rv=qemb.atrespv;
rv2h=semres.resph-qemb.atrespv2h;       % residual cross-response
rh2v=semres.respv-qemb.atresph2v;
vang=atan(-sum(rh.*rv2h)./sum(rh.*rh));	% H unexpected kick
hang=atan( sum(rv.*rh2v)./sum(rv.*rv));	% V unexpected kick
end

