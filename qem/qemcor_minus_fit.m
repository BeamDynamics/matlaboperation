function [cor,qemres]=qemcor_minus_fit(qemres,kn,cor0) %#ok<INUSD>
%QEMCORRDT computes quad correction (opposite of errors)
%   [COR,QEMRES]=QEMCORRDT(QEMRES,QERRORS,COR0)
%
%    QERRORS:     quadrupole strengths
%    COR0:	initial corrector strengths
%
%   see also: QEMCORRDT, QEMCORFIT, QEMCORGRID

mach=qemres.at(:);

cormask=qemres.cormask;
cormask(cormask)=qemres.qcorkeep;
errcormask=qemres.errmask & cormask;
kmask=errcormask(qemres.errmask);
cmask=errcormask(cormask);

% Target values
kn0=atgetfieldvalues(mach(qemres.qpidx),'PolynomB',{2});

% sign minus
disp('apply (-) fitted errors');
disp('start correction');

ldkn=qemres.ql .* (kn0-kn);
    
cor=zeros(length(qemres.qcoridx),1);
cor(cmask) = cor(cmask) + ldkn(kmask);

end
