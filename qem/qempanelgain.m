function [kgain,lgain]=qempanelgain(measresp,modelresp,kgini,bgini)

ok=isfinite(measresp);
goodsteerer = any(isfinite(measresp));
bpmlist=sum(ok,2)>size(measresp(:,goodsteerer,1),2)-1; % sum only columns that are not NaN (disabled/frozen/fault correctors) 
%nok=224-sum(bgini(~bpmlist));
sumgain=sum(bgini(bpmlist));
kgain=kgini;
lgain=bgini;
x=measresp(bpmlist,:);    % Non corrected measured matrix
x(~isfinite(x))=0;
xx=x.*x;
xy=x.*modelresp(bpmlist,:);
for count=2:5
    [k2,l2]=meshgrid(kgain,lgain(bpmlist));
    gain=k2.*l2;
    kincr=sum(gain.*xy,1)./sum(gain.*gain.*xx,1);
    kgain=kgain.*kincr;
    %  kmax=max2(abs(kincr-1))
    [k2,l2]=meshgrid(kgain,lgain(bpmlist));
    gain=(k2.*l2);
    
    lincr=sum(gain(:,goodsteerer).*xy(:,goodsteerer),2)./sum(gain(:,goodsteerer).*gain(:,goodsteerer).*xx(:,goodsteerer),2);
    %   lincr=lincr*power(prod(lincr),-1/nok);
    %   lincr=lincr*(nok/sum(lincr));            % Keep <bgain>=0
    lincr=lincr*(sumgain/sum(lgain(bpmlist).*lincr));    % Keep <bgain>=0
    lgain(bpmlist)=lgain(bpmlist).*lincr;
    %  lmax=max2(abs(lincr-1))
end
