function residual=qem_residual(qemb,qemr,semr)
%QEM_RESIDUAL   Compute and display the fir residuals

if all(isfield(qemr,{'resph','respv','frespx'}))
    diffh=qemb.atresph-qemr.resph;    % Response H -> H in m/rad
    diffv=qemb.atrespv-qemr.respv;    % Response V -> V in m/rad
    diffv2h=qemb.atrespv2h-semr.resph;    % Response V -> H in m/rad
    diffh2v=qemb.atresph2v-semr.respv;    % Response H -> V in m/rad
    diff1=[diffh diffv];
    hdispresidual=qemb.pm.alpha*std2(qemr.bhscale*qemr.frespx-qemb.frespx);
    vdispresidual=qemb.pm.alpha*std2(semr.bvscale*semr.frespz-qemb.frespz);
    residual=struct('residual',std(diff1(:),1,'omitnan'),...
        'h',std(diffh(:),1,'omitnan'),'h2v',std(diffh2v(:),1,'omitnan'),...
        'v2h',std(diffv2h(:),1,'omitnan'),'v',std(diffv(:),1,'omitnan'),...
        'hdisp',hdispresidual,'vdisp',vdispresidual);
    fprintf('residual H     = %g m/rad\n', residual.h);
    fprintf('residual V     = %g m/rad\n', residual.v);
    fprintf('residual Hdisp = %g m\n', residual.hdisp);
    fprintf('residual V2H   = %g m/rad\n', residual.v2h);
    fprintf('residual H2V   = %g m/rad\n', residual.h2v);
    fprintf('residual Vdisp = %g m\n', residual.vdisp);
 end
