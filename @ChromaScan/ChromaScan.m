classdef ChromaScan < GenericScan %& GridScan
    %Chromaticity scan
    %
    % vary Hor. and Ver. chromaticity in a grid of points to seek for optimum.
    %
    %
    %
    %see also:
    
    properties
        
        
    end
    
    
    methods
        
        % creator
        function obj = ChromaScan(machine)
            %ChromaScan Construct an instance of this class
            %
            % for help type:
            % >> doc GenericScan
            %
            %see also: RingControl
            
            obj@GenericScan(machine);
            
            obj.x_label = '\xi_x';
            obj.y_label = '\xi_y';
            
            obj.C_h_range =  [-2 1]; % 2x1 array of min and max Q_h
            obj.C_v_range =  [-2 1]; % 2x1 array of min and max Q_v
            
            obj.DefineGridPoints;
            
            obj.scan_name = ['ChromaScan_' datestr(now,'YYYY_mm_dd_HH_MM')]; 
             
        end
        
        
        function ExecuteChange(obj,DesiredVal)
            % change chroma and recover tunes
            
            % get present tunes.
            
            inittune =  obj.GetTune; % read tunes
            disp(['Present tune: ' num2str(inittune)]);

            % change chroma
            disp('desired Chroma change:')
            DesiredChroma = DesiredVal; %[qx(itun),qy(itun)];
            disp(DesiredChroma);
            M=load('/operation/beamdyn/matlab/optics/sr/theory/chrommat.mat');
            
            obj.SextDev.CorrectionStrengths = obj.SextInitial +...
                (M.DKL_DChromaticity*DesiredChroma')';
            
            pause(obj.wait_for_tune);
            
%            if ~obj.scan_automatic
                input('press if ready to correct tunes','s');
%            end
            
            % change tunes to initial
            obj.movetune(obj.initial_tunes,inittune);
        end
        
        
        
    end
    
end

