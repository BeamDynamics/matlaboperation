function startmachine(rootpath,varargin)
%STARTMACHINE   Set the MATLAB path for operation
% startmachine(rootpath,varargin)
%
%
% % optional inputs
% ...'at',path/to/accelratorToolbox... 
% ...'oar',path/to/cluster_functions...
% ...'mat',path/to/matlab_operation...
% ...'tango',path/to/tango-matlab-binding... 
% ...'tango_high_level',path/to/tango-matlab-binding_highlevel...
%
%see also: 

global APPHOME MACHFS
if nargin <  1, rootpath=fileparts(pwd); end
    [atpath, opts]=getopt(varargin,'at',fullfile(rootpath,'at'));
    [oarpath, opts]=getopt(varargin,'oar',fullfile('/machfs/BeamDynamics/','ATClusterTools'));
    [matpath,opts]=getopt(opts,'mat',fullfile(rootpath,'matlab'));
%     [~,hostname]=system('hostname');
%     switch hostname(1:end-1)
%         case {'grappa','slurm-nice-devel2903'}
%             [tgpath,opts]=getopt(opts,'tango',fullfile(rootpath,'matlab-binding-ubuntu20.04')); %#ok<ASGLU>
%         otherwise
%             [tgpath,opts]=getopt(opts,'tango',fullfile(rootpath,'matlab-binding-debian9')); %#ok<ASGLU>
%     end
    
    [~,osname]=system('head -n 1 /etc/os-release ');
    osname = osname(1:end-1);
    switch osname
        case {'PRETTY_NAME="Ubuntu 24.04 LTS"', 'PRETTY_NAME="Ubuntu 24.04.1 LTS"'}
            [tgpath,opts]=getopt(opts,'tango',fullfile(rootpath,'matlab-binding-ubuntu24.04.1')); %#ok<ASGLU>
        case 'NAME="Ubuntu"'
            [tgpath,opts]=getopt(opts,'tango',fullfile(rootpath,'matlab-binding-ubuntu20.04')); %#ok<ASGLU>
        otherwise
            [tgpath,opts]=getopt(opts,'tango',fullfile(rootpath,'matlab-binding-debian9')); %#ok<ASGLU>
    end
    %val = getenv('LD_LIBRARY_PATH');
    %disp(val)
    %setenv('LD_LIBRARY_PATH',['/segfs/tango/release/ubuntu2404/lib:' val])
    %val = getenv('LD_LIBRARY_PATH');
    %disp(val)
    

    [tgHLpath,opts]=getopt(opts,'tango_high_level',fullfile(rootpath,'tango-matlab')); %#ok<ASGLU>
    
if ~(exist('isdeployed','builtin') && isdeployed)
    
    optpath(...         % Same order as in the resulting PATH
        '~/matlab',...	% personal folder 1st
        matpath,...
        oarpath,...
        fullfile(tgpath,'src/mex-file'),...% fullfile(fullfile(rootpath,'tango-matlab'),'tango-matlab/mex-file'),... 
        fullfile(tgpath,'m-files'),...fullfile(fullfile(rootpath,'tango-matlab'),'tango-matlab/m-files'),...   
        fullfile(tgHLpath,'highlevel-tango-matlab/ESRFspecific'),...
        fullfile(atpath,'atmat'),...
        fullfile(atpath,'atintegrators'),...
        fullfile(atpath,'machine_data'),...
        fullfile(rootpath,'fitandresponseclasses'));
    
    
    % remove from path
    rmpath(genpath( fullfile(matpath,'optics')) );
    rmpath(genpath( fullfile(atpath,'atmat/pubtools/LatticeTuningFunctions')) );
    rmpath(genpath( fullfile(matpath,'MDT_functions')) );

end

set(0,'DefaultFigurePaperType','A4',...
    'DefaultFigurePaperUnit','centimeters',...
    'DefaultFigurePaperPosition',[2.5 9 16 12]);

disp(['APPHOME is: ' APPHOME]);
disp(['MACHFS is: ' MACHFS]);
disp(['AT cluster tools in: ' oarpath]);
disp(['tango-matlab in: ' tgpath]);
disp(['high level tango-matlab in: ' tgHLpath]);
disp(['TANGO_HOST is: ' getenv('TANGO_HOST')]);
%%
    function optpath(varargin)
        for i=nargin:-1:1
            if (exist(varargin{i},'dir') == 7), addpath(genpath(varargin{i})); end
        end
    end

    function [optval,opts] = getopt(opts,optname,defval)
        ok=[strcmpi(optname,opts(1:end-1)) false];  %option name cannot be the last argument
        if any(ok)
            okval=circshift(ok,[0,1]);
            defval=opts{find(okval,1,'last')};
            opts(ok|okval)=[];
        end
        try
            optval=defval;
        catch
            error('getopt:missing','Option "%s" is missing',optname);
        end
    end

    function p = genpath(d)
        %GENPATH Generate recursive toolbox path.
        %  similar to genpath, but skips hidden directories
        
        if nargin==0
            p = genpath(fullfile(matlabroot,'toolbox'));
            if length(p) > 1, p(end) = []; end % Remove trailing pathsep
            return
        end
        
        % initialise variables
        classsep = '@';  % qualifier for overloaded class directories
        packagesep = '+';  % qualifier for overloaded package directories
        skipc=['.' classsep packagesep];
        skips={'CVS','private','obsolete'};
        p = '';           % path to be returned
        
        files = dir(d);
        if ~isempty(files)
            
            % Add d to the path even if it is empty.
            p = [p d pathsep];
            
            % set logical vector for subdirectory entries in d
            isdir = logical(cat(1,files.isdir));
            %
            % Recursively descend through directories which are neither
            % private nor "class" directories.
            %
            dirs = files(isdir); % select only directory entries from the current listing
            
            for i=1:length(dirs)
                dirname = dirs(i).name;
                if	~(any(dirname(1)==skipc) || any(strcmp(dirname,skips)))
                    p = [p genpath(fullfile(d,dirname))]; %#ok<AGROW> % recursive calling of this function.
                end
            end
        end
    end

end
