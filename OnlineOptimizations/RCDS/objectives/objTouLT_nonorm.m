function obj = objTouLT_nonorm(x,devopt,averagingtime)
%in this function we define the objective function
%you need not to change the first block, which does the parameter boundary
%control and scaling. 
%
%vacuum lifetime is assumed 300h constant
% 

%% do not touch, setting range!
global vrange
Nvar = size(vrange, 1);

if size(x,1)==1
    x = x';
end
p = vrange(:,1) + (vrange(:,2) - vrange(:,1)).*x;

if min(x)<0 || max(x)>1
    dxlim = 0;
    for ii=1:Nvar
        if x(ii)<0
            dxlim = dxlim + abs(x(ii));
        elseif x(ii)>1
            dxlim = dxlim + abs(x(ii)-1);
        end
    end
    
    obj = NaN; %-5 + dxlim^2;
    return;
end

%% evaluate the objective function:
% get the current values. if the change is too large, do it in steps
pact=cellfun(@(d)gettangoval(d),attributenames(devopt))';
maxchange=0.1;

if max(abs(p-pact))>maxchange
   
    [pmax]=max(abs(p-pact));
    
    NP=floor(pmax/maxchange)+1;

    disp(['large excursion, sharing in ' num2str(NP) ' steps'])  
    
    ptemp=pact;
    
%     disp('initial values')
%         disp(pact')  
%     disp('final values')
%         disp(p')  
%     
    for stepset=1:NP
        ptemp=ptemp+(p-pact)./(NP);
        
%         disp(['current values, step ' num2str(stepset) '/' num2str(NP)])
%         disp(ptemp')  
%     
        cellfun(@(d,v)settangoval(d,v),attributenames(devopt),num2cell((ptemp)'));
        
        pause(1);
    end
    
    cellfun(@(d,v)settangoval(d,v),attributenames(devopt),num2cell(p'));
else
    % set sextupoles
    cellfun(@(d,v)settangoval(d,v),attributenames(devopt),num2cell(p'));
end

% wait power supply 
pause(2);

% tauvac=300; % h

% obj=-getTouschekLifetime(tauvac,averagingtime);
obj=-getLifetimeLibera(averagingtime);
%% save data to a global variable. 
global g_data g_cnt
g_cnt = g_cnt+1;
g_data(g_cnt,:) = [p',obj]; %

%uncomment the next line to print out the solution
%     [g_cnt, p',obj]

