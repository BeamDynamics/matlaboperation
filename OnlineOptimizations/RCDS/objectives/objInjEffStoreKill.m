function obj = objInjEffStoreKill(x,averagingtime,storecurrent,devgroup)
%in this function we define the objective function
%you need not to change the first block, which does the parameter boundary
%control and scaling. 
%
%vacuum lifetime is assumed 300h constant
% 
% gets injection efficiency from Tango device, and kill when stored current
% is reached.
% 

%% do not touch, setting range!
global vrange
Nvar = size(vrange, 1);

if size(x,1)==1
    x = x';
end
p = vrange(:,1) + (vrange(:,2) - vrange(:,1)).*x;

if min(x)<0 || max(x)>1
    dxlim = 0;
    for ii=1:Nvar
        if x(ii)<0
            dxlim = dxlim + abs(x(ii));
        elseif x(ii)>1
            dxlim = dxlim + abs(x(ii)-1);
        end
    end
    
    obj = NaN; %-5 + dxlim^2;
    return;
end

%% evaluate the objective function:
 
% set sextupoles
cellfun(@(d,v)settangoval(d,v),attributenames(devgroup),num2cell(p'));

% wait power supply 
pause(3);


% check current limit
 Cur=getTotCurrent();
 
if Cur>storecurrent

    disp('Maximum stored current is reached. shoting vertical kicker')
    % shot vertical kicker to kill beam
    
    kickvert=tango.Device('sr/ps-k/vertical');
    kickvert.dvcmd('On');
    pause(2)
    kickvert.dvcmd('Off');
    pause(10);
    
else
    
    disp([ num2str(Cur) '/' num2str(storecurrent) ' mA']);
    
end
    
obj=-getInjectionEfficiencyTango(averagingtime);

%% save data to a global variable. 
global g_data g_cnt
g_cnt = g_cnt+1;
g_data(g_cnt,:) = [p',obj]; %

%uncomment the next line to print out the solution
%     [g_cnt, p',obj]

