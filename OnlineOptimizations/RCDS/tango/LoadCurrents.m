function LoadCurrents(loadname)
%
% loads currents saved with SaveCurrents(savename,devgroup)
% 
%see also: SaveCurrents

% load 
load(loadname,'currents','devgroup');

% set sextupoles
cellfun(@(d,v)settangoval(d,v),...
    attributenames(devgroup),num2cell(currents));

disp(['Loaded: ' loadname]);

return