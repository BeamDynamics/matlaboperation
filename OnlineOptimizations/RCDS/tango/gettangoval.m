function val=gettangoval(dn)
%
% sets Current attribute in the tango device dn
%
%see also: settangoval devicenames

switch dn
    case {}
        
        val=0;
     
    case {'Sext3Fams'} % relative change with no memory! DeltaCorrectionStrength
    
         val=tango.Attribute(dn).value;
         
        
    otherwise
        
        val=tango.Attribute(dn).value;
        
end

return