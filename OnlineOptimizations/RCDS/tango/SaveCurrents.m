function SaveCurrents(savename,devgroup)
%
% save currents of devices of devgroup
% the devicenames are obtained by devicenames(devgroup)
%
%see also: LoadCurrents gettangoval devicenames

currents=cellfun(@gettangoval,attributenames(devgroup));
save(savename,'currents','devgroup');

return
