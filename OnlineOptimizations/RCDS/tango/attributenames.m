function [names,vrange,cellnamerange]=attributenames(typedevice)
% [names,range]=attributenames(typedevice)
%
%  list attribute names and possible ranges for typedevice:
%  sextcor, SF2cor, skewcor,
%
%see also:

switch typedevice
    
    case 'injsextcor'
        
        % +/- 2 A
        sxt = tango.Attribute('srmag/m-s/all/Strengths');
        
        names = sxt.device.MagnetNames.read;
        
        K = sxt.value;
        
        sel = [1:6,187:192]; % injection sextupoles only
        
        names = names(sel);
        K = K(sel);
        
        magnames={};
        for is =1 :length(names)
            
            magnames = [magnames,{[names{is} '/Strength'];[0.98 1.02]*K(is)}];
        end
        
        cellnamerange=magnames;
        
        
    case 'sextcor'
        
        % +/- 2 A
        sxt = tango.Attribute('srmag/m-s/all/Strengths');
        
        names = sxt.device.MagnetNames.read;
        K = sxt.value;
        magnames={};
        for is =1 :length(names)
            
            magnames = [magnames,{[names{is} '/Strength'];[0.98 1.02]*K(is)}];
        end
        
        cellnamerange=magnames;
        
    case 'SF2cor'
        
        % +/- 2 A
        sxt = tango.Attribute('srmag/m-s/all/Strengths');
        
        names = sxt.device.MagnetNames.read;
        K = sxt.value;
        magnames={};
        indsxt = 1:length(names);
        
        for is = sort(indsxt([2:6:end,5:6:end]))
            
            magnames = [magnames,{[names{is} '/Strength'];[-0.3 +0.3]+K(is)}];
        end
        
        cellnamerange=magnames;
        
    case 'Debug_2Var'
        
        % +/- 2 A
        sxt = tango.Attribute('srmag/m-s/all/Strengths');
        
        names = sxt.device.MagnetNames.read;
        K = sxt.value;
        magnames={};
        indsxt = 1:length(names);
        
        for is = sort(indsxt([2,191]))
            
            magnames = [magnames,{[names{is} '/Strength'];[-0.5 +0.5]+K(is)}];
        end
        
        cellnamerange=magnames;
        
    case 'SF2cor_fast'
        load('/mntdirect/_machfs/MDT/2020/2020_06_28/SextupolesOptimizations/SF2cor_names.mat')
        
    case 'Sext3Fams'
        
        % +/- 2 A
        
        names = {'srmag/m-sd1ae/all/DeltaCorrectionStrength',...
            'srmag/m-sf2/all/DeltaCorrectionStrength',...
            'srmag/m-sd1bd/all/DeltaCorrectionStrength'};
        K=zeros(size(names));
        
        magnames={};
        indsxt = 1:length(names);
        
        for is = sort(indsxt)
            
            magnames = [magnames,{names{is};[-0.5 0.5]}]; % 2 percent
            
        end
        
        cellnamerange=magnames;
        
    case 'quadcor'
        
        % +/- 2 A
        % +/- 2 A
        sxt = tango.Attribute('srmag/m-q/all/Strengths');
        
        names = sxt.device.MagnetNames.read;
        K = sxt.value;
        magnames={};
        indsxt = 1:length(names);
        
        for is = indsxt
            if strcmp(names{is}(1:9),'srmag/m-q')
                
                magnames = [magnames,{[names{is} '/Strength'];[0.98 1.02]*K(is)}];
            end
        end
        
        cellnamerange=magnames;
        
        
        
    case 'QF1cor'
        
        % +/- 2 A
        sxt = tango.Attribute('srmag/m-q/all/Strengths');
        
        names = sxt.device.MagnetNames.read;
        K = sxt.value;
        magnames={};
        indsxt = 1:length(names);
        
        for is = sort(indsxt([1, 19:19:end, 20:19:end, end]))
            
            magnames = [magnames,{[names{is} '/Strength'];[0.98 1.02]*K(is)}];
        end
        
        cellnamerange=magnames;
        
    case 'Quad7Fams'
        
        % +/- 2 A
        
        names = {'srmag/m-qf1/all/DeltaCorrectionStrength',...
            'srmag/m-qd2/all/DeltaCorrectionStrength',...
            'srmag/m-qd3/all/DeltaCorrectionStrength',...
            'srmag/m-qf4/all/DeltaCorrectionStrength',...
            'srmag/m-qd5/all/DeltaCorrectionStrength',...
            'srmag/m-qf6/all/DeltaCorrectionStrength',...
            'srmag/m-qf8/all/DeltaCorrectionStrength'};
        
        K=zeros(size(names));
        
        magnames={};
        indsxt = 1:length(names);
        
        for is = sort(indsxt)
            
            magnames = [magnames,{names{is};[-0.001 0.001]}];
            
        end
        
        cellnamerange=magnames;
        
    case 'quadres'
        
        cellnamerange=resnames;
        
    case 'skewres'
        
        cellnamerange=resnames;
        
    case 'sextres'
        
        
        cellnamerange=resnames;
        
    case 'skewcor'
        
        sxt = tango.Attribute('srmag/sqp/all/CorrectionStrengths');
        
        names = sxt.device.CorrectorNames.read;
        K = sxt.value;
        magnames={};
        indsxt = 1:length(names);
        
        for is = indsxt
            if strcmp(names{is}(1:9),'srmag/sqp')
                
                magnames = [magnames,{[names{is} '/Strength'];[-0.0004 0.0004]}];
            end
        end
        
        cellnamerange=magnames;
        
        
    case 'skewcorinj'
        
        %% skew 1
        sxt = tango.Attribute('srmag/sqp/all/CorrectionStrengths');
        
        names = sxt.device.CorrectorNames.read;
        K = sxt.value;
        magnames={};
        indsxt = [1:9,[-8:0]+length(names)]; % only injection cell skew quadrupoles
        
        for is = indsxt
            if strcmp(names{is}(1:9),'srmag/sqp')
                
                magnames = [magnames,{[names{is} '/Strength'];[-0.0004 0.0004]}];
            end
        end
        
        cellnamerange=magnames;
        
        
        
    case 'injcor'
        injcorfamnames=[...
            {'TL2/PS-C1/ch1';[-10,10]},...
            {'TL2/PS-C1/ch2';[-10,10]},...
            {'TL2/PS-C1/ch3';[-10,10]},...
            {'TL2/PS-C1/ch4';[-10,10]},...
            {'TL2/PS-C1/ch5';[-10,10]},...
            {'TL2/PS-C1/ch6';[-10,10]},...
            {'TL2/PS-C1/ch7';[-10,10]},...
            {'TL2/PS-C1/ch8';[-10,10]},...
            {'TL2/PS-C1/cv0';[-10,10]},...
            {'TL2/PS-C1/cv1';[-10,10]},...
            ...{'TL2/PS-C1/cv2';[-10,10]},...
            {'TL2/PS-C1/cv3';[-10,10]},...
            {'TL2/PS-C1/cv4';[-10,10]},...
            {'TL2/PS-C1/cv5';[-10,10]},...
            {'TL2/PS-C1/cv6';[-10,10]},...
            {'TL2/PS-C1/cv7';[-10,10]},...
            {'TL2/PS-C1/cv8';[-10,10]},...
            {'TL2/PS-C1/cv9';[-10,10]},...
            ];
        cellnamerange=injcorfamnames;
        
    case 'injcorlast2V'
        injcorfamnames=[...
            {'TL2/PS-C1/cv8';[-8,8]},...
            {'TL2/PS-C1/cv9';[-8,8]},...
            ];
        cellnamerange=injcorfamnames;
        
    case 'injcorlast'
        
        [~,~,cellnamerange]=attributenames('injcor');
        cellnamerange=cellnamerange(:,end-4:end);
        
    case 'injquaini'
        injquafamnames=[...
            {'TL2/PS-Q2/qf1';[0,50]},...
            {'TL2/PS-Q2/qd2';[0,50]},...
            {'TL2/PS-Q1/qf3';[0,50]},...
            {'TL2/PS-Q1/qf4';[0,50]},...
            {'TL2/PS-Q1/qd5';[0,50]},...
            {'TL2/PS-Q2/qf6';[0,50]},...
            ];
        cellnamerange=injquafamnames;
        
        
    case 'injqua'
        injquafamnames=[...
            {'TL2/PS-Q2/qf1';[0,50]},...
            {'TL2/PS-Q2/qd2';[0,50]},...
            {'TL2/PS-Q1/qf3';[0,50]},...
            {'TL2/PS-Q1/qf4';[0,50]},...
            {'TL2/PS-Q1/qd5';[0,50]},...
            {'TL2/PS-Q2/qf6';[0,50]},...
            {'TL2/PS-Q2/qf7';[0,50]},...
            {'TL2/PS-Q2/qd8';[0,50]},...
            {'TL2/PS-Q2/qf9';[0,50]},...
            {'TL2/PS-Q2/qd10';[0,50]},...
            {'TL2/PS-Q2/qf11';[0,50]},...
            {'TL2/PS-Q1/qd12';[0,50]},...
            {'TL2/PS-Q1/qf13';[0,50]},...
            {'TL2/PS-Q2/qd14';[0,50]},...
            ];
        cellnamerange=injquafamnames;
        
    case 'injqualast'
        
        [~,~,cellnamerange]=attributenames('injqua');
        cellnamerange=cellnamerange(:,end-4:end);
        
    case 'injsepqualast'
        [~,~,cnr1]=attributenames('injsep');
        [~,~,cnr2]=attributenames('injqualast');
        
        cellnamerange=[cnr1 cnr2];
        
    case 'injsep'
        injsepfamnames=[...
            {'SR/PS-si/12';[8620 9180]},...
            {'SR/PS-si/3';[5800 6600]},...
            ];
        cellnamerange=injsepfamnames;
        
    case 'injkic'
        injkicfamnames=[...
            {'SR/PS-K1/1';[0,0]},...
            {'SR/PS-K2/2';[0,0]},...
            {'SR/PS-K3/3';[0,0]},...
            {'SR/PS-K4/4';[0,0]},...
            ];
        cellnamerange=injkicfamnames;
        
    case 'allinj'
        
        [~,~,cnr1]=attributenames('injcor');
        [~,~,cnr2]=attributenames('injqua');
        [~,~,cnr3]=attributenames('ixnjsep');
        
        cellnamerange=[cnr1 cnr2 cnr3];
        
    case 'allinjlast'
        
        [~,~,cnr1]=attributenames('injcorlast');
        [~,~,cnr2]=attributenames('injqualast');
        [~,~,cnr3]=attributenames('injsep');
        
        cellnamerange=[cnr1 cnr2 cnr3];
        
    case 'injXYXpYp'
        
        [~,~,cnr1]=attributenames('injcorlast2V');
        [~,~,cnr2]=attributenames('injsep');
        
        cellnamerange=[cnr1 cnr2];
        
        
    otherwise
        
        disp(typedevice)
        disp('does not exist!')
        disp('typedevice may be: ')
        disp('sextcor, CORRECTORS')
        disp('skewcor, Skew Correctors')
        disp('sextfam, SEXTUPOLE main Family and independent PS, S19 S20 excluded')
        disp('sextall, sextfam+sextcor')
        disp('sextchrom, S19 S20')
        disp('sextfamchrom, sextfam+sextchrom')
        disp('quadfam')
        disp('quadHG')
        disp(['injcor, injqua, injsep, injkic, allinj '...
            'injcorlast, injqualast, allinjlast, injXYXpYp '])
end

vrange=reshape([cellnamerange{2,:}],2,[]);
names=cellnamerange(1,:);

return










