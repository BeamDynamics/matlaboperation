function settangoval(dn,val)

switch dn
    
    case {}
        
    case [attributenames('Sext3Fams') attributenames('Quad7Fams')] % relative change with no memory! DeltaCorrectionStrength
        
        val0=tango.Attribute(dn).value;
        
        dd=tango.Attribute(dn);
        
        dd.set = -val0; % cancel previous delta change
        pause(5);
        dd.set = val; % set new delta change
        
    otherwise
        
        dd=tango.Attribute(dn);
        
        dd.set = val;
        
end

return