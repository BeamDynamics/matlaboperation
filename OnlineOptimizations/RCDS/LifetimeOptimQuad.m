function dfname=LifetimeOptimQuad(varargin) %devopt,devsave,TLTorBLM)
%function dfname=LifetimeOptim(varargin) %devopt,devsave,TLTorBLM)
%
% 'OptQuadrupoles'  = 'quadcor'(default) ,'Quad7Fams'
% 'SaveQuadrupoles' = 'quadcor'(default)  
% 'OptObjective'   = 'TLT','BLM'(default)
% 'ObjAverage'     = integer (default: 1) (averaging of OptObjective)
% 'NIter'          = integer (default: 4)
% 'NoiseEval'      = true/false (default: false, no evalutation)
% 'FixedTune'     = true/false (default: false, not fixed tunes)
%
% all outputs are stored in:
% /machfs/MDT/YYYY/YYYY_MM_DD/QuadrupoleOptimization
%
%  
% example 1): all quad correctors, opt. BLM with fixed tunes for 2 iter.
% >> LifetimeOptimQuad
%
% example 2): some quad., opt Libera Lifetime for 1 iter. free tunes
% >> LifetimeOptimQuad('OptQuadrupole','quadcor8b',...
%                  'OptObjective','TLT',...
%                  'NIter',1,...
%                  'FixedTune',false)
%,
% matlab path must contain (added in the function):
% addpath(genpath('/mntdirect/_machfs/BeamDynamics/LifeTimeOptimizer'))
% addpath(genpath('/mntdirect/_machfs/BeamDynamics/RCDS'))
% 
%see also: attributenames powellmain objTouLT_AnyMode objBLM_AnyMode
%          FixedTuneQuadrupoleDirections

tic;
global APPHOME


% parse inputs
p = inputParser;

defaultQuadOpt='Quad7Fams';
defaultQuadSave='quadcor';
defaultObjctive='TLT';
defaultAvarage=1;
defaultNIterations=2;
defaultNoiseLevelEvaluation=false;
defaultFixedTune=false;

expectedSextupoleGroups={'quadcor', 'Quad7Fams' };
expectedSextupoleGroupsSave={'quadcor'};
expectedObjectives={'TLT','BLM'};

addOptional(p,'OptQuadrupoles',defaultQuadOpt,@(x) any(validatestring(x,expectedSextupoleGroups)));
addOptional(p,'SaveQuadrupoles',defaultQuadSave,@(x) any(validatestring(x,expectedSextupoleGroupsSave)));
addOptional(p,'OptObjective',defaultObjctive,@(x) any(validatestring(x,expectedObjectives)));
addParameter(p,'ObjAverage',defaultAvarage,@isnumeric);
addParameter(p,'NIter',defaultNIterations,@isnumeric);
addParameter(p,'NoiseEval',defaultNoiseLevelEvaluation);
addParameter(p,'FixedTune',defaultFixedTune);

parse(p,varargin{:});

devopt        = p.Results.OptQuadrupoles;
devsave       = p.Results.SaveQuadrupoles;
TLTorBLM      = p.Results.OptObjective;
averagingtime = p.Results.ObjAverage;
Nit           = floor(p.Results.NIter); % set to 3 when working correctly!
NoiseEval     = p.Results.NoiseEval;
FixedTune    = p.Results.FixedTune;

curdir=pwd;

%% define objective function
switch TLTorBLM
    case 'TLT'
        %averagingtime=5;
        objfun=@(xx)objTouLT_AnyMode(xx,devopt,averagingtime);
        noiseval=0.8; % noise evaluated on 50 measurements with this averaging time.
    
    case 'BLM'
        % error('update averaging time and noiseval for NewBLM reading')
        %averagingtime=5;
        objfun=@(xx)objBLM_AnyMode(xx,devopt,averagingtime);
        noiseval=4000; % noise evaluated on 50 measurements with this averaging time.
end

%% define sub working folder
nowlabel=datestr(now,'yymmdd_HHMMSS');

dfname =[devsave '_' nowlabel];
wdirname=fullfile(APPHOME, 'MDT',datestr(now,'YYYY'),...
    datestr(now,'YYYY_mm_DD'),...
    'QuadrupoleOptimizations');
mkdir(wdirname);
cd(wdirname);

%% save initial currents in the sextupoles
disp(['Save initial ' devsave ' set.']);
SaveCurrents([dfname '_Init'],devsave); % all, familiy and chrom. cor family

%% get initial currents

[alldevnames,alldevranges]=attributenames(devopt);
currents_initial=cellfun(@gettangoval,alldevnames);

%% set range
disp(['Define ' devopt ' current ranges.']);

global vrange Nvar

Nobj = 1;
 Nvar = length(currents_initial);
 
% if ~FixedTune
%     Nvar = length(currents_initial);
% else
%     Nvar = length(currents_initial) -2;
% end


% limit to full range in attributenames
range_min=alldevranges(1,:)';
range_max=alldevranges(2,:)';

vrange=[range_min, range_max];

%initial solution, in this is the current set
p0 = currents_initial;

x0 = (p0'-vrange(:,1))./(vrange(:,2)-vrange(:,1));

global g_cnt g_data
g_data=[];
g_cnt = 0;

LTinit=objfun(x0);

g_data=[];
g_cnt = 0;

%% evaluate the random noise level of the objective function
disp(['Averageing time: ' num2str(averagingtime)])

global g_noise;
g_noise = noiseval;

% no need to evaluate every time
if NoiseEval
    disp('Update noise with 50 objective function evaluations')
    for ii=1:50
        tmp_data(ii) = objfun(x0);
        disp([ii tmp_data(ii)])
    end
    g_noise = std(tmp_data);
end

%% perform optimization
disp(['Start optimization of ' num2str(Nvar) ' ' devopt ' .']);

diary([dfname '_diary.txt']);

%use unit vectors as initial direction set
if FixedTune & strcmp(devopt,'Quad7Fams')
    warning('not possible to fix tunes for this optimization')
    FixedTune = false;
end


if FixedTune
    
    a = load('/operation/beamdyn/matlab/optics/ebs/theory/betamodel.mat');
    FixTuneDir=FixedTuneQuadrupoleDirections(a.betamodel,devopt);
    % use twice the first 2 directions, to get an Nvar X Nvar matrix
    dmat = [FixTuneDir(:,[1:end,1,2])];
    %dmat = FixTuneDir;
    
    disp('------------------------------')
    disp('---      fixed tunes       ---')
    disp('------------------------------')
else
    dmat = eye(Nvar);
end

step = 0.01;
g_cnt = 0;
[x1,f1,nf]=powellmain(objfun,x0,step,dmat,0,Nit,'noplot',1000*Nit);

% save all data, global variables included
save([dfname '_Data.mat']);

%% get best value achieved
disp('Processing Optimization Data: find best value achieved.');
%[data_1,xm,fm] = process_scandata(g_data,Nvar,vrange,'plot');
[data_1,xm,fm] = process_scandata(g_data,length(vrange),vrange,'plot');

saveas(gca,[dfname '_Plot.fig']);

% set best value achieved
disp(['Set best ' devsave ' achieved.']);
LTbest1=objfun(xm);
pause(1);

SaveCurrents([dfname '_Best'],devsave); % gets currents from magnets!

diary off;

%% keep or reject solution
prompt = 'Keep optimization results? if not initial setting will be restored (Y/N) [Y]';
str=input(prompt,'s');
if isempty(str)
    str = 'Y';
end

if str == 'Y'
    
    SaveCurrents([dfname '_Best'],'quadcor'); % gets currents from magnets!
    
else
    
    disp('Revert to intial quadrupole set');
    LoadCurrents([dfname '_Init']); % sets currents 

end

%% plot sextupole currents before and after optimization
a=load([dfname '_Init'],'currents');
b=load([dfname '_Best'],'currents');

figure;
bar([a.currents;b.currents]');
ax=gca;
ax.XTickLabel=attributenames(devopt);
ax.XTickLabelRotation=45;
ax.YLabel.String='current [A]';
legend(['before obj: ' num2str(LTinit)],['after obj: ' num2str(LTbest1)]);

saveas(gca,[dfname '_PlotQuad.fig']);


%% close up
cd(curdir);

disp('Finished')
toc
end