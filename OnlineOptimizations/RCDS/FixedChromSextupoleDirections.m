function [FixChromDir,ModChromDir,indsextfams]=FixedChromSextupoleDirections(r,devopt)
% compute fixed chromaticity sextupole bases

% get sextupole families

attrname=attributenames(devopt);

indsextfams={};
tangoname={};
coridxlist={};

%ind_dev = atgetcells(r,'Device'); % list magnest with device field
%latdevnames = atgetfieldvalues(r,ind_dev,'Device'); % get device names

for is=1:length(attrname)
    sel=[];
    coridx=[];

    [devname,~]=fileparts(attrname{is});  %get device name from Attribute name (first part)
    
    % get magnet indexes, independend of intial lattice
    indmag=find(atgetcells(r,'Device',devname))';
    if isempty(sel)
        sel=1:length(indmag); 
    end
    
    indsextfams{is}=indmag(sel);
    tangoname{is}=devname;
    coridxlist{is}=coridx;
end

BRho=1/299792458*6.0e9;
delta = 0.001;% Ampere
% get k variation as 0.1% of present strenght
for is=1:length(attrname)
    
    k = tango.Attribute(attrname{is}).value; 
    deltak{is}=k(1)*delta;
    
end

% % fixed k variation for all magnets
% delta=1e-4;
% for is=1:length(indsextfams)
%     deltak{is}=delta;
% end


% is=1;     
% indsextfams{is}=atgetcells(r,'FamName','S4');is=is+1;
% indsextfams{is}=atgetcells(r,'FamName','S6');is=is+1;
% indsextfams{is}=atgetcells(r,'FamName','S13');is=is+1;
% indsextfams{is}=atgetcells(r,'FamName','S19');is=is+1;
% indsextfams{is}=atgetcells(r,'FamName','S20');is=is+1;
% indsextfams{is}=atgetcells(r,'FamName','S22');is=is+1;
% indsextfams{is}=atgetcells(r,'FamName','S24');is=is+1;

% build chromaticity response with required sextupoles
for inds=1:length(indsextfams)
    dc(inds,:)=sextresp(r,indsextfams{inds},deltak{inds});
end

[u,~,~]=svd(dc);
FixChromDir=u(:,3:end); % remove first 2 eigenvectors, acting on chromaticity
ModChromDir=u(:,[1,2]);

end

% [u,s,v]=svd(dc,0);
% lambda=diag(s);
% nmax=length(lambda);
% eigsorb=u'*b;
% if neig > nmax
%     neig=nmax;
%     warning('Svd:maxsize',['number of vectors limited to ' num2str(nmax)]);
% end
% eigscor=eigsorb(1:neig)./lambda(1:neig);
% dq=v(:,1:neig)*eigscor;
% 
% 
% end

function dc=sextresp(r,ind,deltak)
% compute response of sextupoles to chromaticity

r{ind(1)}.FamName

[~,~,c0] = atlinopt(r,0,1);

k = atgetfieldvalues(r,ind,'PolynomB',{1,3});

rs = atsetfieldvalues(r,ind,'PolynomB',{1,3},k+deltak);
[~,~,c] = atlinopt(rs,0,1);

dc=(c-c0)./deltak;

end
