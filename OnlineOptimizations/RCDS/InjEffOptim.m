function dfname=InjEffOptim(varargin) %devopt,devsave,TLTorBLM)
%function dfname=InjEffOptim(varargin) %devopt,devsave,TLTorBLM)
%
% 'OptMagnets'  = 'injXYXpYp' (default) 'injsep','allinjlast', 'allinj','injsepqualast','injqua','injqualast','injquaini','injcorlast2V'
% 'SaveMagnets' = 'injXYXpYp'(default) 'injsep','allinjlast', 'allinj','injsepqualast','injqua','injqualast','injquaini','injcorlast2V'
% 'OptObjective'   = 'InjEffSpark' (default)
% 'ObjAverage'     = integer (default: 1) (averaging of OptObjective)
% 'NIter'          = integer (default: 4)
% 'NoiseEval'      = true/false (default: false, no evalutation)
%
% all outputs are stored in:
% /machfs/BeamDynamics/InjEfficiencyOptimizations/Year/Date/
%
% example 1): , opt. for 4 iter.
% >> InjEffOptim
%
% example 2): 
%
% matlab path must contain (added in the function):
% addpath(genpath('/mntdirect/_machfs/BeamDynamics/InjectionEfficiency'))
% addpath(genpath('/mntdirect/_machfs/BeamDynamics/RCDS'))
% 
%see also: attributenames powellmain 

tic;

% parse inputs
p = inputParser;

defaultSextOpt='injXYXpYp';
defaultSextSave='injXYXpYp';
defaultObjctive='InjEffSpark';
defaultAvarage=1;
defaultNIterations=4;
defaultNoiseLevelEvaluation=false;


expectedSextupoleGroups={'injXYXpYp','injsep','allinjlast','allinj','injsepqualast','injqua','injqualast','injquaini','injcorlast2V'};
expectedSextupoleGroupsSave={'injXYXpYp','injsep','allinjlast','allinj','injsepqualast','injqua','injqualast','injquaini','injcorlast2V'};
expectedObjectives={'InjEffSpark',''};

addOptional(p,'OptMagnets',defaultSextOpt,@(x) any(validatestring(x,expectedSextupoleGroups)));
addOptional(p,'SaveMagnets',defaultSextSave,@(x) any(validatestring(x,expectedSextupoleGroupsSave)));
addOptional(p,'OptObjective',defaultObjctive,@(x) any(validatestring(x,expectedObjectives)));
addParameter(p,'ObjAverage',defaultAvarage,@isinteger);
addParameter(p,'NIter',defaultNIterations,@isinteger);
addParameter(p,'NoiseEval',defaultNoiseLevelEvaluation);

parse(p,varargin{:});

devopt        = p.Results.OptMagnets;
devsave       = p.Results.SaveMagnets;
InjEffMeas    = p.Results.OptObjective;
averagingtime = p.Results.ObjAverage;
Nit           = p.Results.NIter; % set to 3 when working correctly!
NoiseEval      = p.Results.NoiseEval;

curdir=pwd;

%% define objective function 
switch InjEffMeas 
    case 'InjEffSpark'
        MaxStoreCurrent=210;
        
        injeff=py.sr.injeff.InjEffDevice('sr/d-sbstreff/sy-sr');
        % ke=py.sr.injeff.TrigTest()
        ke=py.pymach.tango.Device('sy/ps-ke/1');

        IE=getInjectionEfficiencyTangoPy(averagingtime,injeff,ke);

        disp(['initial IE (' num2str(averagingtime) '): ' num2str(IE)]);

        objfun=@(xx)objInjEffStoreKillInjEffSpark(xx,averagingtime,MaxStoreCurrent,devopt,injeff,ke);
        noiseval=1.0;
    case ''
        error('no injection efficiency objective function')
end

%% define sub working folder
nowlabel=datestr(now,'yymmdd_HHMMSS');

dfname =[devsave '_' nowlabel];
wdirname=fullfile(['/mntdirect/_machfs/BeamDynamics/InjEfficiencyOptimizations'],...
    datestr(now,'YYYY'),...
    datestr(now,'mmmdd'));
mkdir(wdirname);
cd(wdirname);

%% save initial currents in the sextupoles
disp(['Save initial ' devsave ' set.']);
SaveCurrents([dfname '_Init'],devsave); % all, familiy and chrom. cor family

%% get initial currents 

[alldevnames,alldevranges]=attributenames(devopt);
currents_initial=cellfun(@gettangoval,alldevnames);

%% set range
disp(['Define ' devopt ' current ranges.']);

global vrange Nvar

Nobj = 1;
Nvar = length(currents_initial);

% limit to full range in attributenames
range_min=alldevranges(1,:)';
range_max=alldevranges(2,:)';

vrange=[range_min, range_max];

%initial solution, in this is the current set
p0 = currents_initial; 

x0 = (p0'-vrange(:,1))./(vrange(:,2)-vrange(:,1));

global g_cnt g_data
g_data=[];
g_cnt = 0;

IEinit=objfun(x0);

g_data=[];
g_cnt = 0;

%% evaluate the random noise level of the objective function
disp(['Averageing time: ' num2str(averagingtime)])

global g_noise;
g_noise = noiseval;

% no need to evaluate every time
if NoiseEval
    noise_data=nan(20,1);
    disp('Update noise with 20 objective function evaluations')
    for ii=1:20
        noise_data(ii) = objfun(x0);
        disp([ii noise_data(ii)])
    end
    g_noise = std(noise_data);
    disp(g_noise)
    save([dfname '_noisedata'],'noise_data');
    cd(curdir)
    return
end


%% perform optimization
disp(['Start optimization of ' num2str(Nvar) ' ' devopt ' .']);

diary([dfname '_diary.txt']);

%use unit vectors as initial direction set
dmat = eye(Nvar);

step = 0.001;
g_cnt = 0;
[x1,f1,nf]=powellmain(objfun,x0,step,dmat,0,Nit,'noplot',5000);

% save all data, global variables included
save([dfname '_Data.mat']);

%% get best value achieved
disp('Processing Optimization Data: find best value achieved.');
[data_1,xm,fm] = process_scandata(g_data,Nvar,vrange,'plot');

saveas(gca,[dfname '_Plot.fig']);

% set best value achieved
disp(['Set best ' devsave ' achieved.']);
LTbest1=objfun(xm);
pause(1);

SaveCurrents([dfname '_Best'],devsave); % gets currents from magnets!

diary off;



%% keep or reject solution
prompt = 'Keep optimization results? if not initial setting will be restored (Y/N) [Y]';
str=input(prompt,'s');
if isempty(str)
    str = 'Y';
end

if str == 'Y'
    
else
    
    disp('Revert to intial magnet set');
    LoadCurrents([dfname '_Init']); % sets currents 

end

%% plot sextupole currents before and after optimization
a=load([dfname '_Init'],'currents');
b=load([dfname '_Best'],'currents');

figure;
bar([a.currents;b.currents]');
ax=gca;
ax.XTickLabel=attributenames(devopt);
ax.XTickLabelRotation=45;
ax.YLabel.String='current [A]';
%legend(['before obj: ' num2str()],['after obj: ' num2str()]);

saveas(gca,[dfname '_PlotMagInjEff.fig']);


%% close up
cd(curdir);

disp('Finished')
toc

end
