function dfname=LifetimeOptim(varargin) %devopt,devsave,TLTorBLM)
%function dfname=LifetimeOptim(varargin) %devopt,devsave,TLTorBLM)
%
% 'OptSextupoles'  = 'sextcor', 'Sext3Fams' (default), 'injsextcor'
% 'SaveSextupoles' = 'sextcor'(default)
% 'OptObjective'   = 'TLT','BLM'(default)
% 'ObjAverage'     = integer (default: 1) (averaging of OptObjective)
% 'NIter'          = integer (default: 4)
% 'NoiseEval'      = true/false (default: false, no evalutation)
% 'FixedChrom'     = true/false (default: false, not fixed chromaticity)
%
% all outputs are stored in:
% /machfs/BeamDynamics/SextupolesOptimization/Year/Date/
%
% example 1): S20 sext., opt. BLM with fixed chromaticity for 4 iter.
% >> LifetimeOptim
%
% example 2): all sext., opt Libera Lifetime for 1 iter. free chromaticity
% >> LifetimeOptim('OptSextupoles','sextcor',...
%                  'OptObjective','TLT',...
%                  'NIter',1,...
%                  'FixedChrom',false)
%
% matlab path must contain (added in the function):
% addpath(genpath('/mntdirect/_machfs/BeamDynamics/LifeTimeOptimizer'))
% addpath(genpath('/mntdirect/_machfs/BeamDynamics/RCDS'))
% 
% updated:
% set resonance device Offset (basefile equivalent) (S.L.) 
%
%see also: attributenames powellmain objTouLT_AnyMode objBLM_AnyMode
%          FixedChromSextupoleDirections


tic;
global MACHFS

% set path to required functions

% parse inputs
p = inputParser;

defaultSextOpt='Sext3Fams';
defaultSextSave='sextcor';
defaultObjctive='TLT';
defaultAvarage=1;
defaultNIterations=4;
defaultNoiseLevelEvaluation=false;
defaultFixedChrom=false;

expectedSextupoleGroups={'sextcor','Sext3Fams','SF2cor','SF2cor_fast','Debug_2Var'};
expectedSextupoleGroupsSave={'sextcor'};
expectedObjectives={'TLT','BLM','TLT_nonorm','BLM_nonorm'};

addOptional(p,'OptSextupoles',defaultSextOpt,@(x) any(validatestring(x,expectedSextupoleGroups)));
addOptional(p,'SaveSextupoles',defaultSextSave,@(x) any(validatestring(x,expectedSextupoleGroupsSave)));
addOptional(p,'OptObjective',defaultObjctive,@(x) any(validatestring(x,expectedObjectives)));
addParameter(p,'ObjAverage',defaultAvarage,@isinteger);
addParameter(p,'NIter',defaultNIterations,@isinteger);
addParameter(p,'NoiseEval',defaultNoiseLevelEvaluation);
addParameter(p,'FixedChrom',defaultFixedChrom);

parse(p,varargin{:});

devopt        = p.Results.OptSextupoles;
devsave       = p.Results.SaveSextupoles;
TLTorBLM      = p.Results.OptObjective;
averagingtime = p.Results.ObjAverage;
Nit           = p.Results.NIter; % set to 3 when working correctly!
NoiseEval     = p.Results.NoiseEval;
FixedChrom    = p.Results.FixedChrom;

curdir=pwd;

%% define objective function
switch TLTorBLM
    case 'TLT'
        %averagingtime=5;
        objfun=@(xx)objTouLT_AnyMode(xx,devopt,averagingtime);
        noiseval=0.8; % noise evaluated on 50 measurements with this averaging time.
    case 'BLM'
        % error('update averaging time and noiseval for NewBLM reading')
        %averagingtime=5;
        objfun=@(xx)objBLM_AnyMode(xx,devopt,averagingtime);
        noiseval=4000; % noise evaluated on 50 measurements with this averaging time.
    case 'BLM_nonorm'
        % error('update averaging time and noiseval for NewBLM reading')
        %averagingtime=5;
        objfun=@(xx)objBLM_nonorm(xx,devopt,averagingtime);
        noiseval=400; % noise evaluated on 50 measurements with this averaging time.
    case 'TLT_nonorm'
        %averagingtime=5;
        objfun=@(xx)objTouLT_nonorm(xx,devopt,averagingtime);
        noiseval=0.001; % noise evaluated on 50 measurements with this averaging time.
end

%% define sub working folder
nowlabel=datestr(now,'yymmdd_HHMMSS');

dfname =[devsave '_' nowlabel];

% wdirname=fullfile([MACHFS '/MDT/' datestr(now,'YYYY') '/' datestr(now,'YYYY_mm_DD') '/SextupolesOptimizations']);
wdirname=fullfile('./SextupolesOptimizations');

mkdir(wdirname);
cd(wdirname);

%% save initial currents in the sextupoles
disp(['Save initial ' devsave ' set.']);
SaveCurrents([dfname '_Init'],devsave); % all, familiy and chrom. cor family

%% get initial currents

[alldevnames,alldevranges]=attributenames(devopt);
currents_initial=cellfun(@gettangoval,alldevnames);

%% set range
disp(['Define ' devopt ' current ranges.']);

global vrange Nvar

Nobj = 1;
Nvar = length(currents_initial);

% limit to full range in attributenames
range_min=alldevranges(1,:)';
range_max=alldevranges(2,:)';

vrange=[range_min, range_max];

%initial solution, in this is the current set
p0 = currents_initial;

x0 = (p0'-vrange(:,1))./(vrange(:,2)-vrange(:,1));

global g_cnt g_data
g_data=[];
g_cnt = 0;

LTinit=objfun(x0);

g_data=[];
g_cnt = 0;

%% evaluate the random noise level of the objective function
disp(['Averageing time: ' num2str(averagingtime)])

global g_noise;
g_noise = noiseval;

% no need to evaluate every time
if NoiseEval
    disp('Update noise with 50 objective function evaluations')
    for ii=1:50
        tmp_data(ii) = objfun(x0);
        disp([ii tmp_data(ii)])
    end
    g_noise = std(tmp_data);
end

%% perform optimization
disp(['Start optimization of ' num2str(Nvar) ' ' devopt ' .']);

diary([dfname '_diary.txt']);

%use unit vectors as initial direction set
if FixedChrom & strcmp(devopt,'Sext3Fams')
        warning('not possible to fix chromaticity for this optimization')
        FixedChrom = false;
end

if FixedChrom
    
    a=load('/operation/beamdyn/matlab/optics/ebs/theory/betamodel.mat');
    FixChromDir=FixedChromSextupoleDirections(a.ring,devopt);
    % use twice the first 2 directions, to get an Nvar X Nvar matrix
    dmat = [FixChromDir(:,[1:end,1,2])];
    
    disp('------------------------------')
    disp('---   fixed chromaticity   ---')
    disp('------------------------------')
else
    dmat = eye(Nvar);
end

step = 0.1;
g_cnt = 0;
[x1,f1,nf]=powellmain(objfun,x0,step,dmat,0,Nit,'noplot',1000*Nit);

% save all data, global variables included
save([dfname '_Data.mat']);

%% get best value achieved
disp('Processing Optimization Data: find best value achieved.');
[data_1,xm,fm] = process_scandata(g_data,Nvar,vrange,'plot');

saveas(gca,[dfname '_Plot.fig']);

% set best value achieved
disp(['Set best ' devsave ' achieved.']);
LTbest1=objfun(xm);
pause(1);

SaveCurrents([dfname '_Best'],devsave); % gets currents from magnets!

diary off;

%% keep or reject solution
prompt = 'Keep optimization results? if not initial setting will be restored (Y/N) [Y]';
str=input(prompt,'s');
if isempty(str)
    str = 'Y';
end

if str == 'Y'
    
    SaveCurrents([dfname '_ForBaseFile'],'sextcor'); % gets currents from magnets!
    
else
    
    disp('Revert to intial sextupole set');
    LoadCurrents([dfname '_Init']); % sets currents 

end

%% plot sextupole currents before and after optimization
a=load([dfname '_Init'],'currents');
b=load([dfname '_Best'],'currents');

figure;
bar([a.currents;b.currents]');
ax=gca;
ax.XTickLabel=attributenames(devopt);
ax.XTickLabelRotation=45;
ax.YLabel.String='current [A]';
legend(['before obj: ' num2str(LTinit)],['after obj: ' num2str(LTbest1)]);

saveas(gca,[dfname '_PlotSext.fig']);


%% close up
cd(curdir);

disp('Finished')
toc
end