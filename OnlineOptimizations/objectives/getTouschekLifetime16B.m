function LT_corrected=getTouschekLifetime16B(tauvac,averagingtime)
%LT_corrected=getTouchekLifetime78MB(tauvac,averagingtime)
%
% corrects measured lifetime of 7/8 multibuynch considering current in
% single bunch, lifetime of single bunch and current decay 
% 
%
% tauvac= vacuum lifetime in [h]
% averagingtime= time to average lifetime measurement [s]
% 
%see also: getLifetimeLibera, getZEmittance, getTotCurrent, BunchLength
nbunch=16;


% get  lifetime
LT0 = getLifetimeLibera(averagingtime); % 

% correct for current decay (rescale to 1mA/bunch)
Itot=getTotCurrent()/1000;
ey=getZEmittance(1); % 10 waits 1 second and makes 10 averages

% normalization values
IbNorm=(0.023)/nbunch; % mA  92 mA at full current
eyNorm=0.005;        % nm  5 pm

% bunch length parameters
Zn=.7;
Vrf=9.0e6; % reference voltage
U0=4.88e6;
E0=6.04e9;
alpha=1.78e-4;
sigdelta=1.06e-3;
circ=844.039;
h=992;
Ib=Itot/nbunch;

BL = BunchLength(Ib,Zn,Vrf,U0,E0,h,alpha,sigdelta,circ);
BLNorm = BunchLength(IbNorm,Zn,Vrf,U0,E0,h,alpha,sigdelta,circ);

LT_tou=(Itot)/(Itot/LT0-Itot/tauvac);

% normalize to current, bunch length, emittance
LT_tou_cor = LT_tou * (Ib/IbNorm) * (BLNorm/BL) * sqrt(eyNorm/ey);

LT_corrected=LT_tou_cor;

return
