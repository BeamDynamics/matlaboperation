function dfname=TouschekOptSextCor78MB(varargin)
%TouschekOptSextCor16B(commandstring)
% 
%  TouschekOptSextCor16B('RUN'): 
%        runs optimization for 5 iterations. 
%  TouschekOptSextCor16B('RUN','10'): 
%        runs optimization for numiterations STRING.   
%  TouschekOptSextCor16B('SAVE',filenamestring): 
%        save sextupole corrector currents.   
%  TouschekOptSextCor16B('LOAD',filenamestring): 
%        LOAD sextupole corrector currents.   
%  TouschekOptSextCor16B('SetCor2Zero'): 
%        set sextupole corrector currents all to  zero.   
%
%see also: Run_TouschekOptSextCor16B, 
%          SaveCurrentSextCor, LoadCurrentSextCor, settangoval

commandstring=varargin;

if isempty(commandstring)
    help('TouschekOptSextCor78MB.m');
    return
end

switch commandstring{1}
    
    case {'RUN','run','Run'} % run for given time
        if isempty(commandstring{2})
            time2run=5; % iterations
        else
            time2run=str2double(commandstring{2});
        end
        tic;
        
        % runs form surrent state for time2run, 
        % gets best results, set best result
        dfname=Run_TouschekOptSextCor78MB(time2run);
        
        toc;
        
        % check lifetimes
         %LoadCurrentSextCor([dfname '_Init.mat']);pause(10);
         LTinit=-99;%getTouschekLifetime78MB(300,20);disp('Init');disp(LTinit);
        LoadCurrentSextCor([dfname '_Best.mat']);pause(10);
        LTbest1=getTouschekLifetime78MB(300,20);disp('best1');disp(LTbest1);
%         LoadCurrentSextCor([dfname '_Best.mat']);pause(10);
%         LTbest2=getTouschekLifetime78MB(300,20);disp('best2');disp(LTbest2);
%         TouschekOptSextCor16B('SetCor2Zero');pause(10);
%         LTzero=getTouschekLifetime78MB(300,20);disp('zero');disp(LTzero);
%         
        % back to inital set!
        %LoadCurrentSextCor([dfname '_Init.mat']);
        
        save([dfname '_OptCheck'],'LTinit','LTbest1');%,'LTbest2','LTzero'
        
        % plot correctors strengths
        load([dfname '_Init.mat'],'sextcorcurrents');
        sxtin=sextcorcurrents;
        load([dfname '_Best.mat'],'sextcorcurrents');
        sxtbe=sextcorcurrents;
        figure;bar([sxtin;sxtbe]'); 
        legend(['initial LT: ' num2str(LTinit) ' h'],...
            ['best LT:' num2str(LTbest1) ' h' ],...
            'Location','NorthWest');
        ax=gca;
        ax.XTickLabel=devicenames('sextcor');
        ax.XTickLabelRotation=45;
        ylabel('current [A]');
        saveas(gca,[dfname '_SextCorChanges.fig']);
        
        toc;
        
    case {'SAVE','Save','save'}
        savename=commandstring{2};
        SaveCurrentSextCor(savename);
        
    case {'LOAD','Load','load'}
        loadname=commandstring{2};
        LoadCurrentSextCor(loadname);
        
    case {'SetCor2Zero'}
        cellfun(@(d,v)settangoval(d,v),devicenames('sextcor'),...
            num2cell(zeros(1,12)));

end


return