function dfname=Run_TouschekOptSextCor16B(time2run)
%Run_TouschekOptSextCor16B(time2run)
%
% - run optimizer for time time2run (in minutes, APPROXIMATE!)
% - exit if too long time or 100 iterations
% - get best result achieved
% - set best result
% - save best results to file
%
%see also: gettangoval, powellmain
disp('Save initial sextupole set.');

dfname =['sextcor_' datestr(now,'yymmdd_HHMMSS')];

% save initial currents in the sextupoles
SaveCurrentSextCor([dfname '_Init'])

%% get and 
sextcurrents_initial=cellfun(@gettangoval,devicenames('sextcor'));

%disp('start from zero!')
%sextcurrents_initial=sextcurrents_initial*0;% start from zero!

%% set range and normalize variables
disp('define sextupole current ranges.');

global vrange Nvar

Nobj = 1;
Nvar = length(sextcurrents_initial);

% limit current +/- 2A

vrange = [-ones(Nvar,1), ones(Nvar,1)]*1.9 ;

%initial solution, in this is the current set
p0 = sextcurrents_initial; 

x0 = (p0'-vrange(:,1))./(vrange(:,2)-vrange(:,1));

global g_cnt g_data
g_data=[];
g_cnt = 0;

%% 
averagingtime=3; % seconds to averge lifetime

%% evaluate the random noise level of the objective function
% no need to evaluate every time 
% for ii=1:50
%    tmp_data(ii) = getTouschekLifetime16B(300,averagingtime); 
%    disp([ii tmp_data(ii)])
% end

global g_noise;
g_noise = 0.02;  
%g_noise = std(tmp_data);  

disp('start optimization.');

%% run optimizer
t0=tic;

diary([dfname '_diary.txt']);

%use unit vectors as initial direction set
dmat = eye(Nvar);

%evalfunlimit=floor((time2run*60)/(6*(averagingtime+7)));
%disp(['In ' num2str(time2run) ' min, number of evaluations is: ' num2str(evalfunlimit) ' times']);


step = 0.1;
g_cnt = 0;
[x1,f1,nf]=powellmain(...
    @(xx)objTouLT_16B(xx,averagingtime),...
    x0,step,dmat,0,time2run,'noplot',5000);

%%
save([dfname '_Data.mat']);

% get best value achieved
disp('Processing Optimization Data: find best value achieved.');
[data_1,xm,fm] = process_scandata(g_data,Nvar,vrange,'plot');

[xm(:)', fm]

saveas(gca,[dfname '_Plot.fig']);

% set best value achieved
disp('Set best value achieved.');
objTouLT_16B(xm,averagingtime);

SaveCurrentSextCor([dfname '_Best'])

diary off;

toc


return