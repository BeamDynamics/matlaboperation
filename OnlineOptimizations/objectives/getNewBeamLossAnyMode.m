function BLM_corrected=getNewBeamLossAnyMode(lossvac,averagingtime)
%BLM_corrected=getBeamLossAnyMode(lossvac,averagingtime)
%
% corrects measured BLM of 7/8 multibuynch considering current in
% single bunch, lifetime of single bunch and current decay 
% 
%
% lossvac= vacuum losses in []
% averagingtime= time to average lifetime measurement [s]
% 
%see also: getLifetimeLibera, getZEmittance, getTotCurrent, BunchLength



% get  lifetime
BLM0 = getNewBLM(averagingtime); % 
%LTs = getLifetimeSingle(); % 

% correct for current decay (rescale to 1mA/bunch)
Itot=getTotCurrent()/1000;
%Isin=getSingleBunchCurrent()/1000;
ey=getZEmittance(1); % 10 waits 1 second and makes 10 averages

fillid=tango.Device('sys/machstat/tango');

if fillid.Sr_mode.read==2
    modefill='16 bunch';%getFillingMode();
else
    modefill='multibunch';
end

switch modefill
    case '16 bunch'
    nbunch=16;
    IbNorm=(0.092)/nbunch; % mA
    otherwise 
    nbunch=992*7/8;
    IbNorm=(0.198)/nbunch; % mA
end

% normalization values
%IbsNorm=(4)/1;       % mA
eyNorm=0.005;        % nm

% bunch length parameters
Zn=.7;
Vrf=9.0e6; % reference voltage
U0=4.88e6;
E0=6.04e9;
alpha=1.78e-4;
sigdelta=1.06e-3;
circ=844.039;
h=992;
Ib=Itot/nbunch;
%Ibs=Isin/1;

BL = abs(BunchLength(Ib,Zn,Vrf,U0,E0,h,alpha,sigdelta,circ));
BLNorm = abs(BunchLength(IbNorm,Zn,Vrf,U0,E0,h,alpha,sigdelta,circ));
%BLs = BunchLength(Ibs,Zn,Vrf,U0,E0,h,alpha,sigdelta,circ);
%BLsNorm = BunchLength(IbsNorm,Zn,Vrf,U0,E0,h,alpha,sigdelta,circ);

%LTs_tou=(LTs*tauvac)/(tauvac-LTs); %remove vacuum lifetime component
%LTs_tou_cor = LTs_tou * (Ibs/IbsNorm) * (BLsNorm/BLs) * sqrt(eyNorm/ey); % assume same emittnace (not sure)

%LT_tou=(Itot-Isin)/(Itot/LT0-Isin/LTs_tou-Itot/tauvac);
BLM_tou=(Itot)/(Itot/BLM0-Itot/lossvac);
BLM_tou=BLM0;%(Itot)/(Itot/BLM0-Itot/lossvac);

% normalize to current, bunch length, emittance
BLM_corrected= BLM_tou * (Ib/IbNorm) * (BLNorm/BL) * sqrt(eyNorm/ey);


return
