function run_by_hand_knobs(MagType,Knobs,Range,varargin)
%RUN_BY_HAND_KNOBS Summary of this function goes here
%
%   run_by_hand_knobs(MagType,Knobs,Range,varargin)
%
%   MagType can be only SEXT, OCT, SKEW ( SF, SD maybe in the future)
%
%   Range is a single value then the range is from 
%   Strengths-range*knob to Strengths+range*knob
%
%   run_by_hand_knobs(MagType,Knobs,Range,'Automatic') 
%   will stop when SI3 is on and will save a magnet file
%
%   run_by_hand_knobs(MagType,Range,'IDLosses') 
%     will use only losses from the first BLM after SS except inj and RF
%     
%   run_by_hand_knobs(MagType,Range,'InjLosses') 
%     will use injection losses (need kickers pulsing!)
%
%   run_by_hand_knobs(MagType,Range,'Lifetime') 
%     will use the libera lifetime as objective
%
%   run_by_hand_knobs(MagType,Range,'Seed',7)
%       Option 'Seed' to select the seed of random number generator
%       (default seed is 1)
%       If seed is 0 or negative then the knobs are scaned in the order 
%       given in input.
%
%   run_by_hand_knobs(MagType,Range,'StartFrom',10)
%       Option 'StartFrom' is to start from a precise magnet of the list
%       (default value is 1)
%


[automatic,~]=getflag(varargin,'Automatic');
[IDLosses,~]=getflag(varargin,'IDLosses');
[InjLosses,~]=getflag(varargin,'InjLosses');
[Lifetime_flag,~]=getflag(varargin,'Lifetime');

[seed,varargin]=getoption(varargin,'Seed',1);
if seed>0
    rng(seed);  % initialise random number generator seed
end

[StartFrom,~]=getoption(varargin,'StartFrom',1);

% rips=tango.Device('sy/ps-rips/manager');
SI3=tango.Device('sr/ps-si/3');

npointscan = 7;   % number of values per scan
nacq=2;           % number of acquisition per value

switch MagType      % different options for magnets to scan
    case 'SEXT'
        mags = tango.Attribute('srmag/m-s/all/CorrectionStrengths');
        all_mags = [1:192];
        warning('please turn off AUTOCOR, On FOFB')
    case 'OCT'
        mags = tango.Attribute('srmag/m-o/all/CorrectionStrengths');
        all_mags = [1:64];
    case 'SKEW'
        mags = tango.Attribute('srmag/sqp/all/CorrectionStrengths');
        all_mags = [1:288];
        %all_mags([1 288])=[];
        %DeltaMaxSkew=1.0e-3;
        warning('please turn off AUTOCOR, On FOFB')
    case 'QUAD'
        mags = tango.Attribute('srmag/m-q/all/CorrectionStrengths');
        all_mags = [1:610];
end
if strcmp(MagType,'SKEW')   
    names = mags.device.CorrectorNames.read;
else
    names = mags.device.MagnetNames.read;
end
f1=figure(1);
f1.Position=[93 475 583 820];
subplot(2,1,1)
plot(0,0);

ylabel('total losses (a.u.)');
ax1=gca;
ax1.FontSize = 12;
grid on;
subplot(2,1,2)
plot(0,0);
xlabel(MagType);
ylabel('Lifetime (h)');
ax2=gca;
ax2.FontSize = 12;
grid on;

if seed > 0
    indmags = all_mags(randperm(length(all_mags))); % 2 = cell 5
else
    indmags = all_mags;
end

% listknobs
dirname=['Optim_' MagType '_' datestr(now,'YYYYmmDD_hhMMss')];
mkdir(dirname);
gains=zeros(1,length(Knobs(:,1)));
gains_LT=zeros(1,length(Knobs(:,1)));
residualLosses=zeros(1,length(Knobs(:,1)));
LifetimeEvolution=zeros(1,length(Knobs(:,1)));

% start the loop of the list of magnets to scan
for iknob = StartFrom:length(Knobs(:,1))
    knob=Knobs(iknob,:);
    Mags2Move=find(knob);
    %     sel = indmags(is);
    name = names(Mags2Move);
    
    disp(['Knob : ' num2str(iknob) '/' num2str(length(Knobs(:,1)))]);
    pause(1);
    K0=mags.set;
    % this is an array of 2 x length(Mags2Move)
    % line 1 is the lower limit, line 2 is the upper limit
%     range=[K0-abs(Range)*knob(Mags2Move);...
%         K0+abs(Range)*knob(Mags2Move)];
     range=[K0-abs(Range)*knob;...
        K0+abs(Range)*knob];
       
    K_mid = mean(range); % array of avarage
    K_delta = diff(range)/2;
   
    % define variation steps
    steps=[linspace(0,-1,ceil(npointscan/2)),...
        linspace(-1,+1,npointscan),...
        linspace(+1,0,ceil(npointscan/2))]'; %1D array of steps
    % this is a 2D array (2*npointscan+1,length(Mags2Move))
    % each line is the values of the magnets to move for that step
    mag_values=repmat(K_mid,2*npointscan +1,1)+K_delta.*steps;
    % the scan always starts and ends from the initial values of the
    % magnets
    
    Losses = NaN(1,length(mag_values(:,1)) * nacq);
    LT = NaN(1,length(mag_values(:,1)) * nacq);
    MagVal = nan(1,length(mag_values(:,1)) * nacq);
    
    % loop points for a given magnet
    ii=0;   % index of the steps to do (2*npointscan + 1)
    jj=0;   % index of the measurement (nacq * (2*npointscan + 1))
    BeamCurrent=tango.Device('srdiag/beam-current/total');

    while ii < length(mag_values(:,1))
        
        % check rips running. if running. reset scan to start from zero
        % again
        
        for mm=1:length(Mags2Move)
            magStrAttr{mm}=tango.Attribute([name{mm} '/Strength']);
        end
        if strcmp(SI3.State,'On')
            disp('Injection ON, set to initial values');
            
            for mm=1:length(Mags2Move)
                magStrAttr{mm}.set=K0(mm);
            end
            % pause(28); 
            
            disp("waiting injection")
            while strcmp(SI3.State,'On')
                pause(2); % wait for injection
            end
            disp("waiting losses/cleaning")
            pause(20);% tune to define restart
            
            if ~magsaved
                globalfilename=['Settings' datestr(now,'yyyymmmdd_HHMMSS') ];
                famsave=tango.Device('sys/settings/m-strengths');famsave.Timeout=10000;
                famsave.GenerateSettingsFile({['FILE:' globalfilename ''],'AUTHOR: sext optimization','COMMENTS: '});
                famsave=tango.Device('sys/settings/m-currents');famsave.Timeout=10000;
                famsave.GenerateSettingsFile({['FILE:' globalfilename ''],'AUTHOR: sext optimization','COMMENTS: '});
                magsaved=true;
                ii=0;
                jj=0;
                figure(2);
                saveas(gcf,fullfile(dirname,'ResidualLossesEvolution.fig'));
            else
                ii=0;
                jj=0;
            end
        else            
            magsaved=false;
            ii=ii+1;
            I0=BeamCurrent.Current.read;
            
            SetSextOptim(mags,mag_values(ii,:));
            pause(1);
            if Lifetime_flag
                pause(4);
            end
            
            for nn=1:nacq
                if nn==1
                    pause(1);
                end
                jj=jj+1;
                I=BeamCurrent.Current.read;
                if IDLosses
                    Losses(jj)=getIDLosses(1)*(I0/I)^2;
                elseif InjLosses
                    Losses(jj)=getInjectionLosses()*(I0/I)^2;
                else
                    Losses(jj)=getTotalLosses(1)*(I0/I)^2;
                end
                LT(jj)=getLifetimeLibera(1)*I/I0;
                
                MagVal(jj)=steps(ii)*Range;
            end
            figure(1);
            subplot(2,1,1)
            plot(ax1,MagVal,Losses,'*-','MarkerSize',12,'LineWidth',2);
            xlim([-1.2*Range 1.2*Range]);
            xlabel(['Knob ' num2str(iknob)]);
            ylabel('total losses (a.u.)');
            ax1.FontSize = 12;
            grid on;
            subplot(2,1,2)
            plot(ax2,MagVal,LT,'*-','MarkerSize',12,'LineWidth',2);
            xlim([-1.2*Range 1.2*Range]);
            xlabel(['Knob ' num2str(iknob)]);
            ylabel('Lifetime (h)');
            ax2.FontSize = 12;
            grid on;
        end
    end
    
    % find minimum of losses 
    MagVal_=MagVal(:);
    Losses_=Losses(:);
    
    [fitres,gof] = fit(MagVal_(~isnan(Losses_)),Losses_(~isnan(Losses_)),'poly2');
    rmse=gof.rmse;
    x = linspace(-Range,Range,201);
    f = polyval([fitres.p1,fitres.p2,fitres.p3],x);
    
    [mf,indmin]=min(f);
    Mf=max(f);
    xin=floor(interp1(x,1:201,MagVal(1)));
    InitialX=x(xin);
    InitialY=f(xin);
    
    RelVariation=(Mf-mf)/((Mf+mf)/2);
    if RelVariation>0.001 && RelVariation<4 && fitres.p1>0
        disp('taken')
    else
        indmin=xin;
        disp('change too small or too large, I keep the starting value');
    end
    optimal = x(indmin);
   
    gains(iknob)=(InitialY/f(indmin)-1);
    if iknob==StartFrom
        residualLosses(iknob)=1-gains(iknob);
    else
        residualLosses(iknob)=residualLosses(iknob-1)*(1-gains(iknob));
    end

    % find maximum of lifetime 
    MagVal_=MagVal(:);
    LT_=LT(:);
    
    [fitres_LT,gof_LT] = fit(MagVal_(~isnan(LT_)),LT_(~isnan(LT_)),'poly2');
    rmse_LT=gof_LT.rmse;
    x = linspace(-Range,+Range,201);
    f_LT = polyval([fitres_LT.p1,fitres_LT.p2,fitres_LT.p3],x);
    
    [Mf_LT,indmax_LT]=max(f_LT);
    mf_LT=min(f_LT);
    xin_LT=floor(interp1(x,1:201,MagVal(1)));
    InitialX_LT=x(xin_LT);
    InitialY_LT=f_LT(xin_LT);
    
    RelVariation=(Mf_LT-mf_LT)/((Mf_LT+mf_LT)/2);
    if RelVariation>0.001 && RelVariation<0.6 && fitres.p1>0
        disp('taken')
    else
        indmax_LT=xin_LT;
        disp('change too small or too large, I keep the starting value');
    end
    optimal_LT = x(indmax_LT);
  
    gains_LT(iknob)=(f_LT(indmax_LT)/InitialY_LT-1);
    if iknob==StartFrom
        LifetimeEvolution(iknob)=1+gains_LT(iknob);
    else
        LifetimeEvolution(iknob)=LifetimeEvolution(iknob-1)*(1+gains_LT(iknob));
    end

    % figure1 update
    figure(1);
    subplot(2,1,1) %losses part
    plot(ax1,MagVal,Losses,'o',...
        x,f,'-',...
        optimal,f(indmin),'x',...
        InitialX,InitialY,'s',...
        'LineWidth',3,'MarkerSize',10);
    xlabel(['Knob ' num2str(iknob)]);
    ylabel('total losses (a.u.)');
    ax1.FontSize = 12;
    grid on;
    legend('measured',['fit - rmse = ' num2str(rmse)],...
        ['optimum: - gain = ' num2str(100*gains(iknob),'%1.2f') ' %' ],...
        'initial');
    subplot(2,1,2) %lifetime part
    plot(ax2,MagVal,LT,'o',...
        x,f_LT,'-',...
        optimal_LT,f_LT(indmin),'x',...
        InitialX_LT,InitialY_LT,'s',...
        'LineWidth',3,'MarkerSize',10);
    xlabel(['Knob ' num2str(iknob)]);
    ylabel('Lifetime (h)');
    ax2.FontSize = 12;
    grid on;
    legend('measured',['fit - rmse = ' num2str(rmse_LT)],...
        ['optimum: - gain = ' num2str(100*gains_LT(iknob),'%1.2f') ' %' ],...
        'initial','Location','South');
   
    
    
    filename=strrep(['Scan_K' num2str(iknob) '.fig'],'/','_');
    filenamemat=strrep(['Scan_K' num2str(iknob) '.mat'],'/','_');
    filename=strrep(filename,'-','_');
    saveas(gcf,fullfile(dirname,filename));
    save(fullfile(dirname,filenamemat),'MagVal','Losses','fitres','gof',...
        'LT','fitres_LT','gof_LT','name');
    figure(2);
    hold off;
    if ~Lifetime_flag
        bar(StartFrom:iknob,residualLosses(StartFrom:iknob))
    else
        bar(StartFrom:iknob,LifetimeEvolution(StartFrom:iknob))
    end
    hold on; grid on;
    bar(StartFrom-1,1)
    xlabel('Magnet number')
    if ~Lifetime_flag
        ylabel('Residual losses')
    else
        ylabel('Lifetime evolution')
    end
    if ~Lifetime_flag
        ylim([(residualLosses(iknob)-0.1), 1.05]);
    else
        ylim([0.90, (LifetimeEvolution(iknob)+0.1)]);
    end
    
    if ~Lifetime_flag
        saveas(gcf,fullfile(dirname,'ResidualLossesEvolution.fig'));
    else
        saveas(gcf,fullfile(dirname,'LifetimeEvolution.fig'));
    end
    
    if ~Lifetime_flag
        if optimal~=0
            if ~automatic
                prompt = ['Input factor to optimal correction K*' num2str(optimal) '? [0=no correction] K= '];
                scale_factor=-1e-4;
                while scale_factor>1 | scale_factor<0
                    scale_factor = input(prompt);
                end
            else
                scale_factor=1;
            end
            % check rips runnning. if running wait.
            if scale_factor~=0
                
                SetSextOptim(mags,K0+optimal*K_delta*scale_factor,'FinalSet');
                disp('optimum has been set');
                pause(2);
            else
                % set skews
               SetSextOptim(mags,K0,'FinalSet');
                disp('initial has been set');
                pause(2);
            end
        else
            SetSextOptim(mags,K0,'FinalSet');
            disp('initial values has been set back');
        end
    else
        if optimal_LT~=0
            if ~automatic
                prompt = ['Input factor to optimal correction K*' num2str(optimal) '? [0=no correction] K= '];
                scale_factor=-1e-4;
                while scale_factor>1 | scale_factor<0
                    scale_factor = input(prompt);
                end
            else
                scale_factor=1;
            end
            % check rips runnning. if running wait.
            if scale_factor~=0
                % set sext
                SetSextOptim(mags,K0+optimal*K_delta*scale_factor,'FinalSet');
                disp('optimum has been set');
                pause(2);
            else
                % set sext
                SetSextOptim(mags,K0,'FinalSet');
                disp('initial has been set');
                pause(2);
            end
        else
            SetSextOptim(mags,K0,'FinalSet');
            disp('initial values has been set back');
        end
    end
end
globalfilename=['Settings_endOpt_' datestr(now,'yyyymmmdd_HHMMSS') ];
famsave=tango.Device('sys/settings/m-strengths');famsave.Timeout=10000;
famsave.GenerateSettingsFile({['FILE:' globalfilename ''],'AUTHOR: sext optimization','COMMENTS: '});
famsave=tango.Device('sys/settings/m-currents');famsave.Timeout=10000;
famsave.GenerateSettingsFile({['FILE:' globalfilename ''],'AUTHOR: sext optimization','COMMENTS: '});
figure(2);
if ~Lifetime_flag
    saveas(gcf,fullfile(dirname,'ResidualLossesEvolution.fig'));
else
    saveas(gcf,fullfile(dirname,'LifetimeEvolution.fig'));
end
end

