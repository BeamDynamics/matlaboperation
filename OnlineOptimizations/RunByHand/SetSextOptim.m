function SetSextOptim(SextDev,SextVal,varargin)
%    function SetSextOptim(SextDev,SextVal,varargin)
%       SetSextOptim(SextDev,SextVal,'FinalSet')
%   
%
%
n=0;

[FinalSet_flag,~]=getflag(varargin,'FinalSet');

SVal0=SextDev.set;
try
SextDev.set=SextVal;
pause(1);
SVal0=SextDev.set;
catch
    warning('Not set!')
end
while sum(abs(SVal0-SextVal))>0.3
    n=n+1;
    try
        SextDev.set=SextVal;
        SVal0=SextDev.set;
    catch
        pause(1);
    end
    if ~FinalSet_flag
        if n>2
            warning('Tried to set 2 times but something went wrong');
            break;
        end
    else
        warning(['Try to set the final values # ' num2str(n)]);
    end
end

end