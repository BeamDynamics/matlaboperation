function run_by_hand_singlemagnets(MagType,Range,varargin)
%RUN_BY_HAND_SINGLEMAGNET Summary of this function goes here
%
%   run_by_hand_singlemagnets(MagType,Range,varargin)
%
%   MagType can be OCT, SEXT, SF, SD, SKEW
%
%   if Range is a single value then the range is from K0-range to K0+range
%   if Range is an arrray of 2 elements:
%      if MagType is SEXT then Range(1) is for SF, Range(2) is for SD
%      if MagType is SKEW then Range(1) is for all skew except SH2, 
%                              Range(2) is for SH2
%      if MagType is OCT then the range is from Range(1) to Range(2)
%
%   run_by_hand_singlemagnets(MagType,Range,'Automatic') 
%   will stop when SI3 is on and will save a magnet file
%
%   run_by_hand_singlemagnets(MagType,Range,'IDLosses') 
%     will use only losses from the first BLM after SS except inj and RF
%     
%   run_by_hand_singlemagnets(MagType,Range,'Lifetime') 
%     will use the libera lifetime as objective
%
%   run_by_hand_singlemagnets(MagType,Range,'DA') 
%     will not rescale lifetime and losses with current
%
%   run_by_hand_singlemagnets(MagType,Range,'Seed',7)
%       Option 'Seed' to select the seed of random number generator
%       (default seed is 1)
%
%   run_by_hand_singlemagnets(MagType,Range,'StartFrom',10)
%       Option 'StartFrom' is to start from a precise magnet of the list
%       (default value is 1)
%


[automatic,~]=getflag(varargin,'Automatic');
[IDLosses,~]=getflag(varargin,'IDLosses');
[Lifetime_flag,~]=getflag(varargin,'Lifetime');
[DA_flag,~]=getflag(varargin,'DA');

[seed,varargin]=getoption(varargin,'Seed',1);
rng(seed);  % initialise random number generator seed

[StartFrom,~]=getoption(varargin,'StartFrom',1);

% rips=tango.Device('sy/ps-rips/manager');
SI3=tango.Device('sr/ps-si/3');

npointscan = 7;   % number of values per scan
nacq=2;           % number of acquisition per value

switch MagType      % different options for magnets to scan
    case 'OCT'
        mags = tango.Attribute('srmag/m-o/all/Strengths');
        all_mags = [1:64];
        %         all_mags([1,17])=[];
        %         all_mags=[64];
        %         all_mags = [2 18];
    case 'SKSEXT'
        mags = tango.Attribute('srmag/m-o/all/Strengths');
        %         all_mags = [1,17] ;
        %         all_mags=[64];
    case 'SF'
        mags = tango.Attribute('srmag/m-s/all/Strengths');
        all_mags = sort([2:6:192 5:6:192]);
    case 'SD'
        mags = tango.Attribute('srmag/m-s/all/Strengths');
        all_mags = sort([1:6:192 3:6:192 4:6:192 6:6:192]);
    case 'SEXT'
        mags = tango.Attribute('srmag/m-s/all/Strengths');
        all_mags = [1:192];
        warning('please turn off AUTOCOR, On FOFB')
    case 'SKEW'
        mags = tango.Attribute('srmag/sqp/all/CorrectionStrengths');
        all_mags = [1:288];
        all_mags([1 288])=[];
        DeltaMaxSkew=1.0e-3;
end

if strcmp(MagType,'SKEW')   
    names = mags.device.CorrectorNames.read;
else
    names = mags.device.MagnetNames.read;
end

f1=figure(1);
f1.Position=[93 475 583 820];
subplot(2,1,1)
plot(0,0);
xlabel(MagType);
ylabel('total losses (a.u.)');
ax1=gca;
ax1.FontSize = 12;
grid on;
subplot(2,1,2)
plot(0,0);
xlabel(MagType);
ylabel('Lifetime (h)');
ax2=gca;
ax2.FontSize = 12;
grid on;


indmags = all_mags(randperm(length(all_mags))); % 2 = cell 5
dirname=['Optim_' MagType '_' datestr(now,'YYYYmmDD_hhMMss')];
mkdir(dirname);
gains=zeros(1,length(indmags));
gains_LT=zeros(1,length(indmags));
residualLosses=zeros(1,length(indmags));
LifetimeEvolution=zeros(1,length(indmags));

% start the loop of the list of magnets to scan
for is = StartFrom:length(indmags)
    sel = indmags(is);
    name = names{sel};
    
    disp(['Magnet ' name ': ' num2str(is) '/' num2str(length(indmags))]);
    pause(1);
    
    K0 = tango.Attribute([name '/Strength']).set;
    if length(Range)==2
        if strcmp(MagType,'SEXT')
            if name(10)=='f'
                range=[K0-abs(Range(1)) K0+abs(Range(1))];
            else
                range=[K0-abs(Range(2)) K0+abs(Range(2))];
            end
        elseif strcmp(MagType,'SKEW')
            if name(12)=='h' && name(13)=='2'
                range=[K0-abs(Range(2)) K0+abs(Range(2))];
            else
                range=[K0-abs(Range(1)) K0+abs(Range(1))];
            end
        else
            range=Range;
        end
    else
        range=[K0-abs(Range) K0+abs(Range)]; 
    end
    
    K_mid = mean(range);%-175;
    K_delta = abs(diff(range))/2;
   
    % define variation steps
    mag_values=[linspace(K_mid,range(1),ceil(npointscan/2)),...
        linspace(range(1),range(2),npointscan),...
        linspace(range(2),K_mid,ceil(npointscan/2))];
        
    % % sort to start at closest to present setpoint
    [~,imin]=min((abs(mag_values-K0)));
    mag_values=circshift(mag_values,-(imin-1));
    mag_values = [mag_values,mag_values(1)];
    
    Losses = NaN(1,length(mag_values) * nacq);
    LT = NaN(1,length(mag_values) * nacq);
    MagVal = nan(1,length(mag_values) * nacq);
    
    % loop points for a given magnet
    ii=0;
    jj=0;
    BeamCurrent=tango.Device('srdiag/beam-current/total');

    while ii < length(mag_values)
        
        % check rips running. if running. reset scan to start from zero
        % again
        if strcmp(MagType,'SKEW')   
            magStrAttr=tango.Attribute([name '/CorrectionStrength']);
        else
            magStrAttr=tango.Attribute([name '/Strength']);
        end
        
        if strcmp(SI3.State,'On')
            
            disp('Injection ON, set to initial values');
            magStrAttr.set=K0;
            % pause(28); 
            
            disp("waiting injection")
            while strcmp(SI3.State,'On')
                pause(2); % wait for injection
            end
            disp("waiting losses/cleaning")
            pause(20);% tune to define restart
            
            if ~magsaved
                globalfilename=['Settings' datestr(now,'yyyymmmdd_HHMMSS') ];
                famsave=tango.Device('sys/settings/m-strengths');famsave.Timeout=10000;
                famsave.GenerateSettingsFile({['FILE:' globalfilename ''],'AUTHOR: sext optimization','COMMENTS: '});
                famsave=tango.Device('sys/settings/m-currents');famsave.Timeout=10000;
                famsave.GenerateSettingsFile({['FILE:' globalfilename ''],'AUTHOR: sext optimization','COMMENTS: '});
                magsaved=true;
                ii=0;
                jj=0;
                figure(2);
                saveas(gcf,fullfile(dirname,'ResidualLossesEvolution.fig'));
            else
                ii=0;
                jj=0;
            end
        else            
            magsaved=false;
            ii=ii+1;
            I0=BeamCurrent.Current.read;

            magStrAttr.set=mag_values(ii);
            pause(1);
            if Lifetime_flag
                pause(4);
            end
            
            for nn=1:nacq
                if nn==1
                    pause(1);
                end
                jj=jj+1;
                I=BeamCurrent.Current.read;
                if ~DA_flag
                    if IDLosses
                        Losses(jj)=getIDLosses(1)*(I0/I)^2;
                    else
                        Losses(jj)=getTotalLosses(1)*(I0/I)^2;
                    end
                    LT(jj)=getLifetimeLibera(1)*I/I0;
                else
                    if IDLosses
                        Losses(jj)=getIDLosses(1)*(I0/I);
                    else
                        Losses(jj)=getTotalLosses(1)*(I0/I);
                    end
                    LT(jj)=getLifetimeLibera(1);
                end
                MagVal(jj)=mag_values(ii);
            end
            figure(1);
            subplot(2,1,1)
            plot(ax1,MagVal,Losses,'*-','MarkerSize',12,'LineWidth',2);
            xlim([range(1)-0.2*K_delta range(2)+0.2*K_delta]);
            xlabel(name);
            ylabel('total losses (a.u.)');
            ax1.FontSize = 12;
            grid on;
            subplot(2,1,2)
            plot(ax2,MagVal,LT,'*-','MarkerSize',12,'LineWidth',2);
            xlim([range(1)-0.2*K_delta range(2)+0.2*K_delta]);
            xlabel(name);
            ylabel('Lifetime (h)');
            ax2.FontSize = 12;
            grid on;
        end
    end
    
    % find minimum of losses 
    MagVal_=MagVal(:);
    Losses_=Losses(:);
    
    [fitres,gof] = fit(MagVal_(~isnan(Losses_)),Losses_(~isnan(Losses_)),'poly2');
    rmse=gof.rmse;
    x = linspace(range(1),range(2),201);
    f = polyval([fitres.p1,fitres.p2,fitres.p3],x);
    
    [mf,indmin]=min(f);
    Mf=max(f);
    xin=floor(interp1(x,1:201,MagVal(1)));
    InitialX=x(xin);
    InitialY=f(xin);
    
    RelVariation=(Mf-mf)/((Mf+mf)/2);
    if RelVariation>0.001 && RelVariation<2 && fitres.p1>0
        disp('taken')
    else
        indmin=xin;
        disp('change too small or too large, I keep the starting value');
    end
    optimal = x(indmin);
   
    gains(is)=((InitialY-f(indmin))/InitialY);
    if is==StartFrom
        residualLosses(is)=1-gains(is);
    else
        residualLosses(is)=residualLosses(is-1)*(1-gains(is));
    end

    % find maximum of lifetime 
    MagVal_=MagVal(:);
    LT_=LT(:);
    
    [fitres_LT,gof_LT] = fit(MagVal_(~isnan(LT_)),LT_(~isnan(LT_)),'poly2');
    rmse_LT=gof_LT.rmse;
    x = linspace(range(1),range(2),201);
    f_LT = polyval([fitres_LT.p1,fitres_LT.p2,fitres_LT.p3],x);
    
    [Mf_LT,indmax_LT]=max(f_LT);
    mf_LT=min(f_LT);
    xin_LT=floor(interp1(x,1:201,MagVal(1)));
    InitialX_LT=x(xin_LT);
    InitialY_LT=f_LT(xin_LT);
    
    RelVariation=(Mf_LT-mf_LT)/((Mf_LT+mf_LT)/2);
    if RelVariation>0.001 && RelVariation<2 && fitres.p1>0
        disp('taken')
    else
        indmax_LT=xin_LT;
        disp('change too small or too large, I keep the starting value');
    end
    optimal_LT = x(indmax_LT);
  
    gains_LT(is)=(f_LT(indmax_LT)/InitialY_LT-1);
    if is==StartFrom
        LifetimeEvolution(is)=1+gains_LT(is);
    else
        LifetimeEvolution(is)=LifetimeEvolution(is-1)*(1+gains_LT(is));
    end

    % figure1 update
    figure(1);
    subplot(2,1,1) %losses part
    plot(ax1,MagVal,Losses,'o',...
        x,f,'-',...
        optimal,f(indmin),'x',...
        InitialX,InitialY,'s',...
        'LineWidth',3,'MarkerSize',10);
    xlabel(name);
    ylabel('total losses (a.u.)');
    ax1.FontSize = 12;
    grid on;
    legend('measured',['fit - rmse = ' num2str(rmse)],...
        ['optimum: - gain = ' num2str(100*gains(is),'%1.2f') ' %' ],...
        'initial');
    subplot(2,1,2) %lifetime part
    plot(ax2,MagVal,LT,'o',...
        x,f_LT,'-',...
        optimal_LT,f_LT(indmin),'x',...
        InitialX_LT,InitialY_LT,'s',...
        'LineWidth',3,'MarkerSize',10);
    xlabel(name);
    ylabel('Lifetime (h)');
    ax2.FontSize = 12;
    grid on;
    legend('measured',['fit - rmse = ' num2str(rmse_LT)],...
        ['optimum: - gain = ' num2str(100*gains_LT(is),'%1.2f') ' %' ],...
        'initial','Location','South');
   
    
    
    filename=strrep(['Scan_' name '.fig'],'/','_');
    filenamemat=strrep(['Scan_' name '.mat'],'/','_');
    filename=strrep(filename,'-','_');
    saveas(gcf,fullfile(dirname,filename));
    save(fullfile(dirname,filenamemat),'MagVal','Losses','fitres','gof',...
        'LT','fitres_LT','gof_LT','name');
    figure(2);
    hold off;
    if ~Lifetime_flag
        bar(StartFrom:is,residualLosses(StartFrom:is))
    else
        bar(StartFrom:is,LifetimeEvolution(StartFrom:is))
    end
    hold on; grid on;
    bar(StartFrom-1,1)
    xlabel('Magnet number')
    if ~Lifetime_flag
        ylabel('Residual losses')
    else
        ylabel('Lifetime evolution')
    end
    if ~Lifetime_flag
        ylim([(residualLosses(is)-0.1), 1.05]);
    else
        ylim([0.90, (LifetimeEvolution(is)+0.1)]);
    end
    
    if ~Lifetime_flag
        saveas(gcf,fullfile(dirname,'ResidualLossesEvolution.fig'));
    else
        saveas(gcf,fullfile(dirname,'LifetimeEvolution.fig'));
    end
    
    if ~Lifetime_flag
        if optimal~=0
            if ~automatic
                prompt = ['Input factor to optimal correction K*' num2str(optimal) '? [0=no correction] K= '];
                scale_factor=-1e-4;
                while scale_factor>1 | scale_factor<0
                    scale_factor = input(prompt);
                end
            else
                scale_factor=1;
                if strcmp(MagType,'SKEW')
                    if abs(optimal-MagVal(1))>DeltaMaxSkew
                        optimal=MagVal(1)+DeltaMaxSkew*sign(optimal-MagVal(1));
                    end
                end
            end
            % check rips runnning. if running wait.
            if scale_factor~=0
                % set skews
                magStrAttr.set=K0+(optimal-K0)*scale_factor;
                
                disp('optimum has been set');
                pause(2);
            else
                % set skews
                magStrAttr.set=K0;
                
                disp('initial has been set');
                pause(2);
            end
        else
            magStrAttr.set=K0;
            disp('initial values has been set back');
        end
    else
        if optimal_LT~=0
            if ~automatic
                prompt = ['Input factor to optimal correction K*' num2str(optimal) '? [0=no correction] K= '];
                scale_factor=-1e-4;
                while scale_factor>1 | scale_factor<0
                    scale_factor = input(prompt);
                end
            else
                scale_factor=1;
                if strcmp(MagType,'SKEW')
                    if abs(optimal-MagVal(1))>DeltaMaxSkew
                        optimal=MagVal(1)+DeltaMaxSkew*sign(optimal-MagVal(1));
                    end
                end
            end
            % check rips runnning. if running wait.
            if scale_factor~=0
                % set skews
                magStrAttr.set=K0+(optimal_LT-K0)*scale_factor;
                
                disp('optimum has been set');
                pause(2);
            else
                % set skews
                magStrAttr.set=K0;
                
                disp('initial has been set');
                pause(2);
            end
        else
            magStrAttr.set=K0;
            disp('initial values has been set back');
        end
    end
end
globalfilename=['Settings_endOpt_' datestr(now,'yyyymmmdd_HHMMSS') ];
famsave=tango.Device('sys/settings/m-strengths');famsave.Timeout=10000;
famsave.GenerateSettingsFile({['FILE:' globalfilename ''],'AUTHOR: sext optimization','COMMENTS: '});
famsave=tango.Device('sys/settings/m-currents');famsave.Timeout=10000;
famsave.GenerateSettingsFile({['FILE:' globalfilename ''],'AUTHOR: sext optimization','COMMENTS: '});
figure(2);
if ~Lifetime_flag
    saveas(gcf,fullfile(dirname,'ResidualLossesEvolution.fig'));
else
    saveas(gcf,fullfile(dirname,'LifetimeEvolution.fig'));
end
end

