function knob=KnobCreator(MagType,n,varargin)
%
%   knob=KnobCreator(MagType,n,varargin)
%
%   MagType can be:
%         SEXT, SF, SD, SD_AE, SD_BD, OCT, SEXT_INJ
%   n is the frequency of the sin wave
%   if SEXT_INJ, n is the number of injection sext, from 1 to 6.
%
%

[cos_flag,~]=getflag(varargin,'cos');

switch MagType      % different options for magnets to scan
    case 'SEXT'
        knob=ones(1,192);
    case 'SF'
        knob=ones(1,64);
    case 'SD'
        knob=ones(1,128);
    case 'SD_AE'
        knob=ones(1,64);
    case 'SD_BD'
        knob=ones(1,64);
    case 'OCT'
        knob=ones(1,64);
    case 'SEXT_INJ'
        knob=ones(1,192);
end

if cos_flag
    knob=cos(2*pi*n*(1:length(knob))/length(knob));
else
    knob=sin(2*pi*n*(1:length(knob))/length(knob));
end

switch MagType      % different options for magnets to scan
    case 'SEXT'
        knob192=knob;
    case 'SF'
        iSF=sort([2:6:192 5:6:192]);
        knob192=zeros(1,192);
        knob192(iSF)=knob;
    case 'SD'
        iSD=sort([1:6:192 3:6:192 4:6:192 6:6:192]);
        knob192=zeros(1,192);
        knob192(iSD)=knob;
    case 'SD_AE'
        iSD_AE=sort([1:6:192 6:6:192]);
        knob192=zeros(1,192);
        knob192(iSD_AE)=knob;
    case 'SD_BD'
        iSD_BD=sort([3:6:192 4:6:192]);
        knob192=zeros(1,192);
        knob192(iSD_BD)=knob;
    case 'OCT'
        knob192=knob;
    case 'SEXT_INJ'
        if n>6
            warning('there are only 6 injection cell sextupoles!')
            n=6;
        end
        knob192=zeros(1,192);
        knob192([n end+1-n])=1;
end
knob=knob192;
end
